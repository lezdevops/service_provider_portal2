
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'pagination',
    templateUrl: './pagination.component.html'
})
export class PaginationComponent {

    constructor(){
        
    }

    // @Input() page: number;  //current page
    // @Input() count: number; // total items 
    // @Input() perPage: number; // number of item we want tp show per page
    // @Input() pagesToShow: number; //how many pages between next and prev
    // @Input() loading: boolean;

    // @Output() goPrev = new EventEmitter<boolean>();
    // @Output() goNext = new EventEmitter<boolean>();
    // @Output() goPage = new EventEmitter<number>();

    // constructor() {

    // }

    // getMin(): number {
    //     return ((this.perPage * this.page) - this.perPage) + 1;
    // }

    // getMax(): number {
    //     let max = this.perPage * this.page;
    //     if (max > this.count) {
    //         max = this.count;
    //     }
    //     return max;
    // }

    // onPrev() {
    //     this.goPrev.emit(true);
    // }

    // onNext(next: boolean) {
    //     this.goNext.emit(next);
    // }

    // onPage(pageNum: number) {
    //     this.goPage.emit(pageNum);
    // }

    // getPages(): number[] {
    //     const c = Math.ceil(this.count / this.perPage);
    //     const p = this.page || 1;
    //     const pagesToShow = this.pagesToShow || 9;
    //     const pages: number[] = [];
    //     pages.push(p);
    //     const times = pagesToShow - 1;
    //     for (let i = 0; i < times; i++) {
    //         if (pages.length < pagesToShow) {
    //             if (Math.min.apply(null, pages) > 1) {
    //                 pages.push(Math.min.apply(null, pages) - 1);
    //             }
    //         }
    //         if (pages.length < pagesToShow) {
    //             if (Math.max.apply(null, pages) < c) {
    //                 pages.push(Math.max.apply(null, pages) + 1);
    //             }
    //         }
    //     }
    //     pages.sort((a, b) => a - b);
    //     return pages;
    // }

    // lastPage(): boolean {
    //     return this.perPage * this.page >= this.count;
    // }

}