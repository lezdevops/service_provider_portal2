import { Component, OnInit, NgZone } from '@angular/core';
import { BaseService } from './base.service';
import { Http, Response } from '@angular/http';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Router } from "@angular/router";
import * as Rx from 'rxjs';
import 'rxjs/add/observable/of';
//import { of } from 'rxjs';


import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import * as Handsontable from 'handsontable';
import { HotTableRegisterer } from '@handsontable/angular';
import { AlertService } from '../alert/alert.service'




export class BaseListComponent implements OnInit {


  // hot: Handsontable;
  isLoading: boolean = false;
  self = this;
  container: any;
  columns: any[];
  instance: string = "hotInstance";
  pagination: any;
  pages: Array<number>;


  rows = {};
  data = [];

  constructor(public alertService: AlertService,public baseService: BaseService, public fb: FormBuilder,
    public hotRegisterer: HotTableRegisterer, public route: Router,
    public modelName: string, public baseForm: string) {

  }

  ngOnInit() {

  }


  public fetchData(searchParam): Observable<any> {

    let observable_data = Rx.Observable.of(this.data);
    //searchParam["pageNum"] = pageNum;
    this.baseService.getAll(searchParam).
      subscribe(suc => {
        let dataSchema = suc;
        this.data = dataSchema["data"];
        this.columns = dataSchema["schema"];

        this.columns.push(this.appendEdit());
        this.columns.push(this.appendDelete());
        this.pagination = dataSchema["pagination"];
        this.pages = new Array(this.pagination["numOfPage"]);
        console.log(this.pagination);
        this.initializeHandsonTable();


      },
      err => {
        let msg = err && err.message;
        let status = err && err.status;
        console.log(msg);
        if (msg == 'UNAUTHORIZED' && status == 401) {
          this.alertService.warn("User is not authorized");
          this.route.navigate(['/login']);
        }
      })

    return observable_data;
  }



  initializeHandsonTable() {
    const hot = this.hotRegisterer.getInstance(this.instance);
    let i = this.columns.length;
    while (i--) {
      let col = this.columns[i];
      let visibility = col['visbility'];
      if (visibility == 'false') {
        this.columns.splice(i, 1);

      }
    }

    let settings = {
      data: this.data,
      columns: this.columns,
      colHeaders: true,
      rowHeaders: true,
      minSpareRows: 2,
      contextMenu: true,
      afterChange: (changes, source) => {
        if (!changes) {
          return;
        }

        var change = changes[0];
        var oldVal = change[2];
        var newVal = change[3];
        var rowIndex = change[0];
        var sourceData = hot.getSourceDataAtRow(rowIndex);
        delete sourceData['edit'];
        delete sourceData['delete'];
        //  var obj = { 'Shop': sourceData };
        let obj = sourceData;
        //  obj[this.modelName] = sourceData;
        this.rows[rowIndex] = obj;
      }

    };

    hot.updateSettings(settings, false);
  }





  appendEdit(): any {
    let self = this.self;
    let obj = {
      readonly: true,
      "data": "edit",
      "title": "Edit",
      "type": "text",
      "display": "true",
      "visbility": "true",
      "renderer": (instance, td, row, col, prop, value, cellProperties) => {
        let rowObj = instance.getDataAtRow(row);
        let id = rowObj[0];
        var button;
        if (id != null) {
          button = document.createElement('BUTTON');
          button.setAttribute("name", "Edit");
          button.addEventListener('click', function () {
            self.edit(value);
          })
          Handsontable.dom.empty(td);
          //  td.removeChilds();
          td.appendChild(button);
          return td;
        }
      }
    }
    return obj
  }




  appendDelete(): any {
    let self = this.self;
    let obj = {
      readonly: true,
      data: "delete",
      "title": "Delete",
      "type": "text",
      "display": "true",
      "visbility": "true",
      "renderer": (instance, td, row, col, prop, value, cellProperties) => {
        let rowObj = instance.getDataAtRow(row);
        let id = rowObj[0];
        var button;
        if (id != null) {
          button = document.createElement('BUTTON');
          button.setAttribute("name", "Delete");
          button.addEventListener('click', function () {
            self.delete(value);
          });
          Handsontable.dom.empty(td);
          //Handsontable.dom.empty(td);
          td.appendChild(button);
          return td;
        }
      }
    }
    return obj
  }

  delete(id) {

    this.baseService.delete(id).subscribe(res => {

      this.route.navigate(['/' + this.createListUrl]);
    });

  }



  edit(id) {
    let seachProperty: string = this.modelName + "_id_eq";
    let searchObj = { [this.modelName + "_id_eq"]: id };
    let searchParam = { "params": JSON.stringify(searchObj) }
    searchParam["pageNum"] = 1;
    this.baseService.getAll(searchParam).subscribe(res => {
      let obj = res["data"];
      this.baseService.object = obj[0];
      this.route.navigate(['/' + this.baseForm]);
    });

  }

  saveListData() {
    let objList = [];
    for (let key in this.rows) {
      let row = this.rows[key];
      objList.push(row);
    }
    this.baseService.postAll(objList).subscribe(res => {
      this.route.navigate(['/' + this.createListUrl]);
    });
  }





  // convert Shop to shops 
  createListUrl(): string {
    let firstChar: string = <string><any>this.modelName.charAt(0).toUpperCase;
    let listStr = firstChar.concat(this.modelName.substring(1, this.modelName.length));
    return listStr;

  }


  setPage(i, event: any) {
    event.preventDefault();
    let searchParam = this.baseService.initializeSearchParam('', i);
    this.fetchData(searchParam);

  }




}
