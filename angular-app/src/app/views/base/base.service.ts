import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/map";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { AuthenticationService } from "../../authentication/authentication.service";



@Injectable()
export class BaseService {

  baseUrl = 'http://localhost:8095/';
  models: string; //shops
  url = "";
  object: any;
  constructor(models, public http: Http, public authservice: AuthenticationService) {
    this.models = models;
    this.url = this.baseUrl + this.models;
  }
  headers = new Headers(
    {
      'Content-Type': 'application/json',
    });

  getAll(searchParam: any): Observable<any> {
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({ headers: this.headers, params: searchParam });
    return this.http.get(this.url, options).
      map((response: Response) => response.json()).catch((error: any) => Observable.throw(error.json()));

  }

  fetchWithoutPagination(searchParam: any): Observable<any> {
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({ headers: this.headers, params: searchParam });
    return this.http.get(this.url, options).
      map((response: Response) => response.json()).catch((error: any) => Observable.throw(error.json()));

  }

  

  postAll(model): Observable<any> {
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({ headers: this.headers });
    return this.http.post(this.url, JSON.stringify(model), options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  delete(id): Observable<any> {
    let options = new RequestOptions({ headers: this.headers });

    this.url += '/delete/' + id;
    return this.http.delete(this.url, options).map((response: Response) => response.json());

  }


  handleError(error: Response) {
    return Observable.throw(error.json().error);
  }


  set setter(obj: any) {
    this.object = obj;
  }

  get getter(): any {
    return this.object;
  }

  getModels(): string {
    alert(this.models);
    return this.models;
  }

  getModel() {
    return this.models.substring(0, this.models.length - 1);
  }

  initializeSearchParam(param, pageNum) : any {
    let searchParam = {};
    searchParam['pageNum'] = pageNum;
    if (param == '' || param == undefined) {
      searchParam['params'] = '';
    } else {
      searchParam['params'] = JSON.stringify(param);
    }
    return searchParam;
  }



}
