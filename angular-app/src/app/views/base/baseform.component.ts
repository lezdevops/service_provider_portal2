import { Component, OnInit, NgZone } from '@angular/core';
import { BaseService } from './base.service';
import { Http, Response } from '@angular/http';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Router, NavigationEnd } from "@angular/router";
import { AlertService } from '../alert/alert.service'






export class BaseFormComponent implements OnInit {



    constructor(public router: Router, public baseService: BaseService, public alertService: AlertService) {


        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.createForm();
            }
        })

    }

    public createForm() { };

    ngOnInit() {

    }

    create(formVal) {
        let objArr = [];
        let obj = formVal.value;
        console.log(JSON.stringify(obj));
        objArr.push(obj);
        this.baseService.postAll(objArr).subscribe(suc => {
            this.alertService.success(this.baseService.getModel() + " is inserted successfully");
            console.log(this.baseService.models);
            this.router.navigate(["/"+this.baseService.models]);
        });
    }




}
