import { Component, OnInit } from '@angular/core';
import { AlertService } from './alert.service';
import { Alert, AlertType } from './alert';

@Component({
    'selector': 'alert',
    'templateUrl': 'alert.component.html'

})

export class AlertComponent implements OnInit {

    alerts: Alert[] = [];
    constructor(private alertService: AlertService) {

    }

    ngOnInit() {
        this.alertService.getAlert().subscribe((alertt: Alert) => {
            if (!alertt) {
                this.alerts = [];
                return;
            }

            this.showNotification(alertt);
        })
    }

    showNotification(alertt) {
        this.alerts.push(alertt);
        setTimeout(() => {
            if (alertt) {
                this.alertService.clear();
            }
        }, 2000)
    }

    removeAlert(alert: Alert) {
        this.alerts = this.alerts.filter((x) => x != alert);
    }

    cssClass(alertt: Alert) {
        if (!alertt) {
            return;
        }

        console.log("Type " + alertt.type);
        switch (alertt.type) {
            case AlertType.SUCCESS:
                return 'alert alert-success';
            case AlertType.ERROR:
                return 'alert alert-danger';
            case AlertType.WARNING:
                return 'alert alert-warning';
            case AlertType.INFO:
                return 'alert alert-info';
        }
    }

}