import { Subject } from 'rxjs/Subject';
import { Alert, AlertType } from './alert';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class AlertService {

    private subject = new Subject<Alert>();
    private keepAfterRouterChange = false;

    constructor(public router: Router) {

        // router.events.subscribe((event) => {
        //     if (event instanceof NavigationStart) {
        //         if (this.keepAfterRouterChange) {
        //             this.keepAfterRouterChange = false;
        //         } else {
        //             this.clear();
        //         }
        //     }
        // })

    }


    getAlert(): Observable<Alert> {
        return this.subject.asObservable();
    }

    success(message: string, keepAfterRouterChange = false) {
        this.alert(AlertType.SUCCESS, message, keepAfterRouterChange);
    }

    warn(message: string, keepAfterRouterChange = false) {
        this.alert(AlertType.INFO, message, keepAfterRouterChange);
    }

    error(message: string, keepAfterRouterChange = false) {
        this.alert(AlertType.ERROR, message, keepAfterRouterChange);
    }

    info(message: string, keepAfterRouterChange = false) {
        this.alert(AlertType.INFO, message, keepAfterRouterChange);
    }

    alert(alertType: AlertType, message: string, keepAfterRouterChange = false) {
        this.keepAfterRouterChange = keepAfterRouterChange;
        let alert: Alert = new Alert(alertType, message);
        this.subject.next(alert);
    }

    clear() {
        this.subject.next();
    }

}