import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { SpServiceList }  from '../model/spServiceList';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class SpServiceListService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'spServiceListid':'1'
  });
  private spServiceList = new SpServiceList();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getSpServiceLists(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/spServiceLists',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedSpServiceLists(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/spServiceList/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedSpServiceListsWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/spServiceList/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getSpServiceList(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/spServiceList/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteSpServiceList(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/spServiceList/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createSpServiceList(spServiceList:SpServiceList){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/spServiceList',JSON.stringify(spServiceList),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateSpServiceList(spServiceList:SpServiceList){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/spServiceList',JSON.stringify(spServiceList),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(spServiceList:SpServiceList){
     this.spServiceList=spServiceList;
   }

  getter(){
    return this.spServiceList;
  }
}
