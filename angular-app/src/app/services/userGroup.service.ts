import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { UserGroup }  from '../model/userGroup';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class UserGroupService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'userGroupid':'1'
  });
  private userGroup = new UserGroup();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   
  hasPermission(pageName:string){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.rootUrl+'/hasPermission/check?pageName='+pageName,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getUserGroups(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/userGroups',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedUserGroups(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/userGroup/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedUserGroupsWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/userGroup/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getUserGroup(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/userGroup/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteUserGroup(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/userGroup/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createUserGroup(userGroup:UserGroup){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/userGroup',JSON.stringify(userGroup),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateUserGroup(userGroup:UserGroup){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/userGroup',JSON.stringify(userGroup),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(userGroup:UserGroup){
     this.userGroup=userGroup;
   }

  getter(){
    return this.userGroup;
  }
}
