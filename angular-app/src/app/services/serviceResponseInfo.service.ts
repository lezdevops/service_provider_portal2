import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ServiceResponseInfo }  from '../model/serviceResponseInfo';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class ServiceResponseInfoService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'serviceResponseInfoid':'1'
  });
  private serviceResponseInfo = new ServiceResponseInfo();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getServiceResponseInfos(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/serviceResponseInfos',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedServiceResponseInfos(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/serviceResponseInfo/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedServiceResponseInfosWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/serviceResponseInfo/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getServiceResponseInfo(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/serviceResponseInfo/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteServiceResponseInfo(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/serviceResponseInfo/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createServiceResponseInfo(serviceResponseInfo:ServiceResponseInfo){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/serviceResponseInfo',JSON.stringify(serviceResponseInfo),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateServiceResponseInfo(serviceResponseInfo:ServiceResponseInfo){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/serviceResponseInfo',JSON.stringify(serviceResponseInfo),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(serviceResponseInfo:ServiceResponseInfo){
     this.serviceResponseInfo=serviceResponseInfo;
   }

  getter(){
    return this.serviceResponseInfo;
  }
}
