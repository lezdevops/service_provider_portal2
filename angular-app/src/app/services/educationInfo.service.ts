import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { EducationInfo }  from '../model/educationInfo';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class EducationInfoService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'educationInfoid':'1'
  });
  private educationInfo = new EducationInfo();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getEducationInfos(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/educationInfos',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedEducationInfos(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/educationInfo/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedEducationInfosWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/educationInfo/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getEducationInfo(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/educationInfo/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteEducationInfo(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/educationInfo/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createEducationInfo(educationInfo:EducationInfo){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/educationInfo',JSON.stringify(educationInfo),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateEducationInfo(educationInfo:EducationInfo){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/educationInfo',JSON.stringify(educationInfo),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(educationInfo:EducationInfo){
     this.educationInfo=educationInfo;
   }

  getter(){
    return this.educationInfo;
  }
}
