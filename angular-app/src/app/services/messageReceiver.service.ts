import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { MessageReceiver }  from '../model/messageReceiver';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class MessageReceiverService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'messageReceiverid':'1'
  });
  private messageReceiver = new MessageReceiver();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getMessageReceivers(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/messageReceivers',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedMessageReceivers(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/messageReceiver/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedMessageReceiversWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/messageReceiver/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getMessageReceiver(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/messageReceiver/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteMessageReceiver(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/messageReceiver/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createMessageReceiver(messageReceiver:MessageReceiver){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/messageReceiver',JSON.stringify(messageReceiver),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateMessageReceiver(messageReceiver:MessageReceiver){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/messageReceiver',JSON.stringify(messageReceiver),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(messageReceiver:MessageReceiver){
     this.messageReceiver=messageReceiver;
   }

  getter(){
    return this.messageReceiver;
  }
  getNew(){
    return new MessageReceiver();
  }
}
