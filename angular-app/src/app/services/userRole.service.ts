import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { UserRole }  from '../model/userRole';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class UserRoleService extends CommonService {

    //old private headers = new Headers({'Content-Type':'application/json'});
    private headers = new Headers({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'userRoleid': '1'
    });
    private userRole = new UserRole();

    constructor(private _http: Http, public authservice: AuthenticationService) {
        super();
    }

    getUserRoles() {
        this.headers.set('Authorization', this.authservice.getToken());
        let options = new RequestOptions({ headers: this.headers });

        return this._http.get(this.baseUrl + '/userRoles', options).map((response: Response) => response.json())
            .catch(this.errorHandler);
    }
    createUserRoles(userRoles: UserRole[]) {
        this.headers.set('Authorization', this.authservice.getToken());
        let options = new RequestOptions({ headers: this.headers });

        return this._http.post(this.baseUrl + '/userRole', JSON.stringify(userRoles), options).map((response: Response) => response.json())
            .catch(this.errorHandler);
    }

    errorHandler(error: Response) {
        return Observable.throw(error || "SERVER ERROR");
    }

    setter(userRole: UserRole) {
        this.userRole = userRole;
    }

    getter() {
        return this.userRole;
    }
}
