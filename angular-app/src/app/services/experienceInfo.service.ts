import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ExperienceInfo }  from '../model/experienceInfo';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class ExperienceInfoService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'experienceInfoid':'1'
  });
  private experienceInfo = new ExperienceInfo();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getExperienceInfos(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/experienceInfos',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedExperienceInfos(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/experienceInfo/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedExperienceInfosWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/experienceInfo/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getExperienceInfo(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/experienceInfo/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteExperienceInfo(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/experienceInfo/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createExperienceInfo(experienceInfo:ExperienceInfo){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/experienceInfo',JSON.stringify(experienceInfo),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateExperienceInfo(experienceInfo:ExperienceInfo){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/experienceInfo',JSON.stringify(experienceInfo),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(experienceInfo:ExperienceInfo){
     this.experienceInfo=experienceInfo;
   }

  getter(){
    return this.experienceInfo;
  }
}
