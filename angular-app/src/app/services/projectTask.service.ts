import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ProjectTask }  from '../model/projectTask';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class ProjectTaskService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'projectTaskid':'1'
  });
  private projectTask = new ProjectTask();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getProjectTasks(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectTasks',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedProjectTasks(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectTask/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedProjectTasksWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectTask/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getProjectTask(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectTask/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteProjectTask(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/projectTask/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createProjectTask(projectTask:ProjectTask){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/projectTask',JSON.stringify(projectTask),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateProjectTask(projectTask:ProjectTask){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/projectTask',JSON.stringify(projectTask),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(projectTask:ProjectTask){
     this.projectTask=projectTask;
   }

  getter(){
    return this.projectTask;
  }
  getNew(){
    return new ProjectTask();
  }
}
