import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { SPDuty }  from '../model/sPDuty';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class SPDutyService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'sPDutyid':'1'
  });
  private sPDuty = new SPDuty();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getSPDutys(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/sPDutys',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedSPDutys(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/sPDuty/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedSPDutysWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/sPDuty/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getSPDuty(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/sPDuty/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteSPDuty(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/sPDuty/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createSPDuty(sPDuty:SPDuty){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/sPDuty',JSON.stringify(sPDuty),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateSPDuty(sPDuty:SPDuty){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/sPDuty',JSON.stringify(sPDuty),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(sPDuty:SPDuty){
     this.sPDuty=sPDuty;
   }

  getter(){
    return this.sPDuty;
  }
}
