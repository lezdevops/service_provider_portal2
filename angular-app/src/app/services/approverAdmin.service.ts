import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ApproverAdmin }  from '../model/approverAdmin';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class ApproverAdminService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'approverAdminid':'1'
  });
  private approverAdmin = new ApproverAdmin();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getApproverAdmins(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approverAdmins',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedApproverAdmins(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approverAdmin/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedApproverAdminsWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approverAdmin/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getApproverAdmin(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approverAdmin/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteApproverAdmin(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/approverAdmin/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createApproverAdmin(approverAdmin:ApproverAdmin){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/approverAdmin',JSON.stringify(approverAdmin),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateApproverAdmin(approverAdmin:ApproverAdmin){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/approverAdmin',JSON.stringify(approverAdmin),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(approverAdmin:ApproverAdmin){
     this.approverAdmin=approverAdmin;
   }

  getter(){
    return this.approverAdmin;
  }
  getNew(){
    return new ApproverAdmin();
  }
}
