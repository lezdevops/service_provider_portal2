import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { RegistrationQueue }  from '../model/registrationQueue';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class RegistrationQueueService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'registrationQueueid':'1'
  });
  private registrationQueue = new RegistrationQueue();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   
  hasPermission(pageName:string){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.rootUrl+'/hasPermission/check?pageName='+pageName,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getRegistrationQueues(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/registrationQueues',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedRegistrationQueues(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/registrationQueue/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedRegistrationQueuesWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/registrationQueue/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getRegistrationQueue(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/registrationQueue/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteRegistrationQueue(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/registrationQueue/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
    
  approveRegistrationQueue(registrationQueue:RegistrationQueue){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/approveRegistrationQueue',JSON.stringify(registrationQueue),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
    


  createRegistrationQueue(registrationQueue:RegistrationQueue){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/registrationQueue',JSON.stringify(registrationQueue),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateRegistrationQueue(registrationQueue:RegistrationQueue){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/registrationQueue',JSON.stringify(registrationQueue),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(registrationQueue:RegistrationQueue){
     this.registrationQueue=registrationQueue;
   }

  getter(){
    return this.registrationQueue;
  }
}
