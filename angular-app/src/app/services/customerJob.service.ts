import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { CustomerJob }  from '../model/customerJob';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class CustomerJobService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'customerJobid':'1'
  });
  private customerJob = new CustomerJob();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getCustomerJobs(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/customerJobs',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedCustomerJobs(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/customerJob/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedCustomerJobsWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/customerJob/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getCustomerJob(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/customerJob/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteCustomerJob(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/customerJob/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createCustomerJob(customerJob:CustomerJob){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/customerJob',JSON.stringify(customerJob),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateCustomerJob(customerJob:CustomerJob){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/customerJob',JSON.stringify(customerJob),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(customerJob:CustomerJob){
     this.customerJob=customerJob;
   }

  getter(){
    return this.customerJob;
  }
  getNew(){
    return new CustomerJob();
  }
}
