import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { SalesDetails }  from '../model/salesDetails';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class SalesDetailsService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'salesDetailsid':'1'
  });
  private salesDetails = new SalesDetails();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getSalesDetailss(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/salesDetailss',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedSalesDetailss(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/salesDetails/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedSalesDetailssWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/salesDetails/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getSalesDetails(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/salesDetails/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteSalesDetails(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/salesDetails/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createSalesDetails(salesDetails:SalesDetails){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/salesDetails',JSON.stringify(salesDetails),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateSalesDetails(salesDetails:SalesDetails){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/salesDetails',JSON.stringify(salesDetails),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(salesDetails:SalesDetails){
     this.salesDetails=salesDetails;
   }

  getter(){
    return this.salesDetails;
  }
}
