import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ProjectStory }  from '../model/projectStory';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class ProjectStoryService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'projectStoryid':'1'
  });
  private projectStory = new ProjectStory();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getProjectStorys(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectStorys',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedProjectStorys(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectStory/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedProjectStorysWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectStory/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getProjectStory(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectStory/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteProjectStory(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/projectStory/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createProjectStory(projectStory:ProjectStory){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/projectStory',JSON.stringify(projectStory),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateProjectStory(projectStory:ProjectStory){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/projectStory',JSON.stringify(projectStory),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(projectStory:ProjectStory){
     this.projectStory=projectStory;
   }

  getter(){
    return this.projectStory;
  }
  getNew(){
    return new ProjectStory();
  }
}
