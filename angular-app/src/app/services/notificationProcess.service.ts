import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { NotificationProcess }  from '../model/notificationProcess';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class NotificationProcessService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'notificationProcessid':'1'
  });
  private notificationProcess = new NotificationProcess();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getNotificationProcesss(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationProcesss',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedNotificationProcesss(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationProcess/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedNotificationProcesssWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationProcess/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getNotificationProcess(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationProcess/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteNotificationProcess(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/notificationProcess/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createNotificationProcess(notificationProcess:NotificationProcess){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/notificationProcess',JSON.stringify(notificationProcess),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  saveMultipleNotificationProcess(notificationProcesss:NotificationProcess[]){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/notificationProcess/saveMultipleNotificationProcessData',JSON.stringify(notificationProcesss),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
   
   updateNotificationProcess(notificationProcess:NotificationProcess){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/notificationProcess',JSON.stringify(notificationProcess),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(notificationProcess:NotificationProcess){
     this.notificationProcess=notificationProcess;
   }

  getter(){
    return this.notificationProcess;
  }
  getNew(){
    return new NotificationProcess();
  }
}
