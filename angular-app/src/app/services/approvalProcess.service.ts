import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ApprovalProcess }  from '../model/approvalProcess';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class ApprovalProcessService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'approvalProcessid':'1'
  });
  private approvalProcess = new ApprovalProcess();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getApprovalProcesss(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalProcesss',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedApprovalProcesss(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalProcess/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedApprovalProcesssWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalProcess/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getApprovalProcess(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalProcess/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteApprovalProcess(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/approvalProcess/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createApprovalProcess(approvalProcess:ApprovalProcess){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/approvalProcess',JSON.stringify(approvalProcess),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  saveMultipleApprovalProcess(approvalProcesss:ApprovalProcess[]){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/approvalProcess/saveMultipleApprovalProcessData',JSON.stringify(approvalProcesss),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  
   
   updateApprovalProcess(approvalProcess:ApprovalProcess){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/approvalProcess',JSON.stringify(approvalProcess),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(approvalProcess:ApprovalProcess){
     this.approvalProcess=approvalProcess;
   }

  getter(){
    return this.approvalProcess;
  }
  getNew(){
    return new ApprovalProcess();
  }
}
