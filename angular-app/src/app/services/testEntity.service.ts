import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { TestEntity }  from '../model/testEntity';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class TestEntityService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'testEntityid':'1'
  });
  private testEntity = new TestEntity();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getTestEntitys(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/testEntitys',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedTestEntitys(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/testEntity/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedTestEntitysWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/testEntity/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getTestEntity(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/testEntity/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteTestEntity(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/testEntity/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createTestEntity(testEntity:TestEntity){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/testEntity',JSON.stringify(testEntity),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateTestEntity(testEntity:TestEntity){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/testEntity',JSON.stringify(testEntity),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(testEntity:TestEntity){
     this.testEntity=testEntity;
   }

  getter(){
    return this.testEntity;
  }
  getNew(){
    return new TestEntity();
  }
}
