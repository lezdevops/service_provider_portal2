import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { PurchaseDetails }  from '../model/purchaseDetails';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class PurchaseDetailsService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'purchaseDetailsid':'1'
  });
  private purchaseDetails = new PurchaseDetails();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getPurchaseDetailss(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/purchaseDetailss',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedPurchaseDetailss(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/purchaseDetails/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedPurchaseDetailssWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/purchaseDetails/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPurchaseDetails(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/purchaseDetails/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deletePurchaseDetails(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/purchaseDetails/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createPurchaseDetails(purchaseDetails:PurchaseDetails){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/purchaseDetails',JSON.stringify(purchaseDetails),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updatePurchaseDetails(purchaseDetails:PurchaseDetails){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/purchaseDetails',JSON.stringify(purchaseDetails),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(purchaseDetails:PurchaseDetails){
     this.purchaseDetails=purchaseDetails;
   }

  getter(){
    return this.purchaseDetails;
  }
}
