import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ProjectSubtask }  from '../model/projectSubtask';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class ProjectSubtaskService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'projectSubtaskid':'1'
  });
  private projectSubtask = new ProjectSubtask();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getProjectSubtasks(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectSubtasks',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedProjectSubtasks(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectSubtask/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedProjectSubtasksWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectSubtask/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getProjectSubtask(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/projectSubtask/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteProjectSubtask(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/projectSubtask/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createProjectSubtask(projectSubtask:ProjectSubtask){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/projectSubtask',JSON.stringify(projectSubtask),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateProjectSubtask(projectSubtask:ProjectSubtask){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/projectSubtask',JSON.stringify(projectSubtask),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(projectSubtask:ProjectSubtask){
     this.projectSubtask=projectSubtask;
   }

  getter(){
    return this.projectSubtask;
  }
  getNew(){
    return new ProjectSubtask();
  }
}
