import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { CustomerRequest }  from '../model/customerRequest';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class CustomerRequestService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'customerRequestid':'1'
  });
  private customerRequest = new CustomerRequest();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getCustomerRequests(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/customerRequests',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedCustomerRequests(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/customerRequest/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedCustomerRequestsWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/customerRequest/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getCustomerRequest(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/customerRequest/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteCustomerRequest(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/customerRequest/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createCustomerRequest(customerRequest:CustomerRequest){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/customerRequest',JSON.stringify(customerRequest),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateCustomerRequest(customerRequest:CustomerRequest){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/customerRequest',JSON.stringify(customerRequest),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(customerRequest:CustomerRequest){
     this.customerRequest=customerRequest;
   }

  getter(){
    return this.customerRequest;
  }
  getNew(){
    return new CustomerRequest();
  }
}
