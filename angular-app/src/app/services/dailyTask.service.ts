import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { DailyTask }  from '../model/dailyTask';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class DailyTaskService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'dailyTaskid':'1'
  });
  private dailyTask = new DailyTask();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getDailyTasks(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/dailyTasks',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedDailyTasks(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/dailyTask/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedDailyTasksWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/dailyTask/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getDailyTask(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/dailyTask/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteDailyTask(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/dailyTask/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createDailyTask(dailyTask:DailyTask){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/dailyTask',JSON.stringify(dailyTask),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateDailyTask(dailyTask:DailyTask){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/dailyTask',JSON.stringify(dailyTask),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(dailyTask:DailyTask){
     this.dailyTask=dailyTask;
   }

  getter(){
    return this.dailyTask;
  }
}
