import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { AgentReference }  from '../model/agentReference';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class AgentReferenceService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'agentReferenceid':'1'
  });
  private agentReference = new AgentReference();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getAgentReferences(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/agentReferences',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedAgentReferences(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/agentReference/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedAgentReferencesWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/agentReference/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getAgentReference(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/agentReference/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteAgentReference(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/agentReference/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createAgentReference(agentReference:AgentReference){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/agentReference',JSON.stringify(agentReference),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateAgentReference(agentReference:AgentReference){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/agentReference',JSON.stringify(agentReference),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(agentReference:AgentReference){
     this.agentReference=agentReference;
   }

  getter(){
    return this.agentReference;
  }
}
