import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { LanguageInfo }  from '../model/languageInfo';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class LanguageInfoService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'languageInfoid':'1'
  });
  private languageInfo = new LanguageInfo();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getLanguageInfos(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/languageInfos',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedLanguageInfos(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/languageInfo/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedLanguageInfosWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/languageInfo/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getLanguageInfo(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/languageInfo/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteLanguageInfo(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/languageInfo/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createLanguageInfo(languageInfo:LanguageInfo){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/languageInfo',JSON.stringify(languageInfo),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateLanguageInfo(languageInfo:LanguageInfo){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/languageInfo',JSON.stringify(languageInfo),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(languageInfo:LanguageInfo){
     this.languageInfo=languageInfo;
   }

  getter(){
    return this.languageInfo;
  }
}
