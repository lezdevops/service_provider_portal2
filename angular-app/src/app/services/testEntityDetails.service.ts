import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { TestEntityDetails }  from '../model/testEntityDetails';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class TestEntityDetailsService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'testEntityDetailsid':'1'
  });
  private testEntityDetails = new TestEntityDetails();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getTestEntityDetailss(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/testEntityDetailss',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedTestEntityDetailss(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/testEntityDetails/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedTestEntityDetailssWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/testEntityDetails/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getTestEntityDetails(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/testEntityDetails/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteTestEntityDetails(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/testEntityDetails/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createTestEntityDetails(testEntityDetails:TestEntityDetails){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/testEntityDetails',JSON.stringify(testEntityDetails),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateTestEntityDetails(testEntityDetails:TestEntityDetails){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/testEntityDetails',JSON.stringify(testEntityDetails),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(testEntityDetails:TestEntityDetails){
     this.testEntityDetails=testEntityDetails;
   }

  getter(){
    return this.testEntityDetails;
  }
  getNew(){
    return new TestEntityDetails();
  }
}
