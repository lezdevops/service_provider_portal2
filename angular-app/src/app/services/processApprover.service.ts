import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ProcessApprover }  from '../model/processApprover';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class ProcessApproverService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'processApproverid':'1'
  });
  private processApprover = new ProcessApprover();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getProcessApprovers(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/processApprovers',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedProcessApprovers(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/processApprover/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedProcessApproversWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/processApprover/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getProcessApprover(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/processApprover/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteProcessApprover(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/processApprover/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createProcessApprover(processApprover:ProcessApprover){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/processApprover',JSON.stringify(processApprover),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateProcessApprover(processApprover:ProcessApprover){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/processApprover',JSON.stringify(processApprover),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(processApprover:ProcessApprover){
     this.processApprover=processApprover;
   }

  getter(){
    return this.processApprover;
  }
  getNew(){
    return new ProcessApprover();
  }
}
