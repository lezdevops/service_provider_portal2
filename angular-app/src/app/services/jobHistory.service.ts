import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { JobHistory }  from '../model/jobHistory';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class JobHistoryService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'jobHistoryid':'1'
  });
  private jobHistory = new JobHistory();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getJobHistorys(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/jobHistorys',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedJobHistorys(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/jobHistory/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedJobHistorysWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/jobHistory/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getJobHistory(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/jobHistory/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteJobHistory(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/jobHistory/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createJobHistory(jobHistory:JobHistory){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/jobHistory',JSON.stringify(jobHistory),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateJobHistory(jobHistory:JobHistory){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/jobHistory',JSON.stringify(jobHistory),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(jobHistory:JobHistory){
     this.jobHistory=jobHistory;
   }

  getter(){
    return this.jobHistory;
  }
}
