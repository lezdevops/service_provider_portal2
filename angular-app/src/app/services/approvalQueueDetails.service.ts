import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ApprovalQueueDetails }  from '../model/approvalQueueDetails';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class ApprovalQueueDetailsService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'approvalQueueDetailsid':'1'
  });
  private approvalQueueDetails = new ApprovalQueueDetails();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getApprovalQueueDetailss(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalQueueDetailss',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedApprovalQueueDetailss(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalQueueDetails/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedApprovalQueueDetailssWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalQueueDetails/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getApprovalQueueDetails(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalQueueDetails/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteApprovalQueueDetails(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/approvalQueueDetails/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createApprovalQueueDetails(approvalQueueDetails:ApprovalQueueDetails){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/approvalQueueDetails',JSON.stringify(approvalQueueDetails),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateApprovalQueueDetails(approvalQueueDetails:ApprovalQueueDetails){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/approvalQueueDetails',JSON.stringify(approvalQueueDetails),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(approvalQueueDetails:ApprovalQueueDetails){
     this.approvalQueueDetails=approvalQueueDetails;
   }

  getter(){
    return this.approvalQueueDetails;
  }
  getNew(){
    return new ApprovalQueueDetails();
  }
}
