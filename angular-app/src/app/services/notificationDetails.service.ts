import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { NotificationDetails }  from '../model/notificationDetails';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class NotificationDetailsService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'notificationDetailsid':'1'
  });
  private notificationDetails = new NotificationDetails();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getNotificationDetailss(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationDetailss',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedNotificationDetailss(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationDetails/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedNotificationDetailssWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationDetails/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getNotificationDetails(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationDetails/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteNotificationDetails(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/notificationDetails/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createNotificationDetails(notificationDetails:NotificationDetails){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/notificationDetails',JSON.stringify(notificationDetails),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateNotificationDetails(notificationDetails:NotificationDetails){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/notificationDetails',JSON.stringify(notificationDetails),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(notificationDetails:NotificationDetails){
     this.notificationDetails=notificationDetails;
   }

  getter(){
    return this.notificationDetails;
  }
  getNew(){
    return new NotificationDetails();
  }
}
