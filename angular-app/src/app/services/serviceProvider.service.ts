import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ServiceProvider }  from '../model/serviceProvider';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class ServiceProviderService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'serviceProviderid':'1'
  });
  private serviceProvider = new ServiceProvider();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getServiceProviders(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/serviceProviders',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedServiceProviders(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/serviceProvider/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedServiceProvidersWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/serviceProvider/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getServiceProvider(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/serviceProvider/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteServiceProvider(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/serviceProvider/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createServiceProvider(serviceProvider:ServiceProvider){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/serviceProvider',JSON.stringify(serviceProvider),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateServiceProvider(serviceProvider:ServiceProvider){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/serviceProvider',JSON.stringify(serviceProvider),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(serviceProvider:ServiceProvider){
     this.serviceProvider=serviceProvider;
   }

  getter(){
    return this.serviceProvider;
  }
  getNew(){
    return new ServiceProvider();
  }
}
