import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { RolePermission }  from '../model/rolePermission';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class RolePermissionService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'rolePermissionid':'1'
  });
  private rolePermission = new RolePermission();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getRolePermissions(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/rolePermissions',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedRolePermissions(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/rolePermission/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedRolePermissionsWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/rolePermission/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getRolePermission(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/rolePermission/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteRolePermission(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/rolePermission/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createRolePermission(rolePermission:RolePermission){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/rolePermission',JSON.stringify(rolePermission),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
  createRolePermissions(rolePermissions: RolePermission[]) {
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({ headers: this.headers });

    return this._http.post(this.baseUrl + '/createRolePermissions', JSON.stringify(rolePermissions), options).map((response: Response) => response.json())
            .catch(this.errorHandler);
  }
    
   
   updateRolePermission(rolePermission:RolePermission){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/rolePermission',JSON.stringify(rolePermission),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(rolePermission:RolePermission){
     this.rolePermission=rolePermission;
   }

  getter(){
    return this.rolePermission;
  }
}
