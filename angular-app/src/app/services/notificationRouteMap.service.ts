import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { NotificationRouteMap }  from '../model/notificationRouteMap';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class NotificationRouteMapService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'notificationRouteMapid':'1'
  });
  private notificationRouteMap = new NotificationRouteMap();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getNotificationRouteMaps(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationRouteMaps',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedNotificationRouteMaps(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationRouteMap/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedNotificationRouteMapsWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationRouteMap/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getNotificationRouteMap(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/notificationRouteMap/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteNotificationRouteMap(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/notificationRouteMap/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createNotificationRouteMap(notificationRouteMap:NotificationRouteMap){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/notificationRouteMap',JSON.stringify(notificationRouteMap),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateNotificationRouteMap(notificationRouteMap:NotificationRouteMap){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/notificationRouteMap',JSON.stringify(notificationRouteMap),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(notificationRouteMap:NotificationRouteMap){
     this.notificationRouteMap=notificationRouteMap;
   }

  getter(){
    return this.notificationRouteMap;
  }
  getNew(){
    return new NotificationRouteMap();
  }
}
