import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ApprovalQueue }  from '../model/approvalQueue';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class ApprovalQueueService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'approvalQueueid':'1'
  });
  private approvalQueue = new ApprovalQueue();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }
   

  getApprovalQueues(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalQueues',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedApprovalQueues(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalQueue/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedApprovalQueuesWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalQueue/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getApprovalQueue(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/approvalQueue/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteApprovalQueue(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/approvalQueue/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createApprovalQueue(approvalQueue:ApprovalQueue){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/approvalQueue',JSON.stringify(approvalQueue),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateApprovalQueue(approvalQueue:ApprovalQueue){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/approvalQueue',JSON.stringify(approvalQueue),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(approvalQueue:ApprovalQueue){
     this.approvalQueue=approvalQueue;
   }

  getter(){
    return this.approvalQueue;
  }
  getNew(){
    return new ApprovalQueue();
  }
}
