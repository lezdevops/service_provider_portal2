import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable}   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { TrainingInfo }  from '../model/trainingInfo';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from "../authentication/authentication.service";
import { CommonService } from './common.service';



@Injectable()
export class TrainingInfoService extends CommonService {
  
  //old private headers = new Headers({'Content-Type':'application/json'});
  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'trainingInfoid':'1'
  });
  private trainingInfo = new TrainingInfo();
  
  constructor(private _http:Http, public authservice: AuthenticationService) { 
    super();
  }

  getTrainingInfos(){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/trainingInfos',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedTrainingInfos(pageNo,size){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/trainingInfo/get?page='+pageNo+'&size='+size,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getPaginatedTrainingInfosWithSearchToken(pageNo,size,searchToken){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/trainingInfo/getPaginatedBy?page='+pageNo+'&size='+size+'&searchToken='+searchToken,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getTrainingInfo(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/trainingInfo/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  deleteTrainingInfo(id:Number){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.delete(this.baseUrl+'/trainingInfo/'+id,options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }


  createTrainingInfo(trainingInfo:TrainingInfo){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.post(this.baseUrl+'/trainingInfo',JSON.stringify(trainingInfo),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
   
   updateTrainingInfo(trainingInfo:TrainingInfo){
    this.headers.set('Authorization', this.authservice.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.put(this.baseUrl+'/trainingInfo',JSON.stringify(trainingInfo),  options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){

     return Observable.throw(error||"SERVER ERROR");
  }

   setter(trainingInfo:TrainingInfo){
     this.trainingInfo=trainingInfo;
   }

  getter(){
    return this.trainingInfo;
  }
}
