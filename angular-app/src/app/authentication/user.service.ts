import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {BaseService} from '../views/base/base.service';
import "rxjs/add/operator/map";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { AuthenticationService } from "./authentication.service";



@Injectable()
export class UserService extends BaseService {

  
  constructor(public http: Http,public authservice: AuthenticationService) {
    super("users",http,authservice);
  }




}
