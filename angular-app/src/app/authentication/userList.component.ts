import { Component, OnInit, NgZone } from '@angular/core';
import { UserService } from './user.service';
import { Http, Response } from '@angular/http';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import * as Handsontable from 'handsontable';
import { HotTableRegisterer } from '@handsontable/angular';
import { Router } from "@angular/router";
import { BaseListComponent } from '../views/base/baselist.component';
import { AlertService } from '../views/alert/alert.service'


@Component({
  selector: 'user-list',
  templateUrl: 'userList.component.html'
})
export class UserListComponent extends BaseListComponent implements OnInit {

  searchUserForm: FormGroup;
  Role_name_eq: FormControl;
  instance: string = "hotInstance";
  pagination: any;
  pages: Array<number>;
  isLoading: boolean = false;
  settings = {};
  self = this;


  constructor(public alertService: AlertService,public userService: UserService, public fb: FormBuilder,
    public _hotRegisterer: HotTableRegisterer, public route: Router) {
    super(alertService,userService, fb, _hotRegisterer, route,"User","userForm");
    this.createSearchForm();


  }

  createSearchForm() {
    this.searchUserForm = this.fb.group({
      User_userName_eq: ''
    });
  }


  searchUser() {
   let searchParam = this.userService.initializeSearchParam(this.searchUserForm.value, 1);

    this.fetchData(searchParam);
  }


  ngOnInit() {
    let searchParam = this.userService.initializeSearchParam('', 1);
    this.fetchData(searchParam);

  }

  
}
