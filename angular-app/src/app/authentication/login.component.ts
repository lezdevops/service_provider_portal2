import { Http, Response } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "./authentication.service";
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AlertService } from '../views/alert/alert.service'


@Component({
    selector: 'app-dashboard',
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

    loginForm: any;
    loading = false;
    error = '';
    message = '';
    
    ngOnInit(): void {
        //  this.authService.logout();
        this.createForm();
        this.authService.isLoggedIn.subscribe((res) => {
            if (res) {
                this.router.navigate(['/']);
            } else {

            }
        }, (error) => {
            console.log(error);
            this.router.navigate(['/500']);
        });
    }

    constructor(public alertService: AlertService, public router: Router, private authService: AuthenticationService,
        private route: ActivatedRoute,
        private fb: FormBuilder) {

    }




    login() {
        let credential = this.loginForm.value;
        let self = this;
        this.authService.authenticate(credential).subscribe((res) => {
            if (res) {
                this.alertService.success("Logged In");
                self.router.navigate(['/']);

            } else {
                self.router.navigate(['/login']);
                this.alertService.success("user or password is incorrect");
            }
        }, (error) => {
            console.log(error);
            //this.router.navigate(['/124']);
            self.router.navigate(['/login']);
            this.message = 'user or password is incorrect';
        });
    }

    createForm() {
        this.loginForm = this.fb.group({
            userName: null,
            password: null
        });
    }


}
