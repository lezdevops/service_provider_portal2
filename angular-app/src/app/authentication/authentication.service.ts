import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import 'rxjs/add/operator/finally';
import { Http, Response, RequestOptions, Headers } from '@angular/http';


@Injectable()
export class AuthenticationService implements OnInit {
    roleName:string="SUPER_ADMIN";
    pageName:string="ROLE";
    roles = [];
    permissions = [];
    rootUrl:string='http://localhost:8095';
    authenticated: boolean;
    loginForm: any;

    loggedIn = new BehaviorSubject<boolean>(this.tokenAvailable());

    url = 'http://localhost:8095/login';


    ngOnInit(): void {

    }

    headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private router: Router, private http: Http, private fb: FormBuilder) {

    }

    authenticate(credentials): Observable<boolean> {
        let options = new RequestOptions({ headers: this.headers });
        return this.http.post(this.url, JSON.stringify(credentials), options)
            .map((response: Response) => {
                let token = response.json() && response.json().token;
                let userType = response.json() && response.json().userType;
                let id = response.json() && response.json().id;
                this.roles = response.json() && response.json().roles;
                this.permissions = response.json() && response.json().permissions;
                
                //console.log(this.permissions); ok
                //console.log(this.roles); ok

                if (token) {
                    window.localStorage.clear();
                    window.localStorage.setItem('currentUser', JSON.stringify({
                        username: credentials.userName, 
                        id: id, 
                        token: token,
                        userType: userType,
                        roles: this.roles,
                        permissions: this.permissions  
                    }));
                    console.log("-----------------------");
                    console.log(window.localStorage);
                    this.loggedIn.next(true);
                    return true;
                } else {
                    return false;
                }

            }).catch((error: any) => Observable.throw(error.json().error));


    }
    getLoggedInUserInfo():any{
        return new Observable(observer=>{
            let currentUser = window.localStorage.getItem("currentUser");
            if(currentUser){
                var currentUserData = JSON.parse(currentUser);
                observer.next(currentUserData);
            }else{
                observer.next(false);
            }
            
        });
    }

    hasPermission(pageName:string):any{
        
        const observableData = new Observable(observer => {
            let currentUser = window.localStorage.getItem("currentUser");
            if(currentUser){
                var currentUserData = JSON.parse(currentUser);
                let permissions = currentUserData.permissions;
                let exists = false;
                let createPermission = pageName+"_CREATE";
                let readPermission = pageName+"_READ";
                let editPermission = pageName+"_EDIT";
                let deletePermission = pageName+"_DELETE";
                let reportPermission = pageName+"_REPORT";

                let hasCreatePermission = false;
                let hasReadPermission = false;
                let hasEditPermission = false;
                let hasDeletePermission = false;
                let hasReportPermission = false;
                permissions.forEach(element => {
                    if(element==createPermission){
                        hasCreatePermission = true;
                    }else if(element==readPermission){
                        hasReadPermission = true;
                    }else if(element==editPermission){
                        hasEditPermission = true;
                    }else if(element==deletePermission){
                        hasDeletePermission = true;
                    }else if(element==reportPermission){
                        hasReportPermission = true;
                    }
                });
                let permissionMap = {
                    "hasCreatePermission" : hasCreatePermission, 
                    "hasReadPermission" : hasReadPermission, 
                    "hasEditPermission" : hasEditPermission, 
                    "hasDeletePermission" : hasDeletePermission, 
                    "hasReportPermission" : hasReportPermission, 
                }
                observer.next(permissionMap); 
            }
       });
       return observableData;
    }
    isLoginUserModuleOwner(userType:string):any{
        return new Observable(observer=>{
            let currentUser = window.localStorage.getItem("currentUser");
            if(currentUser){
                var currentUserObj = JSON.parse(currentUser);
                let loggedInType = currentUserObj.userType;
                if(loggedInType!=undefined && loggedInType==userType){
                    observer.next(true);;
                }else{
                    observer.next(false);
                }

            }
            
        });
    }
    
    hasRole(roleName:string):any{
        const observableData = new Observable(observer => {
            let currentUser = window.localStorage.getItem("currentUser");
            if(currentUser){
                var currentUserData = JSON.parse(currentUser);
                let roles = currentUserData.roles;
                console.log(currentUserData);
                var exists = false;
                roles.forEach(element => {
                    if(element==roleName){
                        exists = true;
                    }
                });
                observer.next(exists); 
            }
            
        });
        return observableData;
    }
   
    errorHandler(error:Response){
        return Observable.throw(error||"SERVER ERROR");
    }

    logout(): void {
        //  window.localStorage.clear();
        window.localStorage.removeItem("currentUser");
        this.loggedIn.next(false);
        this.router.navigate(['/login']);
    }

    getToken(): string {
        let currentUser = JSON.parse(window.localStorage.getItem("currentUser"));
        let token = currentUser && currentUser.token;
        return token;
    }

     public userName(): string {
        let currentUser = JSON.parse(window.localStorage.getItem("currentUser"));
        let username = currentUser && currentUser.username;
        return username;
    }

     public userShopId(): string {
        let currentUser = JSON.parse(window.localStorage.getItem("currentUser"));
        let shopId = currentUser && currentUser.shopId;
        return shopId;
    }

    tokenAvailable(): boolean {
        let token = this.getToken();
        if (token) {
            return true;
        }
        return false;
    }

    createForm() {
        this.loginForm = this.fb.group({
            userName: '',
            password: ''
        });
    }

    get isLoggedIn() {
        //this.loggedIn.next(true);
        return this.loggedIn.asObservable();

    }





}
