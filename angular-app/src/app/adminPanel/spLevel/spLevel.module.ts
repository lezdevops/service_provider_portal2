// Angular
import { NgModule } from '@angular/core';
import { SpLevelRoutingModule } from './spLevel-routing.module';
import { ListspLevelComponent } from './listspLevel.component';
import { SpLevelFormComponent } from './spLevel-form.component';
import { SpLevelFormViewComponent } from './spLevel-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SpLevelService } from '../../services/spLevel.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';



@NgModule({
  imports: [
    SpLevelRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListspLevelComponent,
    SpLevelFormComponent,
    SpLevelFormViewComponent
  ],
  providers: [SpLevelService,
    
    ExcelService  
  ],
})
export class SpLevelModule { }
