import { Component, OnInit } from '@angular/core';
import { SpLevelService } from '../../services/spLevel.service';
import { SpLevel }  from '../../model/spLevel';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listspLevel',
  templateUrl: './listspLevel.component.html'
})
export class ListspLevelComponent implements OnInit {
  private spLevels:SpLevel[];
  
  constructor(private _spLevelService: SpLevelService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchSpLevel();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._spLevelService.getSpLevels().subscribe((result)=>{
      console.log(result);
      this.spLevels=result;
    },(error)=>{
      cospLevelnsole.log(error);
    })*/
    
  }
  paginatedSpLevels(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._spLevelService.getPaginatedSpLevels(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.spLevels=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchSpLevel(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._spLevelService.getPaginatedSpLevelsWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.spLevels=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedSpLevels(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedSpLevels(this.pageNo,this.size);
      this.searchSpLevel();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedSpLevels(this.pageNo,this.size);
    this.searchSpLevel();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedSpLevels(pageNo,this.size);
    this.searchSpLevel();
  }
  deleteSpLevel(spLevel){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._spLevelService.deleteSpLevel(spLevel.id).subscribe((data)=>{
        this.spLevels.splice(this.spLevels.indexOf(spLevel),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewSpLevel(spLevel){  
     this._spLevelService.setter(spLevel);
     this._router.navigate(['/adminPanel/spLevels/view']);
   }
   editSpLevel(spLevel){  
     this._spLevelService.setter(spLevel);
     this._router.navigate(['/adminPanel/spLevels/spLevelForm']);
   }
   newSpLevel(){
   let spLevel = new SpLevel();
    this._spLevelService.setter(spLevel);
     this._router.navigate(['/adminPanel/spLevels/spLevelForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.spLevels, 'SpLevel');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("SpLevel List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Short Code", dataKey: "shortCode"},{title: "Description", dataKey: "description"},{title: "Is Active", dataKey: "isActive"},
    ];
    doc.autoTable(reportColumns, this.spLevels, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('SpLevelList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('SpLevelList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
