import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SpLevel }  from '../../model/spLevel';
import { Router ,ActivatedRoute } from "@angular/router";
import { SpLevelService } from '../../services/spLevel.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-spLevel-form',
  templateUrl: './spLevel-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class SpLevelFormComponent implements OnInit {
  private spLevel:SpLevel;
  
    
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
    
  
shortCode_validationMessage = "";
description_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _spLevelService:SpLevelService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.spLevel=this._spLevelService.getter();
    this.spLevelValidationInitializer();  
      
  }
  spLevelValidationInitializer(){
     
this.shortCode_validationMessage = "";
this.description_validationMessage = "";
this.isActive_validationMessage = "";
  }
  clearSpLevelFormFields(){
     this.spLevel=this._spLevelService.getter();   
  }
  validationMessages(result){  
     
this.shortCode_validationMessage = result.shortCode_validationMessage
this.description_validationMessage = result.description_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
  }
  processSpLevelForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.spLevelValidationInitializer();
    if(this.spLevel.id==undefined){
       this._spLevelService.createSpLevel(this.spLevel).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/adminPanel/spLevels']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._spLevelService.updateSpLevel(this.spLevel).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/adminPanel/spLevels']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
