import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListspLevelComponent } from './listspLevel.component';
import { SpLevelFormComponent } from './spLevel-form.component';
import { SpLevelFormViewComponent } from './spLevel-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListspLevelComponent,
    data: {
      title: 'Sp Level List'
    }
  },{
      path: 'spLevelForm',
    component: SpLevelFormComponent,
    data: {
      title: 'Sp Level Form'
    }
  },{
      path: 'view',
    component: SpLevelFormViewComponent,
    data: {
      title: 'Sp Level Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpLevelRoutingModule {}
