import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Thana }  from '../../model/thana';
import { Router ,ActivatedRoute} from "@angular/router";
import { ThanaService } from '../../services/thana.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { District }  from '../../model/district';
import { DistrictService }  from '../../services/district.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-thana-view-form',
  templateUrl: './thana-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ThanaFormViewComponent implements OnInit {
  private thana:Thana;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private districts:District[];  
  
district_validationMessage = "";
description_validationMessage = "";
createdDate_validationMessage = "";
    
  constructor(
    private _thanaService:ThanaService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _districtService:DistrictService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.thana=this._thanaService.getter(); 
    this._districtService.getDistricts().subscribe((districts)=>{
   			      this.districts=districts;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newThana(){
   let thana = new Thana();
    this._thanaService.setter(thana);
     this._rotuer.navigate(['/adminPanel/thanas/thanaForm']);   
   }
  thanaEditAction(){
     this._thanaService.setter(this.thana);
     this._rotuer.navigate(['/adminPanel/thanas/thanaForm']);        
  }
  printThanaDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printThanaFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ThanaList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
