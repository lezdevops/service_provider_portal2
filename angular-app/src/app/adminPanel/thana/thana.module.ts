// Angular
import { NgModule } from '@angular/core';
import { ThanaRoutingModule } from './thana-routing.module';
import { ListthanaComponent } from './listthana.component';
import { ThanaFormComponent } from './thana-form.component';
import { ThanaFormViewComponent } from './thana-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ThanaService } from '../../services/thana.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { DistrictService }  from '../../services/district.service';

@NgModule({
  imports: [
    ThanaRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListthanaComponent,
    ThanaFormComponent,
    ThanaFormViewComponent
  ],
  providers: [ThanaService,
    DistrictService,
    ExcelService  
  ],
})
export class ThanaModule { }
