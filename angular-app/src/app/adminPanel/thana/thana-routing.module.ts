import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListthanaComponent } from './listthana.component';
import { ThanaFormComponent } from './thana-form.component';
import { ThanaFormViewComponent } from './thana-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListthanaComponent,
    data: {
      title: 'Thana List'
    }
  },{
      path: 'thanaForm',
    component: ThanaFormComponent,
    data: {
      title: 'Thana Form'
    }
  },{
      path: 'view',
    component: ThanaFormViewComponent,
    data: {
      title: 'Thana Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThanaRoutingModule {}
