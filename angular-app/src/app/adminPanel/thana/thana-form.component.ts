import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Thana }  from '../../model/thana';
import { Router ,ActivatedRoute } from "@angular/router";
import { ThanaService } from '../../services/thana.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { District }  from '../../model/district';
import { DistrictService }  from '../../services/district.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-thana-form',
  templateUrl: './thana-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ThanaFormComponent implements OnInit {
  private thana:Thana;
  
    
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private districts:District[];  
  
district_validationMessage = "";
description_validationMessage = "";
createdDate_validationMessage = "";
    
  constructor(
    private _thanaService:ThanaService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _districtService:DistrictService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.thana=this._thanaService.getter();
    this.thanaValidationInitializer();  
    this._districtService.getDistricts().subscribe((districts)=>{
   			      this.districts=districts;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  thanaValidationInitializer(){
     
this.district_validationMessage = "";
this.description_validationMessage = "";
this.createdDate_validationMessage = "";
  }
  clearThanaFormFields(){
     this.thana=this._thanaService.getter();   
  }
  validationMessages(result){  
     
this.district_validationMessage = result.district_validationMessage
this.description_validationMessage = result.description_validationMessage
this.createdDate_validationMessage = result.createdDate_validationMessage
  }
  processThanaForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.thanaValidationInitializer();
    if(this.thana.id==undefined){
       this._thanaService.createThana(this.thana).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/adminPanel/thanas']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._thanaService.updateThana(this.thana).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/adminPanel/thanas']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
