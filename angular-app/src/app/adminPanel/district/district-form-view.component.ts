import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { District }  from '../../model/district';
import { Router ,ActivatedRoute} from "@angular/router";
import { DistrictService } from '../../services/district.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { Division }  from '../../model/division';
import { DivisionService }  from '../../services/division.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-district-view-form',
  templateUrl: './district-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class DistrictFormViewComponent implements OnInit {
  private district:District;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private divisions:Division[];  
  
division_validationMessage = "";
description_validationMessage = "";
createdDate_validationMessage = "";
    
  constructor(
    private _districtService:DistrictService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _divisionService:DivisionService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.district=this._districtService.getter(); 
    this._divisionService.getDivisions().subscribe((divisions)=>{
   			      this.divisions=divisions;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newDistrict(){
   let district = new District();
    this._districtService.setter(district);
     this._rotuer.navigate(['/adminPanel/districts/districtForm']);   
   }
  districtEditAction(){
     this._districtService.setter(this.district);
     this._rotuer.navigate(['/adminPanel/districts/districtForm']);        
  }
  printDistrictDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printDistrictFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('DistrictList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
