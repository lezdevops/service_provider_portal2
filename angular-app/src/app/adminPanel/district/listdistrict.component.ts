import { Component, OnInit } from '@angular/core';
import { DistrictService } from '../../services/district.service';
import { District }  from '../../model/district';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listdistrict',
  templateUrl: './listdistrict.component.html'
})
export class ListdistrictComponent implements OnInit {
  private districts:District[];
  
  constructor(private _districtService: DistrictService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchDistrict();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._districtService.getDistricts().subscribe((result)=>{
      console.log(result);
      this.districts=result;
    },(error)=>{
      codistrictnsole.log(error);
    })*/
    
  }
  paginatedDistricts(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._districtService.getPaginatedDistricts(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.districts=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchDistrict(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._districtService.getPaginatedDistrictsWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.districts=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedDistricts(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedDistricts(this.pageNo,this.size);
      this.searchDistrict();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedDistricts(this.pageNo,this.size);
    this.searchDistrict();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedDistricts(pageNo,this.size);
    this.searchDistrict();
  }
  deleteDistrict(district){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._districtService.deleteDistrict(district.id).subscribe((data)=>{
        this.districts.splice(this.districts.indexOf(district),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewDistrict(district){  
     this._districtService.setter(district);
     this._router.navigate(['/adminPanel/districts/view']);
   }
   editDistrict(district){  
     this._districtService.setter(district);
     this._router.navigate(['/adminPanel/districts/districtForm']);
   }
   newDistrict(){
   let district = new District();
    this._districtService.setter(district);
     this._router.navigate(['/adminPanel/districts/districtForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.districts, 'District');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("District List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Division", dataKey: "division"},{title: "Description", dataKey: "description"},{title: "Created Date", dataKey: "createdDate"},
    ];
    doc.autoTable(reportColumns, this.districts, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('DistrictList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('DistrictList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
