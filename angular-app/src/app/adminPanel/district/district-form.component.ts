import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { District }  from '../../model/district';
import { Router ,ActivatedRoute } from "@angular/router";
import { DistrictService } from '../../services/district.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Division }  from '../../model/division';
import { DivisionService }  from '../../services/division.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-district-form',
  templateUrl: './district-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class DistrictFormComponent implements OnInit {
  private district:District;
  
    
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private divisions:Division[];  
  
division_validationMessage = "";
description_validationMessage = "";
createdDate_validationMessage = "";
    
  constructor(
    private _districtService:DistrictService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _divisionService:DivisionService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.district=this._districtService.getter();
    this.districtValidationInitializer();  
    this._divisionService.getDivisions().subscribe((divisions)=>{
   			      this.divisions=divisions;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  districtValidationInitializer(){
     
this.division_validationMessage = "";
this.description_validationMessage = "";
this.createdDate_validationMessage = "";
  }
  clearDistrictFormFields(){
     this.district=this._districtService.getter();   
  }
  validationMessages(result){  
     
this.division_validationMessage = result.division_validationMessage
this.description_validationMessage = result.description_validationMessage
this.createdDate_validationMessage = result.createdDate_validationMessage
  }
  processDistrictForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.districtValidationInitializer();
    if(this.district.id==undefined){
       this._districtService.createDistrict(this.district).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/adminPanel/districts']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._districtService.updateDistrict(this.district).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/adminPanel/districts']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
