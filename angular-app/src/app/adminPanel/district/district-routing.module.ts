import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListdistrictComponent } from './listdistrict.component';
import { DistrictFormComponent } from './district-form.component';
import { DistrictFormViewComponent } from './district-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListdistrictComponent,
    data: {
      title: 'District List'
    }
  },{
      path: 'districtForm',
    component: DistrictFormComponent,
    data: {
      title: 'District Form'
    }
  },{
      path: 'view',
    component: DistrictFormViewComponent,
    data: {
      title: 'District Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DistrictRoutingModule {}
