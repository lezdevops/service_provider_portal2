// Angular
import { NgModule } from '@angular/core';
import { DistrictRoutingModule } from './district-routing.module';
import { ListdistrictComponent } from './listdistrict.component';
import { DistrictFormComponent } from './district-form.component';
import { DistrictFormViewComponent } from './district-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DistrictService } from '../../services/district.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { DivisionService }  from '../../services/division.service';

@NgModule({
  imports: [
    DistrictRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListdistrictComponent,
    DistrictFormComponent,
    DistrictFormViewComponent
  ],
  providers: [DistrictService,
    DivisionService,
    ExcelService  
  ],
})
export class DistrictModule { }
