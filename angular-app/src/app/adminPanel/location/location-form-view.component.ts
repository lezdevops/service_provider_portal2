import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Location }  from '../../model/location';
import { Router ,ActivatedRoute} from "@angular/router";
import { LocationService } from '../../services/location.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { Thana }  from '../../model/thana';
import { ThanaService }  from '../../services/thana.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-location-view-form',
  templateUrl: './location-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class LocationFormViewComponent implements OnInit {
  private location:Location;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private thanas:Thana[];  
  
thana_validationMessage = "";
description_validationMessage = "";
createdDate_validationMessage = "";
    
  constructor(
    private _locationService:LocationService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _thanaService:ThanaService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.location=this._locationService.getter(); 
    this._thanaService.getThanas().subscribe((thanas)=>{
   			      this.thanas=thanas;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newLocation(){
   let location = new Location();
    this._locationService.setter(location);
     this._rotuer.navigate(['/adminPanel/locations/locationForm']);   
   }
  locationEditAction(){
     this._locationService.setter(this.location);
     this._rotuer.navigate(['/adminPanel/locations/locationForm']);        
  }
  printLocationDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printLocationFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('LocationList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
