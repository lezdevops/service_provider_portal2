import { Component, OnInit } from '@angular/core';
import { LocationService } from '../../services/location.service';
import { Location }  from '../../model/location';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listlocation',
  templateUrl: './listlocation.component.html'
})
export class ListlocationComponent implements OnInit {
  private locations:Location[];
  
  constructor(private _locationService: LocationService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchLocation();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._locationService.getLocations().subscribe((result)=>{
      console.log(result);
      this.locations=result;
    },(error)=>{
      colocationnsole.log(error);
    })*/
    
  }
  paginatedLocations(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._locationService.getPaginatedLocations(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.locations=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchLocation(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._locationService.getPaginatedLocationsWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.locations=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedLocations(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedLocations(this.pageNo,this.size);
      this.searchLocation();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedLocations(this.pageNo,this.size);
    this.searchLocation();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedLocations(pageNo,this.size);
    this.searchLocation();
  }
  deleteLocation(location){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._locationService.deleteLocation(location.id).subscribe((data)=>{
        this.locations.splice(this.locations.indexOf(location),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewLocation(location){  
     this._locationService.setter(location);
     this._router.navigate(['/adminPanel/locations/view']);
   }
   editLocation(location){  
     this._locationService.setter(location);
     this._router.navigate(['/adminPanel/locations/locationForm']);
   }
   newLocation(){
   let location = new Location();
    this._locationService.setter(location);
     this._router.navigate(['/adminPanel/locations/locationForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.locations, 'Location');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("Location List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Thana", dataKey: "thana"},{title: "Description", dataKey: "description"},{title: "Created Date", dataKey: "createdDate"},
    ];
    doc.autoTable(reportColumns, this.locations, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('LocationList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('LocationList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
