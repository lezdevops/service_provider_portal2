import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Location }  from '../../model/location';
import { Router ,ActivatedRoute } from "@angular/router";
import { LocationService } from '../../services/location.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Thana }  from '../../model/thana';
import { ThanaService }  from '../../services/thana.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-location-form',
  templateUrl: './location-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class LocationFormComponent implements OnInit {
  private location:Location;
  
    
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private thanas:Thana[];  
  
thana_validationMessage = "";
description_validationMessage = "";
createdDate_validationMessage = "";
    
  constructor(
    private _locationService:LocationService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _thanaService:ThanaService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.location=this._locationService.getter();
    this.locationValidationInitializer();  
    this._thanaService.getThanas().subscribe((thanas)=>{
   			      this.thanas=thanas;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  locationValidationInitializer(){
     
this.thana_validationMessage = "";
this.description_validationMessage = "";
this.createdDate_validationMessage = "";
  }
  clearLocationFormFields(){
     this.location=this._locationService.getter();   
  }
  validationMessages(result){  
     
this.thana_validationMessage = result.thana_validationMessage
this.description_validationMessage = result.description_validationMessage
this.createdDate_validationMessage = result.createdDate_validationMessage
  }
  processLocationForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.locationValidationInitializer();
    if(this.location.id==undefined){
       this._locationService.createLocation(this.location).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/adminPanel/locations']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._locationService.updateLocation(this.location).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/adminPanel/locations']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
