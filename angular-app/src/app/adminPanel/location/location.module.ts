// Angular
import { NgModule } from '@angular/core';
import { LocationRoutingModule } from './location-routing.module';
import { ListlocationComponent } from './listlocation.component';
import { LocationFormComponent } from './location-form.component';
import { LocationFormViewComponent } from './location-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LocationService } from '../../services/location.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ThanaService }  from '../../services/thana.service';

@NgModule({
  imports: [
    LocationRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListlocationComponent,
    LocationFormComponent,
    LocationFormViewComponent
  ],
  providers: [LocationService,
    ThanaService,
    ExcelService  
  ],
})
export class LocationModule { }
