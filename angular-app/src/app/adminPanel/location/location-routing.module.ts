import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListlocationComponent } from './listlocation.component';
import { LocationFormComponent } from './location-form.component';
import { LocationFormViewComponent } from './location-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListlocationComponent,
    data: {
      title: 'Location List'
    }
  },{
      path: 'locationForm',
    component: LocationFormComponent,
    data: {
      title: 'Location Form'
    }
  },{
      path: 'view',
    component: LocationFormViewComponent,
    data: {
      title: 'Location Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationRoutingModule {}
