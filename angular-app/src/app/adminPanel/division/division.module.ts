// Angular
import { NgModule } from '@angular/core';
import { DivisionRoutingModule } from './division-routing.module';
import { ListdivisionComponent } from './listdivision.component';
import { DivisionFormComponent } from './division-form.component';
import { DivisionFormViewComponent } from './division-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DivisionService } from '../../services/division.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { CountryService }  from '../../services/country.service';

@NgModule({
  imports: [
    DivisionRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListdivisionComponent,
    DivisionFormComponent,
    DivisionFormViewComponent
  ],
  providers: [DivisionService,
    CountryService,
    ExcelService  
  ],
})
export class DivisionModule { }
