import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Division }  from '../../model/division';
import { Router ,ActivatedRoute} from "@angular/router";
import { DivisionService } from '../../services/division.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { Country }  from '../../model/country';
import { CountryService }  from '../../services/country.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-division-view-form',
  templateUrl: './division-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class DivisionFormViewComponent implements OnInit {
  private division:Division;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private countrys:Country[];  
  
country_validationMessage = "";
description_validationMessage = "";
createdDate_validationMessage = "";
    
  constructor(
    private _divisionService:DivisionService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _countryService:CountryService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.division=this._divisionService.getter(); 
    this._countryService.getCountrys().subscribe((countrys)=>{
   			      this.countrys=countrys;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newDivision(){
   let division = new Division();
    this._divisionService.setter(division);
     this._rotuer.navigate(['/adminPanel/divisions/divisionForm']);   
   }
  divisionEditAction(){
     this._divisionService.setter(this.division);
     this._rotuer.navigate(['/adminPanel/divisions/divisionForm']);        
  }
  printDivisionDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printDivisionFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('DivisionList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
