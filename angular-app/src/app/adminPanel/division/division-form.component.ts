import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Division }  from '../../model/division';
import { Router ,ActivatedRoute } from "@angular/router";
import { DivisionService } from '../../services/division.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Country }  from '../../model/country';
import { CountryService }  from '../../services/country.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-division-form',
  templateUrl: './division-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class DivisionFormComponent implements OnInit {
  private division:Division;
  
    
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private countrys:Country[];  
  
country_validationMessage = "";
description_validationMessage = "";
createdDate_validationMessage = "";
    
  constructor(
    private _divisionService:DivisionService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _countryService:CountryService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.division=this._divisionService.getter();
    this.divisionValidationInitializer();  
    this._countryService.getCountrys().subscribe((countrys)=>{
   			      this.countrys=countrys;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  divisionValidationInitializer(){
     
this.country_validationMessage = "";
this.description_validationMessage = "";
this.createdDate_validationMessage = "";
  }
  clearDivisionFormFields(){
     this.division=this._divisionService.getter();   
  }
  validationMessages(result){  
     
this.country_validationMessage = result.country_validationMessage
this.description_validationMessage = result.description_validationMessage
this.createdDate_validationMessage = result.createdDate_validationMessage
  }
  processDivisionForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.divisionValidationInitializer();
    if(this.division.id==undefined){
       this._divisionService.createDivision(this.division).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/adminPanel/divisions']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._divisionService.updateDivision(this.division).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/adminPanel/divisions']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
