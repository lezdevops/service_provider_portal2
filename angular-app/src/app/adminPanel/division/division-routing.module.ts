import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListdivisionComponent } from './listdivision.component';
import { DivisionFormComponent } from './division-form.component';
import { DivisionFormViewComponent } from './division-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListdivisionComponent,
    data: {
      title: 'Division List'
    }
  },{
      path: 'divisionForm',
    component: DivisionFormComponent,
    data: {
      title: 'Division Form'
    }
  },{
      path: 'view',
    component: DivisionFormViewComponent,
    data: {
      title: 'Division Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DivisionRoutingModule {}
