import { Component, OnInit } from '@angular/core';
import { DivisionService } from '../../services/division.service';
import { Division }  from '../../model/division';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listdivision',
  templateUrl: './listdivision.component.html'
})
export class ListdivisionComponent implements OnInit {
  private divisions:Division[];
  
  constructor(private _divisionService: DivisionService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchDivision();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._divisionService.getDivisions().subscribe((result)=>{
      console.log(result);
      this.divisions=result;
    },(error)=>{
      codivisionnsole.log(error);
    })*/
    
  }
  paginatedDivisions(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._divisionService.getPaginatedDivisions(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.divisions=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchDivision(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._divisionService.getPaginatedDivisionsWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.divisions=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedDivisions(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedDivisions(this.pageNo,this.size);
      this.searchDivision();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedDivisions(this.pageNo,this.size);
    this.searchDivision();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedDivisions(pageNo,this.size);
    this.searchDivision();
  }
  deleteDivision(division){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._divisionService.deleteDivision(division.id).subscribe((data)=>{
        this.divisions.splice(this.divisions.indexOf(division),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewDivision(division){  
     this._divisionService.setter(division);
     this._router.navigate(['/adminPanel/divisions/view']);
   }
   editDivision(division){  
     this._divisionService.setter(division);
     this._router.navigate(['/adminPanel/divisions/divisionForm']);
   }
   newDivision(){
   let division = new Division();
    this._divisionService.setter(division);
     this._router.navigate(['/adminPanel/divisions/divisionForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.divisions, 'Division');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("Division List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Country", dataKey: "country"},{title: "Description", dataKey: "description"},{title: "Created Date", dataKey: "createdDate"},
    ];
    doc.autoTable(reportColumns, this.divisions, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('DivisionList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('DivisionList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
