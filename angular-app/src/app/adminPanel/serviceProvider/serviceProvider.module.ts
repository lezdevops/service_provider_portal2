// Angular
import { NgModule } from '@angular/core';
import { ServiceProviderRoutingModule } from './serviceProvider-routing.module';
import { ListserviceProviderComponent } from './listserviceProvider.component';
import { ServiceProviderFormComponent } from './serviceProvider-form.component';
import { ServiceProviderFormViewComponent } from './serviceProvider-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ServiceProviderService } from '../../services/serviceProvider.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { UserService }  from '../../services/user.service';import { SpLevelService }  from '../../services/spLevel.service';

@NgModule({
  imports: [
    ServiceProviderRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListserviceProviderComponent,
    ServiceProviderFormComponent,
    ServiceProviderFormViewComponent
  ],
  providers: [ServiceProviderService,
    UserService,SpLevelService,
    ExcelService  
  ],
})
export class ServiceProviderModule { }
