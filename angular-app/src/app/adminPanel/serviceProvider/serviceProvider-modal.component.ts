import { Component, OnInit, Input } from '@angular/core';
import { ServiceProvider }  from '../../model/serviceProvider';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

import { ModalModule } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-serviceProvider-modal',
  templateUrl: './serviceProvider-modal.component.html',
  styleUrls: ['./serviceProvider-modal.component.css']
})

export class ServiceProviderModalComponent {
@Input() serviceProvider: ServiceProvider;
  constructor(public activeModal: NgbActiveModal) { 
  }


}