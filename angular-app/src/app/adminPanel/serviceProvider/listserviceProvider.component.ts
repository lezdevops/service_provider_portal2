import { Component, OnInit } from '@angular/core';
import { ServiceProviderService } from '../../services/serviceProvider.service';
import { ServiceProvider }  from '../../model/serviceProvider';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listserviceProvider',
  templateUrl: './listserviceProvider.component.html'
})
export class ListserviceProviderComponent implements OnInit {
  private serviceProviders:ServiceProvider[];
  private common_validationMessage = '';
  private commonValidationHide = true;
  private pageName = "SERVICEPROVIDER";
  
  constructor(private _serviceProviderService: ServiceProviderService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     pageNo = 0;
     size = 5;
     searchToken = '';  
     totalPages = 0;
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.checkPermission();
          this.searchServiceProvider();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._serviceProviderService.getServiceProviders().subscribe((result)=>{
      console.log(result);
      this.serviceProviders=result;
    },(error)=>{
      coserviceProvidernsole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this.authService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error==undefined || result.error==""){
             this.commonValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.commonValidationHide = false;
            this.common_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedServiceProviders(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._serviceProviderService.getPaginatedServiceProviders(pageNo,size).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
        this.commonValidationHide = false;
        this.common_validationMessage = result.common_validationMessage;
      }else{
          this.commonValidationHide = true;
          this.serviceProviders=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchServiceProvider(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._serviceProviderService.getPaginatedServiceProvidersWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
          this.common_validationMessage = result.common_validationMessage;
      }else{        
          this.commonValidationHide = true;
          this.serviceProviders=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedServiceProviders(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedServiceProviders(this.pageNo,this.size);
      this.searchServiceProvider();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedServiceProviders(this.pageNo,this.size);
    this.searchServiceProvider();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedServiceProviders(pageNo,this.size);
    this.searchServiceProvider();
  }
  deleteServiceProvider(serviceProvider){
    if(confirm("Are you sure?")){
        this.spinnerService.show();
        setTimeout(()=>this.spinnerService.hide(),10000);
        this._serviceProviderService.deleteServiceProvider(serviceProvider.id).subscribe((result)=>{
            if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.common_validationMessage;
            }else if(result.technicalError!=undefined && result.technicalError!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.technicalError;
            }else {
                this.commonValidationHide = true;
                this.serviceProviders.splice(this.serviceProviders.indexOf(serviceProvider),1);    
            }
            this.spinnerService.hide();
        },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
        });
    }
    
  }

   viewServiceProvider(serviceProvider){  
      
    this._serviceProviderService.getServiceProvider(serviceProvider.id).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
              this.common_validationMessage = result.common_validationMessage;
          }else if(result.serviceProvider!=null || result.serviceProvider!=undefined){
              let serviceProvider = result.serviceProvider;
             let createdDate = serviceProvider.createdDate;
  						if(createdDate) {
  							serviceProvider.createdDate = new Date(createdDate);
  						}
             this._serviceProviderService.setter(serviceProvider);
              this._router.navigate(['/adminPanel/serviceProviders/view']);
          }
          this.spinnerService.hide();
      },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
      });
     
   }
   /*editServiceProvider(serviceProvider){  
     this._serviceProviderService.setter(serviceProvider);
     this._router.navigate(['/adminPanel/serviceProviders/serviceProviderForm']);
   }*/
  editServiceProvider(serviceProvider){  
     this.spinnerService.show();  
     this._serviceProviderService.getServiceProvider(serviceProvider.id).subscribe((result)=>{
         if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
             this.commonValidationHide = false;
             this.common_validationMessage = result.common_validationMessage;
         }else if(result.serviceProvider!=null || result.serviceProvider!=undefined){
             let serviceProvider = result.serviceProvider;
             let createdDate = serviceProvider.createdDate;
  						if(createdDate) {
  							serviceProvider.createdDate = new Date(createdDate);
  						}
             this._serviceProviderService.setter(serviceProvider);
             this._router.navigate(['/adminPanel/serviceProviders/serviceProviderForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     });
     //this._serviceProviderService.setter(serviceProvider);
     
   }
   newServiceProvider(){
     let serviceProvider = new ServiceProvider();
     this._serviceProviderService.setter(serviceProvider);
     this._router.navigate(['/adminPanel/serviceProviders/serviceProviderForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.serviceProviders, 'ServiceProvider');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("ServiceProvider List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Name", dataKey: "name"},{title: "About", dataKey: "about"},{title: "Address", dataKey: "address"},{title: "Zip Code", dataKey: "zipCode"},{title: "Phone No", dataKey: "phoneNo"},{title: "Nid No", dataKey: "nidNo"},{title: "Email", dataKey: "email"},{title: "User", dataKey: "user"},{title: "Sp Level", dataKey: "spLevel"},{title: "Gender", dataKey: "gender"},{title: "Desired Hourly Rate", dataKey: "desiredHourlyRate"},{title: "Desired Daily Rate", dataKey: "desiredDailyRate"},{title: "Service Area", dataKey: "serviceArea"},{title: "Availability", dataKey: "availability"},{title: "Is Active", dataKey: "isActive"},{title: "Start Time", dataKey: "startTime"},{title: "End Time", dataKey: "endTime"},{title: "Created Date", dataKey: "createdDate"},{title: "Profile Image", dataKey: "profileImage"},
    ];
    doc.autoTable(reportColumns, this.serviceProviders, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('ServiceProviderList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ServiceProviderList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
