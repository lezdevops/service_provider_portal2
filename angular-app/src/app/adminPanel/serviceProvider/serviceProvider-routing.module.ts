import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListserviceProviderComponent } from './listserviceProvider.component';
import { ServiceProviderFormComponent } from './serviceProvider-form.component';
import { ServiceProviderFormViewComponent } from './serviceProvider-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListserviceProviderComponent,
    data: {
      title: 'Service Provider List'
    }
  },{
      path: 'serviceProviderForm',
    component: ServiceProviderFormComponent,
    data: {
      title: 'Service Provider Form'
    }
  },{
      path: 'view',
    component: ServiceProviderFormViewComponent,
    data: {
      title: 'Service Provider Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceProviderRoutingModule {}
