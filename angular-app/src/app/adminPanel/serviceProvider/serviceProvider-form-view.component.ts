import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ServiceProvider }  from '../../model/serviceProvider';
import { Router ,ActivatedRoute} from "@angular/router";
import { ServiceProviderService } from '../../services/serviceProvider.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { User }  from '../../model/user';import { SpLevel }  from '../../model/spLevel';
import { UserService }  from '../../services/user.service';import { SpLevelService }  from '../../services/spLevel.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-serviceProvider-view-form',
  templateUrl: './serviceProvider-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ServiceProviderFormViewComponent implements OnInit {
  private serviceProvider:ServiceProvider;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private users:User[];private spLevels:SpLevel[];  
  
name_validationMessage = "";
about_validationMessage = "";
address_validationMessage = "";
zipCode_validationMessage = "";
phoneNo_validationMessage = "";
nidNo_validationMessage = "";
email_validationMessage = "";
user_validationMessage = "";
spLevel_validationMessage = "";
gender_validationMessage = "";
desiredHourlyRate_validationMessage = "";
desiredDailyRate_validationMessage = "";
serviceArea_validationMessage = "";
availability_validationMessage = "";
isActive_validationMessage = "";
startTime_validationMessage = "";
endTime_validationMessage = "";
createdDate_validationMessage = "";
profileImage_validationMessage = "";
    
  constructor(
    private _serviceProviderService:ServiceProviderService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _userService:UserService,private _spLevelService:SpLevelService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.serviceProvider=this._serviceProviderService.getter(); 
    this._userService.getUsers().subscribe((result)=>{
   			      this.users=result.users;
   			    },(error)=>{
   			      //console.log(error);
   			    });this._spLevelService.getSpLevels().subscribe((result)=>{
   			      this.spLevels=result.spLevels;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newServiceProvider(){
   let serviceProvider = new ServiceProvider();
    this._serviceProviderService.setter(serviceProvider);
     this._rotuer.navigate(['/adminPanel/serviceProviders/serviceProviderForm']);   
   }
  serviceProviderEditAction(){
     this._serviceProviderService.setter(this.serviceProvider);
     this._rotuer.navigate(['/adminPanel/serviceProviders/serviceProviderForm']);        
  }
  printServiceProviderDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printServiceProviderFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ServiceProviderList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
