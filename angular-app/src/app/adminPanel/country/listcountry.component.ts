import { Component, OnInit } from '@angular/core';
import { CountryService } from '../../services/country.service';
import { Country }  from '../../model/country';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listcountry',
  templateUrl: './listcountry.component.html'
})
export class ListcountryComponent implements OnInit {
  private countrys:Country[];
  
  constructor(private _countryService: CountryService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchCountry();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._countryService.getCountrys().subscribe((result)=>{
      console.log(result);
      this.countrys=result;
    },(error)=>{
      cocountrynsole.log(error);
    })*/
    
  }
  paginatedCountrys(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._countryService.getPaginatedCountrys(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.countrys=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchCountry(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._countryService.getPaginatedCountrysWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.countrys=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedCountrys(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedCountrys(this.pageNo,this.size);
      this.searchCountry();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedCountrys(this.pageNo,this.size);
    this.searchCountry();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedCountrys(pageNo,this.size);
    this.searchCountry();
  }
  deleteCountry(country){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._countryService.deleteCountry(country.id).subscribe((data)=>{
        this.countrys.splice(this.countrys.indexOf(country),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewCountry(country){  
     this._countryService.setter(country);
     this._router.navigate(['/adminPanel/countrys/view']);
   }
   editCountry(country){  
     this._countryService.setter(country);
     this._router.navigate(['/adminPanel/countrys/countryForm']);
   }
   newCountry(){
   let country = new Country();
    this._countryService.setter(country);
     this._router.navigate(['/adminPanel/countrys/countryForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.countrys, 'Country');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("Country List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Currency", dataKey: "currency"},{title: "Description", dataKey: "description"},{title: "Created Date", dataKey: "createdDate"},
    ];
    doc.autoTable(reportColumns, this.countrys, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('CountryList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('CountryList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
