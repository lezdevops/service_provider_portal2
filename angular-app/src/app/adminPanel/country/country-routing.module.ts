import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListcountryComponent } from './listcountry.component';
import { CountryFormComponent } from './country-form.component';
import { CountryFormViewComponent } from './country-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListcountryComponent,
    data: {
      title: 'Country List'
    }
  },{
      path: 'countryForm',
    component: CountryFormComponent,
    data: {
      title: 'Country Form'
    }
  },{
      path: 'view',
    component: CountryFormViewComponent,
    data: {
      title: 'Country Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryRoutingModule {}
