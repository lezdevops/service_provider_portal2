export const navItems = [
  {
    name: 'Dashboard',
    url: '/adminPanel',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: '15'
    }
  },
  {
    divider: true 
  },
  {
    title: true,
    name: 'Generated Components',
  }
,{name: 'Training Type',url: '/adminPanel/trainingTypes',icon: 'icon-pie-chart'}
,{name: 'Service Type',url: '/adminPanel/serviceTypes',icon: 'icon-pie-chart'}
,{name: 'Sp Level',url: '/adminPanel/spLevels',icon: 'icon-pie-chart'}
,{name: 'Service Provider',url: '/adminPanel/serviceProviders',icon: 'icon-pie-chart'}
,{name: 'Location',url: '/adminPanel/locations',icon: 'icon-pie-chart'}
,{name: 'Thana',url: '/adminPanel/thanas',icon: 'icon-pie-chart'}
,{name: 'District',url: '/adminPanel/districts',icon: 'icon-pie-chart'}
,{name: 'Division',url: '/adminPanel/divisions',icon: 'icon-pie-chart'}
,{name: 'Country',url: '/adminPanel/countrys',icon: 'icon-pie-chart'}
,{name: 'Customer',url: '/adminPanel/customers',icon: 'icon-pie-chart'}
// newMenuLinks
];
