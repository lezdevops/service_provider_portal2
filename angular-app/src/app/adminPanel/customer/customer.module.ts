// Angular
import { NgModule } from '@angular/core';
import { CustomerRoutingModule } from './customer-routing.module';
import { ListcustomerComponent } from './listcustomer.component';
import { CustomerFormComponent } from './customer-form.component';
import { CustomerFormViewComponent } from './customer-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CustomerService } from '../../services/customer.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { UserService }  from '../../services/user.service';

@NgModule({
  imports: [
    CustomerRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListcustomerComponent,
    CustomerFormComponent,
    CustomerFormViewComponent
  ],
  providers: [CustomerService,
    UserService,
    ExcelService  
  ],
})
export class CustomerModule { }
