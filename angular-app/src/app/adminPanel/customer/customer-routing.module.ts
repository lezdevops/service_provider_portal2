import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListcustomerComponent } from './listcustomer.component';
import { CustomerFormComponent } from './customer-form.component';
import { CustomerFormViewComponent } from './customer-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListcustomerComponent,
    data: {
      title: 'Customer List'
    }
  },{
      path: 'customerForm',
    component: CustomerFormComponent,
    data: {
      title: 'Customer Form'
    }
  },{
      path: 'view',
    component: CustomerFormViewComponent,
    data: {
      title: 'Customer Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule {}
