import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Customer }  from '../../model/customer';
import { Router ,ActivatedRoute } from "@angular/router";
import { CustomerService } from '../../services/customer.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { User }  from '../../model/user';
import { UserService }  from '../../services/user.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class CustomerFormComponent implements OnInit {
  private customer:Customer;
  profileImageHide = true;
    
  hideElement = true;
  permissionValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private users:User[];  
  
name_validationMessage = "";
securityNumber_validationMessage = "";
profileImage_validationMessage = "";
email_validationMessage = "";
createdDate_validationMessage = "";
User_validationMessage = "";
isActive_validationMessage = "";
  permission_validationMessage = '';
    
  constructor(
    private _customerService:CustomerService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _userService:UserService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.customer=this._customerService.getter();
    this.customerValidationInitializer();  
    this._userService.getUsers().subscribe((users)=>{
   			      this.users=users;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  customerValidationInitializer(){
     
this.name_validationMessage = "";
this.securityNumber_validationMessage = "";
this.profileImage_validationMessage = "";
this.email_validationMessage = "";
this.createdDate_validationMessage = "";
this.User_validationMessage = "";
this.isActive_validationMessage = "";
  }
  clearCustomerFormFields(){
     this.customer=this._customerService.getNew();   
  }
  validationMessages(result){  
     
this.name_validationMessage = result.name_validationMessage
this.securityNumber_validationMessage = result.securityNumber_validationMessage
this.profileImage_validationMessage = result.profileImage_validationMessage
this.email_validationMessage = result.email_validationMessage
this.createdDate_validationMessage = result.createdDate_validationMessage
this.User_validationMessage = result.User_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
  }
  processCustomerForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.customerValidationInitializer();
    if(this.customer.id==undefined){
       this._customerService.createCustomer(this.customer).subscribe((result)=>{
           if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
               this.permissionValidationHide = false;
               this.hideElement = true;
               this.permission_validationMessage = result.permission_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.permissionValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/adminPanel/customers']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._customerService.updateCustomer(this.customer).subscribe((result)=>{
          if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
               this.permissionValidationHide = false;
               this.hideElement = true;
               this.permission_validationMessage = result.permission_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.permissionValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/adminPanel/customers']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    onProfileImageFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.profileImageHide = false;
                
                this.customer.profileImage = reader.result;
            };
        }
    }
    clearprofileImageFile() {
        this.customer.profileImage = '';
        this.profileImageHide = true;
    }

}
