import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { Customer }  from '../../model/customer';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listcustomer',
  templateUrl: './listcustomer.component.html'
})
export class ListcustomerComponent implements OnInit {
  private customers:Customer[];
  private permission_validationMessage = '';
  private permissionValidationHide = true;
  private pageName = "Customer";
  
  constructor(private _customerService: CustomerService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     pageNo = 0;
     size = 5;
     searchToken = '';  
     totalPages = 0;
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.checkPermission();
          this.searchCustomer();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._customerService.getCustomers().subscribe((result)=>{
      console.log(result);
      this.customers=result;
    },(error)=>{
      cocustomernsole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this._customerService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error==undefined || result.error==""){
             this.permissionValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.permissionValidationHide = false;
            this.permission_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedCustomers(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._customerService.getPaginatedCustomers(pageNo,size).subscribe((result)=>{
      if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
        this.permissionValidationHide = false;
        this.permission_validationMessage = result.permission_validationMessage;
      }else{
          this.permissionValidationHide = true;
          this.customers=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchCustomer(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._customerService.getPaginatedCustomersWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
          this.permissionValidationHide = false;
          this.permission_validationMessage = result.permission_validationMessage;
      }else{        
          this.permissionValidationHide = true;
          this.customers=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedCustomers(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedCustomers(this.pageNo,this.size);
      this.searchCustomer();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedCustomers(this.pageNo,this.size);
    this.searchCustomer();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedCustomers(pageNo,this.size);
    this.searchCustomer();
  }
  deleteCustomer(customer){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._customerService.deleteCustomer(customer.id).subscribe((result)=>{
        if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
            this.permissionValidationHide = false;
            this.permission_validationMessage = result.permission_validationMessage;
        }else {
            this.permissionValidationHide = true;
            this.customers.splice(this.customers.indexOf(customer),1);    
        }
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewCustomer(customer){  
     this._customerService.setter(customer);
     this._router.navigate(['/adminPanel/customers/view']);
   }
   /*editCustomer(customer){  
     this._customerService.setter(customer);
     this._router.navigate(['/adminPanel/customers/customerForm']);
   }*/
  editCustomer(customer){  
     this.spinnerService.show();  
     this._customerService.getCustomer(customer.id).subscribe((result)=>{
         if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
             this.permissionValidationHide = false;
             this.permission_validationMessage = result.permission_validationMessage;
         }else if(result.customer!=null || result.customer!=undefined){
             this._customerService.setter(result.customer);
             this._router.navigate(['/adminPanel/customers/customerForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     })
     //this._customerService.setter(customer);
     
   }
   newCustomer(){
     let customer = new Customer();
     this._customerService.setter(customer);
     this._router.navigate(['/adminPanel/customers/customerForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.customers, 'Customer');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("Customer List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Name", dataKey: "name"},{title: "Security Number", dataKey: "securityNumber"},{title: "Profile Image", dataKey: "profileImage"},{title: "Email", dataKey: "email"},{title: "Created Date", dataKey: "createdDate"},{title: "User", dataKey: "User"},{title: "Is Active", dataKey: "isActive"},
    ];
    doc.autoTable(reportColumns, this.customers, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('CustomerList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('CustomerList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
