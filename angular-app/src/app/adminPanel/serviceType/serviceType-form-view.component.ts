import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ServiceType }  from '../../model/serviceType';
import { Router ,ActivatedRoute} from "@angular/router";
import { ServiceTypeService } from '../../services/serviceType.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';




export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-serviceType-view-form',
  templateUrl: './serviceType-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ServiceTypeFormViewComponent implements OnInit {
  private serviceType:ServiceType;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
    
  
shortCode_validationMessage = "";
description_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _serviceTypeService:ServiceTypeService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.serviceType=this._serviceTypeService.getter(); 
      
  }
    
  newServiceType(){
   let serviceType = new ServiceType();
    this._serviceTypeService.setter(serviceType);
     this._rotuer.navigate(['/adminPanel/serviceTypes/serviceTypeForm']);   
   }
  serviceTypeEditAction(){
     this._serviceTypeService.setter(this.serviceType);
     this._rotuer.navigate(['/adminPanel/serviceTypes/serviceTypeForm']);        
  }
  printServiceTypeDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printServiceTypeFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ServiceTypeList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
