import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListserviceTypeComponent } from './listserviceType.component';
import { ServiceTypeFormComponent } from './serviceType-form.component';
import { ServiceTypeFormViewComponent } from './serviceType-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListserviceTypeComponent,
    data: {
      title: 'Service Type List'
    }
  },{
      path: 'serviceTypeForm',
    component: ServiceTypeFormComponent,
    data: {
      title: 'Service Type Form'
    }
  },{
      path: 'view',
    component: ServiceTypeFormViewComponent,
    data: {
      title: 'Service Type Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceTypeRoutingModule {}
