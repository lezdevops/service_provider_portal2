import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ServiceType }  from '../../model/serviceType';
import { Router ,ActivatedRoute } from "@angular/router";
import { ServiceTypeService } from '../../services/serviceType.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-serviceType-form',
  templateUrl: './serviceType-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ServiceTypeFormComponent implements OnInit {
  private serviceType:ServiceType;
  
    
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
    
  
shortCode_validationMessage = "";
description_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _serviceTypeService:ServiceTypeService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.serviceType=this._serviceTypeService.getter();
    this.serviceTypeValidationInitializer();  
      
  }
  serviceTypeValidationInitializer(){
     
this.shortCode_validationMessage = "";
this.description_validationMessage = "";
this.isActive_validationMessage = "";
  }
  clearServiceTypeFormFields(){
     this.serviceType=this._serviceTypeService.getter();   
  }
  validationMessages(result){  
     
this.shortCode_validationMessage = result.shortCode_validationMessage
this.description_validationMessage = result.description_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
  }
  processServiceTypeForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.serviceTypeValidationInitializer();
    if(this.serviceType.id==undefined){
       this._serviceTypeService.createServiceType(this.serviceType).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/adminPanel/serviceTypes']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._serviceTypeService.updateServiceType(this.serviceType).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/adminPanel/serviceTypes']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
