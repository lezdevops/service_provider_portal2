import { Component, OnInit } from '@angular/core';
import { ServiceTypeService } from '../../services/serviceType.service';
import { ServiceType }  from '../../model/serviceType';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listserviceType',
  templateUrl: './listserviceType.component.html'
})
export class ListserviceTypeComponent implements OnInit {
  private serviceTypes:ServiceType[];
  
  constructor(private _serviceTypeService: ServiceTypeService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchServiceType();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._serviceTypeService.getServiceTypes().subscribe((result)=>{
      console.log(result);
      this.serviceTypes=result;
    },(error)=>{
      coserviceTypensole.log(error);
    })*/
    
  }
  paginatedServiceTypes(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._serviceTypeService.getPaginatedServiceTypes(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.serviceTypes=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchServiceType(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._serviceTypeService.getPaginatedServiceTypesWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.serviceTypes=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedServiceTypes(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedServiceTypes(this.pageNo,this.size);
      this.searchServiceType();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedServiceTypes(this.pageNo,this.size);
    this.searchServiceType();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedServiceTypes(pageNo,this.size);
    this.searchServiceType();
  }
  deleteServiceType(serviceType){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._serviceTypeService.deleteServiceType(serviceType.id).subscribe((data)=>{
        this.serviceTypes.splice(this.serviceTypes.indexOf(serviceType),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewServiceType(serviceType){  
     this._serviceTypeService.setter(serviceType);
     this._router.navigate(['/adminPanel/serviceTypes/view']);
   }
   editServiceType(serviceType){  
     this._serviceTypeService.setter(serviceType);
     this._router.navigate(['/adminPanel/serviceTypes/serviceTypeForm']);
   }
   newServiceType(){
   let serviceType = new ServiceType();
    this._serviceTypeService.setter(serviceType);
     this._router.navigate(['/adminPanel/serviceTypes/serviceTypeForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.serviceTypes, 'ServiceType');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("ServiceType List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Short Code", dataKey: "shortCode"},{title: "Description", dataKey: "description"},{title: "Is Active", dataKey: "isActive"},
    ];
    doc.autoTable(reportColumns, this.serviceTypes, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('ServiceTypeList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ServiceTypeList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
