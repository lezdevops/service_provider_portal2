// Angular
import { NgModule } from '@angular/core';
import { ServiceTypeRoutingModule } from './serviceType-routing.module';
import { ListserviceTypeComponent } from './listserviceType.component';
import { ServiceTypeFormComponent } from './serviceType-form.component';
import { ServiceTypeFormViewComponent } from './serviceType-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ServiceTypeService } from '../../services/serviceType.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';



@NgModule({
  imports: [
    ServiceTypeRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListserviceTypeComponent,
    ServiceTypeFormComponent,
    ServiceTypeFormViewComponent
  ],
  providers: [ServiceTypeService,
    
    ExcelService  
  ],
})
export class ServiceTypeModule { }
