import { Component, OnInit } from '@angular/core';
import { TrainingTypeService } from '../../services/trainingType.service';
import { TrainingType }  from '../../model/trainingType';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listtrainingType',
  templateUrl: './listtrainingType.component.html'
})
export class ListtrainingTypeComponent implements OnInit {
  private trainingTypes:TrainingType[];
  
  constructor(private _trainingTypeService: TrainingTypeService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchTrainingType();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._trainingTypeService.getTrainingTypes().subscribe((result)=>{
      console.log(result);
      this.trainingTypes=result;
    },(error)=>{
      cotrainingTypensole.log(error);
    })*/
    
  }
  paginatedTrainingTypes(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._trainingTypeService.getPaginatedTrainingTypes(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.trainingTypes=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchTrainingType(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._trainingTypeService.getPaginatedTrainingTypesWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.trainingTypes=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedTrainingTypes(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedTrainingTypes(this.pageNo,this.size);
      this.searchTrainingType();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedTrainingTypes(this.pageNo,this.size);
    this.searchTrainingType();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedTrainingTypes(pageNo,this.size);
    this.searchTrainingType();
  }
  deleteTrainingType(trainingType){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._trainingTypeService.deleteTrainingType(trainingType.id).subscribe((data)=>{
        this.trainingTypes.splice(this.trainingTypes.indexOf(trainingType),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewTrainingType(trainingType){  
     this._trainingTypeService.setter(trainingType);
     this._router.navigate(['/adminPanel/trainingTypes/view']);
   }
   editTrainingType(trainingType){  
     this._trainingTypeService.setter(trainingType);
     this._router.navigate(['/adminPanel/trainingTypes/trainingTypeForm']);
   }
   newTrainingType(){
   let trainingType = new TrainingType();
    this._trainingTypeService.setter(trainingType);
     this._router.navigate(['/adminPanel/trainingTypes/trainingTypeForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.trainingTypes, 'TrainingType');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("TrainingType List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Short Code", dataKey: "shortCode"},{title: "Description", dataKey: "description"},{title: "Is Active", dataKey: "isActive"},
    ];
    doc.autoTable(reportColumns, this.trainingTypes, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('TrainingTypeList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('TrainingTypeList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
