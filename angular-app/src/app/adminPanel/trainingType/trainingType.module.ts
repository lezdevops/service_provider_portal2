// Angular
import { NgModule } from '@angular/core';
import { TrainingTypeRoutingModule } from './trainingType-routing.module';
import { ListtrainingTypeComponent } from './listtrainingType.component';
import { TrainingTypeFormComponent } from './trainingType-form.component';
import { TrainingTypeFormViewComponent } from './trainingType-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TrainingTypeService } from '../../services/trainingType.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';



@NgModule({
  imports: [
    TrainingTypeRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListtrainingTypeComponent,
    TrainingTypeFormComponent,
    TrainingTypeFormViewComponent
  ],
  providers: [TrainingTypeService,
    
    ExcelService  
  ],
})
export class TrainingTypeModule { }
