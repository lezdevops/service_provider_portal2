import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListtrainingTypeComponent } from './listtrainingType.component';
import { TrainingTypeFormComponent } from './trainingType-form.component';
import { TrainingTypeFormViewComponent } from './trainingType-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListtrainingTypeComponent,
    data: {
      title: 'Training Type List'
    }
  },{
      path: 'trainingTypeForm',
    component: TrainingTypeFormComponent,
    data: {
      title: 'Training Type Form'
    }
  },{
      path: 'view',
    component: TrainingTypeFormViewComponent,
    data: {
      title: 'Training Type Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingTypeRoutingModule {}
