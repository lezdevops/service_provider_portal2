import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TrainingType }  from '../../model/trainingType';
import { Router ,ActivatedRoute } from "@angular/router";
import { TrainingTypeService } from '../../services/trainingType.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-trainingType-form',
  templateUrl: './trainingType-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class TrainingTypeFormComponent implements OnInit {
  private trainingType:TrainingType;
  
    
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
    
  
shortCode_validationMessage = "";
description_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _trainingTypeService:TrainingTypeService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.trainingType=this._trainingTypeService.getter();
    this.trainingTypeValidationInitializer();  
      
  }
  trainingTypeValidationInitializer(){
     
this.shortCode_validationMessage = "";
this.description_validationMessage = "";
this.isActive_validationMessage = "";
  }
  clearTrainingTypeFormFields(){
     this.trainingType=this._trainingTypeService.getter();   
  }
  validationMessages(result){  
     
this.shortCode_validationMessage = result.shortCode_validationMessage
this.description_validationMessage = result.description_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
  }
  processTrainingTypeForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.trainingTypeValidationInitializer();
    if(this.trainingType.id==undefined){
       this._trainingTypeService.createTrainingType(this.trainingType).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/adminPanel/trainingTypes']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._trainingTypeService.updateTrainingType(this.trainingType).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/adminPanel/trainingTypes']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
