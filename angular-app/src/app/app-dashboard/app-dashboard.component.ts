import { Component,OnInit } from '@angular/core';
import { AuthenticationService } from "../authentication/authentication.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './app-dashboard.component.html',
  styleUrls: [ './app-dashboard.component.css' ]
})
export class AppDashboardComponent  implements OnInit{
  name = 'Angular';
  links :any[] = new Array;
  loggedIn:boolean;
  constructor(public authService: AuthenticationService) {

  }

  ngOnInit(){
    this.authService.isLoggedIn.subscribe((result) => {
        this.loggedIn = result;
        if(result){
          this.loadAppUrls();
        }
    }, (error) => {
        console.log(error);
    }); 
  }
  loadAppUrls(){
    let link = {"url":"#/sp-portal","icon":"fa fa-address-card-o","text":"SP Portal"};
    this.links.push(link);

    link = {"url":"#/adminPanel","icon":"fa fa-ambulance","text":"Admin Panel "};
    this.links.push(link);

    link = {"url":"#/globalSetup","icon":"fa fa-h-square","text":"Global Setup"};
    this.links.push(link);

    link = {"url":"#/serviceProviderPanel","icon":"fa fa-ambulance","text":"SP Panel"};
    this.links.push(link);

    link = {"url":"#/customerPanel","icon":"fa fa-heart","text":"Customer Panel"};
    this.links.push(link);

    link = {"url":"#/testModule","icon":"fa fa-heart","text":"Test Module"};
    this.links.push(link);

    link = {"url":"#/registration","icon":"fa fa-calculator","text":"Registration"};
    this.links.push(link);
  }
  onLogout(){
    this.authService.logout();
  }
}
