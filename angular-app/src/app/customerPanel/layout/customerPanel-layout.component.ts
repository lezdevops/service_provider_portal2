//import { navItems } from './adminPanel_nav';
import { navItems } from './customerPanel_nav';
import { Component, OnInit, NgZone } from '@angular/core';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './customerPanel-layout.component.html'
})
export class CustomerPanelLayoutComponent {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  isLoggedIn: Observable<boolean>;

  constructor(public authService: AuthenticationService) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
  }
  

  ngOnInit() {

    this.hasRole("SUPER_ADMIN");
  }
  hasRole(roleName){
    let authServiceObj = this.authService;
    authServiceObj.hasRole(roleName).subscribe((result) => {
        if(result==true){
          authServiceObj.isLoginUserModuleOwner("CUSTOMER").subscribe(result=>{
            if(!result){
              //this._router.navigate(['/']);  
            }
          });
        }else{
          //this._router.navigate(['/']);  
        }
    }, (error) => {
        console.log(error);
        //this._router.navigate(['/']);
    });    
  }

  onLogout(){
    this.authService.logout();
  }

}
