export const navItems = [
  {
    name: 'Dashboard',
    url: '/customerPanel',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: '15'
    }
  },
  {
    divider: true 
  },
  {
    title: true,
    name: 'Generated Components',
  }
,{name: 'Customer Sp',url: '/customerPanel/customerSps',icon: 'icon-pie-chart'}
,{name: 'Customer Job',url: '/customerPanel/customerJobs',icon: 'icon-pie-chart'}
,{name: 'Customer Request',url: '/customerPanel/customerRequests',icon: 'icon-pie-chart'}
// newMenuLinks
];
