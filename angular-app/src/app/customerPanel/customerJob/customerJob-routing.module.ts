import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListcustomerJobComponent } from './listcustomerJob.component';
import { CustomerJobFormComponent } from './customerJob-form.component';
import { CustomerJobFormViewComponent } from './customerJob-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListcustomerJobComponent,
    data: {
      title: 'Customer Job List'
    }
  },{
      path: 'customerJobForm',
    component: CustomerJobFormComponent,
    data: {
      title: 'Customer Job Form'
    }
  },{
      path: 'view',
    component: CustomerJobFormViewComponent,
    data: {
      title: 'Customer Job Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerJobRoutingModule {}
