import { Component, OnInit } from '@angular/core';
import { CustomerJobService } from '../../services/customerJob.service';
import { CustomerJob }  from '../../model/customerJob';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listcustomerJob',
  templateUrl: './listcustomerJob.component.html'
})
export class ListcustomerJobComponent implements OnInit {
  private customerJobs:CustomerJob[];
  private common_validationMessage = '';
  private commonValidationHide = true;
  private pageName = "CUSTOMERJOB";
  
  constructor(private _customerJobService: CustomerJobService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     pageNo = 0;
     size = 5;
     searchToken = '';  
     totalPages = 0;
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.checkPermission();
          this.searchCustomerJob();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._customerJobService.getCustomerJobs().subscribe((result)=>{
      console.log(result);
      this.customerJobs=result;
    },(error)=>{
      cocustomerJobnsole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this.authService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error==undefined || result.error==""){
             this.commonValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.commonValidationHide = false;
            this.common_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedCustomerJobs(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._customerJobService.getPaginatedCustomerJobs(pageNo,size).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
        this.commonValidationHide = false;
        this.common_validationMessage = result.common_validationMessage;
      }else{
          this.commonValidationHide = true;
          this.customerJobs=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchCustomerJob(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._customerJobService.getPaginatedCustomerJobsWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
          this.common_validationMessage = result.common_validationMessage;
      }else{        
          this.commonValidationHide = true;
          this.customerJobs=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedCustomerJobs(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedCustomerJobs(this.pageNo,this.size);
      this.searchCustomerJob();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedCustomerJobs(this.pageNo,this.size);
    this.searchCustomerJob();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedCustomerJobs(pageNo,this.size);
    this.searchCustomerJob();
  }
  deleteCustomerJob(customerJob){
    if(confirm("Are you sure?")){
        this.spinnerService.show();
        setTimeout(()=>this.spinnerService.hide(),10000);
        this._customerJobService.deleteCustomerJob(customerJob.id).subscribe((result)=>{
            if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.common_validationMessage;
            }else if(result.technicalError!=undefined && result.technicalError!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.technicalError;
            }else {
                this.commonValidationHide = true;
                this.customerJobs.splice(this.customerJobs.indexOf(customerJob),1);    
            }
            this.spinnerService.hide();
        },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
        });
    }
    
  }

   viewCustomerJob(customerJob){  
      
    this._customerJobService.getCustomerJob(customerJob.id).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
              this.common_validationMessage = result.common_validationMessage;
          }else if(result.customerJob!=null || result.customerJob!=undefined){
              let customerJob = result.customerJob;
             let createdDate = customerJob.createdDate;
  						if(createdDate) {
  							customerJob.createdDate = new Date(createdDate);
  						}
             this._customerJobService.setter(customerJob);
              this._router.navigate(['/customerPanel/customerJobs/view']);
          }
          this.spinnerService.hide();
      },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
      });
     
   }
   /*editCustomerJob(customerJob){  
     this._customerJobService.setter(customerJob);
     this._router.navigate(['/customerPanel/customerJobs/customerJobForm']);
   }*/
  editCustomerJob(customerJob){  
     this.spinnerService.show();  
     this._customerJobService.getCustomerJob(customerJob.id).subscribe((result)=>{
         if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
             this.commonValidationHide = false;
             this.common_validationMessage = result.common_validationMessage;
         }else if(result.customerJob!=null || result.customerJob!=undefined){
             let customerJob = result.customerJob;
             let createdDate = customerJob.createdDate;
  						if(createdDate) {
  							customerJob.createdDate = new Date(createdDate);
  						}
             this._customerJobService.setter(customerJob);
             this._router.navigate(['/customerPanel/customerJobs/customerJobForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     });
     //this._customerJobService.setter(customerJob);
     
   }
   newCustomerJob(){
     let customerJob = new CustomerJob();
     this._customerJobService.setter(customerJob);
     this._router.navigate(['/customerPanel/customerJobs/customerJobForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.customerJobs, 'CustomerJob');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("CustomerJob List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Customer", dataKey: "Customer"},{title: "Job Details", dataKey: "jobDetails"},{title: "Created Date", dataKey: "createdDate"},{title: "From Time", dataKey: "fromTime"},{title: "To Time", dataKey: "toTime"},{title: "Approval Status", dataKey: "approvalStatus"},
    ];
    doc.autoTable(reportColumns, this.customerJobs, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('CustomerJobList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('CustomerJobList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
