import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CustomerJob }  from '../../model/customerJob';
import { Router ,ActivatedRoute} from "@angular/router";
import { CustomerJobService } from '../../services/customerJob.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { Customer }  from '../../model/customer';
import { CustomerService }  from '../../services/customer.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-customerJob-view-form',
  templateUrl: './customerJob-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class CustomerJobFormViewComponent implements OnInit {
  private customerJob:CustomerJob;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private customers:Customer[];  
  
Customer_validationMessage = "";
jobDetails_validationMessage = "";
createdDate_validationMessage = "";
fromTime_validationMessage = "";
toTime_validationMessage = "";
approvalStatus_validationMessage = "";
    
  constructor(
    private _customerJobService:CustomerJobService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _customerService:CustomerService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.customerJob=this._customerJobService.getter(); 
    this._customerService.getCustomers().subscribe((result)=>{
   			      this.customers=result.customers;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newCustomerJob(){
   let customerJob = new CustomerJob();
    this._customerJobService.setter(customerJob);
     this._rotuer.navigate(['/customerPanel/customerJobs/customerJobForm']);   
   }
  customerJobEditAction(){
     this._customerJobService.setter(this.customerJob);
     this._rotuer.navigate(['/customerPanel/customerJobs/customerJobForm']);        
  }
  printCustomerJobDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printCustomerJobFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('CustomerJobList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
