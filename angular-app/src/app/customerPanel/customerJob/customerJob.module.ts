// Angular
import { NgModule } from '@angular/core';
import { CustomerJobRoutingModule } from './customerJob-routing.module';
import { ListcustomerJobComponent } from './listcustomerJob.component';
import { CustomerJobFormComponent } from './customerJob-form.component';
import { CustomerJobFormViewComponent } from './customerJob-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CustomerJobService } from '../../services/customerJob.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { CustomerService }  from '../../services/customer.service';

@NgModule({
  imports: [
    CustomerJobRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListcustomerJobComponent,
    CustomerJobFormComponent,
    CustomerJobFormViewComponent
  ],
  providers: [CustomerJobService,
    CustomerService,
    ExcelService  
  ],
})
export class CustomerJobModule { }
