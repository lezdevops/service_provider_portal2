import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CustomerJob }  from '../../model/customerJob';
import { Router ,ActivatedRoute } from "@angular/router";
import { CustomerJobService } from '../../services/customerJob.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Customer }  from '../../model/customer';
import { CustomerService }  from '../../services/customer.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-customerJob-form',
  templateUrl: './customerJob-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class CustomerJobFormComponent implements OnInit {
  private customerJob:CustomerJob;
  
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private customers:Customer[];  
  
Customer_validationMessage = "";
jobDetails_validationMessage = "";
createdDate_validationMessage = "";
fromTime_validationMessage = "";
toTime_validationMessage = "";
approvalStatus_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _customerJobService:CustomerJobService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _customerService:CustomerService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.customerJob=this._customerJobService.getter();
    this.customerJobValidationInitializer();  
    this._customerService.getCustomers().subscribe((result)=>{
   			      this.customers=result.customers;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  customerJobValidationInitializer(){
     
this.Customer_validationMessage = "";
this.jobDetails_validationMessage = "";
this.createdDate_validationMessage = "";
this.fromTime_validationMessage = "";
this.toTime_validationMessage = "";
this.approvalStatus_validationMessage = "";
  }
  clearCustomerJobFormFields(){
     this.customerJob=this._customerJobService.getNew();   
  }
  validationMessages(result){  
     
this.Customer_validationMessage = result.Customer_validationMessage
this.jobDetails_validationMessage = result.jobDetails_validationMessage
this.createdDate_validationMessage = result.createdDate_validationMessage
this.fromTime_validationMessage = result.fromTime_validationMessage
this.toTime_validationMessage = result.toTime_validationMessage
this.approvalStatus_validationMessage = result.approvalStatus_validationMessage
  }
  processCustomerJobForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.customerJobValidationInitializer();
    if(this.customerJob.id==undefined){
       this._customerJobService.createCustomerJob(this.customerJob).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/customerPanel/customerJobs']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._customerJobService.updateCustomerJob(this.customerJob).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/customerPanel/customerJobs']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
