import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CustomerSp }  from '../../model/customerSp';
import { Router ,ActivatedRoute } from "@angular/router";
import { CustomerSpService } from '../../services/customerSp.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Customer }  from '../../model/customer';import { ServiceProvider }  from '../../model/serviceProvider';
import { CustomerService }  from '../../services/customer.service';import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-customerSp-form',
  templateUrl: './customerSp-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class CustomerSpFormComponent implements OnInit {
  private customerSp:CustomerSp;
  
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private customers:Customer[];private serviceProviders:ServiceProvider[];  
  
Customer_validationMessage = "";
ServiceProvider_validationMessage = "";
isActive_validationMessage = "";
aproveStatus_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _customerSpService:CustomerSpService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _customerService:CustomerService,private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.customerSp=this._customerSpService.getter();
    this.customerSpValidationInitializer();  
    this._customerService.getCustomers().subscribe((result)=>{
   			      this.customers=result.customers;
   			    },(error)=>{
   			      //console.log(error);
   			    });this._serviceProviderService.getServiceProviders().subscribe((result)=>{
   			      this.serviceProviders=result.serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  customerSpValidationInitializer(){
     
this.Customer_validationMessage = "";
this.ServiceProvider_validationMessage = "";
this.isActive_validationMessage = "";
this.aproveStatus_validationMessage = "";
  }
  clearCustomerSpFormFields(){
     this.customerSp=this._customerSpService.getNew();   
  }
  validationMessages(result){  
     
this.Customer_validationMessage = result.Customer_validationMessage
this.ServiceProvider_validationMessage = result.ServiceProvider_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
this.aproveStatus_validationMessage = result.aproveStatus_validationMessage
  }
  processCustomerSpForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.customerSpValidationInitializer();
    if(this.customerSp.id==undefined){
       this._customerSpService.createCustomerSp(this.customerSp).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/customerPanel/customerSps']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._customerSpService.updateCustomerSp(this.customerSp).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/customerPanel/customerSps']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
