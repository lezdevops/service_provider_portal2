import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListcustomerSpComponent } from './listcustomerSp.component';
import { CustomerSpFormComponent } from './customerSp-form.component';
import { CustomerSpFormViewComponent } from './customerSp-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListcustomerSpComponent,
    data: {
      title: 'Customer Sp List'
    }
  },{
      path: 'customerSpForm',
    component: CustomerSpFormComponent,
    data: {
      title: 'Customer Sp Form'
    }
  },{
      path: 'view',
    component: CustomerSpFormViewComponent,
    data: {
      title: 'Customer Sp Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerSpRoutingModule {}
