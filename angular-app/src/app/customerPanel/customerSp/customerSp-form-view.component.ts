import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CustomerSp }  from '../../model/customerSp';
import { Router ,ActivatedRoute} from "@angular/router";
import { CustomerSpService } from '../../services/customerSp.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { Customer }  from '../../model/customer';import { ServiceProvider }  from '../../model/serviceProvider';
import { CustomerService }  from '../../services/customer.service';import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-customerSp-view-form',
  templateUrl: './customerSp-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class CustomerSpFormViewComponent implements OnInit {
  private customerSp:CustomerSp;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private customers:Customer[];private serviceProviders:ServiceProvider[];  
  
Customer_validationMessage = "";
ServiceProvider_validationMessage = "";
isActive_validationMessage = "";
aproveStatus_validationMessage = "";
    
  constructor(
    private _customerSpService:CustomerSpService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _customerService:CustomerService,private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.customerSp=this._customerSpService.getter(); 
    this._customerService.getCustomers().subscribe((result)=>{
   			      this.customers=result.customers;
   			    },(error)=>{
   			      //console.log(error);
   			    });this._serviceProviderService.getServiceProviders().subscribe((result)=>{
   			      this.serviceProviders=result.serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newCustomerSp(){
   let customerSp = new CustomerSp();
    this._customerSpService.setter(customerSp);
     this._rotuer.navigate(['/customerPanel/customerSps/customerSpForm']);   
   }
  customerSpEditAction(){
     this._customerSpService.setter(this.customerSp);
     this._rotuer.navigate(['/customerPanel/customerSps/customerSpForm']);        
  }
  printCustomerSpDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printCustomerSpFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('CustomerSpList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
