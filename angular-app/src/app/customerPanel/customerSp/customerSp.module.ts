// Angular
import { NgModule } from '@angular/core';
import { CustomerSpRoutingModule } from './customerSp-routing.module';
import { ListcustomerSpComponent } from './listcustomerSp.component';
import { CustomerSpFormComponent } from './customerSp-form.component';
import { CustomerSpFormViewComponent } from './customerSp-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CustomerSpService } from '../../services/customerSp.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { CustomerService }  from '../../services/customer.service';import { ServiceProviderService }  from '../../services/serviceProvider.service';

@NgModule({
  imports: [
    CustomerSpRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListcustomerSpComponent,
    CustomerSpFormComponent,
    CustomerSpFormViewComponent
  ],
  providers: [CustomerSpService,
    CustomerService,ServiceProviderService,
    ExcelService  
  ],
})
export class CustomerSpModule { }
