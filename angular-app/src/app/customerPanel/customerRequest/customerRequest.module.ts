// Angular
import { NgModule } from '@angular/core';
import { CustomerRequestRoutingModule } from './customerRequest-routing.module';
import { ListcustomerRequestComponent } from './listcustomerRequest.component';
import { CustomerRequestFormComponent } from './customerRequest-form.component';
import { CustomerRequestFormViewComponent } from './customerRequest-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CustomerRequestService } from '../../services/customerRequest.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { CustomerService }  from '../../services/customer.service';import { ServiceProviderService }  from '../../services/serviceProvider.service';

@NgModule({
  imports: [
    CustomerRequestRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListcustomerRequestComponent,
    CustomerRequestFormComponent,
    CustomerRequestFormViewComponent
  ],
  providers: [CustomerRequestService,
    CustomerService,ServiceProviderService,
    ExcelService  
  ],
})
export class CustomerRequestModule { }
