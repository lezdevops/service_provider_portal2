import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CustomerRequest }  from '../../model/customerRequest';
import { Router ,ActivatedRoute} from "@angular/router";
import { CustomerRequestService } from '../../services/customerRequest.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { Customer }  from '../../model/customer';import { ServiceProvider }  from '../../model/serviceProvider';
import { CustomerService }  from '../../services/customer.service';import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-customerRequest-view-form',
  templateUrl: './customerRequest-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class CustomerRequestFormViewComponent implements OnInit {
  private customerRequest:CustomerRequest;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private customers:Customer[];private serviceProviders:ServiceProvider[];  
  
Customer_validationMessage = "";
ServiceProvider_validationMessage = "";
fromTime_validationMessage = "";
toTime_validationMessage = "";
approvalStatus_validationMessage = "";
    
  constructor(
    private _customerRequestService:CustomerRequestService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _customerService:CustomerService,private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.customerRequest=this._customerRequestService.getter(); 
    this._customerService.getCustomers().subscribe((result)=>{
   			      this.customers=result.customers;
   			    },(error)=>{
   			      //console.log(error);
   			    });this._serviceProviderService.getServiceProviders().subscribe((result)=>{
   			      this.serviceProviders=result.serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newCustomerRequest(){
   let customerRequest = new CustomerRequest();
    this._customerRequestService.setter(customerRequest);
     this._rotuer.navigate(['/customerPanel/customerRequests/customerRequestForm']);   
   }
  customerRequestEditAction(){
     this._customerRequestService.setter(this.customerRequest);
     this._rotuer.navigate(['/customerPanel/customerRequests/customerRequestForm']);        
  }
  printCustomerRequestDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printCustomerRequestFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('CustomerRequestList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
