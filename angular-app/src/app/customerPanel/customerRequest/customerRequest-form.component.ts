import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CustomerRequest }  from '../../model/customerRequest';
import { Router ,ActivatedRoute } from "@angular/router";
import { CustomerRequestService } from '../../services/customerRequest.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Customer }  from '../../model/customer';import { ServiceProvider }  from '../../model/serviceProvider';
import { CustomerService }  from '../../services/customer.service';import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-customerRequest-form',
  templateUrl: './customerRequest-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class CustomerRequestFormComponent implements OnInit {
  private customerRequest:CustomerRequest;
  
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private customers:Customer[];private serviceProviders:ServiceProvider[];  
  
Customer_validationMessage = "";
ServiceProvider_validationMessage = "";
fromTime_validationMessage = "";
toTime_validationMessage = "";
approvalStatus_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _customerRequestService:CustomerRequestService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _customerService:CustomerService,private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.customerRequest=this._customerRequestService.getter();
    this.customerRequestValidationInitializer();  
    this._customerService.getCustomers().subscribe((result)=>{
   			      this.customers=result.customers;
   			    },(error)=>{
   			      //console.log(error);
   			    });this._serviceProviderService.getServiceProviders().subscribe((result)=>{
   			      this.serviceProviders=result.serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  customerRequestValidationInitializer(){
     
this.Customer_validationMessage = "";
this.ServiceProvider_validationMessage = "";
this.fromTime_validationMessage = "";
this.toTime_validationMessage = "";
this.approvalStatus_validationMessage = "";
  }
  clearCustomerRequestFormFields(){
     this.customerRequest=this._customerRequestService.getNew();   
  }
  validationMessages(result){  
     
this.Customer_validationMessage = result.Customer_validationMessage
this.ServiceProvider_validationMessage = result.ServiceProvider_validationMessage
this.fromTime_validationMessage = result.fromTime_validationMessage
this.toTime_validationMessage = result.toTime_validationMessage
this.approvalStatus_validationMessage = result.approvalStatus_validationMessage
  }
  processCustomerRequestForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.customerRequestValidationInitializer();
    if(this.customerRequest.id==undefined){
       this._customerRequestService.createCustomerRequest(this.customerRequest).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/customerPanel/customerRequests']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._customerRequestService.updateCustomerRequest(this.customerRequest).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/customerPanel/customerRequests']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
