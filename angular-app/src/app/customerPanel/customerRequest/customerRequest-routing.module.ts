import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListcustomerRequestComponent } from './listcustomerRequest.component';
import { CustomerRequestFormComponent } from './customerRequest-form.component';
import { CustomerRequestFormViewComponent } from './customerRequest-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListcustomerRequestComponent,
    data: {
      title: 'Customer Request List'
    }
  },{
      path: 'customerRequestForm',
    component: CustomerRequestFormComponent,
    data: {
      title: 'Customer Request Form'
    }
  },{
      path: 'view',
    component: CustomerRequestFormViewComponent,
    data: {
      title: 'Customer Request Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRequestRoutingModule {}
