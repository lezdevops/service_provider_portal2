import { Component, OnInit } from '@angular/core';
import { CustomerRequestService } from '../../services/customerRequest.service';
import { CustomerRequest }  from '../../model/customerRequest';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listcustomerRequest',
  templateUrl: './listcustomerRequest.component.html'
})
export class ListcustomerRequestComponent implements OnInit {
  private customerRequests:CustomerRequest[];
  private common_validationMessage = '';
  private commonValidationHide = true;
  private pageName = "CUSTOMERREQUEST";
  
  constructor(private _customerRequestService: CustomerRequestService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     pageNo = 0;
     size = 5;
     searchToken = '';  
     totalPages = 0;
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.checkPermission();
          this.searchCustomerRequest();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._customerRequestService.getCustomerRequests().subscribe((result)=>{
      console.log(result);
      this.customerRequests=result;
    },(error)=>{
      cocustomerRequestnsole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this.authService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error==undefined || result.error==""){
             this.commonValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.commonValidationHide = false;
            this.common_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedCustomerRequests(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._customerRequestService.getPaginatedCustomerRequests(pageNo,size).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
        this.commonValidationHide = false;
        this.common_validationMessage = result.common_validationMessage;
      }else{
          this.commonValidationHide = true;
          this.customerRequests=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchCustomerRequest(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._customerRequestService.getPaginatedCustomerRequestsWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
          this.common_validationMessage = result.common_validationMessage;
      }else{        
          this.commonValidationHide = true;
          this.customerRequests=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedCustomerRequests(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedCustomerRequests(this.pageNo,this.size);
      this.searchCustomerRequest();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedCustomerRequests(this.pageNo,this.size);
    this.searchCustomerRequest();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedCustomerRequests(pageNo,this.size);
    this.searchCustomerRequest();
  }
  deleteCustomerRequest(customerRequest){
    if(confirm("Are you sure?")){
        this.spinnerService.show();
        setTimeout(()=>this.spinnerService.hide(),10000);
        this._customerRequestService.deleteCustomerRequest(customerRequest.id).subscribe((result)=>{
            if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.common_validationMessage;
            }else if(result.technicalError!=undefined && result.technicalError!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.technicalError;
            }else {
                this.commonValidationHide = true;
                this.customerRequests.splice(this.customerRequests.indexOf(customerRequest),1);    
            }
            this.spinnerService.hide();
        },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
        });
    }
    
  }

   viewCustomerRequest(customerRequest){  
      
    this._customerRequestService.getCustomerRequest(customerRequest.id).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
              this.common_validationMessage = result.common_validationMessage;
          }else if(result.customerRequest!=null || result.customerRequest!=undefined){
              let customerRequest = result.customerRequest;
             
             this._customerRequestService.setter(customerRequest);
              this._router.navigate(['/customerPanel/customerRequests/view']);
          }
          this.spinnerService.hide();
      },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
      });
     
   }
   /*editCustomerRequest(customerRequest){  
     this._customerRequestService.setter(customerRequest);
     this._router.navigate(['/customerPanel/customerRequests/customerRequestForm']);
   }*/
  editCustomerRequest(customerRequest){  
     this.spinnerService.show();  
     this._customerRequestService.getCustomerRequest(customerRequest.id).subscribe((result)=>{
         if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
             this.commonValidationHide = false;
             this.common_validationMessage = result.common_validationMessage;
         }else if(result.customerRequest!=null || result.customerRequest!=undefined){
             let customerRequest = result.customerRequest;
             
             this._customerRequestService.setter(customerRequest);
             this._router.navigate(['/customerPanel/customerRequests/customerRequestForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     });
     //this._customerRequestService.setter(customerRequest);
     
   }
   newCustomerRequest(){
     let customerRequest = new CustomerRequest();
     this._customerRequestService.setter(customerRequest);
     this._router.navigate(['/customerPanel/customerRequests/customerRequestForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.customerRequests, 'CustomerRequest');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("CustomerRequest List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Customer", dataKey: "Customer"},{title: "Service Provider", dataKey: "ServiceProvider"},{title: "From Time", dataKey: "fromTime"},{title: "To Time", dataKey: "toTime"},{title: "Approval Status", dataKey: "approvalStatus"},
    ];
    doc.autoTable(reportColumns, this.customerRequests, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('CustomerRequestList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('CustomerRequestList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
