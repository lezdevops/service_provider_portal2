import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 

import { RouterModule, Routes } from '@angular/router';
import { HotTableModule } from '@handsontable/angular';
import { CommonModule } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatTableModule, MatSortModule, MatDialogModule } from '@angular/material';


import { AuthGuard } from './authentication/auth.guard';
import { AuthenticationService } from './authentication/authentication.service';
import { FrontPageService } from './services/frontPage.service';

import { DynamicScriptLoaderService } from './shared/dynamic-script-loader.component';

import { AlertComponent } from './views/alert/alert.component';

import { AlertService } from './views/alert/alert.service';
import { PaginationComponent } from './views/pagination/pagination.component';

import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

// Import containers
import { HomeResponsiveComponent } from './public';

import { AdminPanelLayoutComponent } from './adminPanel';

import { ServiceProviderPanelLayoutComponent } from './serviceProviderPanel';

import { GlobalSetupLayoutComponent } from './globalSetup';

import { TestModuleLayoutComponent } from './testModule';

import { CustomerPanelLayoutComponent } from './customerPanel';
// importNewAngularAppComponent
 
 
//import { FrontPageDashboardComponent } from './home/frontPage/dashboard.component';
//import { HomeComponent } from './public/home/home.component';
//import { RegistrationComponent } from './public/registration/registration.component';

import { AppDashboardComponent } from './app-dashboard/app-dashboard.component';
import { P124Component } from './views/error/124.component';
import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './authentication/login.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ModalModule } from 'ngx-bootstrap/modal';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


const APP_CONTAINERS = [

AdminPanelLayoutComponent,

ServiceProviderPanelLayoutComponent,

GlobalSetupLayoutComponent,

TestModuleLayoutComponent,

CustomerPanelLayoutComponent,
// declareNewAngularAppComponentContainer
HomeResponsiveComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';


@NgModule({
  imports: [
    BrowserModule, HttpModule, FormsModule, BrowserAnimationsModule,
    ReactiveFormsModule,  HotTableModule.forRoot(),
    AppRoutingModule,
    AppAsideModule,
    NgbModule,
    ModalModule,
    Ng4LoadingSpinnerModule.forRoot(),
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    AngularMultiSelectModule,
    TabsModule.forRoot(),
    ChartsModule , HotTableModule.forRoot(),
    BsDatepickerModule.forRoot(),
    HttpClientModule, HotTableModule, CommonModule, MatTableModule, MatSortModule, MatDialogModule,
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  declarations: [ 
    AppComponent,
    ...APP_CONTAINERS,
    AppDashboardComponent,
    P124Component,
    P404Component,
    P500Component,
    LoginComponent,
    AlertComponent,PaginationComponent,
    AppComponent
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  },AuthGuard, AuthenticationService,AlertService,DynamicScriptLoaderService,FrontPageService],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
