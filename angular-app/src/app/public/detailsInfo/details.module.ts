// Angular
import { NgModule } from '@angular/core';
import { DetailsRoutingModule } from './detailsInfo-routing.module';
import { DetailsComponent } from './details.component';

import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FrontPageService } from '../../services/frontPage.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    DetailsRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule, 
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  entryComponents: [],
  declarations: [
    DetailsComponent
  ],
  providers: [FrontPageService,
    ExcelService  
  ],
})
export class DetailsModule { }
