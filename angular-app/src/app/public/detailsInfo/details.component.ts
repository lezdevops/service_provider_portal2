import { Component, OnInit } from '@angular/core';
import { FrontPageService } from '../../services/frontPage.service';
 
import { ServiceProvider }  from '../../model/serviceProvider';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: [ './details.component.scss' ]
})
export class DetailsComponent implements OnInit {
  private serviceProviders:ServiceProvider[];
  private isLoggedIn = false; 
  
  constructor(private _frontPageService: FrontPageService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService,
     private modalService: NgbModal
    ) { }
  pageNo = 0;
  size = 12;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
             
          this.isLoggedIn = true;       
      } else {
        //this._router.navigate(['/login']);
          this.isLoggedIn = false;  
      }
    },(error)=>{
      console.log(error);
        //this._router.navigate(['/124']);
        this.isLoggedIn = false; 
    });
      this.loadServiceProviders();
    /*this._frontPageService.getServiceProviders().subscribe((result)=>{
      console.log(result);
      this.serviceProviders=result;
    },(error)=>{
      coserviceProvidernsole.log(error);
    })*/
    
  }

  paginatedServiceProviders(pageNo,size){
    this.spinnerService.show();
    this._frontPageService.getPaginatedServiceProviders(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.serviceProviders=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
       // this._router.navigate(['/124']);
    })
  }
  loadServiceProviders(){
    this.spinnerService.show();
    if(this.searchToken!=""){
      this._frontPageService.getPaginatedServiceProvidersWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.serviceProviders=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
       // this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedServiceProviders(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedServiceProviders(this.pageNo,this.size);
      this.loadServiceProviders();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedServiceProviders(this.pageNo,this.size);
    this.loadServiceProviders();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedServiceProviders(pageNo,this.size);
    this.loadServiceProviders();
  }
   logout() {
        this.authService.logout();
    }

}
