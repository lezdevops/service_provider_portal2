import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { RegistrationQueue }  from '../../model/registrationQueue';
import { Router ,ActivatedRoute } from "@angular/router";
import { FrontPageService } from '../../services/frontPage.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-registrationQueue-form',
  templateUrl: './registration.queue.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class RegistrationComponent implements OnInit {
  private registrationQueue:RegistrationQueue;
    
  private startTabActive;
  private emailVerificationTabActive;
  private finalStageTabActive;
  private verificationCode;

  hideElement = true;
  permissionValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
    
  
  email_validationMessage = "";
  userName_validationMessage = "";
  password_validationMessage = "";
  securityNumber_validationMessage = "";
  securityQuestion_validationMessage = "";
  answer_validationMessage = "";
  accountType_validationMessage = "";
  jsonString_validationMessage = "";
  createdDate_validationMessage = "";
  isApproved_validationMessage = "";
  permission_validationMessage = '';
    
  constructor(
    private _frontPageService:FrontPageService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        //this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.registrationQueue=this._frontPageService.getter();
    this.registrationQueueValidationInitializer();  
    this.showStartTab();
  }

  showStartTab(){
   this.startTabActive = true;
   this.emailVerificationTabActive = false;
   this.finalStageTabActive = false;
  }
 showEmailVerificationTab(){
   this.startTabActive = false;
   this.emailVerificationTabActive = true;
   this.finalStageTabActive = false;
  }
 showFinalStageTab(){
   this.startTabActive = false;
   this.emailVerificationTabActive = false;
   this.finalStageTabActive = true;
  }
 registrationQueueValidationInitializer() {

     this.email_validationMessage = "";
     this.userName_validationMessage = "";
     this.password_validationMessage = "";
     this.securityNumber_validationMessage = "";
     this.securityQuestion_validationMessage = "";
     this.answer_validationMessage = "";
     this.accountType_validationMessage = "";
     this.jsonString_validationMessage = "";
     this.createdDate_validationMessage = "";
     this.isApproved_validationMessage = "";
 }
  clearRegistrationQueueFormFields(){
     this.registrationQueue=this._frontPageService.getNew();   
  }
  validationMessages(result) {

      this.email_validationMessage = result.email_validationMessage
      this.userName_validationMessage = result.userName_validationMessage
      this.password_validationMessage = result.password_validationMessage
      this.securityNumber_validationMessage = result.securityNumber_validationMessage
      this.securityQuestion_validationMessage = result.securityQuestion_validationMessage
      this.answer_validationMessage = result.answer_validationMessage
      this.accountType_validationMessage = result.accountType_validationMessage
      this.jsonString_validationMessage = result.jsonString_validationMessage
      this.createdDate_validationMessage = result.createdDate_validationMessage
      this.isApproved_validationMessage = result.isApproved_validationMessage
  }
  processRegistrationQueueForm() {
      this.spinnerService.show();
      setTimeout(() => this.spinnerService.hide(), 10000);
      this.registrationQueueValidationInitializer();
      this._frontPageService.createRegistrationQueue(this.registrationQueue).subscribe((result) => {
          //console.log(this.registrationQueue);
          if (result.permission_validationMessage != undefined && result.permission_validationMessage != "") {
              this.permissionValidationHide = false;
              this.hideElement = true;
              this.permission_validationMessage = result.permission_validationMessage;
              this.spinnerService.hide();
          } else if (result.error != undefined && result.error != "") {
              this.permissionValidationHide = true;
              this.hideElement = false;
              this.validationMessages(result);
              this.spinnerService.hide();
          } else {
              this.registrationQueue = result.registrationQueue;
              this.showEmailVerificationTab();
              this.spinnerService.hide();
          }
          
      }, (error) => {
          console.log(error);
          this._rotuer.navigate(['/124']);
      });
  }
    
  verifyEmail() {
      this.spinnerService.show();
      setTimeout(() => this.spinnerService.hide(), 10000);
      this.registrationQueueValidationInitializer();
      this.registrationQueue.jsonString = this.verificationCode;
      
      this._frontPageService.verifyEmail(this.registrationQueue).subscribe((result) => {
          //console.log(this.registrationQueue);
          if (result.permission_validationMessage != undefined && result.permission_validationMessage != "") {
              this.permissionValidationHide = false;
              this.hideElement = true;
              this.permission_validationMessage = result.permission_validationMessage;
              this.spinnerService.hide();
          } else if (result.error != undefined && result.error != "") {
              this.permissionValidationHide = true;
              this.hideElement = false;
              this.validationMessages(result);
              this.spinnerService.hide();
          } else {
              this.registrationQueue = result.registrationQueue;
              this.showFinalStageTab();
              this.spinnerService.hide();
          }
          
      }, (error) => {
          console.log(error);
          this._rotuer.navigate(['/124']);
      }); 
        
    }
        
      finalStage(){
          this.showFinalStageTab();
          this.spinnerService.show();
          setTimeout(() => this.spinnerService.hide(), 10000);
          this.registrationQueueValidationInitializer();

          this._frontPageService.finalStage(this.registrationQueue).subscribe((result) => {
              //console.log(this.registrationQueue);
              if (result.permission_validationMessage != undefined && result.permission_validationMessage != "") {
                  this.permissionValidationHide = false;
                  this.hideElement = true;
                  this.permission_validationMessage = result.permission_validationMessage;
                  this.spinnerService.hide();
              } else if (result.error != undefined && result.error != "") {
                  this.permissionValidationHide = true;
                  this.hideElement = false;
                  this.validationMessages(result);
                  this.spinnerService.hide();
              } else {
                  //this.registrationQueue = result.registrationQueue;
                  this.showEmailVerificationTab();
                  this.spinnerService.hide();
              }
              
          }, (error) => {
              console.log(error);
              this._rotuer.navigate(['/124']);
          });

      }
    

}
