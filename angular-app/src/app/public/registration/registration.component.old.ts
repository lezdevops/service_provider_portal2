import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ServiceProviderService } from '../../services/serviceProvider.service';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AlertService } from '../../views/alert/alert.service';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/map";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { User }  from '../../model/user';
import { ServiceProvider }  from '../../model/serviceProvider';
import { CommonService } from '../../services/common.service';
import { UserGroup }  from '../../model/userGroup';
import { UserGroupService } from '../../services/userGroup.service';
import { SpLevel }  from '../../model/spLevel'; 
import { SpLevelService }  from '../../services/spLevel.service'; 
import { UserService }  from '../../services/user.service';
//import { BaseService } from '../base/base.service';

 
@Component({
    selector: 'app-dashboard',
    templateUrl: 'membership.registration.component.html'
})
export class RegistrationComponent extends CommonService implements OnInit {
    private serviceProvider: ServiceProvider;
    private user = new User();
    private userGroups: UserGroup[];
    hideElement = true;
    registrationForm: any;
    loading = false;
    error = '';

    models: string; //shops
    url = "";
    object: any;
    private headers = new Headers({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'userid': '1'
    });
    constructor(public _http: Http, 
        public alertService: AlertService, 
        public router: Router, 
        private authService: AuthenticationService,
        private route: ActivatedRoute,
        private fb: FormBuilder) {
        super();
        this.models = "login";
        this.url = this.baseUrl + this.models;
        
        this.serviceProvider = new ServiceProvider();
        //  this.authService.logout();
        /*this._userGroupService.getUserGroups().subscribe((userGroups) => {
            this.userGroups = userGroups;
        }, (error) => {
                    //console.log(error);
        });*/
        this.createForm();
        this.authService.isLoggedIn.subscribe((res) => {
            if (res) {
                this.router.navigate(['/']);
            } else {
            }
        })

    }
    ngOnInit(): void {
    }


    registration() {
        let formValue = this.registrationForm.value;
        //console.log(formValue);

        let objArr = [];
        let obj = this.registrationForm.value;
        console.log(JSON.stringify(obj));
        objArr.push(obj);
        this.alertService.success("Registered Successfully");
        this.router.navigate(["/registration"]);
    }

    create(formVal) {
        let objArr = [];
        let obj = formVal.value;
        console.log(JSON.stringify(obj));
        objArr.push(obj);
        this.postAll(objArr).subscribe(suc => {
            this.alertService.success(" is inserted successfully");
            this.router.navigate(["/registration"]);
        });
    }
    createUser() {
        this.headers.set('Authorization', this.authService.getToken());
        let options = new RequestOptions({ headers: this.headers });
        var jsonString = JSON.stringify(this.user);
        console.log(this.baseUrl + '/user');
        console.log(jsonString);

        return this._http.post(this.baseUrl + '/user', jsonString, options).map((response: Response) => response.json())
            .catch(this.errorHandler);
    }

    postAll(model): Observable<any> {
        this.headers.set('Authorization', this.authService.getToken());
        let options = new RequestOptions({ headers: this.headers });
        return this._http.post(this.url, JSON.stringify(model), options)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    handleError(error: Response) {
        return Observable.throw(error.json().error);
    }
    errorHandler(error: Response) {
        return Observable.throw(error || "SERVER ERROR");
    }
    createForm() {
        this.registrationForm = this.fb.group({
            memberType: null,
            fullName: null,
            userName: null,
            email: null,
            password: null,
            confirmPassword: null
        });
    }


}
