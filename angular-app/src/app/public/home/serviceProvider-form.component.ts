import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ServiceProvider }  from '../../model/serviceProvider';
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceProviderService } from '../../services/serviceProvider.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { SpLevel }  from '../../model/spLevel'; 
import { User }  from '../../model/user';
import { SpLevelService }  from '../../services/spLevel.service'; 
import { UserService }  from '../../services/user.service';
import { CommonService } from '../../services/common.service';

export function getAlertConfig(): AlertConfig {
    return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
    selector: 'app-serviceProvider-form',
    templateUrl: './serviceProvider-tabbedForm.component.html',
    encapsulation: ViewEncapsulation.None,
    styles: [
        `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
    ],
    providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ServiceProviderFormComponent implements OnInit {
    private serviceProvider: ServiceProvider;
    hideElement = true;

    simpleItems = [true, 'Two', 3];
    selectedSimpleItem = '';

    //private dependentEntitys:DependentEntity[];
    private spLevels: SpLevel[]; private users: User[];

    name_validationMessage = "";
    about_validationMessage = "";
    address_validationMessage = "";
    zipCode_validationMessage = "";
    phoneNo_validationMessage = "";
    nidNo_validationMessage = "";
    email_validationMessage = "";
    spLevel_validationMessage = "";
    gender_validationMessage = "";
    desiredHourlyRate_validationMessage = "";
    desiredDailyRate_validationMessage = "";
    serviceArea_validationMessage = "";
    availability_validationMessage = "";
    isActive_validationMessage = "";
    startTime_validationMessage = "";
    endTime_validationMessage = "";
    user_validationMessage = "";
    createdDate_validationMessage = "";

    constructor(
        private _serviceProviderService: ServiceProviderService,
        private _rotuer: Router,
        private authService: AuthenticationService,
        private _spLevelService: SpLevelService, private _userService: UserService,
        sanitizer: DomSanitizer,
        private route: ActivatedRoute,
        private spinnerService: Ng4LoadingSpinnerService) {
    }


    ngOnInit() {

        /*this.authService.isLoggedIn.subscribe((res) => {
            if (res) {

            } else {
                this._rotuer.navigate(['/login']);
            }
        }, (error) => {
            console.log(error);
            this._rotuer.navigate(['/124']);
        });*/
        this.serviceProvider = this._serviceProviderService.getter();
        this.serviceProviderValidationInitializer();
        this._spLevelService.getSpLevels().subscribe((spLevels) => {
   			      this.spLevels = spLevels;
        }, (error) => {
   			      //console.log(error);
        }); this._userService.getUsers().subscribe((users) => {
   			      this.users = users;
        }, (error) => {
   			      console.log(error);
        });
    }
    serviceProviderValidationInitializer() {

        this.name_validationMessage = "";
        this.about_validationMessage = "";
        this.address_validationMessage = "";
        this.zipCode_validationMessage = "";
        this.phoneNo_validationMessage = "";
        this.nidNo_validationMessage = "";
        this.email_validationMessage = "";
        this.spLevel_validationMessage = "";
        this.gender_validationMessage = "";
        this.desiredHourlyRate_validationMessage = "";
        this.desiredDailyRate_validationMessage = "";
        this.serviceArea_validationMessage = "";
        this.availability_validationMessage = "";
        this.isActive_validationMessage = "";
        this.startTime_validationMessage = "";
        this.endTime_validationMessage = "";
        this.user_validationMessage = "";
        this.createdDate_validationMessage = "";
    }
    clearServiceProviderFormFields() {
        this.serviceProvider = this._serviceProviderService.getter();
    }
    validationMessages(result) {

        this.name_validationMessage = result.name_validationMessage
        this.about_validationMessage = result.about_validationMessage
        this.address_validationMessage = result.address_validationMessage
        this.zipCode_validationMessage = result.zipCode_validationMessage
        this.phoneNo_validationMessage = result.phoneNo_validationMessage
        this.nidNo_validationMessage = result.nidNo_validationMessage
        this.email_validationMessage = result.email_validationMessage
        this.spLevel_validationMessage = result.spLevel_validationMessage
        this.gender_validationMessage = result.gender_validationMessage
        this.desiredHourlyRate_validationMessage = result.desiredHourlyRate_validationMessage
        this.desiredDailyRate_validationMessage = result.desiredDailyRate_validationMessage
        this.serviceArea_validationMessage = result.serviceArea_validationMessage
        this.availability_validationMessage = result.availability_validationMessage
        this.isActive_validationMessage = result.isActive_validationMessage
        this.startTime_validationMessage = result.startTime_validationMessage
        this.endTime_validationMessage = result.endTime_validationMessage
        this.user_validationMessage = result.user_validationMessage
        this.createdDate_validationMessage = result.createdDate_validationMessage
    }
    processServiceProviderForm() {
        this.spinnerService.show();
        setTimeout(() => this.spinnerService.hide(), 10000);
        this.serviceProviderValidationInitializer();
        if (this.serviceProvider.id == undefined) {
            this._serviceProviderService.createServiceProvider(this.serviceProvider).subscribe((result) => {
                console.log(result);
                if (result.error != "") {
                    this.hideElement = false;
                    this.validationMessages(result);
                } else {
                    this._rotuer.navigate(['/adminPanel/serviceProviders']);
                }
                this.spinnerService.hide();
            }, (error) => {
                console.log(error);
                this._rotuer.navigate(['/124']);
            });
        } else {
            this._serviceProviderService.updateServiceProvider(this.serviceProvider).subscribe((result) => {
                if (result.error != "") {
                    this.hideElement = false;
                    this.validationMessages(result);
                } else {
                    this._rotuer.navigate(['/adminPanel/serviceProviders']);
                }
                this.spinnerService.hide();
            }, (error) => {
                console.log(error);
                this._rotuer.navigate(['/124']);
            });
        }
    }

}
