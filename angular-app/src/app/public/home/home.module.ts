// Angular
import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './../home/home.component';

import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FrontPageService } from '../../services/frontPage.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';
import { ServiceProviderModalComponent } from './serviceProvider-modal.component';
import { SpLevelService }  from '../../services/spLevel.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    HomeRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule, 
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  entryComponents: [ServiceProviderModalComponent],
  declarations: [
    HomeComponent,
    ServiceProviderModalComponent
  ],
  providers: [FrontPageService,
    SpLevelService,
    ExcelService  
  ],
})
export class HomeModule { }
