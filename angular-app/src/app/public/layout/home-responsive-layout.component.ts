import { navItems } from './default_nav';
import { Component, OnInit, NgZone } from '@angular/core';
import { AuthenticationService } from '../../authentication/authentication.service';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from "@angular/router";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'app-dashboard',
    templateUrl: './rehomes-layout.component.html',
    styleUrls: [ './home-layout.component.scss' ]
})
export class HomeResponsiveComponent {
    public loaderHide = false;
    public navItems = navItems;
    public sidebarMinimized = true;
    private changes: MutationObserver;
    public element: HTMLElement = document.body;
    isLoggedIn: Observable<boolean>;
    userName = '';
    loggedOut = true;

    constructor(public authService: AuthenticationService,
        private spinnerService: Ng4LoadingSpinnerService,) {
        this.changes = new MutationObserver((mutations) => {
            //this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
            //alert(this.sidebarMinimized);
            //this.sidebarMinimized = true;
        });

        this.changes.observe(<Element>this.element, {
            attributes: true
        });
        this.sidebarMinimized = true;

        setTimeout(function(){
            this.loaderHide = true;
        },2000);
    }


    ngOnInit() {
        this.isLoggedIn = this.authService.isLoggedIn;
        this.authService.isLoggedIn.subscribe((res) => {
            if (res) {
                this.userName = this.authService.userName();
                this.loggedOut = false;
            } else {
                this.loggedOut = true;
            }
        }, (error) => {
            console.log(error);
            this.loggedOut = true;
        });
        this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
        //alert(this.sidebarMinimized);
        this.sidebarMinimized = true;
        //this.spinnerService.show();
        
        setTimeout(function(){
            this.loaderHide = true;
        },2000);
    }

    logout() {
        alert('from layout');
        this.authService.logout();
    }

}
