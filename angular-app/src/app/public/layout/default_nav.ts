export const navItems = [
  {
    name: 'Dashboard',
    url: '/default/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: '15'
    }
  },
  {
    divider: true 
  },
  {
    title: true,
    name: 'Generated Components',
  }
// newMenuLinks
];
