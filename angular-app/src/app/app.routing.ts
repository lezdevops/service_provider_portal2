import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { HomeResponsiveComponent } from './public';

 
import { AdminPanelLayoutComponent } from './adminPanel';

import { ServiceProviderPanelLayoutComponent } from './serviceProviderPanel';

import { GlobalSetupLayoutComponent } from './globalSetup';
import { AppDashboardComponent } from './app-dashboard/app-dashboard.component';

import { TestModuleLayoutComponent } from './testModule';

import { CustomerPanelLayoutComponent } from './customerPanel';
// importNewAngularAppComponent
  
import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { P124Component } from './views/error/124.component';
import { LoginComponent } from './authentication/login.component';
//import { RegistrationComponent } from './public/registration/registration.component';
   
export const routes: Routes = [
  {
    path: '',
    component: AppDashboardComponent,
    pathMatch: 'full',
    data: {
      title: 'Welcome To Application'
    }
  },
  { 
    path: 'sp-portal', 
    component: HomeResponsiveComponent,
    pathMatch: 'full',
    data: { 
      title: 'Home Page'
    },
    children: [
      {
        path: '',
        loadChildren: './public/home/home.module#HomeModule'
      }
    ]
  },
  { 
    path: 'details', 
    component: HomeResponsiveComponent,
    pathMatch: 'full',
    data: { 
      title: 'Details Page'
    },
    children: [
      {
        path: '',
        loadChildren: './public/detailsInfo/details.module#DetailsModule'
      }
    ]
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '124',
    component: P124Component,
    data: {
      title: 'Page 124'
    }
  }, 
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  { 
    path: 'registration', 
    component: HomeResponsiveComponent,
    pathMatch: 'full',
    data: { 
      title: 'Registration Page'
    },
    children: [
      {
        path: '',
        loadChildren: './public/registration/registration.module#RegistrationModule'
      }
    ]
  },
  {
    path: 'adminPanel',
    component: AdminPanelLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [

{path: '',loadChildren: './adminPanel/dashboard/dashboard.module#DashboardModule'}
,{path: 'locations',loadChildren: './adminPanel/location/location.module#LocationModule'}
,{path: 'thanas',loadChildren: './adminPanel/thana/thana.module#ThanaModule'}
,{path: 'districts',loadChildren: './adminPanel/district/district.module#DistrictModule'}
,{path: 'divisions',loadChildren: './adminPanel/division/division.module#DivisionModule'}
,{path: 'countrys',loadChildren: './adminPanel/country/country.module#CountryModule'}
,{path: 'serviceTypes',loadChildren: './adminPanel/serviceType/serviceType.module#ServiceTypeModule'}
,{path: 'spLevels',loadChildren: './adminPanel/spLevel/spLevel.module#SpLevelModule'}
,{path: 'trainingTypes',loadChildren: './adminPanel/trainingType/trainingType.module#TrainingTypeModule'}
,{path: 'serviceProviders',loadChildren: './adminPanel/serviceProvider/serviceProvider.module#ServiceProviderModule'}
,{path: 'customers',loadChildren: './adminPanel/customer/customer.module#CustomerModule'}
// adminPanelRouting
    ]
  }
  ,
  {
    path: 'serviceProviderPanel',
    component: ServiceProviderPanelLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [

{path: '',loadChildren: './serviceProviderPanel/dashboard/dashboard.module#DashboardModule'}
,{path: 'jobHistorys',loadChildren: './serviceProviderPanel/jobHistory/jobHistory.module#JobHistoryModule'}
,{path: 'agentReferences',loadChildren: './serviceProviderPanel/agentReference/agentReference.module#AgentReferenceModule'}
,{path: 'trainingInfos',loadChildren: './serviceProviderPanel/trainingInfo/trainingInfo.module#TrainingInfoModule'}
,{path: 'educationInfos',loadChildren: './serviceProviderPanel/educationInfo/educationInfo.module#EducationInfoModule'}
,{path: 'languageInfos',loadChildren: './serviceProviderPanel/languageInfo/languageInfo.module#LanguageInfoModule'}
,{path: 'experienceInfos',loadChildren: './serviceProviderPanel/experienceInfo/experienceInfo.module#ExperienceInfoModule'}
,{path: 'serviceResponseInfos',loadChildren: './serviceProviderPanel/serviceResponseInfo/serviceResponseInfo.module#ServiceResponseInfoModule'}
,{path: 'otherActivitiess',loadChildren: './serviceProviderPanel/otherActivities/otherActivities.module#OtherActivitiesModule'}
,{path: 'dailyTasks',loadChildren: './serviceProviderPanel/dailyTask/dailyTask.module#DailyTaskModule'}
,{path: 'spServiceLists',loadChildren: './serviceProviderPanel/spServiceList/spServiceList.module#SpServiceListModule'}
// serviceProviderPanelRouting
    ]
  }
  ,
  {
    path: 'globalSetup',
    component: GlobalSetupLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [

{path: '',loadChildren: './globalSetup/dashboard/dashboard.module#DashboardModule'}
,{path: 'roles',loadChildren: './globalSetup/role/role.module#RoleModule'}
,{path: 'userGroups',loadChildren: './globalSetup/userGroup/userGroup.module#UserGroupModule'}
,{path: 'users',loadChildren: './globalSetup/user/user.module#UserModule'}
,{path: 'userRoles',loadChildren: './globalSetup/userRole/userRole.module#UserRoleModule'}
,{path: 'permissions',loadChildren: './globalSetup/permission/permission.module#PermissionModule'}
,{path: 'rolePermissions',loadChildren: './globalSetup/rolePermission/rolePermission.module#RolePermissionModule'}
,{path: 'approvalProcesss',loadChildren: './globalSetup/approvalProcess/approvalProcess.module#ApprovalProcessModule'}
,{path: 'processApprovers',loadChildren: './globalSetup/processApprover/processApprover.module#ProcessApproverModule'}
,{path: 'registrationQueues',loadChildren: './globalSetup/registrationQueue/registrationQueue.module#RegistrationQueueModule'}
,{path: 'notificationProcesss',loadChildren: './globalSetup/notificationProcess/notificationProcess.module#NotificationProcessModule'}
,{path: 'notificationReceiverMap',loadChildren: './globalSetup/notificationReceiverMap/notificationReceiverMap.module#NotificationReceiverMapModule'}
,{path: 'notificationRouteMaps',loadChildren: './globalSetup/notificationRouteMap/notificationRouteMap.module#NotificationRouteMapModule'}
,{path: 'notifications',loadChildren: './globalSetup/notification/notification.module#NotificationModule'}
,{path: 'notificationDetailss',loadChildren: './globalSetup/notificationDetails/notificationDetails.module#NotificationDetailsModule'}
,{path: 'approvalQueues',loadChildren: './globalSetup/approvalQueue/approvalQueue.module#ApprovalQueueModule'}
,{path: 'approvalQueueDetailss',loadChildren: './globalSetup/approvalQueueDetails/approvalQueueDetails.module#ApprovalQueueDetailsModule'}
,{path: 'approverAdmins',loadChildren: './globalSetup/approverAdmin/approverAdmin.module#ApproverAdminModule'}
,{path: 'messageReceivers',loadChildren: './globalSetup/messageReceiver/messageReceiver.module#MessageReceiverModule'}
// globalSetupRouting
    ]
  }
  ,
  {
    path: 'testModule',
    component: TestModuleLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [

{path: '',loadChildren: './testModule/dashboard/dashboard.module#DashboardModule'}
,{path: 'testEntitys',loadChildren: './testModule/testEntity/testEntity.module#TestEntityModule'}
,{path: 'testEntityDetailss',loadChildren: './testModule/testEntityDetails/testEntityDetails.module#TestEntityDetailsModule'}
// testModuleRouting
    ]
  }
  ,
  {
    path: 'customerPanel',
    component: CustomerPanelLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [

{path: '',loadChildren: './customerPanel/dashboard/dashboard.module#DashboardModule'}
,{path: 'customerSps',loadChildren: './customerPanel/customerSp/customerSp.module#CustomerSpModule'}
,{path: 'customerJobs',loadChildren: './customerPanel/customerJob/customerJob.module#CustomerJobModule'}
,{path: 'customerRequests',loadChildren: './customerPanel/customerRequest/customerRequest.module#CustomerRequestModule'}
// customerPanelRouting
    ]
  }
  // newAngularModuleAppRouting
  ,
  {
    path: '**', 
    redirectTo: '404',
    data: {
      title: 'Page Not Found'
    }
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes,{ useHash: false }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
