// Angular
import { NgModule } from '@angular/core';
import { TestEntityRoutingModule } from './testEntity-routing.module';
import { ListtestEntityComponent } from './listtestEntity.component';
import { TestEntityFormComponent } from './testEntity-form.component';
import { TestEntityFormViewComponent } from './testEntity-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TestEntityService } from '../../services/testEntity.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';



@NgModule({
  imports: [
    TestEntityRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListtestEntityComponent,
    TestEntityFormComponent,
    TestEntityFormViewComponent
  ],
  providers: [TestEntityService,
    
    ExcelService  
  ],
})
export class TestEntityModule { }
