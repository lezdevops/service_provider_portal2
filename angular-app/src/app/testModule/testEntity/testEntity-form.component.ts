import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TestEntity }  from '../../model/testEntity';
import { Router ,ActivatedRoute } from "@angular/router";
import { TestEntityService } from '../../services/testEntity.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-testEntity-form',
  templateUrl: './testEntity-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class TestEntityFormComponent implements OnInit {
  private testEntity:TestEntity;
  
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
    
  
name_validationMessage = "";
description_validationMessage = "";
startDate_validationMessage = "";
endDate_validationMessage = "";
createdDate_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _testEntityService:TestEntityService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.testEntity=this._testEntityService.getter();
    this.testEntityValidationInitializer();  
      
  }
  testEntityValidationInitializer(){
     
this.name_validationMessage = "";
this.description_validationMessage = "";
this.startDate_validationMessage = "";
this.endDate_validationMessage = "";
this.createdDate_validationMessage = "";
  }
  clearTestEntityFormFields(){
     this.testEntity=this._testEntityService.getNew();   
  }
  validationMessages(result){  
     
this.name_validationMessage = result.name_validationMessage
this.description_validationMessage = result.description_validationMessage
this.startDate_validationMessage = result.startDate_validationMessage
this.endDate_validationMessage = result.endDate_validationMessage
this.createdDate_validationMessage = result.createdDate_validationMessage
  }
  processTestEntityForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.testEntityValidationInitializer();
    if(this.testEntity.id==undefined){
       this._testEntityService.createTestEntity(this.testEntity).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/testModule/testEntitys']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._testEntityService.updateTestEntity(this.testEntity).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/testModule/testEntitys']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
