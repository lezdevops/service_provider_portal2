import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListtestEntityComponent } from './listtestEntity.component';
import { TestEntityFormComponent } from './testEntity-form.component';
import { TestEntityFormViewComponent } from './testEntity-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListtestEntityComponent,
    data: {
      title: 'Test Entity List'
    }
  },{
      path: 'testEntityForm',
    component: TestEntityFormComponent,
    data: {
      title: 'Test Entity Form'
    }
  },{
      path: 'view',
    component: TestEntityFormViewComponent,
    data: {
      title: 'Test Entity Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestEntityRoutingModule {}
