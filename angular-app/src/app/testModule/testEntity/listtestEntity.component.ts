import { Component, OnInit } from '@angular/core';
import { TestEntityService } from '../../services/testEntity.service';
import { TestEntity }  from '../../model/testEntity';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listtestEntity',
  templateUrl: './listtestEntity.component.html'
})
export class ListtestEntityComponent implements OnInit {
  private testEntitys:TestEntity[];
  private common_validationMessage = '';
  private commonValidationHide = true;
  private pageName = "TESTENTITY";
    
  private firstInactivePages = [];
  private activePages = [];
  private lastInactivePages = [];
  lastPage = false;
  firstPage =false;
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  
  constructor(private _testEntityService: TestEntityService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.checkPermission();
          this.searchTestEntity();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._testEntityService.getTestEntitys().subscribe((result)=>{
      console.log(result);
      this.testEntitys=result;
    },(error)=>{
      cotestEntitynsole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this.authService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error==undefined || result.error==""){
             this.commonValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.commonValidationHide = false;
            this.common_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedTestEntitys(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._testEntityService.getPaginatedTestEntitys(pageNo,size).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
        this.commonValidationHide = false;
        this.common_validationMessage = result.common_validationMessage;
      }else{
          this.commonValidationHide = true;
          this.testEntitys=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
          this.lastPage = result.paginatedResult.last;
          this.firstPage = result.paginatedResult.first;
          this.pageNo = result.paginatedResult.number; 
          
          this.firstInactivePages = [];
          this.activePages = [];
          this.lastInactivePages = [];
          
          var i=0;
          for(i=0;i<this.pageNo;i++){
            this.firstInactivePages.push(i+1);
          }
          
           this.activePages.push(this.pageNo+1);
          
          for(i=this.pageNo+1;i<this.totalPages;i++){
            this.lastInactivePages.push(i+1);
          }
          
          //console.log(this.firstInactivePages);
          //console.log(this.activePages);
          //console.log(this.lastInactivePages);
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchTestEntity(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._testEntityService.getPaginatedTestEntitysWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
          this.common_validationMessage = result.common_validationMessage;
      }else{        
          this.commonValidationHide = true;
          this.testEntitys=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedTestEntitys(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedTestEntitys(this.pageNo,this.size);
      this.searchTestEntity();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedTestEntitys(this.pageNo,this.size);
    this.searchTestEntity();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedTestEntitys(pageNo,this.size);
    this.searchTestEntity();
  }
  deleteTestEntity(testEntity){
    if(confirm("Are you sure?")){
        this.spinnerService.show();
        setTimeout(()=>this.spinnerService.hide(),10000);
        this._testEntityService.deleteTestEntity(testEntity.id).subscribe((result)=>{
            if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.common_validationMessage;
            }else if(result.technicalError!=undefined && result.technicalError!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.technicalError;
            }else {
                this.commonValidationHide = true;
                this.testEntitys.splice(this.testEntitys.indexOf(testEntity),1);    
            }
            this.spinnerService.hide();
        },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
        });
    }
    
  }

   viewTestEntity(testEntity){  
      
    this._testEntityService.getTestEntity(testEntity.id).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
              this.common_validationMessage = result.common_validationMessage;
          }else if(result.testEntity!=null || result.testEntity!=undefined){
              let testEntity = result.testEntity;
             let startDate = testEntity.startDate;
  						if(startDate) {
  							testEntity.startDate = new Date(startDate);
  						}let endDate = testEntity.endDate;
  						if(endDate) {
  							testEntity.endDate = new Date(endDate);
  						}let createdDate = testEntity.createdDate;
  						if(createdDate) {
  							testEntity.createdDate = new Date(createdDate);
  						}
             this._testEntityService.setter(testEntity);
              this._router.navigate(['/testModule/testEntitys/view']);
          }
          this.spinnerService.hide();
      },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
      });
     
   }
   /*editTestEntity(testEntity){  
     this._testEntityService.setter(testEntity);
     this._router.navigate(['/testModule/testEntitys/testEntityForm']);
   }*/
  editTestEntity(testEntity){  
     this.spinnerService.show();  
     this._testEntityService.getTestEntity(testEntity.id).subscribe((result)=>{
         if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
             this.commonValidationHide = false;
             this.common_validationMessage = result.common_validationMessage;
         }else if(result.testEntity!=null || result.testEntity!=undefined){
             let testEntity = result.testEntity;
             let startDate = testEntity.startDate;
  						if(startDate) {
  							testEntity.startDate = new Date(startDate);
  						}let endDate = testEntity.endDate;
  						if(endDate) {
  							testEntity.endDate = new Date(endDate);
  						}let createdDate = testEntity.createdDate;
  						if(createdDate) {
  							testEntity.createdDate = new Date(createdDate);
  						}
             this._testEntityService.setter(testEntity);
             this._router.navigate(['/testModule/testEntitys/testEntityForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     });
     //this._testEntityService.setter(testEntity);
     
   }
   newTestEntity(){
     let testEntity = new TestEntity();
     this._testEntityService.setter(testEntity);
     this._router.navigate(['/testModule/testEntitys/testEntityForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.testEntitys, 'TestEntity');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("TestEntity List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Name", dataKey: "name"},{title: "Description", dataKey: "description"},{title: "Start Date", dataKey: "startDate"},{title: "End Date", dataKey: "endDate"},{title: "Created Date", dataKey: "createdDate"},
    ];
    doc.autoTable(reportColumns, this.testEntitys, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('TestEntityList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('TestEntityList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
