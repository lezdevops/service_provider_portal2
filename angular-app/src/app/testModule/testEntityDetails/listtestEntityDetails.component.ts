import { Component, OnInit } from '@angular/core';
import { TestEntityDetailsService } from '../../services/testEntityDetails.service';
import { TestEntityDetails }  from '../../model/testEntityDetails';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listtestEntityDetails',
  templateUrl: './listtestEntityDetails.component.html'
})
export class ListtestEntityDetailsComponent implements OnInit {
  private testEntityDetailss:TestEntityDetails[];
  private common_validationMessage = '';
  private commonValidationHide = true;
  private pageName = "TESTENTITYDETAILS";
    
  private firstInactivePages = [];
  private activePages = [];
  private lastInactivePages = [];
  lastPage = false;
  firstPage =false;
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  
  constructor(private _testEntityDetailsService: TestEntityDetailsService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.checkPermission();
          this.searchTestEntityDetails();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._testEntityDetailsService.getTestEntityDetailss().subscribe((result)=>{
      console.log(result);
      this.testEntityDetailss=result;
    },(error)=>{
      cotestEntityDetailsnsole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this.authService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error==undefined || result.error==""){
             this.commonValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.commonValidationHide = false;
            this.common_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedTestEntityDetailss(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._testEntityDetailsService.getPaginatedTestEntityDetailss(pageNo,size).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
        this.commonValidationHide = false;
        this.common_validationMessage = result.common_validationMessage;
      }else{
          this.commonValidationHide = true;
          this.testEntityDetailss=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
          this.lastPage = result.paginatedResult.last;
          this.firstPage = result.paginatedResult.first;
          this.pageNo = result.paginatedResult.number; 
          
          this.firstInactivePages = [];
          this.activePages = [];
          this.lastInactivePages = [];
          
          var i=0;
          for(i=0;i<this.pageNo;i++){
            this.firstInactivePages.push(i+1);
          }
          
           this.activePages.push(this.pageNo+1);
          
          for(i=this.pageNo+1;i<this.totalPages;i++){
            this.lastInactivePages.push(i+1);
          }
          
          //console.log(this.firstInactivePages);
          //console.log(this.activePages);
          //console.log(this.lastInactivePages);
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchTestEntityDetails(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._testEntityDetailsService.getPaginatedTestEntityDetailssWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
          this.common_validationMessage = result.common_validationMessage;
      }else{        
          this.commonValidationHide = true;
          this.testEntityDetailss=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedTestEntityDetailss(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedTestEntityDetailss(this.pageNo,this.size);
      this.searchTestEntityDetails();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedTestEntityDetailss(this.pageNo,this.size);
    this.searchTestEntityDetails();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedTestEntityDetailss(pageNo,this.size);
    this.searchTestEntityDetails();
  }
  deleteTestEntityDetails(testEntityDetails){
    if(confirm("Are you sure?")){
        this.spinnerService.show();
        setTimeout(()=>this.spinnerService.hide(),10000);
        this._testEntityDetailsService.deleteTestEntityDetails(testEntityDetails.id).subscribe((result)=>{
            if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.common_validationMessage;
            }else if(result.technicalError!=undefined && result.technicalError!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.technicalError;
            }else {
                this.commonValidationHide = true;
                this.testEntityDetailss.splice(this.testEntityDetailss.indexOf(testEntityDetails),1);    
            }
            this.spinnerService.hide();
        },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
        });
    }
    
  }

   viewTestEntityDetails(testEntityDetails){  
      
    this._testEntityDetailsService.getTestEntityDetails(testEntityDetails.id).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
              this.common_validationMessage = result.common_validationMessage;
          }else if(result.testEntityDetails!=null || result.testEntityDetails!=undefined){
              let testEntityDetails = result.testEntityDetails;
             let createdDate = testEntityDetails.createdDate;
  						if(createdDate) {
  							testEntityDetails.createdDate = new Date(createdDate);
  						}let datePicker = testEntityDetails.datePicker;
  						if(datePicker) {
  							testEntityDetails.datePicker = new Date(datePicker);
  						}
             this._testEntityDetailsService.setter(testEntityDetails);
              this._router.navigate(['/testModule/testEntityDetailss/view']);
          }
          this.spinnerService.hide();
      },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
      });
     
   }
   /*editTestEntityDetails(testEntityDetails){  
     this._testEntityDetailsService.setter(testEntityDetails);
     this._router.navigate(['/testModule/testEntityDetailss/testEntityDetailsForm']);
   }*/
  editTestEntityDetails(testEntityDetails){  
     this.spinnerService.show();  
     this._testEntityDetailsService.getTestEntityDetails(testEntityDetails.id).subscribe((result)=>{
         if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
             this.commonValidationHide = false;
             this.common_validationMessage = result.common_validationMessage;
         }else if(result.testEntityDetails!=null || result.testEntityDetails!=undefined){
             let testEntityDetails = result.testEntityDetails;
             let createdDate = testEntityDetails.createdDate;
  						if(createdDate) {
  							testEntityDetails.createdDate = new Date(createdDate);
  						}let datePicker = testEntityDetails.datePicker;
  						if(datePicker) {
  							testEntityDetails.datePicker = new Date(datePicker);
  						}
             this._testEntityDetailsService.setter(testEntityDetails);
             this._router.navigate(['/testModule/testEntityDetailss/testEntityDetailsForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     });
     //this._testEntityDetailsService.setter(testEntityDetails);
     
   }
   newTestEntityDetails(){
     let testEntityDetails = new TestEntityDetails();
     this._testEntityDetailsService.setter(testEntityDetails);
     this._router.navigate(['/testModule/testEntityDetailss/testEntityDetailsForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.testEntityDetailss, 'TestEntityDetails');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("TestEntityDetails List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Test Entity", dataKey: "testEntity"},{title: "Number", dataKey: "number"},{title: "Text Box", dataKey: "textBox"},{title: "Description", dataKey: "description"},{title: "Checkbox", dataKey: "checkbox"},{title: "Created Date", dataKey: "createdDate"},{title: "Date Picker", dataKey: "datePicker"},{title: "Doc File", dataKey: "docFile"},{title: "Image File", dataKey: "imageFile"},
    ];
    doc.autoTable(reportColumns, this.testEntityDetailss, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('TestEntityDetailsList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('TestEntityDetailsList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
