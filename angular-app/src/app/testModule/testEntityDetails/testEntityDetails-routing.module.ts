import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListtestEntityDetailsComponent } from './listtestEntityDetails.component';
import { TestEntityDetailsFormComponent } from './testEntityDetails-form.component';
import { TestEntityDetailsFormViewComponent } from './testEntityDetails-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListtestEntityDetailsComponent,
    data: {
      title: 'Test Entity Details List'
    }
  },{
      path: 'testEntityDetailsForm',
    component: TestEntityDetailsFormComponent,
    data: {
      title: 'Test Entity Details Form'
    }
  },{
      path: 'view',
    component: TestEntityDetailsFormViewComponent,
    data: {
      title: 'Test Entity Details Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestEntityDetailsRoutingModule {}
