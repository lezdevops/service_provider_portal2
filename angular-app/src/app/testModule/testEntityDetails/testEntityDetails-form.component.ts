import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TestEntityDetails }  from '../../model/testEntityDetails';
import { Router ,ActivatedRoute } from "@angular/router";
import { TestEntityDetailsService } from '../../services/testEntityDetails.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { TestEntity }  from '../../model/testEntity';
import { TestEntityService }  from '../../services/testEntity.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-testEntityDetails-form',
  templateUrl: './testEntityDetails-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class TestEntityDetailsFormComponent implements OnInit {
  private testEntityDetails:TestEntityDetails;
  docFileHide = true;imageFileHide = true;
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private testEntitys:TestEntity[];  
  
testEntity_validationMessage = "";
number_validationMessage = "";
textBox_validationMessage = "";
description_validationMessage = "";
checkbox_validationMessage = "";
createdDate_validationMessage = "";
datePicker_validationMessage = "";
docFile_validationMessage = "";
imageFile_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _testEntityDetailsService:TestEntityDetailsService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _testEntityService:TestEntityService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.testEntityDetails=this._testEntityDetailsService.getter();
    this.testEntityDetailsValidationInitializer();  
    this._testEntityService.getTestEntitys().subscribe((result)=>{
   			      this.testEntitys=result.testEntitys;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  testEntityDetailsValidationInitializer(){
     
this.testEntity_validationMessage = "";
this.number_validationMessage = "";
this.textBox_validationMessage = "";
this.description_validationMessage = "";
this.checkbox_validationMessage = "";
this.createdDate_validationMessage = "";
this.datePicker_validationMessage = "";
this.docFile_validationMessage = "";
this.imageFile_validationMessage = "";
  }
  clearTestEntityDetailsFormFields(){
     this.testEntityDetails=this._testEntityDetailsService.getNew();   
  }
  validationMessages(result){  
     
this.testEntity_validationMessage = result.testEntity_validationMessage
this.number_validationMessage = result.number_validationMessage
this.textBox_validationMessage = result.textBox_validationMessage
this.description_validationMessage = result.description_validationMessage
this.checkbox_validationMessage = result.checkbox_validationMessage
this.createdDate_validationMessage = result.createdDate_validationMessage
this.datePicker_validationMessage = result.datePicker_validationMessage
this.docFile_validationMessage = result.docFile_validationMessage
this.imageFile_validationMessage = result.imageFile_validationMessage
  }
  processTestEntityDetailsForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.testEntityDetailsValidationInitializer();
    if(this.testEntityDetails.id==undefined){
       this._testEntityDetailsService.createTestEntityDetails(this.testEntityDetails).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/testModule/testEntityDetailss']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._testEntityDetailsService.updateTestEntityDetails(this.testEntityDetails).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/testModule/testEntityDetailss']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    onDocFileFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.docFileHide = false;
                
                this.testEntityDetails.docFile = reader.result;
            };
        }
    }
    cleardocFileFile() {
        this.testEntityDetails.docFile = '';
        this.docFileHide = true;
    }onImageFileFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.imageFileHide = false;
                
                this.testEntityDetails.imageFile = reader.result;
            };
        }
    }
    clearimageFileFile() {
        this.testEntityDetails.imageFile = '';
        this.imageFileHide = true;
    }

}
