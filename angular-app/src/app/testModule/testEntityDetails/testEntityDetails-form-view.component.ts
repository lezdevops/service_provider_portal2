import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TestEntityDetails }  from '../../model/testEntityDetails';
import { Router ,ActivatedRoute} from "@angular/router";
import { TestEntityDetailsService } from '../../services/testEntityDetails.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { TestEntity }  from '../../model/testEntity';
import { TestEntityService }  from '../../services/testEntity.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-testEntityDetails-view-form',
  templateUrl: './testEntityDetails-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class TestEntityDetailsFormViewComponent implements OnInit {
  private testEntityDetails:TestEntityDetails;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private testEntitys:TestEntity[];  
  
testEntity_validationMessage = "";
number_validationMessage = "";
textBox_validationMessage = "";
description_validationMessage = "";
checkbox_validationMessage = "";
createdDate_validationMessage = "";
datePicker_validationMessage = "";
docFile_validationMessage = "";
imageFile_validationMessage = "";
    
  constructor(
    private _testEntityDetailsService:TestEntityDetailsService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _testEntityService:TestEntityService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.testEntityDetails=this._testEntityDetailsService.getter(); 
    this._testEntityService.getTestEntitys().subscribe((result)=>{
   			      this.testEntitys=result.testEntitys;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newTestEntityDetails(){
   let testEntityDetails = new TestEntityDetails();
    this._testEntityDetailsService.setter(testEntityDetails);
     this._rotuer.navigate(['/testModule/testEntityDetailss/testEntityDetailsForm']);   
   }
  testEntityDetailsEditAction(){
     this._testEntityDetailsService.setter(this.testEntityDetails);
     this._rotuer.navigate(['/testModule/testEntityDetailss/testEntityDetailsForm']);        
  }
  printTestEntityDetailsDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printTestEntityDetailsFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('TestEntityDetailsList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
