// Angular
import { NgModule } from '@angular/core';
import { TestEntityDetailsRoutingModule } from './testEntityDetails-routing.module';
import { ListtestEntityDetailsComponent } from './listtestEntityDetails.component';
import { TestEntityDetailsFormComponent } from './testEntityDetails-form.component';
import { TestEntityDetailsFormViewComponent } from './testEntityDetails-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TestEntityDetailsService } from '../../services/testEntityDetails.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { TestEntityService }  from '../../services/testEntity.service';

@NgModule({
  imports: [
    TestEntityDetailsRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListtestEntityDetailsComponent,
    TestEntityDetailsFormComponent,
    TestEntityDetailsFormViewComponent
  ],
  providers: [TestEntityDetailsService,
    TestEntityService,
    ExcelService  
  ],
})
export class TestEntityDetailsModule { }
