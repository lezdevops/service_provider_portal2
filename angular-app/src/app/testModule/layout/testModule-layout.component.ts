//import { navItems } from './adminPanel_nav';
import { navItems } from './testModule_nav';
import { Component, OnInit, NgZone } from '@angular/core';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './testModule-layout.component.html'
})
export class TestModuleLayoutComponent {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  isLoggedIn: Observable<boolean>;

  constructor(public authService: AuthenticationService) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
  }
  

  ngOnInit() {

    this.isLoggedIn = this.authService.isLoggedIn;
  }

  onLogout(){
    this.authService.logout();
  }

}
