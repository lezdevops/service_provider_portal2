import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ApproverAdmin }  from '../../model/approverAdmin';
import { Router, ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ApprovalProcess }  from '../../model/approvalProcess';
import { ApprovalProcessService }  from '../../services/approvalProcess.service';
import { ApproverAdminService }  from '../../services/approverAdmin.service';

@Component({
  selector: 'my-app',
  templateUrl: './processApprover.component.html'
})
export class ProcessApproverComponent {
  private approvalProcesss:ApprovalProcess[] = [];
  private approverAdmins:ApproverAdmin[] = [];
   
  dropdownSettings = {}; 
  constructor(private _router: Router,
    private authService: AuthenticationService,
    private _approvalProcessService: ApprovalProcessService,
    private _approverAdminService: ApproverAdminService,
    private excelService: ExcelService,
    private spinnerService: Ng4LoadingSpinnerService
) { }

  removeApprovalProcess(approvalProcess){   
    let index = this.approvalProcesss.indexOf(approvalProcess);
    
    let splicedArray = this.approvalProcesss.splice(index);
    for(var i=1;i<splicedArray.length;i++){
      this.approvalProcesss.push(splicedArray[i]);
    }
  } 
  
  ngOnInit() {

    this.dropdownSettings = {
      singleSelection: false,
      text: "Select Approver.",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    let ref = this;
      this._approvalProcessService.getApprovalProcesss().subscribe((result) => {
          result.approvalProcesss.forEach(approvalProcess => {
            let approvalProcessObj = approvalProcess;
            let processApproverAdmins = [];
            approvalProcess.approverAdmins.forEach(approverAdmin => {
              approverAdmin.itemName = approverAdmin.name;
              processApproverAdmins.push(approverAdmin);
            });
            approvalProcessObj.approverAdmins = processApproverAdmins;
            
            ref.approvalProcesss.push(approvalProcessObj);
          });
          console.log(ref.approvalProcesss);
      }, (error) => {
          
      });

      this._approverAdminService.getApproverAdmins().subscribe((result) => {
          result.approverAdmins.forEach(approverAdmin => {
            approverAdmin.itemName = approverAdmin.name;
              ref.approverAdmins.push(approverAdmin);
          });
      }, (error) => {
          
      });
  }

  onItemSelect(item: any) {
    console.log(item["itemName"]);
    console.log(this.approvalProcesss);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.approvalProcesss);
  }
  onSelectAll(items: any) {
    console.log(this.approvalProcesss);
  }
  onDeSelectAll(items: any) {
    console.log(this.approvalProcesss);
  }
  saveMultipleApprovalProcessData(){
    this._approvalProcessService.saveMultipleApprovalProcess(this.approvalProcesss).subscribe((result)=>{
      console.log(result);
        if(result.error!=""){            
        }else{
           //this._rotuer.navigate(['/globalSetup/approvalProcesss']);
        }      
        this.spinnerService.hide();   
    },(error)=>{
      console.log(error);
        //this._rotuer.navigate(['/124']);
    });

  }

}
