import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProcessApproverComponent } from './processApprover.component';

const routes: Routes = [{
    path: '',
    component: ProcessApproverComponent,
    data: {
      title: 'Process Approver Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessApproverRoutingModule {}
