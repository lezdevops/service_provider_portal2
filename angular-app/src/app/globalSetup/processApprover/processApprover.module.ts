// Angular
import { NgModule } from '@angular/core';
import { ProcessApproverRoutingModule } from './processApprover-routing.module';
import { ProcessApproverComponent } from './processApprover.component';

import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

import { ApprovalProcessService }  from '../../services/approvalProcess.service';
import { ApproverAdminService }  from '../../services/approverAdmin.service';

@NgModule({
  imports: [
    ProcessApproverRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AngularMultiSelectModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ProcessApproverComponent
  ],
  providers: [
    ApprovalProcessService,
    ApproverAdminService,
    ExcelService  
  ],
})
export class ProcessApproverModule { }
