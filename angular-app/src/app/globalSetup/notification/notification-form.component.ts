import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Notification }  from '../../model/notification';
import { Router ,ActivatedRoute } from "@angular/router";
import { NotificationService } from '../../services/notification.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NotificationProcess }  from '../../model/notificationProcess';
import { NotificationProcessService }  from '../../services/notificationProcess.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-notification-form',
  templateUrl: './notification-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class NotificationFormComponent implements OnInit {
  private notification:Notification;
  
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private notificationProcesss:NotificationProcess[];  
  
NotificationProcess_validationMessage = "";
message_validationMessage = "";
oldObjectVal_validationMessage = "";
newObjectVal_validationMessage = "";
attachment_validationMessage = "";
url_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _notificationService:NotificationService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _notificationProcessService:NotificationProcessService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.notification=this._notificationService.getter();
    this.notificationValidationInitializer();  
    this._notificationProcessService.getNotificationProcesss().subscribe((result)=>{
   			      this.notificationProcesss=result.notificationProcesss;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  notificationValidationInitializer(){
     
this.NotificationProcess_validationMessage = "";
this.message_validationMessage = "";
this.oldObjectVal_validationMessage = "";
this.newObjectVal_validationMessage = "";
this.attachment_validationMessage = "";
this.url_validationMessage = "";
  }
  clearNotificationFormFields(){
     this.notification=this._notificationService.getNew();   
  }
  validationMessages(result){  
     
this.NotificationProcess_validationMessage = result.NotificationProcess_validationMessage
this.message_validationMessage = result.message_validationMessage
this.oldObjectVal_validationMessage = result.oldObjectVal_validationMessage
this.newObjectVal_validationMessage = result.newObjectVal_validationMessage
this.attachment_validationMessage = result.attachment_validationMessage
this.url_validationMessage = result.url_validationMessage
  }
  processNotificationForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.notificationValidationInitializer();
    if(this.notification.id==undefined){
       this._notificationService.createNotification(this.notification).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/notifications']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._notificationService.updateNotification(this.notification).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/notifications']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
