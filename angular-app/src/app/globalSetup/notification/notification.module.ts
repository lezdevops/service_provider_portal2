// Angular
import { NgModule } from '@angular/core';
import { NotificationRoutingModule } from './notification-routing.module';
import { ListnotificationComponent } from './listnotification.component';
import { NotificationFormComponent } from './notification-form.component';
import { NotificationFormViewComponent } from './notification-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NotificationService } from '../../services/notification.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { NotificationProcessService }  from '../../services/notificationProcess.service';

@NgModule({
  imports: [
    NotificationRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListnotificationComponent,
    NotificationFormComponent,
    NotificationFormViewComponent
  ],
  providers: [NotificationService,
    NotificationProcessService,
    ExcelService  
  ],
})
export class NotificationModule { }
