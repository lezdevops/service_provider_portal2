import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListnotificationComponent } from './listnotification.component';
import { NotificationFormComponent } from './notification-form.component';
import { NotificationFormViewComponent } from './notification-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListnotificationComponent,
    data: {
      title: 'Notification List'
    }
  },{
      path: 'notificationForm',
    component: NotificationFormComponent,
    data: {
      title: 'Notification Form'
    }
  },{
      path: 'view',
    component: NotificationFormViewComponent,
    data: {
      title: 'Notification Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationRoutingModule {}
