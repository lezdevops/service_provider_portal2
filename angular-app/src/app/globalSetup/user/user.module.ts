// Angular
import { NgModule } from '@angular/core';
import { UserRoutingModule } from './user-routing.module';
import { ListuserComponent } from './listuser.component';
import { UserFormComponent } from './user-form.component';
import { UserFormViewComponent } from './user-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../services/user.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { UserGroupService }  from '../../services/userGroup.service';

@NgModule({
  imports: [
    UserRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListuserComponent,
    UserFormComponent,
    UserFormViewComponent
  ],
  providers: [UserService,
    UserGroupService,
    ExcelService  
  ],
})
export class UserModule { }
