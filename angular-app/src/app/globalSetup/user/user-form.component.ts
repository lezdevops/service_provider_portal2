import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { User }  from '../../model/user';
import { Router ,ActivatedRoute } from "@angular/router";
import { UserService } from '../../services/user.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserGroup }  from '../../model/userGroup';
import { UserGroupService }  from '../../services/userGroup.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-user-form',
  templateUrl: './user-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class UserFormComponent implements OnInit {
  private user:User;
  
    
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private userGroups:UserGroup[];  
  
userName_validationMessage = "";
password_validationMessage = "";
passwordHint_validationMessage = "";
email_validationMessage = "";
firstName_validationMessage = "";
lastName_validationMessage = "";
active_validationMessage = "";
UserGroup_validationMessage = "";
createdBy_validationMessage = "";
created_validationMessage = "";
updated_validationMessage = "";
permission_validationMessage=""
    
  constructor(
    private _userService:UserService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _userGroupService:UserGroupService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.user=this._userService.getter();
    this.userValidationInitializer();  
    this._userGroupService.getUserGroups().subscribe((userGroups)=>{
   			      this.userGroups=userGroups;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  userValidationInitializer(){
     
this.userName_validationMessage = "";
this.password_validationMessage = "";
this.passwordHint_validationMessage = "";
this.email_validationMessage = "";
this.firstName_validationMessage = "";
this.lastName_validationMessage = "";
this.active_validationMessage = "";
this.UserGroup_validationMessage = "";
this.createdBy_validationMessage = "";
this.created_validationMessage = "";
this.updated_validationMessage = "";
this.permission_validationMessage = "";
  }
  clearUserFormFields(){
     this.user=this._userService.getter();   
  }
  validationMessages(result){  
     
this.userName_validationMessage = result.userName_validationMessage
this.password_validationMessage = result.password_validationMessage
this.passwordHint_validationMessage = result.passwordHint_validationMessage
this.email_validationMessage = result.email_validationMessage
this.firstName_validationMessage = result.firstName_validationMessage
this.lastName_validationMessage = result.lastName_validationMessage
this.active_validationMessage = result.active_validationMessage
this.UserGroup_validationMessage = result.UserGroup_validationMessage
this.createdBy_validationMessage = result.createdBy_validationMessage
this.created_validationMessage = result.created_validationMessage
this.updated_validationMessage = result.updated_validationMessage
this.permission_validationMessage = result.permission_validationMessage
  }
  processUserForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.userValidationInitializer();
    if(this.user.id==undefined){
       this._userService.createUser(this.user).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/users']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._userService.updateUser(this.user).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/users']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
