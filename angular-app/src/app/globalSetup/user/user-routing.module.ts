import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListuserComponent } from './listuser.component';
import { UserFormComponent } from './user-form.component';
import { UserFormViewComponent } from './user-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListuserComponent,
    data: {
      title: 'User List'
    }
  },{
      path: 'userForm',
    component: UserFormComponent,
    data: {
      title: 'User Form'
    }
  },{
      path: 'view',
    component: UserFormViewComponent,
    data: {
      title: 'User Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
