import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { User }  from '../../model/user';
import { Router, ActivatedRoute} from "@angular/router";
import { UserService } from '../../services/user.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

import { UserGroup }  from '../../model/userGroup';
import { UserGroupService }  from '../../services/userGroup.service';

export function getAlertConfig(): AlertConfig {
    return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
    selector: 'app-user-view-form',
    templateUrl: './user-tabbedForm-view.component.html',
    encapsulation: ViewEncapsulation.None,
    styles: [
        `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
    ],
    providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class UserFormViewComponent implements OnInit {
    private user: User;
    hideElement = true;

    simpleItems = [true, 'Two', 3];
    selectedSimpleItem = '';

    private userGroups: UserGroup[];

    userName_validationMessage = "";
    password_validationMessage = "";
    passwordHint_validationMessage = "";
    email_validationMessage = "";
    firstName_validationMessage = "";
    lastName_validationMessage = "";
    active_validationMessage = "";
    UserGroup_validationMessage = "";
    createdBy_validationMessage = "";
    created_validationMessage = "";
    updated_validationMessage = "";

    constructor(
        private _userService: UserService,
        private _rotuer: Router,
        private authService: AuthenticationService,
        private _userGroupService: UserGroupService,
        sanitizer: DomSanitizer,
        private route: ActivatedRoute,
        private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
        this.authService.isLoggedIn.subscribe((res) => {
            if (res) {

            } else {
                this._rotuer.navigate(['/login']);
            }
        }, (error) => {
            console.log(error);
            this._rotuer.navigate(['/124']);
        });
        this.user = this._userService.getter();
        this._userGroupService.getUserGroups().subscribe((userGroups) => {
   			      this.userGroups = userGroups;
        }, (error) => {
   			      //console.log(error);
        });
    }

    newUser() {
        let user = new User();
        this._userService.setter(user);
        this._rotuer.navigate(['/globalSetup/users/userForm']);
    }
    userEditAction() {
        this._userService.setter(this.user);
        this._rotuer.navigate(['/globalSetup/users/userForm']);
    }
    printUserDetails() {
        this.spinnerService.show();
        var data = document.getElementById('printUserFormContainer');
        html2canvas(data).then(canvas => {
            // Few necessary setting options  
            var imgWidth = 208;
            var pageHeight = 295;
            var imgHeight = canvas.height * imgWidth / canvas.width;
            var heightLeft = imgHeight;

            const contentDataURL = canvas.toDataURL('image/png')
            let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
            var position = 0;
            pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
            pdf.save('UserList.pdf'); // Generated PDF 
            this.spinnerService.hide();
        });
    }

}
