import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApprovalQueue }  from '../../model/approvalQueue';
import { Router ,ActivatedRoute} from "@angular/router";
import { ApprovalQueueService } from '../../services/approvalQueue.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { ApprovalProcess }  from '../../model/approvalProcess';
import { ApprovalProcessService }  from '../../services/approvalProcess.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-approvalQueue-view-form',
  templateUrl: './approvalQueue-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ApprovalQueueFormViewComponent implements OnInit {
  private approvalQueue:ApprovalQueue;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private approvalProcesss:ApprovalProcess[];  
  
approvalProcess_validationMessage = "";
isActive_validationMessage = "";
attachment_validationMessage = "";
approvalMessage_validationMessage = "";
    
  constructor(
    private _approvalQueueService:ApprovalQueueService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _approvalProcessService:ApprovalProcessService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.approvalQueue=this._approvalQueueService.getter(); 
    this._approvalProcessService.getApprovalProcesss().subscribe((result)=>{
   			      this.approvalProcesss=result.approvalProcesss;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newApprovalQueue(){
   let approvalQueue = new ApprovalQueue();
    this._approvalQueueService.setter(approvalQueue);
     this._rotuer.navigate(['/globalSetup/approvalQueues/approvalQueueForm']);   
   }
  approvalQueueEditAction(){
     this._approvalQueueService.setter(this.approvalQueue);
     this._rotuer.navigate(['/globalSetup/approvalQueues/approvalQueueForm']);        
  }
  printApprovalQueueDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printApprovalQueueFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ApprovalQueueList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
