import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApprovalQueue }  from '../../model/approvalQueue';
import { Router ,ActivatedRoute } from "@angular/router";
import { ApprovalQueueService } from '../../services/approvalQueue.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ApprovalProcess }  from '../../model/approvalProcess';
import { ApprovalProcessService }  from '../../services/approvalProcess.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-approvalQueue-form',
  templateUrl: './approvalQueue-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ApprovalQueueFormComponent implements OnInit {
  private approvalQueue:ApprovalQueue;
  attachmentHide = true;
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private approvalProcesss:ApprovalProcess[];  
  
approvalProcess_validationMessage = "";
isActive_validationMessage = "";
attachment_validationMessage = "";
approvalMessage_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _approvalQueueService:ApprovalQueueService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _approvalProcessService:ApprovalProcessService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.approvalQueue=this._approvalQueueService.getter();
    this.approvalQueueValidationInitializer();  
    this._approvalProcessService.getApprovalProcesss().subscribe((result)=>{
   			      this.approvalProcesss=result.approvalProcesss;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  approvalQueueValidationInitializer(){
     
this.approvalProcess_validationMessage = "";
this.isActive_validationMessage = "";
this.attachment_validationMessage = "";
this.approvalMessage_validationMessage = "";
  }
  clearApprovalQueueFormFields(){
     this.approvalQueue=this._approvalQueueService.getNew();   
  }
  validationMessages(result){  
     
this.approvalProcess_validationMessage = result.approvalProcess_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
this.attachment_validationMessage = result.attachment_validationMessage
this.approvalMessage_validationMessage = result.approvalMessage_validationMessage
  }
  processApprovalQueueForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.approvalQueueValidationInitializer();
    if(this.approvalQueue.id==undefined){
       this._approvalQueueService.createApprovalQueue(this.approvalQueue).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/approvalQueues']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._approvalQueueService.updateApprovalQueue(this.approvalQueue).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/approvalQueues']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    onAttachmentFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.attachmentHide = false;
                
                this.approvalQueue.attachment = reader.result;
            };
        }
    }
    clearattachmentFile() {
        this.approvalQueue.attachment = '';
        this.attachmentHide = true;
    }

}
