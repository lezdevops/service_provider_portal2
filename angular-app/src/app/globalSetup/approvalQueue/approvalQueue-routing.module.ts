import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListapprovalQueueComponent } from './listapprovalQueue.component';
import { ApprovalQueueFormComponent } from './approvalQueue-form.component';
import { ApprovalQueueFormViewComponent } from './approvalQueue-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListapprovalQueueComponent,
    data: {
      title: 'Approval Queue List'
    }
  },{
      path: 'approvalQueueForm',
    component: ApprovalQueueFormComponent,
    data: {
      title: 'Approval Queue Form'
    }
  },{
      path: 'view',
    component: ApprovalQueueFormViewComponent,
    data: {
      title: 'Approval Queue Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApprovalQueueRoutingModule {}
