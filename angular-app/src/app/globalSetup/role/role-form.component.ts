import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Role }  from '../../model/role';
import { Router ,ActivatedRoute } from "@angular/router";
import { RoleService } from '../../services/role.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { User }  from '../../model/user';
import { UserService }  from '../../services/user.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-role-form',
  templateUrl: './role-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class RoleFormComponent implements OnInit {
  private role:Role;
  
    
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private users:User[];  
  
name_validationMessage = "";
users_validationMessage = "";
createdDate_validationMessage = "";
    
  constructor(
    private _roleService:RoleService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _userService:UserService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.role=this._roleService.getter();
    this.roleValidationInitializer();  
    this._userService.getUsers().subscribe((users)=>{
   			      this.users=users;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  roleValidationInitializer(){
     
this.name_validationMessage = "";
this.users_validationMessage = "";
this.createdDate_validationMessage = "";
  }
  clearRoleFormFields(){
     this.role=this._roleService.getter();   
  }
  validationMessages(result){  
     
this.name_validationMessage = result.name_validationMessage
this.users_validationMessage = result.users_validationMessage
this.createdDate_validationMessage = result.createdDate_validationMessage
  }
  processRoleForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.roleValidationInitializer();
    if(this.role.id==undefined){
       this._roleService.createRole(this.role).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/roles']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._roleService.updateRole(this.role).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/roles']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
