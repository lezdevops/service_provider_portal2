import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Role }  from '../../model/role';
import { Router ,ActivatedRoute} from "@angular/router";
import { RoleService } from '../../services/role.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { User }  from '../../model/user';
import { UserService }  from '../../services/user.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-role-view-form',
  templateUrl: './role-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class RoleFormViewComponent implements OnInit {
  private role:Role;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private users:User[];  
  
name_validationMessage = "";
users_validationMessage = "";
createdDate_validationMessage = "";
    
  constructor(
    private _roleService:RoleService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _userService:UserService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.role=this._roleService.getter(); 
    this._userService.getUsers().subscribe((users)=>{
   			      this.users=users;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newRole(){
   let role = new Role();
    this._roleService.setter(role);
     this._rotuer.navigate(['/globalSetup/roles/roleForm']);   
   }
  roleEditAction(){
     this._roleService.setter(this.role);
     this._rotuer.navigate(['/globalSetup/roles/roleForm']);        
  }
  printRoleDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printRoleFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('RoleList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
