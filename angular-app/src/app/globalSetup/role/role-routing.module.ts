import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListroleComponent } from './listrole.component';
import { RoleFormComponent } from './role-form.component';
import { RoleFormViewComponent } from './role-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListroleComponent,
    data: {
      title: 'Role List'
    }
  },{
      path: 'roleForm',
    component: RoleFormComponent,
    data: {
      title: 'Role Form'
    }
  },{
      path: 'view',
    component: RoleFormViewComponent,
    data: {
      title: 'Role Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleRoutingModule {}
