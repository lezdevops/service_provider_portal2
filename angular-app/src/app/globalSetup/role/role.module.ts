// Angular
import { NgModule } from '@angular/core';
import { RoleRoutingModule } from './role-routing.module';
import { ListroleComponent } from './listrole.component';
import { RoleFormComponent } from './role-form.component';
import { RoleFormViewComponent } from './role-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RoleService } from '../../services/role.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { UserService }  from '../../services/user.service';

@NgModule({
  imports: [
    RoleRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListroleComponent,
    RoleFormComponent,
    RoleFormViewComponent
  ],
  providers: [RoleService,
    UserService,
    ExcelService  
  ],
})
export class RoleModule { }
