import { Component, OnInit } from '@angular/core';
import { RoleService } from '../../services/role.service';
import { Role }  from '../../model/role';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listrole',
  templateUrl: './listrole.component.html'
})
export class ListroleComponent implements OnInit {
  private roles:Role[];
  private hasCreatePermission = false;
  private hasReadPermission = false;
  private hasEditPermission = false;
  private hasDeletePermission = false;
  private hasReportPermission = false;
    
  private allowedPermissions:string[];
    
  constructor(private _roleService: RoleService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
    
  ngOnInit() {
    this.allowedPermissions = ["ROLE_CREATE","ROLE_READ","ROLE_EDIT","ROLE_DELETE"];
    this.pageComponentsInitWithPermissions();
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchRole();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._roleService.getRoles().subscribe((result)=>{
      console.log(result);
      this.roles=result;
    },(error)=>{
      corolensole.log(error);
    })*/
    
  }
  pageComponentsInitWithPermissions(){
      //allowedPermissions
      this.hasCreatePermission = true;
      this.hasReadPermission = true;
      this.hasEditPermission = true;
      this.hasDeletePermission = true;
      this.hasReportPermission = true;
  }
  paginatedRoles(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._roleService.getPaginatedRoles(pageNo,size).subscribe((result)=>{
      console.log(result.paginatedResult.content);
      this.roles=result.paginatedResult.content;
      this.totalPages = result.paginatedResult.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchRole(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._roleService.getPaginatedRolesWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.roles=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedRoles(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedRoles(this.pageNo,this.size);
      this.searchRole();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedRoles(this.pageNo,this.size);
    this.searchRole();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedRoles(pageNo,this.size);
    this.searchRole();
  }
  deleteRole(role){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._roleService.deleteRole(role.id).subscribe((data)=>{
        this.roles.splice(this.roles.indexOf(role),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewRole(role){  
     this._roleService.setter(role);
     this._router.navigate(['/globalSetup/roles/view']);
   }
   /*editRole(role){  
     this._roleService.setter(role);
     this._router.navigate(['/globalSetup/roles/roleForm']);
   }*/
  editRole(role){  
     this.spinnerService.show();  
     this._roleService.getRole(role.id).subscribe((result)=>{
          this._roleService.setter(result.role);
          this._router.navigate(['/globalSetup/roles/roleForm']);
          this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     })
     //this._roleService.setter(role);
     
   }
   newRole(){
   let role = new Role();
    this._roleService.setter(role);
     this._router.navigate(['/globalSetup/roles/roleForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.roles, 'Role');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("Role List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Name", dataKey: "name"},{title: "Users", dataKey: "users"},{title: "Created Date", dataKey: "createdDate"},
    ];
    doc.autoTable(reportColumns, this.roles, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('RoleList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('RoleList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
