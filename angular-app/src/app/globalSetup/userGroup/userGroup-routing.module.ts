import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListuserGroupComponent } from './listuserGroup.component';
import { UserGroupFormComponent } from './userGroup-form.component';
import { UserGroupFormViewComponent } from './userGroup-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListuserGroupComponent,
    data: {
      title: 'User Group List'
    }
  },{
      path: 'userGroupForm',
    component: UserGroupFormComponent,
    data: {
      title: 'User Group Form'
    }
  },{
      path: 'view',
    component: UserGroupFormViewComponent,
    data: {
      title: 'User Group Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserGroupRoutingModule {}
