import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { UserGroup }  from '../../model/userGroup';
import { Router ,ActivatedRoute } from "@angular/router";
import { UserGroupService } from '../../services/userGroup.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { User }  from '../../model/user';
import { UserService }  from '../../services/user.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-userGroup-form',
  templateUrl: './userGroup-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class UserGroupFormComponent implements OnInit {
  private userGroup:UserGroup;
  imageAddressHide = true;
    
  hideElement = true;
  permissionValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private users:User[];  
  
name_validationMessage = "";
metaData_validationMessage = "";
websiteAddress_validationMessage = "";
phoneNo_validationMessage = "";
email_validationMessage = "";
address_validationMessage = "";
currency_validationMessage = "";
imageAddress_validationMessage = "";
user_validationMessage = "";
created_validationMessage = "";
updated_validationMessage = "";
  permission_validationMessage = '';
    
  constructor(
    private _userGroupService:UserGroupService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _userService:UserService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.userGroup=this._userGroupService.getter();
    this.userGroupValidationInitializer();  
    this._userService.getUsers().subscribe((users)=>{
   			      this.users=users;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  userGroupValidationInitializer(){
     
this.name_validationMessage = "";
this.metaData_validationMessage = "";
this.websiteAddress_validationMessage = "";
this.phoneNo_validationMessage = "";
this.email_validationMessage = "";
this.address_validationMessage = "";
this.currency_validationMessage = "";
this.imageAddress_validationMessage = "";
this.user_validationMessage = "";
this.created_validationMessage = "";
this.updated_validationMessage = "";
  }
  clearUserGroupFormFields(){
     this.userGroup=this._userGroupService.getter();   
  }
  validationMessages(result){  
     
this.name_validationMessage = result.name_validationMessage
this.metaData_validationMessage = result.metaData_validationMessage
this.websiteAddress_validationMessage = result.websiteAddress_validationMessage
this.phoneNo_validationMessage = result.phoneNo_validationMessage
this.email_validationMessage = result.email_validationMessage
this.address_validationMessage = result.address_validationMessage
this.currency_validationMessage = result.currency_validationMessage
this.imageAddress_validationMessage = result.imageAddress_validationMessage
this.user_validationMessage = result.user_validationMessage
this.created_validationMessage = result.created_validationMessage
this.updated_validationMessage = result.updated_validationMessage
  }
  processUserGroupForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.userGroupValidationInitializer();
    if(this.userGroup.id==undefined){
       this._userGroupService.createUserGroup(this.userGroup).subscribe((result)=>{
           if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
               this.permissionValidationHide = false;
               this.hideElement = true;
               this.permission_validationMessage = result.permission_validationMessage;
           }else if(result.error!=""){
               this.permissionValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/userGroups']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._userGroupService.updateUserGroup(this.userGroup).subscribe((result)=>{
          if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
               this.permissionValidationHide = false;
               this.hideElement = true;
               this.permission_validationMessage = result.permission_validationMessage;
          }else if(result.error!=""){
               this.permissionValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/userGroups']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    onImageAddressFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.imageAddressHide = false;
                
                this.userGroup.imageAddress = reader.result;
            };
        }
    }
    clearimageAddressFile() {
        this.userGroup.imageAddress = '';
        this.imageAddressHide = true;
    }

}
