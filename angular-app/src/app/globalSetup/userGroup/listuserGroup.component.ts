import { Component, OnInit } from '@angular/core';
import { UserGroupService } from '../../services/userGroup.service';
import { UserGroup }  from '../../model/userGroup';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listuserGroup',
  templateUrl: './listuserGroup.component.html'
})
export class ListuserGroupComponent implements OnInit {
  private userGroups:UserGroup[];
  private permission_validationMessage = '';
  private permissionValidationHide = true;
  private pageName = "UserGroup";
  
  constructor(private _userGroupService: UserGroupService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     pageNo = 0;
     size = 5;
     searchToken = '';  
     totalPages = 0;
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.checkPermission();
          this.searchUserGroup();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._userGroupService.getUserGroups().subscribe((result)=>{
      console.log(result);
      this.userGroups=result;
    },(error)=>{
      couserGroupnsole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this._userGroupService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error==undefined || result.error==""){
             this.permissionValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.permissionValidationHide = false;
            this.permission_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedUserGroups(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._userGroupService.getPaginatedUserGroups(pageNo,size).subscribe((result)=>{
      if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
        this.permissionValidationHide = false;
        this.permission_validationMessage = result.permission_validationMessage;
      }else{
          this.permissionValidationHide = true;
          this.userGroups=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchUserGroup(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._userGroupService.getPaginatedUserGroupsWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
          this.permissionValidationHide = false;
          this.permission_validationMessage = result.permission_validationMessage;
      }else{        
          this.permissionValidationHide = true;
          this.userGroups=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedUserGroups(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedUserGroups(this.pageNo,this.size);
      this.searchUserGroup();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedUserGroups(this.pageNo,this.size);
    this.searchUserGroup();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedUserGroups(pageNo,this.size);
    this.searchUserGroup();
  }
  deleteUserGroup(userGroup){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._userGroupService.deleteUserGroup(userGroup.id).subscribe((result)=>{
        if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
            this.permissionValidationHide = false;
            this.permission_validationMessage = result.permission_validationMessage;
        }else {
            this.permissionValidationHide = true;
            this.userGroups.splice(this.userGroups.indexOf(userGroup),1);    
        }
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewUserGroup(userGroup){  
     this._userGroupService.setter(userGroup);
     this._router.navigate(['/globalSetup/userGroups/view']);
   }
   /*editUserGroup(userGroup){  
     this._userGroupService.setter(userGroup);
     this._router.navigate(['/globalSetup/userGroups/userGroupForm']);
   }*/
  editUserGroup(userGroup){  
     this.spinnerService.show();  
     this._userGroupService.getUserGroup(userGroup.id).subscribe((result)=>{
         if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
             this.permissionValidationHide = false;
             this.permission_validationMessage = result.permission_validationMessage;
         }else if(result.userGroup!=null || result.userGroup!=undefined){
             this._userGroupService.setter(result.userGroup);
             this._router.navigate(['/globalSetup/userGroups/userGroupForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     })
     //this._userGroupService.setter(userGroup);
     
   }
   newUserGroup(){
     let userGroup = new UserGroup();
     this._userGroupService.setter(userGroup);
     this._router.navigate(['/globalSetup/userGroups/userGroupForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.userGroups, 'UserGroup');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("UserGroup List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Name", dataKey: "name"},{title: "Meta Data", dataKey: "metaData"},{title: "Website Address", dataKey: "websiteAddress"},{title: "Phone No", dataKey: "phoneNo"},{title: "Email", dataKey: "email"},{title: "Address", dataKey: "address"},{title: "Currency", dataKey: "currency"},{title: "Image Address", dataKey: "imageAddress"},{title: "User", dataKey: "user"},{title: "Created", dataKey: "created"},{title: "Updated", dataKey: "updated"},
    ];
    doc.autoTable(reportColumns, this.userGroups, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('UserGroupList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('UserGroupList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
