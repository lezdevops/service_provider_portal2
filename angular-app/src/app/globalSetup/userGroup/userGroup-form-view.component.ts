import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { UserGroup }  from '../../model/userGroup';
import { Router ,ActivatedRoute} from "@angular/router";
import { UserGroupService } from '../../services/userGroup.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { User }  from '../../model/user';
import { UserService }  from '../../services/user.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-userGroup-view-form',
  templateUrl: './userGroup-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class UserGroupFormViewComponent implements OnInit {
  private userGroup:UserGroup;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private users:User[];  
  
name_validationMessage = "";
metaData_validationMessage = "";
websiteAddress_validationMessage = "";
phoneNo_validationMessage = "";
email_validationMessage = "";
address_validationMessage = "";
currency_validationMessage = "";
imageAddress_validationMessage = "";
user_validationMessage = "";
created_validationMessage = "";
updated_validationMessage = "";
    
  constructor(
    private _userGroupService:UserGroupService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _userService:UserService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.userGroup=this._userGroupService.getter(); 
    this._userService.getUsers().subscribe((users)=>{
   			      this.users=users;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newUserGroup(){
   let userGroup = new UserGroup();
    this._userGroupService.setter(userGroup);
     this._rotuer.navigate(['/globalSetup/userGroups/userGroupForm']);   
   }
  userGroupEditAction(){
     this._userGroupService.setter(this.userGroup);
     this._rotuer.navigate(['/globalSetup/userGroups/userGroupForm']);        
  }
  printUserGroupDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printUserGroupFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('UserGroupList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
