// Angular
import { NgModule } from '@angular/core';
import { UserGroupRoutingModule } from './userGroup-routing.module';
import { ListuserGroupComponent } from './listuserGroup.component';
import { UserGroupFormComponent } from './userGroup-form.component';
import { UserGroupFormViewComponent } from './userGroup-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UserGroupService } from '../../services/userGroup.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { UserService }  from '../../services/user.service';

@NgModule({
  imports: [
    UserGroupRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListuserGroupComponent,
    UserGroupFormComponent,
    UserGroupFormViewComponent
  ],
  providers: [UserGroupService,
    UserService,
    ExcelService  
  ],
})
export class UserGroupModule { }
