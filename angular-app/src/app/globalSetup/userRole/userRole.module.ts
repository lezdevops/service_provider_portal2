// Angular
import { NgModule } from '@angular/core';
import { UserRoleRoutingModule } from './userRole-routing.module';
import { UserRoleComponent } from './userRole.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UserRoleService } from '../../services/userRole.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { UserService }  from '../../services/user.service';
import { RoleService }  from '../../services/role.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

@NgModule({
  imports: [
    UserRoleRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AngularMultiSelectModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    UserRoleComponent
  ],
  providers: [UserRoleService,
    UserService,RoleService,
    ExcelService  
  ],
})
export class UserRoleModule { }
