import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserRoleComponent } from './userRole.component';

const routes: Routes = [
  {    
    path: '',
    component: UserRoleComponent,
    data: {
      title: 'User Role List'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoleRoutingModule {}
