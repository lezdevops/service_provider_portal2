import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { UserRoleService } from '../../services/userRole.service';
import { UserRole }  from '../../model/userRole';
import { Router, ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { User }  from '../../model/user';
import { Role }  from '../../model/role';
import { UserService }  from '../../services/user.service';
import { RoleService }  from '../../services/role.service';

@Component({
  selector: 'my-app',
  templateUrl: './userRole.component.html'
})
export class UserRoleComponent implements OnInit{
  sendEmail:boolean;
  newRole = ""; 
  newUser = "";
  name = 'Angular 5';  
  private users:User[] = [];
  private roles:Role[] = [];
   
  dropdownSettings = {}; 
  constructor(private _userRoleService: UserRoleService,
    private _router: Router,
    private authService: AuthenticationService,
    private _userService: UserService,
    private _roleService: RoleService,
    private excelService: ExcelService,
    private spinnerService: Ng4LoadingSpinnerService
) { }

  removeUser(user){   
    let index = this.users.indexOf(user);
    
    let splicedArray = this.users.splice(index);
    for(var i=1;i<splicedArray.length;i++){
      this.users.push(splicedArray[i]);
    }
  } 
  
  ngOnInit() {

    this.dropdownSettings = {
      singleSelection: false,
      text: "Select UserRoles",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    let ref = this;
      this._userService.getUsers().subscribe((result) => {
          result.forEach(user => {
            let userObj = user;
            let userRoles = [];
            user.roles.forEach(role => {
              role.itemName = role.name;
              userRoles.push(role);
            });
            userObj.roles = userRoles;
            
            ref.users.push(userObj);
          });
      }, (error) => {
          
      });

      this._roleService.getRoles().subscribe((result) => {
          result.roles.forEach(role => {
              role.itemName = role.name;
              role.permissions = null;
              ref.roles.push(role);
          });
      }, (error) => {
          
      });
  }

  onItemSelect(item: any) {
    console.log(item["itemName"]);
    console.log(this.users);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.users);
  }
  onSelectAll(items: any) {
    console.log(this.users);
  }
  onDeSelectAll(items: any) {
    console.log(this.users);
  }
  saveMultipleUserData(){
    this._userService.saveMultipleUser(this.users).subscribe((result)=>{
        if(result.error!=""){            
        }else{
           //this._rotuer.navigate(['/globalSetup/users']);
        }      
        this.spinnerService.hide();   
    },(error)=>{
      console.log(error);
        //this._rotuer.navigate(['/124']);
    });

  }

}
