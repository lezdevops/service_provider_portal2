import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { RegistrationQueue }  from '../../model/registrationQueue';
import { Router ,ActivatedRoute} from "@angular/router";
import { RegistrationQueueService } from '../../services/registrationQueue.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';




export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-registrationQueue-view-form',
  templateUrl: './registrationQueue-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class RegistrationQueueFormViewComponent implements OnInit {
  private registrationQueue:RegistrationQueue;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
    
  
email_validationMessage = "";
userName_validationMessage = "";
password_validationMessage = "";
securityNumber_validationMessage = "";
securityQuestion_validationMessage = "";
answer_validationMessage = "";
accountType_validationMessage = "";
jsonString_validationMessage = "";
createdDate_validationMessage = "";
isApproved_validationMessage = "";
    
  constructor(
    private _registrationQueueService:RegistrationQueueService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.registrationQueue=this._registrationQueueService.getter(); 
      
  }
    
  newRegistrationQueue(){
   let registrationQueue = new RegistrationQueue();
    this._registrationQueueService.setter(registrationQueue);
     this._rotuer.navigate(['/globalSetup/registrationQueues/registrationQueueForm']);   
   }
  registrationQueueEditAction(){
     this._registrationQueueService.setter(this.registrationQueue);
     this._rotuer.navigate(['/globalSetup/registrationQueues/registrationQueueForm']);        
  }
  printRegistrationQueueDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printRegistrationQueueFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('RegistrationQueueList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
