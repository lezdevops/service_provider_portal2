// Angular
import { NgModule } from '@angular/core';
import { RegistrationQueueRoutingModule } from './registrationQueue-routing.module';
import { ListregistrationQueueComponent } from './listregistrationQueue.component';
import { RegistrationQueueFormComponent } from './registrationQueue-form.component';
import { RegistrationQueueFormViewComponent } from './registrationQueue-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RegistrationQueueService } from '../../services/registrationQueue.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';



@NgModule({
  imports: [
    RegistrationQueueRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListregistrationQueueComponent,
    RegistrationQueueFormComponent,
    RegistrationQueueFormViewComponent
  ],
  providers: [RegistrationQueueService,
    
    ExcelService  
  ],
})
export class RegistrationQueueModule { }
