import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { RegistrationQueue }  from '../../model/registrationQueue';
import { Router ,ActivatedRoute } from "@angular/router";
import { RegistrationQueueService } from '../../services/registrationQueue.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-registrationQueue-form',
  templateUrl: './registrationQueue-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class RegistrationQueueFormComponent implements OnInit {
  private registrationQueue:RegistrationQueue;
  
    
  hideElement = true;
  permissionValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
    
  
email_validationMessage = "";
userName_validationMessage = "";
password_validationMessage = "";
securityNumber_validationMessage = "";
securityQuestion_validationMessage = "";
answer_validationMessage = "";
accountType_validationMessage = "";
jsonString_validationMessage = "";
createdDate_validationMessage = "";
approvalStatus_validationMessage = "";
  permission_validationMessage = '';
    
  constructor(
    private _registrationQueueService:RegistrationQueueService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.registrationQueue=this._registrationQueueService.getter();
    this.registrationQueueValidationInitializer();  
      
  }
  registrationQueueValidationInitializer(){
     
this.email_validationMessage = "";
this.userName_validationMessage = "";
this.password_validationMessage = "";
this.securityNumber_validationMessage = "";
this.securityQuestion_validationMessage = "";
this.answer_validationMessage = "";
this.accountType_validationMessage = "";
this.jsonString_validationMessage = "";
this.createdDate_validationMessage = "";
this.approvalStatus_validationMessage = "";
  }
  clearRegistrationQueueFormFields(){
     this.registrationQueue=this._registrationQueueService.getter();   
  }
  validationMessages(result){  
     
this.email_validationMessage = result.email_validationMessage
this.userName_validationMessage = result.userName_validationMessage
this.password_validationMessage = result.password_validationMessage
this.securityNumber_validationMessage = result.securityNumber_validationMessage
this.securityQuestion_validationMessage = result.securityQuestion_validationMessage
this.answer_validationMessage = result.answer_validationMessage
this.accountType_validationMessage = result.accountType_validationMessage
this.jsonString_validationMessage = result.jsonString_validationMessage
this.createdDate_validationMessage = result.createdDate_validationMessage
this.approvalStatus_validationMessage = result.approvalStatus_validationMessage
  }
  processRegistrationQueueForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.registrationQueueValidationInitializer();
    if(this.registrationQueue.id==undefined){
       this._registrationQueueService.createRegistrationQueue(this.registrationQueue).subscribe((result)=>{
           if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
               this.permissionValidationHide = false;
               this.hideElement = true;
               this.permission_validationMessage = result.permission_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.permissionValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/registrationQueues']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._registrationQueueService.updateRegistrationQueue(this.registrationQueue).subscribe((result)=>{
          if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
               this.permissionValidationHide = false;
               this.hideElement = true;
               this.permission_validationMessage = result.permission_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.permissionValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/registrationQueues']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
