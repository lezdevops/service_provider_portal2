import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListregistrationQueueComponent } from './listregistrationQueue.component';
import { RegistrationQueueFormComponent } from './registrationQueue-form.component';
import { RegistrationQueueFormViewComponent } from './registrationQueue-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListregistrationQueueComponent,
    data: {
      title: 'Registration Queue List'
    }
  },{
      path: 'registrationQueueForm',
    component: RegistrationQueueFormComponent,
    data: {
      title: 'Registration Queue Form'
    }
  },{
      path: 'view',
    component: RegistrationQueueFormViewComponent,
    data: {
      title: 'Registration Queue Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationQueueRoutingModule {}
