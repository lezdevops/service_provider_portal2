import { Component, OnInit } from '@angular/core';
import { RegistrationQueueService } from '../../services/registrationQueue.service';
import { RegistrationQueue }  from '../../model/registrationQueue';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listregistrationQueue',
  templateUrl: './listregistrationQueue.component.html'
})
export class ListregistrationQueueComponent implements OnInit {
  private registrationQueues:RegistrationQueue[];
  private permission_validationMessage = '';
  private permissionValidationHide = true;
  private pageName = "RegistrationQueue";
  
  constructor(private _registrationQueueService: RegistrationQueueService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     pageNo = 0;
     size = 5;
     searchToken = '';  
     totalPages = 0;
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.checkPermission();
          this.searchRegistrationQueue();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._registrationQueueService.getRegistrationQueues().subscribe((result)=>{
      console.log(result);
      this.registrationQueues=result;
    },(error)=>{
      coregistrationQueuensole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this._registrationQueueService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error==undefined || result.error==""){
             this.permissionValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.permissionValidationHide = false;
            this.permission_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedRegistrationQueues(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._registrationQueueService.getPaginatedRegistrationQueues(pageNo,size).subscribe((result)=>{
      if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
        this.permissionValidationHide = false;
        this.permission_validationMessage = result.permission_validationMessage;
      }else{
          this.permissionValidationHide = true;
          this.registrationQueues=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchRegistrationQueue(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._registrationQueueService.getPaginatedRegistrationQueuesWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
          this.permissionValidationHide = false;
          this.permission_validationMessage = result.permission_validationMessage;
      }else{        
          this.permissionValidationHide = true;
          this.registrationQueues=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedRegistrationQueues(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedRegistrationQueues(this.pageNo,this.size);
      this.searchRegistrationQueue();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedRegistrationQueues(this.pageNo,this.size);
    this.searchRegistrationQueue();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedRegistrationQueues(pageNo,this.size);
    this.searchRegistrationQueue();
  }
  deleteRegistrationQueue(registrationQueue){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._registrationQueueService.deleteRegistrationQueue(registrationQueue.id).subscribe((result)=>{
        if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
            this.permissionValidationHide = false;
            this.permission_validationMessage = result.permission_validationMessage;
        }else {
            this.permissionValidationHide = true;
            this.registrationQueues.splice(this.registrationQueues.indexOf(registrationQueue),1);    
        }
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }
    
  approveRegistrationQueue(registrationQueue){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
        
    this._registrationQueueService.approveRegistrationQueue(registrationQueue).subscribe((result)=>{
        if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
            this.permissionValidationHide = false;
            this.permission_validationMessage = result.permission_validationMessage;
            this.spinnerService.hide();
        }else {
            this.permissionValidationHide = true;
            this.searchRegistrationQueue();    
        }
        
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewRegistrationQueue(registrationQueue){  
     this._registrationQueueService.setter(registrationQueue);
     this._router.navigate(['/globalSetup/registrationQueues/view']);
   }
   /*editRegistrationQueue(registrationQueue){  
     this._registrationQueueService.setter(registrationQueue);
     this._router.navigate(['/globalSetup/registrationQueues/registrationQueueForm']);
   }*/
  editRegistrationQueue(registrationQueue){  
     this.spinnerService.show();  
     this._registrationQueueService.getRegistrationQueue(registrationQueue.id).subscribe((result)=>{
         if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
             this.permissionValidationHide = false;
             this.permission_validationMessage = result.permission_validationMessage;
         }else if(result.registrationQueue!=null || result.registrationQueue!=undefined){
             this._registrationQueueService.setter(result.registrationQueue);
             this._router.navigate(['/globalSetup/registrationQueues/registrationQueueForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     })
     //this._registrationQueueService.setter(registrationQueue);
     
   }
   newRegistrationQueue(){
     let registrationQueue = new RegistrationQueue();
     this._registrationQueueService.setter(registrationQueue);
     this._router.navigate(['/globalSetup/registrationQueues/registrationQueueForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.registrationQueues, 'RegistrationQueue');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("RegistrationQueue List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Email", dataKey: "email"},{title: "User Name", dataKey: "userName"},{title: "Password", dataKey: "password"},{title: "Security Number", dataKey: "securityNumber"},{title: "Security Question", dataKey: "securityQuestion"},{title: "Answer", dataKey: "answer"},{title: "Account Type", dataKey: "accountType"},{title: "Json String", dataKey: "jsonString"},{title: "Created Date", dataKey: "createdDate"},{title: "Approval Status", dataKey: "approvalStatus"},
    ];
    doc.autoTable(reportColumns, this.registrationQueues, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('RegistrationQueueList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('RegistrationQueueList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
