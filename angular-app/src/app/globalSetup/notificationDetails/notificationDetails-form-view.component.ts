import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationDetails }  from '../../model/notificationDetails';
import { Router ,ActivatedRoute} from "@angular/router";
import { NotificationDetailsService } from '../../services/notificationDetails.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { Notification }  from '../../model/notification';
import { NotificationService }  from '../../services/notification.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-notificationDetails-view-form',
  templateUrl: './notificationDetails-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class NotificationDetailsFormViewComponent implements OnInit {
  private notificationDetails:NotificationDetails;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private notifications:Notification[];  
  
Notification_validationMessage = "";
senderId_validationMessage = "";
receiverId_validationMessage = "";
isRead_validationMessage = "";
    
  constructor(
    private _notificationDetailsService:NotificationDetailsService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _notificationService:NotificationService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.notificationDetails=this._notificationDetailsService.getter(); 
    this._notificationService.getNotifications().subscribe((result)=>{
   			      this.notifications=result.notifications;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newNotificationDetails(){
   let notificationDetails = new NotificationDetails();
    this._notificationDetailsService.setter(notificationDetails);
     this._rotuer.navigate(['/globalSetup/notificationDetailss/notificationDetailsForm']);   
   }
  notificationDetailsEditAction(){
     this._notificationDetailsService.setter(this.notificationDetails);
     this._rotuer.navigate(['/globalSetup/notificationDetailss/notificationDetailsForm']);        
  }
  printNotificationDetailsDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printNotificationDetailsFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('NotificationDetailsList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
