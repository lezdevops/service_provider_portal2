import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListnotificationDetailsComponent } from './listnotificationDetails.component';
import { NotificationDetailsFormComponent } from './notificationDetails-form.component';
import { NotificationDetailsFormViewComponent } from './notificationDetails-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListnotificationDetailsComponent,
    data: {
      title: 'Notification Details List'
    }
  },{
      path: 'notificationDetailsForm',
    component: NotificationDetailsFormComponent,
    data: {
      title: 'Notification Details Form'
    }
  },{
      path: 'view',
    component: NotificationDetailsFormViewComponent,
    data: {
      title: 'Notification Details Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationDetailsRoutingModule {}
