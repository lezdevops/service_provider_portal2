// Angular
import { NgModule } from '@angular/core';
import { NotificationDetailsRoutingModule } from './notificationDetails-routing.module';
import { ListnotificationDetailsComponent } from './listnotificationDetails.component';
import { NotificationDetailsFormComponent } from './notificationDetails-form.component';
import { NotificationDetailsFormViewComponent } from './notificationDetails-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NotificationDetailsService } from '../../services/notificationDetails.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { NotificationService }  from '../../services/notification.service';

@NgModule({
  imports: [
    NotificationDetailsRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListnotificationDetailsComponent,
    NotificationDetailsFormComponent,
    NotificationDetailsFormViewComponent
  ],
  providers: [NotificationDetailsService,
    NotificationService,
    ExcelService  
  ],
})
export class NotificationDetailsModule { }
