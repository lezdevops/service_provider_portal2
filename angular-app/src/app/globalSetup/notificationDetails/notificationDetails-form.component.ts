import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationDetails }  from '../../model/notificationDetails';
import { Router ,ActivatedRoute } from "@angular/router";
import { NotificationDetailsService } from '../../services/notificationDetails.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Notification }  from '../../model/notification';
import { NotificationService }  from '../../services/notification.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-notificationDetails-form',
  templateUrl: './notificationDetails-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class NotificationDetailsFormComponent implements OnInit {
  private notificationDetails:NotificationDetails;
  
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private notifications:Notification[];  
  
Notification_validationMessage = "";
senderId_validationMessage = "";
receiverId_validationMessage = "";
isRead_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _notificationDetailsService:NotificationDetailsService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _notificationService:NotificationService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.notificationDetails=this._notificationDetailsService.getter();
    this.notificationDetailsValidationInitializer();  
    this._notificationService.getNotifications().subscribe((result)=>{
   			      this.notifications=result.notifications;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  notificationDetailsValidationInitializer(){
     
this.Notification_validationMessage = "";
this.senderId_validationMessage = "";
this.receiverId_validationMessage = "";
this.isRead_validationMessage = "";
  }
  clearNotificationDetailsFormFields(){
     this.notificationDetails=this._notificationDetailsService.getNew();   
  }
  validationMessages(result){  
     
this.Notification_validationMessage = result.Notification_validationMessage
this.senderId_validationMessage = result.senderId_validationMessage
this.receiverId_validationMessage = result.receiverId_validationMessage
this.isRead_validationMessage = result.isRead_validationMessage
  }
  processNotificationDetailsForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.notificationDetailsValidationInitializer();
    if(this.notificationDetails.id==undefined){
       this._notificationDetailsService.createNotificationDetails(this.notificationDetails).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/notificationDetailss']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._notificationDetailsService.updateNotificationDetails(this.notificationDetails).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/notificationDetailss']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
