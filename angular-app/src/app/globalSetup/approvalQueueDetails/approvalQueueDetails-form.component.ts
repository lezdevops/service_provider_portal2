import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApprovalQueueDetails }  from '../../model/approvalQueueDetails';
import { Router ,ActivatedRoute } from "@angular/router";
import { ApprovalQueueDetailsService } from '../../services/approvalQueueDetails.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ApprovalQueue }  from '../../model/approvalQueue';
import { ApprovalQueueService }  from '../../services/approvalQueue.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-approvalQueueDetails-form',
  templateUrl: './approvalQueueDetails-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ApprovalQueueDetailsFormComponent implements OnInit {
  private approvalQueueDetails:ApprovalQueueDetails;
  
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private approvalQueues:ApprovalQueue[];  
  
approvalQueue_validationMessage = "";
approverId_validationMessage = "";
level_validationMessage = "";
isLastApprover_validationMessage = "";
isApproved_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _approvalQueueDetailsService:ApprovalQueueDetailsService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _approvalQueueService:ApprovalQueueService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.approvalQueueDetails=this._approvalQueueDetailsService.getter();
    this.approvalQueueDetailsValidationInitializer();  
    this._approvalQueueService.getApprovalQueues().subscribe((result)=>{
   			      this.approvalQueues=result.approvalQueues;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  approvalQueueDetailsValidationInitializer(){
     
this.approvalQueue_validationMessage = "";
this.approverId_validationMessage = "";
this.level_validationMessage = "";
this.isLastApprover_validationMessage = "";
this.isApproved_validationMessage = "";
  }
  clearApprovalQueueDetailsFormFields(){
     this.approvalQueueDetails=this._approvalQueueDetailsService.getNew();   
  }
  validationMessages(result){  
     
this.approvalQueue_validationMessage = result.approvalQueue_validationMessage
this.approverId_validationMessage = result.approverId_validationMessage
this.level_validationMessage = result.level_validationMessage
this.isLastApprover_validationMessage = result.isLastApprover_validationMessage
this.isApproved_validationMessage = result.isApproved_validationMessage
  }
  processApprovalQueueDetailsForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.approvalQueueDetailsValidationInitializer();
    if(this.approvalQueueDetails.id==undefined){
       this._approvalQueueDetailsService.createApprovalQueueDetails(this.approvalQueueDetails).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/approvalQueueDetailss']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._approvalQueueDetailsService.updateApprovalQueueDetails(this.approvalQueueDetails).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/approvalQueueDetailss']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
