import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApprovalQueueDetails }  from '../../model/approvalQueueDetails';
import { Router ,ActivatedRoute} from "@angular/router";
import { ApprovalQueueDetailsService } from '../../services/approvalQueueDetails.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { ApprovalQueue }  from '../../model/approvalQueue';
import { ApprovalQueueService }  from '../../services/approvalQueue.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-approvalQueueDetails-view-form',
  templateUrl: './approvalQueueDetails-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ApprovalQueueDetailsFormViewComponent implements OnInit {
  private approvalQueueDetails:ApprovalQueueDetails;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private approvalQueues:ApprovalQueue[];  
  
approvalQueue_validationMessage = "";
approverId_validationMessage = "";
level_validationMessage = "";
isLastApprover_validationMessage = "";
isApproved_validationMessage = "";
    
  constructor(
    private _approvalQueueDetailsService:ApprovalQueueDetailsService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _approvalQueueService:ApprovalQueueService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.approvalQueueDetails=this._approvalQueueDetailsService.getter(); 
    this._approvalQueueService.getApprovalQueues().subscribe((result)=>{
   			      this.approvalQueues=result.approvalQueues;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newApprovalQueueDetails(){
   let approvalQueueDetails = new ApprovalQueueDetails();
    this._approvalQueueDetailsService.setter(approvalQueueDetails);
     this._rotuer.navigate(['/globalSetup/approvalQueueDetailss/approvalQueueDetailsForm']);   
   }
  approvalQueueDetailsEditAction(){
     this._approvalQueueDetailsService.setter(this.approvalQueueDetails);
     this._rotuer.navigate(['/globalSetup/approvalQueueDetailss/approvalQueueDetailsForm']);        
  }
  printApprovalQueueDetailsDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printApprovalQueueDetailsFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ApprovalQueueDetailsList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
