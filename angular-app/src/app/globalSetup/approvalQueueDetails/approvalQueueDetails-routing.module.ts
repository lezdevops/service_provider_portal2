import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListapprovalQueueDetailsComponent } from './listapprovalQueueDetails.component';
import { ApprovalQueueDetailsFormComponent } from './approvalQueueDetails-form.component';
import { ApprovalQueueDetailsFormViewComponent } from './approvalQueueDetails-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListapprovalQueueDetailsComponent,
    data: {
      title: 'Approval Queue Details List'
    }
  },{
      path: 'approvalQueueDetailsForm',
    component: ApprovalQueueDetailsFormComponent,
    data: {
      title: 'Approval Queue Details Form'
    }
  },{
      path: 'view',
    component: ApprovalQueueDetailsFormViewComponent,
    data: {
      title: 'Approval Queue Details Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApprovalQueueDetailsRoutingModule {}
