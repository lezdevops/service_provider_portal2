import { Component, OnInit } from '@angular/core';
import { ApprovalQueueDetailsService } from '../../services/approvalQueueDetails.service';
import { ApprovalQueueDetails }  from '../../model/approvalQueueDetails';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listapprovalQueueDetails',
  templateUrl: './listapprovalQueueDetails.component.html'
})
export class ListapprovalQueueDetailsComponent implements OnInit {
  private approvalQueueDetailss:ApprovalQueueDetails[];
  private common_validationMessage = '';
  private commonValidationHide = true;
  private pageName = "APPROVALQUEUEDETAILS";
    
  private firstInactivePages = [];
  private activePages = [];
  private lastInactivePages = [];
  lastPage = false;
  firstPage =false;
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  
  constructor(private _approvalQueueDetailsService: ApprovalQueueDetailsService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.checkPermission();
          this.searchApprovalQueueDetails();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._approvalQueueDetailsService.getApprovalQueueDetailss().subscribe((result)=>{
      console.log(result);
      this.approvalQueueDetailss=result;
    },(error)=>{
      coapprovalQueueDetailsnsole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this.authService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error==undefined || result.error==""){
             this.commonValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.commonValidationHide = false;
            this.common_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedApprovalQueueDetailss(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._approvalQueueDetailsService.getPaginatedApprovalQueueDetailss(pageNo,size).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
        this.commonValidationHide = false;
        this.common_validationMessage = result.common_validationMessage;
      }else{
          this.commonValidationHide = true;
          this.approvalQueueDetailss=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
          this.lastPage = result.paginatedResult.last;
          this.firstPage = result.paginatedResult.first;
          this.pageNo = result.paginatedResult.number; 
          
          this.firstInactivePages = [];
          this.activePages = [];
          this.lastInactivePages = [];
          
          var i=0;
          for(i=0;i<this.pageNo;i++){
            this.firstInactivePages.push(i+1);
          }
          
           this.activePages.push(this.pageNo+1);
          
          for(i=this.pageNo+1;i<this.totalPages;i++){
            this.lastInactivePages.push(i+1);
          }
          
          //console.log(this.firstInactivePages);
          //console.log(this.activePages);
          //console.log(this.lastInactivePages);
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchApprovalQueueDetails(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._approvalQueueDetailsService.getPaginatedApprovalQueueDetailssWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
          this.common_validationMessage = result.common_validationMessage;
      }else{        
          this.commonValidationHide = true;
          this.approvalQueueDetailss=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedApprovalQueueDetailss(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedApprovalQueueDetailss(this.pageNo,this.size);
      this.searchApprovalQueueDetails();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedApprovalQueueDetailss(this.pageNo,this.size);
    this.searchApprovalQueueDetails();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedApprovalQueueDetailss(pageNo,this.size);
    this.searchApprovalQueueDetails();
  }
  deleteApprovalQueueDetails(approvalQueueDetails){
    if(confirm("Are you sure?")){
        this.spinnerService.show();
        setTimeout(()=>this.spinnerService.hide(),10000);
        this._approvalQueueDetailsService.deleteApprovalQueueDetails(approvalQueueDetails.id).subscribe((result)=>{
            if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.common_validationMessage;
            }else if(result.technicalError!=undefined && result.technicalError!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.technicalError;
            }else {
                this.commonValidationHide = true;
                this.approvalQueueDetailss.splice(this.approvalQueueDetailss.indexOf(approvalQueueDetails),1);    
            }
            this.spinnerService.hide();
        },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
        });
    }
    
  }

   viewApprovalQueueDetails(approvalQueueDetails){  
      
    this._approvalQueueDetailsService.getApprovalQueueDetails(approvalQueueDetails.id).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
              this.common_validationMessage = result.common_validationMessage;
          }else if(result.approvalQueueDetails!=null || result.approvalQueueDetails!=undefined){
              let approvalQueueDetails = result.approvalQueueDetails;
             
             this._approvalQueueDetailsService.setter(approvalQueueDetails);
              this._router.navigate(['/globalSetup/approvalQueueDetailss/view']);
          }
          this.spinnerService.hide();
      },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
      });
     
   }
   /*editApprovalQueueDetails(approvalQueueDetails){  
     this._approvalQueueDetailsService.setter(approvalQueueDetails);
     this._router.navigate(['/globalSetup/approvalQueueDetailss/approvalQueueDetailsForm']);
   }*/
  editApprovalQueueDetails(approvalQueueDetails){  
     this.spinnerService.show();  
     this._approvalQueueDetailsService.getApprovalQueueDetails(approvalQueueDetails.id).subscribe((result)=>{
         if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
             this.commonValidationHide = false;
             this.common_validationMessage = result.common_validationMessage;
         }else if(result.approvalQueueDetails!=null || result.approvalQueueDetails!=undefined){
             let approvalQueueDetails = result.approvalQueueDetails;
             
             this._approvalQueueDetailsService.setter(approvalQueueDetails);
             this._router.navigate(['/globalSetup/approvalQueueDetailss/approvalQueueDetailsForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     });
     //this._approvalQueueDetailsService.setter(approvalQueueDetails);
     
   }
   newApprovalQueueDetails(){
     let approvalQueueDetails = new ApprovalQueueDetails();
     this._approvalQueueDetailsService.setter(approvalQueueDetails);
     this._router.navigate(['/globalSetup/approvalQueueDetailss/approvalQueueDetailsForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.approvalQueueDetailss, 'ApprovalQueueDetails');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("ApprovalQueueDetails List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Approval Queue", dataKey: "approvalQueue"},{title: "Approver Id", dataKey: "approverId"},{title: "Level", dataKey: "level"},{title: "Is Last Approver", dataKey: "isLastApprover"},{title: "Is Approved", dataKey: "isApproved"},
    ];
    doc.autoTable(reportColumns, this.approvalQueueDetailss, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('ApprovalQueueDetailsList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ApprovalQueueDetailsList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
