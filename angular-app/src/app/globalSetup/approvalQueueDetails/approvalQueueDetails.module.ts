// Angular
import { NgModule } from '@angular/core';
import { ApprovalQueueDetailsRoutingModule } from './approvalQueueDetails-routing.module';
import { ListapprovalQueueDetailsComponent } from './listapprovalQueueDetails.component';
import { ApprovalQueueDetailsFormComponent } from './approvalQueueDetails-form.component';
import { ApprovalQueueDetailsFormViewComponent } from './approvalQueueDetails-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ApprovalQueueDetailsService } from '../../services/approvalQueueDetails.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ApprovalQueueService }  from '../../services/approvalQueue.service';

@NgModule({
  imports: [
    ApprovalQueueDetailsRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListapprovalQueueDetailsComponent,
    ApprovalQueueDetailsFormComponent,
    ApprovalQueueDetailsFormViewComponent
  ],
  providers: [ApprovalQueueDetailsService,
    ApprovalQueueService,
    ExcelService  
  ],
})
export class ApprovalQueueDetailsModule { }
