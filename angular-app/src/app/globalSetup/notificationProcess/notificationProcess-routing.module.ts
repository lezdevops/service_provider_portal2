import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListnotificationProcessComponent } from './listnotificationProcess.component';
import { NotificationProcessFormComponent } from './notificationProcess-form.component';
import { NotificationProcessFormViewComponent } from './notificationProcess-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListnotificationProcessComponent,
    data: {
      title: 'Notification Process List'
    }
  },{
      path: 'notificationProcessForm',
    component: NotificationProcessFormComponent,
    data: {
      title: 'Notification Process Form'
    }
  },{
      path: 'view',
    component: NotificationProcessFormViewComponent,
    data: {
      title: 'Notification Process Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationProcessRoutingModule {}
