// Angular
import { NgModule } from '@angular/core';
import { NotificationProcessRoutingModule } from './notificationProcess-routing.module';
import { ListnotificationProcessComponent } from './listnotificationProcess.component';
import { NotificationProcessFormComponent } from './notificationProcess-form.component';
import { NotificationProcessFormViewComponent } from './notificationProcess-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NotificationProcessService } from '../../services/notificationProcess.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';



@NgModule({
  imports: [
    NotificationProcessRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListnotificationProcessComponent,
    NotificationProcessFormComponent,
    NotificationProcessFormViewComponent
  ],
  providers: [NotificationProcessService,
    
    ExcelService  
  ],
})
export class NotificationProcessModule { }
