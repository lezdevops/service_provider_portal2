import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationProcess }  from '../../model/notificationProcess';
import { Router ,ActivatedRoute} from "@angular/router";
import { NotificationProcessService } from '../../services/notificationProcess.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';




export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-notificationProcess-view-form',
  templateUrl: './notificationProcess-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class NotificationProcessFormViewComponent implements OnInit {
  private notificationProcess:NotificationProcess;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
    
  
name_validationMessage = "";
isActive_validationMessage = "";
sendEmail_validationMessage = "";
sendSms_validationMessage = "";
    
  constructor(
    private _notificationProcessService:NotificationProcessService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.notificationProcess=this._notificationProcessService.getter(); 
      
  }
    
  newNotificationProcess(){
   let notificationProcess = new NotificationProcess();
    this._notificationProcessService.setter(notificationProcess);
     this._rotuer.navigate(['/globalSetup/notificationProcesss/notificationProcessForm']);   
   }
  notificationProcessEditAction(){
     this._notificationProcessService.setter(this.notificationProcess);
     this._rotuer.navigate(['/globalSetup/notificationProcesss/notificationProcessForm']);        
  }
  printNotificationProcessDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printNotificationProcessFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('NotificationProcessList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
