import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationProcess }  from '../../model/notificationProcess';
import { Router ,ActivatedRoute } from "@angular/router";
import { NotificationProcessService } from '../../services/notificationProcess.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-notificationProcess-form',
  templateUrl: './notificationProcess-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class NotificationProcessFormComponent implements OnInit {
  private notificationProcess:NotificationProcess;
  
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
    
  
name_validationMessage = "";
isActive_validationMessage = "";
sendEmail_validationMessage = "";
sendSms_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _notificationProcessService:NotificationProcessService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.notificationProcess=this._notificationProcessService.getter();
    this.notificationProcessValidationInitializer();  
      
  }
  notificationProcessValidationInitializer(){
     
this.name_validationMessage = "";
this.isActive_validationMessage = "";
this.sendEmail_validationMessage = "";
this.sendSms_validationMessage = "";
  }
  clearNotificationProcessFormFields(){
     this.notificationProcess=this._notificationProcessService.getNew();   
  }
  validationMessages(result){  
     
this.name_validationMessage = result.name_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
this.sendEmail_validationMessage = result.sendEmail_validationMessage
this.sendSms_validationMessage = result.sendSms_validationMessage
  }
  processNotificationProcessForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.notificationProcessValidationInitializer();
    if(this.notificationProcess.id==undefined){
       this._notificationProcessService.createNotificationProcess(this.notificationProcess).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/notificationProcesss']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._notificationProcessService.updateNotificationProcess(this.notificationProcess).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/notificationProcesss']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
