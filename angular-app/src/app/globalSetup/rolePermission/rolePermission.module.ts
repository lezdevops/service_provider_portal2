// Angular
import { NgModule } from '@angular/core';
import { RolePermissionRoutingModule } from './rolePermission-routing.module';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';
import { RolePermissionComponent } from './rolePermission.component';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RolePermissionService } from '../../services/rolePermission.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

import { RoleService }  from '../../services/role.service';import { PermissionService }  from '../../services/permission.service';

@NgModule({
  imports: [
    RolePermissionRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AngularMultiSelectModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    RolePermissionComponent
  ],
  providers: [RolePermissionService,
    RoleService,PermissionService,
    ExcelService  
  ],
})
export class RolePermissionModule { }
