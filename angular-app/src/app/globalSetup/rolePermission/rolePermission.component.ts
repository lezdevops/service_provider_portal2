import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { RolePermissionService } from '../../services/rolePermission.service';
import { RolePermission }  from '../../model/rolePermission';
import { Router, ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Role }  from '../../model/role';
import { Permission }  from '../../model/permission';
import { RoleService }  from '../../services/role.service';
import { PermissionService }  from '../../services/permission.service';

@Component({
  selector: 'my-app',
  templateUrl: './rolePermission.component.html',
  styleUrls: ['./rolePermission.component.css']
})
export class RolePermissionComponent implements OnInit{
  sendEmail:boolean;
  newPermission = ""; 
  newRole = "";
  name = 'Angular 5';  
  private roles:Role[] = [];
  private permissions:Permission[] = [];
   
  dropdownSettings = {}; 
  constructor(private _rolePermissionService: RolePermissionService,
    private _router: Router,
    private authService: AuthenticationService,
    private _roleService: RoleService,
    private _permissionService: PermissionService,
    private excelService: ExcelService,
    private spinnerService: Ng4LoadingSpinnerService
) { }

  removeRole(role){   
    let index = this.roles.indexOf(role);
    
    let splicedArray = this.roles.splice(index);
    for(var i=1;i<splicedArray.length;i++){
      this.roles.push(splicedArray[i]);
    }
  } 
  
  ngOnInit() {

    this.dropdownSettings = {
      singleSelection: false,
      text: "Select RolePermissions",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    let ref = this;
      this._roleService.getRoles().subscribe((result) => {
          result.roles.forEach(role => {
            let roleObj = role;
            let rolePermissions = [];
            role.permissions.forEach(permission => {
              permission.itemName = permission.name;
              rolePermissions.push(permission);
            });
            roleObj.permissions = rolePermissions;
            
            ref.roles.push(roleObj);
          });
      }, (error) => {
          
      });

      this._permissionService.getPermissions().subscribe((result) => {
          result.permissions.forEach(permission => {
              permission.itemName = permission.name;
              ref.permissions.push(permission);
          });
      }, (error) => {
          
      });
  }

  onItemSelect(item: any) {
    console.log(item["itemName"]);
    console.log(this.roles);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.roles);
  }
  onSelectAll(items: any) {
    console.log(this.roles);
  }
  onDeSelectAll(items: any) {
    console.log(this.roles);
  }
  saveMultipleRoleData(){
    this._roleService.saveMultipleRole(this.roles).subscribe((result)=>{
      console.log(result);
        if(result.error!=""){            
        }else{
           //this._rotuer.navigate(['/globalSetup/roles']);
        }      
        this.spinnerService.hide();   
    },(error)=>{
      console.log(error);
        //this._rotuer.navigate(['/124']);
    });

  }

}
