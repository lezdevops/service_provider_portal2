import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RolePermissionComponent } from './rolePermission.component';

const routes: Routes = [
  {    
    path: '',
    component: RolePermissionComponent,
    data: {
      title: 'Role Permission List'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolePermissionRoutingModule {}
