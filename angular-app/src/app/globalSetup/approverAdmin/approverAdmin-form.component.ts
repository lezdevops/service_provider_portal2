import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApproverAdmin }  from '../../model/approverAdmin';
import { Router ,ActivatedRoute } from "@angular/router";
import { ApproverAdminService } from '../../services/approverAdmin.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { User }  from '../../model/user';
import { UserService }  from '../../services/user.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-approverAdmin-form',
  templateUrl: './approverAdmin-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ApproverAdminFormComponent implements OnInit {
  private approverAdmin:ApproverAdmin;
  
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private users:User[];  
  
name_validationMessage = "";
designation_validationMessage = "";
user_validationMessage = "";
isActive_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _approverAdminService:ApproverAdminService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _userService:UserService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.approverAdmin=this._approverAdminService.getter();
    this.approverAdminValidationInitializer();  
    this._userService.getUsers().subscribe((result)=>{
   			      this.users=result;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  approverAdminValidationInitializer(){
     
this.name_validationMessage = "";
this.designation_validationMessage = "";
this.user_validationMessage = "";
this.isActive_validationMessage = "";
  }
  clearApproverAdminFormFields(){
     this.approverAdmin=this._approverAdminService.getNew();   
  }
  validationMessages(result){  
     
this.name_validationMessage = result.name_validationMessage
this.designation_validationMessage = result.designation_validationMessage
this.user_validationMessage = result.user_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
  }
  processApproverAdminForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.approverAdminValidationInitializer();
    if(this.approverAdmin.id==undefined){
       this._approverAdminService.createApproverAdmin(this.approverAdmin).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/approverAdmins']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._approverAdminService.updateApproverAdmin(this.approverAdmin).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/approverAdmins']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
