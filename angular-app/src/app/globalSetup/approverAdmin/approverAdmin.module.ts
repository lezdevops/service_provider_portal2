// Angular
import { NgModule } from '@angular/core';
import { ApproverAdminRoutingModule } from './approverAdmin-routing.module';
import { ListapproverAdminComponent } from './listapproverAdmin.component';
import { ApproverAdminFormComponent } from './approverAdmin-form.component';
import { ApproverAdminFormViewComponent } from './approverAdmin-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ApproverAdminService } from '../../services/approverAdmin.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { UserService }  from '../../services/user.service';

@NgModule({
  imports: [
    ApproverAdminRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListapproverAdminComponent,
    ApproverAdminFormComponent,
    ApproverAdminFormViewComponent
  ],
  providers: [ApproverAdminService,
    UserService,
    ExcelService  
  ],
})
export class ApproverAdminModule { }
