import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListapproverAdminComponent } from './listapproverAdmin.component';
import { ApproverAdminFormComponent } from './approverAdmin-form.component';
import { ApproverAdminFormViewComponent } from './approverAdmin-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListapproverAdminComponent,
    data: {
      title: 'Approver Admin List'
    }
  },{
      path: 'approverAdminForm',
    component: ApproverAdminFormComponent,
    data: {
      title: 'Approver Admin Form'
    }
  },{
      path: 'view',
    component: ApproverAdminFormViewComponent,
    data: {
      title: 'Approver Admin Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApproverAdminRoutingModule {}
