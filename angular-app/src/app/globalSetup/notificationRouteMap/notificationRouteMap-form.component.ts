import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationRouteMap }  from '../../model/notificationRouteMap';
import { Router ,ActivatedRoute } from "@angular/router";
import { NotificationRouteMapService } from '../../services/notificationRouteMap.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NotificationProcess }  from '../../model/notificationProcess';
import { MessageReceiver }  from '../../model/messageReceiver';

import { NotificationProcessService }  from '../../services/notificationProcess.service';
import { MessageReceiverService }  from '../../services/messageReceiver.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-notificationRouteMap-form',
  templateUrl: './notificationRouteMap-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class NotificationRouteMapFormComponent implements OnInit {
  private notificationRouteMap:NotificationRouteMap;
  
  private messageReceivers:MessageReceiver[] = [];
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private notificationProcesss:NotificationProcess[];  
  
NotificationProcess_validationMessage = "";
senderId_validationMessage = "";
receiverId_validationMessage = "";
isActive_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _notificationRouteMapService:NotificationRouteMapService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _messageReceiverService: MessageReceiverService,
    private _notificationProcessService:NotificationProcessService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  dropdownSettings :any;

  ngOnInit() {
      
    this.dropdownSettings = {
      singleSelection: false,
      text: "Select Message Receiver",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };

    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    /*let ref = this;
    this._notificationProcessService.getNotificationProcesss().subscribe((result) => {
        result.notificationProcesss.forEach(notificationProcess => {
          let notificationProcessObj = notificationProcess;
          let notificationProcessProcessApprovers = [];
          notificationProcess.messageReceivers.forEach(messageReceiver => {
            messageReceiver.itemName = messageReceiver.name;
            notificationProcessProcessApprovers.push(messageReceiver);
          });
          notificationProcessObj.messageReceivers = notificationProcessProcessApprovers;
          
          ref.notificationProcesss.push(notificationProcessObj);
        });
    }, (error) => {
        
    });*/

    this.notificationRouteMap=this._notificationRouteMapService.getter();
    
    this.notificationRouteMapValidationInitializer();  
    let ref = this;
    this._notificationProcessService.getNotificationProcesss().subscribe((result)=>{
      this.notificationProcesss=result.notificationProcesss;
    },(error)=>{
      //console.log(error);
    }); 

    this._messageReceiverService.getMessageReceivers().subscribe((result) => {
        result.messageReceivers.forEach(messageReceiver => {
          messageReceiver.itemName = messageReceiver.name;
            ref.messageReceivers.push(messageReceiver);
        });
      }, (error) => {
    }); 
  }
  notificationRouteMapValidationInitializer(){
     
this.NotificationProcess_validationMessage = "";
this.senderId_validationMessage = "";
this.receiverId_validationMessage = "";
this.isActive_validationMessage = "";
  }
  clearNotificationRouteMapFormFields(){
     this.notificationRouteMap=this._notificationRouteMapService.getNew();   
  }
  validationMessages(result){  
     
this.NotificationProcess_validationMessage = result.NotificationProcess_validationMessage
this.senderId_validationMessage = result.senderId_validationMessage
this.receiverId_validationMessage = result.receiverId_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
  }
  processNotificationRouteMapForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.notificationRouteMapValidationInitializer();
    if(this.notificationRouteMap.id==undefined){
       this._notificationRouteMapService.createNotificationRouteMap(this.notificationRouteMap).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/notificationRouteMaps']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._notificationRouteMapService.updateNotificationRouteMap(this.notificationRouteMap).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/notificationRouteMaps']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
