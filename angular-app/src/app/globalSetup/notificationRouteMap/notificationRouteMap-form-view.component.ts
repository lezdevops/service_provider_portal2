import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationRouteMap }  from '../../model/notificationRouteMap';
import { Router ,ActivatedRoute} from "@angular/router";
import { NotificationRouteMapService } from '../../services/notificationRouteMap.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { NotificationProcess }  from '../../model/notificationProcess';
import { NotificationProcessService }  from '../../services/notificationProcess.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-notificationRouteMap-view-form',
  templateUrl: './notificationRouteMap-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class NotificationRouteMapFormViewComponent implements OnInit {
  private notificationRouteMap:NotificationRouteMap;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private notificationProcesss:NotificationProcess[];  
  
NotificationProcess_validationMessage = "";
senderId_validationMessage = "";
receiverId_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _notificationRouteMapService:NotificationRouteMapService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _notificationProcessService:NotificationProcessService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.notificationRouteMap=this._notificationRouteMapService.getter(); 
    this._notificationProcessService.getNotificationProcesss().subscribe((result)=>{
   			      this.notificationProcesss=result.notificationProcesss;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newNotificationRouteMap(){
   let notificationRouteMap = new NotificationRouteMap();
    this._notificationRouteMapService.setter(notificationRouteMap);
     this._rotuer.navigate(['/globalSetup/notificationRouteMaps/notificationRouteMapForm']);   
   }
  notificationRouteMapEditAction(){
     this._notificationRouteMapService.setter(this.notificationRouteMap);
     this._rotuer.navigate(['/globalSetup/notificationRouteMaps/notificationRouteMapForm']);        
  }
  printNotificationRouteMapDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printNotificationRouteMapFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('NotificationRouteMapList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
