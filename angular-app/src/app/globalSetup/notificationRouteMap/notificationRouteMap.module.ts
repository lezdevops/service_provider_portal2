// Angular
import { NgModule } from '@angular/core';
import { NotificationRouteMapRoutingModule } from './notificationRouteMap-routing.module';
import { ListnotificationRouteMapComponent } from './listnotificationRouteMap.component';
import { NotificationRouteMapFormComponent } from './notificationRouteMap-form.component';
import { NotificationRouteMapFormViewComponent } from './notificationRouteMap-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NotificationRouteMapService } from '../../services/notificationRouteMap.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { MessageReceiverService }  from '../../services/messageReceiver.service';
import { NotificationProcessService }  from '../../services/notificationProcess.service';

@NgModule({
  imports: [
    NotificationRouteMapRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AngularMultiSelectModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListnotificationRouteMapComponent,
    NotificationRouteMapFormComponent,
    NotificationRouteMapFormViewComponent
  ],
  providers: [NotificationRouteMapService,
    NotificationProcessService,
    MessageReceiverService,
    ExcelService  
  ],
})
export class NotificationRouteMapModule { }
