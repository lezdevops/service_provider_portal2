import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListnotificationRouteMapComponent } from './listnotificationRouteMap.component';
import { NotificationRouteMapFormComponent } from './notificationRouteMap-form.component';
import { NotificationRouteMapFormViewComponent } from './notificationRouteMap-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListnotificationRouteMapComponent,
    data: {
      title: 'Notification Route Map List'
    }
  },{
      path: 'notificationRouteMapForm',
    component: NotificationRouteMapFormComponent,
    data: {
      title: 'Notification Route Map Form'
    }
  },{
      path: 'view',
    component: NotificationRouteMapFormViewComponent,
    data: {
      title: 'Notification Route Map Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationRouteMapRoutingModule {}
