import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotificationReceiverMapComponent } from './notificationReceiverMap.component';

const routes: Routes = [{
    path: '',
    component: NotificationReceiverMapComponent,
    data: {
      title: 'Notification Receiver Map'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationReceiverMapRoutingModule {}
