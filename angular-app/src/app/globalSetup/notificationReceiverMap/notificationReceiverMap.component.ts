import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { MessageReceiver }  from '../../model/messageReceiver';
import { Router, ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NotificationProcess }  from '../../model/notificationProcess';
import { NotificationProcessService }  from '../../services/notificationProcess.service';
import { MessageReceiverService }  from '../../services/messageReceiver.service';

@Component({
  selector: 'my-app',
  templateUrl: './notificationReceiverMap.component.html'
})
export class NotificationReceiverMapComponent implements OnInit{
  private notificationProcesss:NotificationProcess[] = [];
  private messageReceivers:MessageReceiver[] = [];
   
  dropdownSettings = {};
   
  constructor(private _router: Router,
    private authService: AuthenticationService,
    private _notificationProcessService: NotificationProcessService,
    private _messageReceiverService: MessageReceiverService,
    private excelService: ExcelService,
    private spinnerService: Ng4LoadingSpinnerService
) { }

  
  ngOnInit() {

    this.dropdownSettings = {
      singleSelection: false,
      text: "Select Message Receiver",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    let ref = this;
      this._notificationProcessService.getNotificationProcesss().subscribe((result) => {
          result.notificationProcesss.forEach(notificationProcess => {
            let notificationProcessObj = notificationProcess;
            let notificationProcessProcessApprovers = [];
            notificationProcess.messageReceivers.forEach(messageReceiver => {
              messageReceiver.itemName = messageReceiver.name;
              notificationProcessProcessApprovers.push(messageReceiver);
            });
            notificationProcessObj.messageReceivers = notificationProcessProcessApprovers;
            
            ref.notificationProcesss.push(notificationProcessObj);
          });
      }, (error) => {
          
      });

      this._messageReceiverService.getMessageReceivers().subscribe((result) => {
          result.messageReceivers.forEach(messageReceiver => {
            messageReceiver.itemName = messageReceiver.name;
              ref.messageReceivers.push(messageReceiver);
          });
      }, (error) => {
          
      });
  }

  onItemSelect(item: any) {
    console.log(item["itemName"]);
    console.log(this.notificationProcesss);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.notificationProcesss);
  }
  onSelectAll(items: any) {
    console.log(this.notificationProcesss);
  }
  onDeSelectAll(items: any) {
    console.log(this.notificationProcesss);
  }
  saveMultipleNotificationProcessData(){
    this._notificationProcessService.saveMultipleNotificationProcess(this.notificationProcesss).subscribe((result)=>{
      console.log(result);
        if(result.error!=""){            
        }else{
           //this._rotuer.navigate(['/globalSetup/notificationProcesss']);
        }      
        this.spinnerService.hide();   
    },(error)=>{
      console.log(error);
        //this._rotuer.navigate(['/124']);
    });

  }

}
