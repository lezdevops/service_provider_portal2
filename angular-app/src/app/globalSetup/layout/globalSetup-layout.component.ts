//import { navItems } from './adminPanel_nav';
import { navItems } from './globalSetup_nav';
import { Component, OnInit, NgZone } from '@angular/core';
import { AuthenticationService } from "../../authentication/authentication.service";
import { NotificationDetails }  from '../../model/notificationDetails';
import { ApprovalQueueDetails }  from '../../model/approvalQueueDetails';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './globalSetup-layout.component.html'
})
export class GlobalSetupLayoutComponent {
  private baseUrl:string='http://localhost:8095/api';

  //private baseUrl:string='http://servicesbd.herokuapp.com/api';
  //private publicUrl:string='http://servicesbd.herokuapp.com/public';
  //private rootUrl:string='http://servicesbd.herokuapp.com';
  
  public navItems = navItems;
  public notificationDetailsList = [];
  public approvalQueueDetailsList = [];
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  isLoggedIn: Observable<boolean>;

  private headers = new Headers({ 
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*',
    'agentReferenceid':'1'
  });
    
  constructor(private _http:Http,private authService: AuthenticationService) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
  }
  errorMessage = null;

  ngOnInit() {
    this.isLoggedIn = this.authService.isLoggedIn;
    this.reloadNotifications();
    this.reloadApprovalQueueDetailsList();
  }
  reloadNotifications(){
    this.getUserNotifications().subscribe((result)=>{
      this.notificationDetailsList = result.userNotifications;
    },(error)=>{
      console.log(error);
    });    
  }
  reloadApprovalQueueDetailsList(){
    this.getApprovalQueues().subscribe(result => {
      console.log(result);
      this.approvalQueueDetailsList = result.approvalQueueDetailsList; 
    }, error => this.errorMessage = <any>error);   
  }
  hideNotification(notificationDetails:NotificationDetails){
     let ref = this;
     this.hideNotificationAction(notificationDetails).subscribe((result)=>{
         ref.reloadNotifications();
     });
  } 
  approveApprovalQueueDetails(approvalQueueDetails:ApprovalQueueDetails){
     let ref = this;
     this.approveApprovalQueueDetailsAction(approvalQueueDetails).subscribe((result)=>{
         ref.reloadApprovalQueueDetailsList();
     });
  } 
  hideNotificationAction(notificationDetails: NotificationDetails) {
      notificationDetails.isRead = true;
      this.headers.set('Authorization', this.authService.getToken());
      let options = new RequestOptions({ headers: this.headers });

      return this._http.post(this.baseUrl + '/notificationDetails', JSON.stringify(notificationDetails), options).map((response: Response) => response.json())
          .catch(this.errorHandler);
  }  
  approveApprovalQueueDetailsAction(approvalQueueDetails: ApprovalQueueDetails) {
    approvalQueueDetails.isApproved = true;
      this.headers.set('Authorization', this.authService.getToken());
      let options = new RequestOptions({ headers: this.headers });

      return this._http.post(this.baseUrl + '/approvalQueueDetails', JSON.stringify(approvalQueueDetails), options).map((response: Response) => response.json())
          .catch(this.errorHandler);
  }  

  onLogout(){
    this.authService.logout();
  }
  
  getUserNotifications(){
    this.headers.set('Authorization', this.authService.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/userNotifications',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  getApprovalQueues(){
    this.headers.set('Authorization', this.authService.getToken());
    let options = new RequestOptions({headers:this.headers});

    return this._http.get(this.baseUrl+'/userApprovalQueues',options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  errorHandler(error:Response){
     return Observable.throw(error||"SERVER ERROR");
  }
    

}
