export const navItems = [
  {
    name: 'Dashboard',
    url: '/globalSetup',
    icon: 'icon-speedometer',
    visible:false,
    badge: {
      variant: 'info',
      text: '15'
    }
  },
  {
    divider: true 
  },
  {
    title: true,
    name: 'Generated Components',
  }
,{name: 'User Group',url: '/globalSetup/userGroups',icon: 'icon-pie-chart'}
,{name: 'User',url: '/globalSetup/users',icon: 'icon-pie-chart'}
,{name: 'Role',url: '/globalSetup/roles',icon: 'icon-pie-chart'}
,{name: 'User Role',url: '/globalSetup/userRoles',icon: 'icon-pie-chart'}
,{name: 'Permission',url: '/globalSetup/permissions',icon: 'icon-pie-chart'}
,{name: 'Role Permission',url: '/globalSetup/rolePermissions',icon: 'icon-pie-chart'}
,{name: 'Approval Process',url: '/globalSetup/approvalProcesss',icon: 'icon-pie-chart'}
,{name: 'Process Approver',url: '/globalSetup/processApprovers',icon: 'icon-pie-chart'}
,{name: 'Registration Queue',url: '/globalSetup/registrationQueues',icon: 'icon-pie-chart'}
,{name: 'Notification Process',url: '/globalSetup/notificationProcesss',icon: 'icon-pie-chart'}
,{name: 'Notification Receiver Map',url: '/globalSetup/notificationReceiverMap',icon: 'icon-pie-chart'}
,{name: 'Notification Route Map',url: '/globalSetup/notificationRouteMaps',icon: 'icon-pie-chart'}
,{name: 'Notification',url: '/globalSetup/notifications',icon: 'icon-pie-chart'}
,{name: 'Notification Details',url: '/globalSetup/notificationDetailss',icon: 'icon-pie-chart'}

,{name: 'Approval Queue',url: '/globalSetup/approvalQueues',icon: 'icon-pie-chart'}
,{name: 'Approval Queue Details',url: '/globalSetup/approvalQueueDetailss',icon: 'icon-pie-chart'}
,{name: 'Approver Admin',url: '/globalSetup/approverAdmins',icon: 'icon-pie-chart'}
,{name: 'Message Receiver',url: '/globalSetup/messageReceivers',icon: 'icon-pie-chart'}
// newMenuLinks
];
