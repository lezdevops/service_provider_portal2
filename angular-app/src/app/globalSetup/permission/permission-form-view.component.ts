import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Permission }  from '../../model/permission';
import { Router ,ActivatedRoute} from "@angular/router";
import { PermissionService } from '../../services/permission.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';




export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-permission-view-form',
  templateUrl: './permission-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class PermissionFormViewComponent implements OnInit {
  private permission:Permission;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
    
  
name_validationMessage = "";
createdDate_validationMessage = "";
    
  constructor(
    private _permissionService:PermissionService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.permission=this._permissionService.getter(); 
      
  }
    
  newPermission(){
   let permission = new Permission();
    this._permissionService.setter(permission);
     this._rotuer.navigate(['/globalSetup/permissions/permissionForm']);   
   }
  permissionEditAction(){
     this._permissionService.setter(this.permission);
     this._rotuer.navigate(['/globalSetup/permissions/permissionForm']);        
  }
  printPermissionDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printPermissionFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('PermissionList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
