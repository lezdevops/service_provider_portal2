import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Permission }  from '../../model/permission';
import { Router ,ActivatedRoute } from "@angular/router";
import { PermissionService } from '../../services/permission.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-permission-form',
  templateUrl: './permission-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class PermissionFormComponent implements OnInit {
  private permission:Permission;
  
    
  hideElement = true;
  permissionValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
    
  
name_validationMessage = "";
createdDate_validationMessage = "";
  permission_validationMessage = '';
    
  constructor(
    private _permissionService:PermissionService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.permission=this._permissionService.getter();
    this.permissionValidationInitializer();  
      
  }
  permissionValidationInitializer(){
     
this.name_validationMessage = "";
this.createdDate_validationMessage = "";
  }
  clearPermissionFormFields(){
     this.permission=this._permissionService.getter();   
  }
  validationMessages(result){  
     
this.name_validationMessage = result.name_validationMessage
this.createdDate_validationMessage = result.createdDate_validationMessage
  }
  processPermissionForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.permissionValidationInitializer();
    if(this.permission.id==undefined){
       this._permissionService.createPermission(this.permission).subscribe((result)=>{
           if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
               this.permissionValidationHide = false;
               this.hideElement = true;
               this.permission_validationMessage = result.permission_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.permissionValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/permissions']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._permissionService.updatePermission(this.permission).subscribe((result)=>{
          if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
               this.permissionValidationHide = false;
               this.hideElement = true;
               this.permission_validationMessage = result.permission_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.permissionValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/permissions']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
