import { Component, OnInit } from '@angular/core';
import { PermissionService } from '../../services/permission.service';
import { Permission }  from '../../model/permission';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listpermission',
  templateUrl: './listpermission.component.html'
})
export class ListpermissionComponent implements OnInit {
  private permissions:Permission[];
  private permission_validationMessage = '';
  private permissionValidationHide = true;
  private pageName = "Permission";
  
  constructor(private _permissionService: PermissionService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     pageNo = 0;
     size = 5;
     searchToken = '';  
     totalPages = 0;
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {   
          this.checkPermission();
          this.searchPermission();       
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._permissionService.getPermissions().subscribe((result)=>{
      console.log(result);
      this.permissions=result;
    },(error)=>{
      copermissionnsole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this._permissionService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error == undefined || result.error==""){
             this.permissionValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.permissionValidationHide = false;
            this.permission_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedPermissions(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._permissionService.getPaginatedPermissions(pageNo,size).subscribe((result)=>{
      if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
        this.permissionValidationHide = false;
        this.permission_validationMessage = result.permission_validationMessage;
      }else{
          this.permissionValidationHide = true;
          this.permissions=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchPermission(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._permissionService.getPaginatedPermissionsWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
          this.permissionValidationHide = false;
          this.permission_validationMessage = result.permission_validationMessage;
      }else{        
          this.permissionValidationHide = true;
          this.permissions=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedPermissions(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedPermissions(this.pageNo,this.size);
      this.searchPermission();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedPermissions(this.pageNo,this.size);
    this.searchPermission();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedPermissions(pageNo,this.size);
    this.searchPermission();
  }
  deletePermission(permission){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._permissionService.deletePermission(permission.id).subscribe((result)=>{
        if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
            this.permissionValidationHide = false;
            this.permission_validationMessage = result.permission_validationMessage;
        }else {
            this.permissionValidationHide = true;
            this.permissions.splice(this.permissions.indexOf(permission),1);    
        }
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewPermission(permission){  
     this._permissionService.setter(permission);
     this._router.navigate(['/globalSetup/permissions/view']);
   }
   /*editPermission(permission){  
     this._permissionService.setter(permission);
     this._router.navigate(['/globalSetup/permissions/permissionForm']);
   }*/
  editPermission(permission){  
     this.spinnerService.show();  
     this._permissionService.getPermission(permission.id).subscribe((result)=>{
         if(result.permission_validationMessage!=undefined && result.permission_validationMessage!=""){
             this.permissionValidationHide = false;
             this.permission_validationMessage = result.permission_validationMessage;
         }else if(result.permission!=null || result.permission!=undefined){
             this._permissionService.setter(result.permission);
             this._router.navigate(['/globalSetup/permissions/permissionForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     })
     //this._permissionService.setter(permission);
     
   }
   newPermission(){
     let permission = new Permission();
     this._permissionService.setter(permission);
     this._router.navigate(['/globalSetup/permissions/permissionForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.permissions, 'Permission');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("Permission List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Name", dataKey: "name"},{title: "Created Date", dataKey: "createdDate"},
    ];
    doc.autoTable(reportColumns, this.permissions, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('PermissionList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('PermissionList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
