import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListpermissionComponent } from './listpermission.component';
import { PermissionFormComponent } from './permission-form.component';
import { PermissionFormViewComponent } from './permission-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListpermissionComponent,
    data: {
      title: 'Permission List'
    }
  },{
      path: 'permissionForm',
    component: PermissionFormComponent,
    data: {
      title: 'Permission Form'
    }
  },{
      path: 'view',
    component: PermissionFormViewComponent,
    data: {
      title: 'Permission Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermissionRoutingModule {}
