// Angular
import { NgModule } from '@angular/core';
import { PermissionRoutingModule } from './permission-routing.module';
import { ListpermissionComponent } from './listpermission.component';
import { PermissionFormComponent } from './permission-form.component';
import { PermissionFormViewComponent } from './permission-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PermissionService } from '../../services/permission.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';



@NgModule({
  imports: [
    PermissionRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListpermissionComponent,
    PermissionFormComponent,
    PermissionFormViewComponent
  ],
  providers: [PermissionService,
    
    ExcelService  
  ],
})
export class PermissionModule { }
