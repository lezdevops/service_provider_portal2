// Angular
import { NgModule } from '@angular/core';
import { ApprovalProcessRoutingModule } from './approvalProcess-routing.module';
import { ListapprovalProcessComponent } from './listapprovalProcess.component';
import { ApprovalProcessFormComponent } from './approvalProcess-form.component';
import { ApprovalProcessFormViewComponent } from './approvalProcess-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ApprovalProcessService } from '../../services/approvalProcess.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';



@NgModule({
  imports: [
    ApprovalProcessRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListapprovalProcessComponent,
    ApprovalProcessFormComponent,
    ApprovalProcessFormViewComponent
  ],
  providers: [ApprovalProcessService,
    
    ExcelService  
  ],
})
export class ApprovalProcessModule { }
