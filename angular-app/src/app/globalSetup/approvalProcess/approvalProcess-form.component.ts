import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApprovalProcess }  from '../../model/approvalProcess';
import { Router ,ActivatedRoute } from "@angular/router";
import { ApprovalProcessService } from '../../services/approvalProcess.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-approvalProcess-form',
  templateUrl: './approvalProcess-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ApprovalProcessFormComponent implements OnInit {
  private approvalProcess:ApprovalProcess;
  
    
  hideElement = true;
  commonValidationHide = true;
  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
    
  
processName_validationMessage = "";
isActive_validationMessage = "";
createdBy_validationMessage = "";
created_validationMessage = "";
  common_validationMessage = '';
    
  constructor(
    private _approvalProcessService:ApprovalProcessService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.approvalProcess=this._approvalProcessService.getter();
    this.approvalProcessValidationInitializer();  
      
  }
  approvalProcessValidationInitializer(){
     
this.processName_validationMessage = "";
this.isActive_validationMessage = "";
this.createdBy_validationMessage = "";
this.created_validationMessage = "";
  }
  clearApprovalProcessFormFields(){
     this.approvalProcess=this._approvalProcessService.getNew();   
  }
  validationMessages(result){  
     
this.processName_validationMessage = result.processName_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
this.createdBy_validationMessage = result.createdBy_validationMessage
this.created_validationMessage = result.created_validationMessage
  }
  processApprovalProcessForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.approvalProcessValidationInitializer();
    if(this.approvalProcess.id==undefined){
       this._approvalProcessService.createApprovalProcess(this.approvalProcess).subscribe((result)=>{
           if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
           }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/globalSetup/approvalProcesss']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._approvalProcessService.updateApprovalProcess(this.approvalProcess).subscribe((result)=>{
          if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
               this.commonValidationHide = false;
               this.hideElement = true;
               this.common_validationMessage = result.common_validationMessage;
          }else if(result.error!=undefined && result.error!=""){
               this.commonValidationHide = true;
               this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/globalSetup/approvalProcesss']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }
    

}
