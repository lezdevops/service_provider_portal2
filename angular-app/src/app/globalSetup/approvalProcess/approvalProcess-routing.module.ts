import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListapprovalProcessComponent } from './listapprovalProcess.component';
import { ApprovalProcessFormComponent } from './approvalProcess-form.component';
import { ApprovalProcessFormViewComponent } from './approvalProcess-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListapprovalProcessComponent,
    data: {
      title: 'Approval Process List'
    }
  },{
      path: 'approvalProcessForm',
    component: ApprovalProcessFormComponent,
    data: {
      title: 'Approval Process Form'
    }
  },{
      path: 'view',
    component: ApprovalProcessFormViewComponent,
    data: {
      title: 'Approval Process Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApprovalProcessRoutingModule {}
