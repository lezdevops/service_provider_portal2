import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApprovalProcess }  from '../../model/approvalProcess';
import { Router ,ActivatedRoute} from "@angular/router";
import { ApprovalProcessService } from '../../services/approvalProcess.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';




export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-approvalProcess-view-form',
  templateUrl: './approvalProcess-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ApprovalProcessFormViewComponent implements OnInit {
  private approvalProcess:ApprovalProcess;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
    
  
processName_validationMessage = "";
isActive_validationMessage = "";
createdBy_validationMessage = "";
created_validationMessage = "";
    
  constructor(
    private _approvalProcessService:ApprovalProcessService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.approvalProcess=this._approvalProcessService.getter(); 
      
  }
    
  newApprovalProcess(){
   let approvalProcess = new ApprovalProcess();
    this._approvalProcessService.setter(approvalProcess);
     this._rotuer.navigate(['/globalSetup/approvalProcesss/approvalProcessForm']);   
   }
  approvalProcessEditAction(){
     this._approvalProcessService.setter(this.approvalProcess);
     this._rotuer.navigate(['/globalSetup/approvalProcesss/approvalProcessForm']);        
  }
  printApprovalProcessDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printApprovalProcessFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ApprovalProcessList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
