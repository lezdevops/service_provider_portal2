import { Component, OnInit } from '@angular/core';
import { ApprovalProcessService } from '../../services/approvalProcess.service';
import { ApprovalProcess }  from '../../model/approvalProcess';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listapprovalProcess',
  templateUrl: './listapprovalProcess.component.html'
})
export class ListapprovalProcessComponent implements OnInit {
  private approvalProcesss:ApprovalProcess[];
  private common_validationMessage = '';
  private commonValidationHide = true;
  private pageName = "APPROVALPROCESS";
    
  private firstInactivePages = [];
  private activePages = [];
  private lastInactivePages = [];
  lastPage = false;
  firstPage =false;
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  
  constructor(private _approvalProcessService: ApprovalProcessService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.checkPermission();
          this.searchApprovalProcess();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._approvalProcessService.getApprovalProcesss().subscribe((result)=>{
      console.log(result);
      this.approvalProcesss=result;
    },(error)=>{
      coapprovalProcessnsole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this.authService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error==undefined || result.error==""){
             this.commonValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.commonValidationHide = false;
            this.common_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedApprovalProcesss(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._approvalProcessService.getPaginatedApprovalProcesss(pageNo,size).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
        this.commonValidationHide = false;
        this.common_validationMessage = result.common_validationMessage;
      }else{
          this.commonValidationHide = true;
          this.approvalProcesss=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
          this.lastPage = result.paginatedResult.last;
          this.firstPage = result.paginatedResult.first;
          this.pageNo = result.paginatedResult.number; 
          
          this.firstInactivePages = [];
          this.activePages = [];
          this.lastInactivePages = [];
          
          var i=0;
          for(i=0;i<this.pageNo;i++){
            this.firstInactivePages.push(i+1);
          }
          
           this.activePages.push(this.pageNo+1);
          
          for(i=this.pageNo+1;i<this.totalPages;i++){
            this.lastInactivePages.push(i+1);
          }
          
          //console.log(this.firstInactivePages);
          //console.log(this.activePages);
          //console.log(this.lastInactivePages);
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchApprovalProcess(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._approvalProcessService.getPaginatedApprovalProcesssWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
          this.common_validationMessage = result.common_validationMessage;
      }else{        
          this.commonValidationHide = true;
          this.approvalProcesss=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedApprovalProcesss(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedApprovalProcesss(this.pageNo,this.size);
      this.searchApprovalProcess();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedApprovalProcesss(this.pageNo,this.size);
    this.searchApprovalProcess();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedApprovalProcesss(pageNo,this.size);
    this.searchApprovalProcess();
  }
  deleteApprovalProcess(approvalProcess){
    if(confirm("Are you sure?")){
        this.spinnerService.show();
        setTimeout(()=>this.spinnerService.hide(),10000);
        this._approvalProcessService.deleteApprovalProcess(approvalProcess.id).subscribe((result)=>{
            if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.common_validationMessage;
            }else if(result.technicalError!=undefined && result.technicalError!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.technicalError;
            }else {
                this.commonValidationHide = true;
                this.approvalProcesss.splice(this.approvalProcesss.indexOf(approvalProcess),1);    
            }
            this.spinnerService.hide();
        },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
        });
    }
    
  }

   viewApprovalProcess(approvalProcess){  
      
    this._approvalProcessService.getApprovalProcess(approvalProcess.id).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
              this.common_validationMessage = result.common_validationMessage;
          }else if(result.approvalProcess!=null || result.approvalProcess!=undefined){
              let approvalProcess = result.approvalProcess;
             let created = approvalProcess.created;
  						if(created) {
  							approvalProcess.created = new Date(created);
  						}
             this._approvalProcessService.setter(approvalProcess);
              this._router.navigate(['/globalSetup/approvalProcesss/view']);
          }
          this.spinnerService.hide();
      },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
      });
     
   }
   /*editApprovalProcess(approvalProcess){  
     this._approvalProcessService.setter(approvalProcess);
     this._router.navigate(['/globalSetup/approvalProcesss/approvalProcessForm']);
   }*/
  editApprovalProcess(approvalProcess){  
     this.spinnerService.show();  
     this._approvalProcessService.getApprovalProcess(approvalProcess.id).subscribe((result)=>{
         if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
             this.commonValidationHide = false;
             this.common_validationMessage = result.common_validationMessage;
         }else if(result.approvalProcess!=null || result.approvalProcess!=undefined){
             let approvalProcess = result.approvalProcess;
             let created = approvalProcess.created;
  						if(created) {
  							approvalProcess.created = new Date(created);
  						}
             this._approvalProcessService.setter(approvalProcess);
             this._router.navigate(['/globalSetup/approvalProcesss/approvalProcessForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     });
     //this._approvalProcessService.setter(approvalProcess);
     
   }
   newApprovalProcess(){
     let approvalProcess = new ApprovalProcess();
     this._approvalProcessService.setter(approvalProcess);
     this._router.navigate(['/globalSetup/approvalProcesss/approvalProcessForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.approvalProcesss, 'ApprovalProcess');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("ApprovalProcess List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Process Name", dataKey: "processName"},{title: "Is Active", dataKey: "isActive"},{title: "Created By", dataKey: "createdBy"},{title: "Created", dataKey: "created"},
    ];
    doc.autoTable(reportColumns, this.approvalProcesss, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('ApprovalProcessList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ApprovalProcessList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
