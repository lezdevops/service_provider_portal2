import { Component, OnInit } from '@angular/core';
import { MessageReceiverService } from '../../services/messageReceiver.service';
import { MessageReceiver }  from '../../model/messageReceiver';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listmessageReceiver',
  templateUrl: './listmessageReceiver.component.html'
})
export class ListmessageReceiverComponent implements OnInit {
  private messageReceivers:MessageReceiver[];
  private common_validationMessage = '';
  private commonValidationHide = true;
  private pageName = "MESSAGERECEIVER";
    
  private firstInactivePages = [];
  private activePages = [];
  private lastInactivePages = [];
  lastPage = false;
  firstPage =false;
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  
  constructor(private _messageReceiverService: MessageReceiverService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
     
    private hasCreatePermission : boolean;
    private hasReadPermission : boolean;
    private hasEditPermission : boolean;
    private hasDeletePermission : boolean;
    private hasReportPermission : boolean;
    
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.checkPermission();
          this.searchMessageReceiver();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._messageReceiverService.getMessageReceivers().subscribe((result)=>{
      console.log(result);
      this.messageReceivers=result;
    },(error)=>{
      comessageReceivernsole.log(error);
    })*/
      
    this.spinnerService.show();
    
    
  }
  checkPermission(){
    this.authService.hasPermission(this.pageName).subscribe((result) => {
        if(result.error==undefined || result.error==""){
             this.commonValidationHide = true;
             this.hasCreatePermission = result.hasCreatePermission;
             this.hasReadPermission = result.hasReadPermission;
             this.hasEditPermission = result.hasEditPermission;
             this.hasDeletePermission = result.hasDeletePermission;
             this.hasReportPermission = result.hasReportPermission;
        }else{
            this.commonValidationHide = false;
            this.common_validationMessage = result.error;
        }
        console.log(result);
        this.spinnerService.hide();
    }, (error) => {
        console.log(error);
        this._router.navigate(['/124']);
    });    
  }
  paginatedMessageReceivers(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._messageReceiverService.getPaginatedMessageReceivers(pageNo,size).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
        this.commonValidationHide = false;
        this.common_validationMessage = result.common_validationMessage;
      }else{
          this.commonValidationHide = true;
          this.messageReceivers=result.paginatedResult.content;
          this.totalPages = result.paginatedResult.totalPages;
          this.lastPage = result.paginatedResult.last;
          this.firstPage = result.paginatedResult.first;
          this.pageNo = result.paginatedResult.number; 
          
          this.firstInactivePages = [];
          this.activePages = [];
          this.lastInactivePages = [];
          
          var i=0;
          for(i=0;i<this.pageNo;i++){
            this.firstInactivePages.push(i+1);
          }
          
           this.activePages.push(this.pageNo+1);
          
          for(i=this.pageNo+1;i<this.totalPages;i++){
            this.lastInactivePages.push(i+1);
          }
          
          //console.log(this.firstInactivePages);
          //console.log(this.activePages);
          //console.log(this.lastInactivePages);
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchMessageReceiver(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._messageReceiverService.getPaginatedMessageReceiversWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
          this.common_validationMessage = result.common_validationMessage;
      }else{        
          this.commonValidationHide = true;
          this.messageReceivers=result.paginatedResultBySearchToken.content;
          this.totalPages = result.paginatedResultBySearchToken.totalPages;    
      }
      
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedMessageReceivers(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedMessageReceivers(this.pageNo,this.size);
      this.searchMessageReceiver();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedMessageReceivers(this.pageNo,this.size);
    this.searchMessageReceiver();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedMessageReceivers(pageNo,this.size);
    this.searchMessageReceiver();
  }
  deleteMessageReceiver(messageReceiver){
    if(confirm("Are you sure?")){
        this.spinnerService.show();
        setTimeout(()=>this.spinnerService.hide(),10000);
        this._messageReceiverService.deleteMessageReceiver(messageReceiver.id).subscribe((result)=>{
            if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.common_validationMessage;
            }else if(result.technicalError!=undefined && result.technicalError!=""){
                this.commonValidationHide = false;
                this.common_validationMessage = result.technicalError;
            }else {
                this.commonValidationHide = true;
                this.messageReceivers.splice(this.messageReceivers.indexOf(messageReceiver),1);    
            }
            this.spinnerService.hide();
        },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
        });
    }
    
  }

   viewMessageReceiver(messageReceiver){  
      
    this._messageReceiverService.getMessageReceiver(messageReceiver.id).subscribe((result)=>{
      if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
          this.commonValidationHide = false;
              this.common_validationMessage = result.common_validationMessage;
          }else if(result.messageReceiver!=null || result.messageReceiver!=undefined){
              let messageReceiver = result.messageReceiver;
             
             this._messageReceiverService.setter(messageReceiver);
              this._router.navigate(['/globalSetup/messageReceivers/view']);
          }
          this.spinnerService.hide();
      },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
      });
     
   }
   /*editMessageReceiver(messageReceiver){  
     this._messageReceiverService.setter(messageReceiver);
     this._router.navigate(['/globalSetup/messageReceivers/messageReceiverForm']);
   }*/
  editMessageReceiver(messageReceiver){  
     this.spinnerService.show();  
     this._messageReceiverService.getMessageReceiver(messageReceiver.id).subscribe((result)=>{
      
         if(result.common_validationMessage!=undefined && result.common_validationMessage!=""){
             this.commonValidationHide = false;
             this.common_validationMessage = result.common_validationMessage;
         }else if(result.messageReceiver!=null || result.messageReceiver!=undefined){
             let messageReceiver = result.messageReceiver;
             
             this._messageReceiverService.setter(messageReceiver);
            
             this._router.navigate(['/globalSetup/messageReceivers/messageReceiverForm']);
         }
         this.spinnerService.hide();
     },(error)=>{
          console.log(error);
            this._router.navigate(['/124']);
     });
     //this._messageReceiverService.setter(messageReceiver);
     
   }
   newMessageReceiver(){
     let messageReceiver = new MessageReceiver();
     this._messageReceiverService.setter(messageReceiver);
     this._router.navigate(['/globalSetup/messageReceivers/messageReceiverForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.messageReceivers, 'MessageReceiver');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("MessageReceiver List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Name", dataKey: "name"},{title: "Designation", dataKey: "designation"},{title: "User", dataKey: "user"},{title: "Is Active", dataKey: "isActive"},
    ];
    doc.autoTable(reportColumns, this.messageReceivers, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('MessageReceiverList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('MessageReceiverList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
