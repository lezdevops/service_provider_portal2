import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListmessageReceiverComponent } from './listmessageReceiver.component';
import { MessageReceiverFormComponent } from './messageReceiver-form.component';
import { MessageReceiverFormViewComponent } from './messageReceiver-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListmessageReceiverComponent,
    data: {
      title: 'Message Receiver List'
    }
  },{
      path: 'messageReceiverForm',
    component: MessageReceiverFormComponent,
    data: {
      title: 'Message Receiver Form'
    }
  },{
      path: 'view',
    component: MessageReceiverFormViewComponent,
    data: {
      title: 'Message Receiver Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageReceiverRoutingModule {}
