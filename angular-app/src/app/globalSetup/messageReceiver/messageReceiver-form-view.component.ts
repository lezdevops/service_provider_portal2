import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MessageReceiver }  from '../../model/messageReceiver';
import { Router ,ActivatedRoute} from "@angular/router";
import { MessageReceiverService } from '../../services/messageReceiver.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { User }  from '../../model/user';
import { UserService }  from '../../services/user.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-messageReceiver-view-form',
  templateUrl: './messageReceiver-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class MessageReceiverFormViewComponent implements OnInit {
  private messageReceiver:MessageReceiver;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private users:User[];  
  
name_validationMessage = "";
designation_validationMessage = "";
user_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _messageReceiverService:MessageReceiverService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _userService:UserService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.messageReceiver=this._messageReceiverService.getter(); 
    this._userService.getUsers().subscribe((result)=>{
   			      this.users=result.users;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newMessageReceiver(){
   let messageReceiver = new MessageReceiver();
    this._messageReceiverService.setter(messageReceiver);
     this._rotuer.navigate(['/globalSetup/messageReceivers/messageReceiverForm']);   
   }
  messageReceiverEditAction(){
     this._messageReceiverService.setter(this.messageReceiver);
     this._rotuer.navigate(['/globalSetup/messageReceivers/messageReceiverForm']);        
  }
  printMessageReceiverDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printMessageReceiverFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('MessageReceiverList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
