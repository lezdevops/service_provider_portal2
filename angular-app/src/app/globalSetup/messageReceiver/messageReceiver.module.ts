// Angular
import { NgModule } from '@angular/core';
import { MessageReceiverRoutingModule } from './messageReceiver-routing.module';
import { MessageReceiverFormComponent } from './messageReceiver-form.component';
import { MessageReceiverFormViewComponent } from './messageReceiver-form-view.component';
import { ListmessageReceiverComponent } from './listmessageReceiver.component';

import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

import { MessageReceiverService }  from '../../services/messageReceiver.service';
import { UserService }  from '../../services/user.service';

@NgModule({
  imports: [
    MessageReceiverRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AngularMultiSelectModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    MessageReceiverFormComponent,
    MessageReceiverFormViewComponent,
    ListmessageReceiverComponent
  ],
  providers: [
    MessageReceiverService,
    UserService,
    ExcelService  
  ],
})
export class MessageReceiverModule { }
