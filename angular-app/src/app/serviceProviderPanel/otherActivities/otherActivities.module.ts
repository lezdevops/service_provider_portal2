// Angular
import { NgModule } from '@angular/core';
import { OtherActivitiesRoutingModule } from './otherActivities-routing.module';
import { ListotherActivitiesComponent } from './listotherActivities.component';
import { OtherActivitiesFormComponent } from './otherActivities-form.component';
import { OtherActivitiesFormViewComponent } from './otherActivities-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { OtherActivitiesService } from '../../services/otherActivities.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ServiceProviderService }  from '../../services/serviceProvider.service';

@NgModule({
  imports: [
    OtherActivitiesRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListotherActivitiesComponent,
    OtherActivitiesFormComponent,
    OtherActivitiesFormViewComponent
  ],
  providers: [OtherActivitiesService,
    ServiceProviderService,
    ExcelService  
  ],
})
export class OtherActivitiesModule { }
