import { Component, OnInit } from '@angular/core';
import { OtherActivitiesService } from '../../services/otherActivities.service';
import { OtherActivities }  from '../../model/otherActivities';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listotherActivities',
  templateUrl: './listotherActivities.component.html'
})
export class ListotherActivitiesComponent implements OnInit {
  private otherActivitiess:OtherActivities[];
  
  constructor(private _otherActivitiesService: OtherActivitiesService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchOtherActivities();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._otherActivitiesService.getOtherActivitiess().subscribe((result)=>{
      console.log(result);
      this.otherActivitiess=result;
    },(error)=>{
      cootherActivitiesnsole.log(error);
    })*/
    
  }
  paginatedOtherActivitiess(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._otherActivitiesService.getPaginatedOtherActivitiess(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.otherActivitiess=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchOtherActivities(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._otherActivitiesService.getPaginatedOtherActivitiessWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.otherActivitiess=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedOtherActivitiess(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedOtherActivitiess(this.pageNo,this.size);
      this.searchOtherActivities();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedOtherActivitiess(this.pageNo,this.size);
    this.searchOtherActivities();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedOtherActivitiess(pageNo,this.size);
    this.searchOtherActivities();
  }
  deleteOtherActivities(otherActivities){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._otherActivitiesService.deleteOtherActivities(otherActivities.id).subscribe((data)=>{
        this.otherActivitiess.splice(this.otherActivitiess.indexOf(otherActivities),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewOtherActivities(otherActivities){  
     this._otherActivitiesService.setter(otherActivities);
     this._router.navigate(['/serviceProviderPanel/otherActivitiess/view']);
   }
   editOtherActivities(otherActivities){  
     this._otherActivitiesService.setter(otherActivities);
     this._router.navigate(['/serviceProviderPanel/otherActivitiess/otherActivitiesForm']);
   }
   newOtherActivities(){
   let otherActivities = new OtherActivities();
    this._otherActivitiesService.setter(otherActivities);
     this._router.navigate(['/serviceProviderPanel/otherActivitiess/otherActivitiesForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.otherActivitiess, 'OtherActivities');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("OtherActivities List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Service Provider", dataKey: "serviceProvider"},{title: "Description", dataKey: "description"},{title: "Is Active", dataKey: "isActive"},
    ];
    doc.autoTable(reportColumns, this.otherActivitiess, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('OtherActivitiesList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('OtherActivitiesList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
