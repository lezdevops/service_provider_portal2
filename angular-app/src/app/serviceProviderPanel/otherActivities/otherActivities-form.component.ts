import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { OtherActivities }  from '../../model/otherActivities';
import { Router ,ActivatedRoute } from "@angular/router";
import { OtherActivitiesService } from '../../services/otherActivities.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ServiceProvider }  from '../../model/serviceProvider';
import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-otherActivities-form',
  templateUrl: './otherActivities-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class OtherActivitiesFormComponent implements OnInit {
  private otherActivities:OtherActivities;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private serviceProviders:ServiceProvider[];  
  
serviceProvider_validationMessage = "";
description_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _otherActivitiesService:OtherActivitiesService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.otherActivities=this._otherActivitiesService.getter();
    this.otherActivitiesValidationInitializer();  
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  otherActivitiesValidationInitializer(){
     
this.serviceProvider_validationMessage = "";
this.description_validationMessage = "";
this.isActive_validationMessage = "";
  }
  clearOtherActivitiesFormFields(){
     this.otherActivities=this._otherActivitiesService.getter();   
  }
  validationMessages(result){  
     
this.serviceProvider_validationMessage = result.serviceProvider_validationMessage
this.description_validationMessage = result.description_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
  }
  processOtherActivitiesForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.otherActivitiesValidationInitializer();
    if(this.otherActivities.id==undefined){
       this._otherActivitiesService.createOtherActivities(this.otherActivities).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/otherActivitiess']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._otherActivitiesService.updateOtherActivities(this.otherActivities).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/otherActivitiess']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }

}
