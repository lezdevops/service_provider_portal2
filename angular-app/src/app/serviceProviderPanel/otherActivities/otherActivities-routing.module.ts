import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListotherActivitiesComponent } from './listotherActivities.component';
import { OtherActivitiesFormComponent } from './otherActivities-form.component';
import { OtherActivitiesFormViewComponent } from './otherActivities-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListotherActivitiesComponent,
    data: {
      title: 'Other Activities List'
    }
  },{
      path: 'otherActivitiesForm',
    component: OtherActivitiesFormComponent,
    data: {
      title: 'Other Activities Form'
    }
  },{
      path: 'view',
    component: OtherActivitiesFormViewComponent,
    data: {
      title: 'Other Activities Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OtherActivitiesRoutingModule {}
