// Angular
import { NgModule } from '@angular/core';
import { DailyTaskRoutingModule } from './dailyTask-routing.module';
import { ListdailyTaskComponent } from './listdailyTask.component';
import { DailyTaskFormComponent } from './dailyTask-form.component';
import { DailyTaskFormViewComponent } from './dailyTask-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DailyTaskService } from '../../services/dailyTask.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ServiceProviderService }  from '../../services/serviceProvider.service';

@NgModule({
  imports: [
    DailyTaskRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListdailyTaskComponent,
    DailyTaskFormComponent,
    DailyTaskFormViewComponent
  ],
  providers: [DailyTaskService,
    ServiceProviderService,
    ExcelService  
  ],
})
export class DailyTaskModule { }
