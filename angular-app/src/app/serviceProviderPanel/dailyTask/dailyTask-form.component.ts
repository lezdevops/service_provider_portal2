import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DailyTask }  from '../../model/dailyTask';
import { Router ,ActivatedRoute } from "@angular/router";
import { DailyTaskService } from '../../services/dailyTask.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ServiceProvider }  from '../../model/serviceProvider';
import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-dailyTask-form',
  templateUrl: './dailyTask-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class DailyTaskFormComponent implements OnInit {
  private dailyTask:DailyTask;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private serviceProviders:ServiceProvider[];  
  
serviceProvider_validationMessage = "";
fromDate_validationMessage = "";
thruDate_validationMessage = "";
startTime_validationMessage = "";
endTime_validationMessage = "";
    
  constructor(
    private _dailyTaskService:DailyTaskService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.dailyTask=this._dailyTaskService.getter();
    this.dailyTaskValidationInitializer();  
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  dailyTaskValidationInitializer(){
     
this.serviceProvider_validationMessage = "";
this.fromDate_validationMessage = "";
this.thruDate_validationMessage = "";
this.startTime_validationMessage = "";
this.endTime_validationMessage = "";
  }
  clearDailyTaskFormFields(){
     this.dailyTask=this._dailyTaskService.getter();   
  }
  validationMessages(result){  
     
this.serviceProvider_validationMessage = result.serviceProvider_validationMessage
this.fromDate_validationMessage = result.fromDate_validationMessage
this.thruDate_validationMessage = result.thruDate_validationMessage
this.startTime_validationMessage = result.startTime_validationMessage
this.endTime_validationMessage = result.endTime_validationMessage
  }
  processDailyTaskForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.dailyTaskValidationInitializer();
    if(this.dailyTask.id==undefined){
       this._dailyTaskService.createDailyTask(this.dailyTask).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/dailyTasks']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._dailyTaskService.updateDailyTask(this.dailyTask).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/dailyTasks']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }

}
