import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListdailyTaskComponent } from './listdailyTask.component';
import { DailyTaskFormComponent } from './dailyTask-form.component';
import { DailyTaskFormViewComponent } from './dailyTask-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListdailyTaskComponent,
    data: {
      title: 'Daily Task List'
    }
  },{
      path: 'dailyTaskForm',
    component: DailyTaskFormComponent,
    data: {
      title: 'Daily Task Form'
    }
  },{
      path: 'view',
    component: DailyTaskFormViewComponent,
    data: {
      title: 'Daily Task Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DailyTaskRoutingModule {}
