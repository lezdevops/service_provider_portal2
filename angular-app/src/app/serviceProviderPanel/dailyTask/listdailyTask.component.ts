import { Component, OnInit } from '@angular/core';
import { DailyTaskService } from '../../services/dailyTask.service';
import { DailyTask }  from '../../model/dailyTask';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listdailyTask',
  templateUrl: './listdailyTask.component.html'
})
export class ListdailyTaskComponent implements OnInit {
  private dailyTasks:DailyTask[];
  
  constructor(private _dailyTaskService: DailyTaskService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchDailyTask();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._dailyTaskService.getDailyTasks().subscribe((result)=>{
      console.log(result);
      this.dailyTasks=result;
    },(error)=>{
      codailyTasknsole.log(error);
    })*/
    
  }
  paginatedDailyTasks(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._dailyTaskService.getPaginatedDailyTasks(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.dailyTasks=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchDailyTask(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._dailyTaskService.getPaginatedDailyTasksWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.dailyTasks=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedDailyTasks(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedDailyTasks(this.pageNo,this.size);
      this.searchDailyTask();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedDailyTasks(this.pageNo,this.size);
    this.searchDailyTask();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedDailyTasks(pageNo,this.size);
    this.searchDailyTask();
  }
  deleteDailyTask(dailyTask){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._dailyTaskService.deleteDailyTask(dailyTask.id).subscribe((data)=>{
        this.dailyTasks.splice(this.dailyTasks.indexOf(dailyTask),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewDailyTask(dailyTask){  
     this._dailyTaskService.setter(dailyTask);
     this._router.navigate(['/serviceProviderPanel/dailyTasks/view']);
   }
   editDailyTask(dailyTask){  
     this._dailyTaskService.setter(dailyTask);
     this._router.navigate(['/serviceProviderPanel/dailyTasks/dailyTaskForm']);
   }
   newDailyTask(){
   let dailyTask = new DailyTask();
    this._dailyTaskService.setter(dailyTask);
     this._router.navigate(['/serviceProviderPanel/dailyTasks/dailyTaskForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.dailyTasks, 'DailyTask');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("DailyTask List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Service Provider", dataKey: "serviceProvider"},{title: "From Date", dataKey: "fromDate"},{title: "Thru Date", dataKey: "thruDate"},{title: "Start Time", dataKey: "startTime"},{title: "End Time", dataKey: "endTime"},
    ];
    doc.autoTable(reportColumns, this.dailyTasks, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('DailyTaskList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('DailyTaskList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
