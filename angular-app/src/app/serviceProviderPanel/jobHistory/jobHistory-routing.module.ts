import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListjobHistoryComponent } from './listjobHistory.component';
import { JobHistoryFormComponent } from './jobHistory-form.component';
import { JobHistoryFormViewComponent } from './jobHistory-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListjobHistoryComponent,
    data: {
      title: 'Job History List'
    }
  },{
      path: 'jobHistoryForm',
    component: JobHistoryFormComponent,
    data: {
      title: 'Job History Form'
    }
  },{
      path: 'view',
    component: JobHistoryFormViewComponent,
    data: {
      title: 'Job History Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobHistoryRoutingModule {}
