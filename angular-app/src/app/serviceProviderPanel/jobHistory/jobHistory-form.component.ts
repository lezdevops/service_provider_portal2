import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { JobHistory }  from '../../model/jobHistory';
import { Router ,ActivatedRoute } from "@angular/router";
import { JobHistoryService } from '../../services/jobHistory.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ServiceProvider }  from '../../model/serviceProvider';
import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-jobHistory-form',
  templateUrl: './jobHistory-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class JobHistoryFormComponent implements OnInit {
  private jobHistory:JobHistory;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private serviceProviders:ServiceProvider[];  
  
serviceProvider_validationMessage = "";
employerName_validationMessage = "";
jobTitle_validationMessage = "";
fromDate_validationMessage = "";
thruDate_validationMessage = "";
location_validationMessage = "";
uploadedDoc_validationMessage = "";
description_validationMessage = "";
    
  constructor(
    private _jobHistoryService:JobHistoryService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.jobHistory=this._jobHistoryService.getter();
    this.jobHistoryValidationInitializer();  
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  jobHistoryValidationInitializer(){
     
this.serviceProvider_validationMessage = "";
this.employerName_validationMessage = "";
this.jobTitle_validationMessage = "";
this.fromDate_validationMessage = "";
this.thruDate_validationMessage = "";
this.location_validationMessage = "";
this.uploadedDoc_validationMessage = "";
this.description_validationMessage = "";
  }
  clearJobHistoryFormFields(){
     this.jobHistory=this._jobHistoryService.getter();   
  }
  validationMessages(result){  
     
this.serviceProvider_validationMessage = result.serviceProvider_validationMessage
this.employerName_validationMessage = result.employerName_validationMessage
this.jobTitle_validationMessage = result.jobTitle_validationMessage
this.fromDate_validationMessage = result.fromDate_validationMessage
this.thruDate_validationMessage = result.thruDate_validationMessage
this.location_validationMessage = result.location_validationMessage
this.uploadedDoc_validationMessage = result.uploadedDoc_validationMessage
this.description_validationMessage = result.description_validationMessage
  }
  processJobHistoryForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.jobHistoryValidationInitializer();
    if(this.jobHistory.id==undefined){
       this._jobHistoryService.createJobHistory(this.jobHistory).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/jobHistorys']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._jobHistoryService.updateJobHistory(this.jobHistory).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/jobHistorys']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }

}
