// Angular
import { NgModule } from '@angular/core';
import { JobHistoryRoutingModule } from './jobHistory-routing.module';
import { ListjobHistoryComponent } from './listjobHistory.component';
import { JobHistoryFormComponent } from './jobHistory-form.component';
import { JobHistoryFormViewComponent } from './jobHistory-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { JobHistoryService } from '../../services/jobHistory.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ServiceProviderService }  from '../../services/serviceProvider.service';

@NgModule({
  imports: [
    JobHistoryRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListjobHistoryComponent,
    JobHistoryFormComponent,
    JobHistoryFormViewComponent
  ],
  providers: [JobHistoryService,
    ServiceProviderService,
    ExcelService  
  ],
})
export class JobHistoryModule { }
