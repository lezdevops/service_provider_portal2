import { Component, OnInit } from '@angular/core';
import { JobHistoryService } from '../../services/jobHistory.service';
import { JobHistory }  from '../../model/jobHistory';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listjobHistory',
  templateUrl: './listjobHistory.component.html'
})
export class ListjobHistoryComponent implements OnInit {
  private jobHistorys:JobHistory[];
  
  constructor(private _jobHistoryService: JobHistoryService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchJobHistory();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._jobHistoryService.getJobHistorys().subscribe((result)=>{
      console.log(result);
      this.jobHistorys=result;
    },(error)=>{
      cojobHistorynsole.log(error);
    })*/
    
  }
  paginatedJobHistorys(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._jobHistoryService.getPaginatedJobHistorys(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.jobHistorys=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchJobHistory(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._jobHistoryService.getPaginatedJobHistorysWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.jobHistorys=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedJobHistorys(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedJobHistorys(this.pageNo,this.size);
      this.searchJobHistory();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedJobHistorys(this.pageNo,this.size);
    this.searchJobHistory();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedJobHistorys(pageNo,this.size);
    this.searchJobHistory();
  }
  deleteJobHistory(jobHistory){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._jobHistoryService.deleteJobHistory(jobHistory.id).subscribe((data)=>{
        this.jobHistorys.splice(this.jobHistorys.indexOf(jobHistory),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewJobHistory(jobHistory){  
     this._jobHistoryService.setter(jobHistory);
     this._router.navigate(['/serviceProviderPanel/jobHistorys/view']);
   }
   editJobHistory(jobHistory){  
     this._jobHistoryService.setter(jobHistory);
     this._router.navigate(['/serviceProviderPanel/jobHistorys/jobHistoryForm']);
   }
   newJobHistory(){
   let jobHistory = new JobHistory();
    this._jobHistoryService.setter(jobHistory);
     this._router.navigate(['/serviceProviderPanel/jobHistorys/jobHistoryForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.jobHistorys, 'JobHistory');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("JobHistory List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Service Provider", dataKey: "serviceProvider"},{title: "Employer Name", dataKey: "employerName"},{title: "Job Title", dataKey: "jobTitle"},{title: "From Date", dataKey: "fromDate"},{title: "Thru Date", dataKey: "thruDate"},{title: "Location", dataKey: "location"},{title: "Uploaded Doc", dataKey: "uploadedDoc"},{title: "Description", dataKey: "description"},
    ];
    doc.autoTable(reportColumns, this.jobHistorys, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('JobHistoryList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('JobHistoryList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
