export const navItems = [
  {
    name: 'Dashboard',
    url: '/serviceProviderPanel',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: '15'
    }
  },
  {
    divider: true 
  },
  {
    title: true,
    name: 'Generated Components',
  }
,{name: 'Job History',url: '/serviceProviderPanel/jobHistorys',icon: 'icon-pie-chart'}
,{name: 'Agent Reference',url: '/serviceProviderPanel/agentReferences',icon: 'icon-pie-chart'}
,{name: 'Training Info',url: '/serviceProviderPanel/trainingInfos',icon: 'icon-pie-chart'}
,{name: 'Education Info',url: '/serviceProviderPanel/educationInfos',icon: 'icon-pie-chart'}
,{name: 'Language Info',url: '/serviceProviderPanel/languageInfos',icon: 'icon-pie-chart'}
,{name: 'Experience Info',url: '/serviceProviderPanel/experienceInfos',icon: 'icon-pie-chart'}
,{name: 'Service Response Info',url: '/serviceProviderPanel/serviceResponseInfos',icon: 'icon-pie-chart'}
,{name: 'Other Activities',url: '/serviceProviderPanel/otherActivitiess',icon: 'icon-pie-chart'}
,{name: 'Daily Task',url: '/serviceProviderPanel/dailyTasks',icon: 'icon-pie-chart'}
,{name: 'Sp Service List',url: '/serviceProviderPanel/spServiceLists',icon: 'icon-pie-chart'}
// newMenuLinks
];
