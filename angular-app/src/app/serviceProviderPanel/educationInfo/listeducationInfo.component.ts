import { Component, OnInit } from '@angular/core';
import { EducationInfoService } from '../../services/educationInfo.service';
import { EducationInfo }  from '../../model/educationInfo';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listeducationInfo',
  templateUrl: './listeducationInfo.component.html'
})
export class ListeducationInfoComponent implements OnInit {
  private educationInfos:EducationInfo[];
  
  constructor(private _educationInfoService: EducationInfoService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchEducationInfo();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._educationInfoService.getEducationInfos().subscribe((result)=>{
      console.log(result);
      this.educationInfos=result;
    },(error)=>{
      coeducationInfonsole.log(error);
    })*/
    
  }
  paginatedEducationInfos(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._educationInfoService.getPaginatedEducationInfos(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.educationInfos=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchEducationInfo(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._educationInfoService.getPaginatedEducationInfosWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.educationInfos=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedEducationInfos(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedEducationInfos(this.pageNo,this.size);
      this.searchEducationInfo();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedEducationInfos(this.pageNo,this.size);
    this.searchEducationInfo();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedEducationInfos(pageNo,this.size);
    this.searchEducationInfo();
  }
  deleteEducationInfo(educationInfo){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._educationInfoService.deleteEducationInfo(educationInfo.id).subscribe((data)=>{
        this.educationInfos.splice(this.educationInfos.indexOf(educationInfo),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewEducationInfo(educationInfo){  
     this._educationInfoService.setter(educationInfo);
     this._router.navigate(['/serviceProviderPanel/educationInfos/view']);
   }
   editEducationInfo(educationInfo){  
     this._educationInfoService.setter(educationInfo);
     this._router.navigate(['/serviceProviderPanel/educationInfos/educationInfoForm']);
   }
   newEducationInfo(){
   let educationInfo = new EducationInfo();
    this._educationInfoService.setter(educationInfo);
     this._router.navigate(['/serviceProviderPanel/educationInfos/educationInfoForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.educationInfos, 'EducationInfo');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("EducationInfo List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Service Provider", dataKey: "serviceProvider"},{title: "Institute Name", dataKey: "instituteName"},{title: "From Year", dataKey: "fromYear"},{title: "Thru Year", dataKey: "thruYear"},{title: "Degree", dataKey: "degree"},{title: "Is Active", dataKey: "isActive"},
    ];
    doc.autoTable(reportColumns, this.educationInfos, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('EducationInfoList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('EducationInfoList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
