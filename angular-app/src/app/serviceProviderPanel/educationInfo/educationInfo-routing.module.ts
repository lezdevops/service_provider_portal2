import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListeducationInfoComponent } from './listeducationInfo.component';
import { EducationInfoFormComponent } from './educationInfo-form.component';
import { EducationInfoFormViewComponent } from './educationInfo-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListeducationInfoComponent,
    data: {
      title: 'Education Info List'
    }
  },{
      path: 'educationInfoForm',
    component: EducationInfoFormComponent,
    data: {
      title: 'Education Info Form'
    }
  },{
      path: 'view',
    component: EducationInfoFormViewComponent,
    data: {
      title: 'Education Info Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EducationInfoRoutingModule {}
