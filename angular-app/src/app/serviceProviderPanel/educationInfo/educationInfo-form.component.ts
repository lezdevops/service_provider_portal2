import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { EducationInfo }  from '../../model/educationInfo';
import { Router ,ActivatedRoute } from "@angular/router";
import { EducationInfoService } from '../../services/educationInfo.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ServiceProvider }  from '../../model/serviceProvider';
import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-educationInfo-form',
  templateUrl: './educationInfo-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class EducationInfoFormComponent implements OnInit {
  private educationInfo:EducationInfo;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private serviceProviders:ServiceProvider[];  
  
serviceProvider_validationMessage = "";
instituteName_validationMessage = "";
fromYear_validationMessage = "";
thruYear_validationMessage = "";
degree_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _educationInfoService:EducationInfoService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.educationInfo=this._educationInfoService.getter();
    this.educationInfoValidationInitializer();  
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  educationInfoValidationInitializer(){
     
this.serviceProvider_validationMessage = "";
this.instituteName_validationMessage = "";
this.fromYear_validationMessage = "";
this.thruYear_validationMessage = "";
this.degree_validationMessage = "";
this.isActive_validationMessage = "";
  }
  clearEducationInfoFormFields(){
     this.educationInfo=this._educationInfoService.getter();   
  }
  validationMessages(result){  
     
this.serviceProvider_validationMessage = result.serviceProvider_validationMessage
this.instituteName_validationMessage = result.instituteName_validationMessage
this.fromYear_validationMessage = result.fromYear_validationMessage
this.thruYear_validationMessage = result.thruYear_validationMessage
this.degree_validationMessage = result.degree_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
  }
  processEducationInfoForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.educationInfoValidationInitializer();
    if(this.educationInfo.id==undefined){
       this._educationInfoService.createEducationInfo(this.educationInfo).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/educationInfos']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._educationInfoService.updateEducationInfo(this.educationInfo).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/educationInfos']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }

}
