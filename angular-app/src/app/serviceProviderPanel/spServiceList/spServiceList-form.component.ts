import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SpServiceList }  from '../../model/spServiceList';
import { Router ,ActivatedRoute } from "@angular/router";
import { SpServiceListService } from '../../services/spServiceList.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ServiceProvider }  from '../../model/serviceProvider';import { ServiceType }  from '../../model/serviceType';import { Location }  from '../../model/location';
import { ServiceProviderService }  from '../../services/serviceProvider.service';import { ServiceTypeService }  from '../../services/serviceType.service';import { LocationService }  from '../../services/location.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-spServiceList-form',
  templateUrl: './spServiceList-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class SpServiceListFormComponent implements OnInit {
  private spServiceList:SpServiceList;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private serviceProviders:ServiceProvider[];private serviceTypes:ServiceType[];private locations:Location[];  
  
serviceProvider_validationMessage = "";
serviceDetails_validationMessage = "";
serviceType_validationMessage = "";
visitCount_validationMessage = "";
location_validationMessage = "";
requestCount_validationMessage = "";
startTime_validationMessage = "";
endTime_validationMessage = "";
isActive_validationMessage = "";
availFrom_validationMessage = "";
availTo_validationMessage = "";
    
  constructor(
    private _spServiceListService:SpServiceListService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,private _serviceTypeService:ServiceTypeService,private _locationService:LocationService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.spServiceList=this._spServiceListService.getter();
    this.spServiceListValidationInitializer();  
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });this._serviceTypeService.getServiceTypes().subscribe((serviceTypes)=>{
   			      this.serviceTypes=serviceTypes;
   			    },(error)=>{
   			      //console.log(error);
   			    });this._locationService.getLocations().subscribe((locations)=>{
   			      this.locations=locations;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  spServiceListValidationInitializer(){
     
this.serviceProvider_validationMessage = "";
this.serviceDetails_validationMessage = "";
this.serviceType_validationMessage = "";
this.visitCount_validationMessage = "";
this.location_validationMessage = "";
this.requestCount_validationMessage = "";
this.startTime_validationMessage = "";
this.endTime_validationMessage = "";
this.isActive_validationMessage = "";
this.availFrom_validationMessage = "";
this.availTo_validationMessage = "";
  }
  clearSpServiceListFormFields(){
     this.spServiceList=this._spServiceListService.getter();   
  }
  validationMessages(result){  
     
this.serviceProvider_validationMessage = result.serviceProvider_validationMessage
this.serviceDetails_validationMessage = result.serviceDetails_validationMessage
this.serviceType_validationMessage = result.serviceType_validationMessage
this.visitCount_validationMessage = result.visitCount_validationMessage
this.location_validationMessage = result.location_validationMessage
this.requestCount_validationMessage = result.requestCount_validationMessage
this.startTime_validationMessage = result.startTime_validationMessage
this.endTime_validationMessage = result.endTime_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
this.availFrom_validationMessage = result.availFrom_validationMessage
this.availTo_validationMessage = result.availTo_validationMessage
  }
  processSpServiceListForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.spServiceListValidationInitializer();
    if(this.spServiceList.id==undefined){
       this._spServiceListService.createSpServiceList(this.spServiceList).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/spServiceLists']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._spServiceListService.updateSpServiceList(this.spServiceList).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/spServiceLists']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }

}
