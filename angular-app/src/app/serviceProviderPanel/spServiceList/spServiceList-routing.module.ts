import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListspServiceListComponent } from './listspServiceList.component';
import { SpServiceListFormComponent } from './spServiceList-form.component';
import { SpServiceListFormViewComponent } from './spServiceList-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListspServiceListComponent,
    data: {
      title: 'Sp Service List List'
    }
  },{
      path: 'spServiceListForm',
    component: SpServiceListFormComponent,
    data: {
      title: 'Sp Service List Form'
    }
  },{
      path: 'view',
    component: SpServiceListFormViewComponent,
    data: {
      title: 'Sp Service List Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpServiceListRoutingModule {}
