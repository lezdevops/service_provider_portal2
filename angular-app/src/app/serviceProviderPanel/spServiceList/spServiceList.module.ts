// Angular
import { NgModule } from '@angular/core';
import { SpServiceListRoutingModule } from './spServiceList-routing.module';
import { ListspServiceListComponent } from './listspServiceList.component';
import { SpServiceListFormComponent } from './spServiceList-form.component';
import { SpServiceListFormViewComponent } from './spServiceList-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SpServiceListService } from '../../services/spServiceList.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ServiceProviderService }  from '../../services/serviceProvider.service';import { ServiceTypeService }  from '../../services/serviceType.service';import { LocationService }  from '../../services/location.service';

@NgModule({
  imports: [
    SpServiceListRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListspServiceListComponent,
    SpServiceListFormComponent,
    SpServiceListFormViewComponent
  ],
  providers: [SpServiceListService,
    ServiceProviderService,ServiceTypeService,LocationService,
    ExcelService  
  ],
})
export class SpServiceListModule { }
