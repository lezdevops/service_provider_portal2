import { Component, OnInit } from '@angular/core';
import { SpServiceListService } from '../../services/spServiceList.service';
import { SpServiceList }  from '../../model/spServiceList';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listspServiceList',
  templateUrl: './listspServiceList.component.html'
})
export class ListspServiceListComponent implements OnInit {
  private spServiceLists:SpServiceList[];
  
  constructor(private _spServiceListService: SpServiceListService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchSpServiceList();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._spServiceListService.getSpServiceLists().subscribe((result)=>{
      console.log(result);
      this.spServiceLists=result;
    },(error)=>{
      cospServiceListnsole.log(error);
    })*/
    
  }
  paginatedSpServiceLists(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._spServiceListService.getPaginatedSpServiceLists(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.spServiceLists=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchSpServiceList(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._spServiceListService.getPaginatedSpServiceListsWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.spServiceLists=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedSpServiceLists(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedSpServiceLists(this.pageNo,this.size);
      this.searchSpServiceList();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedSpServiceLists(this.pageNo,this.size);
    this.searchSpServiceList();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedSpServiceLists(pageNo,this.size);
    this.searchSpServiceList();
  }
  deleteSpServiceList(spServiceList){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._spServiceListService.deleteSpServiceList(spServiceList.id).subscribe((data)=>{
        this.spServiceLists.splice(this.spServiceLists.indexOf(spServiceList),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewSpServiceList(spServiceList){  
     this._spServiceListService.setter(spServiceList);
     this._router.navigate(['/serviceProviderPanel/spServiceLists/view']);
   }
   editSpServiceList(spServiceList){  
     this._spServiceListService.setter(spServiceList);
     this._router.navigate(['/serviceProviderPanel/spServiceLists/spServiceListForm']);
   }
   newSpServiceList(){
   let spServiceList = new SpServiceList();
    this._spServiceListService.setter(spServiceList);
     this._router.navigate(['/serviceProviderPanel/spServiceLists/spServiceListForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.spServiceLists, 'SpServiceList');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("SpServiceList List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Service Provider", dataKey: "serviceProvider"},{title: "Service Details", dataKey: "serviceDetails"},{title: "Service Type", dataKey: "serviceType"},{title: "Visit Count", dataKey: "visitCount"},{title: "Location", dataKey: "location"},{title: "Request Count", dataKey: "requestCount"},{title: "Start Time", dataKey: "startTime"},{title: "End Time", dataKey: "endTime"},{title: "Is Active", dataKey: "isActive"},{title: "Avail From", dataKey: "availFrom"},{title: "Avail To", dataKey: "availTo"},
    ];
    doc.autoTable(reportColumns, this.spServiceLists, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('SpServiceListList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('SpServiceListList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
