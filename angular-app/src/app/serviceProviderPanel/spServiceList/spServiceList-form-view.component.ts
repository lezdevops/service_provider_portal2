import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SpServiceList }  from '../../model/spServiceList';
import { Router ,ActivatedRoute} from "@angular/router";
import { SpServiceListService } from '../../services/spServiceList.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { ServiceProvider }  from '../../model/serviceProvider';import { ServiceType }  from '../../model/serviceType';import { Location }  from '../../model/location';
import { ServiceProviderService }  from '../../services/serviceProvider.service';import { ServiceTypeService }  from '../../services/serviceType.service';import { LocationService }  from '../../services/location.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-spServiceList-view-form',
  templateUrl: './spServiceList-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class SpServiceListFormViewComponent implements OnInit {
  private spServiceList:SpServiceList;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private serviceProviders:ServiceProvider[];private serviceTypes:ServiceType[];private locations:Location[];  
  
serviceProvider_validationMessage = "";
serviceDetails_validationMessage = "";
serviceType_validationMessage = "";
visitCount_validationMessage = "";
location_validationMessage = "";
requestCount_validationMessage = "";
startTime_validationMessage = "";
endTime_validationMessage = "";
isActive_validationMessage = "";
availFrom_validationMessage = "";
availTo_validationMessage = "";
    
  constructor(
    private _spServiceListService:SpServiceListService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,private _serviceTypeService:ServiceTypeService,private _locationService:LocationService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.spServiceList=this._spServiceListService.getter(); 
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });this._serviceTypeService.getServiceTypes().subscribe((serviceTypes)=>{
   			      this.serviceTypes=serviceTypes;
   			    },(error)=>{
   			      //console.log(error);
   			    });this._locationService.getLocations().subscribe((locations)=>{
   			      this.locations=locations;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newSpServiceList(){
   let spServiceList = new SpServiceList();
    this._spServiceListService.setter(spServiceList);
     this._rotuer.navigate(['/serviceProviderPanel/spServiceLists/spServiceListForm']);   
   }
  spServiceListEditAction(){
     this._spServiceListService.setter(this.spServiceList);
     this._rotuer.navigate(['/serviceProviderPanel/spServiceLists/spServiceListForm']);        
  }
  printSpServiceListDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printSpServiceListFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('SpServiceListList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
