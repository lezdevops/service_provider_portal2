import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListlanguageInfoComponent } from './listlanguageInfo.component';
import { LanguageInfoFormComponent } from './languageInfo-form.component';
import { LanguageInfoFormViewComponent } from './languageInfo-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListlanguageInfoComponent,
    data: {
      title: 'Language Info List'
    }
  },{
      path: 'languageInfoForm',
    component: LanguageInfoFormComponent,
    data: {
      title: 'Language Info Form'
    }
  },{
      path: 'view',
    component: LanguageInfoFormViewComponent,
    data: {
      title: 'Language Info Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LanguageInfoRoutingModule {}
