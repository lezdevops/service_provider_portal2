// Angular
import { NgModule } from '@angular/core';
import { LanguageInfoRoutingModule } from './languageInfo-routing.module';
import { ListlanguageInfoComponent } from './listlanguageInfo.component';
import { LanguageInfoFormComponent } from './languageInfo-form.component';
import { LanguageInfoFormViewComponent } from './languageInfo-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LanguageInfoService } from '../../services/languageInfo.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ServiceProviderService }  from '../../services/serviceProvider.service';

@NgModule({
  imports: [
    LanguageInfoRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListlanguageInfoComponent,
    LanguageInfoFormComponent,
    LanguageInfoFormViewComponent
  ],
  providers: [LanguageInfoService,
    ServiceProviderService,
    ExcelService  
  ],
})
export class LanguageInfoModule { }
