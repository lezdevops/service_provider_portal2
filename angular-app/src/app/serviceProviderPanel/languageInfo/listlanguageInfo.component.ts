import { Component, OnInit } from '@angular/core';
import { LanguageInfoService } from '../../services/languageInfo.service';
import { LanguageInfo }  from '../../model/languageInfo';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listlanguageInfo',
  templateUrl: './listlanguageInfo.component.html'
})
export class ListlanguageInfoComponent implements OnInit {
  private languageInfos:LanguageInfo[];
  
  constructor(private _languageInfoService: LanguageInfoService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchLanguageInfo();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._languageInfoService.getLanguageInfos().subscribe((result)=>{
      console.log(result);
      this.languageInfos=result;
    },(error)=>{
      colanguageInfonsole.log(error);
    })*/
    
  }
  paginatedLanguageInfos(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._languageInfoService.getPaginatedLanguageInfos(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.languageInfos=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchLanguageInfo(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._languageInfoService.getPaginatedLanguageInfosWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.languageInfos=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedLanguageInfos(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedLanguageInfos(this.pageNo,this.size);
      this.searchLanguageInfo();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedLanguageInfos(this.pageNo,this.size);
    this.searchLanguageInfo();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedLanguageInfos(pageNo,this.size);
    this.searchLanguageInfo();
  }
  deleteLanguageInfo(languageInfo){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._languageInfoService.deleteLanguageInfo(languageInfo.id).subscribe((data)=>{
        this.languageInfos.splice(this.languageInfos.indexOf(languageInfo),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewLanguageInfo(languageInfo){  
     this._languageInfoService.setter(languageInfo);
     this._router.navigate(['/serviceProviderPanel/languageInfos/view']);
   }
   editLanguageInfo(languageInfo){  
     this._languageInfoService.setter(languageInfo);
     this._router.navigate(['/serviceProviderPanel/languageInfos/languageInfoForm']);
   }
   newLanguageInfo(){
   let languageInfo = new LanguageInfo();
    this._languageInfoService.setter(languageInfo);
     this._router.navigate(['/serviceProviderPanel/languageInfos/languageInfoForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.languageInfos, 'LanguageInfo');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("LanguageInfo List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Service Provider", dataKey: "serviceProvider"},{title: "Language Name", dataKey: "languageName"},{title: "Short Code", dataKey: "shortCode"},{title: "Is Active", dataKey: "isActive"},
    ];
    doc.autoTable(reportColumns, this.languageInfos, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('LanguageInfoList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('LanguageInfoList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
