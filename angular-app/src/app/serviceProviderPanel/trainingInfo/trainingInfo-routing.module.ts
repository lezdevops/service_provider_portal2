import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListtrainingInfoComponent } from './listtrainingInfo.component';
import { TrainingInfoFormComponent } from './trainingInfo-form.component';
import { TrainingInfoFormViewComponent } from './trainingInfo-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListtrainingInfoComponent,
    data: {
      title: 'Training Info List'
    }
  },{
      path: 'trainingInfoForm',
    component: TrainingInfoFormComponent,
    data: {
      title: 'Training Info Form'
    }
  },{
      path: 'view',
    component: TrainingInfoFormViewComponent,
    data: {
      title: 'Training Info Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingInfoRoutingModule {}
