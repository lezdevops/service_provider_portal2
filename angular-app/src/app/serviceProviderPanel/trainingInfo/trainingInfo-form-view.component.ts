import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TrainingInfo }  from '../../model/trainingInfo';
import { Router ,ActivatedRoute} from "@angular/router";
import { TrainingInfoService } from '../../services/trainingInfo.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { ServiceProvider }  from '../../model/serviceProvider';import { TrainingType }  from '../../model/trainingType';
import { ServiceProviderService }  from '../../services/serviceProvider.service';import { TrainingTypeService }  from '../../services/trainingType.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-trainingInfo-view-form',
  templateUrl: './trainingInfo-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class TrainingInfoFormViewComponent implements OnInit {
  private trainingInfo:TrainingInfo;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private serviceProviders:ServiceProvider[];private trainingTypes:TrainingType[];  
  
serviceProvider_validationMessage = "";
trainingType_validationMessage = "";
monthYear_validationMessage = "";
description_validationMessage = "";
certificationDoc_validationMessage = "";
uploadedDate_validationMessage = "";
    
  constructor(
    private _trainingInfoService:TrainingInfoService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,private _trainingTypeService:TrainingTypeService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.trainingInfo=this._trainingInfoService.getter(); 
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });this._trainingTypeService.getTrainingTypes().subscribe((trainingTypes)=>{
   			      this.trainingTypes=trainingTypes;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newTrainingInfo(){
   let trainingInfo = new TrainingInfo();
    this._trainingInfoService.setter(trainingInfo);
     this._rotuer.navigate(['/serviceProviderPanel/trainingInfos/trainingInfoForm']);   
   }
  trainingInfoEditAction(){
     this._trainingInfoService.setter(this.trainingInfo);
     this._rotuer.navigate(['/serviceProviderPanel/trainingInfos/trainingInfoForm']);        
  }
  printTrainingInfoDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printTrainingInfoFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('TrainingInfoList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
