// Angular
import { NgModule } from '@angular/core';
import { TrainingInfoRoutingModule } from './trainingInfo-routing.module';
import { ListtrainingInfoComponent } from './listtrainingInfo.component';
import { TrainingInfoFormComponent } from './trainingInfo-form.component';
import { TrainingInfoFormViewComponent } from './trainingInfo-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TrainingInfoService } from '../../services/trainingInfo.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ServiceProviderService }  from '../../services/serviceProvider.service';import { TrainingTypeService }  from '../../services/trainingType.service';

@NgModule({
  imports: [
    TrainingInfoRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListtrainingInfoComponent,
    TrainingInfoFormComponent,
    TrainingInfoFormViewComponent
  ],
  providers: [TrainingInfoService,
    ServiceProviderService,TrainingTypeService,
    ExcelService  
  ],
})
export class TrainingInfoModule { }
