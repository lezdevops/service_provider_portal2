import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TrainingInfo }  from '../../model/trainingInfo';
import { Router ,ActivatedRoute } from "@angular/router";
import { TrainingInfoService } from '../../services/trainingInfo.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ServiceProvider }  from '../../model/serviceProvider';import { TrainingType }  from '../../model/trainingType';
import { ServiceProviderService }  from '../../services/serviceProvider.service';import { TrainingTypeService }  from '../../services/trainingType.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-trainingInfo-form',
  templateUrl: './trainingInfo-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class TrainingInfoFormComponent implements OnInit {
  private trainingInfo:TrainingInfo;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private serviceProviders:ServiceProvider[];private trainingTypes:TrainingType[];  
  
serviceProvider_validationMessage = "";
trainingType_validationMessage = "";
monthYear_validationMessage = "";
description_validationMessage = "";
certificationDoc_validationMessage = "";
uploadedDate_validationMessage = "";
    
  constructor(
    private _trainingInfoService:TrainingInfoService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,private _trainingTypeService:TrainingTypeService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.trainingInfo=this._trainingInfoService.getter();
    this.trainingInfoValidationInitializer();  
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });this._trainingTypeService.getTrainingTypes().subscribe((trainingTypes)=>{
   			      this.trainingTypes=trainingTypes;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  trainingInfoValidationInitializer(){
     
this.serviceProvider_validationMessage = "";
this.trainingType_validationMessage = "";
this.monthYear_validationMessage = "";
this.description_validationMessage = "";
this.certificationDoc_validationMessage = "";
this.uploadedDate_validationMessage = "";
  }
  clearTrainingInfoFormFields(){
     this.trainingInfo=this._trainingInfoService.getter();   
  }
  validationMessages(result){  
     
this.serviceProvider_validationMessage = result.serviceProvider_validationMessage
this.trainingType_validationMessage = result.trainingType_validationMessage
this.monthYear_validationMessage = result.monthYear_validationMessage
this.description_validationMessage = result.description_validationMessage
this.certificationDoc_validationMessage = result.certificationDoc_validationMessage
this.uploadedDate_validationMessage = result.uploadedDate_validationMessage
  }
  processTrainingInfoForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.trainingInfoValidationInitializer();
    if(this.trainingInfo.id==undefined){
       this._trainingInfoService.createTrainingInfo(this.trainingInfo).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/trainingInfos']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._trainingInfoService.updateTrainingInfo(this.trainingInfo).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/trainingInfos']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }

}
