import { Component, OnInit } from '@angular/core';
import { TrainingInfoService } from '../../services/trainingInfo.service';
import { TrainingInfo }  from '../../model/trainingInfo';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listtrainingInfo',
  templateUrl: './listtrainingInfo.component.html'
})
export class ListtrainingInfoComponent implements OnInit {
  private trainingInfos:TrainingInfo[];
  
  constructor(private _trainingInfoService: TrainingInfoService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchTrainingInfo();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._trainingInfoService.getTrainingInfos().subscribe((result)=>{
      console.log(result);
      this.trainingInfos=result;
    },(error)=>{
      cotrainingInfonsole.log(error);
    })*/
    
  }
  paginatedTrainingInfos(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._trainingInfoService.getPaginatedTrainingInfos(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.trainingInfos=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchTrainingInfo(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._trainingInfoService.getPaginatedTrainingInfosWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.trainingInfos=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedTrainingInfos(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedTrainingInfos(this.pageNo,this.size);
      this.searchTrainingInfo();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedTrainingInfos(this.pageNo,this.size);
    this.searchTrainingInfo();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedTrainingInfos(pageNo,this.size);
    this.searchTrainingInfo();
  }
  deleteTrainingInfo(trainingInfo){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._trainingInfoService.deleteTrainingInfo(trainingInfo.id).subscribe((data)=>{
        this.trainingInfos.splice(this.trainingInfos.indexOf(trainingInfo),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewTrainingInfo(trainingInfo){  
     this._trainingInfoService.setter(trainingInfo);
     this._router.navigate(['/serviceProviderPanel/trainingInfos/view']);
   }
   editTrainingInfo(trainingInfo){  
     this._trainingInfoService.setter(trainingInfo);
     this._router.navigate(['/serviceProviderPanel/trainingInfos/trainingInfoForm']);
   }
   newTrainingInfo(){
   let trainingInfo = new TrainingInfo();
    this._trainingInfoService.setter(trainingInfo);
     this._router.navigate(['/serviceProviderPanel/trainingInfos/trainingInfoForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.trainingInfos, 'TrainingInfo');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("TrainingInfo List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Service Provider", dataKey: "serviceProvider"},{title: "Training Type", dataKey: "trainingType"},{title: "Month Year", dataKey: "monthYear"},{title: "Description", dataKey: "description"},{title: "Certification Doc", dataKey: "certificationDoc"},{title: "Uploaded Date", dataKey: "uploadedDate"},
    ];
    doc.autoTable(reportColumns, this.trainingInfos, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('TrainingInfoList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('TrainingInfoList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
