// Angular
import { NgModule } from '@angular/core';
import { SPDutyRoutingModule } from './sPDuty-routing.module';
import { ListsPDutyComponent } from './listsPDuty.component';
import { SPDutyFormComponent } from './sPDuty-form.component';
import { SPDutyFormViewComponent } from './sPDuty-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SPDutyService } from '../../services/sPDuty.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ServiceProviderService }  from '../../services/serviceProvider.service';

@NgModule({
  imports: [
    SPDutyRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListsPDutyComponent,
    SPDutyFormComponent,
    SPDutyFormViewComponent
  ],
  providers: [SPDutyService,
    ServiceProviderService,
    ExcelService  
  ],
})
export class SPDutyModule { }
