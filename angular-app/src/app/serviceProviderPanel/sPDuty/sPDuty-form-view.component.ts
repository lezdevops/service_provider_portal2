import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SPDuty }  from '../../model/sPDuty';
import { Router ,ActivatedRoute} from "@angular/router";
import { SPDutyService } from '../../services/sPDuty.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { ServiceProvider }  from '../../model/serviceProvider';
import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-sPDuty-view-form',
  templateUrl: './sPDuty-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class SPDutyFormViewComponent implements OnInit {
  private sPDuty:SPDuty;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private serviceProviders:ServiceProvider[];  
  
fromDate_validationMessage = "";
thruDate_validationMessage = "";
startTime_validationMessage = "";
endTime_validationMessage = "";
    
  constructor(
    private _sPDutyService:SPDutyService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.sPDuty=this._sPDutyService.getter(); 
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newSPDuty(){
   let sPDuty = new SPDuty();
    this._sPDutyService.setter(sPDuty);
     this._rotuer.navigate(['/serviceProviderPanel/sPDutys/sPDutyForm']);   
   }
  sPDutyEditAction(){
     this._sPDutyService.setter(this.sPDuty);
     this._rotuer.navigate(['/serviceProviderPanel/sPDutys/sPDutyForm']);        
  }
  printSPDutyDetails() {  
     var data = document.getElementById('printSPDutyFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('SPDutyList.pdf'); // Generated PDF   
     });  
   } 

}
