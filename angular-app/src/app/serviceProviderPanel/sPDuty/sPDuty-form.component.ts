import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SPDuty }  from '../../model/sPDuty';
import { Router ,ActivatedRoute } from "@angular/router";
import { SPDutyService } from '../../services/sPDuty.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { ServiceProvider }  from '../../model/serviceProvider';
import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-sPDuty-form',
  templateUrl: './sPDuty-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class SPDutyFormComponent implements OnInit {
  private sPDuty:SPDuty;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private serviceProviders:ServiceProvider[];  
  
fromDate_validationMessage = "";
thruDate_validationMessage = "";
startTime_validationMessage = "";
endTime_validationMessage = "";
    
  constructor(
    private _sPDutyService:SPDutyService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.sPDuty=this._sPDutyService.getter();
    this.sPDutyValidationInitializer();  
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  sPDutyValidationInitializer(){
     
this.fromDate_validationMessage = "";
this.thruDate_validationMessage = "";
this.startTime_validationMessage = "";
this.endTime_validationMessage = "";
  }
  clearSPDutyFormFields(){
     this.sPDuty=this._sPDutyService.getter();   
  }
  validationMessages(result){  
     
this.fromDate_validationMessage = result.fromDate_validationMessage
this.thruDate_validationMessage = result.thruDate_validationMessage
this.startTime_validationMessage = result.startTime_validationMessage
this.endTime_validationMessage = result.endTime_validationMessage
  }
  processSPDutyForm(){
    this.sPDutyValidationInitializer();
    if(this.sPDuty.id==undefined){
       this._sPDutyService.createSPDuty(this.sPDuty).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/sPDutys']);
           }         
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._sPDutyService.updateSPDuty(this.sPDuty).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/sPDutys']);
           }
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }

}
