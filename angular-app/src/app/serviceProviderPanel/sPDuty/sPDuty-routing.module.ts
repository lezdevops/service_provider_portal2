import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListsPDutyComponent } from './listsPDuty.component';
import { SPDutyFormComponent } from './sPDuty-form.component';
import { SPDutyFormViewComponent } from './sPDuty-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListsPDutyComponent,
    data: {
      title: 'SPDuty List'
    }
  },{
      path: 'sPDutyForm',
    component: SPDutyFormComponent,
    data: {
      title: 'SPDuty Form'
    }
  },{
      path: 'view',
    component: SPDutyFormViewComponent,
    data: {
      title: 'SPDuty Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SPDutyRoutingModule {}
