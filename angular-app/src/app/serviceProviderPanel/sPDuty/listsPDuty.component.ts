import { Component, OnInit } from '@angular/core';
import { SPDutyService } from '../../services/sPDuty.service';
import { SPDuty }  from '../../model/sPDuty';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-listsPDuty',
  templateUrl: './listsPDuty.component.html'
})
export class ListsPDutyComponent implements OnInit {
  private sPDutys:SPDuty[];
  
  constructor(private _sPDutyService: SPDutyService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchSPDuty();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._sPDutyService.getSPDutys().subscribe((result)=>{
      console.log(result);
      this.sPDutys=result;
    },(error)=>{
      cosPDutynsole.log(error);
    })*/
    
  }
  paginatedSPDutys(pageNo,size){
    this._sPDutyService.getPaginatedSPDutys(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.sPDutys=result.content;
      this.totalPages = result.totalPages;
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchSPDuty(){
    if(this.searchToken!=""){
      this._sPDutyService.getPaginatedSPDutysWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.sPDutys=result.content;
      this.totalPages = result.totalPages;
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedSPDutys(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedSPDutys(this.pageNo,this.size);
      this.searchSPDuty();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedSPDutys(this.pageNo,this.size);
    this.searchSPDuty();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedSPDutys(pageNo,this.size);
    this.searchSPDuty();
  }
  deleteSPDuty(sPDuty){
    this._sPDutyService.deleteSPDuty(sPDuty.id).subscribe((data)=>{
        this.sPDutys.splice(this.sPDutys.indexOf(sPDuty),1);
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewSPDuty(sPDuty){  
     this._sPDutyService.setter(sPDuty);
     this._router.navigate(['/serviceProviderPanel/sPDutys/view']);
   }
   editSPDuty(sPDuty){  
     this._sPDutyService.setter(sPDuty);
     this._router.navigate(['/serviceProviderPanel/sPDutys/sPDutyForm']);
   }
   newSPDuty(){
   let sPDuty = new SPDuty();
    this._sPDutyService.setter(sPDuty);
     this._router.navigate(['/serviceProviderPanel/sPDutys/sPDutyForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.sPDutys, 'SPDuty');
   }
   exportAsPDF(): void{
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("SPDuty List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Service Provider", dataKey: "serviceProvider"},{title: "From Date", dataKey: "fromDate"},{title: "Thru Date", dataKey: "thruDate"},{title: "Start Time", dataKey: "startTime"},{title: "End Time", dataKey: "endTime"},
    ];
    doc.autoTable(reportColumns, this.sPDutys, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('SPDutyList.pdf');
   }
   captureScreen() {  
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('SPDutyList.pdf'); // Generated PDF   
     });  
   }  

}
