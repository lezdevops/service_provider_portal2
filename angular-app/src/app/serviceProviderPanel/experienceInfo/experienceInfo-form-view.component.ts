import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ExperienceInfo }  from '../../model/experienceInfo';
import { Router ,ActivatedRoute} from "@angular/router";
import { ExperienceInfoService } from '../../services/experienceInfo.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { ServiceProvider }  from '../../model/serviceProvider';
import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-experienceInfo-view-form',
  templateUrl: './experienceInfo-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ExperienceInfoFormViewComponent implements OnInit {
  private experienceInfo:ExperienceInfo;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private serviceProviders:ServiceProvider[];  
  
serviceProvider_validationMessage = "";
fromDate_validationMessage = "";
thruDate_validationMessage = "";
description_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _experienceInfoService:ExperienceInfoService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.experienceInfo=this._experienceInfoService.getter(); 
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newExperienceInfo(){
   let experienceInfo = new ExperienceInfo();
    this._experienceInfoService.setter(experienceInfo);
     this._rotuer.navigate(['/serviceProviderPanel/experienceInfos/experienceInfoForm']);   
   }
  experienceInfoEditAction(){
     this._experienceInfoService.setter(this.experienceInfo);
     this._rotuer.navigate(['/serviceProviderPanel/experienceInfos/experienceInfoForm']);        
  }
  printExperienceInfoDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printExperienceInfoFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ExperienceInfoList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
