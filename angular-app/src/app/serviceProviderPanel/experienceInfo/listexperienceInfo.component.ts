import { Component, OnInit } from '@angular/core';
import { ExperienceInfoService } from '../../services/experienceInfo.service';
import { ExperienceInfo }  from '../../model/experienceInfo';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listexperienceInfo',
  templateUrl: './listexperienceInfo.component.html'
})
export class ListexperienceInfoComponent implements OnInit {
  private experienceInfos:ExperienceInfo[];
  
  constructor(private _experienceInfoService: ExperienceInfoService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchExperienceInfo();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._experienceInfoService.getExperienceInfos().subscribe((result)=>{
      console.log(result);
      this.experienceInfos=result;
    },(error)=>{
      coexperienceInfonsole.log(error);
    })*/
    
  }
  paginatedExperienceInfos(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._experienceInfoService.getPaginatedExperienceInfos(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.experienceInfos=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchExperienceInfo(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._experienceInfoService.getPaginatedExperienceInfosWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.experienceInfos=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedExperienceInfos(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedExperienceInfos(this.pageNo,this.size);
      this.searchExperienceInfo();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedExperienceInfos(this.pageNo,this.size);
    this.searchExperienceInfo();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedExperienceInfos(pageNo,this.size);
    this.searchExperienceInfo();
  }
  deleteExperienceInfo(experienceInfo){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._experienceInfoService.deleteExperienceInfo(experienceInfo.id).subscribe((data)=>{
        this.experienceInfos.splice(this.experienceInfos.indexOf(experienceInfo),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewExperienceInfo(experienceInfo){  
     this._experienceInfoService.setter(experienceInfo);
     this._router.navigate(['/serviceProviderPanel/experienceInfos/view']);
   }
   editExperienceInfo(experienceInfo){  
     this._experienceInfoService.setter(experienceInfo);
     this._router.navigate(['/serviceProviderPanel/experienceInfos/experienceInfoForm']);
   }
   newExperienceInfo(){
   let experienceInfo = new ExperienceInfo();
    this._experienceInfoService.setter(experienceInfo);
     this._router.navigate(['/serviceProviderPanel/experienceInfos/experienceInfoForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.experienceInfos, 'ExperienceInfo');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("ExperienceInfo List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Service Provider", dataKey: "serviceProvider"},{title: "From Date", dataKey: "fromDate"},{title: "Thru Date", dataKey: "thruDate"},{title: "Description", dataKey: "description"},{title: "Is Active", dataKey: "isActive"},
    ];
    doc.autoTable(reportColumns, this.experienceInfos, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('ExperienceInfoList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ExperienceInfoList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
