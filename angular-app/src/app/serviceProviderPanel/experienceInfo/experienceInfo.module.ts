// Angular
import { NgModule } from '@angular/core';
import { ExperienceInfoRoutingModule } from './experienceInfo-routing.module';
import { ListexperienceInfoComponent } from './listexperienceInfo.component';
import { ExperienceInfoFormComponent } from './experienceInfo-form.component';
import { ExperienceInfoFormViewComponent } from './experienceInfo-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ExperienceInfoService } from '../../services/experienceInfo.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ServiceProviderService }  from '../../services/serviceProvider.service';

@NgModule({
  imports: [
    ExperienceInfoRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListexperienceInfoComponent,
    ExperienceInfoFormComponent,
    ExperienceInfoFormViewComponent
  ],
  providers: [ExperienceInfoService,
    ServiceProviderService,
    ExcelService  
  ],
})
export class ExperienceInfoModule { }
