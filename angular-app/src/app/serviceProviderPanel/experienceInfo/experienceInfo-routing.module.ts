import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListexperienceInfoComponent } from './listexperienceInfo.component';
import { ExperienceInfoFormComponent } from './experienceInfo-form.component';
import { ExperienceInfoFormViewComponent } from './experienceInfo-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListexperienceInfoComponent,
    data: {
      title: 'Experience Info List'
    }
  },{
      path: 'experienceInfoForm',
    component: ExperienceInfoFormComponent,
    data: {
      title: 'Experience Info Form'
    }
  },{
      path: 'view',
    component: ExperienceInfoFormViewComponent,
    data: {
      title: 'Experience Info Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExperienceInfoRoutingModule {}
