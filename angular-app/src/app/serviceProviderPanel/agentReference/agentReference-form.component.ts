import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AgentReference }  from '../../model/agentReference';
import { Router ,ActivatedRoute } from "@angular/router";
import { AgentReferenceService } from '../../services/agentReference.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ServiceProvider }  from '../../model/serviceProvider';
import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-agentReference-form',
  templateUrl: './agentReference-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class AgentReferenceFormComponent implements OnInit {
  private agentReference:AgentReference;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private serviceProviders:ServiceProvider[];  
  
serviceProvider_validationMessage = "";
refSP_validationMessage = "";
fromDate_validationMessage = "";
thruDate_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _agentReferenceService:AgentReferenceService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.agentReference=this._agentReferenceService.getter();
    this.agentReferenceValidationInitializer();  
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  agentReferenceValidationInitializer(){
     
this.serviceProvider_validationMessage = "";
this.refSP_validationMessage = "";
this.fromDate_validationMessage = "";
this.thruDate_validationMessage = "";
this.isActive_validationMessage = "";
  }
  clearAgentReferenceFormFields(){
     this.agentReference=this._agentReferenceService.getter();   
  }
  validationMessages(result){  
     
this.serviceProvider_validationMessage = result.serviceProvider_validationMessage
this.refSP_validationMessage = result.refSP_validationMessage
this.fromDate_validationMessage = result.fromDate_validationMessage
this.thruDate_validationMessage = result.thruDate_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
  }
  processAgentReferenceForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.agentReferenceValidationInitializer();
    if(this.agentReference.id==undefined){
       this._agentReferenceService.createAgentReference(this.agentReference).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/agentReferences']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._agentReferenceService.updateAgentReference(this.agentReference).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/agentReferences']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }

}
