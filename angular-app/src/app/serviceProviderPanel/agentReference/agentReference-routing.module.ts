import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListagentReferenceComponent } from './listagentReference.component';
import { AgentReferenceFormComponent } from './agentReference-form.component';
import { AgentReferenceFormViewComponent } from './agentReference-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListagentReferenceComponent,
    data: {
      title: 'Agent Reference List'
    }
  },{
      path: 'agentReferenceForm',
    component: AgentReferenceFormComponent,
    data: {
      title: 'Agent Reference Form'
    }
  },{
      path: 'view',
    component: AgentReferenceFormViewComponent,
    data: {
      title: 'Agent Reference Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentReferenceRoutingModule {}
