import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AgentReference }  from '../../model/agentReference';
import { Router ,ActivatedRoute} from "@angular/router";
import { AgentReferenceService } from '../../services/agentReference.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { ServiceProvider }  from '../../model/serviceProvider';
import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-agentReference-view-form',
  templateUrl: './agentReference-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class AgentReferenceFormViewComponent implements OnInit {
  private agentReference:AgentReference;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private serviceProviders:ServiceProvider[];  
  
serviceProvider_validationMessage = "";
refSP_validationMessage = "";
fromDate_validationMessage = "";
thruDate_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _agentReferenceService:AgentReferenceService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.agentReference=this._agentReferenceService.getter(); 
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newAgentReference(){
   let agentReference = new AgentReference();
    this._agentReferenceService.setter(agentReference);
     this._rotuer.navigate(['/serviceProviderPanel/agentReferences/agentReferenceForm']);   
   }
  agentReferenceEditAction(){
     this._agentReferenceService.setter(this.agentReference);
     this._rotuer.navigate(['/serviceProviderPanel/agentReferences/agentReferenceForm']);        
  }
  printAgentReferenceDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printAgentReferenceFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('AgentReferenceList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
