// Angular
import { NgModule } from '@angular/core';
import { AgentReferenceRoutingModule } from './agentReference-routing.module';
import { ListagentReferenceComponent } from './listagentReference.component';
import { AgentReferenceFormComponent } from './agentReference-form.component';
import { AgentReferenceFormViewComponent } from './agentReference-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AgentReferenceService } from '../../services/agentReference.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ServiceProviderService }  from '../../services/serviceProvider.service';

@NgModule({
  imports: [
    AgentReferenceRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListagentReferenceComponent,
    AgentReferenceFormComponent,
    AgentReferenceFormViewComponent
  ],
  providers: [AgentReferenceService,
    ServiceProviderService,
    ExcelService  
  ],
})
export class AgentReferenceModule { }
