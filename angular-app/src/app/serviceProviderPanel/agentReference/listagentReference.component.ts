import { Component, OnInit } from '@angular/core';
import { AgentReferenceService } from '../../services/agentReference.service';
import { AgentReference }  from '../../model/agentReference';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listagentReference',
  templateUrl: './listagentReference.component.html'
})
export class ListagentReferenceComponent implements OnInit {
  private agentReferences:AgentReference[];
  
  constructor(private _agentReferenceService: AgentReferenceService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchAgentReference();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._agentReferenceService.getAgentReferences().subscribe((result)=>{
      console.log(result);
      this.agentReferences=result;
    },(error)=>{
      coagentReferencensole.log(error);
    })*/
    
  }
  paginatedAgentReferences(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._agentReferenceService.getPaginatedAgentReferences(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.agentReferences=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchAgentReference(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._agentReferenceService.getPaginatedAgentReferencesWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.agentReferences=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedAgentReferences(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedAgentReferences(this.pageNo,this.size);
      this.searchAgentReference();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedAgentReferences(this.pageNo,this.size);
    this.searchAgentReference();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedAgentReferences(pageNo,this.size);
    this.searchAgentReference();
  }
  deleteAgentReference(agentReference){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._agentReferenceService.deleteAgentReference(agentReference.id).subscribe((data)=>{
        this.agentReferences.splice(this.agentReferences.indexOf(agentReference),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewAgentReference(agentReference){  
     this._agentReferenceService.setter(agentReference);
     this._router.navigate(['/serviceProviderPanel/agentReferences/view']);
   }
   editAgentReference(agentReference){  
     this._agentReferenceService.setter(agentReference);
     this._router.navigate(['/serviceProviderPanel/agentReferences/agentReferenceForm']);
   }
   newAgentReference(){
   let agentReference = new AgentReference();
    this._agentReferenceService.setter(agentReference);
     this._router.navigate(['/serviceProviderPanel/agentReferences/agentReferenceForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.agentReferences, 'AgentReference');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("AgentReference List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Service Provider", dataKey: "serviceProvider"},{title: "Ref SP", dataKey: "refSP"},{title: "From Date", dataKey: "fromDate"},{title: "Thru Date", dataKey: "thruDate"},{title: "Is Active", dataKey: "isActive"},
    ];
    doc.autoTable(reportColumns, this.agentReferences, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('AgentReferenceList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('AgentReferenceList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
