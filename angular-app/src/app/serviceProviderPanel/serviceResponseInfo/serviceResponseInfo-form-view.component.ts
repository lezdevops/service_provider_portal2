import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ServiceResponseInfo }  from '../../model/serviceResponseInfo';
import { Router ,ActivatedRoute} from "@angular/router";
import { ServiceResponseInfoService } from '../../services/serviceResponseInfo.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';

import { ServiceProvider }  from '../../model/serviceProvider';
import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-serviceResponseInfo-view-form',
  templateUrl: './serviceResponseInfo-tabbedForm-view.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ServiceResponseInfoFormViewComponent implements OnInit {
  private serviceResponseInfo:ServiceResponseInfo;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  private serviceProviders:ServiceProvider[];  
  
serviceProvider_validationMessage = "";
custMessage_validationMessage = "";
spAnswer_validationMessage = "";
sentDate_validationMessage = "";
responseDate_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _serviceResponseInfoService:ServiceResponseInfoService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }

  ngOnInit() {      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.serviceResponseInfo=this._serviceResponseInfoService.getter(); 
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
    
  newServiceResponseInfo(){
   let serviceResponseInfo = new ServiceResponseInfo();
    this._serviceResponseInfoService.setter(serviceResponseInfo);
     this._rotuer.navigate(['/serviceProviderPanel/serviceResponseInfos/serviceResponseInfoForm']);   
   }
  serviceResponseInfoEditAction(){
     this._serviceResponseInfoService.setter(this.serviceResponseInfo);
     this._rotuer.navigate(['/serviceProviderPanel/serviceResponseInfos/serviceResponseInfoForm']);        
  }
  printServiceResponseInfoDetails() { 
    this.spinnerService.show(); 
     var data = document.getElementById('printServiceResponseInfoFormContainer');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ServiceResponseInfoList.pdf'); // Generated PDF 
       this.spinnerService.hide();  
     });  
   } 

}
