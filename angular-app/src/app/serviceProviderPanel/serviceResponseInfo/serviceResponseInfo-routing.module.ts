import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListserviceResponseInfoComponent } from './listserviceResponseInfo.component';
import { ServiceResponseInfoFormComponent } from './serviceResponseInfo-form.component';
import { ServiceResponseInfoFormViewComponent } from './serviceResponseInfo-form-view.component';

const routes: Routes = [
  {    
    path: '',
    component: ListserviceResponseInfoComponent,
    data: {
      title: 'Service Response Info List'
    }
  },{
      path: 'serviceResponseInfoForm',
    component: ServiceResponseInfoFormComponent,
    data: {
      title: 'Service Response Info Form'
    }
  },{
      path: 'view',
    component: ServiceResponseInfoFormViewComponent,
    data: {
      title: 'Service Response Info Details'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceResponseInfoRoutingModule {}
