import { Component, OnInit } from '@angular/core';
import { ServiceResponseInfoService } from '../../services/serviceResponseInfo.service';
import { ServiceResponseInfo }  from '../../model/serviceResponseInfo';
import { Router ,ActivatedRoute} from "@angular/router";
import { AuthenticationService } from "../../authentication/authentication.service";
import { ExcelService } from '../../shared/services/excel-service';
import * as jsPDFAuto from 'jspdf-autotable'; 
import * as jsPDF from 'jspdf'; 
import html2canvas from 'html2canvas';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listserviceResponseInfo',
  templateUrl: './listserviceResponseInfo.component.html'
})
export class ListserviceResponseInfoComponent implements OnInit {
  private serviceResponseInfos:ServiceResponseInfo[];
  
  constructor(private _serviceResponseInfoService: ServiceResponseInfoService,
     private _router:Router, 
     private authService: AuthenticationService,
     private excelService:ExcelService,
     private spinnerService: Ng4LoadingSpinnerService
    ) { }
  pageNo = 0;
  size = 5;
  searchToken = '';  
  totalPages = 0;
  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
          this.searchServiceResponseInfo();          
      } else {
        this._router.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
    /*this._serviceResponseInfoService.getServiceResponseInfos().subscribe((result)=>{
      console.log(result);
      this.serviceResponseInfos=result;
    },(error)=>{
      coserviceResponseInfonsole.log(error);
    })*/
    
  }
  paginatedServiceResponseInfos(pageNo,size){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._serviceResponseInfoService.getPaginatedServiceResponseInfos(pageNo,size).subscribe((result)=>{
      console.log(result.content);
      this.serviceResponseInfos=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })
  }
  searchServiceResponseInfo(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    if(this.searchToken!=""){
      this._serviceResponseInfoService.getPaginatedServiceResponseInfosWithSearchToken(this.pageNo,this.size,this.searchToken).subscribe((result)=>{
      console.log(result.content);
      this.serviceResponseInfos=result.content;
      this.totalPages = result.totalPages;
      this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    })  
    }else{
       this.paginatedServiceResponseInfos(this.pageNo,this.size); 
    }
    
  }
  nextPage(){
    this.pageNo ++;
    //this.paginatedServiceResponseInfos(this.pageNo,this.size);
      this.searchServiceResponseInfo();
  }
  previousPage(){
    this.pageNo --;
    //this.paginatedServiceResponseInfos(this.pageNo,this.size);
    this.searchServiceResponseInfo();
  }
  counter(i) {
    return new Array(i);
  }
  gotoPage(pageNo){
    this.pageNo = pageNo;
    //this.paginatedServiceResponseInfos(pageNo,this.size);
    this.searchServiceResponseInfo();
  }
  deleteServiceResponseInfo(serviceResponseInfo){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this._serviceResponseInfoService.deleteServiceResponseInfo(serviceResponseInfo.id).subscribe((data)=>{
        this.serviceResponseInfos.splice(this.serviceResponseInfos.indexOf(serviceResponseInfo),1);
        this.spinnerService.hide();
    },(error)=>{
      console.log(error);
        this._router.navigate(['/124']);
    });
  }

   viewServiceResponseInfo(serviceResponseInfo){  
     this._serviceResponseInfoService.setter(serviceResponseInfo);
     this._router.navigate(['/serviceProviderPanel/serviceResponseInfos/view']);
   }
   editServiceResponseInfo(serviceResponseInfo){  
     this._serviceResponseInfoService.setter(serviceResponseInfo);
     this._router.navigate(['/serviceProviderPanel/serviceResponseInfos/serviceResponseInfoForm']);
   }
   newServiceResponseInfo(){
   let serviceResponseInfo = new ServiceResponseInfo();
    this._serviceResponseInfoService.setter(serviceResponseInfo);
     this._router.navigate(['/serviceProviderPanel/serviceResponseInfos/serviceResponseInfoForm']);   
   }
   exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.serviceResponseInfos, 'ServiceResponseInfo');
   }
   exportAsPDF(): void{
    this.spinnerService.show();
    var doc = new jsPDF(); jsPDFAuto;
    var totalPagesExp = "{total_pages_count_string}";
  
    var pageContent = function (data) {
        // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        
        doc.text("ServiceResponseInfo List Report", data.settings.margin.right + 15, 22);
  
        // FOOTER
        var str = "Page " + data.pageCount;
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            str = str + " of " + totalPagesExp;
        }
        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight  - 10);
        this.spinnerService.hide();
    };
    /*var columns = [
      {title: "ID", dataKey: "id"},
    ];*/
    var reportColumns = [
      {title: "Service Provider", dataKey: "serviceProvider"},{title: "Cust Message", dataKey: "custMessage"},{title: "Sp Answer", dataKey: "spAnswer"},{title: "Sent Date", dataKey: "sentDate"},{title: "Response Date", dataKey: "responseDate"},{title: "Is Active", dataKey: "isActive"},
    ];
    doc.autoTable(reportColumns, this.serviceResponseInfos, {
        addPageContent: pageContent,
        margin: {top: 30}
    });
  
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
  
    doc.save('ServiceResponseInfoList.pdf');
   }
   captureScreen() {  
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
     var data = document.getElementById('contentToConvert');  
     html2canvas(data).then(canvas => {  
       // Few necessary setting options  
       var imgWidth = 208;   
       var pageHeight = 295;    
       var imgHeight = canvas.height * imgWidth / canvas.width;  
       var heightLeft = imgHeight;  
   
       const contentDataURL = canvas.toDataURL('image/png')  
       let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
       var position = 0;  
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
       pdf.save('ServiceResponseInfoList.pdf'); // Generated PDF
       this.spinnerService.hide();  
     });  
   }  

}
