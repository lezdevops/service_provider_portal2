// Angular
import { NgModule } from '@angular/core';
import { ServiceResponseInfoRoutingModule } from './serviceResponseInfo-routing.module';
import { ListserviceResponseInfoComponent } from './listserviceResponseInfo.component';
import { ServiceResponseInfoFormComponent } from './serviceResponseInfo-form.component';
import { ServiceResponseInfoFormViewComponent } from './serviceResponseInfo-form-view.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../../shared/services/excel-service';

import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ServiceResponseInfoService } from '../../services/serviceResponseInfo.service';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';

import { ServiceProviderService }  from '../../services/serviceProvider.service';

@NgModule({
  imports: [
    ServiceResponseInfoRoutingModule,
    CommonModule,
    TabsModule,
    ModalModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [
    ListserviceResponseInfoComponent,
    ServiceResponseInfoFormComponent,
    ServiceResponseInfoFormViewComponent
  ],
  providers: [ServiceResponseInfoService,
    ServiceProviderService,
    ExcelService  
  ],
})
export class ServiceResponseInfoModule { }
