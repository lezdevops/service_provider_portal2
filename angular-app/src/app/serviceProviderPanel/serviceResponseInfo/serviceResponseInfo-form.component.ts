import { Component, OnInit } from '@angular/core';
import { SecurityContext, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ServiceResponseInfo }  from '../../model/serviceResponseInfo';
import { Router ,ActivatedRoute } from "@angular/router";
import { ServiceResponseInfoService } from '../../services/serviceResponseInfo.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
import { AuthenticationService } from "../../authentication/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ServiceProvider }  from '../../model/serviceProvider';
import { ServiceProviderService }  from '../../services/serviceProvider.service';

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: 'success' });
}
@Component({
  selector: 'app-serviceResponseInfo-form',
  templateUrl: './serviceResponseInfo-tabbedForm.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
  .alert-md-local {
    background-color: #009688;
    border-color: #00695C;
    color: #fff;
  }
  `
  ],
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
})
export class ServiceResponseInfoFormComponent implements OnInit {
  private serviceResponseInfo:ServiceResponseInfo;
  hideElement = true;

  simpleItems = [true, 'Two', 3];
  selectedSimpleItem = '';
  
  //private dependentEntitys:DependentEntity[];
  private serviceProviders:ServiceProvider[];  
  
serviceProvider_validationMessage = "";
custMessage_validationMessage = "";
spAnswer_validationMessage = "";
sentDate_validationMessage = "";
responseDate_validationMessage = "";
isActive_validationMessage = "";
    
  constructor(
    private _serviceResponseInfoService:ServiceResponseInfoService,
    private _rotuer:Router, 
    private authService: AuthenticationService,
    private _serviceProviderService:ServiceProviderService,
  sanitizer: DomSanitizer,
  private route: ActivatedRoute,
  private spinnerService: Ng4LoadingSpinnerService) {
   }


  ngOnInit() {
      
    this.authService.isLoggedIn.subscribe((res) => {
      if (res) {
                    
      } else {
        this._rotuer.navigate(['/login']);
      }
    },(error)=>{
      console.log(error);
      this._rotuer.navigate(['/124']);
    });
    this.serviceResponseInfo=this._serviceResponseInfoService.getter();
    this.serviceResponseInfoValidationInitializer();  
    this._serviceProviderService.getServiceProviders().subscribe((serviceProviders)=>{
   			      this.serviceProviders=serviceProviders;
   			    },(error)=>{
   			      //console.log(error);
   			    });  
  }
  serviceResponseInfoValidationInitializer(){
     
this.serviceProvider_validationMessage = "";
this.custMessage_validationMessage = "";
this.spAnswer_validationMessage = "";
this.sentDate_validationMessage = "";
this.responseDate_validationMessage = "";
this.isActive_validationMessage = "";
  }
  clearServiceResponseInfoFormFields(){
     this.serviceResponseInfo=this._serviceResponseInfoService.getter();   
  }
  validationMessages(result){  
     
this.serviceProvider_validationMessage = result.serviceProvider_validationMessage
this.custMessage_validationMessage = result.custMessage_validationMessage
this.spAnswer_validationMessage = result.spAnswer_validationMessage
this.sentDate_validationMessage = result.sentDate_validationMessage
this.responseDate_validationMessage = result.responseDate_validationMessage
this.isActive_validationMessage = result.isActive_validationMessage
  }
  processServiceResponseInfoForm(){
    this.spinnerService.show();
    setTimeout(()=>this.spinnerService.hide(),10000);
    this.serviceResponseInfoValidationInitializer();
    if(this.serviceResponseInfo.id==undefined){
       this._serviceResponseInfoService.createServiceResponseInfo(this.serviceResponseInfo).subscribe((result)=>{
         console.log(result);
           if(result.error!=""){
               this.hideElement = false;
               this.validationMessages(result);               
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/serviceResponseInfos']);
           }      
           this.spinnerService.hide();   
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }else{
       this._serviceResponseInfoService.updateServiceResponseInfo(this.serviceResponseInfo).subscribe((result)=>{
          if(result.error!=""){
              this.hideElement = false;
              this.validationMessages(result);
           }else{
              this._rotuer.navigate(['/serviceProviderPanel/serviceResponseInfos']);
           }
           this.spinnerService.hide();  
       },(error)=>{
         console.log(error);
           this._rotuer.navigate(['/124']);
       });
    }
  }

}
