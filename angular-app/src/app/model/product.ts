import { Moment } from 'moment';

import { SubCategory }  from '../model/subCategory'; 
export class Product {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
slCode:string;
name:string;
subCategory:SubCategory;
purchasePrice:Number;
sellingPrice:Number;
note:string;
status:string;
imageAddress:string;

}
