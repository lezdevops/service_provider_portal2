import { Moment } from 'moment';

import { UserGroup }  from '../model/userGroup'; 
export class User {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
userName:string;
password:string;
passwordHint:string;
email:string;
firstName:string;
lastName:string;
active:Number;
userGroup:UserGroup;
createdBy:string;
created:Moment;
updated:Moment;

}
