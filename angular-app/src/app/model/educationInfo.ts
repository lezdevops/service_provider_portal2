import { Moment } from 'moment';

import { ServiceProvider }  from '../model/serviceProvider'; 
export class EducationInfo {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
serviceProvider:ServiceProvider;
instituteName:string;
fromYear:string;
thruYear:string;
degree:string;
isActive:string;

}
