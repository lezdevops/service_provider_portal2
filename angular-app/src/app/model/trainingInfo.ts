import { Moment } from 'moment';

import { ServiceProvider }  from '../model/serviceProvider';import { TrainingType }  from '../model/trainingType'; 
export class TrainingInfo {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
serviceProvider:ServiceProvider;
trainingType:TrainingType;
monthYear:string;
description:string;
certificationDoc:string;
uploadedDate:Moment;

}
