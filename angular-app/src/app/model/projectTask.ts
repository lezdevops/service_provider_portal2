import { Moment } from 'moment';

import { ProjectStory }  from '../model/projectStory'; 
export class ProjectTask {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
projectStory:ProjectStory;
description:string;
startDate:Moment;
endDate:Moment;
createdDate:Moment;
document:string;

}
