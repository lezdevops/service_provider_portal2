import { Moment } from 'moment';

import { ServiceProvider }  from '../model/serviceProvider'; 
export class ExperienceInfo {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
serviceProvider:ServiceProvider;
fromDate:Moment;
thruDate:Moment;
description:string;
isActive:string;

}
