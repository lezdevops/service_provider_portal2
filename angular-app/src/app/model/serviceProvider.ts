import { Moment } from 'moment';

import { User }  from '../model/user';import { SpLevel }  from '../model/spLevel'; 
export class ServiceProvider {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
name:string;
about:string;
address:string;
zipCode:string;
phoneNo:string;
nidNo:string;
email:string;
user:User;
spLevel:SpLevel;
gender:string;
desiredHourlyRate:Number;
desiredDailyRate:Number;
serviceArea:Number;
availability:string;
isActive:string;
startTime:string;
endTime:string;
createdDate:Moment;
profileImage:string;

}
