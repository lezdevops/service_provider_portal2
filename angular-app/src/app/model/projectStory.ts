import { Moment } from 'moment';

import { Project }  from '../model/project'; 
export class ProjectStory {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
project:Project;
name:string;
description:string;
startDate:Moment;
endDate:Moment;
createdDate:Moment;
document:string;

}
