import { Moment } from 'moment';

import { NotificationProcess }  from '../model/notificationProcess'; 
import { MessageReceiver }  from '../model/messageReceiver'; 

export class NotificationRouteMap {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
    notificationProcess:NotificationProcess;
    messageReceivers:any;
senderId:string;
receiverId:string;
isActive:boolean;

}
