import { Moment } from 'moment';

import { ProjectTask }  from '../model/projectTask';import { Resource }  from '../model/resource';import { TaskState }  from '../model/taskState'; 
export class ProjectSubtask {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
projectTask:ProjectTask;
name:string;
description:string;
startDate:Moment;
endDate:Moment;
createdDate:Moment;
resource:Resource;
taskState:TaskState;
completed:string;
document:string;

}
