import { Moment } from 'moment';

import { ApprovalQueue }  from '../model/approvalQueue'; 
export class ApprovalQueueDetails {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
approvalQueue:ApprovalQueue;
approverId:string;
level:Number;
isLastApprover:boolean;
isApproved:boolean;

}
