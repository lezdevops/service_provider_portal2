import { Moment } from 'moment';

import { ServiceProvider }  from '../model/serviceProvider'; 
export class AgentReference {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
serviceProvider:ServiceProvider;
refSP:string;
fromDate:Moment;
thruDate:Moment;
isActive:string;

}
