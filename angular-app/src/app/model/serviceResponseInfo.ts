import { Moment } from 'moment';

import { ServiceProvider }  from '../model/serviceProvider'; 
export class ServiceResponseInfo {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
serviceProvider:ServiceProvider;
custMessage:string;
spAnswer:string;
sentDate:Moment;
responseDate:Moment;
isActive:string;

}
