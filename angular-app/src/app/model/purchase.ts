import { Moment } from 'moment';

import { Supplier }  from '../model/supplier'; 
export class Purchase {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
purchaseCode:string;
supplier:Supplier;
date:Moment;
paid:Number;
due:Number;
paymentType:string;

}
