import { Moment } from 'moment';

import { ServiceProvider }  from '../model/serviceProvider';import { ServiceType }  from '../model/serviceType';import { Location }  from '../model/location'; 
export class SpServiceList {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
serviceProvider:ServiceProvider;
serviceDetails:string;
serviceType:ServiceType;
visitCount:Number;
location:Location;
requestCount:Number;
startTime:string;
endTime:string;
isActive:string;
availFrom:Moment;
availTo:Moment;

}
