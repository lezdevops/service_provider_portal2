import { Moment } from 'moment';

import { Customer }  from '../model/customer'; 
export class Sales {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
invoiceCode:string;
customer:Customer;
date:Moment;
discountPercentage:Number;
vat:Number;
paid:Number;
due:Number;
paymentType:string;

}
