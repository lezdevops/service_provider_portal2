import { Moment } from 'moment';

import { User }  from '../model/user'; 
export class UserGroup {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
name:string;
metaData:string;
websiteAddress:string;
phoneNo:string;
email:string;
address:string;
currency:string;
imageAddress:string;
user:User;
created:Moment;
updated:Moment;

}
