import { Moment } from 'moment';

import { ServiceProvider }  from '../model/serviceProvider'; 
export class JobHistory {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
serviceProvider:ServiceProvider;
employerName:string;
jobTitle:string;
fromDate:Moment;
thruDate:Moment;
location:string;
uploadedDoc:string;
description:string;

}
