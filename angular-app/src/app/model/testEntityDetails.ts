import { Moment } from 'moment';

import { TestEntity }  from '../model/testEntity'; 
export class TestEntityDetails {
    /*id:Number;
    fname:string;
    lname:string;*/
    id:Number;
testEntity:TestEntity;
number:Number;
textBox:string;
description:string;
checkbox:boolean;
createdDate:Moment;
datePicker:Moment;
docFile:string;
imageFile:string;

}
