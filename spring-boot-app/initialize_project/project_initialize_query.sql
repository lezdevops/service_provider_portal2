create database service_provider_portal;
--Run Application
use service_provider_portal;
--One UserGroup Has Multiple Employees and Users

--$2a$10$htPBZ332MWgG7GLjTj8ED.lmGWDKCq/DmEq1EPGuRNBr6LkMNb17y
--1234
insert into user (user_name,password,password_hint,email,first_name,last_name,active) 
values ('admin','$2a$10$htPBZ332MWgG7GLjTj8ED.lmGWDKCq/DmEq1EPGuRNBr6LkMNb17y','1234','admin@service_providers.com','Service Provider','Admin',1);


insert into user_group (name,meta_data,website_address,phone_no,email,user_id) 
values ('service_providers','Online Service Portal','service_providers.com','017XX-XXXXX','info@service_providers.com',1);


insert into role(name) values('SUPER_ADMIN');
insert into user_role(user_id,role_id) values(1,1);

insert into permission(name) values('USER_READ');
insert into role_permission(role_id,permission_id) values(1,1);

insert into permission(name) values('ROLE_READ');
insert into role_permission(role_id,permission_id) values(1,2);

insert into permission(name) values('PERMISSION_READ');
insert into role_permission(role_id,permission_id) values(1,3);

insert into permission(name) values('PERMISSION_CREATE');
insert into role_permission(role_id,permission_id) values(1,4);