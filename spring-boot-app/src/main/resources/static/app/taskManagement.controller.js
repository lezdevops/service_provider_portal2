(function() {
	'use strict';

	angular.module('app').controller('TaskManagementController',
			TaskManagementController);

	TaskManagementController.$inject = [ '$http' ];

	function TaskManagementController($http, $resource, $scope) {
		var http = $http;
		var vm = this;
		vm.page = {};
		vm.addTaskManagementMessages = [];
		vm.taskManagements = [];
		vm.multipleTaskManagements = [];
		vm.editTaskManagement = editTaskManagement;
		vm.saveTaskManagement = saveTaskManagement;
		vm.saveMultipleTaskManagementData = saveMultipleTaskManagementData;
		vm.getPaginatedTaskManagements = getPaginatedTaskManagements;
		vm.getAll = getAll;
		vm.sendNotification = sendNotification;
		vm.getUserLogins = getUserLogins;
		vm.getUserLogins = getUserLogins;

		vm.addRow = addRow;
		vm.deleteTaskManagement = deleteTaskManagement;
		vm.previousPage = previousPage;
		vm.nextPage = nextPage;

		vm.page.number = 0;
		vm.page.size = 5;

		init();

		function init() {
			getPaginatedTaskManagements(vm.page.number, vm.page.size);
			getAll();
			getUserLogins();
			getUserLogins();

		}
		function saveTaskManagement() {
			var taskManagement = {};

			taskManagement.taskId = vm.taskId;
			taskManagement.assigneeId = vm.assigneeId;
			taskManagement.reporterId = vm.reporterId;
			taskManagement.taskDetails = vm.taskDetails;
			taskManagement.createdDate = vm.createdDate;
			taskManagement.planReportDate = vm.planReportDate;
			taskManagement.actualReportDate = vm.actualReportDate;
			taskManagement.completeParcentage = vm.completeParcentage;
			taskManagement.taskStatus = vm.taskStatus;

			var url = "/taskManagements/save";
			$http.post(url, taskManagement).then(function(response) {
				vm.addTaskManagementMessages = response.data;
				getPaginatedTaskManagements(vm.page.number, vm.page.size);
				getAll();
				clearFields();
				sendNotification();
			});
		}

		function sendNotification() {
			var notificationMap = {};

			notificationMap.notificationMessage = vm.notificationMessage;
			notificationMap.notificationReceivers = vm.notificationReceivers;
			notificationMap.notifyByEmail = vm.notifyByEmail;
			notificationMap.notifyBySMS = vm.notifyBySMS;

			var url = "/taskManagements/sendNotification";
			$http.post(url, notificationMap).then(function(response) {
				vm.notificationSentResult = response.data;
				clearNotificationFields();
			});
		}
		function clearNotificationFields() {

			vm.notificationMessage = "";
			vm.districtShortName = "";
			vm.distFullName = "";
			vm.createdDate = "";

		}
		function addRow() {
			vm.multipleTaskManagements.push({
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true

			});
		}
		;
		function saveMultipleTaskManagementData() {
			$http({
				method : "post",
				url : "/taskManagements/saveMultipleTaskManagementData",
				data : vm.multipleTaskManagements,
			}).success(function(data) {
				// /vm.addTaskManagementMessages = response.data;
				vm.multipleTaskManagements = response.data;
				getPaginatedTaskManagements(vm.page.number, vm.page.size);
			}).error(function(err) {
				vm.Message = err.Message;
			})
		}
		;
		function editTaskManagement(taskManagement) {

			vm.taskId = taskManagement.taskId;
			vm.assigneeId = taskManagement.assigneeId;
			vm.reporterId = taskManagement.reporterId;
			vm.taskDetails = taskManagement.taskDetails;
			vm.createdDate = taskManagement.createdDate;
			vm.planReportDate = taskManagement.planReportDate;
			vm.actualReportDate = taskManagement.actualReportDate;
			vm.completeParcentage = taskManagement.completeParcentage;
			vm.taskStatus = taskManagement.taskStatus;

		}
		function clearFields() {

			vm.taskId = "";
			vm.assigneeId = "";
			vm.reporterId = "";
			vm.taskDetails = "";
			vm.createdDate = "";
			vm.planReportDate = "";
			vm.actualReportDate = "";
			vm.completeParcentage = "";
			vm.taskStatus = "";

		}

		function getAll() {
			var url = "/taskManagements/all";
			var taskManagementsPromise = $http.get(url);
			taskManagementsPromise.then(function(response) {
				vm.multipleTaskManagements = response.data;
			});
		}
		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}
		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}

		function deleteTaskManagement(id) {
			var url = "/taskManagements/delete/" + id;
			$http.post(url).then(function(response) {
				vm.deleteMessage = response.data;
				getPaginatedTaskManagements(vm.page.number, vm.page.size);
				getAll();
			});
		}
		function getPaginatedTaskManagements(pageNumber, size) {
			var url = "/taskManagements/get?page=" + pageNumber + "&size="
					+ size;
			var taskManagementsPromise = $http.get(url);
			taskManagementsPromise.then(function(response) {
				vm.taskManagements = response.data.content;

				vm.page.number = response.data.number;
				vm.page.size = response.data.size;
				vm.page.numberOfElements = response.data.numberOfElements;
				vm.page.first = response.data.first;
				vm.page.last = response.data.last;
				vm.page.totalPages = response.data.totalPages;
				vm.page.totalElements = response.data.totalElements;
			});
		}
		function previousPage() {
			if (!vm.page.first) {
				vm.page.number = vm.page.number - 1;
				getPaginatedTaskManagements(vm.page.number, vm.page.size);
			}
		}
		function nextPage() {
			if (!vm.page.last) {
				vm.page.number = vm.page.number + 1;
				getPaginatedTaskManagements(vm.page.number, vm.page.size);
			}

		}
	}
})();
