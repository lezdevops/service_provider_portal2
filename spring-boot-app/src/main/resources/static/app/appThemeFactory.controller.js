(function() {
	'use strict';

	angular.module('app').controller('AppThemeFactoryController',
			AppThemeFactoryController);

	AppThemeFactoryController.$inject = [ '$http' ];

	function AppThemeFactoryController($http, $resource, $scope) {
		var http = $http;
		var vm = this;
		vm.page = {};
		vm.addAppThemeFactoryMessages = [];
		vm.appThemeFactorys = [];
		vm.multipleAppThemeFactorys = [];
		vm.editAppThemeFactory = editAppThemeFactory;
		vm.saveAppThemeFactory = saveAppThemeFactory;
		vm.saveMultipleAppThemeFactoryData = saveMultipleAppThemeFactoryData;
		vm.getPaginatedAppThemeFactorys = getPaginatedAppThemeFactorys;
		vm.getAll = getAll;
		vm.getUserLogins = getUserLogins;

		vm.addRow = addRow;
		vm.deleteAppThemeFactory = deleteAppThemeFactory;
		vm.previousPage = previousPage;
		vm.nextPage = nextPage;

		vm.page.number = 0;
		vm.page.size = 25;

		init();

		function init() {
			getPaginatedAppThemeFactorys(vm.page.number, vm.page.size);
			getAll();
			getUserLogins();

		}
		function saveAppThemeFactory() {
			var appThemeFactory = {};

			appThemeFactory.themeFactoryId = vm.themeFactoryId;
			appThemeFactory.themeFileId = vm.themeFileId;
			appThemeFactory.fileName = vm.fileName;
			appThemeFactory.fileType = vm.fileType;
			appThemeFactory.fileContent = vm.fileContent;
			appThemeFactory.firstRowContent = vm.firstRowContent;
			appThemeFactory.secondRowContent = vm.secondRowContent;
			appThemeFactory.thirdRowContent = vm.thirdRowContent;
			appThemeFactory.notificationContent = vm.notificationContent;
			appThemeFactory.taskManagementContent = vm.taskManagementContent;

			var url = "/appThemeFactorys/save";
			$http.post(url, appThemeFactory).then(function(response) {
				vm.addAppThemeFactoryMessages = response.data;
				getPaginatedAppThemeFactorys(vm.page.number, vm.page.size);
				getAll();
				clearFields();

				if (vm.isAllowNotification) {
					sendNotification();
				}
				if (vm.isAllowSendAsTask) {
					sendAsTaskAssignment();
				}
			});
		}

		function sendNotification() {
			var notificationMap = {};

			notificationMap.notificationMessage = vm.notificationMessage;
			notificationMap.notificationReceivers = vm.notificationReceivers;
			notificationMap.notifyByEmail = vm.notifyByEmail;
			notificationMap.notifyBySMS = vm.notifyBySMS;

			var url = "/students/sendNotification";
			$http.post(url, notificationMap).then(function(response) {
				// vm.notificationSentResult = response.data;
				alert('Notification Sent Successfully');
				clearNotificationFields();
			});
		}

		function sendAsTaskAssignment() {
			var taskManagement = {};
			taskManagement.assigneeId = vm.assigneeId;
			taskManagement.reporterId = vm.reporterId;
			taskManagement.taskDetails = vm.taskDetails;
			// taskManagement.createdDate = vm.createdDate;
			// taskManagement.planReportDate = vm.planReportDate;
			// taskManagement.actualReportDate = vm.actualReportDate;
			taskManagement.completeParcentage = 40;
			taskManagement.taskStatus = 'Assigned';

			var url = "/taskManagements/save";
			$http.post(url, taskManagement).then(function(response) {
				// vm.addTaskManagementMessages = response.data;
				alert("Task Assigned Successfully");
			});
		}
		function clearNotificationFields() {

			vm.notificationMessage = "";
			vm.districtShortName = "";
			vm.distFullName = "";
			vm.createdDate = "";

		}
		function addRow() {
			vm.multipleAppThemeFactorys.push({
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true

			});
		}
		;
		function saveMultipleAppThemeFactoryData() {
			$http({
				method : "post",
				url : "/appThemeFactorys/saveMultipleAppThemeFactoryData",
				data : vm.multipleAppThemeFactorys,
			}).then(function(response) {
				// /vm.addAppThemeFactoryMessages = response.data;
				vm.multipleAppThemeFactorys = response.data;
				getPaginatedAppThemeFactorys(vm.page.number, vm.page.size);
			});
		}
		;
		function editAppThemeFactory(appThemeFactory) {

			vm.themeFactoryId = appThemeFactory.themeFactoryId;
			vm.themeFileId = appThemeFactory.themeFileId;
			vm.fileName = appThemeFactory.fileName;
			vm.fileType = appThemeFactory.fileType;
			vm.fileContent = appThemeFactory.fileContent;
			vm.firstRowContent = appThemeFactory.firstRowContent;
			vm.secondRowContent = appThemeFactory.secondRowContent;
			vm.thirdRowContent = appThemeFactory.thirdRowContent;
			vm.notificationContent = appThemeFactory.notificationContent;
			vm.taskManagementContent = appThemeFactory.taskManagementContent;

		}
		function clearFields() {

			vm.themeFactoryId = "";
			vm.themeFileId = "";
			vm.fileName = "";
			vm.fileType = "";
			vm.fileContent = "";
			vm.firstRowContent = "";
			vm.secondRowContent = "";
			vm.thirdRowContent = "";
			vm.notificationContent = "";
			vm.taskManagementContent = "";

		}

		function getAll() {
			var url = "/appThemeFactorys/all";
			var appThemeFactorysPromise = $http.get(url);
			appThemeFactorysPromise.then(function(response) {
				vm.multipleAppThemeFactorys = response.data;
			});
		}
		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}

		function deleteAppThemeFactory(id) {
			var url = "/appThemeFactorys/delete/" + id;
			if (confirm("Are you sure?")) {
				$http.post(url).then(function(response) {
					vm.deleteMessage = response.data;
				});
				getPaginatedAppThemeFactorys(vm.page.number, vm.page.size);
				getAll();
			}
		}
		function getPaginatedAppThemeFactorys(pageNumber, size) {
			var url = "/appThemeFactorys/get?page=" + pageNumber + "&size="
					+ size;
			var appThemeFactorysPromise = $http.get(url);
			appThemeFactorysPromise.then(function(response) {
				vm.appThemeFactorys = response.data.content;

				vm.page.number = response.data.number;
				vm.page.size = response.data.size;
				vm.page.numberOfElements = response.data.numberOfElements;
				vm.page.first = response.data.first;
				vm.page.last = response.data.last;
				vm.page.totalPages = response.data.totalPages;
				vm.page.totalElements = response.data.totalElements;
			});
		}
		function previousPage() {
			if (!vm.page.first) {
				vm.page.number = vm.page.number - 1;
				getPaginatedAppThemeFactorys(vm.page.number, vm.page.size);
			}
		}
		function nextPage() {
			if (!vm.page.last) {
				vm.page.number = vm.page.number + 1;
				getPaginatedAppThemeFactorys(vm.page.number, vm.page.size);
			}

		}
	}
})();
