(function() {
	'use strict';
	angular.module('app').controller('I18NController', I18NController);

	I18NController.$inject = [ '$http' ];

	function I18NController($http, $resource, $scope, fileUpload) {
		var http = $http;
		var vm = this;
		vm.resourceName = "student";
		vm.page = {};
		vm.i18NPropertiesList = [];
		vm.i18nPropertiesesList = [];
		vm.saveI18N = saveI18N;
		vm.geti18NPropertiesList = geti18NPropertiesList;
		vm.loadPropertiesDetails = loadPropertiesDetails;
		
		init();
		function init() {
			geti18NPropertiesList();
		}
		function geti18NPropertiesList() {
			if (vm.applicationName!==undefined) {
				var url = "/appProperties/geti18NPropertiesList/"+vm.applicationName;
				$http.post(url).then(function(response) {
					vm.i18NPropertiesList = response.data;
				});				
			}
		}
		function loadPropertiesDetails() {
			var pageName = vm.i18nFileName;
			var pageName = pageName.split(".")[0];
			vm.resourceName = pageName.split("_")[0];
			var applicationName = vm.applicationName;
			//alert(vm.applicationName);
			//return;
			///loadPropertiesDetails/{applicationName}/{pageName}
			var url = "/appProperties/loadPropertiesDetails/"+applicationName+"/"+pageName;
			
			$http.post(url).then(function(response) {
				vm.i18nPropertiesesList = response.data;
			});
		}
		
		function saveI18N() {
			var payload = {};
			payload.applicationName = vm.applicationName;
			payload.resourceName = vm.resourceName;
			payload.appPropertiess = vm.i18nPropertiesesList;

			var url = "/appProperties/appPropertiesSave";
			$http.post(url, payload).then(function(response) {
				vm.saveMessage = response.data;
				vm.i18nPropertiesesList = [];
				alert(vm.saveMessage);
			});
		}
		function addRow() {
			vm.i18nPropertiesesList.push({
				column1 : true,
				column1 : true
			});
		}

	}
})();
