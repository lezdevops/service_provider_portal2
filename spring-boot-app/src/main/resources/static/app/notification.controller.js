(function() {
	'use strict';

	angular.module('app').controller('NotificationController',
			NotificationController);

	NotificationController.$inject = [ '$http' ];

	function NotificationController($http, $resource, $scope) {
		var http = $http;
		var vm = this;
		vm.page = {};
		vm.addNotificationMessages = [];
		vm.notifications = [];
		vm.multipleNotifications = [];
		vm.editNotification = editNotification;
		vm.saveNotification = saveNotification;
		vm.saveMultipleNotificationData = saveMultipleNotificationData;
		vm.getPaginatedNotifications = getPaginatedNotifications;
		vm.getAll = getAll;
		vm.getUserLogins = getUserLogins;

		vm.addRow = addRow;
		vm.deleteNotification = deleteNotification;
		vm.previousPage = previousPage;
		vm.nextPage = nextPage;

		vm.page.number = 0;
		vm.page.size = 5;

		init();

		function init() {
			getPaginatedNotifications(vm.page.number, vm.page.size);
			getAll();
			getUserLogins();

		}
		function saveNotification() {
			var notification = {};

			notification.notificationId = vm.notificationId;
			notification.notificationMessage = vm.notificationMessage;
			notification.createdBy = vm.createdBy;
			notification.createdDate = vm.createdDate;
			notification.notificationValidityDays = vm.notificationValidityDays;

			var url = "/notifications/save";
			$http.post(url, notification).then(function(response) {
				vm.addNotificationMessages = response.data;
				getPaginatedNotifications(vm.page.number, vm.page.size);
				clearFields();
			});
		}
		function addRow() {
			vm.multipleNotifications.push({
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true

			});
		}
		;
		function saveMultipleNotificationData() {
			$http({
				method : "post",
				url : "/notifications/saveMultipleNotificationData",
				data : vm.multipleNotifications,
			}).success(function(data) {
				// /vm.addNotificationMessages = response.data;
				vm.multipleNotifications = response.data;
			}).error(function(err) {
				vm.Message = err.Message;
			})
		}
		;
		function editNotification(notification) {

			vm.notificationId = notification.notificationId;
			vm.notificationMessage = notification.notificationMessage;
			vm.createdBy = notification.createdBy;
			vm.createdDate = notification.createdDate;
			vm.notificationValidityDays = notification.notificationValidityDays;

		}
		function clearFields() {

			vm.notificationId = "";
			vm.notificationMessage = "";
			vm.createdBy = "";
			vm.createdDate = "";
			vm.notificationValidityDays = "";

		}

		function getAll() {
			var url = "/notifications/all";
			var notificationsPromise = $http.get(url);
			notificationsPromise.then(function(response) {
				vm.multipleNotifications = response.data;
			});
		}
		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}

		function deleteNotification(id) {
			var url = "/notifications/delete/" + id;
			if (confirm("Are You Sure?")) {
				$http.post(url).then(function(response) {
					vm.deleteMessage = response.data;
				});
				getPaginatedNotifications(vm.page.number, vm.page.size);
				
			}
		}
		function getPaginatedNotifications(pageNumber, size) {
			var url = "/notifications/get?page=" + pageNumber + "&size=" + size;
			var notificationsPromise = $http.get(url);
			notificationsPromise.then(function(response) {
				vm.notifications = response.data.content;

				vm.page.number = response.data.number;
				vm.page.size = response.data.size;
				vm.page.numberOfElements = response.data.numberOfElements;
				vm.page.first = response.data.first;
				vm.page.last = response.data.last;
				vm.page.totalPages = response.data.totalPages;
				vm.page.totalElements = response.data.totalElements;
			});
		}
		function previousPage() {
			if (!vm.page.first) {
				vm.page.number = vm.page.number - 1;
				getPaginatedNotifications(vm.page.number, vm.page.size);
			}
		}
		function nextPage() {
			if (!vm.page.last) {
				vm.page.number = vm.page.number + 1;
				getPaginatedNotifications(vm.page.number, vm.page.size);
			}

		}
	}
})();
