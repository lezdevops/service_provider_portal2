(function() {
	'use strict';

	angular.module('app').controller('ApplicationComponentController',
			ApplicationComponentController);

	ApplicationComponentController.$inject = [ '$http' ];

	function ApplicationComponentController($http, $resource, $scope) {
		var vm = this;

		vm.applicationComponents = [];
		vm.editApplicationComponent = editApplicationComponent;
		vm.saveApplicationComponent = saveApplicationComponent;
		vm.saveMultipleApplicationComponentData = saveMultipleApplicationComponentData;
		vm.getAll = getAll;
		vm.addRow = addRow;
		vm.deleteApplicationComponent = deleteApplicationComponent;
		vm.componentGenerationMessages = [];
		vm.generateComponents = generateComponents;
		
		init();

		function init() {
			getAll();
		}
		function saveApplicationComponent() {
			var applicationComponent = {};

			applicationComponent.componentId = vm.componentId;
			applicationComponent.componentName = vm.componentName;
			applicationComponent.details = vm.details;
			applicationComponent.basePermission = vm.basePermission;
			applicationComponent.iconStyle = vm.iconStyle;
			applicationComponent.linkStyle = vm.linkStyle;

			var url = "/applicationComponents/save";
			$http.post(url, applicationComponent).then(function(response) {
				vm.applicationComponents = response.data;
				clearFields();
			});
		}
		function addRow() {
			vm.applicationComponents.push({
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true

			});
		}
		;
		function saveMultipleApplicationComponentData() {
			$http(
					{
						method : "post",
						url : "/applicationComponents/saveMultipleApplicationComponentData",
						data : vm.applicationComponents,
					}).then(function(response) {
				vm.applicationComponents = response.data;
			});
		}
		;
		function editApplicationComponent(applicationComponent) {

			vm.componentId = applicationComponent.componentId;
			vm.componentName = applicationComponent.componentName;
			vm.details = applicationComponent.details;
			vm.basePermission = applicationComponent.basePermission;
			vm.iconStyle = applicationComponent.iconStyle;
			vm.linkStyle = applicationComponent.linkStyle;

		}
		function clearFields() {

			vm.componentId = "";
			vm.componentName = "";
			vm.details = "";
			vm.basePermission = "";
			vm.iconStyle = "";
			vm.linkStyle = "";

		}

		function getAll() {
			var url = "/applicationComponents/all";
			var applicationComponentsPromise = $http.get(url);
			applicationComponentsPromise.then(function(response) {
				vm.applicationComponents = response.data;
			});
		}

		function deleteApplicationComponent(id) {
			var url = "/applicationComponents/delete/" + id;
			$http.post(url).then(function(response) {
				vm.applicationComponents = response.data;
			});
		}
		function generateComponents() {
			var url = "/applicationComponents/generateComponents";
			$http.post(url).then(function(response) {
				vm.componentGenerationMessages = response.data;
				alert("Components Generated Successfully!");
			});
		}
	}
})();
