
(function () {
    'use strict';
    angular.module('app').directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;
                
                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);

    angular.module('app').service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function(file, uploadUrl){
            var fd = new FormData();
            fd.append('file', file);
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(response){
            	alert(response);
            });
        }
    }]);

    angular.module('app').controller('UserSmsNotifyDetailsController', UserSmsNotifyDetailsController);

    UserSmsNotifyDetailsController.$inject = ['$http'];

    function UserSmsNotifyDetailsController($http,$resource,$scope,fileUpload) {
    	var http = $http;
        var vm = this;
        vm.page = {};
        vm.addUserSmsNotifyDetailsMessages = [];
        vm.userSmsNotifyDetailss = [];
        vm.multipleUserSmsNotifyDetailss = [];
        vm.uploadedMultipleUserSmsNotifyDetailss = [];
        vm.editUserSmsNotifyDetails = editUserSmsNotifyDetails;
        vm.saveUserSmsNotifyDetails = saveUserSmsNotifyDetails;
        vm.saveMultipleUserSmsNotifyDetailsData = saveMultipleUserSmsNotifyDetailsData;
        vm.getPaginatedUserSmsNotifyDetailss = getPaginatedUserSmsNotifyDetailss;
        vm.getAll = getAll;
        vm.getUserLogins = getUserLogins;
        vm.excelFileUpload = excelFileUpload;
        vm.deleteAllTempUserSmsNotifyDetailsListData = deleteAllTempUserSmsNotifyDetailsListData;
        vm.saveAllTempUserSmsNotifyDetailsListData = saveAllTempUserSmsNotifyDetailsListData;
        vm.getInstituteProfiles = getInstituteProfiles;


        vm.addRow = addRow;
        vm.deleteUserSmsNotifyDetails = deleteUserSmsNotifyDetails;
        vm.previousPage = previousPage;
        vm.nextPage = nextPage;
        
        vm.page.number = 0;
    	vm.page.size =5;
    	
        init();

        function init(){
        	getPaginatedUserSmsNotifyDetailss(vm.page.number,vm.page.size);
        	getAll();
        	getUserLogins();
        	getInstituteProfiles();


        }
        
        function saveUserSmsNotifyDetails() {
			
			clearValidationMessages();
			
			var userSmsNotifyDetails = {};
        	
        	
userSmsNotifyDetails.notifyDetailsId = vm.notifyDetailsId;
userSmsNotifyDetails.smsNotifyId = vm.smsNotifyId;
userSmsNotifyDetails.receiverId = vm.receiverId;
userSmsNotifyDetails.isSeen = vm.isSeen;
userSmsNotifyDetails.seenDate = vm.seenDate;
userSmsNotifyDetails.isVisible = vm.isVisible;
userSmsNotifyDetails.instituteId = (vm.instituteProfile!==undefined) ? vm.instituteProfile.instituteId : null;


			var url = "/userSmsNotifyDetailss/save";
			$http.post(url, userSmsNotifyDetails).then(function(response) {
				var messageMap = response.data;
				
				if (messageMap.error!=="") {
					//vm.orderId_validationMessage = messageMap.orderId_validationMessage;
					
vm.notifyDetailsId_validationMessage = messageMap.notifyDetailsId_validationMessage;
vm.smsNotifyId_validationMessage = messageMap.smsNotifyId_validationMessage;
vm.receiverId_validationMessage = messageMap.receiverId_validationMessage;
vm.isSeen_validationMessage = messageMap.isSeen_validationMessage;
vm.seenDate_validationMessage = messageMap.seenDate_validationMessage;
vm.isVisible_validationMessage = messageMap.isVisible_validationMessage;
vm.instituteId_validationMessage = messageMap.instituteId_validationMessage;

					vm.technicalError = messageMap.technicalError;
				}else{
					vm.successMessage = messageMap.successMessage;
					getPaginatedUserSmsNotifyDetailss(vm.page.number, vm.page.size);
					getAll();
					clearFields();

					if (vm.isAllowNotification) {
						sendNotification();
					}
					if (vm.isAllowSendAsTask) {
						sendAsTaskAssignment();
					}					
				}
				
			});
		}
        function clearValidationMessages(){
			//vm.orderId_validationMessage = "";
        	
vm.notifyDetailsId_validationMessage = "";
vm.smsNotifyId_validationMessage = "";
vm.receiverId_validationMessage = "";
vm.isSeen_validationMessage = "";
vm.seenDate_validationMessage = "";
vm.isVisible_validationMessage = "";
vm.instituteId_validationMessage = "";

		}

        function sendNotification() {
			var notificationMap = {};

			notificationMap.notificationMessage = vm.notificationMessage;
			notificationMap.notificationReceivers = vm.notificationReceivers;
			notificationMap.notifyByEmail = vm.notifyByEmail;
			notificationMap.notifyBySMS = vm.notifyBySMS;

			var url = "/userSmsNotifyDetailss/sendNotification";
			$http.post(url, notificationMap).then(function(response) {
				//vm.notificationSentResult = response.data;
				alert('Notification Sent Successfully');
				clearNotificationFields();
			});
		}

		function sendAsTaskAssignment() {
			var taskManagement = {};
			taskManagement.assigneeId = vm.assigneeId;
			taskManagement.reporterId = vm.reporterId;
			taskManagement.taskDetails = vm.taskDetails;
			//taskManagement.createdDate = vm.createdDate;
			//taskManagement.planReportDate = vm.planReportDate;
			//taskManagement.actualReportDate = vm.actualReportDate;
			taskManagement.completeParcentage = 40;
			taskManagement.taskStatus = 'Assigned';

			var url = "/taskManagements/save";
			$http.post(url, taskManagement).then(function(response) {
				//vm.addTaskManagementMessages = response.data;
				alert("Task Assigned Successfully");
			});
		}
        function clearNotificationFields(){
        	
        	vm.notificationMessage = "";
        	vm.districtShortName = "";
        	vm.distFullName = "";
        	vm.createdDate = "";

        }
        function addRow() {
	        vm.multipleUserSmsNotifyDetailss.push(
	            { 
	             column1: true,column1: true,column1: true,column1: true,column1: true,column1: true,column1: true,column1: true

	             }
	        );
	    };
        function saveMultipleUserSmsNotifyDetailsData() {
	        $http({
	            method: "post",
	            url: "/userSmsNotifyDetailss/saveMultipleUserSmsNotifyDetailsData",
	            data: vm.multipleUserSmsNotifyDetailss,
	        }).success(function (data) {
//	        	/vm.addUserSmsNotifyDetailsMessages = response.data;
	        	vm.multipleUserSmsNotifyDetailss = response.data;
	        	getPaginatedUserSmsNotifyDetailss(vm.page.number,vm.page.size);
	        }).error(function (err) {
	            vm.Message = err.Message;
	        })
	    };
        function editUserSmsNotifyDetails(userSmsNotifyDetails){
        	
vm.instituteProfile = getInstituteProfileByInstituteId(userSmsNotifyDetails.instituteId);


        }
        function getInstituteProfileByInstituteId(instituteId){for(var i = 0; i < vm.instituteProfileList.length; i += 1){var instituteProfile = vm.instituteProfileList[i];if(instituteProfile.instituteId === instituteId){ return instituteProfile;
}
}
}


        function clearFields(){
        	
vm.notifyDetailsId = "";
vm.smsNotifyId = "";
vm.receiverId = "";
vm.isSeen = "";
vm.seenDate = "";
vm.isVisible = "";
vm.instituteId = "";

        }
       
        function getAll(){
            var url = "/userSmsNotifyDetailss/all";
            var userSmsNotifyDetailssPromise = $http.get(url);
            userSmsNotifyDetailssPromise.then(function(response){
                vm.multipleUserSmsNotifyDetailss = response.data;
            });
        }
        function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}
      function getInstituteProfiles(){var url = "/instituteProfiles/all";var instituteProfilesPromise = http.get(url);instituteProfilesPromise.then(function(response){vm.instituteProfileList = response.data;});}

        function deleteUserSmsNotifyDetails(id){
            var url = "/userSmsNotifyDetailss/delete/" + id;
            if (confirm("Are you sure?")) {
            	$http.post(url).then(function(response){
                	vm.deleteMessage = response.data;
                });
            	getPaginatedUserSmsNotifyDetailss(vm.page.number,vm.page.size);
            	getAll();
			}
        }
        function getPaginatedUserSmsNotifyDetailss(pageNumber,size){
            var url = "/userSmsNotifyDetailss/get?page=" + pageNumber + "&size=" + size ;
            var userSmsNotifyDetailssPromise = $http.get(url);
            userSmsNotifyDetailssPromise.then(function(response){
                vm.userSmsNotifyDetailss = response.data.content;
                if (vm.userSmsNotifyDetailss!=="") {
                    vm.page.number = response.data.number;
                    vm.page.size = response.data.size;
                    vm.page.numberOfElements = response.data.numberOfElements;
                    vm.page.first = response.data.first;
                    vm.page.last = response.data.last;
                    vm.page.totalPages = response.data.totalPages;
                    vm.page.totalElements = response.data.totalElements;					
				}
            });
        }
        function previousPage(){
        	if (!vm.page.first) {
            	vm.page.number = vm.page.number - 1;
            	getPaginatedUserSmsNotifyDetailss(vm.page.number,vm.page.size);				
			}
        }
        function nextPage(){
        	if (!vm.page.last) {
            	vm.page.number = vm.page.number + 1;
            	getPaginatedUserSmsNotifyDetailss(vm.page.number,vm.page.size);
			}
        	
        }
        function excelFileUpload(){
            var file = vm.myFile;
            console.log('file is ');
            console.dir(file);
            var uploadUrl = "/userSmsNotifyDetailss/excelFileUpload";
            
            var fd = new FormData();
            fd.append('file', file);
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(response){
            	vm.uploadedMultipleUserSmsNotifyDetailss = response.data;
            });
        }
        function deleteAllTempUserSmsNotifyDetailsListData(){
            vm.uploadedMultipleUserSmsNotifyDetailss = [];
        }
        function saveAllTempUserSmsNotifyDetailsListData(){
        	 $http({
 	            method: "post",
 	            url: "/userSmsNotifyDetailss/saveMultipleUserSmsNotifyDetailsData",
 	            data: vm.uploadedMultipleUserSmsNotifyDetailss,
 	        }).success(function (response) {
 	        	vm.userSmsNotifyDetailss = response.data;
 	        	vm.uploadedMultipleUserSmsNotifyDetailss = [];
 	        }).error(function (err) {
 	            vm.Message = err.Message;
 	        });
        }
    }
})();
