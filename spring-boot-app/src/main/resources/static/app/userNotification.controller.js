(function() {
	'use strict';

	angular.module('app').controller('UserNotificationController',
			UserNotificationController);

	UserNotificationController.$inject = [ '$http' ];

	function UserNotificationController($http, $resource, $scope) {
		var http = $http;
		var vm = this;
		vm.page = {};
		vm.addUserNotificationMessages = [];
		vm.userNotifications = [];
		vm.userNotificationListByUserName = [];
		vm.multipleUserNotifications = [];
		vm.editUserNotification = editUserNotification;
		vm.saveUserNotification = saveUserNotification;
		vm.saveMultipleUserNotificationData = saveMultipleUserNotificationData;
		vm.getPaginatedUserNotifications = getPaginatedUserNotifications;
		vm.getAll = getAll;
		vm.getNotifications = getNotifications;
		vm.getUserLogins = getUserLogins;
		vm.getUserNotificationByUsername = getUserNotificationByUsername;

		vm.addRow = addRow;
		vm.deleteUserNotification = deleteUserNotification;
		vm.previousPage = previousPage;
		vm.nextPage = nextPage;

		vm.page.number = 0;
		vm.page.size = 2;

		init();

		function init() {
			getPaginatedUserNotifications(vm.page.number, vm.page.size);
			getAll();
			getNotifications();
			getUserLogins();
			getUserNotificationByUsername(1);

		}
		function saveUserNotification() {
			var userNotification = {};

			userNotification.userNotiId = vm.userNotiId;
			userNotification.notificationId = vm.notificationId;
			userNotification.username = vm.username;
			userNotification.isRead = vm.isRead;
			userNotification.isVisible = vm.isVisible;

			var url = "/userNotifications/save";
			$http.post(url, userNotification).then(function(response) {
				vm.addUserNotificationMessages = response.data;
				getPaginatedUserNotifications(vm.page.number, vm.page.size);
				clearFields();
			});
		}
		function addRow() {
			vm.multipleUserNotifications.push({
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true

			});
		}
		;
		function saveMultipleUserNotificationData() {
			$http({
				method : "post",
				url : "/userNotifications/saveMultipleUserNotificationData",
				data : vm.multipleUserNotifications,
			}).success(function(data) {
				// /vm.addUserNotificationMessages = response.data;
				vm.multipleUserNotifications = response.data;
			}).error(function(err) {
				vm.Message = err.Message;
			})
		}
		;
		function editUserNotification(userNotification) {

			vm.userNotiId = userNotification.userNotiId;
			vm.notificationId = userNotification.notificationId;
			vm.username = userNotification.username;
			vm.isRead = userNotification.isRead;
			vm.isVisible = userNotification.isVisible;

		}
		function clearFields() {

			vm.userNotiId = "";
			vm.notificationId = "";
			vm.username = "";
			vm.isRead = "";
			vm.isVisible = "";

		}

		function getAll() {
			var url = "/userNotifications/all";
			var userNotificationsPromise = $http.get(url);
			userNotificationsPromise.then(function(response) {
				vm.multipleUserNotifications = response.data;
			});
		}
		function getUserNotificationByUsername(username) {
			var url = "/userNotifications/getUserNotificationByUsername?username="+username;
			var userNotificationsPromise = $http.get(url);
			userNotificationsPromise.then(function(response) {
				vm.userNotificationListByUserName = response.data;
			});
		}
		function getNotifications() {
			var url = "/notifications/all";
			var notificationsPromise = http.get(url);
			notificationsPromise.then(function(response) {
				vm.notificationList = response.data;
			});
		}
		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}

		function deleteUserNotification(id) {
			var url = "/userNotifications/delete/" + id;
			$http.post(url).then(function(response) {
				vm.deleteMessage = response.data;
				getPaginatedUserNotifications(vm.page.number, vm.page.size);
			});
		}
		function getPaginatedUserNotifications(pageNumber, size) {
			var url = "/userNotifications/get?page=" + pageNumber + "&size="
					+ size;
			var userNotificationsPromise = $http.get(url);
			userNotificationsPromise.then(function(response) {
				vm.userNotifications = response.data.content;

				vm.page.number = response.data.number;
				vm.page.size = response.data.size;
				vm.page.numberOfElements = response.data.numberOfElements;
				vm.page.first = response.data.first;
				vm.page.last = response.data.last;
				vm.page.totalPages = response.data.totalPages;
				vm.page.totalElements = response.data.totalElements;
			});
		}
		function previousPage() {
			if (!vm.page.first) {
				vm.page.number = vm.page.number - 1;
				getPaginatedUserNotifications(vm.page.number, vm.page.size);
			}
		}
		function nextPage() {
			if (!vm.page.last) {
				vm.page.number = vm.page.number + 1;
				getPaginatedUserNotifications(vm.page.number, vm.page.size);
			}

		}
	}
})();
