(function() {
	'use strict';
	angular.module('app').directive('fileModel', [ '$parse', function($parse) {
		return {
			restrict : 'A',
			link : function(scope, element, attrs) {
				var model = $parse(attrs.fileModel);
				var modelSetter = model.assign;

				element.bind('change', function() {
					scope.$apply(function() {
						modelSetter(scope, element[0].files[0]);
					});
				});
			}
		};
	} ]);

	angular.module('app').service('fileUpload', [ '$http', function($http) {
		this.uploadFileToUrl = function(file, uploadUrl) {
			var fd = new FormData();
			fd.append('file', file);
			$http.post(uploadUrl, fd, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).then(function(response) {
				alert(response);
			});
		}
	} ]);

	angular.module('app').controller('BootscaffDatabaseMapController',
			BootscaffDatabaseMapController);

	BootscaffDatabaseMapController.$inject = [ '$http' ];

	function BootscaffDatabaseMapController($http, $resource, $scope,
			fileUpload) {
		var http = $http;
		var vm = this;
		vm.page = {};
		vm.addBootscaffDatabaseMapMessages = [];
		vm.bootscaffDatabaseMaps = [];
		vm.multipleBootscaffDatabaseMaps = [];
		vm.uploadedMultipleBootscaffDatabaseMaps = [];
		vm.editBootscaffDatabaseMap = editBootscaffDatabaseMap;
		vm.saveBootscaffDatabaseMap = saveBootscaffDatabaseMap;
		vm.saveMultipleBootscaffDatabaseMapData = saveMultipleBootscaffDatabaseMapData;
		vm.getPaginatedBootscaffDatabaseMaps = getPaginatedBootscaffDatabaseMaps;
		vm.getAll = getAll;
		vm.getUserLogins = getUserLogins;
		vm.excelFileUpload = excelFileUpload;
		vm.deleteAllTempBootscaffDatabaseMapListData = deleteAllTempBootscaffDatabaseMapListData;
		vm.saveAllTempBootscaffDatabaseMapListData = saveAllTempBootscaffDatabaseMapListData;

		vm.addRow = addRow;
		vm.deleteBootscaffDatabaseMap = deleteBootscaffDatabaseMap;
		vm.previousPage = previousPage;
		vm.nextPage = nextPage;

		vm.page.number = 0;
		vm.page.size = 5;

		init();

		function init() {
			getPaginatedBootscaffDatabaseMaps(vm.page.number, vm.page.size);
			getAll();
			getUserLogins();

		}

		function saveBootscaffDatabaseMap() {

			clearValidationMessages();

			var bootscaffDatabaseMap = {};

			bootscaffDatabaseMap.sequenceId = vm.sequenceId;
			bootscaffDatabaseMap.fieldOrEntity = vm.fieldOrEntity;
			bootscaffDatabaseMap.type = vm.type;
			bootscaffDatabaseMap.primaryKeyField = vm.primaryKeyField;
			bootscaffDatabaseMap.relationType = vm.relationType;
			bootscaffDatabaseMap.relatedEntityName = vm.relatedEntityName;
			bootscaffDatabaseMap.displayField = vm.displayField;
			bootscaffDatabaseMap.isRequired = vm.isRequired;
			bootscaffDatabaseMap.duplicateCheck = vm.duplicateCheck;
			bootscaffDatabaseMap.multipleAddForm = vm.multipleAddForm;
			bootscaffDatabaseMap.singleAddForm = vm.singleAddForm;
			bootscaffDatabaseMap.enableNotification = vm.enableNotification;
			bootscaffDatabaseMap.enableTaskManagement = vm.enableTaskManagement;
			bootscaffDatabaseMap.addDataMigrationUI = vm.addDataMigrationUI;
			bootscaffDatabaseMap.allowExcelGeneration = vm.allowExcelGeneration;
			bootscaffDatabaseMap.showEntityList = vm.showEntityList;
			bootscaffDatabaseMap.showTabularDate = vm.showTabularDate;

			var url = "/bootscaffDatabaseMaps/save";
			$http
					.post(url, bootscaffDatabaseMap)
					.then(
							function(response) {
								var messageMap = response.data;

								if (messageMap.error !== "") {
									// vm.orderId_validationMessage =
									// messageMap.orderId_validationMessage;

									vm.sequenceId_validationMessage = messageMap.sequenceId_validationMessage;
									vm.fieldOrEntity_validationMessage = messageMap.fieldOrEntity_validationMessage;
									vm.type_validationMessage = messageMap.type_validationMessage;
									vm.primaryKeyField_validationMessage = messageMap.primaryKeyField_validationMessage;
									vm.relationType_validationMessage = messageMap.relationType_validationMessage;
									vm.relatedEntityName_validationMessage = messageMap.relatedEntityName_validationMessage;
									vm.displayField_validationMessage = messageMap.displayField_validationMessage;
									vm.isRequired_validationMessage = messageMap.isRequired_validationMessage;
									vm.duplicateCheck_validationMessage = messageMap.duplicateCheck_validationMessage;
									vm.multipleAddForm_validationMessage = messageMap.multipleAddForm_validationMessage;
									vm.singleAddForm_validationMessage = messageMap.singleAddForm_validationMessage;
									vm.enableNotification_validationMessage = messageMap.enableNotification_validationMessage;
									vm.enableTaskManagement_validationMessage = messageMap.enableTaskManagement_validationMessage;
									vm.addDataMigrationUI_validationMessage = messageMap.addDataMigrationUI_validationMessage;
									vm.allowExcelGeneration_validationMessage = messageMap.allowExcelGeneration_validationMessage;
									vm.showEntityList_validationMessage = messageMap.showEntityList_validationMessage;
									vm.showTabularDate_validationMessage = messageMap.showTabularDate_validationMessage;

									vm.technicalError = messageMap.technicalError;
								} else {
									vm.successMessage = messageMap.successMessage;
									getPaginatedBootscaffDatabaseMaps(
											vm.page.number, vm.page.size);
									getAll();
									clearFields();

									if (vm.isAllowNotification) {
										sendNotification();
									}
									if (vm.isAllowSendAsTask) {
										sendAsTaskAssignment();
									}
								}

							});
		}
		function clearValidationMessages() {
			// vm.orderId_validationMessage = "";

			vm.sequenceId_validationMessage = "";
			vm.fieldOrEntity_validationMessage = "";
			vm.type_validationMessage = "";
			vm.primaryKeyField_validationMessage = "";
			vm.relationType_validationMessage = "";
			vm.relatedEntityName_validationMessage = "";
			vm.displayField_validationMessage = "";
			vm.isRequired_validationMessage = "";
			vm.duplicateCheck_validationMessage = "";
			vm.multipleAddForm_validationMessage = "";
			vm.singleAddForm_validationMessage = "";
			vm.enableNotification_validationMessage = "";
			vm.enableTaskManagement_validationMessage = "";
			vm.addDataMigrationUI_validationMessage = "";
			vm.allowExcelGeneration_validationMessage = "";
			vm.showEntityList_validationMessage = "";
			vm.showTabularDate_validationMessage = "";

		}

		function sendNotification() {
			var notificationMap = {};

			notificationMap.notificationMessage = vm.notificationMessage;
			notificationMap.notificationReceivers = vm.notificationReceivers;
			notificationMap.notifyByEmail = vm.notifyByEmail;
			notificationMap.notifyBySMS = vm.notifyBySMS;

			var url = "/bootscaffDatabaseMaps/sendNotification";
			$http.post(url, notificationMap).then(function(response) {
				// vm.notificationSentResult = response.data;
				alert('Notification Sent Successfully');
				clearNotificationFields();
			});
		}

		function sendAsTaskAssignment() {
			var taskManagement = {};
			taskManagement.assigneeId = vm.assigneeId;
			taskManagement.reporterId = vm.reporterId;
			taskManagement.taskDetails = vm.taskDetails;
			// taskManagement.createdDate = vm.createdDate;
			// taskManagement.planReportDate = vm.planReportDate;
			// taskManagement.actualReportDate = vm.actualReportDate;
			taskManagement.completeParcentage = 40;
			taskManagement.taskStatus = 'Assigned';

			var url = "/taskManagements/save";
			$http.post(url, taskManagement).then(function(response) {
				// vm.addTaskManagementMessages = response.data;
				alert("Task Assigned Successfully");
			});
		}
		function clearNotificationFields() {

			vm.notificationMessage = "";
			vm.districtShortName = "";
			vm.distFullName = "";
			vm.createdDate = "";

		}
		function addRow() {
			vm.multipleBootscaffDatabaseMaps.push({
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true

			});
		}
		;
		function saveMultipleBootscaffDatabaseMapData() {
			$http(
					{
						method : "post",
						url : "/bootscaffDatabaseMaps/saveMultipleBootscaffDatabaseMapData",
						data : vm.multipleBootscaffDatabaseMaps,
					}).then(
					function(response) {
						// /vm.addBootscaffDatabaseMapMessages = response.data;
						vm.multipleBootscaffDatabaseMaps = response.data;
						getPaginatedBootscaffDatabaseMaps(vm.page.number,
								vm.page.size);
					}).error(function(err) {
				vm.Message = err.Message;
			})
		}
		;
		function editBootscaffDatabaseMap(bootscaffDatabaseMap) {

			vm.sequenceId = bootscaffDatabaseMap.sequenceId;
			vm.fieldOrEntity = bootscaffDatabaseMap.fieldOrEntity;
			vm.type = bootscaffDatabaseMap.type;
			vm.primaryKeyField = bootscaffDatabaseMap.primaryKeyField;
			vm.relationType = bootscaffDatabaseMap.relationType;
			vm.relatedEntityName = bootscaffDatabaseMap.relatedEntityName;
			vm.displayField = bootscaffDatabaseMap.displayField;
			vm.isRequired = bootscaffDatabaseMap.isRequired;
			vm.duplicateCheck = bootscaffDatabaseMap.duplicateCheck;
			vm.multipleAddForm = bootscaffDatabaseMap.multipleAddForm;
			vm.singleAddForm = bootscaffDatabaseMap.singleAddForm;
			vm.enableNotification = bootscaffDatabaseMap.enableNotification;
			vm.enableTaskManagement = bootscaffDatabaseMap.enableTaskManagement;
			vm.addDataMigrationUI = bootscaffDatabaseMap.addDataMigrationUI;
			vm.allowExcelGeneration = bootscaffDatabaseMap.allowExcelGeneration;
			vm.showEntityList = bootscaffDatabaseMap.showEntityList;
			vm.showTabularDate = bootscaffDatabaseMap.showTabularDate;

		}
		// newGetRelatedDropdownsEdit
		function clearFields() {

			vm.sequenceId = "";
			vm.fieldOrEntity = "";
			vm.type = "";
			vm.primaryKeyField = "";
			vm.relationType = "";
			vm.relatedEntityName = "";
			vm.displayField = "";
			vm.isRequired = "";
			vm.duplicateCheck = "";
			vm.multipleAddForm = "";
			vm.singleAddForm = "";
			vm.enableNotification = "";
			vm.enableTaskManagement = "";
			vm.addDataMigrationUI = "";
			vm.allowExcelGeneration = "";
			vm.showEntityList = "";
			vm.showTabularDate = "";

		}

		function getAll() {
			var url = "/bootscaffDatabaseMaps/all";
			var bootscaffDatabaseMapsPromise = $http.get(url);
			bootscaffDatabaseMapsPromise.then(function(response) {
				vm.multipleBootscaffDatabaseMaps = response.data;
			});
		}
		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}

		function deleteBootscaffDatabaseMap(id) {
			var url = "/bootscaffDatabaseMaps/delete/" + id;
			if (confirm("Are you sure?")) {
				$http.post(url).then(function(response) {
					vm.deleteMessage = response.data;
				});
				getPaginatedBootscaffDatabaseMaps(vm.page.number, vm.page.size);
				getAll();
			}
		}
		function getPaginatedBootscaffDatabaseMaps(pageNumber, size) {
			var url = "/bootscaffDatabaseMaps/get?page=" + pageNumber
					+ "&size=" + size;
			var bootscaffDatabaseMapsPromise = $http.get(url);
			bootscaffDatabaseMapsPromise.then(function(response) {
				vm.bootscaffDatabaseMaps = response.data.content;

				vm.page.number = response.data.number;
				vm.page.size = response.data.size;
				vm.page.numberOfElements = response.data.numberOfElements;
				vm.page.first = response.data.first;
				vm.page.last = response.data.last;
				vm.page.totalPages = response.data.totalPages;
				vm.page.totalElements = response.data.totalElements;
			});
		}
		function previousPage() {
			if (!vm.page.first) {
				vm.page.number = vm.page.number - 1;
				getPaginatedBootscaffDatabaseMaps(vm.page.number, vm.page.size);
			}
		}
		function nextPage() {
			if (!vm.page.last) {
				vm.page.number = vm.page.number + 1;
				getPaginatedBootscaffDatabaseMaps(vm.page.number, vm.page.size);
			}

		}
		function excelFileUpload() {
			var file = vm.myFile;
			console.log('file is ');
			console.dir(file);
			var uploadUrl = "/bootscaffDatabaseMaps/excelFileUpload";

			var fd = new FormData();
			fd.append('file', file);
			$http.post(uploadUrl, fd, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).then(function(response) {
				vm.uploadedMultipleBootscaffDatabaseMaps = response.data;
			});
		}
		function deleteAllTempBootscaffDatabaseMapListData() {
			vm.uploadedMultipleBootscaffDatabaseMaps = [];
		}
		function saveAllTempBootscaffDatabaseMapListData() {
			$http(
					{
						method : "post",
						url : "/bootscaffDatabaseMaps/saveMultipleBootscaffDatabaseMapData",
						data : vm.uploadedMultipleBootscaffDatabaseMaps,
					}).then(function(response) {
				vm.bootscaffDatabaseMaps = response.data;
				vm.uploadedMultipleBootscaffDatabaseMaps = [];
			}).error(function(err) {
				vm.Message = err.Message;
			});
		}
	}
})();
