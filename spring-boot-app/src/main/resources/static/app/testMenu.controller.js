(function() {
	'use strict';
	angular.module('app').directive('datepicker', function() {
		return {
			require : 'ngModel',
			link : function(scope, element, attr, ngModel) {
				$(element).datepicker({
					dateFormat : "yy-mm-dd",
					onSelect : function(dateText) {
						scope.$apply(function() {
							ngModel.$setViewValue(dateText);
						});
					}
				});
			}
		};
	});
	function timeConverter(UNIX_timestamp) {
		var a = new Date(UNIX_timestamp);
		var months = [ '01', '02', '03', '04', '05', '06', '07', '08', '09',
				'10', '11', '12' ];
		var year = a.getFullYear();
		var month = months[a.getMonth()];
		var date = a.getDate();
		var hour = a.getHours();
		var min = a.getMinutes();
		var sec = a.getSeconds();
		date = parseInt(date) < 10 ? '0' + date : date;
		var time = year + '-' + month + '-' + date;
		return time;
	}

	angular.module('app').directive('fileModel', [ '$parse', function($parse) {
		return {
			restrict : 'A',
			link : function(scope, element, attrs) {
				var model = $parse(attrs.fileModel);
				var modelSetter = model.assign;

				element.bind('change', function() {
					scope.$apply(function() {
						modelSetter(scope, element[0].files[0]);
					});
				});
			}
		};
	} ]);

	angular.module('app').service('fileUpload', [ '$http', function($http) {
		this.uploadFileToUrl = function(file, uploadUrl) {
			var fd = new FormData();
			fd.append('file', file);
			$http.post(uploadUrl, fd, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).then(function(response) {
				alert(response);
			});
		}
	} ]);

	angular.module('app').controller('TestMenuController', TestMenuController);

	TestMenuController.$inject = [ '$http', '$scope', '$routeParams' ];

	function TestMenuController($http, $resource, $routeParams, $scope,
			fileUpload) {
		var http = $http;
		var vm = this;
		vm.page = {};
		vm.addTestMenuMessages = [];
		vm.testMenus = [];
		vm.multipleTestMenus = [];
		vm.uploadedMultipleTestMenus = [];
		vm.editTestMenu = editTestMenu;
		vm.saveTestMenu = saveTestMenu;
		vm.saveMultipleTestMenuData = saveMultipleTestMenuData;
		vm.getPaginatedTestMenus = getPaginatedTestMenus;
		vm.getAll = getAll;
		vm.clearFields = clearFields;
		vm.getUserLogins = getUserLogins;
		vm.excelFileUpload = excelFileUpload;
		vm.deleteAllTempTestMenuListData = deleteAllTempTestMenuListData;
		vm.saveAllTempTestMenuListData = saveAllTempTestMenuListData;

		vm.addRow = addRow;
		vm.deleteTestMenu = deleteTestMenu;
		vm.previousPage = previousPage;
		vm.nextPage = nextPage;
		vm.searchOnTestMenuTable = searchOnTestMenuTable;
		vm.loadOnTestMenuDetails = loadOnTestMenuDetails;

		vm.page.number = 0;
		vm.page.size = 5;

		if ($routeParams != undefined && $routeParams.id != null) {
			// console.log("Parameters: " + $routeParams);
			loadDetails($routeParams.id);
		}

		init();

		function init() {
			getPaginatedTestMenus(vm.page.number, vm.page.size);
			getAll();
			getUserLogins();

		}

		function saveTestMenu() {

			clearValidationMessages();

			var testMenu = {};

			testMenu.id = vm.id;
			testMenu.userLoginId = vm.userLoginId;
			testMenu.supplier = vm.supplier;

			var url = "/testMenus/save";
			$http
					.post(url, testMenu)
					.then(
							function(response) {
								var messageMap = response.data;

								if (messageMap.error !== "") {
									// vm.orderId_validationMessage =
									// messageMap.orderId_validationMessage;

									vm.id_validationMessage = messageMap.id_validationMessage;
									vm.userLoginId_validationMessage = messageMap.userLoginId_validationMessage;
									vm.supplier_validationMessage = messageMap.supplier_validationMessage;

									vm.technicalError = messageMap.technicalError;
								} else {
									vm.successMessage = messageMap.successMessage;
									getPaginatedTestMenus(vm.page.number,
											vm.page.size);
									getAll();
									clearFields();

									if (vm.isAllowNotification) {
										sendNotification();
									}
									if (vm.isAllowSendAsTask) {
										sendAsTaskAssignment();
									}
									window.location = '#!/testMenuList';
								}

							});
		}
		function clearValidationMessages() {
			// vm.orderId_validationMessage = "";

			vm.id_validationMessage = "";
			vm.userLoginId_validationMessage = "";
			vm.supplier_validationMessage = "";

		}

		function sendNotification() {
			var notificationMap = {};

			notificationMap.notificationMessage = vm.notificationMessage;
			notificationMap.notificationReceivers = vm.notificationReceivers;
			notificationMap.notifyByEmail = vm.notifyByEmail;
			notificationMap.notifyBySMS = vm.notifyBySMS;

			var url = "/testMenus/sendNotification";
			$http.post(url, notificationMap).then(function(response) {
				// vm.notificationSentResult = response.data;
				alert('Notification Sent Successfully');
				clearNotificationFields();
			});
		}

		function sendAsTaskAssignment() {
			var taskManagement = {};
			taskManagement.assigneeId = vm.assigneeId;
			taskManagement.reporterId = vm.reporterId;
			taskManagement.taskDetails = vm.taskDetails;
			// taskManagement.createdDate = vm.createdDate;
			// taskManagement.planReportDate = vm.planReportDate;
			// taskManagement.actualReportDate = vm.actualReportDate;
			taskManagement.completeParcentage = 40;
			taskManagement.taskStatus = 'Assigned';

			var url = "/taskManagements/save";
			$http.post(url, taskManagement).then(function(response) {
				// vm.addTaskManagementMessages = response.data;
				alert("Task Assigned Successfully");
			});
		}
		function clearNotificationFields() {

			vm.notificationMessage = "";
			vm.districtShortName = "";
			vm.distFullName = "";
			vm.createdDate = "";

		}
		function addRow() {
			vm.multipleTestMenus.push({
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true

			});
		}
		;
		function saveMultipleTestMenuData() {
			$http({
				method : "post",
				url : "/testMenus/saveMultipleTestMenuData",
				data : vm.multipleTestMenus,
			}).then(function(response) {
				// /vm.addTestMenuMessages = response.data;
				vm.multipleTestMenus = response.data;
				getPaginatedTestMenus(vm.page.number, vm.page.size);
			});
		}
		;
		function editTestMenu(testMenu) {

			vm.id = testMenu.id;
			vm.userLoginId = testMenu.userLoginId;
			vm.supplier = testMenu.supplier;

		}
		// newGetRelatedDropdownsEdit
		function clearFields() {

			vm.id = "";
			vm.userLoginId = "";
			vm.supplier = "";

		}

		function getAll() {
			var url = "/testMenus/all";
			var testMenusPromise = $http.get(url);
			testMenusPromise.then(function(response) {
				vm.multipleTestMenus = response.data;
			});
		}
		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}

		function deleteTestMenu(id) {
			var url = "/testMenus/delete/" + id;
			if (confirm("Are you sure?")) {
				$http.post(url).then(function(response) {
					vm.deleteMessage = response.data;
				});
				getPaginatedTestMenus(vm.page.number, vm.page.size);
				getAll();
			}
		}
		function getPaginatedTestMenus(pageNumber, size) {
			var url = "/testMenus/get?page=" + pageNumber + "&size=" + size;
			var testMenusPromise = $http.get(url);
			testMenusPromise.then(function(response) {
				vm.testMenus = response.data.content;
				if (vm.testMenus !== "") {
					vm.page.number = response.data.number;
					vm.page.size = response.data.size;
					vm.page.numberOfElements = response.data.numberOfElements;
					vm.page.first = response.data.first;
					vm.page.last = response.data.last;
					vm.page.totalPages = response.data.totalPages;
					vm.page.totalElements = response.data.totalElements;
					vm.totalPages = new Array(vm.page.totalPages);
				}
			});
		}
		function previousPage() {
			if (!vm.page.first) {
				vm.page.number = vm.page.number - 1;
				getPaginatedTestMenus(vm.page.number, vm.page.size);
			}
		}
		function nextPage() {
			if (!vm.page.last) {
				vm.page.number = vm.page.number + 1;
				getPaginatedTestMenus(vm.page.number, vm.page.size);
			}

		}
		function excelFileUpload() {
			var file = vm.myFile;
			console.log('file is ');
			console.dir(file);
			var uploadUrl = "/testMenus/excelFileUpload";

			var fd = new FormData();
			fd.append('file', file);
			$http.post(uploadUrl, fd, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).then(function(response) {
				vm.uploadedMultipleTestMenus = response.data;
			});
		}
		function deleteAllTempTestMenuListData() {
			vm.uploadedMultipleTestMenus = [];
		}
		function saveAllTempTestMenuListData() {
			$http({
				method : "post",
				url : "/testMenus/saveMultipleTestMenuData",
				data : vm.uploadedMultipleTestMenus,
			}).then(function(response) {
				vm.testMenus = response.data;
				vm.uploadedMultipleTestMenus = [];
			});
		}
		// findById
		function searchOnTestMenuTable() {
			var url = "/testMenus/searchOnTestMenuTable?id=" + vm.idSearch;
			var testMenusPromise = $http.get(url);
			testMenusPromise.then(function(response) {
				vm.testMenus = response.data.content;
				if (vm.testMenus !== "") {
					vm.page.number = response.data.number;
					vm.page.size = response.data.size;
					vm.page.numberOfElements = response.data.numberOfElements;
					vm.page.first = response.data.first;
					vm.page.last = response.data.last;
					vm.page.totalPages = response.data.totalPages;
					vm.page.totalElements = response.data.totalElements;
					vm.totalPages = new Array(vm.page.totalPages);
				}
			});
		}
		function loadDetails(id) {

			var url = "/testMenus/searchOnTestMenuTable?id=" + id;
			var testMenusPromise = $http.get(url);
			testMenusPromise.then(function(response) {
				vm.testMenus = response.data.content;
				if (vm.testMenus !== "") {
					var testMenu = vm.testMenus[0];

					vm.id = testMenu.id;
					vm.userLoginId = testMenu.userLoginId;
					vm.supplier = testMenu.supplier;

				}
			});

		}
		function loadOnTestMenuDetails() {
			var id = vm.idToLoad;
			loadDetails(id);
		}

	}
})();
