(function() {
	'use strict';

	angular.module('app').controller('BootscaffDashboardController',
			BootscaffDashboardController);

	BootscaffDashboardController.$inject = [ '$http' ];

	function BootscaffDashboardController($http, $resource, $scope) {
		var http = $http;
		var vm = this;
		vm.addTaskManagementMessages = [];
		vm.getUserLogins = getUserLogins;
		
		init();

		function init() {
			getUserLogins();
		}
		
		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}
	}
})();
