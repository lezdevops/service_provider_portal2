(function() {
	'use strict';

	angular.module('app').controller('UserMessageController',
			UserMessageController);

	UserMessageController.$inject = [ '$http' ];

	function UserMessageController($http, $resource, $scope) {
		var http = $http;
		var vm = this;
		vm.page = {};
		vm.mailboxType = 'INBOX';
		vm.addUserMessageMessages = [];
		vm.userMessages = [];
		vm.userMessageListByMessageType = [];
		vm.getUserMessagesByMessageType = getUserMessagesByMessageType;

		vm.filteredUserMessageList = [];
		vm.receiverId = 1;
		vm.userMessageListByReceiverId = [];
		vm.getUserMessagesByReceiverId = getUserMessagesByReceiverId;

		vm.senderId = 1;
		vm.userMessageListBySenderId = [];
		vm.getUserMessagesBySenderId = getUserMessagesBySenderId;

		vm.multipleUserMessages = [];
		vm.editUserMessage = editUserMessage;
		vm.saveUserMessage = saveUserMessage;
		vm.sendUserMessage = sendUserMessage;
		vm.saveUserMessageAsDraft = saveUserMessageAsDraft;
		vm.saveMultipleUserMessageData = saveMultipleUserMessageData;
		vm.getPaginatedUserMessages = getPaginatedUserMessages;
		vm.getAll = getAll;
		vm.getUserLogins = getUserLogins;
		vm.getUserLogins = getUserLogins;

		vm.addRow = addRow;
		vm.deleteUserMessage = deleteUserMessage;
		vm.previousPage = previousPage;
		vm.nextPage = nextPage;

		vm.page.number = 0;
		vm.page.size = 10;

		init();

		function init() {
			getPaginatedUserMessages(vm.page.number, vm.page.size);
			getAll();
			getUserLogins();
			getUserMessagesByMessageType('SENT');
			getUserMessagesByReceiverId(vm.receiverId);
		}
		function saveUserMessage() {
			var userMessage = {};

			userMessage.userMessageId = vm.userMessageId;
			userMessage.senderId = vm.senderId;
			userMessage.receiverId = vm.receiverId;
			userMessage.message = vm.message;
			userMessage.subject = vm.subject;
			userMessage.createdDate = vm.createdDate;
			userMessage.seenDate = vm.seenDate;
			userMessage.messageStatus = vm.messageStatus;
			userMessage.senderMessageType = "SENT";

			var url = "/userMessages/save";
			$http.post(url, userMessage).then(function(response) {
				vm.addUserMessageMessages = response.data;
				getPaginatedUserMessages(vm.page.number, vm.page.size);
				getAll();
				clearFields();
			});
		}
		function sendUserMessage(senderMessageType) {
			var userMessage = {};

			// userMessage.userMessageId = vm.userMessageId;
			userMessage.senderId = vm.senderId;
			userMessage.receiverId = vm.receiverId;
			userMessage.message = vm.message;
			userMessage.subject = vm.subject;
			// userMessage.createdDate = vm.createdDate;
			// userMessage.seenDate = vm.seenDate;
			userMessage.messageStatus = senderMessageType;
			userMessage.senderMessageType = senderMessageType;

			var url = "/userMessages/save";
			$http.post(url, userMessage).then(function(response) {
				// vm.addUserMessageMessages = response.data;
				if (senderMessageType == 'SENT') {
					alert("Message Sent.");					
				}
				clearFields();
			});
		}
		function saveUserMessageAsDraft(senderMessageType) {
			var userMessage = {};

			// userMessage.userMessageId = vm.userMessageId;
			userMessage.senderId = '';
			userMessage.receiverId = '';
			userMessage.message = vm.message;
			userMessage.subject = vm.subject;
			// userMessage.createdDate = vm.createdDate;
			// userMessage.seenDate = vm.seenDate;
			userMessage.messageStatus = senderMessageType;
			userMessage.senderMessageType = senderMessageType;

			var url = "/userMessages/save";
			$http.post(url, userMessage).then(function(response) {
				// vm.addUserMessageMessages = response.data;
				if (senderMessageType == 'DRAFT') {
					alert("Message Saved to Draft.");					
				}else if(senderMessageType == 'JUNK'){
					alert("Message Moved to junk.");
				}
				clearFields();
			});
		}
		function addRow() {
			vm.multipleUserMessages.push({
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true,
				column1 : true

			});
		}
		;
		function saveMultipleUserMessageData() {
			$http({
				method : "post",
				url : "/userMessages/saveMultipleUserMessageData",
				data : vm.multipleUserMessages,
			}).success(function(data) {
				// /vm.addUserMessageMessages = response.data;
				vm.multipleUserMessages = response.data;
				getPaginatedUserMessages(vm.page.number, vm.page.size);
			}).error(function(err) {
				vm.Message = err.Message;
			})
		}
		;
		function editUserMessage(userMessage) {

			vm.userMessageId = userMessage.userMessageId;
			vm.senderId = userMessage.senderId;
			vm.receiverId = userMessage.receiverId;
			vm.message = userMessage.message;
			vm.subject = userMessage.subject;
			vm.createdDate = userMessage.createdDate;
			vm.seenDate = userMessage.seenDate;
			vm.messageStatus = userMessage.messageStatus;

		}
		function clearFields() {

			vm.userMessageId = "";
			vm.senderId = "";
			vm.receiverId = "";
			vm.message = "";
			vm.subject = "";
			vm.createdDate = "";
			vm.seenDate = "";
			vm.messageStatus = "";

		}
		// //////////////////////////// data fetch methods /////////////////////
		function getAll() {
			var url = "/userMessages/all";
			var userMessagesPromise = $http.get(url);
			userMessagesPromise.then(function(response) {
				vm.multipleUserMessages = response.data;
			});
		}
		function getUserMessagesByMessageType(messageType) {
			var url = "/userMessages/getUserMessagesByMessageType?messageType="
					+ messageType;
			var userMessagesPromise = http.get(url);
			userMessagesPromise.then(function(response) {
				vm.userMessageListByMessageType = response.data;
				vm.filteredUserMessageList = response.data;
				if (messageType == 'INBOX') {
					vm.mailboxType = 'INBOX';					
				}else if (messageType == 'DRAFT') {
					vm.mailboxType = 'DRAFT ITEMS';
				}else if (messageType == 'JUNK') {
					vm.mailboxType = 'JUNK ITEMS';
				}else if (messageType == 'TRASH') {
					vm.mailboxType = 'TRASH MESSAGES';
				}
			});
		}
		function getUserMessagesBySenderId(senderId) {
			var url = "/userMessages/getUserMessagesBySenderId?senderId="
					+ senderId;
			var userMessagesPromise = http.get(url);
			userMessagesPromise.then(function(response) {
				vm.userMessageListBySenderId = response.data;
				vm.filteredUserMessageList = response.data;
				vm.mailboxType = 'SENT ITEMS';
			});
		}
		function getUserMessagesByReceiverId(receiverId) {
			var url = "/userMessages/getUserMessagesByReceiverId?receiverId="
					+ receiverId;
			var userMessagesPromise = http.get(url);
			userMessagesPromise.then(function(response) {
				vm.userMessageListByReceiverId = response.data;
				vm.filteredUserMessageList = response.data;
				vm.mailboxType = 'INBOX';
			});
		}
		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}

		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}

		function deleteUserMessage(id) {
			var url = "/userMessages/delete/" + id;
			$http.post(url).then(function(response) {
				vm.deleteMessage = response.data;
				getPaginatedUserMessages(vm.page.number, vm.page.size);
				getAll();
			});
		}
		function getPaginatedUserMessages(pageNumber, size) {
			var url = "/userMessages/get?page=" + pageNumber + "&size=" + size;
			var userMessagesPromise = $http.get(url);
			userMessagesPromise.then(function(response) {
				vm.userMessages = response.data.content;

				vm.page.number = response.data.number;
				vm.page.size = response.data.size;
				vm.page.numberOfElements = response.data.numberOfElements;
				vm.page.first = response.data.first;
				vm.page.last = response.data.last;
				vm.page.totalPages = response.data.totalPages;
				vm.page.totalElements = response.data.totalElements;
			});
		}
		function previousPage() {
			if (!vm.page.first) {
				vm.page.number = vm.page.number - 1;
				getPaginatedUserMessages(vm.page.number, vm.page.size);
			}
		}
		function nextPage() {
			if (!vm.page.last) {
				vm.page.number = vm.page.number + 1;
				getPaginatedUserMessages(vm.page.number, vm.page.size);
			}

		}
	}
})();
