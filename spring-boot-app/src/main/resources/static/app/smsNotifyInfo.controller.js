
(function () {
    'use strict';
    angular.module('app').directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;
                
                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);

    angular.module('app').service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function(file, uploadUrl){
            var fd = new FormData();
            fd.append('file', file);
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(response){
            	alert(response);
            });
        }
    }]);

    angular.module('app').controller('SmsNotifyInfoController', SmsNotifyInfoController);

    SmsNotifyInfoController.$inject = ['$http'];

    function SmsNotifyInfoController($http,$resource,$scope,fileUpload) {
    	var http = $http;
        var vm = this;
        vm.page = {};
        vm.addSmsNotifyInfoMessages = [];
        vm.smsNotifyInfos = [];
        vm.multipleSmsNotifyInfos = [];
        vm.uploadedMultipleSmsNotifyInfos = [];
        vm.editSmsNotifyInfo = editSmsNotifyInfo;
        vm.saveSmsNotifyInfo = saveSmsNotifyInfo;
        vm.saveMultipleSmsNotifyInfoData = saveMultipleSmsNotifyInfoData;
        vm.getPaginatedSmsNotifyInfos = getPaginatedSmsNotifyInfos;
        vm.getAll = getAll;
        vm.getUserLogins = getUserLogins;
        vm.excelFileUpload = excelFileUpload;
        vm.deleteAllTempSmsNotifyInfoListData = deleteAllTempSmsNotifyInfoListData;
        vm.saveAllTempSmsNotifyInfoListData = saveAllTempSmsNotifyInfoListData;
        vm.getInstituteProfiles = getInstituteProfiles;


        vm.addRow = addRow;
        vm.deleteSmsNotifyInfo = deleteSmsNotifyInfo;
        vm.previousPage = previousPage;
        vm.nextPage = nextPage;
        
        vm.page.number = 0;
    	vm.page.size =5;
    	
        init();

        function init(){
        	getPaginatedSmsNotifyInfos(vm.page.number,vm.page.size);
        	getAll();
        	getUserLogins();
        	getInstituteProfiles();


        }
        
        function saveSmsNotifyInfo() {
			
			clearValidationMessages();
			
			var smsNotifyInfo = {};
        	
        	
smsNotifyInfo.smsNotifyId = vm.smsNotifyId;
smsNotifyInfo.senderId = vm.senderId;
smsNotifyInfo.message = vm.message;
smsNotifyInfo.sentDate = vm.sentDate;
smsNotifyInfo.isApproved = vm.isApproved;
smsNotifyInfo.approverId = vm.approverId;
smsNotifyInfo.instituteId = (vm.instituteProfile!==undefined) ? vm.instituteProfile.instituteId : null;


			var url = "/smsNotifyInfos/save";
			$http.post(url, smsNotifyInfo).then(function(response) {
				var messageMap = response.data;
				
				if (messageMap.error!=="") {
					//vm.orderId_validationMessage = messageMap.orderId_validationMessage;
					
vm.smsNotifyId_validationMessage = messageMap.smsNotifyId_validationMessage;
vm.senderId_validationMessage = messageMap.senderId_validationMessage;
vm.message_validationMessage = messageMap.message_validationMessage;
vm.sentDate_validationMessage = messageMap.sentDate_validationMessage;
vm.isApproved_validationMessage = messageMap.isApproved_validationMessage;
vm.approverId_validationMessage = messageMap.approverId_validationMessage;
vm.instituteId_validationMessage = messageMap.instituteId_validationMessage;

					vm.technicalError = messageMap.technicalError;
				}else{
					vm.successMessage = messageMap.successMessage;
					getPaginatedSmsNotifyInfos(vm.page.number, vm.page.size);
					getAll();
					clearFields();

					if (vm.isAllowNotification) {
						sendNotification();
					}
					if (vm.isAllowSendAsTask) {
						sendAsTaskAssignment();
					}					
				}
				
			});
		}
        function clearValidationMessages(){
			//vm.orderId_validationMessage = "";
        	
vm.smsNotifyId_validationMessage = "";
vm.senderId_validationMessage = "";
vm.message_validationMessage = "";
vm.sentDate_validationMessage = "";
vm.isApproved_validationMessage = "";
vm.approverId_validationMessage = "";
vm.instituteId_validationMessage = "";

		}

        function sendNotification() {
			var notificationMap = {};

			notificationMap.notificationMessage = vm.notificationMessage;
			notificationMap.notificationReceivers = vm.notificationReceivers;
			notificationMap.notifyByEmail = vm.notifyByEmail;
			notificationMap.notifyBySMS = vm.notifyBySMS;

			var url = "/smsNotifyInfos/sendNotification";
			$http.post(url, notificationMap).then(function(response) {
				//vm.notificationSentResult = response.data;
				alert('Notification Sent Successfully');
				clearNotificationFields();
			});
		}

		function sendAsTaskAssignment() {
			var taskManagement = {};
			taskManagement.assigneeId = vm.assigneeId;
			taskManagement.reporterId = vm.reporterId;
			taskManagement.taskDetails = vm.taskDetails;
			//taskManagement.createdDate = vm.createdDate;
			//taskManagement.planReportDate = vm.planReportDate;
			//taskManagement.actualReportDate = vm.actualReportDate;
			taskManagement.completeParcentage = 40;
			taskManagement.taskStatus = 'Assigned';

			var url = "/taskManagements/save";
			$http.post(url, taskManagement).then(function(response) {
				//vm.addTaskManagementMessages = response.data;
				alert("Task Assigned Successfully");
			});
		}
        function clearNotificationFields(){
        	
        	vm.notificationMessage = "";
        	vm.districtShortName = "";
        	vm.distFullName = "";
        	vm.createdDate = "";

        }
        function addRow() {
	        vm.multipleSmsNotifyInfos.push(
	            { 
	             column1: true,column1: true,column1: true,column1: true,column1: true,column1: true,column1: true,column1: true

	             }
	        );
	    };
        function saveMultipleSmsNotifyInfoData() {
	        $http({
	            method: "post",
	            url: "/smsNotifyInfos/saveMultipleSmsNotifyInfoData",
	            data: vm.multipleSmsNotifyInfos,
	        }).success(function (data) {
//	        	/vm.addSmsNotifyInfoMessages = response.data;
	        	vm.multipleSmsNotifyInfos = response.data;
	        	getPaginatedSmsNotifyInfos(vm.page.number,vm.page.size);
	        }).error(function (err) {
	            vm.Message = err.Message;
	        })
	    };
        function editSmsNotifyInfo(smsNotifyInfo){
        	
vm.instituteProfile = getInstituteProfileByInstituteId(smsNotifyInfo.instituteId);


        }
        function getInstituteProfileByInstituteId(instituteId){for(var i = 0; i < vm.instituteProfileList.length; i += 1){var instituteProfile = vm.instituteProfileList[i];if(instituteProfile.instituteId === instituteId){ return instituteProfile;
}
}
}


        function clearFields(){
        	
vm.smsNotifyId = "";
vm.senderId = "";
vm.message = "";
vm.sentDate = "";
vm.isApproved = "";
vm.approverId = "";
vm.instituteId = "";

        }
       
        function getAll(){
            var url = "/smsNotifyInfos/all";
            var smsNotifyInfosPromise = $http.get(url);
            smsNotifyInfosPromise.then(function(response){
                vm.multipleSmsNotifyInfos = response.data;
            });
        }
        function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}
      function getInstituteProfiles(){var url = "/instituteProfiles/all";var instituteProfilesPromise = http.get(url);instituteProfilesPromise.then(function(response){vm.instituteProfileList = response.data;});}

        function deleteSmsNotifyInfo(id){
            var url = "/smsNotifyInfos/delete/" + id;
            if (confirm("Are you sure?")) {
            	$http.post(url).then(function(response){
                	vm.deleteMessage = response.data;
                });
            	getPaginatedSmsNotifyInfos(vm.page.number,vm.page.size);
            	getAll();
			}
        }
        function getPaginatedSmsNotifyInfos(pageNumber,size){
            var url = "/smsNotifyInfos/get?page=" + pageNumber + "&size=" + size ;
            var smsNotifyInfosPromise = $http.get(url);
            smsNotifyInfosPromise.then(function(response){
                vm.smsNotifyInfos = response.data.content;
                if (vm.smsNotifyInfos!=="") {
                    vm.page.number = response.data.number;
                    vm.page.size = response.data.size;
                    vm.page.numberOfElements = response.data.numberOfElements;
                    vm.page.first = response.data.first;
                    vm.page.last = response.data.last;
                    vm.page.totalPages = response.data.totalPages;
                    vm.page.totalElements = response.data.totalElements;					
				}
            });
        }
        function previousPage(){
        	if (!vm.page.first) {
            	vm.page.number = vm.page.number - 1;
            	getPaginatedSmsNotifyInfos(vm.page.number,vm.page.size);				
			}
        }
        function nextPage(){
        	if (!vm.page.last) {
            	vm.page.number = vm.page.number + 1;
            	getPaginatedSmsNotifyInfos(vm.page.number,vm.page.size);
			}
        	
        }
        function excelFileUpload(){
            var file = vm.myFile;
            console.log('file is ');
            console.dir(file);
            var uploadUrl = "/smsNotifyInfos/excelFileUpload";
            
            var fd = new FormData();
            fd.append('file', file);
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(response){
            	vm.uploadedMultipleSmsNotifyInfos = response.data;
            });
        }
        function deleteAllTempSmsNotifyInfoListData(){
            vm.uploadedMultipleSmsNotifyInfos = [];
        }
        function saveAllTempSmsNotifyInfoListData(){
        	 $http({
 	            method: "post",
 	            url: "/smsNotifyInfos/saveMultipleSmsNotifyInfoData",
 	            data: vm.uploadedMultipleSmsNotifyInfos,
 	        }).success(function (response) {
 	        	vm.smsNotifyInfos = response.data;
 	        	vm.uploadedMultipleSmsNotifyInfos = [];
 	        }).error(function (err) {
 	            vm.Message = err.Message;
 	        });
        }
    }
})();
