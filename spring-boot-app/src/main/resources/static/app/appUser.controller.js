(function() {
	'use strict';
	angular.module('app').directive('datepicker', function() {
		return {
			require : 'ngModel',
			link : function(scope, element, attr, ngModel) {
				$(element).datepicker({
					dateFormat : "yy-mm-dd",
					onSelect : function(dateText) {
						scope.$apply(function() {
							ngModel.$setViewValue(dateText);
						});
					}
				});
			}
		};
	});
	function timeConverter(UNIX_timestamp) {
		var a = new Date(UNIX_timestamp);
		var months = [ '01', '02', '03', '04', '05', '06', '07', '08',
				'09', '10', '11', '12' ];
		var year = a.getFullYear();
		var month = months[a.getMonth()];
		var date = a.getDate();
		var hour = a.getHours();
		var min = a.getMinutes();
		var sec = a.getSeconds();
		date = parseInt(date)<10 ? '0'+date : date;
		var time = year + '-' + month + '-' + date;
		return time;
	}

	angular.module('app').directive('fileModel', [ '$parse', function($parse) {
		return {
			restrict : 'A',
			link : function(scope, element, attrs) {
				var model = $parse(attrs.fileModel);
				var modelSetter = model.assign;

				element.bind('change', function() {
					scope.$apply(function() {
						modelSetter(scope, element[0].files[0]);
					});
				});
			}
		};
	} ]);

	angular.module('app').service('fileUpload', [ '$http', function($http) {
		this.uploadFileToUrl = function(file, uploadUrl) {
			var fd = new FormData();
			fd.append('file', file);
			$http.post(uploadUrl, fd, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).then(function(response) {
				alert(response);
			});
		}
	} ]);

	angular.module('app').controller('AppUserController',
			AppUserController);

	AppUserController.$inject = [ '$http' ];

	function AppUserController($http, $resource, $scope, fileUpload) {
		var http = $http;
		var vm = this;
		vm.page = {};
		vm.addAppUserMessages = [];
		vm.appUsers = [];
		vm.multipleAppUsers = [];
		vm.uploadedMultipleAppUsers = [];
		vm.editAppUser = editAppUser;
		vm.saveAppUser = saveAppUser;
		vm.saveMultipleAppUserData = saveMultipleAppUserData;
		vm.getPaginatedAppUsers = getPaginatedAppUsers;
		vm.getAll = getAll;
		vm.clearFields = clearFields;
		vm.getUserLogins = getUserLogins;
		vm.excelFileUpload = excelFileUpload;
		vm.deleteAllTempAppUserListData = deleteAllTempAppUserListData;
		vm.saveAllTempAppUserListData = saveAllTempAppUserListData;
		

		vm.addRow = addRow;
		vm.deleteAppUser = deleteAppUser;
		vm.previousPage = previousPage;
		vm.nextPage = nextPage;
		vm.searchOnAppUserTable = searchOnAppUserTable;
		vm.loadOnAppUserDetails = loadOnAppUserDetails;
		
		vm.page.number = 0;
		vm.page.size = 5;

		init();

		function init() {
			getPaginatedAppUsers(vm.page.number, vm.page.size);
			getAll();
			getUserLogins();
			

		}

		function saveAppUser() {

			clearValidationMessages();

			var appUser = {};

			
appUser.userLoginId = vm.userLoginId;
appUser.fullName = vm.fullName;
appUser.password = vm.password;
appUser.isActive = vm.isActive;
appUser.approveBy = vm.approveBy;
appUser.requestedDate = vm.requestedDate;
appUser.approvedDate = vm.approvedDate;
appUser.lastPassword = vm.lastPassword;
appUser.instituteId = vm.instituteId;


			var url = "/appUsers/save";
			$http.post(url, appUser).then(function(response) {
				var messageMap = response.data;

				if (messageMap.error !== "") {
					//vm.orderId_validationMessage =
					//messageMap.orderId_validationMessage;
					
vm.userLoginId_validationMessage = messageMap.userLoginId_validationMessage;
vm.fullName_validationMessage = messageMap.fullName_validationMessage;
vm.password_validationMessage = messageMap.password_validationMessage;
vm.isActive_validationMessage = messageMap.isActive_validationMessage;
vm.approveBy_validationMessage = messageMap.approveBy_validationMessage;
vm.requestedDate_validationMessage = messageMap.requestedDate_validationMessage;
vm.approvedDate_validationMessage = messageMap.approvedDate_validationMessage;
vm.lastPassword_validationMessage = messageMap.lastPassword_validationMessage;
vm.instituteId_validationMessage = messageMap.instituteId_validationMessage;

					vm.technicalError = messageMap.technicalError;
				} else {
					vm.successMessage = messageMap.successMessage;
					getPaginatedAppUsers(vm.page.number, vm.page.size);
					getAll();
					clearFields();

					if (vm.isAllowNotification) {
						sendNotification();
					}
					if (vm.isAllowSendAsTask) {
						sendAsTaskAssignment();
					}
				}

			});
		}
		function clearValidationMessages() {
			//vm.orderId_validationMessage = "";
			
vm.userLoginId_validationMessage = "";
vm.fullName_validationMessage = "";
vm.password_validationMessage = "";
vm.isActive_validationMessage = "";
vm.approveBy_validationMessage = "";
vm.requestedDate_validationMessage = "";
vm.approvedDate_validationMessage = "";
vm.lastPassword_validationMessage = "";
vm.instituteId_validationMessage = "";

		}

		function sendNotification() {
			var notificationMap = {};

			notificationMap.notificationMessage = vm.notificationMessage;
			notificationMap.notificationReceivers = vm.notificationReceivers;
			notificationMap.notifyByEmail = vm.notifyByEmail;
			notificationMap.notifyBySMS = vm.notifyBySMS;

			var url = "/appUsers/sendNotification";
			$http.post(url, notificationMap).then(function(response) {
				//vm.notificationSentResult = response.data;
				alert('Notification Sent Successfully');
				clearNotificationFields();
			});
		}

		function sendAsTaskAssignment() {
			var taskManagement = {};
			taskManagement.assigneeId = vm.assigneeId;
			taskManagement.reporterId = vm.reporterId;
			taskManagement.taskDetails = vm.taskDetails;
			//taskManagement.createdDate = vm.createdDate;
			//taskManagement.planReportDate = vm.planReportDate;
			//taskManagement.actualReportDate = vm.actualReportDate;
			taskManagement.completeParcentage = 40;
			taskManagement.taskStatus = 'Assigned';

			var url = "/taskManagements/save";
			$http.post(url, taskManagement).then(function(response) {
				//vm.addTaskManagementMessages = response.data;
				alert("Task Assigned Successfully");
			});
		}
		function clearNotificationFields() {

			vm.notificationMessage = "";
			vm.districtShortName = "";
			vm.distFullName = "";
			vm.createdDate = "";

		}
		function addRow() {
			vm.multipleAppUsers.push({
			column1: true,column1: true,column1: true,column1: true,column1: true,column1: true,column1: true,column1: true,column1: true,column1: true

			});
		}
		;
		function saveMultipleAppUserData() {
			$http({
				method : "post",
				url : "/appUsers/saveMultipleAppUserData",
				data : vm.multipleAppUsers,
			}).then(function(response) {
				///vm.addAppUserMessages = response.data;
				vm.multipleAppUsers = response.data;
				getPaginatedAppUsers(vm.page.number, vm.page.size);
			});
		}
		;
		function editAppUser(appUser) {
			
vm.userLoginId = appUser.userLoginId;
vm.fullName = appUser.fullName;
vm.password = appUser.password;
vm.isActive = appUser.isActive;
vm.approveBy = appUser.approveBy;
vm.requestedDate = timeConverter(appUser.requestedDate);
vm.approvedDate = timeConverter(appUser.approvedDate);
vm.lastPassword = appUser.lastPassword;
vm.instituteId = appUser.instituteId;

		}
		//newGetRelatedDropdownsEdit
		function clearFields() {
			
vm.userLoginId = "";
vm.fullName = "";
vm.password = "";
vm.isActive = "";
vm.approveBy = "";
vm.requestedDate = "";
vm.approvedDate = "";
vm.lastPassword = "";
vm.instituteId = "";

		}

		function getAll() {
			var url = "/appUsers/all";
			var appUsersPromise = $http.get(url);
			appUsersPromise.then(function(response) {
				vm.multipleAppUsers = response.data;
			});
		}
		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}
		

		function deleteAppUser(id) {
			var url = "/appUsers/delete/" + id;
			if (confirm("Are you sure?")) {
				$http.post(url).then(function(response) {
					vm.deleteMessage = response.data;
				});
				getPaginatedAppUsers(vm.page.number, vm.page.size);
				getAll();
			}
		}
		function getPaginatedAppUsers(pageNumber, size) {
			var url = "/appUsers/get?page=" + pageNumber + "&size=" + size;
			var appUsersPromise = $http.get(url);
			appUsersPromise.then(function(response) {
				vm.appUsers = response.data.content;
				if (vm.appUsers !== "") {
					vm.page.number = response.data.number;
					vm.page.size = response.data.size;
					vm.page.numberOfElements = response.data.numberOfElements;
					vm.page.first = response.data.first;
					vm.page.last = response.data.last;
					vm.page.totalPages = response.data.totalPages;
					vm.page.totalElements = response.data.totalElements;
				}
			});
		}
		function previousPage() {
			if (!vm.page.first) {
				vm.page.number = vm.page.number - 1;
				getPaginatedAppUsers(vm.page.number, vm.page.size);
			}
		}
		function nextPage() {
			if (!vm.page.last) {
				vm.page.number = vm.page.number + 1;
				getPaginatedAppUsers(vm.page.number, vm.page.size);
			}

		}
		function excelFileUpload() {
			var file = vm.myFile;
			console.log('file is ');
			console.dir(file);
			var uploadUrl = "/appUsers/excelFileUpload";

			var fd = new FormData();
			fd.append('file', file);
			$http.post(uploadUrl, fd, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).then(function(response) {
				vm.uploadedMultipleAppUsers = response.data;
			});
		}
		function deleteAllTempAppUserListData() {
			vm.uploadedMultipleAppUsers = [];
		}
		function saveAllTempAppUserListData() {
			$http({
				method : "post",
				url : "/appUsers/saveMultipleAppUserData",
				data : vm.uploadedMultipleAppUsers,
			}).then(function(response) {
				vm.appUsers = response.data;
				vm.uploadedMultipleAppUsers = [];
			});
		}
		//findByUserLoginId
		function searchOnAppUserTable(){
			var url = "/appUsers/searchOnAppUserTable?userLoginId=" + vm.userLoginIdSearch;
			var appUsersPromise = $http.get(url);
			appUsersPromise.then(function(response) {
				vm.appUsers = response.data.content;
				if (vm.appUsers !== "") {
					vm.page.number = response.data.number;
					vm.page.size = response.data.size;
					vm.page.numberOfElements = response.data.numberOfElements;
					vm.page.first = response.data.first;
					vm.page.last = response.data.last;
					vm.page.totalPages = response.data.totalPages;
					vm.page.totalElements = response.data.totalElements;
				}
			});
		}
		function loadOnAppUserDetails(){
			var url = "/appUsers/searchOnAppUserTable?userLoginId=" + vm.userLoginIdToLoad;
			var appUsersPromise = $http.get(url);
			appUsersPromise.then(function(response) {
				vm.appUsers = response.data.content;
				if (vm.appUsers !== "") {
					var appUser = vm.appUsers[0];
					
vm.userLoginId = appUser.userLoginId;
vm.fullName = appUser.fullName;
vm.password = appUser.password;
vm.isActive = appUser.isActive;
vm.approveBy = appUser.approveBy;
vm.requestedDate = timeConverter(appUser.requestedDate);
vm.approvedDate = timeConverter(appUser.approvedDate);
vm.lastPassword = appUser.lastPassword;
vm.instituteId = appUser.instituteId;

				}
			});
		}
		
	}
})();
