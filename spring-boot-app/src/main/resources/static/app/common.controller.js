(function() {
	'use strict';

	angular.module('app')
			.controller('CommonAppController', CommonAppController);

	CommonAppController.$inject = [ '$http', '$timeout' ];

	function CommonAppController($http, $resource, $scope, $timeout) {
		var http = $http;
		var vm = this;
		vm.isLoadNotifications = false;
		vm.time = 0;
		vm.notifications = [];
		vm.userMessages = [];
		vm.taskManagements = [];
		// filtered messge list
		vm.filteredUserMessageList = [];
		vm.receiverId = 1;
		vm.userMessageListByReceiverId = [];
		vm.getUserMessagesByReceiverId = getUserMessagesByReceiverId;

		vm.senderId = 1;
		vm.userMessageListBySenderId = [];
		vm.getUserMessagesBySenderId = getUserMessagesBySenderId;

		vm.userNotificationListByUserName = [];
		vm.getUserNotificationByUsername = getUserNotificationByUsername;

		vm.taskManagementListByAssigneeId = [];
		vm.getTaskManagementByAssigneeId = getTaskManagementByAssigneeId;

		// for paginated data
		vm.notificationPage = {};
		vm.userMessagePage = {};
		vm.taskManagementPage = {};

		vm.notificationPage.number = 0;
		vm.notificationPage.size = 2;

		vm.userMessagePage.number = 0;
		vm.userMessagePage.size = 2;

		vm.taskManagementPage.number = 0;
		vm.taskManagementPage.size = 2;

		vm.getAllNotifications = getAllNotifications;
		vm.getAllUserMessages = getAllUserMessages;
		vm.getAllTaskManagements = getAllTaskManagements;
		vm.changeLanguage = changeLanguage;

		vm.init = function() {
			// getAllNotifications();
			// getPaginatedUserMessages(0, 5);

			if (vm.isLoadNotifications) {
				getUserMessagesByReceiverId(vm.receiverId);
			}
			setTimeout(function() {
				vm.init();
			}, 10000);
		}
		vm.init();
		vm.getUserMessagesByReceiverId(vm.receiverId);

		function getUserMessagesByReceiverId(receiverId) {
			var url = "/userMessages/getUserMessagesByReceiverId?receiverId="
					+ receiverId;
			var userMessagesPromise = http.get(url);
			userMessagesPromise.then(function(response) {
				vm.userMessageListByReceiverId = response.data;
				// vm.filteredUserMessageList = response.data;
				getUserNotificationByUsername(receiverId);
			});
		}
		function getUserNotificationByUsername(username) {
			var url = "/userNotifications/getUserNotificationByUsername?username="
					+ username;
			var userNotificationsPromise = $http.get(url);
			userNotificationsPromise.then(function(response) {
				vm.userNotificationListByUserName = response.data;
				getTaskManagementByAssigneeId(username);
			});
		}
		function getTaskManagementByAssigneeId(assigneeId) {
			var url = "/taskManagements/getTaskManagementByAssigneeId?assigneeId="
					+ assigneeId;
			var taskManagementsPromise = $http.get(url);
			taskManagementsPromise.then(function(response) {
				vm.taskManagementListByAssigneeId = response.data;
			});
		}

		// ///////////////////////////////////////// code to future use
		// /////////////////////////

		function getUserMessagesBySenderId(senderId) {
			var url = "/userMessages/getUserMessagesBySenderId?senderId="
					+ senderId;
			var userMessagesPromise = http.get(url);
			userMessagesPromise.then(function(response) {
				vm.userMessageListBySenderId = response.data;
				vm.filteredUserMessageList = response.data;
			});
		}
		function getAllNotifications() {
			var url = "/notifications/all";
			var notificationsPromise = $http.get(url);
			notificationsPromise.then(function(response) {
				vm.notifications = response.data;
				getAllUserMessages();
			});
		}
		function getAllUserMessages() {
			var url = "/userMessages/all";
			var userMessagessPromise = $http.get(url);
			userMessagessPromise.then(function(response) {
				vm.userMessages = response.data;
				getAllTaskManagements();
			});
		}
		function getAllTaskManagements() {
			var url = "/taskManagements/all";
			var taskManagementsPromise = $http.get(url);
			taskManagementsPromise.then(function(response) {
				vm.taskManagements = response.data;
			});
		}
		function getPaginatedUserMessages(pageNumber, size) {
			var url = "/userMessages/get?page=" + pageNumber + "&size=" + size;
			var userMessagesPromise = $http.get(url);
			userMessagesPromise
					.then(function(response) {
						vm.userMessages = response.data.content;

						vm.userMessagePage.number = response.data.number;
						vm.userMessagePage.size = response.data.size;
						vm.userMessagePage.numberOfElements = response.data.numberOfElements;
						vm.userMessagePage.first = response.data.first;
						vm.userMessagePage.last = response.data.last;
						vm.userMessagePage.totalPages = response.data.totalPages;
						vm.userMessagePage.totalElements = response.data.totalElements;
						getPaginatedTaskManagements(pageNumber, size);
					});
		}
		function previousUserMessagePage() {
			if (!vm.userMessagePage.first) {
				vm.userMessagePage.number = vm.userMessagePage.number - 1;
				getPaginatedUserMessages(vm.userMessagePage.number,
						vm.userMessagePage.size);
			}
		}
		function nextUserMessagePage() {
			if (!vm.userMessagePage.last) {
				vm.userMessagePage.number = vm.userMessagePage.number + 1;
				getPaginatedUserMessages(vm.userMessagePage.number,
						vm.userMessagePage.size);
			}

		}
		function getPaginatedTaskManagements(pageNumber, size) {
			var url = "/taskManagements/get?page=" + pageNumber + "&size="
					+ size;
			var taskManagementsPromise = $http.get(url);
			taskManagementsPromise
					.then(function(response) {
						vm.taskManagements = response.data.content;

						vm.taskManagementPage.number = response.data.number;
						vm.taskManagementPage.size = response.data.size;
						vm.taskManagementPage.numberOfElements = response.data.numberOfElements;
						vm.taskManagementPage.first = response.data.first;
						vm.taskManagementPage.last = response.data.last;
						vm.taskManagementPage.totalPages = response.data.totalPages;
						vm.taskManagementPage.totalElements = response.data.totalElements;
					});
		}

		function previousTaskManagementPage() {
			if (!vm.taskManagementPage.first) {
				vm.taskManagementPage.number = vm.taskManagementPage.number - 1;
				getPaginatedTaskManagements(vm.taskManagementPage.number,
						vm.taskManagementPage.size);
			}
		}
		function nextTaskManagementPage() {
			if (!vm.taskManagementPage.last) {
				vm.taskManagementPage.number = vm.taskManagementPage.number + 1;
				getPaginatedTaskManagements(vm.taskManagementPage.number,
						vm.taskManagementPage.size);
			}

		}

		function getPaginatedNotifications(pageNumber, size) {
			var url = "/notifications/get?page=" + pageNumber + "&size=" + size;
			var notificationsPromise = $http.get(url);
			notificationsPromise
					.then(function(response) {
						vm.notifications = response.data.content;

						vm.notificationPage.number = response.data.number;
						vm.notificationPage.size = response.data.size;
						vm.notificationPage.numberOfElements = response.data.numberOfElements;
						vm.notificationPage.first = response.data.first;
						vm.notificationPage.last = response.data.last;
						vm.notificationPage.totalPages = response.data.totalPages;
						vm.notificationPage.totalElements = response.data.totalElements;
					});
		}
		function previousNotificationPage() {
			if (!vm.notificationPage.first) {
				vm.notificationPage.number = vm.notificationPage.number - 1;
				getPaginatedNotifications(vm.notificationPage.number,
						vm.notificationPage.size);
			}
		}
		function nextNotificationPage() {
			if (!vm.notificationPage.last) {
				vm.notificationPage.number = vm.notificationPage.number + 1;
				getPaginatedNotifications(vm.notificationPage.number,
						vm.notificationPage.size);
			}

		}
		function changeLanguage() {
			var language = vm.language;

			var url = "/appProperties/changeLanguage/" + language;

			$http.post(url).then(function(response) {
				vm.message = response.data;
				alert(vm.message);
			});
		}

	}
})();