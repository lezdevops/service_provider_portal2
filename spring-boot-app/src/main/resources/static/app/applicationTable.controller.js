(function() {
	'use strict';

	angular.module('app').controller('ApplicationTableController',
			ApplicationTableController);

	ApplicationTableController.$inject = [ '$http' ];

	function ApplicationTableController($http, $resource, $scope) {
		var vm = this;
		/* Bootscaff Initializer */
		vm.generateFullScaffolding = true;
		vm.applicationTableSetup = false;
		vm.angularAppCreation = false;
		vm.mobileAppCreation = false;
		vm.threeTierAPICreation = false;
		
		vm.showDataMigrationUI = true;
		vm.showSingleAddForm = true;
		vm.showMultipleAddForm = true;
		vm.enableSendingNotification = true;
		vm.enableTaskAssignment = true;
		vm.isNewController = false;
		vm.existingUIFile = "";
		vm.showEntityList = true;
		vm.showTabularDataRecordList = false;
		vm.enableHistoryTrackingForCRUD = false;
		vm.allowPdfGeneration = true;
		vm.allowExcelGeneration = true;
		vm.allowDynamicReportGeneration = false;
		vm.removeScaffoldedEntity = false;

		/* Bootscaff Initializer */
		vm.addRow = addRow;
		vm.applicationTableList = [];
		vm.uploadedMultipleDatabaseConfigurations = [];
		vm.applicationTables = [];
		vm.moduleFileList = [];
		vm.applicationTableEntityList = [];

		vm.applicationTableEntityDetailsList = [];
		vm.generatedModuleEntityDetailsList = [];

		vm.readDatabaseConfiguration = readDatabaseConfiguration;
		vm.editApplicationTable = editApplicationTable;
		vm.saveApplicationTable = saveApplicationTable;
		vm.getAll = getAll;

		vm.getApplicationModuleList = getApplicationModuleList;
		vm.getModuleFileListByModuleName = getModuleFileListByModuleName;
		vm.getApplicationTableEntityList = getApplicationTableEntityList;

		vm.deleteApplicationTable = deleteApplicationTable;
		vm.saveMultipleApplicationTableData = saveMultipleApplicationTableData;
		vm.getFieldlistByEntityName = getFieldlistByEntityName;
		vm.removeSelectedEntityField = removeSelectedEntityField;
		vm.runBootscaffEngine = runBootscaffEngine;
		vm.deleteScaffoldingOnEntity = deleteScaffoldingOnEntity;
		vm.getApplicationTableEntityDetailsList = getApplicationTableEntityDetailsList;
		vm.getGeneratedModuleEntityDetailsList = getGeneratedModuleEntityDetailsList;
		vm.removeEntityDetails = removeEntityDetails;
		init();

		function init() {
			// getAll();
			getApplicationModuleList();
			getApplicationTableEntityList();
			getApplicationTableEntityDetailsList();
			getGeneratedModuleEntityDetailsList();
		}
		function saveApplicationTable() {

			var applicationTable = {};
			applicationTable.id = vm.id;
			applicationTable.entityName = vm.entityName;
			applicationTable.fieldName = vm.fieldName;
			applicationTable.uiFieldNameDetails = vm.uiFieldNameDetails;
			applicationTable.dataType = vm.dataType;
			applicationTable.inputType = vm.inputType;
			applicationTable.isEmptyCheck = vm.isEmptyCheck;
			applicationTable.isNumeric = vm.isNumeric;
			/*
			 * applicationTable.isUIType = vm.isUIType;
			 * applicationTable.validationMessage = vm.validationMessage;
			 */
			applicationTable.jsValidationFormula = vm.jsValidationFormula;
			applicationTable.belongsToEntityNames = vm.belongsToEntityNames;
			applicationTable.hasEntityNames = vm.hasEntityNames;

			alert(vm.hasEntityNames);
			var url = "/applicationTables/save";
			$http.post(url, applicationTable).then(function(response) {
				vm.applicationTables = response.data;
				clearFields();
			});
		}
		/*
		 * $http({ method: "GET", url: "/Attendance/Details",
		 * }).then(function (data) { $scope.employeeList = data.a;
		 * $scope.dropDownList = data.b; }).error(function (error) {
		 * $scope.Message = error.Message; })
		 */
		/*
		 * private long id; private String entityName; private String fieldName;
		 * private String uiFieldNameDetails; private String dataType; private
		 * String inputType; private String isUIType; private String
		 * isEmptyCheck; private String isNumeric; private String
		 * validationMessage; private String jsValidationFormula; private String
		 * belongsToEntityNames; private String hasEntityNames;
		 */
		function addRow() {
			vm.applicationTableList.push({
				column1 : true,
				column2 : true,
				column3 : true,
				column4 : true,
				column5 : true,
				column6 : true,
				column7 : true,
				column8 : true,
				column9 : true
			});
		}
		;
		function saveMultipleApplicationTableData() {
			// alert(JSON.stringify(vm.applicationTableList));
			for (var int = 0; int < vm.applicationTableList.length; int++) {
				vm.applicationTableList[int].entityName = vm.entityNameToFind;
			}
			
			//alert(vm.applicationTableList[vm.applicationTableList.length-1].entityName);
			$http({
				method : "post",
				url : "/applicationTables/saveMultipleApplicationTableData",
				data : vm.applicationTableList,
			}).then(function(response) {
				// vm.applicationTables = response.data;
				console.log(response.data);
				vm.applicationTableList = response.data;
				vm.getApplicationTableEntityList();
			});

		}

		function editApplicationTable(applicationTable) {
			vm.id = applicationTable.id;
			vm.entityName = applicationTable.entityName;
			vm.fieldName = applicationTable.fieldName;
			vm.uiFieldNameDetails = applicationTable.uiFieldNameDetails;
			vm.dataType = applicationTable.dataType;
			vm.inputType = applicationTable.inputType;
			vm.isEmptyCheck = applicationTable.isEmptyCheck;
			vm.isNumeric = applicationTable.isNumeric;
			/*
			 * vm.isUIType = applicationTable.isUIType; vm.validationMessage =
			 * applicationTable.validationMessage;
			 */
			vm.jsValidationFormula = applicationTable.jsValidationFormula;
			vm.belongsToEntityNames = applicationTable.belongsToEntityNames;
			vm.hasEntityNames = applicationTable.hasEntityNames;
		}
		function clearFields() {
			vm.id = '';
			vm.entityName = '';
			vm.fieldName = '';
			vm.uiFieldNameDetails = '';
			vm.dataType = '';
			vm.inputType = '';
			vm.isUIType = '';
			vm.isEmptyCheck = '';
			vm.isNumeric = '';
			vm.validationMessage = '';
			vm.jsValidationFormula = '';
			vm.belongsToEntityNames = '';
			vm.hasEntityNames = '';
		}

		function getAll() {
			var url = "/applicationTables/all";
			var applicationTablesPromise = $http.get(url);
			applicationTablesPromise.then(function(response) {
				vm.applicationTables = response.data;
			});
		}
		function getApplicationModuleList() {
			var url = "/applicationTables/getApplicationModuleList";
			var applicationTablesPromise = $http.get(url);
			applicationTablesPromise.then(function(response) {
				vm.applicationModuleList = response.data;
			});
		}
		function getApplicationTableEntityList() {
			var url = "/applicationTables/getApplicationTableEntityList";
			var applicationTablesPromise = $http.get(url);
			applicationTablesPromise.then(function(response) {
				vm.applicationTableEntityList = response.data;
			});
		}
		function getModuleFileListByModuleName() {
			var moduleName = vm.applicationModuleName;

			var url = "/applicationTables/getModuleFileListByModuleName/"
					+ moduleName;
			$http.post(url).then(function(response) {
				vm.moduleFileList = response.data;
			});
		}
		function getApplicationTableEntityDetailsList() {
			var url = "/applicationTables/getApplicationTableEntityDetailsList";
			var applicationTablesPromise = $http.get(url);
			applicationTablesPromise.then(function(response) {
				vm.applicationTableEntityDetailsList = response.data;
				// alert(vm.applicationTableEntityDetailsList);
			});
		}
		function getGeneratedModuleEntityDetailsList() {
			var url = "/applicationTables/getGeneratedModuleEntityDetailsList";
			var applicationTablesPromise = $http.get(url);
			applicationTablesPromise.then(function(response) {
				vm.generatedModuleEntityDetailsList = response.data;
			});
		}

		function getFieldlistByEntityName() {

			var entityNameToFind = vm.entityNameToFind;

			var url = "/applicationTables/getFieldlistByEntityName/"
					+ entityNameToFind;
			$http.post(url).then(function(response) {
				vm.applicationTableList = response.data;
			});
		}
		function deleteApplicationTable(id) {
			var url = "/applicationTables/delete/" + id;
			$http.post(url).then(function(response) {
				vm.applicationTables = response.data;
			});
		}
		function removeSelectedEntityField(id) {
			var url = "/applicationTables/removeSelectedEntityField/" + id;
			$http.post(url).then(function(response) {
				vm.applicationTableList = response.data;
			});
		}
		function runBootscaffEngine() {
			var bootScaffEngineMap = {};
			bootScaffEngineMap.moduleName = vm.applicationModuleName;
			
			bootScaffEngineMap.isNewController = vm.isNewController;
			bootScaffEngineMap.uiFileName = vm.uiFileName;
			bootScaffEngineMap.existingUIFile = vm.existingUIFile;
			bootScaffEngineMap.entityName = vm.entityNameToFind;

			bootScaffEngineMap.generateFullScaffolding = vm.generateFullScaffolding;

			bootScaffEngineMap.showDataMigrationUI = vm.showDataMigrationUI;
			bootScaffEngineMap.showSingleAddForm = vm.showSingleAddForm;

			bootScaffEngineMap.showMultipleAddForm = vm.showMultipleAddForm;
			bootScaffEngineMap.enableSendingNotification = vm.enableSendingNotification;
			bootScaffEngineMap.enableTaskAssignment = vm.enableTaskAssignment;
			bootScaffEngineMap.showEntityList = vm.showEntityList;
			bootScaffEngineMap.showTabularDataRecordList = vm.showTabularDataRecordList;

			bootScaffEngineMap.enableHistoryTrackingForCRUD = vm.enableHistoryTrackingForCRUD;
			bootScaffEngineMap.allowPdfGeneration = vm.allowPdfGeneration;
			bootScaffEngineMap.allowExcelGeneration = vm.allowExcelGeneration;
			bootScaffEngineMap.allowDynamicReportGeneration = vm.allowDynamicReportGeneration;

			var url = "/applicationTables/runBootscaffEngine";

			$http.post(url, bootScaffEngineMap).then(function(response) {
				vm.bootScaffSuccessMessages = response.data;
				getApplicationTableEntityDetailsList();
				getGeneratedModuleEntityDetailsList();
			});

		}

		function deleteScaffoldingOnEntity() {
			var bootScaffEngineMap = {};
			bootScaffEngineMap.moduleName = vm.applicationModuleName;
			bootScaffEngineMap.entityName = vm.entityNameToFind;
			bootScaffEngineMap.removeScaffoldedEntity = vm.removeScaffoldedEntity;

			var url = "/applicationTables/deleteScaffoldingOnEntity";

			$http.post(url, bootScaffEngineMap).then(function(response) {
				vm.bootScaffSuccessMessages = response.data;
				// alert("Scaffolding Generation Reversed.");
			});

		}
		
		function readDatabaseConfiguration() {
			var file = vm.myFile;
			console.log('file is ');
			console.dir(file);
			var uploadUrl = "/bootscaff/readDatabaseConfiguration";
			var applicationTableSetup = vm.applicationTableSetup;
			var angularAppCreation = vm.angularAppCreation;
			var mobileAppCreation = vm.mobileAppCreation;
			var threeTierAPICreation = vm.threeTierAPICreation;
			//alert(threeTierAPICreation);
			//return;
			if (threeTierAPICreation) {
				threeTierAPICreation = confirm("Are you sure to prepare API?");				
			}
			if (angularAppCreation) {
				angularAppCreation = confirm("Are you sure to generate Angular App?");				
			}
			if (mobileAppCreation) {
				mobileAppCreation = confirm("Are you sure to generate Mobile App?");				
			}
			if (applicationTableSetup) {
				applicationTableSetup = confirm("Are you sure to Create Component?");				
			}
			var fd = new FormData();
			fd.append('file', file);
			fd.append('applicationTableSetup', applicationTableSetup);
			fd.append('angularAppCreation', angularAppCreation);
			fd.append('mobileAppCreation', mobileAppCreation);
			fd.append('threeTierAPICreation', threeTierAPICreation);
			$http.post(uploadUrl, fd, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).then(function(response) {
				vm.uploadedMultipleDatabaseConfigurations = response.data;
				init();
			});
			
		}
		function deleteAllDatabaseConfigurationListData() {
			vm.uploadedMultipleDatabaseConfigurations = [];
		}
		function saveAllDatabaseConfigurationListData() {
			$http({
				method : "post",
				url : "/bootscaff/savebootscaffData",
				data : vm.uploadedMultipleDatabaseConfigurations,
			}).then(function(response) {
				vm.bootscaffDatabaseMaps = response.data;
				vm.uploadedMultipleDatabaseConfigurations = [];
			});
		}
		function removeEntityDetails(entityName){
			if (!confirm("Are You Sure!")) {
				return false;
			}
			var url = "/applicationTables/removeEntityDetails/" + entityName;
			$http.post(url).then(function(response) {
				//vm.applicationTableList = response.data;
			});
		}
	}
})();
