
(function () {
    'use strict';

    angular.module('app').controller('ComponentEntityMapController', ComponentEntityMapController);

    ComponentEntityMapController.$inject = ['$http'];

    function ComponentEntityMapController($http,$resource,$scope) {
    	var http = $http;
        var vm = this;
        vm.page = {};
        vm.addComponentEntityMapMessages = [];
        vm.componentEntityMaps = [];
        vm.multipleComponentEntityMaps = [];
        vm.editComponentEntityMap = editComponentEntityMap;
        vm.saveComponentEntityMap = saveComponentEntityMap;
        vm.saveMultipleComponentEntityMapData = saveMultipleComponentEntityMapData;
        vm.getPaginatedComponentEntityMaps = getPaginatedComponentEntityMaps;
        vm.getAll = getAll;
        vm.getUserLogins = getUserLogins;
        

        vm.addRow = addRow;
        vm.deleteComponentEntityMap = deleteComponentEntityMap;
        vm.previousPage = previousPage;
        vm.nextPage = nextPage;
        
        vm.page.number = 0;
    	vm.page.size =2;
    	
        init();

        function init(){
        	getPaginatedComponentEntityMaps(vm.page.number,vm.page.size);
        	getAll();
        	getUserLogins();
        	

        }
        function saveComponentEntityMap(){
        	var componentEntityMap = {};
        	
        	
componentEntityMap.entityMapId = vm.entityMapId;
componentEntityMap.componentName = vm.componentName;
componentEntityMap.entityName = vm.entityName;
componentEntityMap.creationDate = vm.creationDate;

        	
    	   var url = "/componentEntityMaps/save";
	       $http.post(url,componentEntityMap).then(function(response){
	    	   vm.addComponentEntityMapMessages = response.data;
	    	   getPaginatedComponentEntityMaps(vm.page.number,vm.page.size);
	        	getAll();
	          clearFields();

				if (vm.isAllowNotification) {
					sendNotification();
				}
				if (vm.isAllowSendAsTask) {
					sendAsTaskAssignment();
				}
	       });
        }

        function sendNotification() {
			var notificationMap = {};

			notificationMap.notificationMessage = vm.notificationMessage;
			notificationMap.notificationReceivers = vm.notificationReceivers;
			notificationMap.notifyByEmail = vm.notifyByEmail;
			notificationMap.notifyBySMS = vm.notifyBySMS;

			var url = "/students/sendNotification";
			$http.post(url, notificationMap).then(function(response) {
				//vm.notificationSentResult = response.data;
				alert('Notification Sent Successfully');
				clearNotificationFields();
			});
		}

		function sendAsTaskAssignment() {
			var taskManagement = {};
			taskManagement.assigneeId = vm.assigneeId;
			taskManagement.reporterId = vm.reporterId;
			taskManagement.taskDetails = vm.taskDetails;
			//taskManagement.createdDate = vm.createdDate;
			//taskManagement.planReportDate = vm.planReportDate;
			//taskManagement.actualReportDate = vm.actualReportDate;
			taskManagement.completeParcentage = 40;
			taskManagement.taskStatus = 'Assigned';

			var url = "/taskManagements/save";
			$http.post(url, taskManagement).then(function(response) {
				//vm.addTaskManagementMessages = response.data;
				alert("Task Assigned Successfully");
			});
		}
        function clearNotificationFields(){
        	
        	vm.notificationMessage = "";
        	vm.districtShortName = "";
        	vm.distFullName = "";
        	vm.createdDate = "";

        }
        function addRow() {
	        vm.multipleComponentEntityMaps.push(
	            { 
	             column1: true,column1: true,column1: true,column1: true,column1: true

	             }
	        );
	    };
        function saveMultipleComponentEntityMapData() {
	        $http({
	            method: "post",
	            url: "/componentEntityMaps/saveMultipleComponentEntityMapData",
	            data: vm.multipleComponentEntityMaps,
	        }).success(function (data) {
//	        	/vm.addComponentEntityMapMessages = response.data;
	        	vm.multipleComponentEntityMaps = response.data;
	        	getPaginatedComponentEntityMaps(vm.page.number,vm.page.size);
	        }).error(function (err) {
	            vm.Message = err.Message;
	        })
	    };
        function editComponentEntityMap(componentEntityMap){
        	
vm.entityMapId = componentEntityMap.entityMapId;
vm.componentName = componentEntityMap.componentName;
vm.entityName = componentEntityMap.entityName;
vm.creationDate = componentEntityMap.creationDate;

        }
        function clearFields(){
        	
vm.entityMapId = "";
vm.componentName = "";
vm.entityName = "";
vm.creationDate = "";

        }
       
        function getAll(){
            var url = "/componentEntityMaps/all";
            var componentEntityMapsPromise = $http.get(url);
            componentEntityMapsPromise.then(function(response){
                vm.multipleComponentEntityMaps = response.data;
            });
        }
        function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}
      

        function deleteComponentEntityMap(id){
            var url = "/componentEntityMaps/delete/" + id;
            $http.post(url).then(function(response){
            	vm.deleteMessage = response.data;
            	getPaginatedComponentEntityMaps(vm.page.number,vm.page.size);
            	//getAll();
            });
        }
        function getPaginatedComponentEntityMaps(pageNumber,size){
            var url = "/componentEntityMaps/get?page=" + pageNumber + "&size=" + size ;
            var componentEntityMapsPromise = $http.get(url);
            componentEntityMapsPromise.then(function(response){
                vm.componentEntityMaps = response.data.content;

                vm.page.number = response.data.number;
                vm.page.size = response.data.size;
                vm.page.numberOfElements = response.data.numberOfElements;
                vm.page.first = response.data.first;
                vm.page.last = response.data.last;
                vm.page.totalPages = response.data.totalPages;
                vm.page.totalElements = response.data.totalElements;
            });
        }
        function previousPage(){
        	if (!vm.page.first) {
            	vm.page.number = vm.page.number - 1;
            	getPaginatedComponentEntityMaps(vm.page.number,vm.page.size);				
			}
        }
        function nextPage(){
        	if (!vm.page.last) {
            	vm.page.number = vm.page.number + 1;
            	getPaginatedComponentEntityMaps(vm.page.number,vm.page.size);
			}
        	
        }
    }
})();
