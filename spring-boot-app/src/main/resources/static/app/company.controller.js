(function() {
	'use strict';
	angular.module('app').directive('datepicker', function() {
		return {
			require : 'ngModel',
			link : function(scope, element, attr, ngModel) {
				$(element).datepicker({
					dateFormat : "yy-mm-dd",
					onSelect : function(dateText) {
						scope.$apply(function() {
							ngModel.$setViewValue(dateText);
						});
					}
				});
			}
		};
	});
	function timeConverter(UNIX_timestamp) {
		var a = new Date(UNIX_timestamp);
		var months = [ '01', '02', '03', '04', '05', '06', '07', '08',
				'09', '10', '11', '12' ];
		var year = a.getFullYear();
		var month = months[a.getMonth()];
		var date = a.getDate();
		var hour = a.getHours();
		var min = a.getMinutes();
		var sec = a.getSeconds();
		date = parseInt(date)<10 ? '0'+date : date;
		var time = year + '-' + month + '-' + date;
		return time;
	}

	angular.module('app').directive('fileModel', [ '$parse', function($parse) {
		return {
			restrict : 'A',
			link : function(scope, element, attrs) {
				var model = $parse(attrs.fileModel);
				var modelSetter = model.assign;

				element.bind('change', function() {
					scope.$apply(function() {
						modelSetter(scope, element[0].files[0]);
					});
				});
			}
		};
	} ]);

	angular.module('app').service('fileUpload', [ '$http', function($http) {
		this.uploadFileToUrl = function(file, uploadUrl) {
			var fd = new FormData();
			fd.append('file', file);
			$http.post(uploadUrl, fd, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).then(function(response) {
				alert(response);
			});
		}
	} ]);

	angular.module('app').controller('CompanyController',
			CompanyController);

	CompanyController.$inject = [ '$http' ];

	function CompanyController($http, $resource, $scope, fileUpload) {
		var http = $http;
		var vm = this;
		vm.page = {};
		vm.addCompanyMessages = [];
		vm.companys = [];
		vm.multipleCompanys = [];
		vm.uploadedMultipleCompanys = [];
		vm.editCompany = editCompany;
		vm.saveCompany = saveCompany;
		vm.saveMultipleCompanyData = saveMultipleCompanyData;
		vm.getPaginatedCompanys = getPaginatedCompanys;
		vm.getAll = getAll;
		vm.clearFields = clearFields;
		vm.getUserLogins = getUserLogins;
		vm.excelFileUpload = excelFileUpload;
		vm.deleteAllTempCompanyListData = deleteAllTempCompanyListData;
		vm.saveAllTempCompanyListData = saveAllTempCompanyListData;
		

		vm.addRow = addRow;
		vm.deleteCompany = deleteCompany;
		vm.previousPage = previousPage;
		vm.nextPage = nextPage;
		vm.searchOnCompanyTable = searchOnCompanyTable;
		vm.loadOnCompanyDetails = loadOnCompanyDetails;
		
		vm.page.number = 0;
		vm.page.size = 5;

		init();

		function init() {
			getPaginatedCompanys(vm.page.number, vm.page.size);
			getAll();
			getUserLogins();
			

		}

		function saveCompany() {

			clearValidationMessages();

			var company = {};

			
company.companyId = vm.companyId;
company.companyName = vm.companyName;
company.createdDate = vm.createdDate;


			var url = "/companys/save";
			$http.post(url, company).then(function(response) {
				var messageMap = response.data;

				if (messageMap.error !== "") {
					//vm.orderId_validationMessage =
					//messageMap.orderId_validationMessage;
					
vm.companyId_validationMessage = messageMap.companyId_validationMessage;
vm.companyName_validationMessage = messageMap.companyName_validationMessage;
vm.createdDate_validationMessage = messageMap.createdDate_validationMessage;

					vm.technicalError = messageMap.technicalError;
				} else {
					vm.successMessage = messageMap.successMessage;
					getPaginatedCompanys(vm.page.number, vm.page.size);
					getAll();
					clearFields();

					if (vm.isAllowNotification) {
						sendNotification();
					}
					if (vm.isAllowSendAsTask) {
						sendAsTaskAssignment();
					}
				}

			});
		}
		function clearValidationMessages() {
			//vm.orderId_validationMessage = "";
			
vm.companyId_validationMessage = "";
vm.companyName_validationMessage = "";
vm.createdDate_validationMessage = "";

		}

		function sendNotification() {
			var notificationMap = {};

			notificationMap.notificationMessage = vm.notificationMessage;
			notificationMap.notificationReceivers = vm.notificationReceivers;
			notificationMap.notifyByEmail = vm.notifyByEmail;
			notificationMap.notifyBySMS = vm.notifyBySMS;

			var url = "/companys/sendNotification";
			$http.post(url, notificationMap).then(function(response) {
				//vm.notificationSentResult = response.data;
				alert('Notification Sent Successfully');
				clearNotificationFields();
			});
		}

		function sendAsTaskAssignment() {
			var taskManagement = {};
			taskManagement.assigneeId = vm.assigneeId;
			taskManagement.reporterId = vm.reporterId;
			taskManagement.taskDetails = vm.taskDetails;
			//taskManagement.createdDate = vm.createdDate;
			//taskManagement.planReportDate = vm.planReportDate;
			//taskManagement.actualReportDate = vm.actualReportDate;
			taskManagement.completeParcentage = 40;
			taskManagement.taskStatus = 'Assigned';

			var url = "/taskManagements/save";
			$http.post(url, taskManagement).then(function(response) {
				//vm.addTaskManagementMessages = response.data;
				alert("Task Assigned Successfully");
			});
		}
		function clearNotificationFields() {

			vm.notificationMessage = "";
			vm.districtShortName = "";
			vm.distFullName = "";
			vm.createdDate = "";

		}
		function addRow() {
			vm.multipleCompanys.push({
			column1: true,column1: true,column1: true,column1: true

			});
		}
		;
		function saveMultipleCompanyData() {
			$http({
				method : "post",
				url : "/companys/saveMultipleCompanyData",
				data : vm.multipleCompanys,
			}).success(function(data) {
				///vm.addCompanyMessages = response.data;
				vm.multipleCompanys = response.data;
				getPaginatedCompanys(vm.page.number, vm.page.size);
			}).error(function(err) {
				vm.Message = err.Message;
			})
		}
		;
		function editCompany(company) {
			

		}
		//newGetRelatedDropdownsEdit
		function clearFields() {
			
vm.companyId = "";
vm.companyName = "";
vm.createdDate = "";

		}

		function getAll() {
			var url = "/companys/all";
			var companysPromise = $http.get(url);
			companysPromise.then(function(response) {
				vm.multipleCompanys = response.data;
			});
		}
		function getUserLogins() {
			var url = "/userLogins/all";
			var userLoginsPromise = http.get(url);
			userLoginsPromise.then(function(response) {
				vm.userLoginList = response.data;
			});
		}
		

		function deleteCompany(id) {
			var url = "/companys/delete/" + id;
			if (confirm("Are you sure?")) {
				$http.post(url).then(function(response) {
					vm.deleteMessage = response.data;
				});
				getPaginatedCompanys(vm.page.number, vm.page.size);
				getAll();
			}
		}
		function getPaginatedCompanys(pageNumber, size) {
			var url = "/companys/get?page=" + pageNumber + "&size=" + size;
			var companysPromise = $http.get(url);
			companysPromise.then(function(response) {
				vm.companys = response.data.content;
				if (vm.companys !== "") {
					vm.page.number = response.data.number;
					vm.page.size = response.data.size;
					vm.page.numberOfElements = response.data.numberOfElements;
					vm.page.first = response.data.first;
					vm.page.last = response.data.last;
					vm.page.totalPages = response.data.totalPages;
					vm.page.totalElements = response.data.totalElements;
				}
			});
		}
		function previousPage() {
			if (!vm.page.first) {
				vm.page.number = vm.page.number - 1;
				getPaginatedCompanys(vm.page.number, vm.page.size);
			}
		}
		function nextPage() {
			if (!vm.page.last) {
				vm.page.number = vm.page.number + 1;
				getPaginatedCompanys(vm.page.number, vm.page.size);
			}

		}
		function excelFileUpload() {
			var file = vm.myFile;
			console.log('file is ');
			console.dir(file);
			var uploadUrl = "/companys/excelFileUpload";

			var fd = new FormData();
			fd.append('file', file);
			$http.post(uploadUrl, fd, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).then(function(response) {
				vm.uploadedMultipleCompanys = response.data;
			});
		}
		function deleteAllTempCompanyListData() {
			vm.uploadedMultipleCompanys = [];
		}
		function saveAllTempCompanyListData() {
			$http({
				method : "post",
				url : "/companys/saveMultipleCompanyData",
				data : vm.uploadedMultipleCompanys,
			}).success(function(response) {
				vm.companys = response.data;
				vm.uploadedMultipleCompanys = [];
			}).error(function(err) {
				vm.Message = err.Message;
			});
		}
		//findByCompanyId
		function searchOnCompanyTable(){
			var url = "/companys/searchOnCompanyTable?companyId=" + vm.companyIdSearch;
			var companysPromise = $http.get(url);
			companysPromise.then(function(response) {
				vm.companys = response.data.content;
				if (vm.companys !== "") {
					vm.page.number = response.data.number;
					vm.page.size = response.data.size;
					vm.page.numberOfElements = response.data.numberOfElements;
					vm.page.first = response.data.first;
					vm.page.last = response.data.last;
					vm.page.totalPages = response.data.totalPages;
					vm.page.totalElements = response.data.totalElements;
				}
			});
		}
		function loadOnCompanyDetails(){
			var url = "/companys/searchOnCompanyTable?companyId=" + vm.companyIdToLoad;
			var companysPromise = $http.get(url);
			companysPromise.then(function(response) {
				vm.companys = response.data.content;
				if (vm.companys !== "") {
					var company = vm.companys[0];
					

				}
			});
		}
		
	}
})();
