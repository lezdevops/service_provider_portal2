<?php
include_once 'includes/header.php';
?>
<?php
$selectedMenu = 'BanglaWriting';
include_once 'includes/menus.php';
?>
<script
	src="http://arifrahman.site11.com/responsive/bangla_writing/layout.js"></script>
<script
	src="http://arifrahman.site11.com/responsive/bangla_writing/common.js"></script>
<script
	src="http://arifrahman.site11.com/responsive/bangla_writing/main.js"></script>
<!--This CSS part is needed to show virtual keyboard perfectly-->
<style type="text/css">
table.inlinekbtable tbody tr {
	border: 1px solid #cfcfcf;
	background: #F4F1C0;
}

table.inlinekbtable tbody td {
	border: 1px solid #999900;
	padding: 0.01em 0.1em 0.01em 0.1em;
	font-weight: normal;
	font-size: 1.3em;
	color: #000000;
	font-family: SolaimanLipi, AponaLohit, Bangla, Siyam Rupali, Likhan,
		Vrinda, Verdana, Helvetica, Arial, Sans-serif;
	cursor: pointer;
	text-align: center !important;
}

table.inlinekbtable tbody td:hover {
	background-color: #CCFF00 !important;
	color: #000000;
}
</style>
<!--End CSS for virtual keyboard-->
<style type="text/css">
p.english1 {
	font-family: Verdana, Arial, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #187104
}

p.bengali1 {
	font-family: Vrinda, sans-serif;
	font-size: 24px;
	color: #A52A2A
}

p.english {
	font-family: Verdana, Arial, sans-serif;
	font-size: 12px;
	color: #000000
}

p.bengali {
	font-family: Vrinda, sans-serif;
	font-size: 13px;
	color: #000000
}

.bengali {
	font-family: SolaimanLipi, vrinda;
	font-size: 16px;
	color: #000000
}

.style1e {
	font-family: Verdana, Arial, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #187104
}

.style2e {
	font-family: Verdana, Arial, sans-serif;
	font-size: 12px;
	color: #000000
}

.style1b {
	font-family: Vrinda, sans-serif;
	font-size: 24px;
	color: #A52A2A
}

.style2b {
	font-family: SolaimanLipi, Vrinda, sans-serif;
	font-size: 18px;
	color: #000000
}

.style3 {
	font-family: SolaimanLipi, vrinda;
	font-size: 18px;
	font-weight: bold;
	color: #1A7C04
}

.class1 A:link {
	font-family: Verdana, Arial, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: yellow;
	text-decoration: none
}

.class1 A:visited {
	font-family: Verdana, Arial, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: yellow;
	text-decoration: none
}

.class1 A:active {
	text-decoration: none
}

.class1 A:hover {
	text-decoration: underline;
	color: white;
}

.class2 A:link {
	font-family: Verdana, Arial, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #151B8D;
	text-decoration: none
}

.class2 A:visited {
	font-family: Verdana, Arial, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #151B8D;
	text-decoration: none
}

.class2 A:active {
	text-decoration: underline
}

.class2 A:hover {
	text-decoration: underline;
	color: #C12267;
}

.roundedcornr_box_254073 {
	background: url(bangla-Images/roundedcornr_254073_tl.png) no-repeat top
		left;
}

.roundedcornr_top_254073 {
	background: url(bangla-Images/roundedcornr_254073_tr.png) no-repeat top
		right;
}

.roundedcornr_bottom_254073 {
	background: url(bangla-Images/roundedcornr_254073_bl.png) no-repeat
		bottom left;
}

.roundedcornr_bottom_254073 div {
	background: url(bangla-Images/roundedcornr_254073_br.png) no-repeat
		bottom right;
}

.roundedcornr_content_254073 {
	background: url(bangla-Images/roundedcornr_254073_r.png) top right
		repeat-y;
}

.roundedcornr_top_254073 div, .roundedcornr_top_254073,
	.roundedcornr_bottom_254073 div, .roundedcornr_bottom_254073 {
	width: 100%;
	height: 15px;
	font-size: 1px;
}

.roundedcornr_content_254073, .roundedcornr_bottom_254073 {
	margin-top: -19px;
}

.roundedcornr_content_254073 {
	padding: 0 15px;
}
</style>
<!--Script for Select All button-->
<script type="text/javascript">
function selectAll()
{
document.formvkb.textfield.focus();
document.formvkb.textfield.select();
}
</script>
<script type="text/javascript">
function backspace()
{
formvkb.textfield.value = formvkb.textfield.value.substring(0, formvkb.textfield.value.length -1);
}
$(document).ready(function(){
	document.formvkb.textfield.focus();
});
</script>
<!-- Page Content -->
<div class="container">

	<div class="row">
		<div class="col-md-8">
			<table align="center" border="0" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td align="center" bgcolor="#93a2cd">
							<table align="center" border="0" cellpadding="0" cellspacing="0"
								width="100%">
								<tbody>
									<tr bgcolor="#d5d2f4">
										<td align="center" valign="middle">
											<!-- content table start -->
											<table border="1" bordercolor="#347c17" cellpadding="2"
												cellspacing="2" width="100%">
												<tbody>

													<tr>
														<td align="center" bgcolor="#ddb9f6" valign="top"
															width="710">
															<!-- content left col start-->
															<table align="center" border="0" cellpadding="0"
																cellspacing="2" width="100%">
																<tbody>

																	<tr>
																		<td align="center" valign="top">
																			<form name="formvkb">
																				<table align="center" bgcolor="#d5d2f4" border="0"
																					cellpadding="2" cellspacing="0" width="100%">
																					<tbody>
																						<tr>
																							<td align="center" valign="top"><input
																								onclick="return KeyboardLayoutOptionClick(event);"
																								value="English" name="KeyboardLayoutOption"
																								type="radio"><span class="style3">ইংরেজি&nbsp;&nbsp;&nbsp;
																							</span><input
																								onclick="return KeyboardLayoutOptionClick(event);"
																								value="Bijoy" name="KeyboardLayoutOption"
																								type="radio"><span class="style3">বিজয়&nbsp;&nbsp;&nbsp;</span>
																								<input
																								onclick="return KeyboardLayoutOptionClick(event);"
																								checked="checked" value="Somewhere-in"
																								name="KeyboardLayoutOption" type="radio"><span
																								class="style3">ফনেটিক&nbsp;&nbsp;</span> <!--<input onclick="return KeyboardLayoutOptionClick(event);" value="Avro" name="KeyboardLayoutOption" type="radio"><span class="style3">(অভ্র ফনেটিক)&nbsp;</span> <input onclick="return KeyboardLayoutOptionClick(event);" value="Unijoy" name="KeyboardLayoutOption" type="radio"><span class="style3">ইউনিজয়&nbsp; </span>-->
																							</td>
																						</tr>
																						<tr>
																							<td align="center" valign="top"><textarea
																									name="TextField" cols="70" rows="16"
																									class="bangla" id="textfield"
																									style="font-size: 12pt; font-family: SolaimanLipi;"
																									onkeypress="return KeyBoardPress(event);"
																									onkeydown="return KeyBoardDown(event);"></textarea><br>
																								<!--Use this link for toggling(Show/hide) virtual keyboad -->
																								<a href="#" onclick="showhide('inlinevkb');"
																								id="linkid">Show virtual keyboard</a><br> <!--Start Inline Virtual keyboard--->
																								<div id="inlinevkb" style="display: none;"
																									align="center">
																									<table class="inlinekbtable" border="1"
																										cellpadding="1">
																										<tbody>
																											<tr>
																												<td
																													onclick="insertAtCursor('!');self.focus();">!</td>
																												<td
																													onclick="insertAtCursor('ক');self.focus();">ক</td>
																												<td
																													onclick="insertAtCursor('খ');self.focus();">খ</td>
																												<td
																													onclick="insertAtCursor('গ');self.focus();">গ</td>
																												<td
																													onclick="insertAtCursor('ঘ');self.focus();">ঘ</td>
																												<td
																													onclick="insertAtCursor('ঙ');self.focus();">ঙ</td>
																												<td
																													onclick="insertAtCursor('চ');self.focus();">চ</td>
																												<td
																													onclick="insertAtCursor('ছ');self.focus();">ছ</td>
																												<td
																													onclick="insertAtCursor('জ');self.focus();">জ</td>
																												<td
																													onclick="insertAtCursor('ঝ');self.focus();">ঝ</td>
																												<td
																													onclick="insertAtCursor('ঞ');self.focus();">ঞ</td>
																												<td
																													onclick="insertAtCursor('ট');self.focus();">ট</td>
																												<td
																													onclick="insertAtCursor('ঠ');self.focus();">ঠ</td>
																												<td
																													onclick="insertAtCursor('ড');self.focus();">ড</td>
																												<td
																													onclick="insertAtCursor('ঢ');self.focus();">ঢ</td>
																												<td
																													onclick="insertAtCursor('ণ');self.focus();">ণ</td>
																												<td
																													onclick="insertAtCursor('\u200C'); insertAtCursor('র্'); self.focus();"
																													align="center"><input value="রেফ"
																													type="button">
																												</td>
																												<td colspan="3"><input name="BackSpace"
																													value="←Del" onclick="backspace();"
																													type="button">
																												</td>
																												<td
																													onclick="insertAtCursor('১');self.focus();">১</td>
																												<td
																													onclick="insertAtCursor('২');self.focus();">২</td>
																											</tr>
																											<tr>
																												<td
																													onclick="insertAtCursor('\u200D');self.focus();">~</td>
																												<td
																													onclick="insertAtCursor('ত');self.focus();">ত</td>
																												<td
																													onclick="insertAtCursor('থ');self.focus();">থ</td>
																												<td
																													onclick="insertAtCursor('দ');self.focus();">দ</td>
																												<td
																													onclick="insertAtCursor('ধ');self.focus();">ধ</td>
																												<td
																													onclick="insertAtCursor('ন');self.focus();">ন</td>
																												<td
																													onclick="insertAtCursor('প');self.focus();">প</td>
																												<td
																													onclick="insertAtCursor('ফ');self.focus();">ফ</td>
																												<td
																													onclick="insertAtCursor('ব');self.focus();">ব</td>
																												<td
																													onclick="insertAtCursor('ভ');self.focus();">ভ</td>
																												<td
																													onclick="insertAtCursor('ম');self.focus();">ম</td>
																												<td
																													onclick="insertAtCursor('য');self.focus();">য</td>
																												<td
																													onclick="insertAtCursor('র');self.focus();">র</td>
																												<td
																													onclick="insertAtCursor('ল');self.focus();">ল</td>
																												<td
																													onclick="insertAtCursor('শ');self.focus();">শ</td>
																												<td
																													onclick="insertAtCursor('ষ');self.focus();">ষ</td>
																												<td
																													onclick="insertAtCursor('\u09CD');insertAtCursor('\u09AF');self.focus();"
																													#f4c1c0="" align="center" bgcolor=""><input
																													value="য-ফলা" type="button">
																												</td>
																												<td
																													onclick="insertAtCursor('\u09f3');self.focus();">৳</td>
																												<td
																													onclick="insertAtCursor('-');self.focus();">-</td>
																												<td
																													onclick="insertAtCursor('+');self.focus();">+</td>
																												<td
																													onclick="insertAtCursor('৩');self.focus();">৩</td>
																												<td
																													onclick="insertAtCursor('৪');self.focus();">৪</td>
																											</tr>
																											<tr>
																												<td
																													onclick="insertAtCursor('\u200C');self.focus();">`</td>
																												<td
																													onclick="insertAtCursor('স');self.focus();">স</td>
																												<td
																													onclick="insertAtCursor('হ');self.focus();">হ</td>
																												<td
																													onclick="insertAtCursor('ড়');self.focus();">ড়</td>
																												<td
																													onclick="insertAtCursor('ঢ়');self.focus();">ঢ়</td>
																												<td
																													onclick="insertAtCursor('য়');self.focus();">য়</td>
																												<td
																													onclick="insertAtCursor('ৎ');self.focus();">ৎ</td>
																												<td
																													onclick="insertAtCursor('ং');self.focus();">ং</td>
																												<td
																													onclick="insertAtCursor('ঃ');self.focus();">ঃ</td>
																												<td
																													onclick="insertAtCursor('ঁ');self.focus();">ঁ</td>
																												<td
																													onclick="insertAtCursor('―');self.focus();">―</td>
																												<td
																													onclick="insertAtCursor('=');self.focus();">=</td>
																												<td
																													onclick="insertAtCursor('?');self.focus();">?</td>
																												<td
																													onclick="insertAtCursor(':');self.focus();">:</td>
																												<td
																													onclick="insertAtCursor('\u0027');self.focus();">'</td>
																												<td
																													onclick="insertAtCursor('\u0022');self.focus();">"</td>
																												<td
																													onclick="insertAtCursor('\u09CD');self.focus();"><input
																													value="-যুক্ত-" type="button"></td>
																												<td colspan="3"
																													onclick="insertAtCursor('\n');self.focus();"><input
																													value="Enter" type="button"></td>
																												<td
																													onclick="insertAtCursor('৫');self.focus();">৫</td>
																												<td
																													onclick="insertAtCursor('৬');self.focus();">৬</td>
																											</tr>
																											<tr>
																												<td
																													onclick="insertAtCursor('[');self.focus();">[</td>
																												<td
																													onclick="insertAtCursor(']');self.focus();">]</td>
																												<td
																													onclick="insertAtCursor('অ');self.focus();"
																													bgcolor="#e8e38e">অ</td>
																												<td
																													onclick="insertAtCursor('আ');self.focus();"
																													bgcolor="#e8e38e">আ</td>
																												<td
																													onclick="insertAtCursor('ই');self.focus();"
																													bgcolor="#e8e38e">ই</td>
																												<td
																													onclick="insertAtCursor('ঈ');self.focus();"
																													bgcolor="#e8e38e">ঈ</td>
																												<td
																													onclick="insertAtCursor('উ');self.focus();"
																													bgcolor="#e8e38e">উ</td>
																												<td
																													onclick="insertAtCursor('ঊ');self.focus();"
																													bgcolor="#e8e38e">ঊ</td>
																												<td
																													onclick="insertAtCursor('ঋ');self.focus();"
																													bgcolor="#e8e38e">ঋ</td>
																												<td
																													onclick="insertAtCursor('এ');self.focus();"
																													bgcolor="#e8e38e">এ</td>
																												<td
																													onclick="insertAtCursor('ঐ');self.focus();"
																													bgcolor="#e8e38e">ঐ</td>
																												<td
																													onclick="insertAtCursor('ও');self.focus();"
																													bgcolor="#e8e38e">ও</td>
																												<td
																													onclick="insertAtCursor('ঔ');self.focus();"
																													bgcolor="#e8e38e">ঔ</td>
																												<td
																													onclick="insertAtCursor(';');self.focus();">;</td>
																												<td
																													onclick="insertAtCursor(',');self.focus();">,</td>
																												<td
																													onclick="insertAtCursor('\u0964');self.focus();">।</td>
																												<td
																													onclick="insertAtCursor('\u09CD');insertAtCursor('র'); self.focus();"
																													align="center"><input value="র-ফলা‌‌‌"
																													type="button">
																												</td>
																												<td
																													onclick="insertAtCursor('%');self.focus();">%</td>
																												<td
																													onclick="insertAtCursor('*');self.focus();">*</td>
																												<td
																													onclick="insertAtCursor('/');self.focus();">/</td>
																												<td
																													onclick="insertAtCursor('৭');self.focus();">৭</td>
																												<td
																													onclick="insertAtCursor('৮');self.focus();">৮</td>
																											</tr>
																											<tr>
																												<td colspan="3"
																													onclick="insertAtCursor(' ');self.focus();"><input
																													value="Space" type="button">
																												</td>
																												<td
																													onclick="insertAtCursor('া');self.focus();"
																													bgcolor="#e8e38e">া</td>
																												<td
																													onclick="insertAtCursor('ি');self.focus();"
																													bgcolor="#e8e38e">ি</td>
																												<td
																													onclick="insertAtCursor('ী');self.focus();"
																													bgcolor="#e8e38e">ী</td>
																												<td
																													onclick="insertAtCursor('ু');self.focus();"
																													bgcolor="#e8e38e">ু</td>
																												<td
																													onclick="insertAtCursor('ূ');self.focus();"
																													bgcolor="#e8e38e">ূ</td>
																												<td
																													onclick="insertAtCursor('ৃ');self.focus();"
																													bgcolor="#e8e38e">ৃ</td>
																												<td
																													onclick="insertAtCursor('ে');self.focus();"
																													bgcolor="#e8e38e">ে</td>
																												<td
																													onclick="insertAtCursor('ৈ');self.focus();"
																													bgcolor="#e8e38e">ৈ</td>
																												<td
																													onclick="insertAtCursor('ো');self.focus();"
																													bgcolor="#e8e38e">ো</td>
																												<td
																													onclick="insertAtCursor('ৌ');self.focus();"
																													bgcolor="#e8e38e">ৌ</td>
																												<td colspan="3"
																													onclick="insertAtCursor(' ');self.focus();"><input
																													value="Space" type="button">
																												</td>
																												<td
																													onclick="insertAtCursor('\u09CD');self.focus();"><input
																													value="হসন্ত" type="button">
																												</td>
																												<td
																													onclick="insertAtCursor('(');self.focus();">(</td>
																												<td
																													onclick="insertAtCursor(')');self.focus();">)</td>
																												<td
																													onclick="insertAtCursor('.');self.focus();">.</td>
																												<td
																													onclick="insertAtCursor('৯');self.focus();">৯</td>
																												<td
																													onclick="insertAtCursor('০');self.focus();">০</td>
																											</tr>
																										</tbody>
																									</table>
																								</div> <!--script--> <script
																									type="text/javascript"> var activeta; //active text area, input field
function insertAtCursor(myValue) {
/**
* this function inserts a character at the current cursor position in a text area
* many thanks to alex king and phpMyAdmin for this cool function * This function is originally found in phpMyAdmin package and modified by Hasin Hayder(http://hasin.wordpress.com) to meet the requirement */
var myField = document.getElementById(activeta);
if (document.selection) { myField.focus();
sel = document.selection.createRange();
sel.text = myValue;
sel.collapse(true);
sel.select();
}
//MOZILLA/NETSCAPE support
else if (myField.selectionStart || myField.selectionStart == 0) {
var startPos = myField.selectionStart;
var endPos = myField.selectionEnd;
var scrollTop = myField.scrollTop;
startPos = (startPos == -1 ? myField.value.length : startPos );
myField.value = myField.value.substring(0, startPos)
+ myValue
+ myField.value.substring(endPos, myField.value.length);
myField.focus();
myField.selectionStart = startPos + myValue.length;
myField.selectionEnd = startPos + myValue.length;
myField.scrollTop = scrollTop;
} else {
var scrollTop = myField.scrollTop;
myField.value += myValue;
myField.focus();
myField.scrollTop = scrollTop;
}
}
function makeVirtualEditor(textAreaId)
{
activeTextAreaInstance = document.getElementById(textAreaId);
activeTextAreaInstance.onfocus = function(){activeta=textAreaId;}; }
</script> <script type="text/javascript"> //get all the input fields and register them ...
var nodeList=document.getElementsByTagName('input'); for(var i=0;i<nodeList.length;i++) {
if(nodeList[i])
{
//filter only the text and password fields
if(nodeList[i].type=="text" || nodeList[i].type=="password")
{
makeVirtualEditor(nodeList[i].id); }
}
}
//get all textareas and register them
var nodeList=document.getElementsByTagName('textarea'); for(var i=0;i<nodeList.length;i++)
{
if(nodeList[i])
{
makeVirtualEditor(nodeList[i].id);
}
}
/*
Or just simple/directl register any textarea or input with it's id makeVirtualEditor(textarea id here);
example:
makeVirtualEditor('textfield');
*/ </script> <script language="javascript">
var state = 'none';
var linktext='';
var link = document.getElementById('linkid');
function showhide(layer_ref) {
if (state == 'block') {state = 'none';linktext='Show virtual keyboard';}
else {state = 'block';linktext='Hide virtual keyboard';}
if (document.all) { //IS IE 4 or 5 or later
eval( "document.all." + layer_ref + ".style.display = state");
link.innerText=linktext;
//alert("IE 4 or 5 or 6 or 7 or later"); check ur browser
}
//IS NETSCAPE 4 or below
if (document.layers) { document.layers[layer_ref].display = state;
link.innerText=linktetxt;
//alert("NETSCAPE 4 or below"); check ur browser
}
//Mozilla/Netscape6+ and all the other Gecko-based browsers
if (document.getElementById &&!document.all) {
temp = document.getElementById(layer_ref);
temp.style.display = state;
link.firstChild.nodeValue=linktext;
//alert(" by id and not all"); check ur browser
}
}
</script> <!--End Inline virtual keyboard by Manchumahara--> <br> <input
																								name="selectit" value="Select All"
																								onclick="selectAll();" type="button">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
																								value="Clear All" type="reset"></td>
																						</tr>
																					</tbody>
																				</table>
																			</form>
																		</td>
																	</tr>

																</tbody>
															</table> <!--content right col start-->
														</td>


													</tr>

													<tr>
														<td align="center" background="../Images1/sea3.gif"
															height="27" width="100%">
															<p align="center">
																<font color="#93a2cd" face="Verdana" size="1">Copyright
																	© 2015 arifrahman.site11.com</font>
															</p>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<?php include_once 'sidebar_includes/ideamaker_sidebar.php'?>

	</div>
	<!-- /.row -->

</div>
<!-- /.container -->

<?php include_once 'includes/footer.php'?>
