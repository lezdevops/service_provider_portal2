<?php
include_once 'includes/header.php';
?>
<?php
$selectedMenu = 'BanglaWriting';
include_once 'includes/menus.php';
?>
<!-- Page Content -->
<script
	src="http://arifrahman.site11.com/responsive/bangla_writing/layout.js"></script>
<script
	src="http://arifrahman.site11.com/responsive/bangla_writing/common.js"></script>
<script
	src="http://arifrahman.site11.com/responsive/bangla_writing/main.js"></script>
<div class="container">

	<div class="row">

		<!-- Blog Entries Column -->
		<div class="col-md-8">
			<h1 style="color: red;"></h1>
			<h1 style="color: green;"></h1>

			<h1 class="my-4">
				New Post <small>Form</small>
			</h1>

			<!-- Blog Post -->
			<form action="http://arifrahman.site11.com/index.php/newblog/add"
				enctype="multipart/form-data" method="post" accept-charset="utf-8">
				<div class="box-body">
					<div class="form-group">
						<!-- label for="securityCode"></label-->
						<input placeholder="Enter Security Code" class="form-control"
							name="securityCode" type="password">
					</div>
					<div class="form-group">
						<!-- label for="headline"></label-->
						<input placeholder="Enter Headline" class="form-control"
							name="headline" type="text">
					</div>

					<div class="form-group">
						<input onclick="return KeyboardLayoutOptionClick(event);"
							value="English" name="KeyboardLayoutOption" type="radio"><span
							class="style3">ইংরেজি&nbsp;&nbsp;&nbsp; </span><input
							onclick="return KeyboardLayoutOptionClick(event);" value="Bijoy"
							name="KeyboardLayoutOption" type="radio"><span class="style3">বিজয়&nbsp;&nbsp;&nbsp;</span>
						<input onclick="return KeyboardLayoutOptionClick(event);"
							checked="checked" value="Somewhere-in"
							name="KeyboardLayoutOption" type="radio"><span class="style3">ফনেটিক&nbsp;&nbsp;</span>
						<!--<input onclick="return KeyboardLayoutOptionClick(event);" value="Avro" name="KeyboardLayoutOption" type="radio"><span class="style3">(অভ্র ফনেটিক)&nbsp;</span> <input onclick="return KeyboardLayoutOptionClick(event);" value="Unijoy" name="KeyboardLayoutOption" type="radio"><span class="style3">ইউনিজয়&nbsp; </span>-->

					</div>
					<div class="form-group">
						<textarea name="details" cols="70" rows="16" class="bangla"
							rows="10" cols="100" placeholder="Enter Post Details"
							class="form-control" id="textfield"
							style="font-size: 12pt; font-family: SolaimanLipi;"
							onkeypress="return KeyBoardPress(event);"
							onkeydown="return KeyBoardDown(event);"></textarea>
					</div>
					<div class="form-group" style="vertical-align: middle">
						<a href="#" onclick="showhide('inlinevkb');" id="linkid">Show
							virtual keyboard</a><br>
						<div id="inlinevkb" style="display: none;" align="center">
							<table class="inlinekbtable" border="1" cellpadding="1">
								<tbody>
									<tr>
										<td onclick="insertAtCursor('!');self.focus();">!</td>
										<td onclick="insertAtCursor('ক');self.focus();">ক</td>
										<td onclick="insertAtCursor('খ');self.focus();">খ</td>
										<td onclick="insertAtCursor('গ');self.focus();">গ</td>
										<td onclick="insertAtCursor('ঘ');self.focus();">ঘ</td>
										<td onclick="insertAtCursor('ঙ');self.focus();">ঙ</td>
										<td onclick="insertAtCursor('চ');self.focus();">চ</td>
										<td onclick="insertAtCursor('ছ');self.focus();">ছ</td>
										<td onclick="insertAtCursor('জ');self.focus();">জ</td>
										<td onclick="insertAtCursor('ঝ');self.focus();">ঝ</td>
										<td onclick="insertAtCursor('ঞ');self.focus();">ঞ</td>
										<td onclick="insertAtCursor('ট');self.focus();">ট</td>
										<td onclick="insertAtCursor('ঠ');self.focus();">ঠ</td>
										<td onclick="insertAtCursor('ড');self.focus();">ড</td>
										<td onclick="insertAtCursor('ঢ');self.focus();">ঢ</td>
										<td onclick="insertAtCursor('ণ');self.focus();">ণ</td>
										<td
											onclick="insertAtCursor('\u200C'); insertAtCursor('র্'); self.focus();"
											align="center"><input value="রেফ" type="button">
										</td>
										<td colspan="3"><input name="BackSpace" value="←Del"
											onclick="backspace();" type="button">
										</td>
										<td onclick="insertAtCursor('১');self.focus();">১</td>
										<td onclick="insertAtCursor('২');self.focus();">২</td>
									</tr>
									<tr>
										<td onclick="insertAtCursor('\u200D');self.focus();">~</td>
										<td onclick="insertAtCursor('ত');self.focus();">ত</td>
										<td onclick="insertAtCursor('থ');self.focus();">থ</td>
										<td onclick="insertAtCursor('দ');self.focus();">দ</td>
										<td onclick="insertAtCursor('ধ');self.focus();">ধ</td>
										<td onclick="insertAtCursor('ন');self.focus();">ন</td>
										<td onclick="insertAtCursor('প');self.focus();">প</td>
										<td onclick="insertAtCursor('ফ');self.focus();">ফ</td>
										<td onclick="insertAtCursor('ব');self.focus();">ব</td>
										<td onclick="insertAtCursor('ভ');self.focus();">ভ</td>
										<td onclick="insertAtCursor('ম');self.focus();">ম</td>
										<td onclick="insertAtCursor('য');self.focus();">য</td>
										<td onclick="insertAtCursor('র');self.focus();">র</td>
										<td onclick="insertAtCursor('ল');self.focus();">ল</td>
										<td onclick="insertAtCursor('শ');self.focus();">শ</td>
										<td onclick="insertAtCursor('ষ');self.focus();">ষ</td>
										<td
											onclick="insertAtCursor('\u09CD');insertAtCursor('\u09AF');self.focus();"
											#f4c1c0="" align="center" bgcolor=""><input value="য-ফলা"
											type="button">
										</td>
										<td onclick="insertAtCursor('\u09f3');self.focus();">৳</td>
										<td onclick="insertAtCursor('-');self.focus();">-</td>
										<td onclick="insertAtCursor('+');self.focus();">+</td>
										<td onclick="insertAtCursor('৩');self.focus();">৩</td>
										<td onclick="insertAtCursor('৪');self.focus();">৪</td>
									</tr>
									<tr>
										<td onclick="insertAtCursor('\u200C');self.focus();">`</td>
										<td onclick="insertAtCursor('স');self.focus();">স</td>
										<td onclick="insertAtCursor('হ');self.focus();">হ</td>
										<td onclick="insertAtCursor('ড়');self.focus();">ড়</td>
										<td onclick="insertAtCursor('ঢ়');self.focus();">ঢ়</td>
										<td onclick="insertAtCursor('য়');self.focus();">য়</td>
										<td onclick="insertAtCursor('ৎ');self.focus();">ৎ</td>
										<td onclick="insertAtCursor('ং');self.focus();">ং</td>
										<td onclick="insertAtCursor('ঃ');self.focus();">ঃ</td>
										<td onclick="insertAtCursor('ঁ');self.focus();">ঁ</td>
										<td onclick="insertAtCursor('―');self.focus();">―</td>
										<td onclick="insertAtCursor('=');self.focus();">=</td>
										<td onclick="insertAtCursor('?');self.focus();">?</td>
										<td onclick="insertAtCursor(':');self.focus();">:</td>
										<td onclick="insertAtCursor('\u0027');self.focus();">'</td>
										<td onclick="insertAtCursor('\u0022');self.focus();">"</td>
										<td onclick="insertAtCursor('\u09CD');self.focus();"><input
											value="-যুক্ত-" type="button"></td>
										<td colspan="3" onclick="insertAtCursor('\n');self.focus();"><input
											value="Enter" type="button"></td>
										<td onclick="insertAtCursor('৫');self.focus();">৫</td>
										<td onclick="insertAtCursor('৬');self.focus();">৬</td>
									</tr>
									<tr>
										<td onclick="insertAtCursor('[');self.focus();">[</td>
										<td onclick="insertAtCursor(']');self.focus();">]</td>
										<td onclick="insertAtCursor('অ');self.focus();"
											bgcolor="#e8e38e">অ</td>
										<td onclick="insertAtCursor('আ');self.focus();"
											bgcolor="#e8e38e">আ</td>
										<td onclick="insertAtCursor('ই');self.focus();"
											bgcolor="#e8e38e">ই</td>
										<td onclick="insertAtCursor('ঈ');self.focus();"
											bgcolor="#e8e38e">ঈ</td>
										<td onclick="insertAtCursor('উ');self.focus();"
											bgcolor="#e8e38e">উ</td>
										<td onclick="insertAtCursor('ঊ');self.focus();"
											bgcolor="#e8e38e">ঊ</td>
										<td onclick="insertAtCursor('ঋ');self.focus();"
											bgcolor="#e8e38e">ঋ</td>
										<td onclick="insertAtCursor('এ');self.focus();"
											bgcolor="#e8e38e">এ</td>
										<td onclick="insertAtCursor('ঐ');self.focus();"
											bgcolor="#e8e38e">ঐ</td>
										<td onclick="insertAtCursor('ও');self.focus();"
											bgcolor="#e8e38e">ও</td>
										<td onclick="insertAtCursor('ঔ');self.focus();"
											bgcolor="#e8e38e">ঔ</td>
										<td onclick="insertAtCursor(';');self.focus();">;</td>
										<td onclick="insertAtCursor(',');self.focus();">,</td>
										<td onclick="insertAtCursor('\u0964');self.focus();">।</td>
										<td
											onclick="insertAtCursor('\u09CD');insertAtCursor('র'); self.focus();"
											align="center"><input value="র-ফলা‌‌‌" type="button">
										</td>
										<td onclick="insertAtCursor('%');self.focus();">%</td>
										<td onclick="insertAtCursor('*');self.focus();">*</td>
										<td onclick="insertAtCursor('/');self.focus();">/</td>
										<td onclick="insertAtCursor('৭');self.focus();">৭</td>
										<td onclick="insertAtCursor('৮');self.focus();">৮</td>
									</tr>
									<tr>
										<td colspan="3" onclick="insertAtCursor(' ');self.focus();"><input
											value="Space" type="button">
										</td>
										<td onclick="insertAtCursor('া');self.focus();"
											bgcolor="#e8e38e">া</td>
										<td onclick="insertAtCursor('ি');self.focus();"
											bgcolor="#e8e38e">ি</td>
										<td onclick="insertAtCursor('ী');self.focus();"
											bgcolor="#e8e38e">ী</td>
										<td onclick="insertAtCursor('ু');self.focus();"
											bgcolor="#e8e38e">ু</td>
										<td onclick="insertAtCursor('ূ');self.focus();"
											bgcolor="#e8e38e">ূ</td>
										<td onclick="insertAtCursor('ৃ');self.focus();"
											bgcolor="#e8e38e">ৃ</td>
										<td onclick="insertAtCursor('ে');self.focus();"
											bgcolor="#e8e38e">ে</td>
										<td onclick="insertAtCursor('ৈ');self.focus();"
											bgcolor="#e8e38e">ৈ</td>
										<td onclick="insertAtCursor('ো');self.focus();"
											bgcolor="#e8e38e">ো</td>
										<td onclick="insertAtCursor('ৌ');self.focus();"
											bgcolor="#e8e38e">ৌ</td>
										<td colspan="3" onclick="insertAtCursor(' ');self.focus();"><input
											value="Space" type="button">
										</td>
										<td onclick="insertAtCursor('\u09CD');self.focus();"><input
											value="হসন্ত" type="button">
										</td>
										<td onclick="insertAtCursor('(');self.focus();">(</td>
										<td onclick="insertAtCursor(')');self.focus();">)</td>
										<td onclick="insertAtCursor('.');self.focus();">.</td>
										<td onclick="insertAtCursor('৯');self.focus();">৯</td>
										<td onclick="insertAtCursor('০');self.focus();">০</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="form-group">
						<!-- label for="postedby"></label-->
						<input placeholder="Posted By" class="form-control"
							name="postedby" type="text">
					</div>
					<div class="form-group">
						<!-- label for="headline"></label-->
						<input class="form-control" placeholder="Enter Headline"
							type="file" name="userfile" size="20">

					</div>
					<div class="form-group">
						<script type="text/javascript"> var activeta; //active text area, input field
function insertAtCursor(myValue) {
/**
* this function inserts a character at the current cursor position in a text area
* many thanks to alex king and phpMyAdmin for this cool function * This function is originally found in phpMyAdmin package and modified by Hasin Hayder(http://hasin.wordpress.com) to meet the requirement */
var myField = document.getElementById(activeta);
if (document.selection) { myField.focus();
sel = document.selection.createRange();
sel.text = myValue;
sel.collapse(true);
sel.select();
}
//MOZILLA/NETSCAPE support
else if (myField.selectionStart || myField.selectionStart == 0) {
var startPos = myField.selectionStart;
var endPos = myField.selectionEnd;
var scrollTop = myField.scrollTop;
startPos = (startPos == -1 ? myField.value.length : startPos );
myField.value = myField.value.substring(0, startPos)
+ myValue
+ myField.value.substring(endPos, myField.value.length);
myField.focus();
myField.selectionStart = startPos + myValue.length;
myField.selectionEnd = startPos + myValue.length;
myField.scrollTop = scrollTop;
} else {
var scrollTop = myField.scrollTop;
myField.value += myValue;
myField.focus();
myField.scrollTop = scrollTop;
}
}
function makeVirtualEditor(textAreaId)
{
activeTextAreaInstance = document.getElementById(textAreaId);
activeTextAreaInstance.onfocus = function(){activeta=textAreaId;}; }
</script>
						<script type="text/javascript"> //get all the input fields and register them ...
var nodeList=document.getElementsByTagName('input'); for(var i=0;i<nodeList.length;i++) {
if(nodeList[i])
{
//filter only the text and password fields
if(nodeList[i].type=="text" || nodeList[i].type=="password")
{
makeVirtualEditor(nodeList[i].id); }
}
}
//get all textareas and register them
var nodeList=document.getElementsByTagName('textarea'); for(var i=0;i<nodeList.length;i++)
{
if(nodeList[i])
{
makeVirtualEditor(nodeList[i].id);
}
}
/*
Or just simple/directl register any textarea or input with it's id makeVirtualEditor(textarea id here);
example:
makeVirtualEditor('textfield');
*/ </script>
						<script language="javascript">
var state = 'none';
var linktext='';
var link = document.getElementById('linkid');
function showhide(layer_ref) {
if (state == 'block') {state = 'none';linktext='Show virtual keyboard';}
else {state = 'block';linktext='Hide virtual keyboard';}
if (document.all) { //IS IE 4 or 5 or later
eval( "document.all." + layer_ref + ".style.display = state");
link.innerText=linktext;
//alert("IE 4 or 5 or 6 or 7 or later"); check ur browser
}
//IS NETSCAPE 4 or below
if (document.layers) { document.layers[layer_ref].display = state;
link.innerText=linktetxt;
//alert("NETSCAPE 4 or below"); check ur browser
}
//Mozilla/Netscape6+ and all the other Gecko-based browsers
if (document.getElementById &&!document.all) {
temp = document.getElementById(layer_ref);
temp.style.display = state;
link.firstChild.nodeValue=linktext;
//alert(" by id and not all"); check ur browser
}
}
</script>
						<!--End Inline virtual keyboard by Manchumahara-->


					</div>


				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<input name="selectit" value="Select All" onclick="selectAll();"
						class="btn btn-primary float-left" type="button" /> <input
						value="Clear All" class="btn btn-primary float-left" type="reset" />
					<input type="submit" class="btn btn-success float-right"
						value="Publish" />
					<!-- input type="button" class="btn btn-primary pull-right"
					value="Insert" onclick="dataInsert();" /-->
				</div>
			</form>

		</div>

		<!-- Sidebar Widgets Column -->

		<?php include_once 'sidebar_includes/ideamaker_sidebar.php'?>
	</div>
	<!-- /.row -->

</div>
<?php include_once 'includes/footer.php'?>

