package app.base;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import app.base.BaseUtil;
import app.base.MapUtil;
import app.base.UtilValidate;

/**
 * Created by eclipse
 */
@Component
public class QueryBuilderImpl implements QueryBuilder {

  @PersistenceContext
  private EntityManager em;

  CriteriaBuilder cb;

  Criteria cr;

  Session session;

  CriteriaQuery crtQuery;

  @Autowired
  MapUtil utilMap;

  List<Predicate> predicates = new ArrayList<>();
  List<Selection<?>> selections = new ArrayList<>();
  List<String> fields = new ArrayList<>();
  private String aliasConcatenate = "";

  public QueryBuilderImpl() {
  }

  public void setCb() {
    this.cb = em.getCriteriaBuilder();
  }

  public void setCrtQuery() {
    this.crtQuery = this.cb.createQuery(Tuple.class);
  }

  public Criteria getCriteria(Class modelClass) {
    return em.unwrap(Session.class).createCriteria(modelClass);
  }

  public CriteriaQuery getCriteriaQuery() {
    return cb.createTupleQuery();
  }

  PageRequest pageRequest;

  public Query buildQuery(Node searchNode, Class modelClass, String modelName)  {
    this.setCb();
    this.setCrtQuery();
    Root root = crtQuery.from(modelClass);
    List<Map<String, Object>> result = new ArrayList();
    Query query = null;
    this.prepareSelectionList(modelName, root);
    try {
      crtQuery.multiselect(selections);
      if (!UtilValidate.isEmpty(searchNode)) {
        this.preparePredicateList(searchNode, root, null, null);
        crtQuery.where(cb.and(predicates.toArray(new Predicate[0])));
      }

      query = em.createQuery(crtQuery);
     // int fromResult = (paginationParam.getPageNum() - 1) * paginationParam.getMaxResult();
     // query.setFirstResult(fromResult);
     // query.setMaxResults(paginationParam.getMaxResult());
    ///  List<Tuple> tuples = query.getResultList();
  //    paginationParam.setResultSize(tuples.size());
//      if (!tuples.isEmpty()) {
//        result = prepareResultMap(tuples);
//      }

    } catch (Exception exp) {
      exp.printStackTrace();
    }

    //this.clearResource();

    return query;
  }


  public void clearResource() {
    this.selections.clear();
    this.predicates.clear();
    this.fields.clear();
    em.clear();
    em.close();
  }


  public void preparePredicateList(Node searchNode, Root root, Path path, Join uJoin) throws ClassNotFoundException, NoSuchFieldException {

    if (path == null) {
      path = root;
    }

    int flag = 0;

    List<SearchProperty> searchProperties = searchNode.getSearchProperties();
    for (SearchProperty searchProperty : searchProperties) {

      // String className = PackagePath.MODEL_PACKAGE + "." + searchNode.getNodeName();
      Field field = null;
      Class clazz2 = BaseUtil.getModelClass(searchNode.getNodeName());
      if (searchProperty.getFieldName().equals("id")) {
        field = BaseUtil.getField(clazz2, searchProperty.getFieldName());
      } else {
        field = BaseUtil.getField(clazz2, searchProperty.getFieldName());
      }
      String fieldType = field.getType().getSimpleName();
      String fieldValStr = String.valueOf(searchProperty.getValue());
      Object fieldVal = BaseUtil.castFieldOrRaiseTypeCastError(fieldType, fieldValStr);
      field.setAccessible(true);
      Class<? extends Comparable> clazzz = (Class<? extends Comparable>) searchProperty.getValue().getClass();
      if (searchProperty.getOpName().equals("eq")) {
        predicates.add(this.cb.equal(path.get(searchProperty.getFieldName()), fieldVal));
      } else if (searchProperty.getOpName().equals("notEq")) {
        predicates.add(this.cb.notEqual(path.get(searchProperty.getFieldName()), fieldVal));
      } else if (searchProperty.getOpName().equals("gt")) {
        predicates.add(this.cb.greaterThanOrEqualTo(path.get(searchProperty.getFieldName()), clazzz.cast(searchProperty.getValue())));
      } else if (searchProperty.getOpName().equals("lt")) {
        predicates.add(this.cb.lessThanOrEqualTo(path.get(searchProperty.getFieldName()), clazzz.cast(searchProperty.getValue())));
      } else if (searchProperty.getOpName().equals("%st")) {
        predicates.add(this.cb.like(path.get(searchProperty.getFieldName()), fieldValStr + "%"));
      } else if (searchProperty.getOpName().equals("%en")) {
        predicates.add(this.cb.like(path.get(searchProperty.getFieldName()), "%" + fieldValStr));
      } else if (searchProperty.getOpName().equals("%cn")) {
        predicates.add(this.cb.like(path.get(searchProperty.getFieldName()), "%" + fieldValStr + "%"));
      }


      if (!this.selectAlreadyExist(searchProperty.getAliasName())) {
        String aliasName = searchProperty.getAliasName();
        selections.add(path.get(searchProperty.getFieldName()).alias(aliasName));
      }

    }


    List<Node> childs = searchNode.getChildNodes();

    if (flag == 0) {
      for (Node child : childs) {
        Join cJoin = root.join(child.getNodeName(), JoinType.LEFT);
        child.setJoin(cJoin);
      }
    }

    flag++;

    for (Node child : childs) {
      Join sJoin = null;
      if (child.getJoin() == null) {
        sJoin = uJoin.join(child.getNodeName(), JoinType.LEFT);
        child.setJoin(sJoin);
      } else {
        sJoin = child.getJoin();
      }
      preparePredicateList(child, root, sJoin, sJoin);
    }


  }

  public List<String> getFields() {
    return this.fields;
  }



  public void prepareSelectionList(String fileName, Root root) {
    Path path = null;
    List<Map> schema = utilMap.getSchemaMap(fileName);
    schema = utilMap.removeEdit(schema);
    for (Map schemaMap : schema) {
      String fieldName = (String) schemaMap.get("data");
      fields.add(fieldName);
      if (fieldName.contains(".")) {
        String fieldArr[] = fieldName.split("\\.");
        path = prepareNestedPath(fieldArr, root, null, 0);
      } else {
        path = root.get(fieldName);
      }

      if (!this.selectAlreadyExist(fieldName)) {
        selections.add(path.alias(fieldName));
      }

    }

  }

  public boolean selectAlreadyExist(String fieldName) {

    for (Selection selection : this.selections) {
      if (selection.getAlias().equals(fieldName))
        return true;

    }

    return false;
  }

  public Path prepareNestedPath(String fieldArr[], Root root, Path path, int length) {
    Path innerPath = null;
    if (length == fieldArr.length) {
      return path;
    } else if (length == 0) {
      innerPath = root.get(fieldArr[length]);
      path = prepareNestedPath(fieldArr, root, innerPath, ++length);
    } else {
      innerPath = path.get(fieldArr[length]);
      path = prepareNestedPath(fieldArr, root, innerPath, ++length);
    }

    return path;

  }



}
