package app.base;

/**
 * Created by eclipse
 */
public class SearchProperty {

  private String fieldName;
  private String opName;
  private Object value;
  private String aliasName;

  public SearchProperty(String fieldName, String opName, Object value, String aliasName) {
    this.fieldName = fieldName;
    this.opName = opName;
    this.value = value;
    this.aliasName = aliasName;
  }

  public SearchProperty(){

  }

  public String getFieldName() {
    return fieldName;
  }

  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }

  public String getOpName() {
    return opName;
  }

  public void setOpName(String opName) {
    this.opName = opName;
  }

  public Object getValue() {
    return value;
  }

  public void setValue(Object value) {
    this.value = value;
  }

  public String getAliasName() {
    return aliasName;
  }

  public void setAliasName(String aliasName) {
    this.aliasName = aliasName;
  }
}
