package app.base;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.base.BaseUtil;
import app.base.CustomValidate;
import app.base.ErrorObj;
import app.base.MapUtil;
import app.base.PaginationParam;
import app.base.UtilValidate;
import app.security.config.PackagePath;

/**
 * Created by eclipse
 */
public class BaseService {

  @Autowired
  CustomValidate customValidate;

  @Autowired
  MapUtil utilMap;

  CrudRepository repository;
  private String modelName;


  private boolean modelContainError = false;

  Session session;
  @PersistenceContext
  EntityManager em;

  @Autowired
  QueryBuilder qb;

  PaginationParam paginationParam = new PaginationParam(4);

  private Query query;

  BaseService() {

  }

  protected BaseService(CrudRepository repository, String modelName) {
    this.repository = repository;
    this.modelName = modelName;
  }

  @Transactional
  public void saveBean(List<Object> objects) {
    //repository.saveAll(objects);

  }


  public Query getQuery() {
    return query;
  }

  public void setQuery(Node searchNode) throws ClassNotFoundException, JsonProcessingException {
    this.query = this.getQb().buildQuery(searchNode, BaseUtil.getModelClass(this.getModelName()), this.getModelName());
  }

  public QueryBuilder getQb() {
    return this.qb;
  }

  public void setQb(QueryBuilder qb) {
    this.qb = qb;
  }

  public Map list(Node searchNode) throws ClassNotFoundException, JsonProcessingException {
    this.setQuery(searchNode);
    Query query = this.getQuery();
    double count = this.totalNumOfRow();
    if (paginationParam.getPageNum() > 0) {
      this.setPaginationParam(query, this.paginationParam);
    }

    List data = this.getResult();
    this.appendEditAndDelete(data);
    List<Map> schema = utilMap.getSchemaMap(this.getModelName());
    double numOfPages = Math.ceil(count / (double) paginationParam.getMaxResult());
    if (numOfPages < 1) {
      numOfPages = 1;
    }
    this.paginationParam.setNumOfPage((int) numOfPages);
    this.paginationParam.setResultSize(data.size());
    //  paginationParam.setResultSize(data.size());
    this.clearResource();
    return this.initialResultMapAndReturn(data, schema);
  }


  public Map initialResultMapAndReturn(List data, List schema) {
    Map resultMap = new HashMap();
    resultMap.put("pagination", this.getPaginationParam());
    resultMap.put("data", data);
    resultMap.put("schema", schema);
    return resultMap;
  }

  public void clearResource() {
    this.qb.clearResource();
  }

  public void setPaginationParam(Query query, PaginationParam paginationParam) {
    int fromResult = (paginationParam.getPageNum() - 1) * paginationParam.getMaxResult();
    query.setFirstResult(fromResult);
    query.setMaxResults(paginationParam.getMaxResult());
  }

  public List getResult() {
    List<Tuple> tuples = this.query.getResultList();
    List<Map<String, Object>> result = new ArrayList();
    return !tuples.isEmpty() ? prepareResultMap(tuples) : result;
  }


  public List<Map<String, Object>> prepareResultMap(List<Tuple> resultList) {

    List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
    Map<String, Object> rowMap = new HashMap<String, Object>();
    for (Tuple tuple : resultList) {
      for (String field : this.qb.getFields()) {
        Object tupleVal = tuple.get(field);
        if (field.contains(".")) {
          String fieldArr[] = field.split("\\.");
          Map nestedMap = new HashMap();
          if (rowMap.containsKey(fieldArr[0])) {
            nestedMap = (Map) rowMap.get(fieldArr[0]);
          }
          rowMap.put(fieldArr[0], nestedMap);
          prepareNestedMap(fieldArr, tupleVal, nestedMap, 1);
        } else {
          rowMap.put(field, tupleVal);
        }//employee.shop.id
      }
      results.add(rowMap);
      rowMap = new HashMap<String, Object>();

    }

    return results;
  }

  public void prepareNestedMap(String fieldArr[], Object fieldVal, Map nestedMap, int length) {

    while (length < fieldArr.length - 1) {
      nestedMap.put(fieldArr[length], new HashMap());
      nestedMap = (Map) nestedMap.get(fieldArr[length]);
    }
    nestedMap.put(fieldArr[fieldArr.length - 1], fieldVal);

  }


  public long totalNumOfRow() {
    return this.query.getResultList().size();
  }

  public List appendEditAndDelete(List<Map> result) {

    for (Map map : result) {
      Integer id = (Integer) map.get("id");
      map.put("edit", id);
      map.put("delete", id);
    }
    return result;
  }


  public Class getModelClass() throws ClassNotFoundException {
    return BaseUtil.getModelClass(this.modelName);
  }

  public List createAndvalidateBean(List<Map> reqMapList) throws ClassNotFoundException {
    BeanWrapper beanWrapper = null;
    ObjectMapper mapper = new ObjectMapper();
    List<Object> listOfModel = new ArrayList<>();
    Class modelclass = this.getModelClass();
    try {
      for (Map modelMap : reqMapList) {
        //  Map modelMap = (Map) reqMap.get(this.modelName);
        beanWrapper = preparemodel(modelclass, modelMap);
        Object bean = beanWrapper.getWrappedInstance();
        List<ErrorObj> errors = customValidate.customValidate(this.getModelName(), bean, this.getCustomValidator());
        if (errors.isEmpty()) {
          listOfModel.add(bean);
        } else {
          modelMap.put("errors", errors);
          setModelContainError(true);

        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    if (this.isModelContainError()) {
      return reqMapList;
    }

    return listOfModel;
  }


  public BeanWrapper preparemodel(Class modelclass, Map modelMap) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException {
    Iterator it = modelMap.entrySet().iterator();
    BeanWrapper bean = new BeanWrapperImpl(modelclass.newInstance());
    List<Field> fields = new ArrayList<>(Arrays.asList(modelclass.getDeclaredFields()));
    try {
      Field idField = BaseUtil.getField(modelclass, "id");
      idField.setAccessible(true);
      fields.add(idField);
    } catch (NoSuchFieldException exp) {
      exp.printStackTrace();
    }
    for (Field field : fields) {
      String fieldName = field.getName();
      if (!UtilValidate.isEmpty(modelMap.get(field.getName()))) {
        String type = modelMap.get(fieldName).getClass().getSimpleName();
        if (type.equals("LinkedHashMap")) {
          BeanWrapper innerBean = null;
          String fullClassName = PackagePath.MODEL_PACKAGE + "." + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
          Class mClass = Class.forName(fullClassName);
          innerBean = preparemodel(mClass, (Map) modelMap.get(field.getName()));
          bean.setPropertyValue(fieldName, innerBean.getWrappedInstance());
        } else if (type.equals("ArrayList")) {
          List<Map> innerList = (List) modelMap.get(field.getName());
          List<Object> innerObject = new ArrayList<>();
          for (Map innerMap : innerList) {
            BeanWrapper innerBean = null;
            String fullClassName = PackagePath.MODEL_PACKAGE + "." + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length() - 1);
            Class mClass = Class.forName(fullClassName);
            innerBean = preparemodel(mClass, innerMap);
            innerObject.add(innerBean.getWrappedInstance());
          }
          bean.setPropertyValue(fieldName, innerObject);
        } else {
          bean.setPropertyValue(field.getName(), modelMap.get(field.getName()));
        }
      }

    }
    return bean;
  }

  public List typeValidatedList(String requestData) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    List<Map> reqMapList = new ArrayList<>();
    try {
      reqMapList = mapper.readValue(requestData, List.class);
      for (Map reqMap : reqMapList) {
        Map modelMap = (Map) reqMap.get(this.modelName);
        validateTypeCast(modelMap);
      }
    } catch (Exception ex) {

    }
    return reqMapList;
  }

  public void validateTypeCast(Map modelMap) throws ClassNotFoundException, NoSuchFieldException {
    Object obj = null;
    List errors = new ArrayList();
    Class modelClass = null;
    try {
      modelClass = BaseUtil.getModelClass(this.modelName);

      Iterator modelIt = modelMap.entrySet().iterator();
      while (modelIt.hasNext()) {
        Map.Entry entry = (Map.Entry) modelIt.next();
        String fieldName = (String) entry.getKey();
        String fieldVal = (String) entry.getValue();
        if (fieldName.equals("id") && fieldVal.equals("")) {
          continue;
        }
        Field f = modelClass.getDeclaredField(fieldName);
        //field cast validation
        obj = BaseUtil.castFieldOrRaiseTypeCastError(f.getType().getSimpleName(), fieldVal);
        if (obj instanceof ErrorObj) {
          errors.add(obj);
        } else {
          modelMap.put(fieldName, obj);
        }
      }
      if (errors.size() > 0) {
        modelMap.put("errors", errors);
        this.setModelContainError(true);
      }
    } catch (ClassNotFoundException exp) {
      throw exp;
    } catch (NoSuchFieldException exp) {
      throw exp;
    }

  }


  public List get() {
    return (List) repository.findAll();

  }


  public String getModelName() {
    return this.modelName;
  }

  public String getCustomValidator() {
    return PackagePath.VALIDATOR_PACKAGE + "." + this.modelName;
  }

  public boolean isModelContainError() {
    return modelContainError;
  }

  public void setModelContainError(boolean modelContainError) {
    this.modelContainError = modelContainError;
  }

  public PaginationParam getPaginationParam() {
    return paginationParam;
  }

  public void setPaginationParam(PaginationParam paginationParam) {
    this.paginationParam = paginationParam;
  }

  public void delete(Integer id) {
    //Object obj = this.repository.findById(id);
    //this.repository.delete(obj);

  }

  public Object getById(Integer id) {
    //return this.repository.findById(id);
	  return null;
  }

}
