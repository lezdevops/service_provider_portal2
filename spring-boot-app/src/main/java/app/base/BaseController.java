package app.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by eclipse
 */
public class BaseController {

  BaseService service;

  @Autowired
  MapUtil utilMap;

  @Autowired
  QueryBuilder qb;

  protected BaseController(BaseService service) {
    this.service = service;
  }

  public Object save(String payload) throws Exception {
    List<Map> reqMap = service.typeValidatedList(payload);  //type validation
    if (service.isModelContainError()) {
      return reqMap;
    }
    List listOfBean = service.createAndvalidateBean(reqMap);

    if (service.isModelContainError()) {
      return listOfBean;
    } else {
      try {
        service.saveBean(listOfBean);
      } catch (Exception exp) {
        exp.printStackTrace();
        return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
      }
      return new ResponseEntity<>(listOfBean, HttpStatus.CREATED);
    }
  }


  public Object list(String params, int pageNum) throws Exception {

    Node searchNode = null;
    Map queryMap = new HashMap();
    if (!params.isEmpty()) {
      queryMap = utilMap.convertStringToMap(params);
    }
    //if (!queryMap.isEmpty()) {
    searchNode = utilMap.prepareSearchMap(queryMap, service.getModelName());
    // }else{

    //}
    service.getPaginationParam().setPageNum(pageNum);
    Map result = service.list(searchNode);
    if (result.isEmpty()) {
      return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
    return result;
  }


  public Object fetchWithoutPagination(String params, int pageNum) throws Exception {

    Node searchNode = null;
    Map queryMap = new HashMap();
    if (!params.isEmpty()) {
      queryMap = utilMap.convertStringToMap(params);
    }
    //if (!queryMap.isEmpty()) {
    searchNode = utilMap.prepareSearchMap(queryMap, service.getModelName());
    // }else{

    //}
    service.getPaginationParam().setPageNum(pageNum);
    Map result = service.list(searchNode);
    if (result.isEmpty()) {
      return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
    return result;
  }

  public Object delete(int id) {
    this.service.delete(id);
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }




}
