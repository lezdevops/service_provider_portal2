package app.base;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import app.base.ErrorObj;
import app.base.MapUtil;


@Component
public class CustomValidate {

    @Autowired
    MapUtil mapUtil;

    public List<ErrorObj> customValidate(String modelName, Object obj,String validatorPackage) {

        List<String> validatorMapList = mapUtil.getValidatorList(modelName);
        List<ErrorObj> errors = new ArrayList();

        for(String validator : validatorMapList ){

            String fullClassName = validatorPackage + "." + validator;
            try {
                Class modelclass = Class.forName(fullClassName);
                Object object = modelclass.newInstance();
                Method method = object.getClass().getMethod("validate",obj.getClass());
                ErrorObj errorObj= (ErrorObj) method.invoke(obj);
                errors.add(errorObj);

            }catch(Exception exp){

            }

        }

        return errors;
    }
}
