package app.base;

/**
 * Created by eclipse
 */
public class PaginationParam {

    private int pageNum;
    private int maxResult;
    private int resultSize;
    private int numOfPage;

    PaginationParam(){

    }

    public PaginationParam(int maxResult){
        this.maxResult = maxResult;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getMaxResult() {
        return maxResult;
    }

    public void setMaxResult(int maxResult) {
        this.maxResult = maxResult;
    }

    public int getResultSize() {
        return resultSize;
    }

    public void setResultSize(int resultSize) {
        this.resultSize = resultSize;
    }

    public int getNumOfPage() {
        return numOfPage;
    }

    public void setNumOfPage(int numOfPage) {
        this.numOfPage = numOfPage;
    }


}
