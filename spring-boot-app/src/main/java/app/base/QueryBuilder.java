package app.base;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Created by eclipse
 */
public interface QueryBuilder {

  public void prepareSelectionList(String fileName, Root root);

  public void preparePredicateList(Node searchNode, Root root, Path path, Join join) throws ClassNotFoundException, NoSuchFieldException;

  public Query buildQuery(Node searchNode, Class modelClass, String modelName) throws JsonProcessingException;

  public List<String> getFields();

  public void clearResource() ;
}
