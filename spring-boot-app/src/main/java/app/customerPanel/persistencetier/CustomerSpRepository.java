package app.customerPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.CustomerSp;

@Repository
public interface CustomerSpRepository extends JpaRepository<CustomerSp, Long>{
	List<CustomerSp> findById(Long id);
	CustomerSp findCustomerSpById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT customerSp FROM CustomerSp customerSp WHERE  cast(customerSp.customer.name AS string) like %:searchToken% OR cast(customerSp.serviceProvider.name AS string) like %:searchToken% OR cast(customerSp.isActive AS string) like %:searchToken% OR cast(customerSp.aproveStatus AS string) like %:searchToken%")
	Page<CustomerSp> findPaginatedCustomerSpsBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
