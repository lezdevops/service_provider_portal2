package app.customerPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.CustomerJob;

@Repository
public interface CustomerJobRepository extends JpaRepository<CustomerJob, Long>{
	List<CustomerJob> findById(Long id);
	CustomerJob findCustomerJobById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT customerJob FROM CustomerJob customerJob WHERE  cast(customerJob.customer.name AS string) like %:searchToken% OR cast(customerJob.jobDetails AS string) like %:searchToken% OR cast(customerJob.createdDate AS string) like %:searchToken% OR cast(customerJob.fromTime AS string) like %:searchToken% OR cast(customerJob.toTime AS string) like %:searchToken% OR cast(customerJob.approvalStatus AS string) like %:searchToken%")
	Page<CustomerJob> findPaginatedCustomerJobsBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
