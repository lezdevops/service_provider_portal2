package app.customerPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.CustomerRequest;

@Repository
public interface CustomerRequestRepository extends JpaRepository<CustomerRequest, Long>{
	List<CustomerRequest> findById(Long id);
	CustomerRequest findCustomerRequestById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT customerRequest FROM CustomerRequest customerRequest WHERE  cast(customerRequest.customer.name AS string) like %:searchToken% OR cast(customerRequest.serviceProvider.name AS string) like %:searchToken% OR cast(customerRequest.fromTime AS string) like %:searchToken% OR cast(customerRequest.toTime AS string) like %:searchToken% OR cast(customerRequest.approvalStatus AS string) like %:searchToken%")
	Page<CustomerRequest> findPaginatedCustomerRequestsBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
