package app.customerPanel.businesstier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.CustomerSp;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.customerPanel.persistencetier.CustomerSpRepository;


//newDependentBeanImportGoesHere

@Service
public class CustomerSpManager {
	private static final org.apache.log4j.Logger CustomerSpManagerLogger = org.apache.log4j.Logger.getLogger(CustomerSpManager.class);
	@Autowired
	private CustomerSpRepository customerSpRepository;
	@Autowired
	private CustomerSpValidator customerSpValidator;
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getCustomerSpForEdit(HttpServletRequest request, Long id){
		CustomerSp newCustomerSp = new CustomerSp();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERSP_EDIT");
		
		returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		CustomerSp customerSp = null;
		try {
			customerSp = customerSpRepository.getOne(id);
			if (UtilValidate.isNotEmpty(customerSp)) {
				newCustomerSp.setId(customerSp.getId());newCustomerSp.setCustomer(customerSp.getCustomer());newCustomerSp.setServiceProvider(customerSp.getServiceProvider());newCustomerSp.setIsActive(customerSp.getIsActive());newCustomerSp.setAproveStatus(customerSp.getAproveStatus());
				
			}
			returnResult.put("customerSp", newCustomerSp);
		} catch (Exception e) {
			CustomerSpManagerLogger.info("Exception Occured During Fetching CustomerSp Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public Map<String,Object> getCustomerSp(HttpServletRequest request, Long id){
		CustomerSp newCustomerSp = new CustomerSp();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERSP_READ");
		
		returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			CustomerSp customerSp = customerSpRepository.getOne(id);
			if (UtilValidate.isNotEmpty(customerSp)) {
				newCustomerSp.setId(customerSp.getId());newCustomerSp.setCustomer(customerSp.getCustomer());newCustomerSp.setServiceProvider(customerSp.getServiceProvider());newCustomerSp.setIsActive(customerSp.getIsActive());newCustomerSp.setAproveStatus(customerSp.getAproveStatus());
				
			}
			
			returnResult.put("customerSp", newCustomerSp);
		} catch (Exception e) {
			CustomerSpManagerLogger.info("Exception Occured During Fetching CustomerSp Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getCustomerSps(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERSP_READ");
		
		returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<CustomerSp> customerSpsList = new ArrayList<CustomerSp>();
			Iterable<CustomerSp> customerSps = customerSpRepository.findAll();
			for (CustomerSp customerSp : customerSps) {
				customerSpsList.add(customerSp);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("customerSps", customerSpsList);
		} catch (Exception e) {
			CustomerSpManagerLogger.info("Exception Occured During Fetching CustomerSp List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	
	public Map<String,Object> create(HttpServletRequest request, CustomerSp customerSp) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("CUSTOMERSP_CREATE");
			
			returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = customerSpValidator.getValidated(request,customerSp);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/customerSp");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				customerSp = customerSpRepository.save(customerSp);
				returnResult.put("successMessage", "Successfully Saved CustomerSp Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			CustomerSpManagerLogger.info("Exception Occured During Creating CustomerSp Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> update(HttpServletRequest request, CustomerSp customerSp) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("CUSTOMERSP_EDIT");
			
			returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = customerSpValidator.getValidated(request,customerSp);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/customerSp");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				customerSp = customerSpRepository.save(customerSp);
				returnResult.put("successMessage", "Successfully Saved CustomerSp Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(CustomerSpManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			CustomerSpManagerLogger.info("Exception Occured During Updating CustomerSp Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	
	public Map<String,Object> saveMultipleCustomerSpData(HttpServletRequest request, List<CustomerSp> customerSps) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERSP_CREATE");
		
		returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (CustomerSp customerSp : customerSps) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = customerSpValidator.getValidated(request, customerSp);
				customerSp = customerSpRepository.save(customerSp);
				returnResult.put(customerSp+" "+customerSp.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			CustomerSpManagerLogger.info("Exception Occured During Adding CustomerSp List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully CustomerSp Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERSP_DELETE");
		
		returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			CustomerSp customerSp = customerSpRepository.findOne(id);
			if (UtilValidate.isNotEmpty(customerSp)) {
				customerSpRepository.delete(id);
				
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			CustomerSpManagerLogger.info("Exception Occured During Removing CustomerSp Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERSP_DELETE");
		
		returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All CustomerSp Deleted.";
		try {
			customerSpRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerSpManagerLogger.info("Exception Occured During Removing CustomerSp Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERSP_READ");
		
		returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<CustomerSp> paginatedResult = customerSpRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerSpManagerLogger.info("Exception Occured During Fetching Paginated CustomerSp Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Map<String,Object> findPaginatedCustomerSpsBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERSP_READ");
		
		returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<CustomerSp> paginatedResultBySearchToken = customerSpRepository.findPaginatedCustomerSpsBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerSpManagerLogger.info("Exception Occured During Fetching Paginated CustomerSp Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERSP_READ");
		
		returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			CustomerSp customerSp = customerSpRepository.findOne(id);
			returnResult.put("customerSp", customerSp);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerSpManagerLogger.info("Exception Occured During Fetching CustomerSp Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<CustomerSp> customerSps = new ArrayList<CustomerSp>();
		allowedPermissions.add("CUSTOMERSP_READ");
		
		returnResult = customerSpValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			customerSps = customerSpRepository.findById(id);
			Page<CustomerSp> pages = new PageImpl<CustomerSp>(customerSps, pageRequest, customerSps.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerSpManagerLogger.info("Exception Occured During Fetching CustomerSp Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
