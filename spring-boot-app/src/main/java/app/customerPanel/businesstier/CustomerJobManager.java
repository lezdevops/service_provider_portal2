package app.customerPanel.businesstier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.CustomerJob;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.customerPanel.persistencetier.CustomerJobRepository;


//newDependentBeanImportGoesHere

@Service
public class CustomerJobManager {
	private static final org.apache.log4j.Logger CustomerJobManagerLogger = org.apache.log4j.Logger.getLogger(CustomerJobManager.class);
	@Autowired
	private CustomerJobRepository customerJobRepository;
	@Autowired
	private CustomerJobValidator customerJobValidator;
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getCustomerJobForEdit(HttpServletRequest request, Long id){
		CustomerJob newCustomerJob = new CustomerJob();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERJOB_EDIT");
		
		returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		CustomerJob customerJob = null;
		try {
			customerJob = customerJobRepository.getOne(id);
			if (UtilValidate.isNotEmpty(customerJob)) {
				newCustomerJob.setId(customerJob.getId());newCustomerJob.setCustomer(customerJob.getCustomer());newCustomerJob.setJobDetails(customerJob.getJobDetails());newCustomerJob.setCreatedDate(customerJob.getCreatedDate());newCustomerJob.setFromTime(customerJob.getFromTime());newCustomerJob.setToTime(customerJob.getToTime());newCustomerJob.setApprovalStatus(customerJob.getApprovalStatus());
				
			}
			returnResult.put("customerJob", newCustomerJob);
		} catch (Exception e) {
			CustomerJobManagerLogger.info("Exception Occured During Fetching CustomerJob Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public Map<String,Object> getCustomerJob(HttpServletRequest request, Long id){
		CustomerJob newCustomerJob = new CustomerJob();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERJOB_READ");
		
		returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			CustomerJob customerJob = customerJobRepository.getOne(id);
			if (UtilValidate.isNotEmpty(customerJob)) {
				newCustomerJob.setId(customerJob.getId());newCustomerJob.setCustomer(customerJob.getCustomer());newCustomerJob.setJobDetails(customerJob.getJobDetails());newCustomerJob.setCreatedDate(customerJob.getCreatedDate());newCustomerJob.setFromTime(customerJob.getFromTime());newCustomerJob.setToTime(customerJob.getToTime());newCustomerJob.setApprovalStatus(customerJob.getApprovalStatus());
				
			}
			
			returnResult.put("customerJob", newCustomerJob);
		} catch (Exception e) {
			CustomerJobManagerLogger.info("Exception Occured During Fetching CustomerJob Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getCustomerJobs(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERJOB_READ");
		
		returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<CustomerJob> customerJobsList = new ArrayList<CustomerJob>();
			Iterable<CustomerJob> customerJobs = customerJobRepository.findAll();
			for (CustomerJob customerJob : customerJobs) {
				customerJobsList.add(customerJob);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("customerJobs", customerJobsList);
		} catch (Exception e) {
			CustomerJobManagerLogger.info("Exception Occured During Fetching CustomerJob List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	
	public Map<String,Object> create(HttpServletRequest request, CustomerJob customerJob) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("CUSTOMERJOB_CREATE");
			
			returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = customerJobValidator.getValidated(request,customerJob);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/customerJob");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				customerJob = customerJobRepository.save(customerJob);
				returnResult.put("successMessage", "Successfully Saved CustomerJob Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			CustomerJobManagerLogger.info("Exception Occured During Creating CustomerJob Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> update(HttpServletRequest request, CustomerJob customerJob) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("CUSTOMERJOB_EDIT");
			
			returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = customerJobValidator.getValidated(request,customerJob);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/customerJob");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				customerJob = customerJobRepository.save(customerJob);
				returnResult.put("successMessage", "Successfully Saved CustomerJob Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(CustomerJobManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			CustomerJobManagerLogger.info("Exception Occured During Updating CustomerJob Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	
	public Map<String,Object> saveMultipleCustomerJobData(HttpServletRequest request, List<CustomerJob> customerJobs) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERJOB_CREATE");
		
		returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (CustomerJob customerJob : customerJobs) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = customerJobValidator.getValidated(request, customerJob);
				customerJob = customerJobRepository.save(customerJob);
				returnResult.put(customerJob+" "+customerJob.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			CustomerJobManagerLogger.info("Exception Occured During Adding CustomerJob List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully CustomerJob Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERJOB_DELETE");
		
		returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			CustomerJob customerJob = customerJobRepository.findOne(id);
			if (UtilValidate.isNotEmpty(customerJob)) {
				customerJobRepository.delete(id);
				
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			CustomerJobManagerLogger.info("Exception Occured During Removing CustomerJob Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERJOB_DELETE");
		
		returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All CustomerJob Deleted.";
		try {
			customerJobRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerJobManagerLogger.info("Exception Occured During Removing CustomerJob Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERJOB_READ");
		
		returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<CustomerJob> paginatedResult = customerJobRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerJobManagerLogger.info("Exception Occured During Fetching Paginated CustomerJob Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Map<String,Object> findPaginatedCustomerJobsBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERJOB_READ");
		
		returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<CustomerJob> paginatedResultBySearchToken = customerJobRepository.findPaginatedCustomerJobsBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerJobManagerLogger.info("Exception Occured During Fetching Paginated CustomerJob Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERJOB_READ");
		
		returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			CustomerJob customerJob = customerJobRepository.findOne(id);
			returnResult.put("customerJob", customerJob);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerJobManagerLogger.info("Exception Occured During Fetching CustomerJob Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<CustomerJob> customerJobs = new ArrayList<CustomerJob>();
		allowedPermissions.add("CUSTOMERJOB_READ");
		
		returnResult = customerJobValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			customerJobs = customerJobRepository.findById(id);
			Page<CustomerJob> pages = new PageImpl<CustomerJob>(customerJobs, pageRequest, customerJobs.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerJobManagerLogger.info("Exception Occured During Fetching CustomerJob Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
