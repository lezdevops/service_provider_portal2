package app.customerPanel.businesstier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.CustomerRequest;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.customerPanel.persistencetier.CustomerRequestRepository;


//newDependentBeanImportGoesHere

@Service
public class CustomerRequestManager {
	private static final org.apache.log4j.Logger CustomerRequestManagerLogger = org.apache.log4j.Logger.getLogger(CustomerRequestManager.class);
	@Autowired
	private CustomerRequestRepository customerRequestRepository;
	@Autowired
	private CustomerRequestValidator customerRequestValidator;
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getCustomerRequestForEdit(HttpServletRequest request, Long id){
		CustomerRequest newCustomerRequest = new CustomerRequest();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERREQUEST_EDIT");
		
		returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		CustomerRequest customerRequest = null;
		try {
			customerRequest = customerRequestRepository.getOne(id);
			if (UtilValidate.isNotEmpty(customerRequest)) {
				newCustomerRequest.setId(customerRequest.getId());newCustomerRequest.setCustomer(customerRequest.getCustomer());newCustomerRequest.setServiceProvider(customerRequest.getServiceProvider());newCustomerRequest.setFromTime(customerRequest.getFromTime());newCustomerRequest.setToTime(customerRequest.getToTime());newCustomerRequest.setApprovalStatus(customerRequest.getApprovalStatus());
				
			}
			returnResult.put("customerRequest", newCustomerRequest);
		} catch (Exception e) {
			CustomerRequestManagerLogger.info("Exception Occured During Fetching CustomerRequest Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public Map<String,Object> getCustomerRequest(HttpServletRequest request, Long id){
		CustomerRequest newCustomerRequest = new CustomerRequest();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERREQUEST_READ");
		
		returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			CustomerRequest customerRequest = customerRequestRepository.getOne(id);
			if (UtilValidate.isNotEmpty(customerRequest)) {
				newCustomerRequest.setId(customerRequest.getId());newCustomerRequest.setCustomer(customerRequest.getCustomer());newCustomerRequest.setServiceProvider(customerRequest.getServiceProvider());newCustomerRequest.setFromTime(customerRequest.getFromTime());newCustomerRequest.setToTime(customerRequest.getToTime());newCustomerRequest.setApprovalStatus(customerRequest.getApprovalStatus());
				
			}
			
			returnResult.put("customerRequest", newCustomerRequest);
		} catch (Exception e) {
			CustomerRequestManagerLogger.info("Exception Occured During Fetching CustomerRequest Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getCustomerRequests(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERREQUEST_READ");
		
		returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<CustomerRequest> customerRequestsList = new ArrayList<CustomerRequest>();
			Iterable<CustomerRequest> customerRequests = customerRequestRepository.findAll();
			for (CustomerRequest customerRequest : customerRequests) {
				customerRequestsList.add(customerRequest);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("customerRequests", customerRequestsList);
		} catch (Exception e) {
			CustomerRequestManagerLogger.info("Exception Occured During Fetching CustomerRequest List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	
	public Map<String,Object> create(HttpServletRequest request, CustomerRequest customerRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("CUSTOMERREQUEST_CREATE");
			
			returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = customerRequestValidator.getValidated(request,customerRequest);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/customerRequest");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				customerRequest = customerRequestRepository.save(customerRequest);
				returnResult.put("successMessage", "Successfully Saved CustomerRequest Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			CustomerRequestManagerLogger.info("Exception Occured During Creating CustomerRequest Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> update(HttpServletRequest request, CustomerRequest customerRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("CUSTOMERREQUEST_EDIT");
			
			returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = customerRequestValidator.getValidated(request,customerRequest);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/customerRequest");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				customerRequest = customerRequestRepository.save(customerRequest);
				returnResult.put("successMessage", "Successfully Saved CustomerRequest Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(CustomerRequestManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			CustomerRequestManagerLogger.info("Exception Occured During Updating CustomerRequest Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	
	public Map<String,Object> saveMultipleCustomerRequestData(HttpServletRequest request, List<CustomerRequest> customerRequests) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERREQUEST_CREATE");
		
		returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (CustomerRequest customerRequest : customerRequests) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = customerRequestValidator.getValidated(request, customerRequest);
				customerRequest = customerRequestRepository.save(customerRequest);
				returnResult.put(customerRequest+" "+customerRequest.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			CustomerRequestManagerLogger.info("Exception Occured During Adding CustomerRequest List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully CustomerRequest Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERREQUEST_DELETE");
		
		returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			CustomerRequest customerRequest = customerRequestRepository.findOne(id);
			if (UtilValidate.isNotEmpty(customerRequest)) {
				customerRequestRepository.delete(id);
				
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			CustomerRequestManagerLogger.info("Exception Occured During Removing CustomerRequest Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERREQUEST_DELETE");
		
		returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All CustomerRequest Deleted.";
		try {
			customerRequestRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerRequestManagerLogger.info("Exception Occured During Removing CustomerRequest Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERREQUEST_READ");
		
		returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<CustomerRequest> paginatedResult = customerRequestRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerRequestManagerLogger.info("Exception Occured During Fetching Paginated CustomerRequest Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Map<String,Object> findPaginatedCustomerRequestsBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERREQUEST_READ");
		
		returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<CustomerRequest> paginatedResultBySearchToken = customerRequestRepository.findPaginatedCustomerRequestsBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerRequestManagerLogger.info("Exception Occured During Fetching Paginated CustomerRequest Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMERREQUEST_READ");
		
		returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			CustomerRequest customerRequest = customerRequestRepository.findOne(id);
			returnResult.put("customerRequest", customerRequest);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerRequestManagerLogger.info("Exception Occured During Fetching CustomerRequest Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<CustomerRequest> customerRequests = new ArrayList<CustomerRequest>();
		allowedPermissions.add("CUSTOMERREQUEST_READ");
		
		returnResult = customerRequestValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			customerRequests = customerRequestRepository.findById(id);
			Page<CustomerRequest> pages = new PageImpl<CustomerRequest>(customerRequests, pageRequest, customerRequests.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerRequestManagerLogger.info("Exception Occured During Fetching CustomerRequest Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
