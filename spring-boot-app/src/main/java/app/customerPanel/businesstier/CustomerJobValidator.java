package app.customerPanel.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.CustomerJob;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.customerPanel.businesstier.CustomerJobManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class CustomerJobValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private CustomerJobManager customerJobManager;
	public void validate(CustomerJob customerJob) throws Exception {
		if (!checkNull(customerJob)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}
	
	public Map<String,Object> getValidated(HttpServletRequest request, CustomerJob customerJob){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility customerJobProperties = new ConfigUtility(new File("i18n/customerPanel/customerJob_i18n.properties"));
			Properties customerJobPropertyValue = customerJobProperties.loadProperties();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(CustomerJob customerJob){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
