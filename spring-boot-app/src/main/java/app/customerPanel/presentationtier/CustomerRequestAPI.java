package app.customerPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.CustomerRequest;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.customerPanel.businesstier.CustomerRequestManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class CustomerRequestAPI {
	
	@Autowired
	private CustomerRequestManager customerRequestManager;

	@PostMapping("/customerRequest")
	public Map<String,Object> createCustomerRequest(HttpServletRequest request, @RequestBody CustomerRequest customerRequest) {
		
		customerRequest.setCustomer(null);

		return customerRequestManager.create(request,customerRequest);
	}
	@GetMapping("/customerRequest/{id}")
	public Map<String,Object> getCustomerRequest(HttpServletRequest request, @PathVariable Long id) {
		return customerRequestManager.getCustomerRequestForEdit(request,id);
	}
	@GetMapping(value = "/customerRequests")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return customerRequestManager.getCustomerRequests(request);
	}
	@PutMapping("/customerRequest")
	public Map<String,Object> updateCustomerRequest(HttpServletRequest request, @RequestBody CustomerRequest customerRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = customerRequestManager.getCustomerRequest(request,customerRequest.getId());
			CustomerRequest newCustomerRequest = (CustomerRequest)resultMap.get("customerRequest");
			newCustomerRequest.setFromTime(customerRequest.getFromTime());
newCustomerRequest.setToTime(customerRequest.getToTime());
newCustomerRequest.setApprovalStatus(customerRequest.getApprovalStatus());

			return customerRequestManager.update(request,newCustomerRequest);
		}
		
	}
	@DeleteMapping("/customerRequest/{id}")
	public Map<String,Object> deleteCustomerRequest(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return customerRequestManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/customerRequest/saveMultipleCustomerRequestData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleCustomerRequestData( HttpServletRequest request, @RequestBody List<CustomerRequest> customerRequests) {
		return customerRequestManager.saveMultipleCustomerRequestData(request,customerRequests);
	}
	@RequestMapping(value = "/customerRequest/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return customerRequestManager.remove(request,id);
	}
	@RequestMapping(value = "/customerRequest/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return customerRequestManager.removeAll(request);
	}
	@RequestMapping(value = "/customerRequest/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return customerRequestManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/customerRequest/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return customerRequestManager.findPaginatedCustomerRequestsBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/customerRequest/searchOnCustomerRequestTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnCustomerRequestTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return customerRequestManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/customerRequest/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<CustomerRequest> customerRequests = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				CustomerRequest customerRequest = new CustomerRequest();
				HSSFRow row = worksheet.getRow(i++);
				//customerRequest.setField2(row.getCell(1).getStringCellValue());
				customerRequest.setFromTime(row.getCell(0).getStringCellValue());
customerRequest.setToTime(row.getCell(1).getStringCellValue());
customerRequest.setApprovalStatus(row.getCell(2).getStringCellValue());

				customerRequests.add(customerRequest);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("customerRequests", customerRequests);
		return returnResult;
	}
	@RequestMapping(value = "/customerRequest/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<CustomerRequest> customerRequests = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				CustomerRequest customerRequest = new CustomerRequest();
				XSSFRow row = worksheet.getRow(i++);
				//customerRequest.setField2(row.getCell(1).getStringCellValue());
				customerRequest.setFromTime(row.getCell(0).getStringCellValue());
customerRequest.setToTime(row.getCell(1).getStringCellValue());
customerRequest.setApprovalStatus(row.getCell(2).getStringCellValue());

				customerRequests.add(customerRequest);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("customerRequests", customerRequests);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/customerRequest/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<CustomerRequest> customerRequests = new ArrayList<CustomerRequest>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			customerRequests = (List<CustomerRequest>) resultMap.get("customerRequests");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			customerRequests = (List<CustomerRequest>) resultMap.get("customerRequests");
		}
		
		if (UtilValidate.isNotEmpty(customerRequests) && customerRequests.size()>0) {
			customerRequests.remove(0);
		}
		returnResult.put("customerRequests", customerRequests);
		return returnResult;
	}
}
