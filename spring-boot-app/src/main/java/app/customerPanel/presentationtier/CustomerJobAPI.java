package app.customerPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.CustomerJob;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.customerPanel.businesstier.CustomerJobManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class CustomerJobAPI {
	
	@Autowired
	private CustomerJobManager customerJobManager;

	@PostMapping("/customerJob")
	public Map<String,Object> createCustomerJob(HttpServletRequest request, @RequestBody CustomerJob customerJob) {
		
		customerJob.setCustomer(null);

		return customerJobManager.create(request,customerJob);
	}
	@GetMapping("/customerJob/{id}")
	public Map<String,Object> getCustomerJob(HttpServletRequest request, @PathVariable Long id) {
		return customerJobManager.getCustomerJobForEdit(request,id);
	}
	@GetMapping(value = "/customerJobs")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return customerJobManager.getCustomerJobs(request);
	}
	@PutMapping("/customerJob")
	public Map<String,Object> updateCustomerJob(HttpServletRequest request, @RequestBody CustomerJob customerJob) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = customerJobManager.getCustomerJob(request,customerJob.getId());
			CustomerJob newCustomerJob = (CustomerJob)resultMap.get("customerJob");
			newCustomerJob.setJobDetails(customerJob.getJobDetails());
newCustomerJob.setCreatedDate(customerJob.getCreatedDate());
newCustomerJob.setFromTime(customerJob.getFromTime());
newCustomerJob.setToTime(customerJob.getToTime());
newCustomerJob.setApprovalStatus(customerJob.getApprovalStatus());

			return customerJobManager.update(request,newCustomerJob);
		}
		
	}
	@DeleteMapping("/customerJob/{id}")
	public Map<String,Object> deleteCustomerJob(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return customerJobManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/customerJob/saveMultipleCustomerJobData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleCustomerJobData( HttpServletRequest request, @RequestBody List<CustomerJob> customerJobs) {
		return customerJobManager.saveMultipleCustomerJobData(request,customerJobs);
	}
	@RequestMapping(value = "/customerJob/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return customerJobManager.remove(request,id);
	}
	@RequestMapping(value = "/customerJob/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return customerJobManager.removeAll(request);
	}
	@RequestMapping(value = "/customerJob/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return customerJobManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/customerJob/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return customerJobManager.findPaginatedCustomerJobsBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/customerJob/searchOnCustomerJobTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnCustomerJobTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return customerJobManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/customerJob/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<CustomerJob> customerJobs = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				CustomerJob customerJob = new CustomerJob();
				HSSFRow row = worksheet.getRow(i++);
				//customerJob.setField2(row.getCell(1).getStringCellValue());
				customerJob.setJobDetails(row.getCell(0).getStringCellValue());
customerJob.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));
customerJob.setFromTime(row.getCell(2).getStringCellValue());
customerJob.setToTime(row.getCell(3).getStringCellValue());
customerJob.setApprovalStatus(row.getCell(4).getStringCellValue());

				customerJobs.add(customerJob);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("customerJobs", customerJobs);
		return returnResult;
	}
	@RequestMapping(value = "/customerJob/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<CustomerJob> customerJobs = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				CustomerJob customerJob = new CustomerJob();
				XSSFRow row = worksheet.getRow(i++);
				//customerJob.setField2(row.getCell(1).getStringCellValue());
				customerJob.setJobDetails(row.getCell(0).getStringCellValue());
customerJob.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));
customerJob.setFromTime(row.getCell(2).getStringCellValue());
customerJob.setToTime(row.getCell(3).getStringCellValue());
customerJob.setApprovalStatus(row.getCell(4).getStringCellValue());

				customerJobs.add(customerJob);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("customerJobs", customerJobs);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/customerJob/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<CustomerJob> customerJobs = new ArrayList<CustomerJob>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			customerJobs = (List<CustomerJob>) resultMap.get("customerJobs");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			customerJobs = (List<CustomerJob>) resultMap.get("customerJobs");
		}
		
		if (UtilValidate.isNotEmpty(customerJobs) && customerJobs.size()>0) {
			customerJobs.remove(0);
		}
		returnResult.put("customerJobs", customerJobs);
		return returnResult;
	}
}
