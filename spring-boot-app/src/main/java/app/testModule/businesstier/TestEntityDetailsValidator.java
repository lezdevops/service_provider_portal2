package app.testModule.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.TestEntityDetails;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.testModule.businesstier.TestEntityDetailsManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class TestEntityDetailsValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private TestEntityDetailsManager testEntityDetailsManager;
	public void validate(TestEntityDetails testEntityDetails) throws Exception {
		if (!checkNull(testEntityDetails)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}
	
	public Map<String,Object> getValidated(HttpServletRequest request, TestEntityDetails testEntityDetails){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility testEntityDetailsProperties = new ConfigUtility(new File("i18n/testModule/testEntityDetails_i18n.properties"));
			Properties testEntityDetailsPropertyValue = testEntityDetailsProperties.loadProperties();
			
if (UtilValidate.isEmpty(testEntityDetails.getTestEntity())) {
errorMessageMap.put("testEntity_validationMessage" , testEntityDetailsPropertyValue.getProperty(language+ ".testEntity.requiredValidationLabel"));
}
if (UtilValidate.isNotEmpty(testEntityDetailsManager.getTestEntityDetailsesByTestEntity(testEntityDetails.getTestEntity(),testEntityDetails.getId()))) {
errorMessageMap.put("TestEntity_validationMessage" , testEntityDetailsPropertyValue.getProperty(language+ ".testEntity.duplicateValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(TestEntityDetails testEntityDetails){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
