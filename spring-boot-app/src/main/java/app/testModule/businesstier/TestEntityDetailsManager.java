package app.testModule.businesstier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.TestEntityDetails;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.ApprovalQueueManager;
import app.globalSetup.businesstier.NotificationManager;
import app.testModule.persistencetier.TestEntityDetailsRepository;


import app.appmodel.TestEntity;


@Service
public class TestEntityDetailsManager {
	private static final org.apache.log4j.Logger TestEntityDetailsManagerLogger = org.apache.log4j.Logger.getLogger(TestEntityDetailsManager.class);
	@Autowired
	private TestEntityDetailsRepository testEntityDetailsRepository;
	@Autowired
	private TestEntityDetailsValidator testEntityDetailsValidator;
	@Autowired
	private NotificationManager notificationManager;
	@Autowired
	private ApprovalQueueManager approvalQueueManager;
	
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getTestEntityDetailsForEdit(HttpServletRequest request, Long id){
		TestEntityDetails newTestEntityDetails = new TestEntityDetails();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITYDETAILS_EDIT");
		
		returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		TestEntityDetails testEntityDetails = null;
		try {
			testEntityDetails = testEntityDetailsRepository.getOne(id);
			if (UtilValidate.isNotEmpty(testEntityDetails)) {
				newTestEntityDetails.setId(testEntityDetails.getId());
newTestEntityDetails.setTestEntity(testEntityDetails.getTestEntity());
newTestEntityDetails.setNumber(testEntityDetails.getNumber());
newTestEntityDetails.setTextBox(testEntityDetails.getTextBox());
newTestEntityDetails.setDescription(testEntityDetails.getDescription());
newTestEntityDetails.setCheckbox(testEntityDetails.getCheckbox());
newTestEntityDetails.setCreatedDate(testEntityDetails.getCreatedDate());
newTestEntityDetails.setDatePicker(testEntityDetails.getDatePicker());

				String docFile = testEntityDetails.getDocFile();
	if (UtilValidate.isNotEmpty(docFile)) {	File file = new File(docFile);
		if (file.exists()) {
		String base64String ="";
		try {
			base64String = FileUploadUtils.getBase64StringFromFilePath(docFile);
			String extension = FileUploadUtils.getFileExtension(file.getName());
			
			base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		newTestEntityDetails.setDocFile(base64String);
	}
}
String imageFile = testEntityDetails.getImageFile();
	if (UtilValidate.isNotEmpty(imageFile)) {	File file = new File(imageFile);
		if (file.exists()) {
		String base64String ="";
		try {
			base64String = FileUploadUtils.getBase64StringFromFilePath(imageFile);
			String extension = FileUploadUtils.getFileExtension(file.getName());
			
			base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		newTestEntityDetails.setImageFile(base64String);
	}
}

			}
			returnResult.put("testEntityDetails", newTestEntityDetails);
		} catch (Exception e) {
			TestEntityDetailsManagerLogger.info("Exception Occured During Fetching TestEntityDetails Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public Map<String,Object> getTestEntityDetails(HttpServletRequest request, Long id){
		TestEntityDetails newTestEntityDetails = new TestEntityDetails();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITYDETAILS_READ");
		
		returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			TestEntityDetails testEntityDetails = testEntityDetailsRepository.getOne(id);
			if (UtilValidate.isNotEmpty(testEntityDetails)) {
				newTestEntityDetails.setId(testEntityDetails.getId());
newTestEntityDetails.setTestEntity(testEntityDetails.getTestEntity());
newTestEntityDetails.setNumber(testEntityDetails.getNumber());
newTestEntityDetails.setTextBox(testEntityDetails.getTextBox());
newTestEntityDetails.setDescription(testEntityDetails.getDescription());
newTestEntityDetails.setCheckbox(testEntityDetails.getCheckbox());
newTestEntityDetails.setCreatedDate(testEntityDetails.getCreatedDate());
newTestEntityDetails.setDatePicker(testEntityDetails.getDatePicker());

				String docFile = testEntityDetails.getDocFile();
	if (UtilValidate.isNotEmpty(docFile)) {	File file = new File(docFile);
		if (file.exists()) {
		String base64String ="";
		try {
			base64String = FileUploadUtils.getBase64StringFromFilePath(docFile);
			String extension = FileUploadUtils.getFileExtension(file.getName());
			
			base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		newTestEntityDetails.setDocFile(base64String);
	}
}
String imageFile = testEntityDetails.getImageFile();
	if (UtilValidate.isNotEmpty(imageFile)) {	File file = new File(imageFile);
		if (file.exists()) {
		String base64String ="";
		try {
			base64String = FileUploadUtils.getBase64StringFromFilePath(imageFile);
			String extension = FileUploadUtils.getFileExtension(file.getName());
			
			base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		newTestEntityDetails.setImageFile(base64String);
	}
}

			}
			
			returnResult.put("testEntityDetails", newTestEntityDetails);
		} catch (Exception e) {
			TestEntityDetailsManagerLogger.info("Exception Occured During Fetching TestEntityDetails Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getTestEntityDetailss(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITYDETAILS_READ");
		
		returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<TestEntityDetails> testEntityDetailssList = new ArrayList<TestEntityDetails>();
			Iterable<TestEntityDetails> testEntityDetailss = testEntityDetailsRepository.findAll();
			for (TestEntityDetails testEntityDetails : testEntityDetailss) {
				testEntityDetailssList.add(testEntityDetails);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("testEntityDetailss", testEntityDetailssList);
		} catch (Exception e) {
			TestEntityDetailsManagerLogger.info("Exception Occured During Fetching TestEntityDetails List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public List<TestEntityDetails> getTestEntityDetailsesByTestEntity(TestEntity TestEntity, Long id){List<TestEntityDetails> testEntityDetailssList = new ArrayList<TestEntityDetails>();Iterable<TestEntityDetails> testEntityDetailss = null;if(UtilValidate.isNotEmpty(id)){testEntityDetailss = testEntityDetailsRepository.findByTestEntityDuringUpdate(TestEntity,id);}else{testEntityDetailss = testEntityDetailsRepository.findByTestEntity(TestEntity);}for (TestEntityDetails testEntityDetails : testEntityDetailss) {	testEntityDetailssList.add(testEntityDetails);}return testEntityDetailssList;}
	public Map<String,Object> create(HttpServletRequest request, TestEntityDetails testEntityDetails) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("TESTENTITYDETAILS_CREATE");
			
			returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = testEntityDetailsValidator.getValidated(request,testEntityDetails);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/testEntityDetails");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				String docFile = testEntityDetails.getDocFile();
  if (UtilValidate.isNotEmpty(docFile)) {
	if (UtilValidate.isNotEmpty(testEntityDetails.getId())) {
	TestEntityDetails oldTestEntityDetails = testEntityDetailsRepository.findTestEntityDetailsById(testEntityDetails.getId());
		if (UtilValidate.isNotEmpty(oldTestEntityDetails)) {
		String docFileOld = oldTestEntityDetails.getDocFile();
		if (UtilValidate.isNotEmpty(docFileOld)) {
			File file = new File(docFileOld);
			if (file.exists()) {
				FileUtils.forceDelete(file);
				}
			}
		}	}		long now = HelperUtils.nowTimestamp().getTime();
		Map<String,Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(docFile);
		String byteArray = (String)mapData.get("byteArray");
		String extension = (String)mapData.get("extension");		String filePath = path+"/"+now+"."+extension;
		testEntityDetails.setDocFile(filePath);
		FileUploadUtils.store(byteArray, filePath);
}String imageFile = testEntityDetails.getImageFile();
  if (UtilValidate.isNotEmpty(imageFile)) {
	if (UtilValidate.isNotEmpty(testEntityDetails.getId())) {
	TestEntityDetails oldTestEntityDetails = testEntityDetailsRepository.findTestEntityDetailsById(testEntityDetails.getId());
		if (UtilValidate.isNotEmpty(oldTestEntityDetails)) {
		String imageFileOld = oldTestEntityDetails.getImageFile();
		if (UtilValidate.isNotEmpty(imageFileOld)) {
			File file = new File(imageFileOld);
			if (file.exists()) {
				FileUtils.forceDelete(file);
				}
			}
		}	}		long now = HelperUtils.nowTimestamp().getTime();
		Map<String,Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(imageFile);
		String byteArray = (String)mapData.get("byteArray");
		String extension = (String)mapData.get("extension");		String filePath = path+"/"+now+"."+extension;
		testEntityDetails.setImageFile(filePath);
		FileUploadUtils.store(byteArray, filePath);
}
				testEntityDetails = testEntityDetailsRepository.save(testEntityDetails);
				notificationManager.send(request, null, testEntityDetails, "TESTENTITYDETAILS_CREATE");
				approvalQueueManager.saveApprovalQueue(request, testEntityDetails, "TESTENTITYDETAILS_CREATE");
				returnResult.put("successMessage", "Successfully Saved TestEntityDetails Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			TestEntityDetailsManagerLogger.info("Exception Occured During Creating TestEntityDetails Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> update(HttpServletRequest request, TestEntityDetails testEntityDetails) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("TESTENTITYDETAILS_EDIT");
			
			returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = testEntityDetailsValidator.getValidated(request,testEntityDetails);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/testEntityDetails");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				String docFile = testEntityDetails.getDocFile();
  if (UtilValidate.isNotEmpty(docFile)) {
	if (UtilValidate.isNotEmpty(testEntityDetails.getId())) {
	TestEntityDetails oldTestEntityDetails = testEntityDetailsRepository.findTestEntityDetailsById(testEntityDetails.getId());
		if (UtilValidate.isNotEmpty(oldTestEntityDetails)) {
		String docFileOld = oldTestEntityDetails.getDocFile();
		if (UtilValidate.isNotEmpty(docFileOld)) {
			File file = new File(docFileOld);
			if (file.exists()) {
				FileUtils.forceDelete(file);
				}
			}
		}	}		long now = HelperUtils.nowTimestamp().getTime();
		Map<String,Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(docFile);
		String byteArray = (String)mapData.get("byteArray");
		String extension = (String)mapData.get("extension");		String filePath = path+"/"+now+"."+extension;
		testEntityDetails.setDocFile(filePath);
		FileUploadUtils.store(byteArray, filePath);
}String imageFile = testEntityDetails.getImageFile();
  if (UtilValidate.isNotEmpty(imageFile)) {
	if (UtilValidate.isNotEmpty(testEntityDetails.getId())) {
	TestEntityDetails oldTestEntityDetails = testEntityDetailsRepository.findTestEntityDetailsById(testEntityDetails.getId());
		if (UtilValidate.isNotEmpty(oldTestEntityDetails)) {
		String imageFileOld = oldTestEntityDetails.getImageFile();
		if (UtilValidate.isNotEmpty(imageFileOld)) {
			File file = new File(imageFileOld);
			if (file.exists()) {
				FileUtils.forceDelete(file);
				}
			}
		}	}		long now = HelperUtils.nowTimestamp().getTime();
		Map<String,Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(imageFile);
		String byteArray = (String)mapData.get("byteArray");
		String extension = (String)mapData.get("extension");		String filePath = path+"/"+now+"."+extension;
		testEntityDetails.setImageFile(filePath);
		FileUploadUtils.store(byteArray, filePath);
}
				testEntityDetails = testEntityDetailsRepository.save(testEntityDetails);
				returnResult.put("successMessage", "Successfully Saved TestEntityDetails Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(TestEntityDetailsManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			TestEntityDetailsManagerLogger.info("Exception Occured During Updating TestEntityDetails Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	
	public Map<String,Object> saveMultipleTestEntityDetailsData(HttpServletRequest request, List<TestEntityDetails> testEntityDetailss) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITYDETAILS_CREATE");
		
		returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (TestEntityDetails testEntityDetails : testEntityDetailss) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = testEntityDetailsValidator.getValidated(request, testEntityDetails);
				testEntityDetails = testEntityDetailsRepository.save(testEntityDetails);
				returnResult.put(testEntityDetails+" "+testEntityDetails.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			TestEntityDetailsManagerLogger.info("Exception Occured During Adding TestEntityDetails List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully TestEntityDetails Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITYDETAILS_DELETE");
		
		returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			TestEntityDetails testEntityDetails = testEntityDetailsRepository.findOne(id);
			if (UtilValidate.isNotEmpty(testEntityDetails)) {
				testEntityDetailsRepository.delete(id);
						String docFile = testEntityDetails.getDocFile();
		if (UtilValidate.isNotEmpty(docFile)) {				
			File file = new File(docFile);
			if (file.exists()) {
						FileUtils.forceDelete(file);
			}
		}		String imageFile = testEntityDetails.getImageFile();
		if (UtilValidate.isNotEmpty(imageFile)) {				
			File file = new File(imageFile);
			if (file.exists()) {
						FileUtils.forceDelete(file);
			}
		}
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			TestEntityDetailsManagerLogger.info("Exception Occured During Removing TestEntityDetails Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITYDETAILS_DELETE");
		
		returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All TestEntityDetails Deleted.";
		try {
			testEntityDetailsRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			TestEntityDetailsManagerLogger.info("Exception Occured During Removing TestEntityDetails Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITYDETAILS_READ");
		
		returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<TestEntityDetails> paginatedResult = testEntityDetailsRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			TestEntityDetailsManagerLogger.info("Exception Occured During Fetching Paginated TestEntityDetails Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Map<String,Object> findPaginatedTestEntityDetailssBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITYDETAILS_READ");
		
		returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<TestEntityDetails> paginatedResultBySearchToken = testEntityDetailsRepository.findPaginatedTestEntityDetailssBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			TestEntityDetailsManagerLogger.info("Exception Occured During Fetching Paginated TestEntityDetails Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITYDETAILS_READ");
		
		returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			TestEntityDetails testEntityDetails = testEntityDetailsRepository.findOne(id);
			returnResult.put("testEntityDetails", testEntityDetails);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			TestEntityDetailsManagerLogger.info("Exception Occured During Fetching TestEntityDetails Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<TestEntityDetails> testEntityDetailss = new ArrayList<TestEntityDetails>();
		allowedPermissions.add("TESTENTITYDETAILS_READ");
		
		returnResult = testEntityDetailsValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			testEntityDetailss = testEntityDetailsRepository.findById(id);
			Page<TestEntityDetails> pages = new PageImpl<TestEntityDetails>(testEntityDetailss, pageRequest, testEntityDetailss.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			TestEntityDetailsManagerLogger.info("Exception Occured During Fetching TestEntityDetails Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
