package app.testModule.businesstier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.TestEntity;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.ApprovalQueueManager;
import app.globalSetup.businesstier.NotificationManager;
import app.testModule.persistencetier.TestEntityRepository;


import app.appmodel.TestEntityDetails;


@Service
public class TestEntityManager {
	private static final org.apache.log4j.Logger TestEntityManagerLogger = org.apache.log4j.Logger.getLogger(TestEntityManager.class);
	@Autowired
	private TestEntityRepository testEntityRepository;
	@Autowired
	private TestEntityValidator testEntityValidator;
	@Autowired
	private NotificationManager notificationManager;
	@Autowired
	private ApprovalQueueManager approvalQueueManager;
	
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getTestEntityForEdit(HttpServletRequest request, Long id){
		TestEntity newTestEntity = new TestEntity();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITY_EDIT");
		
		returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		TestEntity testEntity = null;
		try {
			testEntity = testEntityRepository.getOne(id);
			if (UtilValidate.isNotEmpty(testEntity)) {
				newTestEntity.setId(testEntity.getId());
newTestEntity.setName(testEntity.getName());
newTestEntity.setDescription(testEntity.getDescription());
newTestEntity.setStartDate(testEntity.getStartDate());
newTestEntity.setEndDate(testEntity.getEndDate());
newTestEntity.setCreatedDate(testEntity.getCreatedDate());

				
			}
			returnResult.put("testEntity", newTestEntity);
		} catch (Exception e) {
			TestEntityManagerLogger.info("Exception Occured During Fetching TestEntity Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public Map<String,Object> getTestEntity(HttpServletRequest request, Long id){
		TestEntity newTestEntity = new TestEntity();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITY_READ");
		
		returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			TestEntity testEntity = testEntityRepository.getOne(id);
			if (UtilValidate.isNotEmpty(testEntity)) {
				newTestEntity.setId(testEntity.getId());
newTestEntity.setName(testEntity.getName());
newTestEntity.setDescription(testEntity.getDescription());
newTestEntity.setStartDate(testEntity.getStartDate());
newTestEntity.setEndDate(testEntity.getEndDate());
newTestEntity.setCreatedDate(testEntity.getCreatedDate());

				
			}
			
			returnResult.put("testEntity", newTestEntity);
		} catch (Exception e) {
			TestEntityManagerLogger.info("Exception Occured During Fetching TestEntity Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getTestEntitys(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITY_READ");
		
		returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<TestEntity> testEntitysList = new ArrayList<TestEntity>();
			Iterable<TestEntity> testEntitys = testEntityRepository.findAll();
			for (TestEntity testEntity : testEntitys) {
				testEntitysList.add(testEntity);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("testEntitys", testEntitysList);
		} catch (Exception e) {
			TestEntityManagerLogger.info("Exception Occured During Fetching TestEntity List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public List<TestEntity> getTestEntityesByName(String name, Long id){List<TestEntity> testEntitysList = new ArrayList<TestEntity>();Iterable<TestEntity> testEntitys = null;if(UtilValidate.isNotEmpty(id)){testEntitys = testEntityRepository.findByNameDuringUpdate(name,id);}else{testEntitys = testEntityRepository.findByName(name);}for (TestEntity testEntity : testEntitys) {	testEntitysList.add(testEntity);}return testEntitysList;}
	public Map<String,Object> create(HttpServletRequest request, TestEntity testEntity) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("TESTENTITY_CREATE");
			
			returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = testEntityValidator.getValidated(request,testEntity);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/testEntity");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				testEntity = testEntityRepository.save(testEntity);
				notificationManager.send(request, null, testEntity, "TESTENTITY_CREATE");
				approvalQueueManager.saveApprovalQueue(request, testEntity, "TESTENTITY_CREATE");
				returnResult.put("successMessage", "Successfully Saved TestEntity Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			TestEntityManagerLogger.info("Exception Occured During Creating TestEntity Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> update(HttpServletRequest request, TestEntity testEntity) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("TESTENTITY_EDIT");
			
			returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = testEntityValidator.getValidated(request,testEntity);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/testEntity");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				testEntity = testEntityRepository.save(testEntity);
				returnResult.put("successMessage", "Successfully Saved TestEntity Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(TestEntityManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			TestEntityManagerLogger.info("Exception Occured During Updating TestEntity Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	
	public Map<String,Object> saveMultipleTestEntityData(HttpServletRequest request, List<TestEntity> testEntitys) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITY_CREATE");
		
		returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (TestEntity testEntity : testEntitys) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = testEntityValidator.getValidated(request, testEntity);
				testEntity = testEntityRepository.save(testEntity);
				returnResult.put(testEntity+" "+testEntity.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			TestEntityManagerLogger.info("Exception Occured During Adding TestEntity List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully TestEntity Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITY_DELETE");
		
		returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			TestEntity testEntity = testEntityRepository.findOne(id);
			if (UtilValidate.isNotEmpty(testEntity)) {
				testEntityRepository.delete(id);
				
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			TestEntityManagerLogger.info("Exception Occured During Removing TestEntity Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITY_DELETE");
		
		returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All TestEntity Deleted.";
		try {
			testEntityRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			TestEntityManagerLogger.info("Exception Occured During Removing TestEntity Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITY_READ");
		
		returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<TestEntity> paginatedResult = testEntityRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			TestEntityManagerLogger.info("Exception Occured During Fetching Paginated TestEntity Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Map<String,Object> findPaginatedTestEntitysBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITY_READ");
		
		returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<TestEntity> paginatedResultBySearchToken = testEntityRepository.findPaginatedTestEntitysBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			TestEntityManagerLogger.info("Exception Occured During Fetching Paginated TestEntity Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("TESTENTITY_READ");
		
		returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			TestEntity testEntity = testEntityRepository.findOne(id);
			returnResult.put("testEntity", testEntity);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			TestEntityManagerLogger.info("Exception Occured During Fetching TestEntity Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<TestEntity> testEntitys = new ArrayList<TestEntity>();
		allowedPermissions.add("TESTENTITY_READ");
		
		returnResult = testEntityValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			testEntitys = testEntityRepository.findById(id);
			Page<TestEntity> pages = new PageImpl<TestEntity>(testEntitys, pageRequest, testEntitys.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			TestEntityManagerLogger.info("Exception Occured During Fetching TestEntity Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
