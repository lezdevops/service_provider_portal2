package app.testModule.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.TestEntity;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.testModule.businesstier.TestEntityManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class TestEntityValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private TestEntityManager testEntityManager;
	public void validate(TestEntity testEntity) throws Exception {
		if (!checkNull(testEntity)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}
	
	public Map<String,Object> getValidated(HttpServletRequest request, TestEntity testEntity){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility testEntityProperties = new ConfigUtility(new File("i18n/testModule/testEntity_i18n.properties"));
			Properties testEntityPropertyValue = testEntityProperties.loadProperties();
			
if (UtilValidate.isEmpty(testEntity.getName())) {
errorMessageMap.put("name_validationMessage" , testEntityPropertyValue.getProperty(language+ ".name.requiredValidationLabel"));
}
if (UtilValidate.isNotEmpty(testEntityManager.getTestEntityesByName(testEntity.getName(),testEntity.getId()))) {
errorMessageMap.put("name_validationMessage" , testEntityPropertyValue.getProperty(language+ ".name.duplicateValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(TestEntity testEntity){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
