package app.testModule.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.TestEntityDetails;
import app.appmodel.TestEntity;


@Repository
public interface TestEntityDetailsRepository extends JpaRepository<TestEntityDetails, Long>{
	List<TestEntityDetails> findById(Long id);
	TestEntityDetails findTestEntityDetailsById(Long id);
	@Query("select testEntityDetails from TestEntityDetails testEntityDetails where testEntityDetails.testEntity = :testEntity and testEntityDetails.id!=:id")
	List<TestEntityDetails> findByTestEntityDuringUpdate(@Param("testEntity") TestEntity testEntity, @Param("id") Long id);List<TestEntityDetails> findByTestEntity(TestEntity testEntity);
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT testEntityDetails FROM TestEntityDetails testEntityDetails WHERE  cast(testEntityDetails.testEntity.name AS string) like %:searchToken% OR cast(testEntityDetails.number AS string) like %:searchToken% OR cast(testEntityDetails.textBox AS string) like %:searchToken% OR cast(testEntityDetails.description AS string) like %:searchToken% OR cast(testEntityDetails.checkbox AS string) like %:searchToken% OR cast(testEntityDetails.createdDate AS string) like %:searchToken% OR cast(testEntityDetails.datePicker AS string) like %:searchToken% OR cast(testEntityDetails.docFile AS string) like %:searchToken% OR cast(testEntityDetails.imageFile AS string) like %:searchToken%")
	Page<TestEntityDetails> findPaginatedTestEntityDetailssBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
