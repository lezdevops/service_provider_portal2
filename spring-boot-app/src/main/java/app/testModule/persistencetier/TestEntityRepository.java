package app.testModule.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.TestEntity;
import app.appmodel.TestEntityDetails;


@Repository
public interface TestEntityRepository extends JpaRepository<TestEntity, Long>{
	List<TestEntity> findById(Long id);
	TestEntity findTestEntityById(Long id);
	@Query("select testEntity from TestEntity testEntity where testEntity.name = :name and testEntity.id!=:id")
	List<TestEntity> findByNameDuringUpdate(@Param("name") String name, @Param("id") Long id);List<TestEntity> findByName(String name);
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT testEntity FROM TestEntity testEntity WHERE  cast(testEntity.name AS string) like %:searchToken% OR cast(testEntity.description AS string) like %:searchToken% OR cast(testEntity.startDate AS string) like %:searchToken% OR cast(testEntity.endDate AS string) like %:searchToken% OR cast(testEntity.createdDate AS string) like %:searchToken%")
	Page<TestEntity> findPaginatedTestEntitysBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
