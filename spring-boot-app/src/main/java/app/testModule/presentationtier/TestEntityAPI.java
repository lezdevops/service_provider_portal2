package app.testModule.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.TestEntity;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.testModule.businesstier.TestEntityManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class TestEntityAPI {
	
	@Autowired
	private TestEntityManager testEntityManager;

	@PostMapping("/testEntity")
	public Map<String,Object> createTestEntity(HttpServletRequest request, @RequestBody TestEntity testEntity) {
		
		testEntity.setCreatedDate(null);

		return testEntityManager.create(request,testEntity);
	}
	@GetMapping("/testEntity/{id}")
	public Map<String,Object> getTestEntity(HttpServletRequest request, @PathVariable Long id) {
		return testEntityManager.getTestEntityForEdit(request,id);
	}
	@GetMapping(value = "/testEntitys")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return testEntityManager.getTestEntitys(request);
	}
	@PutMapping("/testEntity")
	public Map<String,Object> updateTestEntity(HttpServletRequest request, @RequestBody TestEntity testEntity) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = testEntityManager.getTestEntity(request,testEntity.getId());
			TestEntity newTestEntity = (TestEntity)resultMap.get("testEntity");
			newTestEntity.setDescription(testEntity.getDescription());
newTestEntity.setStartDate(testEntity.getStartDate());
newTestEntity.setEndDate(testEntity.getEndDate());
newTestEntity.setCreatedDate(testEntity.getCreatedDate());

			return testEntityManager.update(request,newTestEntity);
		}
		
	}
	@DeleteMapping("/testEntity/{id}")
	public Map<String,Object> deleteTestEntity(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return testEntityManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/testEntity/saveMultipleTestEntityData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleTestEntityData( HttpServletRequest request, @RequestBody List<TestEntity> testEntitys) {
		return testEntityManager.saveMultipleTestEntityData(request,testEntitys);
	}
	@RequestMapping(value = "/testEntity/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return testEntityManager.remove(request,id);
	}
	@RequestMapping(value = "/testEntity/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return testEntityManager.removeAll(request);
	}
	@RequestMapping(value = "/testEntity/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return testEntityManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/testEntity/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return testEntityManager.findPaginatedTestEntitysBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/testEntity/searchOnTestEntityTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnTestEntityTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return testEntityManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/testEntity/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<TestEntity> testEntitys = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				TestEntity testEntity = new TestEntity();
				HSSFRow row = worksheet.getRow(i++);
				//testEntity.setField2(row.getCell(1).getStringCellValue());
				testEntity.setName(row.getCell(0).getStringCellValue());
testEntity.setDescription(row.getCell(1).getStringCellValue());
testEntity.setStartDate(HelperUtils.getTimestampFromString(row.getCell(2).getStringCellValue()));
testEntity.setEndDate(HelperUtils.getTimestampFromString(row.getCell(3).getStringCellValue()));
testEntity.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(4).getStringCellValue()));

				testEntitys.add(testEntity);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("testEntitys", testEntitys);
		return returnResult;
	}
	@RequestMapping(value = "/testEntity/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<TestEntity> testEntitys = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				TestEntity testEntity = new TestEntity();
				XSSFRow row = worksheet.getRow(i++);
				//testEntity.setField2(row.getCell(1).getStringCellValue());
				testEntity.setName(row.getCell(0).getStringCellValue());
testEntity.setDescription(row.getCell(1).getStringCellValue());
testEntity.setStartDate(HelperUtils.getTimestampFromString(row.getCell(2).getStringCellValue()));
testEntity.setEndDate(HelperUtils.getTimestampFromString(row.getCell(3).getStringCellValue()));
testEntity.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(4).getStringCellValue()));

				testEntitys.add(testEntity);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("testEntitys", testEntitys);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/testEntity/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<TestEntity> testEntitys = new ArrayList<TestEntity>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			testEntitys = (List<TestEntity>) resultMap.get("testEntitys");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			testEntitys = (List<TestEntity>) resultMap.get("testEntitys");
		}
		
		if (UtilValidate.isNotEmpty(testEntitys) && testEntitys.size()>0) {
			testEntitys.remove(0);
		}
		returnResult.put("testEntitys", testEntitys);
		return returnResult;
	}
}
