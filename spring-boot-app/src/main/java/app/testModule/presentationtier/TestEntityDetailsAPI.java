package app.testModule.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.TestEntityDetails;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.testModule.businesstier.TestEntityDetailsManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class TestEntityDetailsAPI {
	
	@Autowired
	private TestEntityDetailsManager testEntityDetailsManager;

	@PostMapping("/testEntityDetails")
	public Map<String,Object> createTestEntityDetails(HttpServletRequest request, @RequestBody TestEntityDetails testEntityDetails) {
		
		testEntityDetails.setCreatedDate(null);

		return testEntityDetailsManager.create(request,testEntityDetails);
	}
	@GetMapping("/testEntityDetails/{id}")
	public Map<String,Object> getTestEntityDetails(HttpServletRequest request, @PathVariable Long id) {
		return testEntityDetailsManager.getTestEntityDetailsForEdit(request,id);
	}
	@GetMapping(value = "/testEntityDetailss")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return testEntityDetailsManager.getTestEntityDetailss(request);
	}
	@PutMapping("/testEntityDetails")
	public Map<String,Object> updateTestEntityDetails(HttpServletRequest request, @RequestBody TestEntityDetails testEntityDetails) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = testEntityDetailsManager.getTestEntityDetails(request,testEntityDetails.getId());
			TestEntityDetails newTestEntityDetails = (TestEntityDetails)resultMap.get("testEntityDetails");
			newTestEntityDetails.setNumber(testEntityDetails.getNumber());
newTestEntityDetails.setTextBox(testEntityDetails.getTextBox());
newTestEntityDetails.setDescription(testEntityDetails.getDescription());
newTestEntityDetails.setCheckbox(testEntityDetails.getCheckbox());
newTestEntityDetails.setCreatedDate(testEntityDetails.getCreatedDate());
newTestEntityDetails.setDatePicker(testEntityDetails.getDatePicker());
newTestEntityDetails.setDocFile(testEntityDetails.getDocFile());
newTestEntityDetails.setImageFile(testEntityDetails.getImageFile());

			return testEntityDetailsManager.update(request,newTestEntityDetails);
		}
		
	}
	@DeleteMapping("/testEntityDetails/{id}")
	public Map<String,Object> deleteTestEntityDetails(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return testEntityDetailsManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/testEntityDetails/saveMultipleTestEntityDetailsData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleTestEntityDetailsData( HttpServletRequest request, @RequestBody List<TestEntityDetails> testEntityDetailss) {
		return testEntityDetailsManager.saveMultipleTestEntityDetailsData(request,testEntityDetailss);
	}
	@RequestMapping(value = "/testEntityDetails/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return testEntityDetailsManager.remove(request,id);
	}
	@RequestMapping(value = "/testEntityDetails/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return testEntityDetailsManager.removeAll(request);
	}
	@RequestMapping(value = "/testEntityDetails/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return testEntityDetailsManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/testEntityDetails/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return testEntityDetailsManager.findPaginatedTestEntityDetailssBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/testEntityDetails/searchOnTestEntityDetailsTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnTestEntityDetailsTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return testEntityDetailsManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/testEntityDetails/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<TestEntityDetails> testEntityDetailss = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				TestEntityDetails testEntityDetails = new TestEntityDetails();
				HSSFRow row = worksheet.getRow(i++);
				//testEntityDetails.setField2(row.getCell(1).getStringCellValue());
				testEntityDetails.setNumber(Double.valueOf(row.getCell(0).getNumericCellValue()).longValue());
testEntityDetails.setTextBox(row.getCell(1).getStringCellValue());
testEntityDetails.setDescription(row.getCell(2).getStringCellValue());
testEntityDetails.setCheckbox(row.getCell(3).getBooleanCellValue());
testEntityDetails.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(4).getStringCellValue()));
testEntityDetails.setDatePicker(HelperUtils.getTimestampFromString(row.getCell(5).getStringCellValue()));
testEntityDetails.setDocFile(row.getCell(6).getStringCellValue());
testEntityDetails.setImageFile(row.getCell(7).getStringCellValue());

				testEntityDetailss.add(testEntityDetails);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("testEntityDetailss", testEntityDetailss);
		return returnResult;
	}
	@RequestMapping(value = "/testEntityDetails/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<TestEntityDetails> testEntityDetailss = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				TestEntityDetails testEntityDetails = new TestEntityDetails();
				XSSFRow row = worksheet.getRow(i++);
				//testEntityDetails.setField2(row.getCell(1).getStringCellValue());
				testEntityDetails.setNumber(Double.valueOf(row.getCell(0).getNumericCellValue()).longValue());
testEntityDetails.setTextBox(row.getCell(1).getStringCellValue());
testEntityDetails.setDescription(row.getCell(2).getStringCellValue());
testEntityDetails.setCheckbox(row.getCell(3).getBooleanCellValue());
testEntityDetails.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(4).getStringCellValue()));
testEntityDetails.setDatePicker(HelperUtils.getTimestampFromString(row.getCell(5).getStringCellValue()));
testEntityDetails.setDocFile(row.getCell(6).getStringCellValue());
testEntityDetails.setImageFile(row.getCell(7).getStringCellValue());

				testEntityDetailss.add(testEntityDetails);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("testEntityDetailss", testEntityDetailss);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/testEntityDetails/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<TestEntityDetails> testEntityDetailss = new ArrayList<TestEntityDetails>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			testEntityDetailss = (List<TestEntityDetails>) resultMap.get("testEntityDetailss");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			testEntityDetailss = (List<TestEntityDetails>) resultMap.get("testEntityDetailss");
		}
		
		if (UtilValidate.isNotEmpty(testEntityDetailss) && testEntityDetailss.size()>0) {
			testEntityDetailss.remove(0);
		}
		returnResult.put("testEntityDetailss", testEntityDetailss);
		return returnResult;
	}
}
