package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.TrainingInfo; 
import app.appmodel.TrainingInfo; 
// importDependentEntity

@Entity
@Table(name = "TrainingType")
public class TrainingType {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
private String shortCode;
private String description;
@JsonIgnoreProperties({"trainingType"})
@OneToMany(targetEntity=TrainingInfo.class, mappedBy = "trainingType",fetch = FetchType.LAZY)

private List<TrainingInfo> trainingInfos = new ArrayList();

private String isActive;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}

public void setShortCode(String shortCode){
this.shortCode= shortCode;
}

public String getShortCode(){
return shortCode;
}

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}
    public List<TrainingInfo> getTrainingInfos() {
        return trainingInfos;
    }

    public void setTrainingInfos(List<TrainingInfo> trainingInfos) {
        this.trainingInfos = trainingInfos;
    }
public void setIsActive(String isActive){
this.isActive= isActive;
}

public String getIsActive(){
return isActive;
}

	

}
