package app.appmodel;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "ApprovalProcess")
public class ApprovalProcess {
	@Id

	@GeneratedValue(strategy = GenerationType.AUTO)

	private Long id;
	private String processName;
	private String isActive;
	private String createdBy;

	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(name = "process_approver_map", joinColumns = @JoinColumn(name = "approval_proc_id"), inverseJoinColumns = @JoinColumn(name = "approver_id"))
	@JsonIgnoreProperties({ "approvalProcess" })
	private List<ApproverAdmin> approverAdmins = new ArrayList();


	private Timestamp created;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProcessName() {
		return processName;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public List<ApproverAdmin> getApproverAdmins() {
		return approverAdmins;
	}

	public void setApproverAdmins(List<ApproverAdmin> approverAdmins) {
		this.approverAdmins = approverAdmins;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getCreated() {
		return created;
	}

}
