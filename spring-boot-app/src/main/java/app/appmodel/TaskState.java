package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ProjectSubtask; 
import app.appmodel.ProjectSubtask; 
// importDependentEntity

@Entity
@Table(name = "TaskState")
public class TaskState {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
private String stateName;
private String description;
@JsonIgnoreProperties({"taskState"})
@OneToMany(targetEntity=ProjectSubtask.class, mappedBy = "taskState",fetch = FetchType.LAZY)

private List<ProjectSubtask> projectSubtasks = new ArrayList();


	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}

public void setStateName(String stateName){
this.stateName= stateName;
}

public String getStateName(){
return stateName;
}

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}
    public List<ProjectSubtask> getProjectSubtasks() {
        return projectSubtasks;
    }

    public void setProjectSubtasks(List<ProjectSubtask> projectSubtasks) {
        this.projectSubtasks = projectSubtasks;
    }
	

}
