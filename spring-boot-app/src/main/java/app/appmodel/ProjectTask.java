package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ProjectStory; 
import app.appmodel.ProjectSubtask; 
import app.appmodel.ProjectSubtask; 
// importDependentEntity

@Entity
@Table(name = "ProjectTask")
public class ProjectTask {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"projectTasks"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ProjectStory projectStory;

private String description;
private Timestamp startDate;
private Timestamp endDate;
private Timestamp createdDate;
@JsonIgnoreProperties({"projectTask"})
@OneToMany(targetEntity=ProjectSubtask.class, mappedBy = "projectTask",fetch = FetchType.LAZY)

private List<ProjectSubtask> projectSubtasks = new ArrayList();

private String document;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public ProjectStory getProjectStory() {
    return projectStory;
  }

  public void setProjectStory(ProjectStory projectStory) {
    this.projectStory = projectStory;
  }

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}

public void setStartDate(Timestamp startDate){
this.startDate= startDate;
}

public Timestamp getStartDate(){
return startDate;
}

public void setEndDate(Timestamp endDate){
this.endDate= endDate;
}

public Timestamp getEndDate(){
return endDate;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}
    public List<ProjectSubtask> getProjectSubtasks() {
        return projectSubtasks;
    }

    public void setProjectSubtasks(List<ProjectSubtask> projectSubtasks) {
        this.projectSubtasks = projectSubtasks;
    }
public void setDocument(String document){
this.document= document;
}

public String getDocument(){
return document;
}

	

}
