package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.Notification; 
// importDependentEntity

@Entity
@Table(name = "NotificationDetails")
public class NotificationDetails {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"notificationDetailss"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private Notification notification;

private String senderId;
private String receiverId;
private Boolean isRead;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public Notification getNotification() {
    return notification;
  }

  public void setNotification(Notification notification) {
    this.notification = notification;
  }

public void setSenderId(String senderId){
this.senderId= senderId;
}

public String getSenderId(){
return senderId;
}

public void setReceiverId(String receiverId){
this.receiverId= receiverId;
}

public String getReceiverId(){
return receiverId;
}

public void setIsRead(Boolean isRead){
this.isRead= isRead;
}

public Boolean getIsRead(){
return isRead;
}

	

}
