package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.User; 
import app.appmodel.CustomerSp; 
import app.appmodel.CustomerRequest; 
import app.appmodel.CustomerJob; 
import app.appmodel.CustomerSp; 
import app.appmodel.CustomerRequest; 
import app.appmodel.CustomerJob; 
// importDependentEntity

@Entity
@Table(name = "Customer")
public class Customer {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
private String name;
private String securityNumber;
private String profileImage;
private String email;
private Timestamp createdDate;
@JsonIgnoreProperties({"customers"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private User user;

@JsonIgnoreProperties({"customer"})
@OneToMany(targetEntity=CustomerSp.class, mappedBy = "customer",fetch = FetchType.LAZY)

private List<CustomerSp> customerSps = new ArrayList();

@JsonIgnoreProperties({"customer"})
@OneToMany(targetEntity=CustomerRequest.class, mappedBy = "customer",fetch = FetchType.LAZY)

private List<CustomerRequest> customerRequests = new ArrayList();

@JsonIgnoreProperties({"customer"})
@OneToMany(targetEntity=CustomerJob.class, mappedBy = "customer",fetch = FetchType.LAZY)

private List<CustomerJob> customerJobs = new ArrayList();

private Long isActive;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}

public void setName(String name){
this.name= name;
}

public String getName(){
return name;
}

public void setSecurityNumber(String securityNumber){
this.securityNumber= securityNumber;
}

public String getSecurityNumber(){
return securityNumber;
}

public void setProfileImage(String profileImage){
this.profileImage= profileImage;
}

public String getProfileImage(){
return profileImage;
}

public void setEmail(String email){
this.email= email;
}

public String getEmail(){
return email;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}
  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
    public List<CustomerSp> getCustomerSps() {
        return customerSps;
    }

    public void setCustomerSps(List<CustomerSp> customerSps) {
        this.customerSps = customerSps;
    }    public List<CustomerRequest> getCustomerRequests() {
        return customerRequests;
    }

    public void setCustomerRequests(List<CustomerRequest> customerRequests) {
        this.customerRequests = customerRequests;
    }    public List<CustomerJob> getCustomerJobs() {
        return customerJobs;
    }

    public void setCustomerJobs(List<CustomerJob> customerJobs) {
        this.customerJobs = customerJobs;
    }
public void setIsActive(Long isActive){
this.isActive= isActive;
}

public Long getIsActive(){
return isActive;
}

	

}
