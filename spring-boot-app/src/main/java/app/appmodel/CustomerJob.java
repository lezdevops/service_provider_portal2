package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.Customer; 
// importDependentEntity

@Entity
@Table(name = "CustomerJob")
public class CustomerJob {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"customerJobs"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private Customer customer;

private String jobDetails;
private Timestamp createdDate;
private String fromTime;
private String toTime;
private String approvalStatus;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

public void setJobDetails(String jobDetails){
this.jobDetails= jobDetails;
}

public String getJobDetails(){
return jobDetails;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}

public void setFromTime(String fromTime){
this.fromTime= fromTime;
}

public String getFromTime(){
return fromTime;
}

public void setToTime(String toTime){
this.toTime= toTime;
}

public String getToTime(){
return toTime;
}

public void setApprovalStatus(String approvalStatus){
this.approvalStatus= approvalStatus;
}

public String getApprovalStatus(){
return approvalStatus;
}

	

}
