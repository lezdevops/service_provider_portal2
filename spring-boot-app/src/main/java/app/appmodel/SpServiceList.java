package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ServiceProvider; 
import app.appmodel.ServiceType; 
import app.appmodel.Location; 
// importDependentEntity

@Entity
@Table(name = "SpServiceList")
public class SpServiceList {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"spServiceLists"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ServiceProvider serviceProvider;

private String serviceDetails;
@JsonIgnoreProperties({"spServiceLists"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ServiceType serviceType;

private Long visitCount;
@JsonIgnoreProperties({"spServiceLists"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private Location location;

private Long requestCount;
private String startTime;
private String endTime;
private String isActive;
private Timestamp availFrom;
private Timestamp availTo;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public ServiceProvider getServiceProvider() {
    return serviceProvider;
  }

  public void setServiceProvider(ServiceProvider serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

public void setServiceDetails(String serviceDetails){
this.serviceDetails= serviceDetails;
}

public String getServiceDetails(){
return serviceDetails;
}
  public ServiceType getServiceType() {
    return serviceType;
  }

  public void setServiceType(ServiceType serviceType) {
    this.serviceType = serviceType;
  }

public void setVisitCount(Long visitCount){
this.visitCount= visitCount;
}

public Long getVisitCount(){
return visitCount;
}
  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

public void setRequestCount(Long requestCount){
this.requestCount= requestCount;
}

public Long getRequestCount(){
return requestCount;
}

public void setStartTime(String startTime){
this.startTime= startTime;
}

public String getStartTime(){
return startTime;
}

public void setEndTime(String endTime){
this.endTime= endTime;
}

public String getEndTime(){
return endTime;
}

public void setIsActive(String isActive){
this.isActive= isActive;
}

public String getIsActive(){
return isActive;
}

public void setAvailFrom(Timestamp availFrom){
this.availFrom= availFrom;
}

public Timestamp getAvailFrom(){
return availFrom;
}

public void setAvailTo(Timestamp availTo){
this.availTo= availTo;
}

public Timestamp getAvailTo(){
return availTo;
}

	

}
