package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.Customer; 
import app.appmodel.ServiceProvider; 
// importDependentEntity

@Entity
@Table(name = "CustomerSp")
public class CustomerSp {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"customerSps"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private Customer customer;

@JsonIgnoreProperties({"customerSps"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ServiceProvider serviceProvider;

private Long isActive;
private String aproveStatus;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }
  public ServiceProvider getServiceProvider() {
    return serviceProvider;
  }

  public void setServiceProvider(ServiceProvider serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

public void setIsActive(Long isActive){
this.isActive= isActive;
}

public Long getIsActive(){
return isActive;
}

public void setAproveStatus(String aproveStatus){
this.aproveStatus= aproveStatus;
}

public String getAproveStatus(){
return aproveStatus;
}

	

}
