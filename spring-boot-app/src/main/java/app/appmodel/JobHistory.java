package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ServiceProvider; 
// importDependentEntity

@Entity
@Table(name = "JobHistory")
public class JobHistory {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"jobHistorys"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ServiceProvider serviceProvider;

private String employerName;
private String jobTitle;
private Timestamp fromDate;
private Timestamp thruDate;
private String location;
private String uploadedDoc;
private String description;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public ServiceProvider getServiceProvider() {
    return serviceProvider;
  }

  public void setServiceProvider(ServiceProvider serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

public void setEmployerName(String employerName){
this.employerName= employerName;
}

public String getEmployerName(){
return employerName;
}

public void setJobTitle(String jobTitle){
this.jobTitle= jobTitle;
}

public String getJobTitle(){
return jobTitle;
}

public void setFromDate(Timestamp fromDate){
this.fromDate= fromDate;
}

public Timestamp getFromDate(){
return fromDate;
}

public void setThruDate(Timestamp thruDate){
this.thruDate= thruDate;
}

public Timestamp getThruDate(){
return thruDate;
}

public void setLocation(String location){
this.location= location;
}

public String getLocation(){
return location;
}

public void setUploadedDoc(String uploadedDoc){
this.uploadedDoc= uploadedDoc;
}

public String getUploadedDoc(){
return uploadedDoc;
}

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}

	

}
