package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.Project; 
import app.appmodel.ProjectTask; 
import app.appmodel.ProjectTask; 
// importDependentEntity

@Entity
@Table(name = "ProjectStory")
public class ProjectStory {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"projectStorys"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private Project project;

private String name;
private String description;
private Timestamp startDate;
private Timestamp endDate;
private Timestamp createdDate;
@JsonIgnoreProperties({"projectStory"})
@OneToMany(targetEntity=ProjectTask.class, mappedBy = "projectStory",fetch = FetchType.LAZY)

private List<ProjectTask> projectTasks = new ArrayList();

private String document;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

public void setName(String name){
this.name= name;
}

public String getName(){
return name;
}

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}

public void setStartDate(Timestamp startDate){
this.startDate= startDate;
}

public Timestamp getStartDate(){
return startDate;
}

public void setEndDate(Timestamp endDate){
this.endDate= endDate;
}

public Timestamp getEndDate(){
return endDate;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}
    public List<ProjectTask> getProjectTasks() {
        return projectTasks;
    }

    public void setProjectTasks(List<ProjectTask> projectTasks) {
        this.projectTasks = projectTasks;
    }
public void setDocument(String document){
this.document= document;
}

public String getDocument(){
return document;
}

	

}
