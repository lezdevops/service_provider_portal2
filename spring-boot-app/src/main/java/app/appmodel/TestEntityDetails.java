package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.TestEntity; 
// importDependentEntity

@Entity
@Table(name = "TestEntityDetails")
public class TestEntityDetails {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"testEntityDetailss"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private TestEntity testEntity;

private Long number;
private String textBox;
@Column(columnDefinition="text")
private String description;
private Boolean checkbox;
private Timestamp createdDate;
private Timestamp datePicker;
private String docFile;
private String imageFile;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public TestEntity getTestEntity() {
    return testEntity;
  }

  public void setTestEntity(TestEntity testEntity) {
    this.testEntity = testEntity;
  }

public void setNumber(Long number){
this.number= number;
}

public Long getNumber(){
return number;
}

public void setTextBox(String textBox){
this.textBox= textBox;
}

public String getTextBox(){
return textBox;
}

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}

public void setCheckbox(Boolean checkbox){
this.checkbox= checkbox;
}

public Boolean getCheckbox(){
return checkbox;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}

public void setDatePicker(Timestamp datePicker){
this.datePicker= datePicker;
}

public Timestamp getDatePicker(){
return datePicker;
}

public void setDocFile(String docFile){
this.docFile= docFile;
}

public String getDocFile(){
return docFile;
}

public void setImageFile(String imageFile){
this.imageFile= imageFile;
}

public String getImageFile(){
return imageFile;
}

	

}
