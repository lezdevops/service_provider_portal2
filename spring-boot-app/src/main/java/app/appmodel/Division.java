package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.District; 
import app.appmodel.Country; 
import app.appmodel.District; 
// importDependentEntity

@Entity
@Table(name = "Division")
public class Division {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"division"})
@OneToMany(targetEntity=District.class, mappedBy = "division",fetch = FetchType.LAZY)

private List<District> districts = new ArrayList();

@JsonIgnoreProperties({"divisions"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private Country country;

private String description;
private Timestamp createdDate;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }  public Country getCountry() {
    return country;
  }

  public void setCountry(Country country) {
    this.country = country;
  }

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}

	

}
