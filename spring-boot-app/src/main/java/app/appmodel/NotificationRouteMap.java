package app.appmodel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "NotificationRouteMap")
public class NotificationRouteMap {
	@Id

	@GeneratedValue(strategy = GenerationType.AUTO)

	private Long id;
	@JsonIgnoreProperties({ "notificationRouteMaps" })
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)

	private NotificationProcess notificationProcess;

	private String senderId;
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(name = "notification_receiver_map", joinColumns = @JoinColumn(name = "notification_map_id"), inverseJoinColumns = @JoinColumn(name = "receiver_id"))
	@JsonIgnoreProperties({ "notificationRouteMap" })
	private List<MessageReceiver> messageReceivers = new ArrayList();
	
	private Boolean isActive;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public NotificationProcess getNotificationProcess() {
		return notificationProcess;
	}

	public void setNotificationProcess(NotificationProcess notificationProcess) {
		this.notificationProcess = notificationProcess;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getSenderId() {
		return senderId;
	}



	public List<MessageReceiver> getMessageReceivers() {
		return messageReceivers;
	}

	public void setMessageReceivers(List<MessageReceiver> messageReceivers) {
		this.messageReceivers = messageReceivers;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

}
