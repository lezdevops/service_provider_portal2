package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.User; 
import app.appmodel.SpLevel; 
import app.appmodel.JobHistory; 
import app.appmodel.AgentReference; 
import app.appmodel.TrainingInfo; 
import app.appmodel.DailyTask; 
import app.appmodel.EducationInfo; 
import app.appmodel.SpServiceList; 
import app.appmodel.LanguageInfo; 
import app.appmodel.ExperienceInfo; 
import app.appmodel.ServiceResponseInfo; 
import app.appmodel.OtherActivities; 
import app.appmodel.CustomerSp; 
import app.appmodel.CustomerRequest; 
import app.appmodel.JobHistory; 
import app.appmodel.AgentReference; 
import app.appmodel.TrainingInfo; 
import app.appmodel.DailyTask; 
import app.appmodel.EducationInfo; 
import app.appmodel.SpServiceList; 
import app.appmodel.LanguageInfo; 
import app.appmodel.ExperienceInfo; 
import app.appmodel.ServiceResponseInfo; 
import app.appmodel.OtherActivities; 
import app.appmodel.CustomerSp; 
import app.appmodel.CustomerRequest; 
// importDependentEntity

@Entity
@Table(name = "ServiceProvider")
public class ServiceProvider {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
private String name;
private String about;
private String address;
private String zipCode;
private String phoneNo;
private String nidNo;
private String email;
@JsonIgnoreProperties({"serviceProviders"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private User user;

@JsonIgnoreProperties({"serviceProviders"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private SpLevel spLevel;

private String gender;
private Double desiredHourlyRate;
private Double desiredDailyRate;
private Double serviceArea;
private String availability;
@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=JobHistory.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<JobHistory> jobHistorys = new ArrayList();

@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=AgentReference.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<AgentReference> agentReferences = new ArrayList();

@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=TrainingInfo.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<TrainingInfo> trainingInfos = new ArrayList();

@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=DailyTask.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<DailyTask> dailyTasks = new ArrayList();

@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=EducationInfo.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<EducationInfo> educationInfos = new ArrayList();

@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=SpServiceList.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<SpServiceList> spServiceLists = new ArrayList();

@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=LanguageInfo.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<LanguageInfo> languageInfos = new ArrayList();

@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=ExperienceInfo.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<ExperienceInfo> experienceInfos = new ArrayList();

@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=ServiceResponseInfo.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<ServiceResponseInfo> serviceResponseInfos = new ArrayList();

@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=OtherActivities.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<OtherActivities> otherActivitiess = new ArrayList();

@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=CustomerSp.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<CustomerSp> customerSps = new ArrayList();

@JsonIgnoreProperties({"serviceProvider"})
@OneToMany(targetEntity=CustomerRequest.class, mappedBy = "serviceProvider",fetch = FetchType.LAZY)

private List<CustomerRequest> customerRequests = new ArrayList();

private String isActive;
private String startTime;
private String endTime;
private Timestamp createdDate;
private String profileImage;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}

public void setName(String name){
this.name= name;
}

public String getName(){
return name;
}

public void setAbout(String about){
this.about= about;
}

public String getAbout(){
return about;
}

public void setAddress(String address){
this.address= address;
}

public String getAddress(){
return address;
}

public void setZipCode(String zipCode){
this.zipCode= zipCode;
}

public String getZipCode(){
return zipCode;
}

public void setPhoneNo(String phoneNo){
this.phoneNo= phoneNo;
}

public String getPhoneNo(){
return phoneNo;
}

public void setNidNo(String nidNo){
this.nidNo= nidNo;
}

public String getNidNo(){
return nidNo;
}

public void setEmail(String email){
this.email= email;
}

public String getEmail(){
return email;
}
  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
  public SpLevel getSpLevel() {
    return spLevel;
  }

  public void setSpLevel(SpLevel spLevel) {
    this.spLevel = spLevel;
  }

public void setGender(String gender){
this.gender= gender;
}

public String getGender(){
return gender;
}

public void setDesiredHourlyRate(Double desiredHourlyRate){
this.desiredHourlyRate= desiredHourlyRate;
}

public Double getDesiredHourlyRate(){
return desiredHourlyRate;
}

public void setDesiredDailyRate(Double desiredDailyRate){
this.desiredDailyRate= desiredDailyRate;
}

public Double getDesiredDailyRate(){
return desiredDailyRate;
}

public void setServiceArea(Double serviceArea){
this.serviceArea= serviceArea;
}

public Double getServiceArea(){
return serviceArea;
}

public void setAvailability(String availability){
this.availability= availability;
}

public String getAvailability(){
return availability;
}
    public List<JobHistory> getJobHistorys() {
        return jobHistorys;
    }

    public void setJobHistorys(List<JobHistory> jobHistorys) {
        this.jobHistorys = jobHistorys;
    }    public List<AgentReference> getAgentReferences() {
        return agentReferences;
    }

    public void setAgentReferences(List<AgentReference> agentReferences) {
        this.agentReferences = agentReferences;
    }    public List<TrainingInfo> getTrainingInfos() {
        return trainingInfos;
    }

    public void setTrainingInfos(List<TrainingInfo> trainingInfos) {
        this.trainingInfos = trainingInfos;
    }    public List<DailyTask> getDailyTasks() {
        return dailyTasks;
    }

    public void setDailyTasks(List<DailyTask> dailyTasks) {
        this.dailyTasks = dailyTasks;
    }    public List<EducationInfo> getEducationInfos() {
        return educationInfos;
    }

    public void setEducationInfos(List<EducationInfo> educationInfos) {
        this.educationInfos = educationInfos;
    }    public List<SpServiceList> getSpServiceLists() {
        return spServiceLists;
    }

    public void setSpServiceLists(List<SpServiceList> spServiceLists) {
        this.spServiceLists = spServiceLists;
    }    public List<LanguageInfo> getLanguageInfos() {
        return languageInfos;
    }

    public void setLanguageInfos(List<LanguageInfo> languageInfos) {
        this.languageInfos = languageInfos;
    }    public List<ExperienceInfo> getExperienceInfos() {
        return experienceInfos;
    }

    public void setExperienceInfos(List<ExperienceInfo> experienceInfos) {
        this.experienceInfos = experienceInfos;
    }    public List<ServiceResponseInfo> getServiceResponseInfos() {
        return serviceResponseInfos;
    }

    public void setServiceResponseInfos(List<ServiceResponseInfo> serviceResponseInfos) {
        this.serviceResponseInfos = serviceResponseInfos;
    }    public List<OtherActivities> getOtherActivitiess() {
        return otherActivitiess;
    }

    public void setOtherActivitiess(List<OtherActivities> otherActivitiess) {
        this.otherActivitiess = otherActivitiess;
    }    public List<CustomerSp> getCustomerSps() {
        return customerSps;
    }

    public void setCustomerSps(List<CustomerSp> customerSps) {
        this.customerSps = customerSps;
    }    public List<CustomerRequest> getCustomerRequests() {
        return customerRequests;
    }

    public void setCustomerRequests(List<CustomerRequest> customerRequests) {
        this.customerRequests = customerRequests;
    }
public void setIsActive(String isActive){
this.isActive= isActive;
}

public String getIsActive(){
return isActive;
}

public void setStartTime(String startTime){
this.startTime= startTime;
}

public String getStartTime(){
return startTime;
}

public void setEndTime(String endTime){
this.endTime= endTime;
}

public String getEndTime(){
return endTime;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}

public void setProfileImage(String profileImage){
this.profileImage= profileImage;
}

public String getProfileImage(){
return profileImage;
}

	

}
