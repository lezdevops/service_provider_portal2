package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ServiceProvider; 
// importDependentEntity

@Entity
@Table(name = "ServiceResponseInfo")
public class ServiceResponseInfo {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"serviceResponseInfos"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ServiceProvider serviceProvider;

private String custMessage;
private String spAnswer;
private Timestamp sentDate;
private Timestamp responseDate;
private String isActive;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public ServiceProvider getServiceProvider() {
    return serviceProvider;
  }

  public void setServiceProvider(ServiceProvider serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

public void setCustMessage(String custMessage){
this.custMessage= custMessage;
}

public String getCustMessage(){
return custMessage;
}

public void setSpAnswer(String spAnswer){
this.spAnswer= spAnswer;
}

public String getSpAnswer(){
return spAnswer;
}

public void setSentDate(Timestamp sentDate){
this.sentDate= sentDate;
}

public Timestamp getSentDate(){
return sentDate;
}

public void setResponseDate(Timestamp responseDate){
this.responseDate= responseDate;
}

public Timestamp getResponseDate(){
return responseDate;
}

public void setIsActive(String isActive){
this.isActive= isActive;
}

public String getIsActive(){
return isActive;
}

	

}
