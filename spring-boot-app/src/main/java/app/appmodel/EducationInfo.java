package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ServiceProvider; 
// importDependentEntity

@Entity
@Table(name = "EducationInfo")
public class EducationInfo {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"educationInfos"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ServiceProvider serviceProvider;

private String instituteName;
private String fromYear;
private String thruYear;
private String degree;
private String isActive;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public ServiceProvider getServiceProvider() {
    return serviceProvider;
  }

  public void setServiceProvider(ServiceProvider serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

public void setInstituteName(String instituteName){
this.instituteName= instituteName;
}

public String getInstituteName(){
return instituteName;
}

public void setFromYear(String fromYear){
this.fromYear= fromYear;
}

public String getFromYear(){
return fromYear;
}

public void setThruYear(String thruYear){
this.thruYear= thruYear;
}

public String getThruYear(){
return thruYear;
}

public void setDegree(String degree){
this.degree= degree;
}

public String getDegree(){
return degree;
}

public void setIsActive(String isActive){
this.isActive= isActive;
}

public String getIsActive(){
return isActive;
}

	

}
