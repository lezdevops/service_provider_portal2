package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ServiceProvider; 
// importDependentEntity

@Entity
@Table(name = "DailyTask")
public class DailyTask {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"dailyTasks"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ServiceProvider serviceProvider;

private Timestamp fromDate;
private Timestamp thruDate;
private String startTime;
private String endTime;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public ServiceProvider getServiceProvider() {
    return serviceProvider;
  }

  public void setServiceProvider(ServiceProvider serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

public void setFromDate(Timestamp fromDate){
this.fromDate= fromDate;
}

public Timestamp getFromDate(){
return fromDate;
}

public void setThruDate(Timestamp thruDate){
this.thruDate= thruDate;
}

public Timestamp getThruDate(){
return thruDate;
}

public void setStartTime(String startTime){
this.startTime= startTime;
}

public String getStartTime(){
return startTime;
}

public void setEndTime(String endTime){
this.endTime= endTime;
}

public String getEndTime(){
return endTime;
}

	

}
