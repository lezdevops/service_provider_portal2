package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.Division; 
import app.appmodel.Division; 
// importDependentEntity

@Entity
@Table(name = "Country")
public class Country {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"country"})
@OneToMany(targetEntity=Division.class, mappedBy = "country",fetch = FetchType.LAZY)

private List<Division> divisions = new ArrayList();

private String currency;
private String description;
private Timestamp createdDate;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
    public List<Division> getDivisions() {
        return divisions;
    }

    public void setDivisions(List<Division> divisions) {
        this.divisions = divisions;
    }
public void setCurrency(String currency){
this.currency= currency;
}

public String getCurrency(){
return currency;
}

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}

	

}
