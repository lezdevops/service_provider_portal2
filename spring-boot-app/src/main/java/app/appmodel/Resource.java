package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ProjectSubtask; 
import app.appmodel.ProjectSubtask; 
// importDependentEntity

@Entity
@Table(name = "Resource")
public class Resource {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
private String name;
private String description;
@JsonIgnoreProperties({"resource"})
@OneToMany(targetEntity=ProjectSubtask.class, mappedBy = "resource",fetch = FetchType.LAZY)

private List<ProjectSubtask> projectSubtasks = new ArrayList();

private String document;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}

public void setName(String name){
this.name= name;
}

public String getName(){
return name;
}

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}
    public List<ProjectSubtask> getProjectSubtasks() {
        return projectSubtasks;
    }

    public void setProjectSubtasks(List<ProjectSubtask> projectSubtasks) {
        this.projectSubtasks = projectSubtasks;
    }
public void setDocument(String document){
this.document= document;
}

public String getDocument(){
return document;
}

	

}
