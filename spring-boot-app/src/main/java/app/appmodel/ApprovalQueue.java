package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ApprovalProcess; 
import app.appmodel.ApprovalQueueDetails; 
import app.appmodel.ApprovalQueueDetails; 
// importDependentEntity

@Entity
@Table(name = "ApprovalQueue")
public class ApprovalQueue {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"approvalQueues"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ApprovalProcess approvalProcess;

private Boolean isActive;
@JsonIgnoreProperties({"approvalQueue"})
@OneToMany(targetEntity=ApprovalQueueDetails.class, mappedBy = "approvalQueue",fetch = FetchType.LAZY)

private List<ApprovalQueueDetails> approvalQueueDetailss = new ArrayList();

private String attachment;
@Column(columnDefinition = "text")
private String approvalMessage;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public ApprovalProcess getApprovalProcess() {
    return approvalProcess;
  }

  public void setApprovalProcess(ApprovalProcess approvalProcess) {
    this.approvalProcess = approvalProcess;
  }

public void setIsActive(Boolean isActive){
this.isActive= isActive;
}

public Boolean getIsActive(){
return isActive;
}
    public List<ApprovalQueueDetails> getApprovalQueueDetailss() {
        return approvalQueueDetailss;
    }

    public void setApprovalQueueDetailss(List<ApprovalQueueDetails> approvalQueueDetailss) {
        this.approvalQueueDetailss = approvalQueueDetailss;
    }
public void setAttachment(String attachment){
this.attachment= attachment;
}

public String getAttachment(){
return attachment;
}

public void setApprovalMessage(String approvalMessage){
this.approvalMessage= approvalMessage;
}

public String getApprovalMessage(){
return approvalMessage;
}

	

}
