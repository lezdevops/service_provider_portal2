package app.appmodel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
// importDependentEntity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "ApproverAdmin")
public class ApproverAdmin {
	@Id

	@GeneratedValue(strategy = GenerationType.AUTO)

	private Long id;
	private String name;
	private String itemName;
	private String designation;
	private Boolean isActive;
	@JsonIgnoreProperties({ "approvalProcesss" })
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	private ApprovalProcess approvalProcess;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDesignation() {
		return designation;
	}


	public ApprovalProcess getApprovalProcess() {
		return approvalProcess;
	}

	public void setApprovalProcess(ApprovalProcess approvalProcess) {
		this.approvalProcess = approvalProcess;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

}
