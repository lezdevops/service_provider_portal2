package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.Thana; 
import app.appmodel.Division; 
import app.appmodel.Thana; 
// importDependentEntity

@Entity
@Table(name = "District")
public class District {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"district"})
@OneToMany(targetEntity=Thana.class, mappedBy = "district",fetch = FetchType.LAZY)

private List<Thana> thanas = new ArrayList();

@JsonIgnoreProperties({"districts"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private Division division;

private String description;
private Timestamp createdDate;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
    public List<Thana> getThanas() {
        return thanas;
    }

    public void setThanas(List<Thana> thanas) {
        this.thanas = thanas;
    }  public Division getDivision() {
    return division;
  }

  public void setDivision(Division division) {
    this.division = division;
  }

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}

	

}
