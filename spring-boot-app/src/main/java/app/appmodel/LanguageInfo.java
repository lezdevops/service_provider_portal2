package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ServiceProvider; 
// importDependentEntity

@Entity
@Table(name = "LanguageInfo")
public class LanguageInfo {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"languageInfos"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ServiceProvider serviceProvider;

private String languageName;
private String shortCode;
private String isActive;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public ServiceProvider getServiceProvider() {
    return serviceProvider;
  }

  public void setServiceProvider(ServiceProvider serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

public void setLanguageName(String languageName){
this.languageName= languageName;
}

public String getLanguageName(){
return languageName;
}

public void setShortCode(String shortCode){
this.shortCode= shortCode;
}

public String getShortCode(){
return shortCode;
}

public void setIsActive(String isActive){
this.isActive= isActive;
}

public String getIsActive(){
return isActive;
}

	

}
