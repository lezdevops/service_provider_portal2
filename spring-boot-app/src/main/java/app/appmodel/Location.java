package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.Thana; 
import app.appmodel.SpServiceList; 
import app.appmodel.SpServiceList; 
// importDependentEntity

@Entity
@Table(name = "Location")
public class Location {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"locations"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private Thana thana;

@JsonIgnoreProperties({"location"})
@OneToMany(targetEntity=SpServiceList.class, mappedBy = "location",fetch = FetchType.LAZY)

private List<SpServiceList> spServiceLists = new ArrayList();

private String description;
private Timestamp createdDate;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public Thana getThana() {
    return thana;
  }

  public void setThana(Thana thana) {
    this.thana = thana;
  }
    public List<SpServiceList> getSpServiceLists() {
        return spServiceLists;
    }

    public void setSpServiceLists(List<SpServiceList> spServiceLists) {
        this.spServiceLists = spServiceLists;
    }
public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}

	

}
