package app.appmodel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
// importDependentEntity

@Entity
@Table(name = "MessageReceiver")
public class MessageReceiver {
	@Id

	@GeneratedValue(strategy = GenerationType.AUTO)

	private Long id;
	private String name;
	private String itemName;

	@JsonIgnoreProperties({ "messageReceivers" })
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	private NotificationRouteMap notificationRouteMap;
	
	private String designation;

	private Boolean isActive;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDesignation() {
		return designation;
	}


	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public NotificationRouteMap getNotificationRouteMap() {
		return notificationRouteMap;
	}

	public void setNotificationRouteMap(NotificationRouteMap notificationRouteMap) {
		this.notificationRouteMap = notificationRouteMap;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

}
