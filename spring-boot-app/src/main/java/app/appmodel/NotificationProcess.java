package app.appmodel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "NotificationProcess")
public class NotificationProcess {
	@Id

	@GeneratedValue(strategy = GenerationType.AUTO)

	private Long id;
	private String name;
	@JsonIgnoreProperties({ "notificationProcess" })
	@OneToMany(targetEntity = NotificationRouteMap.class, mappedBy = "notificationProcess", fetch = FetchType.LAZY)

	private List<NotificationRouteMap> notificationRouteMaps = new ArrayList();

	@JsonIgnoreProperties({ "notificationProcess" })
	@OneToMany(targetEntity = Notification.class, mappedBy = "notificationProcess", fetch = FetchType.LAZY)

	private List<Notification> notifications = new ArrayList();

	private Boolean isActive;
	private Boolean sendEmail;
	private Boolean sendSms;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public List<NotificationRouteMap> getNotificationRouteMaps() {
		return notificationRouteMaps;
	}

	public void setNotificationRouteMaps(List<NotificationRouteMap> notificationRouteMaps) {
		this.notificationRouteMaps = notificationRouteMaps;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setSendEmail(Boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	public Boolean getSendEmail() {
		return sendEmail;
	}

	public void setSendSms(Boolean sendSms) {
		this.sendSms = sendSms;
	}

	public Boolean getSendSms() {
		return sendSms;
	}

}
