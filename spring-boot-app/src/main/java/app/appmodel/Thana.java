package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.Location; 
import app.appmodel.District; 
import app.appmodel.Location; 
// importDependentEntity

@Entity
@Table(name = "Thana")
public class Thana {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"thana"})
@OneToMany(targetEntity=Location.class, mappedBy = "thana",fetch = FetchType.LAZY)

private List<Location> locations = new ArrayList();

@JsonIgnoreProperties({"thanas"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private District district;

private String description;
private Timestamp createdDate;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }  public District getDistrict() {
    return district;
  }

  public void setDistrict(District district) {
    this.district = district;
  }

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}

	

}
