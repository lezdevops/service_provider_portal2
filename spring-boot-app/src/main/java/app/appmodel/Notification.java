package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import app.appmodel.NotificationProcess;
import app.appmodel.NotificationDetails;
import app.appmodel.NotificationDetails;
// importDependentEntity

@Entity
@Table(name = "Notification")
public class Notification {
	@Id

	@GeneratedValue(strategy = GenerationType.AUTO)

	private Long id;
	@JsonIgnoreProperties({ "notifications" })
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)

	private NotificationProcess notificationProcess;
	@Column(columnDefinition = "text")
	private String message;
	@Column(columnDefinition = "text")
	private String oldObjectVal;
	@Column(columnDefinition = "text")
	private String newObjectVal;
	private String attachment;
	@JsonIgnoreProperties({ "notification" })
	@OneToMany(targetEntity = NotificationDetails.class, mappedBy = "notification", fetch = FetchType.LAZY)

	private List<NotificationDetails> notificationDetailss = new ArrayList();

	private String url;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public NotificationProcess getNotificationProcess() {
		return notificationProcess;
	}

	public void setNotificationProcess(NotificationProcess notificationProcess) {
		this.notificationProcess = notificationProcess;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setOldObjectVal(String oldObjectVal) {
		this.oldObjectVal = oldObjectVal;
	}

	public String getOldObjectVal() {
		return oldObjectVal;
	}

	public void setNewObjectVal(String newObjectVal) {
		this.newObjectVal = newObjectVal;
	}

	public String getNewObjectVal() {
		return newObjectVal;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getAttachment() {
		return attachment;
	}

	public List<NotificationDetails> getNotificationDetailss() {
		return notificationDetailss;
	}

	public void setNotificationDetailss(List<NotificationDetails> notificationDetailss) {
		this.notificationDetailss = notificationDetailss;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

}
