package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ApprovalQueue; 
// importDependentEntity

@Entity
@Table(name = "ApprovalQueueDetails")
public class ApprovalQueueDetails {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"approvalQueueDetailss"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ApprovalQueue approvalQueue;

private String approverId;
private Long level;
private Boolean isLastApprover;
private Boolean isApproved;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public ApprovalQueue getApprovalQueue() {
    return approvalQueue;
  }

  public void setApprovalQueue(ApprovalQueue approvalQueue) {
    this.approvalQueue = approvalQueue;
  }

public void setApproverId(String approverId){
this.approverId= approverId;
}

public String getApproverId(){
return approverId;
}

public void setLevel(Long level){
this.level= level;
}

public Long getLevel(){
return level;
}

public void setIsLastApprover(Boolean isLastApprover){
this.isLastApprover= isLastApprover;
}

public Boolean getIsLastApprover(){
return isLastApprover;
}

public void setIsApproved(Boolean isApproved){
this.isApproved= isApproved;
}

public Boolean getIsApproved(){
return isApproved;
}

	

}
