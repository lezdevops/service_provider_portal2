package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.ProjectTask; 
import app.appmodel.Resource; 
import app.appmodel.TaskState; 
// importDependentEntity

@Entity
@Table(name = "ProjectSubtask")
public class ProjectSubtask {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
@JsonIgnoreProperties({"projectSubtasks"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private ProjectTask projectTask;

private String name;
private String description;
private Timestamp startDate;
private Timestamp endDate;
private Timestamp createdDate;
@JsonIgnoreProperties({"projectSubtasks"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private Resource resource;

@JsonIgnoreProperties({"projectSubtasks"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private TaskState taskState;

private String completed;
private String document;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}
  public ProjectTask getProjectTask() {
    return projectTask;
  }

  public void setProjectTask(ProjectTask projectTask) {
    this.projectTask = projectTask;
  }

public void setName(String name){
this.name= name;
}

public String getName(){
return name;
}

public void setDescription(String description){
this.description= description;
}

public String getDescription(){
return description;
}

public void setStartDate(Timestamp startDate){
this.startDate= startDate;
}

public Timestamp getStartDate(){
return startDate;
}

public void setEndDate(Timestamp endDate){
this.endDate= endDate;
}

public Timestamp getEndDate(){
return endDate;
}

public void setCreatedDate(Timestamp createdDate){
this.createdDate= createdDate;
}

public Timestamp getCreatedDate(){
return createdDate;
}
  public Resource getResource() {
    return resource;
  }

  public void setResource(Resource resource) {
    this.resource = resource;
  }
  public TaskState getTaskState() {
    return taskState;
  }

  public void setTaskState(TaskState taskState) {
    this.taskState = taskState;
  }

public void setCompleted(String completed){
this.completed= completed;
}

public String getCompleted(){
return completed;
}

public void setDocument(String document){
this.document= document;
}

public String getDocument(){
return document;
}

	

}
