package app.appmodel;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 
import app.appmodel.User; 
// importDependentEntity

@Entity
@Table(name = "UserGroup")
public class UserGroup {
	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)

	
	
private Long id;
private String name;
private String metaData;
private String websiteAddress;
private String phoneNo;
private String email;
private String address;
private String currency;
private String imageAddress;
@JsonIgnoreProperties({"userGroups"})
@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)

private User user;

private Timestamp created;
private Timestamp updated;

	
	
	
public void setId(Long id){
this.id= id;
}

public Long getId(){
return id;
}

public void setName(String name){
this.name= name;
}

public String getName(){
return name;
}

public void setMetaData(String metaData){
this.metaData= metaData;
}

public String getMetaData(){
return metaData;
}

public void setWebsiteAddress(String websiteAddress){
this.websiteAddress= websiteAddress;
}

public String getWebsiteAddress(){
return websiteAddress;
}

public void setPhoneNo(String phoneNo){
this.phoneNo= phoneNo;
}

public String getPhoneNo(){
return phoneNo;
}

public void setEmail(String email){
this.email= email;
}

public String getEmail(){
return email;
}

public void setAddress(String address){
this.address= address;
}

public String getAddress(){
return address;
}

public void setCurrency(String currency){
this.currency= currency;
}

public String getCurrency(){
return currency;
}

public void setImageAddress(String imageAddress){
this.imageAddress= imageAddress;
}

public String getImageAddress(){
return imageAddress;
}
  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

public void setCreated(Timestamp created){
this.created= created;
}

public Timestamp getCreated(){
return created;
}

public void setUpdated(Timestamp updated){
this.updated= updated;
}

public Timestamp getUpdated(){
return updated;
}

	

}
