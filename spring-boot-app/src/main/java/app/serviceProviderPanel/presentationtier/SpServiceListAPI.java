package app.serviceProviderPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.SpServiceList;
import app.framework.businesstier.HelperUtils;
import app.serviceProviderPanel.businesstier.SpServiceListManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules SpServiceList API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class SpServiceListAPI {

	@Autowired
	private SpServiceListManager spServiceListManager;

	@PostMapping("/spServiceList")
	public Map<String,Object> createSpServiceList(@RequestBody SpServiceList spServiceList) {
		spServiceList.setServiceProvider(null);

		
		return spServiceListManager.save(spServiceList);
	}
	@GetMapping("/spServiceList/{id}")
	public SpServiceList getSpServiceList(@PathVariable Long id) {
		return spServiceListManager.findOne(id);
	}
	@GetMapping(value = "/spServiceLists")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<SpServiceList> getAll() {
		return spServiceListManager.getSpServiceLists();
	}

	@PutMapping("/spServiceList")
	public Map<String,Object> updateSpServiceList(@RequestBody SpServiceList spServiceList) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			SpServiceList newSpServiceList = getSpServiceList(spServiceList.getId());
			newSpServiceList.setServiceDetails(spServiceList.getServiceDetails());
newSpServiceList.setVisitCount(spServiceList.getVisitCount());
newSpServiceList.setRequestCount(spServiceList.getRequestCount());
newSpServiceList.setStartTime(spServiceList.getStartTime());
newSpServiceList.setEndTime(spServiceList.getEndTime());
newSpServiceList.setIsActive(spServiceList.getIsActive());
newSpServiceList.setAvailFrom(spServiceList.getAvailFrom());
newSpServiceList.setAvailTo(spServiceList.getAvailTo());

			return spServiceListManager.save(newSpServiceList);
		}
		
	}
	
	@DeleteMapping("/spServiceList/{id}")
	public boolean deleteSpServiceList(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		spServiceListManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/spServiceList/saveMultipleSpServiceListData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<SpServiceList> saveMultipleSpServiceListData(@RequestBody List<SpServiceList> spServiceLists) {
		return spServiceListManager.saveMultipleSpServiceListData(spServiceLists);
	}

	@RequestMapping(value = "/spServiceList/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = spServiceListManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/spServiceList/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return spServiceListManager.removeAll();
	}
	@RequestMapping(value = "/spServiceList/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<SpServiceList> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<SpServiceList> resultPage = spServiceListManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/spServiceList/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<SpServiceList> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<SpServiceList> resultPage = spServiceListManager.findPaginatedSpServiceListsBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/spServiceList/searchOnSpServiceListTable", method = RequestMethod.GET)
    public Page<SpServiceList> searchOnSpServiceListTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return spServiceListManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/spServiceList/processExcel2003", method = RequestMethod.POST)
	public List<SpServiceList> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<SpServiceList> spServiceLists = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				SpServiceList spServiceList = new SpServiceList();
				HSSFRow row = worksheet.getRow(i++);
				//spServiceList.setField2(row.getCell(1).getStringCellValue());
				spServiceList.setServiceDetails(row.getCell(0).getStringCellValue());
spServiceList.setVisitCount(Double.valueOf(row.getCell(1).getNumericCellValue()).longValue());
spServiceList.setRequestCount(Double.valueOf(row.getCell(2).getNumericCellValue()).longValue());
spServiceList.setStartTime(row.getCell(3).getStringCellValue());
spServiceList.setEndTime(row.getCell(4).getStringCellValue());
spServiceList.setIsActive(row.getCell(5).getStringCellValue());
spServiceList.setAvailFrom(HelperUtils.getTimestampFromString(row.getCell(6).getStringCellValue()));
spServiceList.setAvailTo(HelperUtils.getTimestampFromString(row.getCell(7).getStringCellValue()));

				spServiceLists.add(spServiceList);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return spServiceLists;
	}

	@RequestMapping(value = "/spServiceList/processExcel2007", method = RequestMethod.POST)
	public List<SpServiceList> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<SpServiceList> spServiceLists = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				SpServiceList spServiceList = new SpServiceList();
				XSSFRow row = worksheet.getRow(i++);
				//spServiceList.setField2(row.getCell(1).getStringCellValue());
				spServiceList.setServiceDetails(row.getCell(0).getStringCellValue());
spServiceList.setVisitCount(Double.valueOf(row.getCell(1).getNumericCellValue()).longValue());
spServiceList.setRequestCount(Double.valueOf(row.getCell(2).getNumericCellValue()).longValue());
spServiceList.setStartTime(row.getCell(3).getStringCellValue());
spServiceList.setEndTime(row.getCell(4).getStringCellValue());
spServiceList.setIsActive(row.getCell(5).getStringCellValue());
spServiceList.setAvailFrom(HelperUtils.getTimestampFromString(row.getCell(6).getStringCellValue()));
spServiceList.setAvailTo(HelperUtils.getTimestampFromString(row.getCell(7).getStringCellValue()));

				spServiceLists.add(spServiceList);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return spServiceLists;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/spServiceList/excelFileUpload")
	@ResponseBody
	public List<SpServiceList> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<SpServiceList> spServiceLists = new ArrayList<SpServiceList>();
		if (extension.equalsIgnoreCase("xlsx")) {
			spServiceLists = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			spServiceLists = processExcel2003(file);
		}
		
		if (spServiceLists.size()>0) {
			spServiceLists.remove(0);
		}
		return spServiceLists;
	}
}
