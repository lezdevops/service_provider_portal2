package app.serviceProviderPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.JobHistory;
import app.framework.businesstier.HelperUtils;
import app.serviceProviderPanel.businesstier.JobHistoryManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules JobHistory API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class JobHistoryAPI {

	@Autowired
	private JobHistoryManager jobHistoryManager;

	@PostMapping("/jobHistory")
	public Map<String,Object> createJobHistory(@RequestBody JobHistory jobHistory) {
		jobHistory.setServiceProvider(null);

		
		return jobHistoryManager.save(jobHistory);
	}
	@GetMapping("/jobHistory/{id}")
	public JobHistory getJobHistory(@PathVariable Long id) {
		return jobHistoryManager.findOne(id);
	}
	@GetMapping(value = "/jobHistorys")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<JobHistory> getAll() {
		return jobHistoryManager.getJobHistorys();
	}

	@PutMapping("/jobHistory")
	public Map<String,Object> updateJobHistory(@RequestBody JobHistory jobHistory) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			JobHistory newJobHistory = getJobHistory(jobHistory.getId());
			newJobHistory.setEmployerName(jobHistory.getEmployerName());
newJobHistory.setJobTitle(jobHistory.getJobTitle());
newJobHistory.setFromDate(jobHistory.getFromDate());
newJobHistory.setThruDate(jobHistory.getThruDate());
newJobHistory.setLocation(jobHistory.getLocation());
newJobHistory.setUploadedDoc(jobHistory.getUploadedDoc());
newJobHistory.setDescription(jobHistory.getDescription());

			return jobHistoryManager.save(newJobHistory);
		}
		
	}
	
	@DeleteMapping("/jobHistory/{id}")
	public boolean deleteJobHistory(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		jobHistoryManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/jobHistory/saveMultipleJobHistoryData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<JobHistory> saveMultipleJobHistoryData(@RequestBody List<JobHistory> jobHistorys) {
		return jobHistoryManager.saveMultipleJobHistoryData(jobHistorys);
	}

	@RequestMapping(value = "/jobHistory/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = jobHistoryManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/jobHistory/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return jobHistoryManager.removeAll();
	}
	@RequestMapping(value = "/jobHistory/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<JobHistory> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<JobHistory> resultPage = jobHistoryManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/jobHistory/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<JobHistory> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<JobHistory> resultPage = jobHistoryManager.findPaginatedJobHistorysBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/jobHistory/searchOnJobHistoryTable", method = RequestMethod.GET)
    public Page<JobHistory> searchOnJobHistoryTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return jobHistoryManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/jobHistory/processExcel2003", method = RequestMethod.POST)
	public List<JobHistory> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<JobHistory> jobHistorys = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				JobHistory jobHistory = new JobHistory();
				HSSFRow row = worksheet.getRow(i++);
				//jobHistory.setField2(row.getCell(1).getStringCellValue());
				jobHistory.setEmployerName(row.getCell(0).getStringCellValue());
jobHistory.setJobTitle(row.getCell(1).getStringCellValue());
jobHistory.setFromDate(HelperUtils.getTimestampFromString(row.getCell(2).getStringCellValue()));
jobHistory.setThruDate(HelperUtils.getTimestampFromString(row.getCell(3).getStringCellValue()));
jobHistory.setLocation(row.getCell(4).getStringCellValue());
jobHistory.setUploadedDoc(row.getCell(5).getStringCellValue());
jobHistory.setDescription(row.getCell(6).getStringCellValue());

				jobHistorys.add(jobHistory);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return jobHistorys;
	}

	@RequestMapping(value = "/jobHistory/processExcel2007", method = RequestMethod.POST)
	public List<JobHistory> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<JobHistory> jobHistorys = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				JobHistory jobHistory = new JobHistory();
				XSSFRow row = worksheet.getRow(i++);
				//jobHistory.setField2(row.getCell(1).getStringCellValue());
				jobHistory.setEmployerName(row.getCell(0).getStringCellValue());
jobHistory.setJobTitle(row.getCell(1).getStringCellValue());
jobHistory.setFromDate(HelperUtils.getTimestampFromString(row.getCell(2).getStringCellValue()));
jobHistory.setThruDate(HelperUtils.getTimestampFromString(row.getCell(3).getStringCellValue()));
jobHistory.setLocation(row.getCell(4).getStringCellValue());
jobHistory.setUploadedDoc(row.getCell(5).getStringCellValue());
jobHistory.setDescription(row.getCell(6).getStringCellValue());

				jobHistorys.add(jobHistory);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jobHistorys;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/jobHistory/excelFileUpload")
	@ResponseBody
	public List<JobHistory> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<JobHistory> jobHistorys = new ArrayList<JobHistory>();
		if (extension.equalsIgnoreCase("xlsx")) {
			jobHistorys = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			jobHistorys = processExcel2003(file);
		}
		
		if (jobHistorys.size()>0) {
			jobHistorys.remove(0);
		}
		return jobHistorys;
	}
}
