package app.serviceProviderPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.LanguageInfo;
import app.framework.businesstier.HelperUtils;
import app.serviceProviderPanel.businesstier.LanguageInfoManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules LanguageInfo API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class LanguageInfoAPI {

	@Autowired
	private LanguageInfoManager languageInfoManager;

	@PostMapping("/languageInfo")
	public Map<String,Object> createLanguageInfo(@RequestBody LanguageInfo languageInfo) {
		languageInfo.setServiceProvider(null);

		
		return languageInfoManager.save(languageInfo);
	}
	@GetMapping("/languageInfo/{id}")
	public LanguageInfo getLanguageInfo(@PathVariable Long id) {
		return languageInfoManager.findOne(id);
	}
	@GetMapping(value = "/languageInfos")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<LanguageInfo> getAll() {
		return languageInfoManager.getLanguageInfos();
	}

	@PutMapping("/languageInfo")
	public Map<String,Object> updateLanguageInfo(@RequestBody LanguageInfo languageInfo) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			LanguageInfo newLanguageInfo = getLanguageInfo(languageInfo.getId());
			newLanguageInfo.setLanguageName(languageInfo.getLanguageName());
newLanguageInfo.setShortCode(languageInfo.getShortCode());
newLanguageInfo.setIsActive(languageInfo.getIsActive());

			return languageInfoManager.save(newLanguageInfo);
		}
		
	}
	
	@DeleteMapping("/languageInfo/{id}")
	public boolean deleteLanguageInfo(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		languageInfoManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/languageInfo/saveMultipleLanguageInfoData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<LanguageInfo> saveMultipleLanguageInfoData(@RequestBody List<LanguageInfo> languageInfos) {
		return languageInfoManager.saveMultipleLanguageInfoData(languageInfos);
	}

	@RequestMapping(value = "/languageInfo/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = languageInfoManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/languageInfo/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return languageInfoManager.removeAll();
	}
	@RequestMapping(value = "/languageInfo/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<LanguageInfo> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<LanguageInfo> resultPage = languageInfoManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/languageInfo/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<LanguageInfo> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<LanguageInfo> resultPage = languageInfoManager.findPaginatedLanguageInfosBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/languageInfo/searchOnLanguageInfoTable", method = RequestMethod.GET)
    public Page<LanguageInfo> searchOnLanguageInfoTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return languageInfoManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/languageInfo/processExcel2003", method = RequestMethod.POST)
	public List<LanguageInfo> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<LanguageInfo> languageInfos = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				LanguageInfo languageInfo = new LanguageInfo();
				HSSFRow row = worksheet.getRow(i++);
				//languageInfo.setField2(row.getCell(1).getStringCellValue());
				languageInfo.setLanguageName(row.getCell(0).getStringCellValue());
languageInfo.setShortCode(row.getCell(1).getStringCellValue());
languageInfo.setIsActive(row.getCell(2).getStringCellValue());

				languageInfos.add(languageInfo);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return languageInfos;
	}

	@RequestMapping(value = "/languageInfo/processExcel2007", method = RequestMethod.POST)
	public List<LanguageInfo> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<LanguageInfo> languageInfos = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				LanguageInfo languageInfo = new LanguageInfo();
				XSSFRow row = worksheet.getRow(i++);
				//languageInfo.setField2(row.getCell(1).getStringCellValue());
				languageInfo.setLanguageName(row.getCell(0).getStringCellValue());
languageInfo.setShortCode(row.getCell(1).getStringCellValue());
languageInfo.setIsActive(row.getCell(2).getStringCellValue());

				languageInfos.add(languageInfo);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return languageInfos;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/languageInfo/excelFileUpload")
	@ResponseBody
	public List<LanguageInfo> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<LanguageInfo> languageInfos = new ArrayList<LanguageInfo>();
		if (extension.equalsIgnoreCase("xlsx")) {
			languageInfos = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			languageInfos = processExcel2003(file);
		}
		
		if (languageInfos.size()>0) {
			languageInfos.remove(0);
		}
		return languageInfos;
	}
}
