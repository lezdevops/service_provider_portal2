package app.serviceProviderPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.EducationInfo;
import app.framework.businesstier.HelperUtils;
import app.serviceProviderPanel.businesstier.EducationInfoManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules EducationInfo API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class EducationInfoAPI {

	@Autowired
	private EducationInfoManager educationInfoManager;

	@PostMapping("/educationInfo")
	public Map<String,Object> createEducationInfo(@RequestBody EducationInfo educationInfo) {
		educationInfo.setServiceProvider(null);

		
		return educationInfoManager.save(educationInfo);
	}
	@GetMapping("/educationInfo/{id}")
	public EducationInfo getEducationInfo(@PathVariable Long id) {
		return educationInfoManager.findOne(id);
	}
	@GetMapping(value = "/educationInfos")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<EducationInfo> getAll() {
		return educationInfoManager.getEducationInfos();
	}

	@PutMapping("/educationInfo")
	public Map<String,Object> updateEducationInfo(@RequestBody EducationInfo educationInfo) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			EducationInfo newEducationInfo = getEducationInfo(educationInfo.getId());
			newEducationInfo.setInstituteName(educationInfo.getInstituteName());
newEducationInfo.setFromYear(educationInfo.getFromYear());
newEducationInfo.setThruYear(educationInfo.getThruYear());
newEducationInfo.setDegree(educationInfo.getDegree());
newEducationInfo.setIsActive(educationInfo.getIsActive());

			return educationInfoManager.save(newEducationInfo);
		}
		
	}
	
	@DeleteMapping("/educationInfo/{id}")
	public boolean deleteEducationInfo(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		educationInfoManager.remove(id);
		return true;
		}
	}

	@RequestMapping(value = "/educationInfo/saveMultipleEducationInfoData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<EducationInfo> saveMultipleEducationInfoData(@RequestBody List<EducationInfo> educationInfos) {
		return educationInfoManager.saveMultipleEducationInfoData(educationInfos);
	}

	@RequestMapping(value = "/educationInfo/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = educationInfoManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/educationInfo/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return educationInfoManager.removeAll();
	}
	@RequestMapping(value = "/educationInfo/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<EducationInfo> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<EducationInfo> resultPage = educationInfoManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/educationInfo/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<EducationInfo> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<EducationInfo> resultPage = educationInfoManager.findPaginatedEducationInfosBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/educationInfo/searchOnEducationInfoTable", method = RequestMethod.GET)
    public Page<EducationInfo> searchOnEducationInfoTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return educationInfoManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/educationInfo/processExcel2003", method = RequestMethod.POST)
	public List<EducationInfo> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<EducationInfo> educationInfos = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				EducationInfo educationInfo = new EducationInfo();
				HSSFRow row = worksheet.getRow(i++);
				//educationInfo.setField2(row.getCell(1).getStringCellValue());
				educationInfo.setInstituteName(row.getCell(0).getStringCellValue());
educationInfo.setFromYear(row.getCell(1).getStringCellValue());
educationInfo.setThruYear(row.getCell(2).getStringCellValue());
educationInfo.setDegree(row.getCell(3).getStringCellValue());
educationInfo.setIsActive(row.getCell(4).getStringCellValue());

				educationInfos.add(educationInfo);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return educationInfos;
	}

	@RequestMapping(value = "/educationInfo/processExcel2007", method = RequestMethod.POST)
	public List<EducationInfo> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<EducationInfo> educationInfos = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				EducationInfo educationInfo = new EducationInfo();
				XSSFRow row = worksheet.getRow(i++);
				//educationInfo.setField2(row.getCell(1).getStringCellValue());
				educationInfo.setInstituteName(row.getCell(0).getStringCellValue());
educationInfo.setFromYear(row.getCell(1).getStringCellValue());
educationInfo.setThruYear(row.getCell(2).getStringCellValue());
educationInfo.setDegree(row.getCell(3).getStringCellValue());
educationInfo.setIsActive(row.getCell(4).getStringCellValue());

				educationInfos.add(educationInfo);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return educationInfos;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/educationInfo/excelFileUpload")
	@ResponseBody
	public List<EducationInfo> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<EducationInfo> educationInfos = new ArrayList<EducationInfo>();
		if (extension.equalsIgnoreCase("xlsx")) {
			educationInfos = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			educationInfos = processExcel2003(file);
		}
		
		if (educationInfos.size()>0) {
			educationInfos.remove(0);
		}
		return educationInfos;
	}
}
