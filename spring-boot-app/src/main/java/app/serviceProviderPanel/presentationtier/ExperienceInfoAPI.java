package app.serviceProviderPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.ExperienceInfo;
import app.framework.businesstier.HelperUtils;
import app.serviceProviderPanel.businesstier.ExperienceInfoManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules ExperienceInfo API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class ExperienceInfoAPI {

	@Autowired
	private ExperienceInfoManager experienceInfoManager;

	@PostMapping("/experienceInfo")
	public Map<String,Object> createExperienceInfo(@RequestBody ExperienceInfo experienceInfo) {
		

		
		return experienceInfoManager.save(experienceInfo);
	}
	@GetMapping("/experienceInfo/{id}")
	public ExperienceInfo getExperienceInfo(@PathVariable Long id) {
		return experienceInfoManager.findOne(id);
	}
	@GetMapping(value = "/experienceInfos")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<ExperienceInfo> getAll() {
		return experienceInfoManager.getExperienceInfos();
	}

	@PutMapping("/experienceInfo")
	public Map<String,Object> updateExperienceInfo(@RequestBody ExperienceInfo experienceInfo) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			ExperienceInfo newExperienceInfo = getExperienceInfo(experienceInfo.getId());
			newExperienceInfo.setFromDate(experienceInfo.getFromDate());
newExperienceInfo.setThruDate(experienceInfo.getThruDate());
newExperienceInfo.setDescription(experienceInfo.getDescription());
newExperienceInfo.setIsActive(experienceInfo.getIsActive());

			return experienceInfoManager.save(newExperienceInfo);
		}
		
	}
	
	@DeleteMapping("/experienceInfo/{id}")
	public boolean deleteExperienceInfo(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		experienceInfoManager.remove(id);
		return true;
		}
	}

	@RequestMapping(value = "/experienceInfo/saveMultipleExperienceInfoData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<ExperienceInfo> saveMultipleExperienceInfoData(@RequestBody List<ExperienceInfo> experienceInfos) {
		return experienceInfoManager.saveMultipleExperienceInfoData(experienceInfos);
	}

	@RequestMapping(value = "/experienceInfo/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = experienceInfoManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/experienceInfo/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return experienceInfoManager.removeAll();
	}
	@RequestMapping(value = "/experienceInfo/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<ExperienceInfo> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<ExperienceInfo> resultPage = experienceInfoManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/experienceInfo/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<ExperienceInfo> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<ExperienceInfo> resultPage = experienceInfoManager.findPaginatedExperienceInfosBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/experienceInfo/searchOnExperienceInfoTable", method = RequestMethod.GET)
    public Page<ExperienceInfo> searchOnExperienceInfoTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return experienceInfoManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/experienceInfo/processExcel2003", method = RequestMethod.POST)
	public List<ExperienceInfo> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<ExperienceInfo> experienceInfos = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ExperienceInfo experienceInfo = new ExperienceInfo();
				HSSFRow row = worksheet.getRow(i++);
				//experienceInfo.setField2(row.getCell(1).getStringCellValue());
				experienceInfo.setFromDate(HelperUtils.getTimestampFromString(row.getCell(0).getStringCellValue()));
experienceInfo.setThruDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));
experienceInfo.setDescription(row.getCell(2).getStringCellValue());
experienceInfo.setIsActive(row.getCell(3).getStringCellValue());

				experienceInfos.add(experienceInfo);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return experienceInfos;
	}

	@RequestMapping(value = "/experienceInfo/processExcel2007", method = RequestMethod.POST)
	public List<ExperienceInfo> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<ExperienceInfo> experienceInfos = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ExperienceInfo experienceInfo = new ExperienceInfo();
				XSSFRow row = worksheet.getRow(i++);
				//experienceInfo.setField2(row.getCell(1).getStringCellValue());
				experienceInfo.setFromDate(HelperUtils.getTimestampFromString(row.getCell(0).getStringCellValue()));
experienceInfo.setThruDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));
experienceInfo.setDescription(row.getCell(2).getStringCellValue());
experienceInfo.setIsActive(row.getCell(3).getStringCellValue());

				experienceInfos.add(experienceInfo);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return experienceInfos;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/experienceInfo/excelFileUpload")
	@ResponseBody
	public List<ExperienceInfo> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<ExperienceInfo> experienceInfos = new ArrayList<ExperienceInfo>();
		if (extension.equalsIgnoreCase("xlsx")) {
			experienceInfos = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			experienceInfos = processExcel2003(file);
		}
		
		if (experienceInfos.size()>0) {
			experienceInfos.remove(0);
		}
		return experienceInfos;
	}
}
