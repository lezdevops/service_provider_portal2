package app.serviceProviderPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.ServiceResponseInfo;
import app.framework.businesstier.HelperUtils;
import app.serviceProviderPanel.businesstier.ServiceResponseInfoManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules ServiceResponseInfo API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class ServiceResponseInfoAPI {

	@Autowired
	private ServiceResponseInfoManager serviceResponseInfoManager;

	@PostMapping("/serviceResponseInfo")
	public Map<String,Object> createServiceResponseInfo(@RequestBody ServiceResponseInfo serviceResponseInfo) {
		serviceResponseInfo.setServiceProvider(null);

		
		return serviceResponseInfoManager.save(serviceResponseInfo);
	}
	@GetMapping("/serviceResponseInfo/{id}")
	public ServiceResponseInfo getServiceResponseInfo(@PathVariable Long id) {
		return serviceResponseInfoManager.findOne(id);
	}
	@GetMapping(value = "/serviceResponseInfos")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<ServiceResponseInfo> getAll() {
		return serviceResponseInfoManager.getServiceResponseInfos();
	}

	@PutMapping("/serviceResponseInfo")
	public Map<String,Object> updateServiceResponseInfo(@RequestBody ServiceResponseInfo serviceResponseInfo) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			ServiceResponseInfo newServiceResponseInfo = getServiceResponseInfo(serviceResponseInfo.getId());
			newServiceResponseInfo.setCustMessage(serviceResponseInfo.getCustMessage());
newServiceResponseInfo.setSpAnswer(serviceResponseInfo.getSpAnswer());
newServiceResponseInfo.setIsActive(serviceResponseInfo.getIsActive());

			return serviceResponseInfoManager.save(newServiceResponseInfo);
		}
		
	}
	
	@DeleteMapping("/serviceResponseInfo/{id}")
	public boolean deleteServiceResponseInfo(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		serviceResponseInfoManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/serviceResponseInfo/saveMultipleServiceResponseInfoData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<ServiceResponseInfo> saveMultipleServiceResponseInfoData(@RequestBody List<ServiceResponseInfo> serviceResponseInfos) {
		return serviceResponseInfoManager.saveMultipleServiceResponseInfoData(serviceResponseInfos);
	}

	@RequestMapping(value = "/serviceResponseInfo/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = serviceResponseInfoManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/serviceResponseInfo/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return serviceResponseInfoManager.removeAll();
	}
	@RequestMapping(value = "/serviceResponseInfo/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<ServiceResponseInfo> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<ServiceResponseInfo> resultPage = serviceResponseInfoManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/serviceResponseInfo/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<ServiceResponseInfo> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<ServiceResponseInfo> resultPage = serviceResponseInfoManager.findPaginatedServiceResponseInfosBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/serviceResponseInfo/searchOnServiceResponseInfoTable", method = RequestMethod.GET)
    public Page<ServiceResponseInfo> searchOnServiceResponseInfoTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return serviceResponseInfoManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/serviceResponseInfo/processExcel2003", method = RequestMethod.POST)
	public List<ServiceResponseInfo> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<ServiceResponseInfo> serviceResponseInfos = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ServiceResponseInfo serviceResponseInfo = new ServiceResponseInfo();
				HSSFRow row = worksheet.getRow(i++);
				//serviceResponseInfo.setField2(row.getCell(1).getStringCellValue());
				serviceResponseInfo.setCustMessage(row.getCell(0).getStringCellValue());
serviceResponseInfo.setSpAnswer(row.getCell(1).getStringCellValue());
serviceResponseInfo.setSentDate(HelperUtils.getTimestampFromString(row.getCell(2).getStringCellValue()));
serviceResponseInfo.setResponseDate(HelperUtils.getTimestampFromString(row.getCell(3).getStringCellValue()));
serviceResponseInfo.setIsActive(row.getCell(4).getStringCellValue());

				serviceResponseInfos.add(serviceResponseInfo);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return serviceResponseInfos;
	}

	@RequestMapping(value = "/serviceResponseInfo/processExcel2007", method = RequestMethod.POST)
	public List<ServiceResponseInfo> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<ServiceResponseInfo> serviceResponseInfos = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ServiceResponseInfo serviceResponseInfo = new ServiceResponseInfo();
				XSSFRow row = worksheet.getRow(i++);
				//serviceResponseInfo.setField2(row.getCell(1).getStringCellValue());
				serviceResponseInfo.setCustMessage(row.getCell(0).getStringCellValue());
serviceResponseInfo.setSpAnswer(row.getCell(1).getStringCellValue());
serviceResponseInfo.setSentDate(HelperUtils.getTimestampFromString(row.getCell(2).getStringCellValue()));
serviceResponseInfo.setResponseDate(HelperUtils.getTimestampFromString(row.getCell(3).getStringCellValue()));
serviceResponseInfo.setIsActive(row.getCell(4).getStringCellValue());

				serviceResponseInfos.add(serviceResponseInfo);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return serviceResponseInfos;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/serviceResponseInfo/excelFileUpload")
	@ResponseBody
	public List<ServiceResponseInfo> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<ServiceResponseInfo> serviceResponseInfos = new ArrayList<ServiceResponseInfo>();
		if (extension.equalsIgnoreCase("xlsx")) {
			serviceResponseInfos = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			serviceResponseInfos = processExcel2003(file);
		}
		
		if (serviceResponseInfos.size()>0) {
			serviceResponseInfos.remove(0);
		}
		return serviceResponseInfos;
	}
}
