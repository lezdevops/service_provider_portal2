package app.serviceProviderPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.DailyTask;
import app.framework.businesstier.HelperUtils;
import app.serviceProviderPanel.businesstier.DailyTaskManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules DailyTask API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class DailyTaskAPI {

	@Autowired
	private DailyTaskManager dailyTaskManager;

	@PostMapping("/dailyTask")
	public Map<String,Object> createDailyTask(@RequestBody DailyTask dailyTask) {
		dailyTask.setServiceProvider(null);

		
		return dailyTaskManager.save(dailyTask);
	}
	@GetMapping("/dailyTask/{id}")
	public DailyTask getDailyTask(@PathVariable Long id) {
		return dailyTaskManager.findOne(id);
	}
	@GetMapping(value = "/dailyTasks")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<DailyTask> getAll() {
		return dailyTaskManager.getDailyTasks();
	}

	@PutMapping("/dailyTask")
	public Map<String,Object> updateDailyTask(@RequestBody DailyTask dailyTask) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			DailyTask newDailyTask = getDailyTask(dailyTask.getId());
			newDailyTask.setFromDate(dailyTask.getFromDate());
newDailyTask.setThruDate(dailyTask.getThruDate());
newDailyTask.setStartTime(dailyTask.getStartTime());
newDailyTask.setEndTime(dailyTask.getEndTime());

			return dailyTaskManager.save(newDailyTask);
		}
		
	}
	
	@DeleteMapping("/dailyTask/{id}")
	public boolean deleteDailyTask(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		dailyTaskManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/dailyTask/saveMultipleDailyTaskData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<DailyTask> saveMultipleDailyTaskData(@RequestBody List<DailyTask> dailyTasks) {
		return dailyTaskManager.saveMultipleDailyTaskData(dailyTasks);
	}

	@RequestMapping(value = "/dailyTask/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = dailyTaskManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/dailyTask/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return dailyTaskManager.removeAll();
	}
	@RequestMapping(value = "/dailyTask/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<DailyTask> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<DailyTask> resultPage = dailyTaskManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/dailyTask/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<DailyTask> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<DailyTask> resultPage = dailyTaskManager.findPaginatedDailyTasksBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/dailyTask/searchOnDailyTaskTable", method = RequestMethod.GET)
    public Page<DailyTask> searchOnDailyTaskTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return dailyTaskManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/dailyTask/processExcel2003", method = RequestMethod.POST)
	public List<DailyTask> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<DailyTask> dailyTasks = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				DailyTask dailyTask = new DailyTask();
				HSSFRow row = worksheet.getRow(i++);
				//dailyTask.setField2(row.getCell(1).getStringCellValue());
				dailyTask.setFromDate(HelperUtils.getTimestampFromString(row.getCell(0).getStringCellValue()));
dailyTask.setThruDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));
dailyTask.setStartTime(row.getCell(2).getStringCellValue());
dailyTask.setEndTime(row.getCell(3).getStringCellValue());

				dailyTasks.add(dailyTask);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dailyTasks;
	}

	@RequestMapping(value = "/dailyTask/processExcel2007", method = RequestMethod.POST)
	public List<DailyTask> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<DailyTask> dailyTasks = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				DailyTask dailyTask = new DailyTask();
				XSSFRow row = worksheet.getRow(i++);
				//dailyTask.setField2(row.getCell(1).getStringCellValue());
				dailyTask.setFromDate(HelperUtils.getTimestampFromString(row.getCell(0).getStringCellValue()));
dailyTask.setThruDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));
dailyTask.setStartTime(row.getCell(2).getStringCellValue());
dailyTask.setEndTime(row.getCell(3).getStringCellValue());

				dailyTasks.add(dailyTask);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dailyTasks;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/dailyTask/excelFileUpload")
	@ResponseBody
	public List<DailyTask> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<DailyTask> dailyTasks = new ArrayList<DailyTask>();
		if (extension.equalsIgnoreCase("xlsx")) {
			dailyTasks = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			dailyTasks = processExcel2003(file);
		}
		
		if (dailyTasks.size()>0) {
			dailyTasks.remove(0);
		}
		return dailyTasks;
	}
}
