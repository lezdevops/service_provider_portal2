package app.serviceProviderPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.TrainingInfo;
import app.framework.businesstier.HelperUtils;
import app.serviceProviderPanel.businesstier.TrainingInfoManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules TrainingInfo API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class TrainingInfoAPI {

	@Autowired
	private TrainingInfoManager trainingInfoManager;

	@PostMapping("/trainingInfo")
	public Map<String,Object> createTrainingInfo(@RequestBody TrainingInfo trainingInfo) {
		trainingInfo.setServiceProvider(null);

		
		return trainingInfoManager.save(trainingInfo);
	}
	@GetMapping("/trainingInfo/{id}")
	public TrainingInfo getTrainingInfo(@PathVariable Long id) {
		return trainingInfoManager.findOne(id);
	}
	@GetMapping(value = "/trainingInfos")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<TrainingInfo> getAll() {
		return trainingInfoManager.getTrainingInfos();
	}

	@PutMapping("/trainingInfo")
	public Map<String,Object> updateTrainingInfo(@RequestBody TrainingInfo trainingInfo) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			TrainingInfo newTrainingInfo = getTrainingInfo(trainingInfo.getId());
			newTrainingInfo.setMonthYear(trainingInfo.getMonthYear());
newTrainingInfo.setDescription(trainingInfo.getDescription());
newTrainingInfo.setCertificationDoc(trainingInfo.getCertificationDoc());
newTrainingInfo.setUploadedDate(trainingInfo.getUploadedDate());

			return trainingInfoManager.save(newTrainingInfo);
		}
		
	}
	
	@DeleteMapping("/trainingInfo/{id}")
	public boolean deleteTrainingInfo(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		trainingInfoManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/trainingInfo/saveMultipleTrainingInfoData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<TrainingInfo> saveMultipleTrainingInfoData(@RequestBody List<TrainingInfo> trainingInfos) {
		return trainingInfoManager.saveMultipleTrainingInfoData(trainingInfos);
	}

	@RequestMapping(value = "/trainingInfo/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = trainingInfoManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/trainingInfo/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return trainingInfoManager.removeAll();
	}
	@RequestMapping(value = "/trainingInfo/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<TrainingInfo> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<TrainingInfo> resultPage = trainingInfoManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/trainingInfo/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<TrainingInfo> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<TrainingInfo> resultPage = trainingInfoManager.findPaginatedTrainingInfosBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/trainingInfo/searchOnTrainingInfoTable", method = RequestMethod.GET)
    public Page<TrainingInfo> searchOnTrainingInfoTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return trainingInfoManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/trainingInfo/processExcel2003", method = RequestMethod.POST)
	public List<TrainingInfo> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<TrainingInfo> trainingInfos = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				TrainingInfo trainingInfo = new TrainingInfo();
				HSSFRow row = worksheet.getRow(i++);
				//trainingInfo.setField2(row.getCell(1).getStringCellValue());
				trainingInfo.setMonthYear(row.getCell(0).getStringCellValue());
trainingInfo.setDescription(row.getCell(1).getStringCellValue());
trainingInfo.setCertificationDoc(row.getCell(2).getStringCellValue());
trainingInfo.setUploadedDate(HelperUtils.getTimestampFromString(row.getCell(3).getStringCellValue()));

				trainingInfos.add(trainingInfo);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return trainingInfos;
	}

	@RequestMapping(value = "/trainingInfo/processExcel2007", method = RequestMethod.POST)
	public List<TrainingInfo> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<TrainingInfo> trainingInfos = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				TrainingInfo trainingInfo = new TrainingInfo();
				XSSFRow row = worksheet.getRow(i++);
				//trainingInfo.setField2(row.getCell(1).getStringCellValue());
				trainingInfo.setMonthYear(row.getCell(0).getStringCellValue());
trainingInfo.setDescription(row.getCell(1).getStringCellValue());
trainingInfo.setCertificationDoc(row.getCell(2).getStringCellValue());
trainingInfo.setUploadedDate(HelperUtils.getTimestampFromString(row.getCell(3).getStringCellValue()));

				trainingInfos.add(trainingInfo);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return trainingInfos;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/trainingInfo/excelFileUpload")
	@ResponseBody
	public List<TrainingInfo> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<TrainingInfo> trainingInfos = new ArrayList<TrainingInfo>();
		if (extension.equalsIgnoreCase("xlsx")) {
			trainingInfos = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			trainingInfos = processExcel2003(file);
		}
		
		if (trainingInfos.size()>0) {
			trainingInfos.remove(0);
		}
		return trainingInfos;
	}
}
