package app.serviceProviderPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.OtherActivities;
import app.framework.businesstier.HelperUtils;
import app.serviceProviderPanel.businesstier.OtherActivitiesManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules OtherActivities API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class OtherActivitiesAPI {

	@Autowired
	private OtherActivitiesManager otherActivitiesManager;

	@PostMapping("/otherActivities")
	public Map<String,Object> createOtherActivities(@RequestBody OtherActivities otherActivities) {
		otherActivities.setServiceProvider(null);

		
		return otherActivitiesManager.save(otherActivities);
	}
	@GetMapping("/otherActivities/{id}")
	public OtherActivities getOtherActivities(@PathVariable Long id) {
		return otherActivitiesManager.findOne(id);
	}
	@GetMapping(value = "/otherActivitiess")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<OtherActivities> getAll() {
		return otherActivitiesManager.getOtherActivitiess();
	}

	@PutMapping("/otherActivities")
	public Map<String,Object> updateOtherActivities(@RequestBody OtherActivities otherActivities) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			OtherActivities newOtherActivities = getOtherActivities(otherActivities.getId());
			newOtherActivities.setDescription(otherActivities.getDescription());
newOtherActivities.setIsActive(otherActivities.getIsActive());

			return otherActivitiesManager.save(newOtherActivities);
		}
		
	}
	
	@DeleteMapping("/otherActivities/{id}")
	public boolean deleteOtherActivities(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		otherActivitiesManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/otherActivities/saveMultipleOtherActivitiesData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<OtherActivities> saveMultipleOtherActivitiesData(@RequestBody List<OtherActivities> otherActivitiess) {
		return otherActivitiesManager.saveMultipleOtherActivitiesData(otherActivitiess);
	}

	@RequestMapping(value = "/otherActivities/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = otherActivitiesManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/otherActivities/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return otherActivitiesManager.removeAll();
	}
	@RequestMapping(value = "/otherActivities/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<OtherActivities> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<OtherActivities> resultPage = otherActivitiesManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/otherActivities/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<OtherActivities> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<OtherActivities> resultPage = otherActivitiesManager.findPaginatedOtherActivitiessBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/otherActivities/searchOnOtherActivitiesTable", method = RequestMethod.GET)
    public Page<OtherActivities> searchOnOtherActivitiesTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return otherActivitiesManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/otherActivities/processExcel2003", method = RequestMethod.POST)
	public List<OtherActivities> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<OtherActivities> otherActivitiess = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				OtherActivities otherActivities = new OtherActivities();
				HSSFRow row = worksheet.getRow(i++);
				//otherActivities.setField2(row.getCell(1).getStringCellValue());
				otherActivities.setDescription(row.getCell(0).getStringCellValue());
otherActivities.setIsActive(row.getCell(1).getStringCellValue());

				otherActivitiess.add(otherActivities);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return otherActivitiess;
	}

	@RequestMapping(value = "/otherActivities/processExcel2007", method = RequestMethod.POST)
	public List<OtherActivities> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<OtherActivities> otherActivitiess = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				OtherActivities otherActivities = new OtherActivities();
				XSSFRow row = worksheet.getRow(i++);
				//otherActivities.setField2(row.getCell(1).getStringCellValue());
				otherActivities.setDescription(row.getCell(0).getStringCellValue());
otherActivities.setIsActive(row.getCell(1).getStringCellValue());

				otherActivitiess.add(otherActivities);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return otherActivitiess;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/otherActivities/excelFileUpload")
	@ResponseBody
	public List<OtherActivities> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<OtherActivities> otherActivitiess = new ArrayList<OtherActivities>();
		if (extension.equalsIgnoreCase("xlsx")) {
			otherActivitiess = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			otherActivitiess = processExcel2003(file);
		}
		
		if (otherActivitiess.size()>0) {
			otherActivitiess.remove(0);
		}
		return otherActivitiess;
	}
}
