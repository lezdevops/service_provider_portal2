package app.serviceProviderPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.AgentReference;
import app.framework.businesstier.HelperUtils;
import app.serviceProviderPanel.businesstier.AgentReferenceManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules AgentReference API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class AgentReferenceAPI {

	@Autowired
	private AgentReferenceManager agentReferenceManager;

	@PostMapping("/agentReference")
	public Map<String,Object> createAgentReference(@RequestBody AgentReference agentReference) {
		agentReference.setServiceProvider(null);

		
		return agentReferenceManager.save(agentReference);
	}
	@GetMapping("/agentReference/{id}")
	public AgentReference getAgentReference(@PathVariable Long id) {
		return agentReferenceManager.findOne(id);
	}
	@GetMapping(value = "/agentReferences")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<AgentReference> getAll() {
		return agentReferenceManager.getAgentReferences();
	}

	@PutMapping("/agentReference")
	public Map<String,Object> updateAgentReference(@RequestBody AgentReference agentReference) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			AgentReference newAgentReference = getAgentReference(agentReference.getId());
			newAgentReference.setRefSP(agentReference.getRefSP());
newAgentReference.setFromDate(agentReference.getFromDate());
newAgentReference.setThruDate(agentReference.getThruDate());
newAgentReference.setIsActive(agentReference.getIsActive());

			return agentReferenceManager.save(newAgentReference);
		}
		
	}
	
	@DeleteMapping("/agentReference/{id}")
	public boolean deleteAgentReference(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		agentReferenceManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/agentReference/saveMultipleAgentReferenceData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<AgentReference> saveMultipleAgentReferenceData(@RequestBody List<AgentReference> agentReferences) {
		return agentReferenceManager.saveMultipleAgentReferenceData(agentReferences);
	}

	@RequestMapping(value = "/agentReference/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = agentReferenceManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/agentReference/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return agentReferenceManager.removeAll();
	}
	@RequestMapping(value = "/agentReference/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<AgentReference> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<AgentReference> resultPage = agentReferenceManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/agentReference/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<AgentReference> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<AgentReference> resultPage = agentReferenceManager.findPaginatedAgentReferencesBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/agentReference/searchOnAgentReferenceTable", method = RequestMethod.GET)
    public Page<AgentReference> searchOnAgentReferenceTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return agentReferenceManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/agentReference/processExcel2003", method = RequestMethod.POST)
	public List<AgentReference> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<AgentReference> agentReferences = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				AgentReference agentReference = new AgentReference();
				HSSFRow row = worksheet.getRow(i++);
				//agentReference.setField2(row.getCell(1).getStringCellValue());
				agentReference.setRefSP(row.getCell(0).getStringCellValue());
agentReference.setFromDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));
agentReference.setThruDate(HelperUtils.getTimestampFromString(row.getCell(2).getStringCellValue()));
agentReference.setIsActive(row.getCell(3).getStringCellValue());

				agentReferences.add(agentReference);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return agentReferences;
	}

	@RequestMapping(value = "/agentReference/processExcel2007", method = RequestMethod.POST)
	public List<AgentReference> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<AgentReference> agentReferences = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				AgentReference agentReference = new AgentReference();
				XSSFRow row = worksheet.getRow(i++);
				//agentReference.setField2(row.getCell(1).getStringCellValue());
				agentReference.setRefSP(row.getCell(0).getStringCellValue());
agentReference.setFromDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));
agentReference.setThruDate(HelperUtils.getTimestampFromString(row.getCell(2).getStringCellValue()));
agentReference.setIsActive(row.getCell(3).getStringCellValue());

				agentReferences.add(agentReference);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return agentReferences;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/agentReference/excelFileUpload")
	@ResponseBody
	public List<AgentReference> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<AgentReference> agentReferences = new ArrayList<AgentReference>();
		if (extension.equalsIgnoreCase("xlsx")) {
			agentReferences = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			agentReferences = processExcel2003(file);
		}
		
		if (agentReferences.size()>0) {
			agentReferences.remove(0);
		}
		return agentReferences;
	}
}
