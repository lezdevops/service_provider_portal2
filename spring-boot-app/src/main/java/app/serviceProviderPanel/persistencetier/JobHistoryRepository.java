package app.serviceProviderPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.JobHistory;

@Repository
public interface JobHistoryRepository extends JpaRepository<JobHistory, Long>{
	List<JobHistory> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT jobHistory FROM JobHistory jobHistory WHERE  cast(jobHistory.serviceProvider.name AS string) like %:searchToken% OR cast(jobHistory.employerName AS string) like %:searchToken% OR cast(jobHistory.jobTitle AS string) like %:searchToken% OR cast(jobHistory.fromDate AS string) like %:searchToken% OR cast(jobHistory.thruDate AS string) like %:searchToken% OR cast(jobHistory.location AS string) like %:searchToken% OR cast(jobHistory.uploadedDoc AS string) like %:searchToken% OR cast(jobHistory.description AS string) like %:searchToken%")
	Page<JobHistory> findPaginatedJobHistorysBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
