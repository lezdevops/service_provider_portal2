package app.serviceProviderPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.TrainingInfo;

@Repository
public interface TrainingInfoRepository extends JpaRepository<TrainingInfo, Long>{
	List<TrainingInfo> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT trainingInfo FROM TrainingInfo trainingInfo WHERE  cast(trainingInfo.serviceProvider.name AS string) like %:searchToken% OR cast(trainingInfo.trainingType.description AS string) like %:searchToken% OR cast(trainingInfo.monthYear AS string) like %:searchToken% OR cast(trainingInfo.description AS string) like %:searchToken% OR cast(trainingInfo.certificationDoc AS string) like %:searchToken% OR cast(trainingInfo.uploadedDate AS string) like %:searchToken%")
	Page<TrainingInfo> findPaginatedTrainingInfosBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
