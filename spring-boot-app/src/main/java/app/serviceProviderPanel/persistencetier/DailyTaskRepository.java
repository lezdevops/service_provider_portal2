package app.serviceProviderPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.DailyTask;

@Repository
public interface DailyTaskRepository extends JpaRepository<DailyTask, Long>{
	List<DailyTask> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT dailyTask FROM DailyTask dailyTask WHERE  cast(dailyTask.serviceProvider.name AS string) like %:searchToken% OR cast(dailyTask.fromDate AS string) like %:searchToken% OR cast(dailyTask.thruDate AS string) like %:searchToken% OR cast(dailyTask.startTime AS string) like %:searchToken% OR cast(dailyTask.endTime AS string) like %:searchToken%")
	Page<DailyTask> findPaginatedDailyTasksBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
