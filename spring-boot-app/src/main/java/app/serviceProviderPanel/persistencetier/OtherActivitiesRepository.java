package app.serviceProviderPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.OtherActivities;

@Repository
public interface OtherActivitiesRepository extends JpaRepository<OtherActivities, Long>{
	List<OtherActivities> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT otherActivities FROM OtherActivities otherActivities WHERE  cast(otherActivities.serviceProvider.name AS string) like %:searchToken% OR cast(otherActivities.description AS string) like %:searchToken% OR cast(otherActivities.isActive AS string) like %:searchToken%")
	Page<OtherActivities> findPaginatedOtherActivitiessBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
