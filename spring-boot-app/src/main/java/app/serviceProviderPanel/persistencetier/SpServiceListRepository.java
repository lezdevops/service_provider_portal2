package app.serviceProviderPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.SpServiceList;

@Repository
public interface SpServiceListRepository extends JpaRepository<SpServiceList, Long>{
	List<SpServiceList> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT spServiceList FROM SpServiceList spServiceList WHERE  cast(spServiceList.serviceProvider.name AS string) like %:searchToken% OR cast(spServiceList.serviceDetails AS string) like %:searchToken% OR cast(spServiceList.serviceType.description AS string) like %:searchToken% OR cast(spServiceList.visitCount AS string) like %:searchToken% OR cast(spServiceList.location.description AS string) like %:searchToken% OR cast(spServiceList.requestCount AS string) like %:searchToken% OR cast(spServiceList.startTime AS string) like %:searchToken% OR cast(spServiceList.endTime AS string) like %:searchToken% OR cast(spServiceList.isActive AS string) like %:searchToken% OR cast(spServiceList.availFrom AS string) like %:searchToken% OR cast(spServiceList.availTo AS string) like %:searchToken%")
	Page<SpServiceList> findPaginatedSpServiceListsBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
