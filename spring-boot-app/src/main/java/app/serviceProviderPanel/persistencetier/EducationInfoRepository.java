package app.serviceProviderPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.EducationInfo;

@Repository
public interface EducationInfoRepository extends JpaRepository<EducationInfo, Long>{
	List<EducationInfo> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT educationInfo FROM EducationInfo educationInfo WHERE  cast(educationInfo.serviceProvider.name AS string) like %:searchToken% OR cast(educationInfo.instituteName AS string) like %:searchToken% OR cast(educationInfo.fromYear AS string) like %:searchToken% OR cast(educationInfo.thruYear AS string) like %:searchToken% OR cast(educationInfo.degree AS string) like %:searchToken% OR cast(educationInfo.isActive AS string) like %:searchToken%")
	Page<EducationInfo> findPaginatedEducationInfosBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
