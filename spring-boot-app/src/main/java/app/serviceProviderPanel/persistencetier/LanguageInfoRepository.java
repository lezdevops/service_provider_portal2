package app.serviceProviderPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.LanguageInfo;

@Repository
public interface LanguageInfoRepository extends JpaRepository<LanguageInfo, Long>{
	List<LanguageInfo> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT languageInfo FROM LanguageInfo languageInfo WHERE  cast(languageInfo.serviceProvider.name AS string) like %:searchToken% OR cast(languageInfo.languageName AS string) like %:searchToken% OR cast(languageInfo.shortCode AS string) like %:searchToken% OR cast(languageInfo.isActive AS string) like %:searchToken%")
	Page<LanguageInfo> findPaginatedLanguageInfosBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
