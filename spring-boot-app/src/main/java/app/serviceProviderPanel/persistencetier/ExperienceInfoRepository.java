package app.serviceProviderPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.ExperienceInfo;

@Repository
public interface ExperienceInfoRepository extends JpaRepository<ExperienceInfo, Long>{
	List<ExperienceInfo> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT experienceInfo FROM ExperienceInfo experienceInfo WHERE  cast(experienceInfo.serviceProvider.name AS string) like %:searchToken% OR cast(experienceInfo.fromDate AS string) like %:searchToken% OR cast(experienceInfo.thruDate AS string) like %:searchToken% OR cast(experienceInfo.description AS string) like %:searchToken% OR cast(experienceInfo.isActive AS string) like %:searchToken%")
	Page<ExperienceInfo> findPaginatedExperienceInfosBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
