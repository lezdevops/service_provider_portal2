package app.serviceProviderPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.ServiceResponseInfo;

@Repository
public interface ServiceResponseInfoRepository extends JpaRepository<ServiceResponseInfo, Long>{
	List<ServiceResponseInfo> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT serviceResponseInfo FROM ServiceResponseInfo serviceResponseInfo WHERE  cast(serviceResponseInfo.serviceProvider.name AS string) like %:searchToken% OR cast(serviceResponseInfo.custMessage AS string) like %:searchToken% OR cast(serviceResponseInfo.spAnswer AS string) like %:searchToken% OR cast(serviceResponseInfo.sentDate AS string) like %:searchToken% OR cast(serviceResponseInfo.responseDate AS string) like %:searchToken% OR cast(serviceResponseInfo.isActive AS string) like %:searchToken%")
	Page<ServiceResponseInfo> findPaginatedServiceResponseInfosBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
