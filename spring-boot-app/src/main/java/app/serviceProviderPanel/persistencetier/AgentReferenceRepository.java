package app.serviceProviderPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.AgentReference;

@Repository
public interface AgentReferenceRepository extends JpaRepository<AgentReference, Long>{
	List<AgentReference> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT agentReference FROM AgentReference agentReference WHERE  cast(agentReference.serviceProvider.name AS string) like %:searchToken% OR cast(agentReference.refSP AS string) like %:searchToken% OR cast(agentReference.fromDate AS string) like %:searchToken% OR cast(agentReference.thruDate AS string) like %:searchToken% OR cast(agentReference.isActive AS string) like %:searchToken%")
	Page<AgentReference> findPaginatedAgentReferencesBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
