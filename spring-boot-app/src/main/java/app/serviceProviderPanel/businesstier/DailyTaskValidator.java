package app.serviceProviderPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.DailyTask;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.businesstier.DailyTaskManager;

@Service
public class DailyTaskValidator {
	@Autowired
	private DailyTaskManager dailyTaskManager;
	public void validate(DailyTask dailyTask) throws Exception {
		if (!checkNull(dailyTask)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(DailyTask dailyTask){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility dailyTaskProperties = new ConfigUtility(new File("i18n/serviceProviderPanel/dailyTask_i18n.properties"));
			Properties dailyTaskPropertyValue = dailyTaskProperties.loadProperties();
			
if (UtilValidate.isEmpty(dailyTask.getServiceProvider())) {
errorMessageMap.put("serviceProvider_validationMessage" , dailyTaskPropertyValue.getProperty(language+ ".serviceProvider.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(DailyTask dailyTask){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
