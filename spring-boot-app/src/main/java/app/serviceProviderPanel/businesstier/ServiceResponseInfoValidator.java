package app.serviceProviderPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.ServiceResponseInfo;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.businesstier.ServiceResponseInfoManager;

@Service
public class ServiceResponseInfoValidator {
	@Autowired
	private ServiceResponseInfoManager serviceResponseInfoManager;
	public void validate(ServiceResponseInfo serviceResponseInfo) throws Exception {
		if (!checkNull(serviceResponseInfo)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(ServiceResponseInfo serviceResponseInfo){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility serviceResponseInfoProperties = new ConfigUtility(new File("i18n/serviceProviderPanel/serviceResponseInfo_i18n.properties"));
			Properties serviceResponseInfoPropertyValue = serviceResponseInfoProperties.loadProperties();
			
if (UtilValidate.isEmpty(serviceResponseInfo.getServiceProvider())) {
errorMessageMap.put("serviceProvider_validationMessage" , serviceResponseInfoPropertyValue.getProperty(language+ ".serviceProvider.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(serviceResponseInfo.getSentDate())) {
errorMessageMap.put("sentDate_validationMessage" , serviceResponseInfoPropertyValue.getProperty(language+ ".sentDate.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(serviceResponseInfo.getResponseDate())) {
errorMessageMap.put("responseDate_validationMessage" , serviceResponseInfoPropertyValue.getProperty(language+ ".responseDate.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(ServiceResponseInfo serviceResponseInfo){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
