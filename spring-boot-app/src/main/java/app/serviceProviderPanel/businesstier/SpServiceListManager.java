package app.serviceProviderPanel.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.SpServiceList;
import app.serviceProviderPanel.persistencetier.SpServiceListRepository;


//newDependentBeanImportGoesHere

@Service
public class SpServiceListManager {
	@Autowired
	private SpServiceListRepository spServiceListRepository;
	@Autowired
	private SpServiceListValidator spServiceListValidator;
	// newAutowiredPropertiesGoesHere
	
	public SpServiceList getSpServiceList(Long id){
		SpServiceList spServiceList = spServiceListRepository.getOne(id);
		return spServiceList;
	}

	
	public List<SpServiceList> getSpServiceLists(){
		List<SpServiceList> spServiceListsList = new ArrayList<SpServiceList>();
		Iterable<SpServiceList> spServiceLists = spServiceListRepository.findAll();
		for (SpServiceList spServiceList : spServiceLists) {
			spServiceListsList.add(spServiceList);
			// newGenerateEntityFetchActionGoesHere
		}
		return spServiceListsList;
	}
	
	
	public Map<String,Object> save(SpServiceList spServiceList) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = spServiceListValidator.getValidated(spServiceList);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/spServiceList");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				spServiceList = spServiceListRepository.save(spServiceList);
				messageMap.put("successMessage", "Successfully Saved SpServiceList Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(SpServiceListManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}
	
	public List<SpServiceList> saveMultipleSpServiceListData(List<SpServiceList> spServiceLists) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (SpServiceList spServiceList : spServiceLists) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = spServiceListValidator.getValidated(spServiceList);
				spServiceList = spServiceListRepository.save(spServiceList);
				messageMap.put("addSucessmessage", "Successfully SpServiceList Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getSpServiceLists();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(SpServiceListManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getSpServiceLists();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully SpServiceList Deleted.";
		try {
			spServiceListRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All SpServiceList Deleted.";
		spServiceListRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<SpServiceList> findPaginated(int page, int size) {
        return spServiceListRepository.findAll(new PageRequest(page, size));
    }
	public Page<SpServiceList> findPaginatedSpServiceListsBySearchToken(int page, int size, String searchToken) {
        return spServiceListRepository.findPaginatedSpServiceListsBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public SpServiceList findOne(Long id) {
		SpServiceList spServiceList = spServiceListRepository.findOne(id);
		return spServiceList;
	}
	public Page<SpServiceList> findById(Long id,
			Pageable pageRequest) {
		List<SpServiceList> spServiceLists = spServiceListRepository.findById(id);
		Page<SpServiceList> pages = null;
		pages = new PageImpl<SpServiceList>(spServiceLists, pageRequest, spServiceLists.size());
		return pages;
	}
	
}
