package app.serviceProviderPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.ExperienceInfo;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.businesstier.ExperienceInfoManager;

@Service
public class ExperienceInfoValidator {
	@Autowired
	private ExperienceInfoManager experienceInfoManager;
	public void validate(ExperienceInfo experienceInfo) throws Exception {
		if (!checkNull(experienceInfo)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(ExperienceInfo experienceInfo){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility experienceInfoProperties = new ConfigUtility(new File("i18n/serviceProviderPanel/experienceInfo_i18n.properties"));
			Properties experienceInfoPropertyValue = experienceInfoProperties.loadProperties();
			
if (UtilValidate.isEmpty(experienceInfo.getServiceProvider())) {
errorMessageMap.put("serviceProvider_validationMessage" , experienceInfoPropertyValue.getProperty(language+ ".serviceProvider.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(ExperienceInfo experienceInfo){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
