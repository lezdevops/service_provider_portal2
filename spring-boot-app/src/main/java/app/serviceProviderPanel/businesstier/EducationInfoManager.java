package app.serviceProviderPanel.businesstier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.EducationInfo;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.persistencetier.EducationInfoRepository;


//newDependentBeanImportGoesHere

@Service
public class EducationInfoManager {
	@Autowired
	private EducationInfoRepository educationInfoRepository;
	@Autowired
	private EducationInfoValidator educationInfoValidator;
	// newAutowiredPropertiesGoesHere
	
	public EducationInfo getEducationInfo(Long id){
		EducationInfo educationInfo = educationInfoRepository.getOne(id);
		return educationInfo;
	}

	
	public List<EducationInfo> getEducationInfos(){
		List<EducationInfo> educationInfosList = new ArrayList<EducationInfo>();
		Iterable<EducationInfo> educationInfos = educationInfoRepository.findAll();
		for (EducationInfo educationInfo : educationInfos) {
			educationInfosList.add(educationInfo);
			// newGenerateEntityFetchActionGoesHere
		}
		return educationInfosList;
	}
	
	
	public Map<String,Object> save(EducationInfo educationInfo) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = educationInfoValidator.getValidated(educationInfo);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/educationInfo");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				educationInfo = educationInfoRepository.save(educationInfo);
				messageMap.put("successMessage", "Successfully Saved EducationInfo Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(EducationInfoManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}

	public List<EducationInfo> saveMultipleEducationInfoData(List<EducationInfo> educationInfos) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (EducationInfo educationInfo : educationInfos) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = educationInfoValidator.getValidated(educationInfo);
				educationInfo = educationInfoRepository.save(educationInfo);
				messageMap.put("addSucessmessage", "Successfully EducationInfo Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getEducationInfos();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(EducationInfoManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getEducationInfos();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully EducationInfo Deleted.";
		try {
			educationInfoRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All EducationInfo Deleted.";
		educationInfoRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<EducationInfo> findPaginated(int page, int size) {
        return educationInfoRepository.findAll(new PageRequest(page, size));
    }
	public Page<EducationInfo> findPaginatedEducationInfosBySearchToken(int page, int size, String searchToken) {
        return educationInfoRepository.findPaginatedEducationInfosBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public EducationInfo findOne(Long id) {
		EducationInfo educationInfo = educationInfoRepository.findOne(id);
		return educationInfo;
	}
	public Page<EducationInfo> findById(Long id,
			Pageable pageRequest) {
		List<EducationInfo> educationInfos = educationInfoRepository.findById(id);
		Page<EducationInfo> pages = null;
		pages = new PageImpl<EducationInfo>(educationInfos, pageRequest, educationInfos.size());
		return pages;
	}
	
}
