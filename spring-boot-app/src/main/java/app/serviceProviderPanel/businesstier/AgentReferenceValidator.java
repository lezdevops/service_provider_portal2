package app.serviceProviderPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.AgentReference;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.businesstier.AgentReferenceManager;

@Service
public class AgentReferenceValidator {
	@Autowired
	private AgentReferenceManager agentReferenceManager;
	public void validate(AgentReference agentReference) throws Exception {
		if (!checkNull(agentReference)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(AgentReference agentReference){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility agentReferenceProperties = new ConfigUtility(new File("i18n/serviceProviderPanel/agentReference_i18n.properties"));
			Properties agentReferencePropertyValue = agentReferenceProperties.loadProperties();
			
if (UtilValidate.isEmpty(agentReference.getServiceProvider())) {
errorMessageMap.put("serviceProvider_validationMessage" , agentReferencePropertyValue.getProperty(language+ ".serviceProvider.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(AgentReference agentReference){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
