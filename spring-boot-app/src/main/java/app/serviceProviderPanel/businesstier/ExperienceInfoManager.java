package app.serviceProviderPanel.businesstier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.ExperienceInfo;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.persistencetier.ExperienceInfoRepository;


//newDependentBeanImportGoesHere

@Service
public class ExperienceInfoManager {
	@Autowired
	private ExperienceInfoRepository experienceInfoRepository;
	@Autowired
	private ExperienceInfoValidator experienceInfoValidator;
	// newAutowiredPropertiesGoesHere
	
	public ExperienceInfo getExperienceInfo(Long id){
		ExperienceInfo experienceInfo = experienceInfoRepository.getOne(id);
		return experienceInfo;
	}

	
	public List<ExperienceInfo> getExperienceInfos(){
		List<ExperienceInfo> experienceInfosList = new ArrayList<ExperienceInfo>();
		Iterable<ExperienceInfo> experienceInfos = experienceInfoRepository.findAll();
		for (ExperienceInfo experienceInfo : experienceInfos) {
			experienceInfosList.add(experienceInfo);
			// newGenerateEntityFetchActionGoesHere
		}
		return experienceInfosList;
	}
	
	
	public Map<String,Object> save(ExperienceInfo experienceInfo) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = experienceInfoValidator.getValidated(experienceInfo);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/experienceInfo");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				experienceInfo = experienceInfoRepository.save(experienceInfo);
				messageMap.put("successMessage", "Successfully Saved ExperienceInfo Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ExperienceInfoManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}
	
	public List<ExperienceInfo> saveMultipleExperienceInfoData(List<ExperienceInfo> experienceInfos) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (ExperienceInfo experienceInfo : experienceInfos) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = experienceInfoValidator.getValidated(experienceInfo);
				experienceInfo = experienceInfoRepository.save(experienceInfo);
				messageMap.put("addSucessmessage", "Successfully ExperienceInfo Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getExperienceInfos();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ExperienceInfoManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getExperienceInfos();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully ExperienceInfo Deleted.";
		try {
			experienceInfoRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All ExperienceInfo Deleted.";
		experienceInfoRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<ExperienceInfo> findPaginated(int page, int size) {
        return experienceInfoRepository.findAll(new PageRequest(page, size));
    }
	public Page<ExperienceInfo> findPaginatedExperienceInfosBySearchToken(int page, int size, String searchToken) {
        return experienceInfoRepository.findPaginatedExperienceInfosBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public ExperienceInfo findOne(Long id) {
		ExperienceInfo experienceInfo = experienceInfoRepository.findOne(id);
		return experienceInfo;
	}
	public Page<ExperienceInfo> findById(Long id,
			Pageable pageRequest) {
		List<ExperienceInfo> experienceInfos = experienceInfoRepository.findById(id);
		Page<ExperienceInfo> pages = null;
		pages = new PageImpl<ExperienceInfo>(experienceInfos, pageRequest, experienceInfos.size());
		return pages;
	}
	
}
