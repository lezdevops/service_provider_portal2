package app.serviceProviderPanel.businesstier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.DailyTask;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.persistencetier.DailyTaskRepository;


//newDependentBeanImportGoesHere

@Service
public class DailyTaskManager {
	@Autowired
	private DailyTaskRepository dailyTaskRepository;
	@Autowired
	private DailyTaskValidator dailyTaskValidator;
	// newAutowiredPropertiesGoesHere
	
	public DailyTask getDailyTask(Long id){
		DailyTask dailyTask = dailyTaskRepository.getOne(id);
		return dailyTask;
	}

	
	public List<DailyTask> getDailyTasks(){
		List<DailyTask> dailyTasksList = new ArrayList<DailyTask>();
		Iterable<DailyTask> dailyTasks = dailyTaskRepository.findAll();
		for (DailyTask dailyTask : dailyTasks) {
			dailyTasksList.add(dailyTask);
			// newGenerateEntityFetchActionGoesHere
		}
		return dailyTasksList;
	}
	
	
	public Map<String,Object> save(DailyTask dailyTask) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = dailyTaskValidator.getValidated(dailyTask);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/dailyTask");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				dailyTask = dailyTaskRepository.save(dailyTask);
				messageMap.put("successMessage", "Successfully Saved DailyTask Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(DailyTaskManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}

	public List<DailyTask> saveMultipleDailyTaskData(List<DailyTask> dailyTasks) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (DailyTask dailyTask : dailyTasks) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = dailyTaskValidator.getValidated(dailyTask);
				dailyTask = dailyTaskRepository.save(dailyTask);
				messageMap.put("addSucessmessage", "Successfully DailyTask Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getDailyTasks();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(DailyTaskManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getDailyTasks();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully DailyTask Deleted.";
		try {
			dailyTaskRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All DailyTask Deleted.";
		dailyTaskRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<DailyTask> findPaginated(int page, int size) {
        return dailyTaskRepository.findAll(new PageRequest(page, size));
    }
	public Page<DailyTask> findPaginatedDailyTasksBySearchToken(int page, int size, String searchToken) {
        return dailyTaskRepository.findPaginatedDailyTasksBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public DailyTask findOne(Long id) {
		DailyTask dailyTask = dailyTaskRepository.findOne(id);
		return dailyTask;
	}
	public Page<DailyTask> findById(Long id,
			Pageable pageRequest) {
		List<DailyTask> dailyTasks = dailyTaskRepository.findById(id);
		Page<DailyTask> pages = null;
		pages = new PageImpl<DailyTask>(dailyTasks, pageRequest, dailyTasks.size());
		return pages;
	}
	
}
