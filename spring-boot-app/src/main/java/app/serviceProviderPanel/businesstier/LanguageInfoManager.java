package app.serviceProviderPanel.businesstier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.LanguageInfo;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.persistencetier.LanguageInfoRepository;


//newDependentBeanImportGoesHere

@Service
public class LanguageInfoManager {
	@Autowired
	private LanguageInfoRepository languageInfoRepository;
	@Autowired
	private LanguageInfoValidator languageInfoValidator;
	// newAutowiredPropertiesGoesHere
	
	public LanguageInfo getLanguageInfo(Long id){
		LanguageInfo languageInfo = languageInfoRepository.getOne(id);
		return languageInfo;
	}

	
	public List<LanguageInfo> getLanguageInfos(){
		List<LanguageInfo> languageInfosList = new ArrayList<LanguageInfo>();
		Iterable<LanguageInfo> languageInfos = languageInfoRepository.findAll();
		for (LanguageInfo languageInfo : languageInfos) {
			languageInfosList.add(languageInfo);
			// newGenerateEntityFetchActionGoesHere
		}
		return languageInfosList;
	}
	
	
	public Map<String,Object> save(LanguageInfo languageInfo) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = languageInfoValidator.getValidated(languageInfo);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/languageInfo");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				languageInfo = languageInfoRepository.save(languageInfo);
				messageMap.put("successMessage", "Successfully Saved LanguageInfo Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(LanguageInfoManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}
	
	public List<LanguageInfo> saveMultipleLanguageInfoData(List<LanguageInfo> languageInfos) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (LanguageInfo languageInfo : languageInfos) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = languageInfoValidator.getValidated(languageInfo);
				languageInfo = languageInfoRepository.save(languageInfo);
				messageMap.put("addSucessmessage", "Successfully LanguageInfo Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getLanguageInfos();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(LanguageInfoManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getLanguageInfos();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully LanguageInfo Deleted.";
		try {
			languageInfoRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All LanguageInfo Deleted.";
		languageInfoRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<LanguageInfo> findPaginated(int page, int size) {
        return languageInfoRepository.findAll(new PageRequest(page, size));
    }
	public Page<LanguageInfo> findPaginatedLanguageInfosBySearchToken(int page, int size, String searchToken) {
        return languageInfoRepository.findPaginatedLanguageInfosBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public LanguageInfo findOne(Long id) {
		LanguageInfo languageInfo = languageInfoRepository.findOne(id);
		return languageInfo;
	}
	public Page<LanguageInfo> findById(Long id,
			Pageable pageRequest) {
		List<LanguageInfo> languageInfos = languageInfoRepository.findById(id);
		Page<LanguageInfo> pages = null;
		pages = new PageImpl<LanguageInfo>(languageInfos, pageRequest, languageInfos.size());
		return pages;
	}
	
}
