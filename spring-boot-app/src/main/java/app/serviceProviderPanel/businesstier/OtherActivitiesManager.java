package app.serviceProviderPanel.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.OtherActivities;
import app.serviceProviderPanel.persistencetier.OtherActivitiesRepository;


//newDependentBeanImportGoesHere

@Service
public class OtherActivitiesManager {
	@Autowired
	private OtherActivitiesRepository otherActivitiesRepository;
	@Autowired
	private OtherActivitiesValidator otherActivitiesValidator;
	// newAutowiredPropertiesGoesHere
	
	public OtherActivities getOtherActivities(Long id){
		OtherActivities otherActivities = otherActivitiesRepository.getOne(id);
		return otherActivities;
	}

	
	public List<OtherActivities> getOtherActivitiess(){
		List<OtherActivities> otherActivitiessList = new ArrayList<OtherActivities>();
		Iterable<OtherActivities> otherActivitiess = otherActivitiesRepository.findAll();
		for (OtherActivities otherActivities : otherActivitiess) {
			otherActivitiessList.add(otherActivities);
			// newGenerateEntityFetchActionGoesHere
		}
		return otherActivitiessList;
	}
	
	
	public Map<String,Object> save(OtherActivities otherActivities) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = otherActivitiesValidator.getValidated(otherActivities);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/otherActivities");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				otherActivities = otherActivitiesRepository.save(otherActivities);
				messageMap.put("successMessage", "Successfully Saved OtherActivities Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(OtherActivitiesManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}
	
	public List<OtherActivities> saveMultipleOtherActivitiesData(List<OtherActivities> otherActivitiess) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (OtherActivities otherActivities : otherActivitiess) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = otherActivitiesValidator.getValidated(otherActivities);
				otherActivities = otherActivitiesRepository.save(otherActivities);
				messageMap.put("addSucessmessage", "Successfully OtherActivities Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getOtherActivitiess();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(OtherActivitiesManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getOtherActivitiess();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully OtherActivities Deleted.";
		try {
			otherActivitiesRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All OtherActivities Deleted.";
		otherActivitiesRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<OtherActivities> findPaginated(int page, int size) {
        return otherActivitiesRepository.findAll(new PageRequest(page, size));
    }
	public Page<OtherActivities> findPaginatedOtherActivitiessBySearchToken(int page, int size, String searchToken) {
        return otherActivitiesRepository.findPaginatedOtherActivitiessBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public OtherActivities findOne(Long id) {
		OtherActivities otherActivities = otherActivitiesRepository.findOne(id);
		return otherActivities;
	}
	public Page<OtherActivities> findById(Long id,
			Pageable pageRequest) {
		List<OtherActivities> otherActivitiess = otherActivitiesRepository.findById(id);
		Page<OtherActivities> pages = null;
		pages = new PageImpl<OtherActivities>(otherActivitiess, pageRequest, otherActivitiess.size());
		return pages;
	}
	
}
