package app.serviceProviderPanel.businesstier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.AgentReference;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.persistencetier.AgentReferenceRepository;


//newDependentBeanImportGoesHere

@Service
public class AgentReferenceManager {
	@Autowired
	private AgentReferenceRepository agentReferenceRepository;
	@Autowired
	private AgentReferenceValidator agentReferenceValidator;
	// newAutowiredPropertiesGoesHere
	
	public AgentReference getAgentReference(Long id){
		AgentReference agentReference = agentReferenceRepository.getOne(id);
		return agentReference;
	}

	
	public List<AgentReference> getAgentReferences(){
		List<AgentReference> agentReferencesList = new ArrayList<AgentReference>();
		Iterable<AgentReference> agentReferences = agentReferenceRepository.findAll();
		for (AgentReference agentReference : agentReferences) {
			agentReferencesList.add(agentReference);
			// newGenerateEntityFetchActionGoesHere
		}
		return agentReferencesList;
	}
	
	
	public Map<String,Object> save(AgentReference agentReference) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = agentReferenceValidator.getValidated(agentReference);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/agentReference");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				agentReference = agentReferenceRepository.save(agentReference);
				messageMap.put("successMessage", "Successfully Saved AgentReference Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(AgentReferenceManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}

	public List<AgentReference> saveMultipleAgentReferenceData(List<AgentReference> agentReferences) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (AgentReference agentReference : agentReferences) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = agentReferenceValidator.getValidated(agentReference);
				agentReference = agentReferenceRepository.save(agentReference);
				messageMap.put("addSucessmessage", "Successfully AgentReference Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getAgentReferences();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(AgentReferenceManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getAgentReferences();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully AgentReference Deleted.";
		try {
			agentReferenceRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All AgentReference Deleted.";
		agentReferenceRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<AgentReference> findPaginated(int page, int size) {
        return agentReferenceRepository.findAll(new PageRequest(page, size));
    }
	public Page<AgentReference> findPaginatedAgentReferencesBySearchToken(int page, int size, String searchToken) {
        return agentReferenceRepository.findPaginatedAgentReferencesBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public AgentReference findOne(Long id) {
		AgentReference agentReference = agentReferenceRepository.findOne(id);
		return agentReference;
	}
	public Page<AgentReference> findById(Long id,
			Pageable pageRequest) {
		List<AgentReference> agentReferences = agentReferenceRepository.findById(id);
		Page<AgentReference> pages = null;
		pages = new PageImpl<AgentReference>(agentReferences, pageRequest, agentReferences.size());
		return pages;
	}
	
}
