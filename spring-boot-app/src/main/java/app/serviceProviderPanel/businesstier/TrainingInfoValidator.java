package app.serviceProviderPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.TrainingInfo;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.businesstier.TrainingInfoManager;

@Service
public class TrainingInfoValidator {
	@Autowired
	private TrainingInfoManager trainingInfoManager;
	public void validate(TrainingInfo trainingInfo) throws Exception {
		if (!checkNull(trainingInfo)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(TrainingInfo trainingInfo){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility trainingInfoProperties = new ConfigUtility(new File("i18n/serviceProviderPanel/trainingInfo_i18n.properties"));
			Properties trainingInfoPropertyValue = trainingInfoProperties.loadProperties();
			
if (UtilValidate.isEmpty(trainingInfo.getServiceProvider())) {
errorMessageMap.put("serviceProvider_validationMessage" , trainingInfoPropertyValue.getProperty(language+ ".serviceProvider.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(TrainingInfo trainingInfo){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
