package app.serviceProviderPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.EducationInfo;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.businesstier.EducationInfoManager;

@Service
public class EducationInfoValidator {
	@Autowired
	private EducationInfoManager educationInfoManager;
	public void validate(EducationInfo educationInfo) throws Exception {
		if (!checkNull(educationInfo)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(EducationInfo educationInfo){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility educationInfoProperties = new ConfigUtility(new File("i18n/serviceProviderPanel/educationInfo_i18n.properties"));
			Properties educationInfoPropertyValue = educationInfoProperties.loadProperties();
			
if (UtilValidate.isEmpty(educationInfo.getServiceProvider())) {
errorMessageMap.put("serviceProvider_validationMessage" , educationInfoPropertyValue.getProperty(language+ ".serviceProvider.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(EducationInfo educationInfo){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
