package app.serviceProviderPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.JobHistory;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.businesstier.JobHistoryManager;

@Service
public class JobHistoryValidator {
	@Autowired
	private JobHistoryManager jobHistoryManager;
	public void validate(JobHistory jobHistory) throws Exception {
		if (!checkNull(jobHistory)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(JobHistory jobHistory){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility jobHistoryProperties = new ConfigUtility(new File("i18n/serviceProviderPanel/jobHistory_i18n.properties"));
			Properties jobHistoryPropertyValue = jobHistoryProperties.loadProperties();
			
if (UtilValidate.isEmpty(jobHistory.getServiceProvider())) {
errorMessageMap.put("serviceProvider_validationMessage" , jobHistoryPropertyValue.getProperty(language+ ".serviceProvider.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(JobHistory jobHistory){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
