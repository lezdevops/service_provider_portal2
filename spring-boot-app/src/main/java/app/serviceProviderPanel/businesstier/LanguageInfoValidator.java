package app.serviceProviderPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.LanguageInfo;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.businesstier.LanguageInfoManager;

@Service
public class LanguageInfoValidator {
	@Autowired
	private LanguageInfoManager languageInfoManager;
	public void validate(LanguageInfo languageInfo) throws Exception {
		if (!checkNull(languageInfo)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(LanguageInfo languageInfo){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility languageInfoProperties = new ConfigUtility(new File("i18n/serviceProviderPanel/languageInfo_i18n.properties"));
			Properties languageInfoPropertyValue = languageInfoProperties.loadProperties();
			
if (UtilValidate.isEmpty(languageInfo.getServiceProvider())) {
errorMessageMap.put("serviceProvider_validationMessage" , languageInfoPropertyValue.getProperty(language+ ".serviceProvider.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(LanguageInfo languageInfo){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
