package app.serviceProviderPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.SpServiceList;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.businesstier.SpServiceListManager;

@Service
public class SpServiceListValidator {
	@Autowired
	private SpServiceListManager spServiceListManager;
	public void validate(SpServiceList spServiceList) throws Exception {
		if (!checkNull(spServiceList)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(SpServiceList spServiceList){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility spServiceListProperties = new ConfigUtility(new File("i18n/serviceProviderPanel/spServiceList_i18n.properties"));
			Properties spServiceListPropertyValue = spServiceListProperties.loadProperties();
			
if (UtilValidate.isEmpty(spServiceList.getServiceProvider())) {
errorMessageMap.put("serviceProvider_validationMessage" , spServiceListPropertyValue.getProperty(language+ ".serviceProvider.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(spServiceList.getServiceType())) {
errorMessageMap.put("serviceType_validationMessage" , spServiceListPropertyValue.getProperty(language+ ".serviceType.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(spServiceList.getLocation())) {
errorMessageMap.put("location_validationMessage" , spServiceListPropertyValue.getProperty(language+ ".location.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(SpServiceList spServiceList){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
