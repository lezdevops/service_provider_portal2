package app.serviceProviderPanel.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.JobHistory;
import app.serviceProviderPanel.persistencetier.JobHistoryRepository;


//newDependentBeanImportGoesHere

@Service
public class JobHistoryManager {
	@Autowired
	private JobHistoryRepository jobHistoryRepository;
	@Autowired
	private JobHistoryValidator jobHistoryValidator;
	// newAutowiredPropertiesGoesHere
	
	public JobHistory getJobHistory(Long id){
		JobHistory jobHistory = jobHistoryRepository.getOne(id);
		return jobHistory;
	}

	
	public List<JobHistory> getJobHistorys(){
		List<JobHistory> jobHistorysList = new ArrayList<JobHistory>();
		Iterable<JobHistory> jobHistorys = jobHistoryRepository.findAll();
		for (JobHistory jobHistory : jobHistorys) {
			jobHistorysList.add(jobHistory);
			// newGenerateEntityFetchActionGoesHere
		}
		return jobHistorysList;
	}
	
	
	public Map<String,Object> save(JobHistory jobHistory) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = jobHistoryValidator.getValidated(jobHistory);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/jobHistory");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				jobHistory = jobHistoryRepository.save(jobHistory);
				messageMap.put("successMessage", "Successfully Saved JobHistory Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(JobHistoryManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}
	
	public List<JobHistory> saveMultipleJobHistoryData(List<JobHistory> jobHistorys) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (JobHistory jobHistory : jobHistorys) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = jobHistoryValidator.getValidated(jobHistory);
				jobHistory = jobHistoryRepository.save(jobHistory);
				messageMap.put("addSucessmessage", "Successfully JobHistory Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getJobHistorys();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(JobHistoryManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getJobHistorys();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully JobHistory Deleted.";
		try {
			jobHistoryRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All JobHistory Deleted.";
		jobHistoryRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<JobHistory> findPaginated(int page, int size) {
        return jobHistoryRepository.findAll(new PageRequest(page, size));
    }
	public Page<JobHistory> findPaginatedJobHistorysBySearchToken(int page, int size, String searchToken) {
        return jobHistoryRepository.findPaginatedJobHistorysBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public JobHistory findOne(Long id) {
		JobHistory jobHistory = jobHistoryRepository.findOne(id);
		return jobHistory;
	}
	public Page<JobHistory> findById(Long id,
			Pageable pageRequest) {
		List<JobHistory> jobHistorys = jobHistoryRepository.findById(id);
		Page<JobHistory> pages = null;
		pages = new PageImpl<JobHistory>(jobHistorys, pageRequest, jobHistorys.size());
		return pages;
	}
	
}
