package app.serviceProviderPanel.businesstier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.TrainingInfo;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.persistencetier.TrainingInfoRepository;


//newDependentBeanImportGoesHere

@Service
public class TrainingInfoManager {
	@Autowired
	private TrainingInfoRepository trainingInfoRepository;
	@Autowired
	private TrainingInfoValidator trainingInfoValidator;
	// newAutowiredPropertiesGoesHere
	
	public TrainingInfo getTrainingInfo(Long id){
		TrainingInfo trainingInfo = trainingInfoRepository.getOne(id);
		return trainingInfo;
	}

	
	public List<TrainingInfo> getTrainingInfos(){
		List<TrainingInfo> trainingInfosList = new ArrayList<TrainingInfo>();
		Iterable<TrainingInfo> trainingInfos = trainingInfoRepository.findAll();
		for (TrainingInfo trainingInfo : trainingInfos) {
			trainingInfosList.add(trainingInfo);
			// newGenerateEntityFetchActionGoesHere
		}
		return trainingInfosList;
	}
	
	
	public Map<String,Object> save(TrainingInfo trainingInfo) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = trainingInfoValidator.getValidated(trainingInfo);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/trainingInfo");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				trainingInfo = trainingInfoRepository.save(trainingInfo);
				messageMap.put("successMessage", "Successfully Saved TrainingInfo Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(TrainingInfoManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}

	public List<TrainingInfo> saveMultipleTrainingInfoData(List<TrainingInfo> trainingInfos) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (TrainingInfo trainingInfo : trainingInfos) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = trainingInfoValidator.getValidated(trainingInfo);
				trainingInfo = trainingInfoRepository.save(trainingInfo);
				messageMap.put("addSucessmessage", "Successfully TrainingInfo Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getTrainingInfos();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(TrainingInfoManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getTrainingInfos();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully TrainingInfo Deleted.";
		try {
			trainingInfoRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All TrainingInfo Deleted.";
		trainingInfoRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<TrainingInfo> findPaginated(int page, int size) {
        return trainingInfoRepository.findAll(new PageRequest(page, size));
    }
	public Page<TrainingInfo> findPaginatedTrainingInfosBySearchToken(int page, int size, String searchToken) {
        return trainingInfoRepository.findPaginatedTrainingInfosBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public TrainingInfo findOne(Long id) {
		TrainingInfo trainingInfo = trainingInfoRepository.findOne(id);
		return trainingInfo;
	}
	public Page<TrainingInfo> findById(Long id,
			Pageable pageRequest) {
		List<TrainingInfo> trainingInfos = trainingInfoRepository.findById(id);
		Page<TrainingInfo> pages = null;
		pages = new PageImpl<TrainingInfo>(trainingInfos, pageRequest, trainingInfos.size());
		return pages;
	}
	
}
