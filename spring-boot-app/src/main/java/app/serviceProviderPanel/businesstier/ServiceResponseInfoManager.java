package app.serviceProviderPanel.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.ServiceResponseInfo;
import app.serviceProviderPanel.persistencetier.ServiceResponseInfoRepository;


//newDependentBeanImportGoesHere

@Service
public class ServiceResponseInfoManager {
	@Autowired
	private ServiceResponseInfoRepository serviceResponseInfoRepository;
	@Autowired
	private ServiceResponseInfoValidator serviceResponseInfoValidator;
	// newAutowiredPropertiesGoesHere
	
	public ServiceResponseInfo getServiceResponseInfo(Long id){
		ServiceResponseInfo serviceResponseInfo = serviceResponseInfoRepository.getOne(id);
		return serviceResponseInfo;
	}

	
	public List<ServiceResponseInfo> getServiceResponseInfos(){
		List<ServiceResponseInfo> serviceResponseInfosList = new ArrayList<ServiceResponseInfo>();
		Iterable<ServiceResponseInfo> serviceResponseInfos = serviceResponseInfoRepository.findAll();
		for (ServiceResponseInfo serviceResponseInfo : serviceResponseInfos) {
			serviceResponseInfosList.add(serviceResponseInfo);
			// newGenerateEntityFetchActionGoesHere
		}
		return serviceResponseInfosList;
	}
	
	
	public Map<String,Object> save(ServiceResponseInfo serviceResponseInfo) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = serviceResponseInfoValidator.getValidated(serviceResponseInfo);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/serviceResponseInfo");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				serviceResponseInfo = serviceResponseInfoRepository.save(serviceResponseInfo);
				messageMap.put("successMessage", "Successfully Saved ServiceResponseInfo Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ServiceResponseInfoManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}

	public List<ServiceResponseInfo> saveMultipleServiceResponseInfoData(List<ServiceResponseInfo> serviceResponseInfos) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (ServiceResponseInfo serviceResponseInfo : serviceResponseInfos) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = serviceResponseInfoValidator.getValidated(serviceResponseInfo);
				serviceResponseInfo = serviceResponseInfoRepository.save(serviceResponseInfo);
				messageMap.put("addSucessmessage", "Successfully ServiceResponseInfo Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getServiceResponseInfos();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ServiceResponseInfoManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getServiceResponseInfos();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully ServiceResponseInfo Deleted.";
		try {
			serviceResponseInfoRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All ServiceResponseInfo Deleted.";
		serviceResponseInfoRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<ServiceResponseInfo> findPaginated(int page, int size) {
        return serviceResponseInfoRepository.findAll(new PageRequest(page, size));
    }
	public Page<ServiceResponseInfo> findPaginatedServiceResponseInfosBySearchToken(int page, int size, String searchToken) {
        return serviceResponseInfoRepository.findPaginatedServiceResponseInfosBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public ServiceResponseInfo findOne(Long id) {
		ServiceResponseInfo serviceResponseInfo = serviceResponseInfoRepository.findOne(id);
		return serviceResponseInfo;
	}
	public Page<ServiceResponseInfo> findById(Long id,
			Pageable pageRequest) {
		List<ServiceResponseInfo> serviceResponseInfos = serviceResponseInfoRepository.findById(id);
		Page<ServiceResponseInfo> pages = null;
		pages = new PageImpl<ServiceResponseInfo>(serviceResponseInfos, pageRequest, serviceResponseInfos.size());
		return pages;
	}
	
}
