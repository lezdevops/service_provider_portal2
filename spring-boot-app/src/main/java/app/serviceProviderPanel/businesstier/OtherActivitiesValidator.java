package app.serviceProviderPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.OtherActivities;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.serviceProviderPanel.businesstier.OtherActivitiesManager;

@Service
public class OtherActivitiesValidator {
	@Autowired
	private OtherActivitiesManager otherActivitiesManager;
	public void validate(OtherActivities otherActivities) throws Exception {
		if (!checkNull(otherActivities)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(OtherActivities otherActivities){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility otherActivitiesProperties = new ConfigUtility(new File("i18n/serviceProviderPanel/otherActivities_i18n.properties"));
			Properties otherActivitiesPropertyValue = otherActivitiesProperties.loadProperties();
			
if (UtilValidate.isEmpty(otherActivities.getServiceProvider())) {
errorMessageMap.put("serviceProvider_validationMessage" , otherActivitiesPropertyValue.getProperty(language+ ".serviceProvider.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(OtherActivities otherActivities){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
