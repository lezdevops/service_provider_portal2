package app.common.persistencetier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.base.BaseService;
import app.globalSetup.persistencetier.RoleRepository;

/**
 * Created by eclipse
 */
@Service
public class RoleService extends BaseService{


    @Autowired
    RoleService(RoleRepository roleRepository){
        super(roleRepository,"Role");
    }

}
