package app.common.persistencetier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.Role;
import app.appmodel.User;
import app.base.BaseService;
import app.globalSetup.persistencetier.RoleRepository;
import app.globalSetup.persistencetier.UserRepository;

/**
 * Created by eclipse
 */
@Service
public class UserService extends BaseService {

  private UserRepository userRepository;
  private RoleRepository roleRepository;

  @Autowired
  UserService(UserRepository userRepository,RoleRepository roleRepository){
    super(userRepository,"User");
    this.userRepository = userRepository;
    this.roleRepository = roleRepository;
  }

  public User findUserByEmail(String email) {
	  return userRepository.findUserByEmail(email);  
  }
  public User findByUserName(String userName) {
    return userRepository.findUserByUserName(userName);
  }

  public User findByEmail(String email) {
    return userRepository.findUserByEmail(email);
  }


  public void saveUser(User user) {
    Role role = roleRepository.findByName("ROLE_USER");
    user.getRoles().add(role);
    user.setActive(Long.valueOf(1));
    this.userRepository.save(user);
  }
}
