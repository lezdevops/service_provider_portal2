package app.common.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import app.framework.businesstier.FileUploadUtils;

public class EmailManager {
	public static List<MimeBodyPart> getMimeBodyPartByFiles(List<File> files, boolean attachmentCompression) {
		List<MimeBodyPart> mimeBodyParts = new ArrayList<MimeBodyPart>();
		if (attachmentCompression) {
			files = new ArrayList<File>();
			String fileName = "for-email/attachments.zip";
			File zippedFile = FileUploadUtils.getZippedFile(files, fileName);
			files = new ArrayList<File>();
			files.add(zippedFile);
		}
		for (File file : files) {
			if (file.exists()) {
				MimeBodyPart attachmentBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource(file);
				try {
					attachmentBodyPart.setDataHandler(new DataHandler(source));
					attachmentBodyPart.setFileName(file.getName());
				} catch (MessagingException e) {
					e.printStackTrace();
				}
				mimeBodyParts.add(attachmentBodyPart);
			}
		}
		return mimeBodyParts;
	}

	public static void send(Set<String> recipients,String subject, StringBuilder emailBody, StringBuilder emailFooter,
			List<File> attachmentFiles, boolean attachmentCompression) throws Exception{
		Properties props = new Properties();
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.starttls.enable", true);
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		final String username = "arif.bjitgroup@gmail.com";
		final String password = "bdtech@1234";

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			for (String recipient : recipients) {
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
			}

			message.setSubject(subject);
			message.setText("SP Portal");

			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(emailBody.toString(), "text/html");
			
			MimeBodyPart messageFooterPart = new MimeBodyPart();
			messageFooterPart.setContent(emailFooter.toString(), "text/html");
			
			List<File> files = new ArrayList<File>();
			
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			multipart.addBodyPart(messageFooterPart);
			
			List<MimeBodyPart> mimeBodyParts = getMimeBodyPartByFiles(files, attachmentCompression);
			for (MimeBodyPart mimeBodyPart : mimeBodyParts) {
				multipart.addBodyPart(mimeBodyPart);
			}

			message.setContent(multipart);

			System.out.println("Sending");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			/*
			 * Message: 535-5.7.8 Username and Password not accepted. Learn more at 535
			 * 5.7.8 https://support.google.com/mail/?p=BadCredentials m3sm7616219pgp.85 -
			 * gsmtp
			 */
			String message = e.getMessage();
			System.out.println("Message: " + e.getMessage());
			System.out.println("Cause: " + e.getCause());
			if (message.contains("Username and Password not accepted.")) {
				throw new Exception("BAD_CREDENTIALS");
				
			}else {
				
				throw new Exception("EMAIL_SENDING_FAILED.");
			}
		}

	}
}
