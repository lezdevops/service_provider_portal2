package app.common.presentationtier;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import app.appmodel.User;
import app.base.BaseController;
import app.common.persistencetier.UserService;

@RestController
public class UserController extends BaseController {


  BCryptPasswordEncoder bCryptPasswordEncoder;

  private UserService userService;

  @Autowired
  UserController(UserService userService,BCryptPasswordEncoder bCryptPasswordEncoder){
    super(userService);
    this.userService = userService;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
  }




  @CrossOrigin
  @RequestMapping(value = "/users", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
  public Object createNewUser(@RequestBody User user, BindingResult bindingResult, HttpServletRequest request) throws JsonProcessingException {
    User userExists = userService.findByUserName(user.getUserName());
    Map responseMap = new HashMap();
    if (userExists != null) {
      responseMap.put("error", "User Already Exist");
    }
    user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

    if (bindingResult.hasErrors()) {
      responseMap.put("error", "Something is wrong");

    } else {
      userService.saveUser(user);
      responseMap.put("success", "Successfully Inserted");
    }
    return responseMap;
  }


  @CrossOrigin
  @RequestMapping(value = "/users", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
  public Object list(@RequestParam String params, @RequestParam("pageNum") Integer pageNum, HttpServletRequest req, HttpServletResponse resp) throws Exception {
    return this.list(params, pageNum);
  }

  @CrossOrigin
  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public Object login(@RequestParam String params, HttpServletRequest req, HttpServletResponse resp) throws Exception {
    //System.out.println("login" + params);
    return null;
  }


}
