package app.common.presentationtier;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.appmodel.Permission;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.PermissionManager;
import app.globalSetup.businesstier.UserManager;
// newDependentBeanImportGoesHere
import app.security.httpRequest.HttpRequestProcessor;

@RestController
@RequestMapping(value = "/")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules Permission API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class PermissionCheckerAPI {

	@Autowired
	PermissionManager permissionManager;
	@Autowired
	UserManager userManager;
	@Autowired
	HttpRequestProcessor httpRequestProcessor;

	@RequestMapping(value = "/hasPermission/check", params = { "pageName" },  method = RequestMethod.GET)
	public Map<String,Object> validatePermission( HttpServletRequest request, @RequestParam("pageName") String pageName) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		try {
			User user = httpRequestProcessor.getUser(request);
			if (UtilValidate.isNotEmpty(user)) {
				return httpRequestProcessor.getPermissionMap(request, pageName);
			} else {
				errorMessageMap.put("permission_validationMessage", "User Not Found!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("error", "Technical Error! Cause: "+e.getMessage());
		}
		
		return errorMessageMap;
	 }
	public List<User> getUsersByPermission(HttpServletRequest request, String permissionName){
		List<User> permittedUserList = new ArrayList<User>();
		List<User> userList = userManager.getUsers(request);
		
		Permission permission = permissionManager.findPermissionByName(permissionName);
		if (UtilValidate.isNotEmpty(permission)) {
			Role role = permission.getRole();
			for (User user : userList) {
				List<Role> roles = user.getRoles();
				for (Role roleObj : roles) {
					if (roleObj.getName().equalsIgnoreCase(role.getName())) {
						permittedUserList.add(user);
					}
				}
				
			}
			
		}
		
		return permittedUserList;
	}
	
	
}

