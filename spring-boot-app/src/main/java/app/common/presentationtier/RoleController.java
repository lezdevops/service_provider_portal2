package app.common.presentationtier;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.base.BaseController;
import app.common.persistencetier.RoleService;

@RestController
public class RoleController extends BaseController {


  @Autowired
  RoleController(RoleService service) {
    super(service);
  }


  @CrossOrigin
  @RequestMapping(value = "/roles", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
  public Object save(@RequestBody String payload, HttpServletResponse resp, BindingResult res) throws Exception {
    return this.save(payload);
  }

  @CrossOrigin
  @RequestMapping(value = "/roles", method = RequestMethod.GET)
  public Object list(@RequestParam String params, @RequestParam("pageNum") Integer pageNum, HttpServletRequest req, HttpServletResponse resp) throws Exception {
    return this.list(params, pageNum);
  }


}
