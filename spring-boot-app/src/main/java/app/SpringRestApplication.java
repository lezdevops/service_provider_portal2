package app;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication(scanBasePackages = {"app"})
public class SpringRestApplication  implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestApplication.class, args);
	}

	//@Autowired
	//private PersonRepository personRepository;
	@Override
	public void run(String... args) throws Exception {
		//personRepository.save(new Person("one", "one"));
		//personRepository.save(new Person("two", "one"));
		//personRepository.save(new Person("three", "one"));
		
	}
	
	

  @Bean
  public BCryptPasswordEncoder bCryptPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }

}
