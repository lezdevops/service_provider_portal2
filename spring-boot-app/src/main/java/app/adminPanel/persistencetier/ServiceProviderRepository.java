package app.adminPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.ServiceProvider;

@Repository
public interface ServiceProviderRepository extends JpaRepository<ServiceProvider, Long>{
	List<ServiceProvider> findById(Long id);
	ServiceProvider findServiceProviderById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT serviceProvider FROM ServiceProvider serviceProvider WHERE  cast(serviceProvider.name AS string) like %:searchToken% OR cast(serviceProvider.about AS string) like %:searchToken% OR cast(serviceProvider.address AS string) like %:searchToken% OR cast(serviceProvider.zipCode AS string) like %:searchToken% OR cast(serviceProvider.phoneNo AS string) like %:searchToken% OR cast(serviceProvider.nidNo AS string) like %:searchToken% OR cast(serviceProvider.email AS string) like %:searchToken% OR cast(serviceProvider.user.userName AS string) like %:searchToken% OR cast(serviceProvider.spLevel.description AS string) like %:searchToken% OR cast(serviceProvider.gender AS string) like %:searchToken% OR cast(serviceProvider.desiredHourlyRate AS string) like %:searchToken% OR cast(serviceProvider.desiredDailyRate AS string) like %:searchToken% OR cast(serviceProvider.serviceArea AS string) like %:searchToken% OR cast(serviceProvider.availability AS string) like %:searchToken% OR cast(serviceProvider.isActive AS string) like %:searchToken% OR cast(serviceProvider.startTime AS string) like %:searchToken% OR cast(serviceProvider.endTime AS string) like %:searchToken% OR cast(serviceProvider.createdDate AS string) like %:searchToken% OR cast(serviceProvider.profileImage AS string) like %:searchToken%")
	Page<ServiceProvider> findPaginatedServiceProvidersBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
