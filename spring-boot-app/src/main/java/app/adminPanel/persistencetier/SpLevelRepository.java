package app.adminPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.SpLevel;

@Repository
public interface SpLevelRepository extends JpaRepository<SpLevel, Long>{
	List<SpLevel> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT spLevel FROM SpLevel spLevel WHERE  cast(spLevel.shortCode AS string) like %:searchToken% OR cast(spLevel.description AS string) like %:searchToken% OR cast(spLevel.isActive AS string) like %:searchToken%")
	Page<SpLevel> findPaginatedSpLevelsBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
