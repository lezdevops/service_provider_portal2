package app.adminPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.TrainingType;

@Repository
public interface TrainingTypeRepository extends JpaRepository<TrainingType, Long>{
	List<TrainingType> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT trainingType FROM TrainingType trainingType WHERE  cast(trainingType.shortCode AS string) like %:searchToken% OR cast(trainingType.description AS string) like %:searchToken% OR cast(trainingType.isActive AS string) like %:searchToken%")
	Page<TrainingType> findPaginatedTrainingTypesBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
