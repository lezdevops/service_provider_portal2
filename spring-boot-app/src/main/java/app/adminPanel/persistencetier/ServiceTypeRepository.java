package app.adminPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.ServiceType;

@Repository
public interface ServiceTypeRepository extends JpaRepository<ServiceType, Long>{
	List<ServiceType> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT serviceType FROM ServiceType serviceType WHERE  cast(serviceType.shortCode AS string) like %:searchToken% OR cast(serviceType.description AS string) like %:searchToken% OR cast(serviceType.isActive AS string) like %:searchToken%")
	Page<ServiceType> findPaginatedServiceTypesBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
