package app.adminPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{
	List<Customer> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT customer FROM Customer customer WHERE  cast(customer.name AS string) like %:searchToken% OR cast(customer.securityNumber AS string) like %:searchToken% OR cast(customer.profileImage AS string) like %:searchToken% OR cast(customer.email AS string) like %:searchToken% OR cast(customer.createdDate AS string) like %:searchToken% OR cast(customer.user.userName AS string) like %:searchToken% OR cast(customer.isActive AS string) like %:searchToken%")
	Page<Customer> findPaginatedCustomersBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
