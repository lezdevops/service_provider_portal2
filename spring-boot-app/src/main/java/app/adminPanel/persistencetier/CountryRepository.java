package app.adminPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long>{
	List<Country> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT country FROM Country country WHERE  cast(country.currency AS string) like %:searchToken% OR cast(country.description AS string) like %:searchToken% OR cast(country.createdDate AS string) like %:searchToken%")
	Page<Country> findPaginatedCountrysBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
