package app.adminPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.District;

@Repository
public interface DistrictRepository extends JpaRepository<District, Long>{
	List<District> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT district FROM District district WHERE  cast(district.division.description AS string) like %:searchToken% OR cast(district.description AS string) like %:searchToken% OR cast(district.createdDate AS string) like %:searchToken%")
	Page<District> findPaginatedDistrictsBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
