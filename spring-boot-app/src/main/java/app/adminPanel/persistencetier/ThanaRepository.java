package app.adminPanel.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.Thana;

@Repository
public interface ThanaRepository extends JpaRepository<Thana, Long>{
	List<Thana> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT thana FROM Thana thana WHERE  cast(thana.district.description AS string) like %:searchToken% OR cast(thana.description AS string) like %:searchToken% OR cast(thana.createdDate AS string) like %:searchToken%")
	Page<Thana> findPaginatedThanasBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
