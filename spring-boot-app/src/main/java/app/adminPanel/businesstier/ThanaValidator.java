package app.adminPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.Thana;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.businesstier.ThanaManager;

@Service
public class ThanaValidator {
	@Autowired
	private ThanaManager thanaManager;
	public void validate(Thana thana) throws Exception {
		if (!checkNull(thana)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(Thana thana){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility thanaProperties = new ConfigUtility(new File("i18n/adminPanel/thana_i18n.properties"));
			Properties thanaPropertyValue = thanaProperties.loadProperties();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(Thana thana){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
