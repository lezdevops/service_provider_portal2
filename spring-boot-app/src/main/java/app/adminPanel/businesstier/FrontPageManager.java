package app.adminPanel.businesstier;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import app.adminPanel.persistencetier.ServiceProviderRepository;
import app.appmodel.RegistrationQueue;
import app.appmodel.ServiceProvider;
import app.common.businesstier.EmailManager;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.RegistrationQueueValidator;
import app.globalSetup.persistencetier.RegistrationQueueRepository;
import net.bytebuddy.utility.RandomString;


//newDependentBeanImportGoesHere

@Service
public class FrontPageManager {
	private static final org.apache.log4j.Logger RegistrationQueueManagerLogger = org.apache.log4j.Logger.getLogger(FrontPageManager.class);
	@Autowired
	private RegistrationQueueRepository registrationQueueRepository;@Autowired
	private ServiceProviderRepository serviceProviderRepository;
	
	@Autowired
	private RegistrationQueueValidator registrationQueueValidator;
	
	BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	FrontPageManager(BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getRegistrationQueueForEdit(HttpServletRequest request, Long id){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_EDIT");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		RegistrationQueue registrationQueue = null;
		try {
			registrationQueue = registrationQueueRepository.getOne(id);
			
			returnResult.put("registrationQueue", registrationQueue);
		} catch (Exception e) {
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching RegistrationQueue Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public Map<String,Object> getRegistrationQueue(HttpServletRequest request, Long id){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			RegistrationQueue registrationQueue = registrationQueueRepository.getOne(id);
			
			returnResult.put("registrationQueue", registrationQueue);
		} catch (Exception e) {
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching RegistrationQueue Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getRegistrationQueues(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<RegistrationQueue> registrationQueuesList = new ArrayList<RegistrationQueue>();
			Iterable<RegistrationQueue> registrationQueues = registrationQueueRepository.findAll();
			for (RegistrationQueue registrationQueue : registrationQueues) {
				registrationQueuesList.add(registrationQueue);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("registrationQueues", registrationQueuesList);
		} catch (Exception e) {
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching RegistrationQueue List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public List<RegistrationQueue> getRegistrationQueueesByEmail(String email){
		List<RegistrationQueue> registrationQueuesList = new ArrayList<RegistrationQueue>();
		Iterable<RegistrationQueue> registrationQueues = registrationQueueRepository.findByEmail(email);
		for (RegistrationQueue registrationQueue : registrationQueues) {	
			registrationQueuesList.add(registrationQueue);
		}
		return registrationQueuesList;
	}
	public Map<String,Object> create(HttpServletRequest request, RegistrationQueue registrationQueue) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("REGISTRATIONQUEUE_CREATE");
			
			//returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = registrationQueueValidator.getValidated(request,registrationQueue);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/registrationQueue");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				String email = registrationQueue.getEmail();
				String userName = registrationQueue.getUserName();
				String accountType = registrationQueue.getAccountType();

				try {
					String randomString = RandomString.make(10);
					sendEmailForVerification(userName,email,accountType,randomString);
					
					registrationQueue.setJsonString(randomString);
					
					String passwordInText = registrationQueue.getPassword();
					if (UtilValidate.isNotEmpty(passwordInText)) {
						registrationQueue.setPassword(bCryptPasswordEncoder.encode(passwordInText));
					}
					
					registrationQueue = registrationQueueRepository.save(registrationQueue);
					registrationQueue.setApprovalStatus("REGISTRATION_REQUESTED");
					registrationQueue.setPassword(passwordInText);
					registrationQueue.setJsonString("");
					
					returnResult.put("emailVerificationMessage", "Verification Email is sent to : "+email);
					returnResult.put("registrationQueue", registrationQueue);
					returnResult.put("successMessage", "Successfully Saved RegistrationQueue Data.");
				} catch (Exception e) {
					returnResult.put("error", "found");
					returnResult.put("email_validationMessage", "Email Is Not Valid.");
				}
				
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Creating RegistrationQueue Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public void sendEmailForVerification(String userName, String email, String accountType, String randomString) throws Exception {
		Set<String> recipients = new LinkedHashSet<String>();
		recipients.add(email);
		String subject = "Application Registration: Email Verification Needed.";
		
		Timestamp today = HelperUtils.getToday();
		StringBuilder emailBody = new StringBuilder("<html>");
		emailBody.append("<h1>Dear "+userName+", Please Find Following Information Below:</h1>");
		emailBody.append("<ul>");
			emailBody.append("<li>Email: "+email+"</li>");
		emailBody.append("<li>Account Type: "+accountType+"</li>");
		emailBody.append("<li>Verification Code: "+randomString+"</li>");
			emailBody.append("<li>Registration Date: "+today+"</li>");
		emailBody.append("</ul>");
		
		StringBuilder emailFooter = new StringBuilder("<div>");
		emailFooter.append("<p>This email may contain confidential and/or legally privileged information. "
				+ "If you have received it in error, please notify the sender immediately and delete it "
				+ "(together with any attachments) from your system without using or "
				+ "disclosing its contents for any purposes or to any other person. Many thanks for your co-operation.</p>");
		emailFooter.append("</div>");
		emailFooter.append("</html>");
		List<File> attachmentFiles = new ArrayList<File>();
		EmailManager.send(recipients, subject, emailBody, emailFooter, attachmentFiles, false);
	}
	public Map<String,Object> update(HttpServletRequest request, RegistrationQueue registrationQueue) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("REGISTRATIONQUEUE_EDIT");
			
			//returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				//returnResult = registrationQueueValidator.getValidated(request,registrationQueue);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/registrationQueue");
				if (!path.exists()) {
					path.mkdirs();
				}
				Long id = registrationQueue.getId();
				Map<String,Object> resultMap = findOne(request, id);
				if (UtilValidate.isNotEmpty(resultMap.get("registrationQueue"))) {
					
					RegistrationQueue registrationQueueOld = (RegistrationQueue)resultMap.get("registrationQueue");
					
					String jsonString = registrationQueue.getJsonString();
					if (registrationQueueOld.getJsonString().equalsIgnoreCase(jsonString)) {
						String email = registrationQueue.getEmail();
						String userName = registrationQueue.getUserName();
						String accountType = registrationQueue.getAccountType();
						registrationQueue.setPassword(registrationQueueOld.getPassword());
						
						try {
							sendEmailForSuccessfulVerification(userName, email, accountType);
							
							registrationQueue.setApprovalStatus("EMAIL_VERIFIED");
							
							registrationQueue = registrationQueueRepository.save(registrationQueue);
							registrationQueue.setPassword("");
							returnResult.put("registrationQueue", registrationQueue);
							
							returnResult.put("emailVerificationMessage", "Successfully Email verified.");
						} catch (Exception e) {
							returnResult.put("emailVerificationMessage", "Sorry Email cannot be verified.");
						}
						
						
					}else {
						returnResult.put("emailVerificationMessage", "Sorry Email cannot be verified.");
					}
					
				}
				
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(FrontPageManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Updating RegistrationQueue Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public void sendEmailForSuccessfulVerification(String userName, String email, String accountType) throws Exception {
		Set<String> recipients = new LinkedHashSet<String>();
		recipients.add(email);
		String subject = "Application Registration: Email Is Verified Successfully.";
		
		Timestamp today = HelperUtils.getToday();
		StringBuilder emailBody = new StringBuilder("<html>");
		emailBody.append("<h1>Dear "+userName+", Please Find Following Information Below:</h1>");
		emailBody.append("<ul>");
		emailBody.append("<li>Email: "+email+"</li>");
		emailBody.append("<li>Account Type: "+accountType+"</li>");
		emailBody.append("<li>Registration Date: "+today+"</li>");
		emailBody.append("<li>Registration Status: Waiting For Requesters Final Data Entry.</li>");
		emailBody.append("</ul>");
		
		StringBuilder emailFooter = new StringBuilder("<div>");
		emailFooter.append("<p>This email may contain confidential and/or legally privileged information. "
				+ "If you have received it in error, please notify the sender immediately and delete it "
				+ "(together with any attachments) from your system without using or "
				+ "disclosing its contents for any purposes or to any other person. Many thanks for your co-operation.</p>");
		emailFooter.append("</div>");
		emailFooter.append("</html>");
		List<File> attachmentFiles = new ArrayList<File>();
		EmailManager.send(recipients, subject, emailBody, emailFooter, attachmentFiles, false);
	}
	
	public Map<String,Object> finalStage(HttpServletRequest request, RegistrationQueue registrationQueue) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("REGISTRATIONQUEUE_EDIT");
			
			//returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				//returnResult = registrationQueueValidator.getValidated(request,registrationQueue);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/registrationQueue");
				if (!path.exists()) {
					path.mkdirs();
				}
				
				try {
					Long id = registrationQueue.getId();
					Map<String,Object> resultMap = findOne(request, id);
					if (UtilValidate.isNotEmpty(resultMap.get("registrationQueue"))) {
						
						RegistrationQueue registrationQueueOld = (RegistrationQueue)resultMap.get("registrationQueue");
						registrationQueue.setPassword(registrationQueueOld.getPassword());
						
						String userName = registrationQueue.getUserName();
						//String passwordInText = registrationQueue.getPassword();
						String email = registrationQueue.getEmail();
						String accountType = registrationQueue.getAccountType();
						//registrationQueue.setPassword(bCryptPasswordEncoder.encode(passwordInText));
						registrationQueue.setApprovalStatus("FINAL_REGISTRATION_ENTRY");
						
						sendEmailForFinalStage(userName, email, accountType);
						
						registrationQueue = registrationQueueRepository.save(registrationQueue);
						
						registrationQueue.setPassword("");
						returnResult.put("registrationQueue", registrationQueue);
						returnResult.put("finalStageEmailSending", "We have sent email to "+email+", Please See The email for details.");
						returnResult.put("emailVerificationMessage", "Successfully Final Stage Completed.");
						
					}
					
				} catch (Exception e) {
					returnResult.put("finalStageEmailSending", "Failed to send email.");
				}
				
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(FrontPageManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Updating RegistrationQueue Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public void sendEmailForFinalStage(String userName, String email, String accountType) throws Exception {
		Set<String> recipients = new LinkedHashSet<String>();
		recipients.add(email);
		String subject = "Application Registration: Successfully Registration Completed.";
		
		Timestamp today = HelperUtils.getToday();
		StringBuilder emailBody = new StringBuilder("<html>");
		emailBody.append("<h1>Dear "+userName+", Please Find Following Information Below:</h1>");
		emailBody.append("<ul>");
		emailBody.append("<li>Email: "+email+"</li>");
		emailBody.append("<li>Account Type: "+accountType+"</li>");
		emailBody.append("<li>User Name: "+userName+"</li>");
		emailBody.append("<li>Password: '****************'</li>");
		emailBody.append("<li>Registration Date: "+today+"</li>");
		emailBody.append("<li>Registration Status: Pending For Admin Approval</li>");
		emailBody.append("</ul>");
		
		StringBuilder emailFooter = new StringBuilder("<div>");
		emailFooter.append("<p>This email may contain confidential and/or legally privileged information. "
				+ "If you have received it in error, please notify the sender immediately and delete it "
				+ "(together with any attachments) from your system without using or "
				+ "disclosing its contents for any purposes or to any other person. Many thanks for your co-operation.</p>");
		emailFooter.append("</div>");
		emailFooter.append("</html>");
		List<File> attachmentFiles = new ArrayList<File>();
		EmailManager.send(recipients, subject, emailBody, emailFooter, attachmentFiles, false);
	}
	
	public Map<String,Object> saveMultipleRegistrationQueueData(HttpServletRequest request, List<RegistrationQueue> registrationQueues) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_CREATE");
		
		//returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (RegistrationQueue registrationQueue : registrationQueues) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = registrationQueueValidator.getValidated(request, registrationQueue);
				registrationQueue = registrationQueueRepository.save(registrationQueue);
				returnResult.put(registrationQueue+" "+registrationQueue.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Adding RegistrationQueue List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully RegistrationQueue Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_DELETE");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			RegistrationQueue registrationQueue = registrationQueueRepository.findOne(id);
			if (UtilValidate.isNotEmpty(registrationQueue)) {
				
				registrationQueueRepository.delete(id);
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Removing RegistrationQueue Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_DELETE");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All RegistrationQueue Deleted.";
		try {
			registrationQueueRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Removing RegistrationQueue Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<RegistrationQueue> paginatedResult = registrationQueueRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching Paginated RegistrationQueue Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Page<ServiceProvider> findPaginated(int page, int size) {
        return serviceProviderRepository.findAll(new PageRequest(page, size));
    }
	public Page<ServiceProvider> findPaginatedServiceProvidersBySearchToken(int page, int size, String searchToken) {
        return serviceProviderRepository.findPaginatedServiceProvidersBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public Map<String,Object> findPaginatedRegistrationQueuesBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<RegistrationQueue> paginatedResultBySearchToken = registrationQueueRepository.findPaginatedRegistrationQueuesBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching Paginated RegistrationQueue Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		//returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			RegistrationQueue registrationQueue = registrationQueueRepository.findOne(id);
			returnResult.put("registrationQueue", registrationQueue);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching RegistrationQueue Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<RegistrationQueue> registrationQueues = new ArrayList<RegistrationQueue>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			registrationQueues = registrationQueueRepository.findById(id);
			Page<RegistrationQueue> pages = new PageImpl<RegistrationQueue>(registrationQueues, pageRequest, registrationQueues.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching RegistrationQueue Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
