package app.adminPanel.businesstier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.District;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.persistencetier.DistrictRepository;


//newDependentBeanImportGoesHere

@Service
public class DistrictManager {
	@Autowired
	private DistrictRepository districtRepository;
	@Autowired
	private DistrictValidator districtValidator;
	// newAutowiredPropertiesGoesHere
	
	public District getDistrict(Long id){
		District district = districtRepository.getOne(id);
		return district;
	}

	
	public List<District> getDistricts(){
		List<District> districtsList = new ArrayList<District>();
		Iterable<District> districts = districtRepository.findAll();
		for (District district : districts) {
			districtsList.add(district);
			// newGenerateEntityFetchActionGoesHere
		}
		return districtsList;
	}
	
	
	public Map<String,Object> save(District district) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = districtValidator.getValidated(district);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/district");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				district = districtRepository.save(district);
				messageMap.put("successMessage", "Successfully Saved District Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(DistrictManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}

	public List<District> saveMultipleDistrictData(List<District> districts) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (District district : districts) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = districtValidator.getValidated(district);
				district = districtRepository.save(district);
				messageMap.put("addSucessmessage", "Successfully District Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getDistricts();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(DistrictManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getDistricts();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully District Deleted.";
		try {
			districtRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All District Deleted.";
		districtRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<District> findPaginated(int page, int size) {
        return districtRepository.findAll(new PageRequest(page, size));
    }
	public Page<District> findPaginatedDistrictsBySearchToken(int page, int size, String searchToken) {
        return districtRepository.findPaginatedDistrictsBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public District findOne(Long id) {
		District district = districtRepository.findOne(id);
		return district;
	}
	public Page<District> findById(Long id,
			Pageable pageRequest) {
		List<District> districts = districtRepository.findById(id);
		Page<District> pages = null;
		pages = new PageImpl<District>(districts, pageRequest, districts.size());
		return pages;
	}
	
}
