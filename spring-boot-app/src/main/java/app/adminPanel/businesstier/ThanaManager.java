package app.adminPanel.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.adminPanel.persistencetier.ThanaRepository;
import app.appmodel.Thana;


//newDependentBeanImportGoesHere

@Service
public class ThanaManager {
	@Autowired
	private ThanaRepository thanaRepository;
	@Autowired
	private ThanaValidator thanaValidator;
	// newAutowiredPropertiesGoesHere
	
	public Thana getThana(Long id){
		Thana thana = thanaRepository.getOne(id);
		return thana;
	}

	
	public List<Thana> getThanas(){
		List<Thana> thanasList = new ArrayList<Thana>();
		Iterable<Thana> thanas = thanaRepository.findAll();
		for (Thana thana : thanas) {
			thanasList.add(thana);
			// newGenerateEntityFetchActionGoesHere
		}
		return thanasList;
	}
	
	
	public Map<String,Object> save(Thana thana) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = thanaValidator.getValidated(thana);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/thana");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				thana = thanaRepository.save(thana);
				messageMap.put("successMessage", "Successfully Saved Thana Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ThanaManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}

	public List<Thana> saveMultipleThanaData(List<Thana> thanas) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (Thana thana : thanas) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = thanaValidator.getValidated(thana);
				thana = thanaRepository.save(thana);
				messageMap.put("addSucessmessage", "Successfully Thana Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getThanas();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ThanaManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getThanas();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully Thana Deleted.";
		try {
			thanaRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All Thana Deleted.";
		thanaRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<Thana> findPaginated(int page, int size) {
        return thanaRepository.findAll(new PageRequest(page, size));
    }
	public Page<Thana> findPaginatedThanasBySearchToken(int page, int size, String searchToken) {
        return thanaRepository.findPaginatedThanasBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public Thana findOne(Long id) {
		Thana thana = thanaRepository.findOne(id);
		return thana;
	}
	public Page<Thana> findById(Long id,
			Pageable pageRequest) {
		List<Thana> thanas = thanaRepository.findById(id);
		Page<Thana> pages = null;
		pages = new PageImpl<Thana>(thanas, pageRequest, thanas.size());
		return pages;
	}
	
}
