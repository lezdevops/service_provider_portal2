package app.adminPanel.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.adminPanel.persistencetier.ServiceTypeRepository;
import app.appmodel.ServiceType;


//newDependentBeanImportGoesHere

@Service
public class ServiceTypeManager {
	@Autowired
	private ServiceTypeRepository serviceTypeRepository;
	@Autowired
	private ServiceTypeValidator serviceTypeValidator;
	// newAutowiredPropertiesGoesHere
	
	public ServiceType getServiceType(Long id){
		ServiceType serviceType = serviceTypeRepository.getOne(id);
		return serviceType;
	}

	
	public List<ServiceType> getServiceTypes(){
		List<ServiceType> serviceTypesList = new ArrayList<ServiceType>();
		Iterable<ServiceType> serviceTypes = serviceTypeRepository.findAll();
		for (ServiceType serviceType : serviceTypes) {
			serviceTypesList.add(serviceType);
			// newGenerateEntityFetchActionGoesHere
		}
		return serviceTypesList;
	}
	
	
	public Map<String,Object> save(ServiceType serviceType) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = serviceTypeValidator.getValidated(serviceType);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/serviceType");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				serviceType = serviceTypeRepository.save(serviceType);
				messageMap.put("successMessage", "Successfully Saved ServiceType Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ServiceTypeManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}
	
	public List<ServiceType> saveMultipleServiceTypeData(List<ServiceType> serviceTypes) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (ServiceType serviceType : serviceTypes) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = serviceTypeValidator.getValidated(serviceType);
				serviceType = serviceTypeRepository.save(serviceType);
				messageMap.put("addSucessmessage", "Successfully ServiceType Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getServiceTypes();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ServiceTypeManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getServiceTypes();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully ServiceType Deleted.";
		try {
			serviceTypeRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All ServiceType Deleted.";
		serviceTypeRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<ServiceType> findPaginated(int page, int size) {
        return serviceTypeRepository.findAll(new PageRequest(page, size));
    }
	public Page<ServiceType> findPaginatedServiceTypesBySearchToken(int page, int size, String searchToken) {
        return serviceTypeRepository.findPaginatedServiceTypesBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public ServiceType findOne(Long id) {
		ServiceType serviceType = serviceTypeRepository.findOne(id);
		return serviceType;
	}
	public Page<ServiceType> findById(Long id,
			Pageable pageRequest) {
		List<ServiceType> serviceTypes = serviceTypeRepository.findById(id);
		Page<ServiceType> pages = null;
		pages = new PageImpl<ServiceType>(serviceTypes, pageRequest, serviceTypes.size());
		return pages;
	}
	
}
