package app.adminPanel.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.Customer;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.businesstier.CustomerManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class CustomerValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private CustomerManager customerManager;
	public void validate(Customer customer) throws Exception {
		if (!checkNull(customer)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("permission_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}
	
	public Map<String,Object> getValidated(HttpServletRequest request, Customer customer){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility customerProperties = new ConfigUtility(new File("i18n/adminPanel/customer_i18n.properties"));
			Properties customerPropertyValue = customerProperties.loadProperties();
			
if (UtilValidate.isEmpty(customer.getName())) {
errorMessageMap.put("name_validationMessage" , customerPropertyValue.getProperty(language+ ".name.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(customer.getSecurityNumber())) {
errorMessageMap.put("securityNumber_validationMessage" , customerPropertyValue.getProperty(language+ ".securityNumber.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(customer.getEmail())) {
errorMessageMap.put("email_validationMessage" , customerPropertyValue.getProperty(language+ ".email.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(customer.getUser())) {
errorMessageMap.put("user_validationMessage" , customerPropertyValue.getProperty(language+ ".user.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(customer.getIsActive())) {
errorMessageMap.put("isActive_validationMessage" , customerPropertyValue.getProperty(language+ ".isActive.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(Customer customer){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
