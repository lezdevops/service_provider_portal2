package app.adminPanel.businesstier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.ServiceProvider;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.persistencetier.ServiceProviderRepository;


//newDependentBeanImportGoesHere

@Service
public class ServiceProviderManager {
	private static final org.apache.log4j.Logger ServiceProviderManagerLogger = org.apache.log4j.Logger.getLogger(ServiceProviderManager.class);
	@Autowired
	private ServiceProviderRepository serviceProviderRepository;
	@Autowired
	private ServiceProviderValidator serviceProviderValidator;
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getServiceProviderForEdit(HttpServletRequest request, Long id){
		ServiceProvider newServiceProvider = new ServiceProvider();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("SERVICEPROVIDER_EDIT");
		
		returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		ServiceProvider serviceProvider = null;
		try {
			serviceProvider = serviceProviderRepository.getOne(id);
			if (UtilValidate.isNotEmpty(serviceProvider)) {
				newServiceProvider.setId(serviceProvider.getId());newServiceProvider.setName(serviceProvider.getName());newServiceProvider.setAbout(serviceProvider.getAbout());newServiceProvider.setAddress(serviceProvider.getAddress());newServiceProvider.setZipCode(serviceProvider.getZipCode());newServiceProvider.setPhoneNo(serviceProvider.getPhoneNo());newServiceProvider.setNidNo(serviceProvider.getNidNo());newServiceProvider.setEmail(serviceProvider.getEmail());newServiceProvider.setUser(serviceProvider.getUser());newServiceProvider.setSpLevel(serviceProvider.getSpLevel());newServiceProvider.setGender(serviceProvider.getGender());newServiceProvider.setDesiredHourlyRate(serviceProvider.getDesiredHourlyRate());newServiceProvider.setDesiredDailyRate(serviceProvider.getDesiredDailyRate());newServiceProvider.setServiceArea(serviceProvider.getServiceArea());newServiceProvider.setAvailability(serviceProvider.getAvailability());newServiceProvider.setIsActive(serviceProvider.getIsActive());newServiceProvider.setStartTime(serviceProvider.getStartTime());newServiceProvider.setEndTime(serviceProvider.getEndTime());newServiceProvider.setCreatedDate(serviceProvider.getCreatedDate());
				String profileImage = serviceProvider.getProfileImage();
	if (UtilValidate.isNotEmpty(profileImage)) {	File file = new File(profileImage);
		if (file.exists()) {
		String base64String ="";
		try {
			base64String = FileUploadUtils.getBase64StringFromFilePath(profileImage);
			String extension = FileUploadUtils.getFileExtension(file.getName());
			
			base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		newServiceProvider.setProfileImage(base64String);
	}
}

			}
			returnResult.put("serviceProvider", newServiceProvider);
		} catch (Exception e) {
			ServiceProviderManagerLogger.info("Exception Occured During Fetching ServiceProvider Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public Map<String,Object> getServiceProvider(HttpServletRequest request, Long id){
		ServiceProvider newServiceProvider = new ServiceProvider();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("SERVICEPROVIDER_READ");
		
		returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			ServiceProvider serviceProvider = serviceProviderRepository.getOne(id);
			if (UtilValidate.isNotEmpty(serviceProvider)) {
				newServiceProvider.setId(serviceProvider.getId());newServiceProvider.setName(serviceProvider.getName());newServiceProvider.setAbout(serviceProvider.getAbout());newServiceProvider.setAddress(serviceProvider.getAddress());newServiceProvider.setZipCode(serviceProvider.getZipCode());newServiceProvider.setPhoneNo(serviceProvider.getPhoneNo());newServiceProvider.setNidNo(serviceProvider.getNidNo());newServiceProvider.setEmail(serviceProvider.getEmail());newServiceProvider.setUser(serviceProvider.getUser());newServiceProvider.setSpLevel(serviceProvider.getSpLevel());newServiceProvider.setGender(serviceProvider.getGender());newServiceProvider.setDesiredHourlyRate(serviceProvider.getDesiredHourlyRate());newServiceProvider.setDesiredDailyRate(serviceProvider.getDesiredDailyRate());newServiceProvider.setServiceArea(serviceProvider.getServiceArea());newServiceProvider.setAvailability(serviceProvider.getAvailability());newServiceProvider.setIsActive(serviceProvider.getIsActive());newServiceProvider.setStartTime(serviceProvider.getStartTime());newServiceProvider.setEndTime(serviceProvider.getEndTime());newServiceProvider.setCreatedDate(serviceProvider.getCreatedDate());
				String profileImage = serviceProvider.getProfileImage();
	if (UtilValidate.isNotEmpty(profileImage)) {	File file = new File(profileImage);
		if (file.exists()) {
		String base64String ="";
		try {
			base64String = FileUploadUtils.getBase64StringFromFilePath(profileImage);
			String extension = FileUploadUtils.getFileExtension(file.getName());
			
			base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		newServiceProvider.setProfileImage(base64String);
	}
}

			}
			
			returnResult.put("serviceProvider", newServiceProvider);
		} catch (Exception e) {
			ServiceProviderManagerLogger.info("Exception Occured During Fetching ServiceProvider Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getServiceProviders(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("SERVICEPROVIDER_READ");
		
		returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<ServiceProvider> serviceProvidersList = new ArrayList<ServiceProvider>();
			Iterable<ServiceProvider> serviceProviders = serviceProviderRepository.findAll();
			for (ServiceProvider serviceProvider : serviceProviders) {
				serviceProvidersList.add(serviceProvider);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("serviceProviders", serviceProvidersList);
		} catch (Exception e) {
			ServiceProviderManagerLogger.info("Exception Occured During Fetching ServiceProvider List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	
	public Map<String,Object> create(HttpServletRequest request, ServiceProvider serviceProvider) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("SERVICEPROVIDER_CREATE");
			
			returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = serviceProviderValidator.getValidated(request,serviceProvider);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/serviceProvider");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				String profileImage = serviceProvider.getProfileImage();
  if (UtilValidate.isNotEmpty(profileImage)) {
	if (UtilValidate.isNotEmpty(serviceProvider.getId())) {
	ServiceProvider oldServiceProvider = serviceProviderRepository.findServiceProviderById(serviceProvider.getId());
		if (UtilValidate.isNotEmpty(oldServiceProvider)) {
		String profileImageOld = oldServiceProvider.getProfileImage();
		if (UtilValidate.isNotEmpty(profileImageOld)) {
			File file = new File(profileImageOld);
			if (file.exists()) {
				FileUtils.forceDelete(file);
				}
			}
		}	}		long now = HelperUtils.nowTimestamp().getTime();
		Map<String,Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(profileImage);
		String byteArray = (String)mapData.get("byteArray");
		String extension = (String)mapData.get("extension");		String filePath = path+"/"+now+"."+extension;
		serviceProvider.setProfileImage(filePath);
		FileUploadUtils.store(byteArray, filePath);
}
				serviceProvider = serviceProviderRepository.save(serviceProvider);
				returnResult.put("successMessage", "Successfully Saved ServiceProvider Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			ServiceProviderManagerLogger.info("Exception Occured During Creating ServiceProvider Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> update(HttpServletRequest request, ServiceProvider serviceProvider) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("SERVICEPROVIDER_EDIT");
			
			returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = serviceProviderValidator.getValidated(request,serviceProvider);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/serviceProvider");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				String profileImage = serviceProvider.getProfileImage();
  if (UtilValidate.isNotEmpty(profileImage)) {
	if (UtilValidate.isNotEmpty(serviceProvider.getId())) {
	ServiceProvider oldServiceProvider = serviceProviderRepository.findServiceProviderById(serviceProvider.getId());
		if (UtilValidate.isNotEmpty(oldServiceProvider)) {
		String profileImageOld = oldServiceProvider.getProfileImage();
		if (UtilValidate.isNotEmpty(profileImageOld)) {
			File file = new File(profileImageOld);
			if (file.exists()) {
				FileUtils.forceDelete(file);
				}
			}
		}	}		long now = HelperUtils.nowTimestamp().getTime();
		Map<String,Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(profileImage);
		String byteArray = (String)mapData.get("byteArray");
		String extension = (String)mapData.get("extension");		String filePath = path+"/"+now+"."+extension;
		serviceProvider.setProfileImage(filePath);
		FileUploadUtils.store(byteArray, filePath);
}
				serviceProvider = serviceProviderRepository.save(serviceProvider);
				returnResult.put("successMessage", "Successfully Saved ServiceProvider Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ServiceProviderManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			ServiceProviderManagerLogger.info("Exception Occured During Updating ServiceProvider Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> save(HttpServletRequest request, ServiceProvider serviceProvider) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();

			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("SERVICEPROVIDER_CREATE");
			
			errorMessageMap = serviceProviderValidator.validatePermission(request, allowedPermissions);
			
			if (errorMessageMap.keySet().size()==0) {
				errorMessageMap = serviceProviderValidator.getValidated(request,serviceProvider);
			}
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/serviceProvider");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				String profileImage = serviceProvider.getProfileImage();
				
				if (UtilValidate.isNotEmpty(profileImage)) {
					Timestamp now = HelperUtils.nowTimestamp();
					long nowLong = now.getTime();
					
					Map<String,Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(profileImage);
					String byteArray = (String)mapData.get("byteArray");
					String extension = (String)mapData.get("extension");
					
					String filePath = path+"/"+nowLong+"."+extension;
					serviceProvider.setProfileImage(filePath);
					FileUploadUtils.store(byteArray, filePath);
				}
				
				serviceProvider = serviceProviderRepository.save(serviceProvider);
				messageMap.put("error", "");
				
				messageMap.put("successMessage", "Successfully Saved ServiceProvider Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ServiceProviderManager.class.getName());
			logger.info(e);
			e.printStackTrace();
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}
	
	public Map<String,Object> saveMultipleServiceProviderData(HttpServletRequest request, List<ServiceProvider> serviceProviders) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("SERVICEPROVIDER_CREATE");
		
		returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (ServiceProvider serviceProvider : serviceProviders) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = serviceProviderValidator.getValidated(request, serviceProvider);
				serviceProvider = serviceProviderRepository.save(serviceProvider);
				returnResult.put(serviceProvider+" "+serviceProvider.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			ServiceProviderManagerLogger.info("Exception Occured During Adding ServiceProvider List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully ServiceProvider Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("SERVICEPROVIDER_DELETE");
		
		returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			ServiceProvider serviceProvider = serviceProviderRepository.findOne(id);
			if (UtilValidate.isNotEmpty(serviceProvider)) {
				serviceProviderRepository.delete(id);
						String profileImage = serviceProvider.getProfileImage();
		if (UtilValidate.isNotEmpty(profileImage)) {				
			File file = new File(profileImage);
			if (file.exists()) {
						FileUtils.forceDelete(file);
			}
		}
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			ServiceProviderManagerLogger.info("Exception Occured During Removing ServiceProvider Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("SERVICEPROVIDER_DELETE");
		
		returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All ServiceProvider Deleted.";
		try {
			serviceProviderRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			ServiceProviderManagerLogger.info("Exception Occured During Removing ServiceProvider Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("SERVICEPROVIDER_READ");
		
		returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<ServiceProvider> paginatedResult = serviceProviderRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			ServiceProviderManagerLogger.info("Exception Occured During Fetching Paginated ServiceProvider Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Map<String,Object> findPaginatedServiceProvidersBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("SERVICEPROVIDER_READ");
		
		returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<ServiceProvider> paginatedResultBySearchToken = serviceProviderRepository.findPaginatedServiceProvidersBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			ServiceProviderManagerLogger.info("Exception Occured During Fetching Paginated ServiceProvider Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("SERVICEPROVIDER_READ");
		
		returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			ServiceProvider serviceProvider = serviceProviderRepository.findOne(id);
			returnResult.put("serviceProvider", serviceProvider);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			ServiceProviderManagerLogger.info("Exception Occured During Fetching ServiceProvider Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<ServiceProvider> serviceProviders = new ArrayList<ServiceProvider>();
		allowedPermissions.add("SERVICEPROVIDER_READ");
		
		returnResult = serviceProviderValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			serviceProviders = serviceProviderRepository.findById(id);
			Page<ServiceProvider> pages = new PageImpl<ServiceProvider>(serviceProviders, pageRequest, serviceProviders.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			ServiceProviderManagerLogger.info("Exception Occured During Fetching ServiceProvider Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
