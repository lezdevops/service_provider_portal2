package app.adminPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.ServiceType;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.businesstier.ServiceTypeManager;

@Service
public class ServiceTypeValidator {
	@Autowired
	private ServiceTypeManager serviceTypeManager;
	public void validate(ServiceType serviceType) throws Exception {
		if (!checkNull(serviceType)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(ServiceType serviceType){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility serviceTypeProperties = new ConfigUtility(new File("i18n/adminPanel/serviceType_i18n.properties"));
			Properties serviceTypePropertyValue = serviceTypeProperties.loadProperties();
			
if (UtilValidate.isEmpty(serviceType.getShortCode())) {
errorMessageMap.put("shortCode_validationMessage" , serviceTypePropertyValue.getProperty(language+ ".shortCode.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(serviceType.getDescription())) {
errorMessageMap.put("description_validationMessage" , serviceTypePropertyValue.getProperty(language+ ".description.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(ServiceType serviceType){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
