package app.adminPanel.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.ServiceProvider;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.businesstier.ServiceProviderManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class ServiceProviderValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private ServiceProviderManager serviceProviderManager;
	public void validate(ServiceProvider serviceProvider) throws Exception {
		if (!checkNull(serviceProvider)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}
	
	public Map<String,Object> getValidated(HttpServletRequest request, ServiceProvider serviceProvider){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility serviceProviderProperties = new ConfigUtility(new File("i18n/adminPanel/serviceProvider_i18n.properties"));
			Properties serviceProviderPropertyValue = serviceProviderProperties.loadProperties();
			
if (UtilValidate.isEmpty(serviceProvider.getName())) {
errorMessageMap.put("name_validationMessage" , serviceProviderPropertyValue.getProperty(language+ ".name.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(serviceProvider.getNidNo())) {
errorMessageMap.put("nidNo_validationMessage" , serviceProviderPropertyValue.getProperty(language+ ".nidNo.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(serviceProvider.getEmail())) {
errorMessageMap.put("email_validationMessage" , serviceProviderPropertyValue.getProperty(language+ ".email.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(serviceProvider.getSpLevel())) {
errorMessageMap.put("spLevel_validationMessage" , serviceProviderPropertyValue.getProperty(language+ ".spLevel.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(serviceProvider.getProfileImage())) {
errorMessageMap.put("profileImage_validationMessage" , serviceProviderPropertyValue.getProperty(language+ ".profileImage.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(ServiceProvider serviceProvider){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
