package app.adminPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.Location;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.businesstier.LocationManager;

@Service
public class LocationValidator {
	@Autowired
	private LocationManager locationManager;
	public void validate(Location location) throws Exception {
		if (!checkNull(location)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(Location location){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility locationProperties = new ConfigUtility(new File("i18n/adminPanel/location_i18n.properties"));
			Properties locationPropertyValue = locationProperties.loadProperties();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(Location location){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
