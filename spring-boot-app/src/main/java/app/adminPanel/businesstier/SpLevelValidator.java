package app.adminPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.SpLevel;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.businesstier.SpLevelManager;

@Service
public class SpLevelValidator {
	@Autowired
	private SpLevelManager spLevelManager;
	public void validate(SpLevel spLevel) throws Exception {
		if (!checkNull(spLevel)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(SpLevel spLevel){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility spLevelProperties = new ConfigUtility(new File("i18n/adminPanel/spLevel_i18n.properties"));
			Properties spLevelPropertyValue = spLevelProperties.loadProperties();
			
if (UtilValidate.isEmpty(spLevel.getShortCode())) {
errorMessageMap.put("shortCode_validationMessage" , spLevelPropertyValue.getProperty(language+ ".shortCode.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(spLevel.getDescription())) {
errorMessageMap.put("description_validationMessage" , spLevelPropertyValue.getProperty(language+ ".description.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(SpLevel spLevel){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
