package app.adminPanel.businesstier;

import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.TrainingType;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.businesstier.TrainingTypeManager;

@Service
public class TrainingTypeValidator {
	@Autowired
	private TrainingTypeManager trainingTypeManager;
	public void validate(TrainingType trainingType) throws Exception {
		if (!checkNull(trainingType)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String,Object> getValidated(TrainingType trainingType){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility trainingTypeProperties = new ConfigUtility(new File("i18n/adminPanel/trainingType_i18n.properties"));
			Properties trainingTypePropertyValue = trainingTypeProperties.loadProperties();
			
if (UtilValidate.isEmpty(trainingType.getShortCode())) {
errorMessageMap.put("shortCode_validationMessage" , trainingTypePropertyValue.getProperty(language+ ".shortCode.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(trainingType.getDescription())) {
errorMessageMap.put("description_validationMessage" , trainingTypePropertyValue.getProperty(language+ ".description.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(TrainingType trainingType){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
