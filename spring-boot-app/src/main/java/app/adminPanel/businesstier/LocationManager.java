package app.adminPanel.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.adminPanel.persistencetier.LocationRepository;
import app.appmodel.Location;


//newDependentBeanImportGoesHere

@Service
public class LocationManager {
	@Autowired
	private LocationRepository locationRepository;
	@Autowired
	private LocationValidator locationValidator;
	// newAutowiredPropertiesGoesHere
	
	public Location getLocation(Long id){
		Location location = locationRepository.getOne(id);
		return location;
	}

	
	public List<Location> getLocations(){
		List<Location> locationsList = new ArrayList<Location>();
		Iterable<Location> locations = locationRepository.findAll();
		for (Location location : locations) {
			locationsList.add(location);
			// newGenerateEntityFetchActionGoesHere
		}
		return locationsList;
	}
	
	
	public Map<String,Object> save(Location location) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = locationValidator.getValidated(location);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/location");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				location = locationRepository.save(location);
				messageMap.put("successMessage", "Successfully Saved Location Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(LocationManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}
	
	public List<Location> saveMultipleLocationData(List<Location> locations) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (Location location : locations) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = locationValidator.getValidated(location);
				location = locationRepository.save(location);
				messageMap.put("addSucessmessage", "Successfully Location Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getLocations();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(LocationManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getLocations();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully Location Deleted.";
		try {
			locationRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All Location Deleted.";
		locationRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<Location> findPaginated(int page, int size) {
        return locationRepository.findAll(new PageRequest(page, size));
    }
	public Page<Location> findPaginatedLocationsBySearchToken(int page, int size, String searchToken) {
        return locationRepository.findPaginatedLocationsBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public Location findOne(Long id) {
		Location location = locationRepository.findOne(id);
		return location;
	}
	public Page<Location> findById(Long id,
			Pageable pageRequest) {
		List<Location> locations = locationRepository.findById(id);
		Page<Location> pages = null;
		pages = new PageImpl<Location>(locations, pageRequest, locations.size());
		return pages;
	}
	
}
