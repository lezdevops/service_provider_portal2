package app.adminPanel.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.adminPanel.persistencetier.CountryRepository;
import app.appmodel.Country;


//newDependentBeanImportGoesHere

@Service
public class CountryManager {
	@Autowired
	private CountryRepository countryRepository;
	@Autowired
	private CountryValidator countryValidator;
	// newAutowiredPropertiesGoesHere
	
	public Country getCountry(Long id){
		Country country = countryRepository.getOne(id);
		return country;
	}

	
	public List<Country> getCountrys(){
		List<Country> countrysList = new ArrayList<Country>();
		Iterable<Country> countrys = countryRepository.findAll();
		for (Country country : countrys) {
			countrysList.add(country);
			// newGenerateEntityFetchActionGoesHere
		}
		return countrysList;
	}
	
	
	public Map<String,Object> save(Country country) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = countryValidator.getValidated(country);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/country");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				country = countryRepository.save(country);
				messageMap.put("successMessage", "Successfully Saved Country Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(CountryManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}

	public List<Country> saveMultipleCountryData(List<Country> countrys) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (Country country : countrys) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = countryValidator.getValidated(country);
				country = countryRepository.save(country);
				messageMap.put("addSucessmessage", "Successfully Country Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getCountrys();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(CountryManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getCountrys();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully Country Deleted.";
		try {
			countryRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All Country Deleted.";
		countryRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<Country> findPaginated(int page, int size) {
        return countryRepository.findAll(new PageRequest(page, size));
    }
	public Page<Country> findPaginatedCountrysBySearchToken(int page, int size, String searchToken) {
        return countryRepository.findPaginatedCountrysBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public Country findOne(Long id) {
		Country country = countryRepository.findOne(id);
		return country;
	}
	public Page<Country> findById(Long id,
			Pageable pageRequest) {
		List<Country> countrys = countryRepository.findById(id);
		Page<Country> pages = null;
		pages = new PageImpl<Country>(countrys, pageRequest, countrys.size());
		return pages;
	}
	
}
