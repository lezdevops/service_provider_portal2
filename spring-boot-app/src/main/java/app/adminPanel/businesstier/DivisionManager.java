package app.adminPanel.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.adminPanel.persistencetier.DivisionRepository;
import app.appmodel.Division;


//newDependentBeanImportGoesHere

@Service
public class DivisionManager {
	@Autowired
	private DivisionRepository divisionRepository;
	@Autowired
	private DivisionValidator divisionValidator;
	// newAutowiredPropertiesGoesHere
	
	public Division getDivision(Long id){
		Division division = divisionRepository.getOne(id);
		return division;
	}

	
	public List<Division> getDivisions(){
		List<Division> divisionsList = new ArrayList<Division>();
		Iterable<Division> divisions = divisionRepository.findAll();
		for (Division division : divisions) {
			divisionsList.add(division);
			// newGenerateEntityFetchActionGoesHere
		}
		return divisionsList;
	}
	
	
	public Map<String,Object> save(Division division) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = divisionValidator.getValidated(division);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/division");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				division = divisionRepository.save(division);
				messageMap.put("successMessage", "Successfully Saved Division Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(DivisionManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}
	
	public List<Division> saveMultipleDivisionData(List<Division> divisions) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (Division division : divisions) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = divisionValidator.getValidated(division);
				division = divisionRepository.save(division);
				messageMap.put("addSucessmessage", "Successfully Division Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getDivisions();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(DivisionManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getDivisions();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully Division Deleted.";
		try {
			divisionRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All Division Deleted.";
		divisionRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<Division> findPaginated(int page, int size) {
        return divisionRepository.findAll(new PageRequest(page, size));
    }
	public Page<Division> findPaginatedDivisionsBySearchToken(int page, int size, String searchToken) {
        return divisionRepository.findPaginatedDivisionsBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public Division findOne(Long id) {
		Division division = divisionRepository.findOne(id);
		return division;
	}
	public Page<Division> findById(Long id,
			Pageable pageRequest) {
		List<Division> divisions = divisionRepository.findById(id);
		Page<Division> pages = null;
		pages = new PageImpl<Division>(divisions, pageRequest, divisions.size());
		return pages;
	}
	
}
