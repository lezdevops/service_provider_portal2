package app.adminPanel.businesstier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.SpLevel;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.persistencetier.SpLevelRepository;


//newDependentBeanImportGoesHere

@Service
public class SpLevelManager {
	@Autowired
	private SpLevelRepository spLevelRepository;
	@Autowired
	private SpLevelValidator spLevelValidator;
	// newAutowiredPropertiesGoesHere
	
	public SpLevel getSpLevel(Long id){
		SpLevel spLevel = spLevelRepository.getOne(id);
		return spLevel;
	}

	
	public List<SpLevel> getSpLevels(){
		List<SpLevel> spLevelsList = new ArrayList<SpLevel>();
		Iterable<SpLevel> spLevels = spLevelRepository.findAll();
		for (SpLevel spLevel : spLevels) {
			spLevelsList.add(spLevel);
			// newGenerateEntityFetchActionGoesHere
		}
		return spLevelsList;
	}
	
	
	public Map<String,Object> save(SpLevel spLevel) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = spLevelValidator.getValidated(spLevel);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/spLevel");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				spLevel = spLevelRepository.save(spLevel);
				messageMap.put("successMessage", "Successfully Saved SpLevel Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(SpLevelManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}
	
	public List<SpLevel> saveMultipleSpLevelData(List<SpLevel> spLevels) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (SpLevel spLevel : spLevels) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = spLevelValidator.getValidated(spLevel);
				spLevel = spLevelRepository.save(spLevel);
				messageMap.put("addSucessmessage", "Successfully SpLevel Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getSpLevels();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(SpLevelManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getSpLevels();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully SpLevel Deleted.";
		try {
			spLevelRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All SpLevel Deleted.";
		spLevelRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<SpLevel> findPaginated(int page, int size) {
        return spLevelRepository.findAll(new PageRequest(page, size));
    }
	public Page<SpLevel> findPaginatedSpLevelsBySearchToken(int page, int size, String searchToken) {
        return spLevelRepository.findPaginatedSpLevelsBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public SpLevel findOne(Long id) {
		SpLevel spLevel = spLevelRepository.findOne(id);
		return spLevel;
	}
	public Page<SpLevel> findById(Long id,
			Pageable pageRequest) {
		List<SpLevel> spLevels = spLevelRepository.findById(id);
		Page<SpLevel> pages = null;
		pages = new PageImpl<SpLevel>(spLevels, pageRequest, spLevels.size());
		return pages;
	}
	
}
