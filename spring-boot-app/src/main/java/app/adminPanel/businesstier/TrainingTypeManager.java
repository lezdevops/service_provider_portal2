package app.adminPanel.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.adminPanel.persistencetier.TrainingTypeRepository;
import app.appmodel.TrainingType;


//newDependentBeanImportGoesHere

@Service
public class TrainingTypeManager {
	@Autowired
	private TrainingTypeRepository trainingTypeRepository;
	@Autowired
	private TrainingTypeValidator trainingTypeValidator;
	// newAutowiredPropertiesGoesHere
	
	public TrainingType getTrainingType(Long id){
		TrainingType trainingType = trainingTypeRepository.getOne(id);
		return trainingType;
	}

	
	public List<TrainingType> getTrainingTypes(){
		List<TrainingType> trainingTypesList = new ArrayList<TrainingType>();
		Iterable<TrainingType> trainingTypes = trainingTypeRepository.findAll();
		for (TrainingType trainingType : trainingTypes) {
			trainingTypesList.add(trainingType);
			// newGenerateEntityFetchActionGoesHere
		}
		return trainingTypesList;
	}
	
	
	public Map<String,Object> save(TrainingType trainingType) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap = trainingTypeValidator.getValidated(trainingType);
			
			if (errorMessageMap.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			}else{
				File path = new File("uploads/trainingType");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}
				
				trainingType = trainingTypeRepository.save(trainingType);
				messageMap.put("successMessage", "Successfully Saved TrainingType Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(TrainingTypeManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}

	public List<TrainingType> saveMultipleTrainingTypeData(List<TrainingType> trainingTypes) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (TrainingType trainingType : trainingTypes) {
				Map<String,Object> errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = trainingTypeValidator.getValidated(trainingType);
				trainingType = trainingTypeRepository.save(trainingType);
				messageMap.put("addSucessmessage", "Successfully TrainingType Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getTrainingTypes();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(TrainingTypeManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getTrainingTypes();
	}
	public String remove(Long id) {
		String removeMessage = "Successfully TrainingType Deleted.";
		try {
			trainingTypeRepository.delete(id);
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public String removeAll() {
		String removeMessage = "Successfully All TrainingType Deleted.";
		trainingTypeRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<TrainingType> findPaginated(int page, int size) {
        return trainingTypeRepository.findAll(new PageRequest(page, size));
    }
	public Page<TrainingType> findPaginatedTrainingTypesBySearchToken(int page, int size, String searchToken) {
        return trainingTypeRepository.findPaginatedTrainingTypesBySearchToken(searchToken,  new PageRequest(page, size));
    }
	public TrainingType findOne(Long id) {
		TrainingType trainingType = trainingTypeRepository.findOne(id);
		return trainingType;
	}
	public Page<TrainingType> findById(Long id,
			Pageable pageRequest) {
		List<TrainingType> trainingTypes = trainingTypeRepository.findById(id);
		Page<TrainingType> pages = null;
		pages = new PageImpl<TrainingType>(trainingTypes, pageRequest, trainingTypes.size());
		return pages;
	}
	
}
