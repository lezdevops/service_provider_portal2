package app.adminPanel.businesstier;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.adminPanel.persistencetier.CustomerRepository;
import app.appmodel.Customer;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;


//newDependentBeanImportGoesHere

@Service
public class CustomerManager {
	private static final org.apache.log4j.Logger CustomerManagerLogger = org.apache.log4j.Logger.getLogger(CustomerManager.class);
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private CustomerValidator customerValidator;
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getCustomerForEdit(HttpServletRequest request, Long id){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMER_EDIT");
		
		returnResult = customerValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		Customer customer = null;
		try {
			customer = customerRepository.getOne(id);
			String profileImage = customer.getProfileImage();
	if (UtilValidate.isNotEmpty(profileImage)) {	File file = new File(profileImage);
		if (file.exists()) {
		String base64String ="";
		try {
			base64String = FileUploadUtils.getBase64StringFromFilePath(profileImage);
			String extension = FileUploadUtils.getFileExtension(file.getName());
			
			base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		customer.setProfileImage(base64String);
	}
}

			returnResult.put("customer", customer);
		} catch (Exception e) {
			CustomerManagerLogger.info("Exception Occured During Fetching Customer Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public Map<String,Object> getCustomer(HttpServletRequest request, Long id){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMER_READ");
		
		returnResult = customerValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Customer customer = customerRepository.getOne(id);
			String profileImage = customer.getProfileImage();
	if (UtilValidate.isNotEmpty(profileImage)) {	File file = new File(profileImage);
		if (file.exists()) {
		String base64String ="";
		try {
			base64String = FileUploadUtils.getBase64StringFromFilePath(profileImage);
			String extension = FileUploadUtils.getFileExtension(file.getName());
			
			base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		customer.setProfileImage(base64String);
	}
}

			returnResult.put("customer", customer);
		} catch (Exception e) {
			CustomerManagerLogger.info("Exception Occured During Fetching Customer Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getCustomers(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMER_READ");
		
		returnResult = customerValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<Customer> customersList = new ArrayList<Customer>();
			Iterable<Customer> customers = customerRepository.findAll();
			for (Customer customer : customers) {
				customersList.add(customer);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("customers", customersList);
		} catch (Exception e) {
			CustomerManagerLogger.info("Exception Occured During Fetching Customer List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	
	public Map<String,Object> create(HttpServletRequest request, Customer customer) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("CUSTOMER_CREATE");
			
			returnResult = customerValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = customerValidator.getValidated(request,customer);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/customer");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				String profileImage = customer.getProfileImage();
  if (UtilValidate.isNotEmpty(profileImage)) {
		long now = HelperUtils.nowTimestamp().getTime();
		Map<String,Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(profileImage);
		String byteArray = (String)mapData.get("byteArray");
		String extension = (String)mapData.get("extension");		String filePath = path+"/"+now+"."+extension;
		customer.setProfileImage(filePath);
		FileUploadUtils.store(byteArray, filePath);
}
				customer = customerRepository.save(customer);
				returnResult.put("successMessage", "Successfully Saved Customer Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			CustomerManagerLogger.info("Exception Occured During Creating Customer Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> update(HttpServletRequest request, Customer customer) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("CUSTOMER_EDIT");
			
			returnResult = customerValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = customerValidator.getValidated(request,customer);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/customer");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				String profileImage = customer.getProfileImage();
  if (UtilValidate.isNotEmpty(profileImage)) {
		long now = HelperUtils.nowTimestamp().getTime();
		Map<String,Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(profileImage);
		String byteArray = (String)mapData.get("byteArray");
		String extension = (String)mapData.get("extension");		String filePath = path+"/"+now+"."+extension;
		customer.setProfileImage(filePath);
		FileUploadUtils.store(byteArray, filePath);
}
				customer = customerRepository.save(customer);
				returnResult.put("successMessage", "Successfully Saved Customer Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(CustomerManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			CustomerManagerLogger.info("Exception Occured During Updating Customer Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	
	public Map<String,Object> saveMultipleCustomerData(HttpServletRequest request, List<Customer> customers) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMER_CREATE");
		
		returnResult = customerValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (Customer customer : customers) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = customerValidator.getValidated(request, customer);
				customer = customerRepository.save(customer);
				returnResult.put(customer+" "+customer.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			CustomerManagerLogger.info("Exception Occured During Adding Customer List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully Customer Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMER_DELETE");
		
		returnResult = customerValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			Customer customer = customerRepository.findOne(id);
			if (UtilValidate.isNotEmpty(customer)) {
						String profileImage = customer.getProfileImage();
		if (UtilValidate.isNotEmpty(profileImage)) {				
			File file = new File(profileImage);
			if (file.exists()) {
						FileUtils.forceDelete(file);
			}
		}
				customerRepository.delete(id);
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerManagerLogger.info("Exception Occured During Removing Customer Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMER_DELETE");
		
		returnResult = customerValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All Customer Deleted.";
		try {
			customerRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerManagerLogger.info("Exception Occured During Removing Customer Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMER_READ");
		
		returnResult = customerValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<Customer> paginatedResult = customerRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerManagerLogger.info("Exception Occured During Fetching Paginated Customer Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Map<String,Object> findPaginatedCustomersBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMER_READ");
		
		returnResult = customerValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<Customer> paginatedResultBySearchToken = customerRepository.findPaginatedCustomersBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerManagerLogger.info("Exception Occured During Fetching Paginated Customer Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("CUSTOMER_READ");
		
		returnResult = customerValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Customer customer = customerRepository.findOne(id);
			returnResult.put("customer", customer);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerManagerLogger.info("Exception Occured During Fetching Customer Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<Customer> customers = new ArrayList<Customer>();
		allowedPermissions.add("CUSTOMER_READ");
		
		returnResult = customerValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			customers = customerRepository.findById(id);
			Page<Customer> pages = new PageImpl<Customer>(customers, pageRequest, customers.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			CustomerManagerLogger.info("Exception Occured During Fetching Customer Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
