package app.adminPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.ServiceProvider;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.businesstier.ServiceProviderManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class ServiceProviderAPI {
	
	@Autowired
	private ServiceProviderManager serviceProviderManager;

	@PostMapping("/serviceProvider")
	public Map<String,Object> createServiceProvider(HttpServletRequest request, @RequestBody ServiceProvider serviceProvider) {
		
		serviceProvider.setUser(null);serviceProvider.setCreatedDate(null);

		return serviceProviderManager.create(request,serviceProvider);
	}
	@GetMapping("/serviceProvider/{id}")
	public Map<String,Object> getServiceProvider(HttpServletRequest request, @PathVariable Long id) {
		return serviceProviderManager.getServiceProviderForEdit(request,id);
	}
	@GetMapping(value = "/serviceProviders")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return serviceProviderManager.getServiceProviders(request);
	}
	@PutMapping("/serviceProvider")
	public Map<String,Object> updateServiceProvider(HttpServletRequest request, @RequestBody ServiceProvider serviceProvider) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = serviceProviderManager.getServiceProvider(request,serviceProvider.getId());
			ServiceProvider newServiceProvider = (ServiceProvider)resultMap.get("serviceProvider");
			newServiceProvider.setAbout(serviceProvider.getAbout());
newServiceProvider.setAddress(serviceProvider.getAddress());
newServiceProvider.setZipCode(serviceProvider.getZipCode());
newServiceProvider.setPhoneNo(serviceProvider.getPhoneNo());
newServiceProvider.setGender(serviceProvider.getGender());
newServiceProvider.setDesiredHourlyRate(serviceProvider.getDesiredHourlyRate());
newServiceProvider.setDesiredDailyRate(serviceProvider.getDesiredDailyRate());
newServiceProvider.setServiceArea(serviceProvider.getServiceArea());
newServiceProvider.setAvailability(serviceProvider.getAvailability());
newServiceProvider.setIsActive(serviceProvider.getIsActive());
newServiceProvider.setStartTime(serviceProvider.getStartTime());
newServiceProvider.setEndTime(serviceProvider.getEndTime());
newServiceProvider.setCreatedDate(serviceProvider.getCreatedDate());
newServiceProvider.setProfileImage(serviceProvider.getProfileImage());

			return serviceProviderManager.update(request,newServiceProvider);
		}
		
	}
	@DeleteMapping("/serviceProvider/{id}")
	public Map<String,Object> deleteServiceProvider(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return serviceProviderManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/serviceProvider/saveMultipleServiceProviderData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleServiceProviderData( HttpServletRequest request, @RequestBody List<ServiceProvider> serviceProviders) {
		return serviceProviderManager.saveMultipleServiceProviderData(request,serviceProviders);
	}
	@RequestMapping(value = "/serviceProvider/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return serviceProviderManager.remove(request,id);
	}
	@RequestMapping(value = "/serviceProvider/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return serviceProviderManager.removeAll(request);
	}
	@RequestMapping(value = "/serviceProvider/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return serviceProviderManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/serviceProvider/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return serviceProviderManager.findPaginatedServiceProvidersBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/serviceProvider/searchOnServiceProviderTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnServiceProviderTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return serviceProviderManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/serviceProvider/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<ServiceProvider> serviceProviders = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ServiceProvider serviceProvider = new ServiceProvider();
				HSSFRow row = worksheet.getRow(i++);
				//serviceProvider.setField2(row.getCell(1).getStringCellValue());
				serviceProvider.setName(row.getCell(0).getStringCellValue());
serviceProvider.setAbout(row.getCell(1).getStringCellValue());
serviceProvider.setAddress(row.getCell(2).getStringCellValue());
serviceProvider.setZipCode(row.getCell(3).getStringCellValue());
serviceProvider.setPhoneNo(row.getCell(4).getStringCellValue());
serviceProvider.setNidNo(row.getCell(5).getStringCellValue());
serviceProvider.setEmail(row.getCell(6).getStringCellValue());
serviceProvider.setGender(row.getCell(7).getStringCellValue());
serviceProvider.setDesiredHourlyRate(Double.valueOf(row.getCell(8).getNumericCellValue()));
serviceProvider.setDesiredDailyRate(Double.valueOf(row.getCell(9).getNumericCellValue()));
serviceProvider.setServiceArea(Double.valueOf(row.getCell(10).getNumericCellValue()));
serviceProvider.setAvailability(row.getCell(11).getStringCellValue());
serviceProvider.setIsActive(row.getCell(12).getStringCellValue());
serviceProvider.setStartTime(row.getCell(13).getStringCellValue());
serviceProvider.setEndTime(row.getCell(14).getStringCellValue());
serviceProvider.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(15).getStringCellValue()));
serviceProvider.setProfileImage(row.getCell(16).getStringCellValue());

				serviceProviders.add(serviceProvider);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("serviceProviders", serviceProviders);
		return returnResult;
	}
	@RequestMapping(value = "/serviceProvider/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<ServiceProvider> serviceProviders = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ServiceProvider serviceProvider = new ServiceProvider();
				XSSFRow row = worksheet.getRow(i++);
				//serviceProvider.setField2(row.getCell(1).getStringCellValue());
				serviceProvider.setName(row.getCell(0).getStringCellValue());
serviceProvider.setAbout(row.getCell(1).getStringCellValue());
serviceProvider.setAddress(row.getCell(2).getStringCellValue());
serviceProvider.setZipCode(row.getCell(3).getStringCellValue());
serviceProvider.setPhoneNo(row.getCell(4).getStringCellValue());
serviceProvider.setNidNo(row.getCell(5).getStringCellValue());
serviceProvider.setEmail(row.getCell(6).getStringCellValue());
serviceProvider.setGender(row.getCell(7).getStringCellValue());
serviceProvider.setDesiredHourlyRate(Double.valueOf(row.getCell(8).getNumericCellValue()));
serviceProvider.setDesiredDailyRate(Double.valueOf(row.getCell(9).getNumericCellValue()));
serviceProvider.setServiceArea(Double.valueOf(row.getCell(10).getNumericCellValue()));
serviceProvider.setAvailability(row.getCell(11).getStringCellValue());
serviceProvider.setIsActive(row.getCell(12).getStringCellValue());
serviceProvider.setStartTime(row.getCell(13).getStringCellValue());
serviceProvider.setEndTime(row.getCell(14).getStringCellValue());
serviceProvider.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(15).getStringCellValue()));
serviceProvider.setProfileImage(row.getCell(16).getStringCellValue());

				serviceProviders.add(serviceProvider);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("serviceProviders", serviceProviders);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/serviceProvider/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<ServiceProvider> serviceProviders = new ArrayList<ServiceProvider>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			serviceProviders = (List<ServiceProvider>) resultMap.get("serviceProviders");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			serviceProviders = (List<ServiceProvider>) resultMap.get("serviceProviders");
		}
		
		if (UtilValidate.isNotEmpty(serviceProviders) && serviceProviders.size()>0) {
			serviceProviders.remove(0);
		}
		returnResult.put("serviceProviders", serviceProviders);
		return returnResult;
	}
}
