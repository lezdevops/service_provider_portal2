package app.adminPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.Country;
import app.framework.businesstier.HelperUtils;
import app.adminPanel.businesstier.CountryManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules Country API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class CountryAPI {

	@Autowired
	private CountryManager countryManager;

	@PostMapping("/country")
	public Map<String,Object> createCountry(@RequestBody Country country) {
		country.setCreatedDate(null);

		
		return countryManager.save(country);
	}
	@GetMapping("/country/{id}")
	public Country getCountry(@PathVariable Long id) {
		return countryManager.findOne(id);
	}
	@GetMapping(value = "/countrys")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<Country> getAll() {
		return countryManager.getCountrys();
	}

	@PutMapping("/country")
	public Map<String,Object> updateCountry(@RequestBody Country country) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			Country newCountry = getCountry(country.getId());
			newCountry.setCurrency(country.getCurrency());
newCountry.setDescription(country.getDescription());
newCountry.setCreatedDate(country.getCreatedDate());

			return countryManager.save(newCountry);
		}
		
	}
	
	@DeleteMapping("/country/{id}")
	public boolean deleteCountry(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		countryManager.remove(id);
		return true;
		}
	}

	@RequestMapping(value = "/country/saveMultipleCountryData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<Country> saveMultipleCountryData(@RequestBody List<Country> countrys) {
		return countryManager.saveMultipleCountryData(countrys);
	}

	@RequestMapping(value = "/country/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = countryManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/country/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return countryManager.removeAll();
	}
	@RequestMapping(value = "/country/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<Country> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<Country> resultPage = countryManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/country/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<Country> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<Country> resultPage = countryManager.findPaginatedCountrysBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/country/searchOnCountryTable", method = RequestMethod.GET)
    public Page<Country> searchOnCountryTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return countryManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/country/processExcel2003", method = RequestMethod.POST)
	public List<Country> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<Country> countrys = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Country country = new Country();
				HSSFRow row = worksheet.getRow(i++);
				//country.setField2(row.getCell(1).getStringCellValue());
				country.setCurrency(row.getCell(0).getStringCellValue());
country.setDescription(row.getCell(1).getStringCellValue());
country.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(2).getStringCellValue()));

				countrys.add(country);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return countrys;
	}

	@RequestMapping(value = "/country/processExcel2007", method = RequestMethod.POST)
	public List<Country> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<Country> countrys = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Country country = new Country();
				XSSFRow row = worksheet.getRow(i++);
				//country.setField2(row.getCell(1).getStringCellValue());
				country.setCurrency(row.getCell(0).getStringCellValue());
country.setDescription(row.getCell(1).getStringCellValue());
country.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(2).getStringCellValue()));

				countrys.add(country);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return countrys;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/country/excelFileUpload")
	@ResponseBody
	public List<Country> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<Country> countrys = new ArrayList<Country>();
		if (extension.equalsIgnoreCase("xlsx")) {
			countrys = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			countrys = processExcel2003(file);
		}
		
		if (countrys.size()>0) {
			countrys.remove(0);
		}
		return countrys;
	}
}
