package app.adminPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.TrainingType;
import app.framework.businesstier.HelperUtils;
import app.adminPanel.businesstier.TrainingTypeManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules TrainingType API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class TrainingTypeAPI {

	@Autowired
	private TrainingTypeManager trainingTypeManager;

	@PostMapping("/trainingType")
	public Map<String,Object> createTrainingType(@RequestBody TrainingType trainingType) {
		

		
		return trainingTypeManager.save(trainingType);
	}
	@GetMapping("/trainingType/{id}")
	public TrainingType getTrainingType(@PathVariable Long id) {
		return trainingTypeManager.findOne(id);
	}
	@GetMapping(value = "/trainingTypes")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<TrainingType> getAll() {
		return trainingTypeManager.getTrainingTypes();
	}

	@PutMapping("/trainingType")
	public Map<String,Object> updateTrainingType(@RequestBody TrainingType trainingType) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			TrainingType newTrainingType = getTrainingType(trainingType.getId());
			newTrainingType.setShortCode(trainingType.getShortCode());
newTrainingType.setDescription(trainingType.getDescription());
newTrainingType.setIsActive(trainingType.getIsActive());

			return trainingTypeManager.save(newTrainingType);
		}
		
	}
	
	@DeleteMapping("/trainingType/{id}")
	public boolean deleteTrainingType(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		trainingTypeManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/trainingType/saveMultipleTrainingTypeData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<TrainingType> saveMultipleTrainingTypeData(@RequestBody List<TrainingType> trainingTypes) {
		return trainingTypeManager.saveMultipleTrainingTypeData(trainingTypes);
	}

	@RequestMapping(value = "/trainingType/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = trainingTypeManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/trainingType/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return trainingTypeManager.removeAll();
	}
	@RequestMapping(value = "/trainingType/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<TrainingType> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<TrainingType> resultPage = trainingTypeManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/trainingType/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<TrainingType> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<TrainingType> resultPage = trainingTypeManager.findPaginatedTrainingTypesBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/trainingType/searchOnTrainingTypeTable", method = RequestMethod.GET)
    public Page<TrainingType> searchOnTrainingTypeTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return trainingTypeManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/trainingType/processExcel2003", method = RequestMethod.POST)
	public List<TrainingType> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<TrainingType> trainingTypes = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				TrainingType trainingType = new TrainingType();
				HSSFRow row = worksheet.getRow(i++);
				//trainingType.setField2(row.getCell(1).getStringCellValue());
				trainingType.setShortCode(row.getCell(0).getStringCellValue());
trainingType.setDescription(row.getCell(1).getStringCellValue());
trainingType.setIsActive(row.getCell(2).getStringCellValue());

				trainingTypes.add(trainingType);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return trainingTypes;
	}

	@RequestMapping(value = "/trainingType/processExcel2007", method = RequestMethod.POST)
	public List<TrainingType> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<TrainingType> trainingTypes = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				TrainingType trainingType = new TrainingType();
				XSSFRow row = worksheet.getRow(i++);
				//trainingType.setField2(row.getCell(1).getStringCellValue());
				trainingType.setShortCode(row.getCell(0).getStringCellValue());
trainingType.setDescription(row.getCell(1).getStringCellValue());
trainingType.setIsActive(row.getCell(2).getStringCellValue());

				trainingTypes.add(trainingType);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return trainingTypes;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/trainingType/excelFileUpload")
	@ResponseBody
	public List<TrainingType> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<TrainingType> trainingTypes = new ArrayList<TrainingType>();
		if (extension.equalsIgnoreCase("xlsx")) {
			trainingTypes = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			trainingTypes = processExcel2003(file);
		}
		
		if (trainingTypes.size()>0) {
			trainingTypes.remove(0);
		}
		return trainingTypes;
	}
}
