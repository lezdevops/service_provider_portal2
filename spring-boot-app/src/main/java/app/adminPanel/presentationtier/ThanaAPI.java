package app.adminPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.Thana;
import app.framework.businesstier.HelperUtils;
import app.adminPanel.businesstier.ThanaManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules Thana API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class ThanaAPI {

	@Autowired
	private ThanaManager thanaManager;

	@PostMapping("/thana")
	public Map<String,Object> createThana(@RequestBody Thana thana) {
		thana.setCreatedDate(null);

		
		return thanaManager.save(thana);
	}
	@GetMapping("/thana/{id}")
	public Thana getThana(@PathVariable Long id) {
		return thanaManager.findOne(id);
	}
	@GetMapping(value = "/thanas")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<Thana> getAll() {
		return thanaManager.getThanas();
	}

	@PutMapping("/thana")
	public Map<String,Object> updateThana(@RequestBody Thana thana) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			Thana newThana = getThana(thana.getId());
			newThana.setDescription(thana.getDescription());
newThana.setCreatedDate(thana.getCreatedDate());

			return thanaManager.save(newThana);
		}
		
	}
	
	@DeleteMapping("/thana/{id}")
	public boolean deleteThana(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		thanaManager.remove(id);
		return true;
		}
	}

	@RequestMapping(value = "/thana/saveMultipleThanaData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<Thana> saveMultipleThanaData(@RequestBody List<Thana> thanas) {
		return thanaManager.saveMultipleThanaData(thanas);
	}

	@RequestMapping(value = "/thana/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = thanaManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/thana/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return thanaManager.removeAll();
	}
	@RequestMapping(value = "/thana/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<Thana> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<Thana> resultPage = thanaManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/thana/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<Thana> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<Thana> resultPage = thanaManager.findPaginatedThanasBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/thana/searchOnThanaTable", method = RequestMethod.GET)
    public Page<Thana> searchOnThanaTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return thanaManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/thana/processExcel2003", method = RequestMethod.POST)
	public List<Thana> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<Thana> thanas = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Thana thana = new Thana();
				HSSFRow row = worksheet.getRow(i++);
				//thana.setField2(row.getCell(1).getStringCellValue());
				thana.setDescription(row.getCell(0).getStringCellValue());
thana.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				thanas.add(thana);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return thanas;
	}

	@RequestMapping(value = "/thana/processExcel2007", method = RequestMethod.POST)
	public List<Thana> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<Thana> thanas = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Thana thana = new Thana();
				XSSFRow row = worksheet.getRow(i++);
				//thana.setField2(row.getCell(1).getStringCellValue());
				thana.setDescription(row.getCell(0).getStringCellValue());
thana.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				thanas.add(thana);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return thanas;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/thana/excelFileUpload")
	@ResponseBody
	public List<Thana> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<Thana> thanas = new ArrayList<Thana>();
		if (extension.equalsIgnoreCase("xlsx")) {
			thanas = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			thanas = processExcel2003(file);
		}
		
		if (thanas.size()>0) {
			thanas.remove(0);
		}
		return thanas;
	}
}
