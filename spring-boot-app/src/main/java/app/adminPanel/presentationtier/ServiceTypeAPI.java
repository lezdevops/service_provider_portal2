package app.adminPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.ServiceType;
import app.framework.businesstier.HelperUtils;
import app.adminPanel.businesstier.ServiceTypeManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules ServiceType API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class ServiceTypeAPI {

	@Autowired
	private ServiceTypeManager serviceTypeManager;

	@PostMapping("/serviceType")
	public Map<String,Object> createServiceType(@RequestBody ServiceType serviceType) {
		

		
		return serviceTypeManager.save(serviceType);
	}
	@GetMapping("/serviceType/{id}")
	public ServiceType getServiceType(@PathVariable Long id) {
		return serviceTypeManager.findOne(id);
	}
	@GetMapping(value = "/serviceTypes")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<ServiceType> getAll() {
		return serviceTypeManager.getServiceTypes();
	}

	@PutMapping("/serviceType")
	public Map<String,Object> updateServiceType(@RequestBody ServiceType serviceType) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			ServiceType newServiceType = getServiceType(serviceType.getId());
			newServiceType.setShortCode(serviceType.getShortCode());
newServiceType.setDescription(serviceType.getDescription());
newServiceType.setIsActive(serviceType.getIsActive());

			return serviceTypeManager.save(newServiceType);
		}
		
	}
	
	@DeleteMapping("/serviceType/{id}")
	public boolean deleteServiceType(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		serviceTypeManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/serviceType/saveMultipleServiceTypeData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<ServiceType> saveMultipleServiceTypeData(@RequestBody List<ServiceType> serviceTypes) {
		return serviceTypeManager.saveMultipleServiceTypeData(serviceTypes);
	}

	@RequestMapping(value = "/serviceType/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = serviceTypeManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/serviceType/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return serviceTypeManager.removeAll();
	}
	@RequestMapping(value = "/serviceType/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<ServiceType> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<ServiceType> resultPage = serviceTypeManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/serviceType/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<ServiceType> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<ServiceType> resultPage = serviceTypeManager.findPaginatedServiceTypesBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/serviceType/searchOnServiceTypeTable", method = RequestMethod.GET)
    public Page<ServiceType> searchOnServiceTypeTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return serviceTypeManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/serviceType/processExcel2003", method = RequestMethod.POST)
	public List<ServiceType> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<ServiceType> serviceTypes = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ServiceType serviceType = new ServiceType();
				HSSFRow row = worksheet.getRow(i++);
				//serviceType.setField2(row.getCell(1).getStringCellValue());
				serviceType.setShortCode(row.getCell(0).getStringCellValue());
serviceType.setDescription(row.getCell(1).getStringCellValue());
serviceType.setIsActive(row.getCell(2).getStringCellValue());

				serviceTypes.add(serviceType);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return serviceTypes;
	}

	@RequestMapping(value = "/serviceType/processExcel2007", method = RequestMethod.POST)
	public List<ServiceType> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<ServiceType> serviceTypes = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ServiceType serviceType = new ServiceType();
				XSSFRow row = worksheet.getRow(i++);
				//serviceType.setField2(row.getCell(1).getStringCellValue());
				serviceType.setShortCode(row.getCell(0).getStringCellValue());
serviceType.setDescription(row.getCell(1).getStringCellValue());
serviceType.setIsActive(row.getCell(2).getStringCellValue());

				serviceTypes.add(serviceType);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return serviceTypes;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/serviceType/excelFileUpload")
	@ResponseBody
	public List<ServiceType> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<ServiceType> serviceTypes = new ArrayList<ServiceType>();
		if (extension.equalsIgnoreCase("xlsx")) {
			serviceTypes = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			serviceTypes = processExcel2003(file);
		}
		
		if (serviceTypes.size()>0) {
			serviceTypes.remove(0);
		}
		return serviceTypes;
	}
}
