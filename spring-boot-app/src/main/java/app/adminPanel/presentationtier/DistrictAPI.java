package app.adminPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.District;
import app.framework.businesstier.HelperUtils;
import app.adminPanel.businesstier.DistrictManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules District API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class DistrictAPI {

	@Autowired
	private DistrictManager districtManager;

	@PostMapping("/district")
	public Map<String,Object> createDistrict(@RequestBody District district) {
		district.setCreatedDate(null);

		
		return districtManager.save(district);
	}
	@GetMapping("/district/{id}")
	public District getDistrict(@PathVariable Long id) {
		return districtManager.findOne(id);
	}
	@GetMapping(value = "/districts")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<District> getAll() {
		return districtManager.getDistricts();
	}

	@PutMapping("/district")
	public Map<String,Object> updateDistrict(@RequestBody District district) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			District newDistrict = getDistrict(district.getId());
			newDistrict.setDescription(district.getDescription());
newDistrict.setCreatedDate(district.getCreatedDate());

			return districtManager.save(newDistrict);
		}
		
	}
	
	@DeleteMapping("/district/{id}")
	public boolean deleteDistrict(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		districtManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/district/saveMultipleDistrictData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<District> saveMultipleDistrictData(@RequestBody List<District> districts) {
		return districtManager.saveMultipleDistrictData(districts);
	}

	@RequestMapping(value = "/district/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = districtManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/district/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return districtManager.removeAll();
	}
	@RequestMapping(value = "/district/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<District> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<District> resultPage = districtManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/district/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<District> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<District> resultPage = districtManager.findPaginatedDistrictsBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/district/searchOnDistrictTable", method = RequestMethod.GET)
    public Page<District> searchOnDistrictTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return districtManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/district/processExcel2003", method = RequestMethod.POST)
	public List<District> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<District> districts = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				District district = new District();
				HSSFRow row = worksheet.getRow(i++);
				//district.setField2(row.getCell(1).getStringCellValue());
				district.setDescription(row.getCell(0).getStringCellValue());
district.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				districts.add(district);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return districts;
	}

	@RequestMapping(value = "/district/processExcel2007", method = RequestMethod.POST)
	public List<District> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<District> districts = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				District district = new District();
				XSSFRow row = worksheet.getRow(i++);
				//district.setField2(row.getCell(1).getStringCellValue());
				district.setDescription(row.getCell(0).getStringCellValue());
district.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				districts.add(district);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return districts;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/district/excelFileUpload")
	@ResponseBody
	public List<District> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<District> districts = new ArrayList<District>();
		if (extension.equalsIgnoreCase("xlsx")) {
			districts = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			districts = processExcel2003(file);
		}
		
		if (districts.size()>0) {
			districts.remove(0);
		}
		return districts;
	}
}
