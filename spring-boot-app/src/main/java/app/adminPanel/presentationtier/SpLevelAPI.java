package app.adminPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.SpLevel;
import app.framework.businesstier.HelperUtils;
import app.adminPanel.businesstier.SpLevelManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules SpLevel API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class SpLevelAPI {

	@Autowired
	private SpLevelManager spLevelManager;

	@PostMapping("/spLevel")
	public Map<String,Object> createSpLevel(@RequestBody SpLevel spLevel) {
		

		
		return spLevelManager.save(spLevel);
	}
	@GetMapping("/spLevel/{id}")
	public SpLevel getSpLevel(@PathVariable Long id) {
		return spLevelManager.findOne(id);
	}
	@GetMapping(value = "/spLevels")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<SpLevel> getAll() {
		return spLevelManager.getSpLevels();
	}

	@PutMapping("/spLevel")
	public Map<String,Object> updateSpLevel(@RequestBody SpLevel spLevel) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			SpLevel newSpLevel = getSpLevel(spLevel.getId());
			newSpLevel.setShortCode(spLevel.getShortCode());
newSpLevel.setDescription(spLevel.getDescription());
newSpLevel.setIsActive(spLevel.getIsActive());

			return spLevelManager.save(newSpLevel);
		}
		
	}
	
	@DeleteMapping("/spLevel/{id}")
	public boolean deleteSpLevel(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		spLevelManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/spLevel/saveMultipleSpLevelData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<SpLevel> saveMultipleSpLevelData(@RequestBody List<SpLevel> spLevels) {
		return spLevelManager.saveMultipleSpLevelData(spLevels);
	}

	@RequestMapping(value = "/spLevel/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = spLevelManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/spLevel/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return spLevelManager.removeAll();
	}
	@RequestMapping(value = "/spLevel/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<SpLevel> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<SpLevel> resultPage = spLevelManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/spLevel/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<SpLevel> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<SpLevel> resultPage = spLevelManager.findPaginatedSpLevelsBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/spLevel/searchOnSpLevelTable", method = RequestMethod.GET)
    public Page<SpLevel> searchOnSpLevelTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return spLevelManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/spLevel/processExcel2003", method = RequestMethod.POST)
	public List<SpLevel> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<SpLevel> spLevels = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				SpLevel spLevel = new SpLevel();
				HSSFRow row = worksheet.getRow(i++);
				//spLevel.setField2(row.getCell(1).getStringCellValue());
				spLevel.setShortCode(row.getCell(0).getStringCellValue());
spLevel.setDescription(row.getCell(1).getStringCellValue());
spLevel.setIsActive(row.getCell(2).getStringCellValue());

				spLevels.add(spLevel);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return spLevels;
	}

	@RequestMapping(value = "/spLevel/processExcel2007", method = RequestMethod.POST)
	public List<SpLevel> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<SpLevel> spLevels = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				SpLevel spLevel = new SpLevel();
				XSSFRow row = worksheet.getRow(i++);
				//spLevel.setField2(row.getCell(1).getStringCellValue());
				spLevel.setShortCode(row.getCell(0).getStringCellValue());
spLevel.setDescription(row.getCell(1).getStringCellValue());
spLevel.setIsActive(row.getCell(2).getStringCellValue());

				spLevels.add(spLevel);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return spLevels;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/spLevel/excelFileUpload")
	@ResponseBody
	public List<SpLevel> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<SpLevel> spLevels = new ArrayList<SpLevel>();
		if (extension.equalsIgnoreCase("xlsx")) {
			spLevels = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			spLevels = processExcel2003(file);
		}
		
		if (spLevels.size()>0) {
			spLevels.remove(0);
		}
		return spLevels;
	}
}
