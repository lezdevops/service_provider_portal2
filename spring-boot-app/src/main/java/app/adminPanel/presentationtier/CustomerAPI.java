package app.adminPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.Customer;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.adminPanel.businesstier.CustomerManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class CustomerAPI {
	
	@Autowired
	private CustomerManager customerManager;

	@PostMapping("/customer")
	public Map<String,Object> createCustomer(HttpServletRequest request, @RequestBody Customer customer) {
		
		customer.setCreatedDate(null);

		return customerManager.create(request,customer);
	}
	@GetMapping("/customer/{id}")
	public Map<String,Object> getCustomer(HttpServletRequest request, @PathVariable Long id) {
		return customerManager.getCustomerForEdit(request,id);
	}
	@GetMapping(value = "/customers")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return customerManager.getCustomers(request);
	}
	@PutMapping("/customer")
	public Map<String,Object> updateCustomer(HttpServletRequest request, @RequestBody Customer customer) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = customerManager.getCustomer(request,customer.getId());
			Customer newCustomer = (Customer)resultMap.get("customer");
			newCustomer.setName(customer.getName());
newCustomer.setProfileImage(customer.getProfileImage());
newCustomer.setCreatedDate(customer.getCreatedDate());
newCustomer.setIsActive(customer.getIsActive());

			return customerManager.update(request,newCustomer);
		}
		
	}
	@DeleteMapping("/customer/{id}")
	public Map<String,Object> deleteCustomer(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return customerManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/customer/saveMultipleCustomerData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleCustomerData( HttpServletRequest request, @RequestBody List<Customer> customers) {
		return customerManager.saveMultipleCustomerData(request,customers);
	}
	@RequestMapping(value = "/customer/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return customerManager.remove(request,id);
	}
	@RequestMapping(value = "/customer/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return customerManager.removeAll(request);
	}
	@RequestMapping(value = "/customer/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return customerManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/customer/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return customerManager.findPaginatedCustomersBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/customer/searchOnCustomerTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnCustomerTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return customerManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/customer/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<Customer> customers = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Customer customer = new Customer();
				HSSFRow row = worksheet.getRow(i++);
				//customer.setField2(row.getCell(1).getStringCellValue());
				customer.setName(row.getCell(0).getStringCellValue());
customer.setSecurityNumber(row.getCell(1).getStringCellValue());
customer.setProfileImage(row.getCell(2).getStringCellValue());
customer.setEmail(row.getCell(3).getStringCellValue());
customer.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(4).getStringCellValue()));
customer.setIsActive(Double.valueOf(row.getCell(5).getNumericCellValue()).longValue());

				customers.add(customer);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("customers", customers);
		return returnResult;
	}
	@RequestMapping(value = "/customer/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<Customer> customers = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Customer customer = new Customer();
				XSSFRow row = worksheet.getRow(i++);
				//customer.setField2(row.getCell(1).getStringCellValue());
				customer.setName(row.getCell(0).getStringCellValue());
customer.setSecurityNumber(row.getCell(1).getStringCellValue());
customer.setProfileImage(row.getCell(2).getStringCellValue());
customer.setEmail(row.getCell(3).getStringCellValue());
customer.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(4).getStringCellValue()));
customer.setIsActive(Double.valueOf(row.getCell(5).getNumericCellValue()).longValue());

				customers.add(customer);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("customers", customers);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/customer/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<Customer> customers = new ArrayList<Customer>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			customers = (List<Customer>) resultMap.get("customers");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			customers = (List<Customer>) resultMap.get("customers");
		}
		
		if (UtilValidate.isNotEmpty(customers) && customers.size()>0) {
			customers.remove(0);
		}
		returnResult.put("customers", customers);
		return returnResult;
	}
}
