package app.adminPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.Division;
import app.framework.businesstier.HelperUtils;
import app.adminPanel.businesstier.DivisionManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules Division API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class DivisionAPI {

	@Autowired
	private DivisionManager divisionManager;

	@PostMapping("/division")
	public Map<String,Object> createDivision(@RequestBody Division division) {
		division.setCreatedDate(null);

		
		return divisionManager.save(division);
	}
	@GetMapping("/division/{id}")
	public Division getDivision(@PathVariable Long id) {
		return divisionManager.findOne(id);
	}
	@GetMapping(value = "/divisions")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<Division> getAll() {
		return divisionManager.getDivisions();
	}

	@PutMapping("/division")
	public Map<String,Object> updateDivision(@RequestBody Division division) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			Division newDivision = getDivision(division.getId());
			newDivision.setDescription(division.getDescription());
newDivision.setCreatedDate(division.getCreatedDate());

			return divisionManager.save(newDivision);
		}
		
	}
	
	@DeleteMapping("/division/{id}")
	public boolean deleteDivision(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		divisionManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/division/saveMultipleDivisionData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<Division> saveMultipleDivisionData(@RequestBody List<Division> divisions) {
		return divisionManager.saveMultipleDivisionData(divisions);
	}

	@RequestMapping(value = "/division/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = divisionManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/division/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return divisionManager.removeAll();
	}
	@RequestMapping(value = "/division/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<Division> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<Division> resultPage = divisionManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/division/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<Division> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<Division> resultPage = divisionManager.findPaginatedDivisionsBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/division/searchOnDivisionTable", method = RequestMethod.GET)
    public Page<Division> searchOnDivisionTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return divisionManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/division/processExcel2003", method = RequestMethod.POST)
	public List<Division> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<Division> divisions = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Division division = new Division();
				HSSFRow row = worksheet.getRow(i++);
				//division.setField2(row.getCell(1).getStringCellValue());
				division.setDescription(row.getCell(0).getStringCellValue());
division.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				divisions.add(division);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return divisions;
	}

	@RequestMapping(value = "/division/processExcel2007", method = RequestMethod.POST)
	public List<Division> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<Division> divisions = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Division division = new Division();
				XSSFRow row = worksheet.getRow(i++);
				//division.setField2(row.getCell(1).getStringCellValue());
				division.setDescription(row.getCell(0).getStringCellValue());
division.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				divisions.add(division);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return divisions;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/division/excelFileUpload")
	@ResponseBody
	public List<Division> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<Division> divisions = new ArrayList<Division>();
		if (extension.equalsIgnoreCase("xlsx")) {
			divisions = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			divisions = processExcel2003(file);
		}
		
		if (divisions.size()>0) {
			divisions.remove(0);
		}
		return divisions;
	}
}
