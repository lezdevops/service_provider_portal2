package app.adminPanel.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.Location;
import app.framework.businesstier.HelperUtils;
import app.adminPanel.businesstier.LocationManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules Location API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class LocationAPI {

	@Autowired
	private LocationManager locationManager;

	@PostMapping("/location")
	public Map<String,Object> createLocation(@RequestBody Location location) {
		location.setCreatedDate(null);

		
		return locationManager.save(location);
	}
	@GetMapping("/location/{id}")
	public Location getLocation(@PathVariable Long id) {
		return locationManager.findOne(id);
	}
	@GetMapping(value = "/locations")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<Location> getAll() {
		return locationManager.getLocations();
	}

	@PutMapping("/location")
	public Map<String,Object> updateLocation(@RequestBody Location location) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		}else {
			Location newLocation = getLocation(location.getId());
			newLocation.setDescription(location.getDescription());
newLocation.setCreatedDate(location.getCreatedDate());

			return locationManager.save(newLocation);
		}
		
	}
	
	@DeleteMapping("/location/{id}")
	public boolean deleteLocation(@PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		}else {
		locationManager.remove(id);
		return true;
		}
	}


	@RequestMapping(value = "/location/saveMultipleLocationData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<Location> saveMultipleLocationData(@RequestBody List<Location> locations) {
		return locationManager.saveMultipleLocationData(locations);
	}

	@RequestMapping(value = "/location/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public Map<String,Object> remove(@PathVariable Long id) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = locationManager.remove(id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		}else{
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}
	@RequestMapping(value = "/location/removeAll", method = RequestMethod.POST)
	public String removeAll() {
		return locationManager.removeAll();
	}
	@RequestMapping(value = "/location/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<Location> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<Location> resultPage = locationManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/location/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Page<Location> getPaginatedBy(@RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
		 
        Page<Location> resultPage = locationManager.findPaginatedLocationsBySearchToken(page, size,searchToken);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	@RequestMapping(value = "/location/searchOnLocationTable", method = RequestMethod.GET)
    public Page<Location> searchOnLocationTable(@RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return locationManager.findById(id, pageRequest);
    }
	@RequestMapping(value = "/location/processExcel2003", method = RequestMethod.POST)
	public List<Location> processExcel2003(@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<Location> locations = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Location location = new Location();
				HSSFRow row = worksheet.getRow(i++);
				//location.setField2(row.getCell(1).getStringCellValue());
				location.setDescription(row.getCell(0).getStringCellValue());
location.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				locations.add(location);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return locations;
	}

	@RequestMapping(value = "/location/processExcel2007", method = RequestMethod.POST)
	public List<Location> processExcel2007(@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<Location> locations = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Location location = new Location();
				XSSFRow row = worksheet.getRow(i++);
				//location.setField2(row.getCell(1).getStringCellValue());
				location.setDescription(row.getCell(0).getStringCellValue());
location.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				locations.add(location);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return locations;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/location/excelFileUpload")
	@ResponseBody
	public List<Location> excelFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<Location> locations = new ArrayList<Location>();
		if (extension.equalsIgnoreCase("xlsx")) {
			locations = processExcel2007(file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			locations = processExcel2003(file);
		}
		
		if (locations.size()>0) {
			locations.remove(0);
		}
		return locations;
	}
}
