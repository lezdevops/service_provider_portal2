package app.framework.businesstier;

import java.util.ArrayList;
import java.util.List;

public class CommonUtils {
	public static List<String> getEntityNames(String[] filesLines) {
		List<String> entityNames = new ArrayList<String>();
		for (String fileLine : filesLines) {
			if (fileLine.contains("<entity name=") && fileLine.contains("\">")) {
				String entityName = fileLine.substring(
						fileLine.indexOf("<entity name=\"")
								+ ("<entity name=\"").length(),
						fileLine.indexOf("\">"));
				entityNames.add(entityName);
			}
		}
		return entityNames;
	}

	public static List<String> getEntityFields(String[] filesLines,
			String entityName) {

		List<String> fieldNames = new ArrayList<String>();
		List<String> fieldTypes = new ArrayList<String>();
		List<String> relationalFields = new ArrayList<String>();
		List<String> relationalFieldNames = new ArrayList<String>();
		List<String> relationalEntityNames = new ArrayList<String>();

		int l = 0;// , m = 0;
		for (int k = 0; k < filesLines.length; k++) {
			String fileLine = filesLines[k];

			/* <field name="categoryId" type="Long" /> */
			if (fileLine.contains("<entity name=\"" + entityName + "\"")) {

				while (!fileLine.contains("</entity>")) {
					/* <field name="categoryId" type="Long" /> */
					fileLine = filesLines[k++];
					if (fileLine.contains("<field name=\"")) {

						String fieldNameTemp = fileLine.substring(
								fileLine.indexOf("<field name=\"")
										+ ("<field name=\"").length(),
								fileLine.indexOf("\" type="));
						fieldNames.add(fieldNameTemp);
						l++;
					}
					if (fileLine.contains("<field name=\"")) {
						String terminalPoint = fileLine.contains("\"></field>") ? "\"></field>"
								: "\" />";
						String fieldTypeTemp = CommonFileUtils.getJavaType(fileLine.substring(
								fileLine.indexOf("type=\"")
										+ ("type=\"").length(),
								fileLine.indexOf(terminalPoint)));
						fieldTypes.add(fieldTypeTemp);
					}
					if (fileLine.contains("<prim-key field=\"")) {
						String entityNameId = fileLine.substring(
								fileLine.indexOf("<prim-key field=\"")
										+ ("<prim-key field=\"").length(),
								fileLine.indexOf("\" />"));
					}
					if (fileLine.contains("<relation type=")) {
						while (!fileLine.contains("</relation>")) {

							if (fileLine.contains("rel-entity-name=\"")) {
								relationalEntityNames.add(fileLine.substring(
										fileLine.indexOf("rel-entity-name=\"")
												+ ("rel-entity-name=\"")
														.length(),
										fileLine.indexOf("\">")));
							}
							if (fileLine.contains("<key-map field-name=\"")) {
								relationalFields
										.add(fileLine.substring(
												fileLine.indexOf("<key-map field-name=\"")
														+ ("<key-map field-name=\"")
																.length(),
												fileLine.indexOf("\" rel-field")));
							}
							if (fileLine.contains("rel-field-name=\"")) {
								relationalFieldNames.add(fileLine.substring(
										fileLine.indexOf("rel-field-name=\"")
												+ ("rel-field-name=\"")
														.length(),
										fileLine.indexOf("\" />")));
							}
							fileLine = filesLines[k++];
						}
					}
				}

			}
		}
		return fieldNames;
	}
}
