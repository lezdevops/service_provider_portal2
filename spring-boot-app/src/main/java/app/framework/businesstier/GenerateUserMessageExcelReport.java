package app.framework.businesstier;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import app.framework.persistencetier.UserMessage;

public class GenerateUserMessageExcelReport extends AbstractXlsView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// change the file name
		response.setHeader("Content-Disposition",
				"attachment; filename=\"all-userMessage-list.xls\"");

		@SuppressWarnings("unchecked")
		List<UserMessage> userMessages = (List<UserMessage>) model.get("userMessages");

		// create excel xls sheet
		Sheet sheet = workbook.createSheet("User Detail");
		sheet.setDefaultColumnWidth(30);

		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.BLUE.index);
		//style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		font.setBold(true);
		font.setColor(HSSFColor.BLACK.index);
		style.setFont(font);

		// create header row
		Row header = sheet.createRow(0);		
		header.createCell(0).setCellValue("Message Id");header.getCell(0).setCellStyle(style);header.createCell(1).setCellValue("Sender");header.getCell(1).setCellStyle(style);header.createCell(2).setCellValue("Receiver");header.getCell(2).setCellStyle(style);header.createCell(3).setCellValue("Message");header.getCell(3).setCellStyle(style);header.createCell(4).setCellValue("Created Date");header.getCell(4).setCellStyle(style);header.createCell(5).setCellValue("Seen Date");header.getCell(5).setCellStyle(style);header.createCell(6).setCellValue("Message Status");header.getCell(6).setCellStyle(style);

		
		/*header.createCell(0).setCellValue("UserMessage Id");header.getCell(0).setCellStyle(style);
		 * header.createCell(1).setCellValue("UserMessage Name");
		header.getCell(1).setCellStyle(style);
		header.createCell(2).setCellValue("Email");
		header.getCell(2).setCellStyle(style);
		header.createCell(3).setCellValue("District");
		header.getCell(3).setCellStyle(style);*/

		int rowCount = 1;

		for (UserMessage userMessage : userMessages) {
			Row sheetRow = sheet.createRow(rowCount++);
			
			 if(UtilValidate.isNotEmpty(userMessage.getUserMessageId())){
sheetRow.createCell(0).setCellValue(userMessage.getUserMessageId().toString());}else{
sheetRow.createCell(0).setCellValue("");}
if(UtilValidate.isNotEmpty(userMessage.getSenderId())){
sheetRow.createCell(1).setCellValue(userMessage.getSenderId().toString());}else{
sheetRow.createCell(1).setCellValue("");}
if(UtilValidate.isNotEmpty(userMessage.getReceiverId())){
sheetRow.createCell(2).setCellValue(userMessage.getReceiverId().toString());}else{
sheetRow.createCell(2).setCellValue("");}
if(UtilValidate.isNotEmpty(userMessage.getMessage())){
sheetRow.createCell(3).setCellValue(userMessage.getMessage().toString());}else{
sheetRow.createCell(3).setCellValue("");}
if(UtilValidate.isNotEmpty(userMessage.getCreatedDate())){
sheetRow.createCell(4).setCellValue(userMessage.getCreatedDate().toString());}else{
sheetRow.createCell(4).setCellValue("");}
if(UtilValidate.isNotEmpty(userMessage.getSeenDate())){
sheetRow.createCell(5).setCellValue(userMessage.getSeenDate().toString());}else{
sheetRow.createCell(5).setCellValue("");}
if(UtilValidate.isNotEmpty(userMessage.getMessageStatus())){
sheetRow.createCell(6).setCellValue(userMessage.getMessageStatus().toString());}else{
sheetRow.createCell(6).setCellValue("");}

			
			/*sheetRow.createCell(0).setCellValue(userMessage.getUserMessageId());
			 * sheetRow.createCell(1).setCellValue(userMessage.getUserMessageName());
			sheetRow.createCell(2).setCellValue(userMessage.getEmail());
			sheetRow.createCell(3).setCellValue(userMessage.getDistrictId());*/

		}
	}

}
