package app.framework.businesstier;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import app.framework.persistencetier.TaskManagement;

public class GenerateTaskManagementExcelReport extends AbstractXlsView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// change the file name
		response.setHeader("Content-Disposition",
				"attachment; filename=\"all-taskManagement-list.xls\"");

		@SuppressWarnings("unchecked")
		List<TaskManagement> taskManagements = (List<TaskManagement>) model.get("taskManagements");

		// create excel xls sheet
		Sheet sheet = workbook.createSheet("User Detail");
		sheet.setDefaultColumnWidth(30);

		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.BLUE.index);
		//style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		font.setBold(true);
		font.setColor(HSSFColor.BLACK.index);
		style.setFont(font);

		// create header row
		Row header = sheet.createRow(0);		
		header.createCell(0).setCellValue("Task Id");header.getCell(0).setCellStyle(style);header.createCell(1).setCellValue("Assignee Id");header.getCell(1).setCellStyle(style);header.createCell(2).setCellValue("reporterId");header.getCell(2).setCellStyle(style);header.createCell(3).setCellValue("Task Details");header.getCell(3).setCellStyle(style);header.createCell(4).setCellValue("Created Date");header.getCell(4).setCellStyle(style);header.createCell(5).setCellValue("Plan Report Date");header.getCell(5).setCellStyle(style);header.createCell(6).setCellValue("Actual Report Date");header.getCell(6).setCellStyle(style);header.createCell(7).setCellValue("Complete Parcentage");header.getCell(7).setCellStyle(style);header.createCell(8).setCellValue("Task Status");header.getCell(8).setCellStyle(style);

		
		/*header.createCell(0).setCellValue("TaskManagement Id");header.getCell(0).setCellStyle(style);
		 * header.createCell(1).setCellValue("TaskManagement Name");
		header.getCell(1).setCellStyle(style);
		header.createCell(2).setCellValue("Email");
		header.getCell(2).setCellStyle(style);
		header.createCell(3).setCellValue("District");
		header.getCell(3).setCellStyle(style);*/

		int rowCount = 1;

		for (TaskManagement taskManagement : taskManagements) {
			Row sheetRow = sheet.createRow(rowCount++);
			
			 if(UtilValidate.isNotEmpty(taskManagement.getTaskId())){
sheetRow.createCell(0).setCellValue(taskManagement.getTaskId().toString());}else{
sheetRow.createCell(0).setCellValue("");}
if(UtilValidate.isNotEmpty(taskManagement.getAssigneeId())){
sheetRow.createCell(1).setCellValue(taskManagement.getAssigneeId().toString());}else{
sheetRow.createCell(1).setCellValue("");}
if(UtilValidate.isNotEmpty(taskManagement.getReporterId())){
sheetRow.createCell(2).setCellValue(taskManagement.getReporterId().toString());}else{
sheetRow.createCell(2).setCellValue("");}
if(UtilValidate.isNotEmpty(taskManagement.getTaskDetails())){
sheetRow.createCell(3).setCellValue(taskManagement.getTaskDetails().toString());}else{
sheetRow.createCell(3).setCellValue("");}
if(UtilValidate.isNotEmpty(taskManagement.getCreatedDate())){
sheetRow.createCell(4).setCellValue(taskManagement.getCreatedDate().toString());}else{
sheetRow.createCell(4).setCellValue("");}
if(UtilValidate.isNotEmpty(taskManagement.getPlanReportDate())){
sheetRow.createCell(5).setCellValue(taskManagement.getPlanReportDate().toString());}else{
sheetRow.createCell(5).setCellValue("");}
if(UtilValidate.isNotEmpty(taskManagement.getActualReportDate())){
sheetRow.createCell(6).setCellValue(taskManagement.getActualReportDate().toString());}else{
sheetRow.createCell(6).setCellValue("");}
if(UtilValidate.isNotEmpty(taskManagement.getCompleteParcentage())){
sheetRow.createCell(7).setCellValue(taskManagement.getCompleteParcentage().toString());}else{
sheetRow.createCell(7).setCellValue("");}
if(UtilValidate.isNotEmpty(taskManagement.getTaskStatus())){
sheetRow.createCell(8).setCellValue(taskManagement.getTaskStatus().toString());}else{
sheetRow.createCell(8).setCellValue("");}

			
			/*sheetRow.createCell(0).setCellValue(taskManagement.getTaskManagementId());
			 * sheetRow.createCell(1).setCellValue(taskManagement.getTaskManagementName());
			sheetRow.createCell(2).setCellValue(taskManagement.getEmail());
			sheetRow.createCell(3).setCellValue(taskManagement.getDistrictId());*/

		}
	}

}
