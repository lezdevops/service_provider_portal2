package app.framework.businesstier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import app.framework.persistencetier.UserMessage;
import app.framework.persistencetier.UserMessageRepository;
//newDependentBeanImportGoesHere

@Service
public class UserMessageManager {
	@Autowired
	private UserMessageRepository userMessageRepository;
	@Autowired
	private UserMessageValidator userMessageValidator;
	// newAutowiredPropertiesGoesHere
	
	public List<UserMessage> getUserMessages(){
		List<UserMessage> userMessagesList = new ArrayList<UserMessage>();
		Iterable<UserMessage> userMessages = userMessageRepository.findAll();
		for (UserMessage userMessage : userMessages) {
			userMessagesList.add(userMessage);
			// newGenerateEntityFetchActionGoesHere
		}
		return userMessagesList;
	}
	public List<Map<String,Object>> save(@RequestBody UserMessage userMessage) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			
			userMessageValidator.validate(userMessage);
			userMessage = userMessageRepository.save(userMessage);
			messageMap.put("addSucessMessage", "Successfully UserMessage Added.");
			messageList.add(messageMap);
			// newGenerateEntityAddActionGoesHere
			return messageList;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(UserMessageManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return messageList;
	}
	public List<UserMessage> saveMultipleUserMessageData(@RequestBody List<UserMessage> userMessages) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (UserMessage userMessage : userMessages) {
				userMessageValidator.validate(userMessage);
				userMessage = userMessageRepository.save(userMessage);
				messageMap.put("addSucessmessage", "Successfully UserMessage Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getUserMessages();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(UserMessageManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getUserMessages();
	}
	public String remove(@PathVariable long id) {
		String removeMessage = "Successfully UserMessage Deleted.";
		userMessageRepository.delete(id);
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<UserMessage> findPaginated(int page, int size) {
        return userMessageRepository.findAll(new PageRequest(page, size));
    }
	public List<UserMessage> getUserMessagesByMessageType(String senderMessageType) {
		return userMessageRepository.findUserMessagesBySenderMessageType(senderMessageType);
	}
	public List<UserMessage> getUserMessagesBySenderId(Long senderId) {
		return userMessageRepository.findUserMessagesBySenderId(senderId);
	}
	public List<UserMessage> getUserMessagesByReceiverId(Long receiverId) {
		return userMessageRepository.findUserMessagesByReceiverId(receiverId);
	}
}
