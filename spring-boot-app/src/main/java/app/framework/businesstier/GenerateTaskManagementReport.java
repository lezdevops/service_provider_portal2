package app.framework.businesstier;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.framework.persistencetier.TaskManagement;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class GenerateTaskManagementReport extends AbstractPdfView {

    @Override
    protected void buildPdfDocument(Map<String, Object> model,Document document, PdfWriter writer,
    		HttpServletRequest request,HttpServletResponse response) throws Exception {

        List<TaskManagement> taskManagements = (List<TaskManagement>) model.get("taskManagements");
        
        PdfPTable table = new PdfPTable(9);
        table.setWidthPercentage(98);
        //3,3,3,3,3,3,3, = 3,3,3,
        table.setWidths(new int[] {1, 3,3,3,3,3,3,3, 3});

        Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

        PdfPCell hcell;
        
        hcell = new PdfPCell(new Phrase("TaskId", headFont));
hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(hcell);hcell = new PdfPCell(new Phrase("AssigneeId", headFont));
hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(hcell);hcell = new PdfPCell(new Phrase("reporter Id", headFont));
hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(hcell);hcell = new PdfPCell(new Phrase("TaskDetails", headFont));
hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(hcell);hcell = new PdfPCell(new Phrase("CreatedDate", headFont));
hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(hcell);hcell = new PdfPCell(new Phrase("PlanReportDate", headFont));
hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(hcell);hcell = new PdfPCell(new Phrase("ActualReportDate", headFont));
hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(hcell);hcell = new PdfPCell(new Phrase("CompleteParcentage", headFont));
hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(hcell);hcell = new PdfPCell(new Phrase("TaskStatus", headFont));
hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(hcell);



        for (TaskManagement taskManagement : taskManagements) {

            PdfPCell cell = null;

            
            if(UtilValidate.isNotEmpty(taskManagement.getTaskId())){cell = new PdfPCell(new Phrase(taskManagement.getTaskId().toString()));}else{
cell = new PdfPCell(new Phrase(""));}
cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
cell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(cell);
if(UtilValidate.isNotEmpty(taskManagement.getAssigneeId())){cell = new PdfPCell(new Phrase(taskManagement.getAssigneeId().toString()));}else{
cell = new PdfPCell(new Phrase(""));}
cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
cell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(cell);
if(UtilValidate.isNotEmpty(taskManagement.getReporterId())){cell = new PdfPCell(new Phrase(taskManagement.getReporterId().toString()));}else{
cell = new PdfPCell(new Phrase(""));}
cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
cell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(cell);
if(UtilValidate.isNotEmpty(taskManagement.getTaskDetails())){cell = new PdfPCell(new Phrase(taskManagement.getTaskDetails().toString()));}else{
cell = new PdfPCell(new Phrase(""));}
cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
cell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(cell);
if(UtilValidate.isNotEmpty(taskManagement.getCreatedDate())){cell = new PdfPCell(new Phrase(taskManagement.getCreatedDate().toString()));}else{
cell = new PdfPCell(new Phrase(""));}
cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
cell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(cell);
if(UtilValidate.isNotEmpty(taskManagement.getPlanReportDate())){cell = new PdfPCell(new Phrase(taskManagement.getPlanReportDate().toString()));}else{
cell = new PdfPCell(new Phrase(""));}
cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
cell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(cell);
if(UtilValidate.isNotEmpty(taskManagement.getActualReportDate())){cell = new PdfPCell(new Phrase(taskManagement.getActualReportDate().toString()));}else{
cell = new PdfPCell(new Phrase(""));}
cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
cell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(cell);
if(UtilValidate.isNotEmpty(taskManagement.getCompleteParcentage())){cell = new PdfPCell(new Phrase(taskManagement.getCompleteParcentage().toString()));}else{
cell = new PdfPCell(new Phrase(""));}
cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
cell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(cell);
if(UtilValidate.isNotEmpty(taskManagement.getTaskStatus())){cell = new PdfPCell(new Phrase(taskManagement.getTaskStatus().toString()));}else{
cell = new PdfPCell(new Phrase(""));}
cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
cell.setHorizontalAlignment(Element.ALIGN_CENTER);
table.addCell(cell);

            
        }

        document.add(table);
    }
}