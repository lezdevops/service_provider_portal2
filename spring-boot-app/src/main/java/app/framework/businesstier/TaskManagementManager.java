package app.framework.businesstier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import app.framework.persistencetier.TaskManagement;
import app.framework.persistencetier.TaskManagementRepository;
//newDependentBeanImportGoesHere

@Service
public class TaskManagementManager {
	@Autowired
	private TaskManagementRepository taskManagementRepository;
	@Autowired
	private TaskManagementValidator taskManagementValidator;
	// newAutowiredPropertiesGoesHere
	
	public List<TaskManagement> getTaskManagements(){
		List<TaskManagement> taskManagementsList = new ArrayList<TaskManagement>();
		Iterable<TaskManagement> taskManagements = taskManagementRepository.findAll();
		for (TaskManagement taskManagement : taskManagements) {
			taskManagementsList.add(taskManagement);
			// newGenerateEntityFetchActionGoesHere
		}
		return taskManagementsList;
	}
	public List<Map<String,Object>> save(@RequestBody TaskManagement taskManagement) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			
			taskManagementValidator.validate(taskManagement);
			taskManagement = taskManagementRepository.save(taskManagement);
			messageMap.put("addSucessMessage", "Successfully TaskManagement Added.");
			messageList.add(messageMap);
			// newGenerateEntityAddActionGoesHere
			return messageList;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(TaskManagementManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return messageList;
	}

	public List<TaskManagement> saveMultipleTaskManagementData(@RequestBody List<TaskManagement> taskManagements) {
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (TaskManagement taskManagement : taskManagements) {
				taskManagementValidator.validate(taskManagement);
				taskManagement = taskManagementRepository.save(taskManagement);
				messageMap.put("addSucessmessage", "Successfully TaskManagement Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getTaskManagements();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(TaskManagementManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getTaskManagements();
	}
	public String remove(@PathVariable long id) {
		String removeMessage = "Successfully TaskManagement Deleted.";
		taskManagementRepository.delete(id);
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}
	public Page<TaskManagement> findPaginated(int page, int size) {
        return taskManagementRepository.findAll(new PageRequest(page, size));
    }
	public List<TaskManagement> findTaskManagementsByAssigneeId(Long assigneeId) {
		return taskManagementRepository.findTaskManagementsByAssigneeId(assigneeId);
	}
}
