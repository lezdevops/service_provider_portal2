package app.framework.businesstier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * A utility class that reads/saves SMTP settings from/to a properties file.
 * @author www.codejava.net
 *
 */
public class ConfigUtility {
	
	private File configFile = new File("smtp.properties"); 
	public ConfigUtility(File file){
		this.configFile = file;
	}
	
	private Properties configProps;
	
	public Properties loadProperties() throws IOException {
		/*Properties defaultProps = new Properties();
		// sets default properties
		defaultProps.setProperty("mail.smtp.host", "smtp.gmail.com");
		defaultProps.setProperty("mail.smtp.port", "587");
		defaultProps.setProperty("mail.user", "sabbir@gmail.com");
		defaultProps.setProperty("mail.password", "secret");
		defaultProps.setProperty("mail.smtp.starttls.enable", "true");
		defaultProps.setProperty("mail.smtp.auth", "true");*/
		
		//configProps = new Properties(defaultProps);
		configProps = new Properties();
		
		// loads properties from file
		if (configFile.exists()) {
			InputStream inputStream = new FileInputStream(configFile);
			//configProps.load(inputStream);
			configProps.load(new InputStreamReader(inputStream, "UTF-8"));
			inputStream.close();
		}
		
		return configProps;
	}
	
	public void saveProperties(List<Map<String,Object>> properties,Set<String> keys) throws IOException {
		configProps = new Properties();
		for (String key : keys) {
			for (Map<String, Object> property : properties) {
				configProps.setProperty(key, property.get(key).toString());
			}
		}
		/*
		configProps.setProperty("mail.smtp.host", host);
		configProps.setProperty("mail.smtp.port", port);
		configProps.setProperty("mail.user", user);
		configProps.setProperty("mail.password", pass);
		configProps.setProperty("mail.smtp.starttls.enable", "true");
		configProps.setProperty("mail.smtp.auth", "true");
		*/
		OutputStream outputStream = new FileOutputStream(configFile);
		configProps.store(outputStream, "host setttings");
		outputStream.close();
	}	
}