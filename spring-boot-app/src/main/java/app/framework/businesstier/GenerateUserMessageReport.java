package app.framework.businesstier;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.framework.persistencetier.UserMessage;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class GenerateUserMessageReport extends AbstractPdfView {

	@Override
	protected void buildPdfDocument(Map<String, Object> model,
			Document document, PdfWriter writer, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		List<UserMessage> userMessages = (List<UserMessage>) model
				.get("userMessages");

		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(98);
		// 3,3,3,3,3, = 3,3,3,
		table.setWidths(new int[] { 1, 3, 3, 3, 3, 3, 3 });

		Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

		PdfPCell hcell;

		hcell = new PdfPCell(new Phrase("MessageId", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);
		hcell = new PdfPCell(new Phrase("Sender", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);
		hcell = new PdfPCell(new Phrase("Receiver", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);
		hcell = new PdfPCell(new Phrase("Message", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);
		hcell = new PdfPCell(new Phrase("CreatedDate", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);
		hcell = new PdfPCell(new Phrase("SeenDate", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);
		hcell = new PdfPCell(new Phrase("MessageStatus", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);

		for (UserMessage userMessage : userMessages) {

			PdfPCell cell = null;

			if (UtilValidate.isNotEmpty(userMessage.getUserMessageId())) {
				cell = new PdfPCell(new Phrase(userMessage.getUserMessageId()
						.toString()));
			} else {
				cell = new PdfPCell(new Phrase(""));
			}
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			
			if (UtilValidate.isNotEmpty(userMessage.getSenderId())) {
				cell = new PdfPCell(new Phrase(userMessage.getSenderId()
						.toString()));
			} else {
				cell = new PdfPCell(new Phrase(""));
			}
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			
			
			if (UtilValidate.isNotEmpty(userMessage.getReceiverId())) {
				cell = new PdfPCell(new Phrase(userMessage.getReceiverId()
						.toString()));
			} else {
				cell = new PdfPCell(new Phrase(""));
			}
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			if (UtilValidate.isNotEmpty(userMessage.getMessage())) {
				cell = new PdfPCell(new Phrase(userMessage.getMessage()
						.toString()));
			} else {
				cell = new PdfPCell(new Phrase(""));
			}
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			if (UtilValidate.isNotEmpty(userMessage.getCreatedDate())) {
				cell = new PdfPCell(new Phrase(userMessage.getCreatedDate()
						.toString()));
			} else {
				cell = new PdfPCell(new Phrase(""));
			}
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			if (UtilValidate.isNotEmpty(userMessage.getSeenDate())) {
				cell = new PdfPCell(new Phrase(userMessage.getSeenDate()
						.toString()));
			} else {
				cell = new PdfPCell(new Phrase(""));
			}
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			if (UtilValidate.isNotEmpty(userMessage.getMessageStatus())) {
				cell = new PdfPCell(new Phrase(userMessage.getMessageStatus()
						.toString()));
			} else {
				cell = new PdfPCell(new Phrase(""));
			}
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
		}

		document.add(table);
	}
}