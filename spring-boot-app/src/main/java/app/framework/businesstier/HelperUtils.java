package app.framework.businesstier;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/*import net.sf.json.JSONArray;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;*/


public class HelperUtils {


    /** Check whether an object is NOT empty, will see if it is a String, Map, Collection, etc. */
    public static boolean isNotEmpty(Object o) {
        return !isEmpty(o);
    }
    @SuppressWarnings("unchecked")
    public static boolean isEmpty(Object value) {
        if (value == null) return true;

        if (value instanceof String) return ((String) value).length() == 0;
        if (value instanceof Collection) return ((Collection<? extends Object>) value).size() == 0;
        if (value instanceof Map) return ((Map<? extends Object, ? extends Object>) value).size() == 0;
        if (value instanceof CharSequence) return ((CharSequence) value).length() == 0;
        if (value instanceof IsEmpty) return ((IsEmpty) value).isEmpty();

        // These types would flood the log
        // Number covers: BigDecimal, BigInteger, Byte, Double, Float, Integer, Long, Short
        if (value instanceof Boolean) return false;
        if (value instanceof Number) return false;
        if (value instanceof Character) return false;
        if (value instanceof java.util.Date) return false;

        return false;
    }


    /** Check whether IsEmpty o is empty. */
    public static boolean isEmpty(IsEmpty o) {
        return o == null || o.isEmpty();
    }

    /** Check whether IsEmpty o is NOT empty. */
    public static boolean isNotEmpty(IsEmpty o) {
        return o != null && !o.isEmpty();
    }

    /** Check whether string s is empty. */
    public static boolean isEmpty(String s) {
        return (s == null) || s.length() == 0;
    }

    /** Check whether collection c is empty. */
    public static <E> boolean isEmpty(Collection<E> c) {
        return (c == null) || c.isEmpty();
    }

    /** Check whether map m is empty. */
    public static <K,E> boolean isEmpty(Map<K,E> m) {
        return (m == null) || m.isEmpty();
    }

    /** Check whether charsequence c is empty. */
    public static <E> boolean isEmpty(CharSequence c) {
        return (c == null) || c.length() == 0;
    }

    /** Check whether string s is NOT empty. */
    public static boolean isNotEmpty(String s) {
        return (s != null) && s.length() > 0;
    }

    /** Check whether collection c is NOT empty. */
    public static <E> boolean isNotEmpty(Collection<E> c) {
        return (c != null) && !c.isEmpty();
    }

    /** Check whether charsequence c is NOT empty. */
    public static <E> boolean isNotEmpty(CharSequence c) {
        return ((c != null) && (c.length() > 0));
    }
	
	public static String getRandomColor() {
		String[] letters = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"A", "B", "C", "D", "E", "F" };
		String color = "#";
		for (int i = 0; i < 6; i++) {
			color += letters[(int) Math.round(Math.random() * 15)];
		}
		return color;
	}

	public static Timestamp getDateAfterAdditionOfDays(Timestamp ts, Long days) {
		java.sql.Timestamp dateTSAfterAddition = new Timestamp(ts.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateTSAfterAddition);
		cal.add(Calendar.DAY_OF_WEEK, days.intValue());
		dateTSAfterAddition.setTime(cal.getTime().getTime()); // or
		dateTSAfterAddition = new Timestamp(cal.getTime().getTime());
		return dateTSAfterAddition;
	}
	public static Timestamp getDateAfterSubtractionOfDays(Timestamp ts,
			Long days) {
		java.sql.Timestamp dateTSAfterSubtraction = new Timestamp(ts.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateTSAfterSubtraction);
		cal.add(Calendar.DAY_OF_WEEK, -days.intValue());
		dateTSAfterSubtraction.setTime(cal.getTime().getTime()); // or
		dateTSAfterSubtraction = new Timestamp(cal.getTime().getTime());
		return dateTSAfterSubtraction;
	}
	public static java.sql.Date getSqlDateAfterAdditionOfDays(java.sql.Date date, Long days) {
		java.sql.Date resultSqlDate = null;
		java.sql.Timestamp dateTSAfterAddition = new Timestamp(date.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateTSAfterAddition);
		cal.add(Calendar.DAY_OF_WEEK, days.intValue());
		dateTSAfterAddition.setTime(cal.getTime().getTime()); // or
		dateTSAfterAddition = new Timestamp(cal.getTime().getTime());
		resultSqlDate = getDateFromTimestamp(dateTSAfterAddition);
		return resultSqlDate;
	}
   
	public static java.sql.Date getSqlDateAfterSubtractionOfDays(java.sql.Date date,
			Long days) {
		java.sql.Date resultSqlDate = null;
		java.sql.Timestamp dateTSAfterSubtraction = new Timestamp(date.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateTSAfterSubtraction);
		cal.add(Calendar.DAY_OF_WEEK, -days.intValue());
		dateTSAfterSubtraction.setTime(cal.getTime().getTime()); // or
		dateTSAfterSubtraction = new Timestamp(cal.getTime().getTime());
		resultSqlDate = getDateFromTimestamp(dateTSAfterSubtraction);
		return resultSqlDate;
	}
	public static Timestamp getTimestampAfterAdditionsOfMonths(java.sql.Timestamp ts, Long months){
	   java.sql.Timestamp dateTSAfterAddition = new Timestamp(ts.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateTSAfterAddition);
		cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH)+months.intValue()));
		dateTSAfterAddition.setTime(cal.getTime().getTime()); // or
		dateTSAfterAddition = new Timestamp(cal.getTime().getTime());
		return dateTSAfterAddition; 	    	
	 }
	public static Timestamp getTimestampAfterSubtractionOfMonths(java.sql.Timestamp ts, Long months){
		java.sql.Timestamp dateTSAfterSubtraction = new Timestamp(ts.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateTSAfterSubtraction);
		cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH)-months.intValue()));
		dateTSAfterSubtraction.setTime(cal.getTime().getTime()); // or
		dateTSAfterSubtraction = new Timestamp(cal.getTime().getTime());
		return dateTSAfterSubtraction;  	
	}
   /* public static void main(String[] args) {
    	java.sql.Timestamp today = getToday();
		System.out.println(getTimestampAfterAdditionsOfMonths(today, java.lang.Long.valueOf(5)));
		System.out.println(getTimestampAfterAdditionsOfMonths(today, java.lang.Long.valueOf(5)));
	}*/
	public static Timestamp getToday() {
		Timestamp today = nowTimestamp();
		Calendar todayCalDate = Calendar.getInstance();

		todayCalDate.set(Calendar.HOUR, 0);
		todayCalDate.set(Calendar.MINUTE, 0);
		todayCalDate.set(Calendar.SECOND, 0);
		todayCalDate.set(Calendar.AM_PM, 0);
		todayCalDate.set(Calendar.MILLISECOND, 0);

		today.setTime(todayCalDate.getTimeInMillis());
		return today;
	}
	 public static java.sql.Timestamp nowTimestamp() {
	        return getTimestamp(System.currentTimeMillis());
	    }

	    /**
	     * Convert a millisecond value to a Timestamp.
	     * @param time millsecond value
	     * @return Timestamp
	     */
	
	public static java.sql.Timestamp getTimestamp(long time) {
        return new java.sql.Timestamp(time);
    }

	public static java.sql.Date getDateFromTimestamp(Timestamp timeStampValue) {
		java.sql.Date date = new java.sql.Date(timeStampValue.getTime());

		return date;
	}
	public static String getYearMonthFromTimestamp(Timestamp timeStampValue) {
		String yearMonth = "";
		String yearStr = "";
		String monthStr = "";
		
		Long month = getMonthInt(timeStampValue);
		monthStr = month<10 ? "0"+month: month.toString();
		Long year = getYearLong(timeStampValue);
		monthStr = year<1000 ? "0"+year: year.toString();
		
		yearMonth = yearMonth + yearStr +monthStr;
		return yearMonth;
	}

	public static Timestamp getTimestampFromDate(Date dateValue) {
		Timestamp date = new Timestamp(dateValue.getTime());
		return date;
	}

	public static Date getDateFromString(String str_date, DateFormat formatter) {
		java.util.Date utilDate = new java.util.Date();
		java.sql.Date sqlDate = null;

		try {
			utilDate = formatter.parse(str_date);
			sqlDate = new java.sql.Date(utilDate.getTime());

		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return sqlDate;
	}

	public static java.sql.Date getDateFromString(String str_date) {
		java.util.Date utilDate = new java.util.Date();
		java.sql.Date sqlDate = null;
		DateFormat formatter = null;
		if (str_date.contains("/")) {
			formatter = new SimpleDateFormat("dd/MM/yyyy");
		} else {
			formatter = new SimpleDateFormat("yyyy-MM-dd");
		}
		try {
			utilDate = formatter.parse(str_date);
			sqlDate = new java.sql.Date(utilDate.getTime());

		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return sqlDate;
	}

	public static String getStringFromDate(Date date, DateFormat formatter) {
		String str_date = "";
		str_date = formatter.format(date);

		return str_date;
	}

	public static List<String> getFirstAndLastDate(int year, int month) {
		List<String> dateLists = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		int date = 1;
		int maxDay = 0;
		calendar.set(year, month, date);
		String startDateStr = formatter.format(calendar.getTime());

		// System.out.println("First Day: " +
		// formatter.format(calendar.getTime()));

		// Getting Maximum day for Given Month
		maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(year, month, maxDay);
		String endDateStr = formatter.format(calendar.getTime());
		// System.out.println("Last Day: " +
		// formatter.format(calendar.getTime()));

		dateLists.add(0, startDateStr);
		dateLists.add(1, endDateStr);
		return dateLists;
	}

	public static List<String> getFirstAndLastDateOfYear(int year) {
		List<String> dateLists = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		int date = 1;
		int maxDay = 0;
		calendar.set(year, 0, date);
		String startDateStr = formatter.format(calendar.getTime());

		// System.out.println("First Day: " +
		// formatter.format(calendar.getTime()));

		// Getting Maximum day for Given Month
		maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(year, 11, maxDay);
		String endDateStr = formatter.format(calendar.getTime());
		// System.out.println("Last Day: " +
		// formatter.format(calendar.getTime()));

		dateLists.add(0, startDateStr);
		dateLists.add(1, endDateStr);
		return dateLists;
	}

	public static List<Timestamp> getFirstAndLastTimestamp(int year, int month) {

		List<Timestamp> dateLists = new ArrayList<Timestamp>();
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dformatter = new SimpleDateFormat("yyyy-MM-dd");

		calendar.set(year, month, 1);
		String startDateStr = dformatter.format(calendar.getTime());
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(year, month, maxDay);
		String endDateStr = dformatter.format(calendar.getTime());

		java.sql.Timestamp startDate;
		//startDate = UtilDateTime.toTimestamp(java.sql.Date.valueOf(startDateStr));
		startDate = getTimestampFromString(startDateStr);
		java.sql.Timestamp endDate;
		endDate = getTimestampFromString(endDateStr);
		//endDate = UtilDateTime.toTimestamp(java.sql.Date.valueOf(endDateStr));

		dateLists.add(0, startDate);
		dateLists.add(1, endDate);
		return dateLists;

	}
	public static List<Timestamp> getFirstAndLastTimestamp(int year) {

		List<Timestamp> dateLists = new ArrayList<Timestamp>();
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dformatter = new SimpleDateFormat("yyyy-MM-dd");

		calendar.set(year, 0, 1);
		String startDateStr = dformatter.format(calendar.getTime());
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(year, 11, maxDay);
		String endDateStr = dformatter.format(calendar.getTime());

		java.sql.Timestamp startDate;
		//startDate = UtilDateTime.toTimestamp(java.sql.Date.valueOf(startDateStr));
		startDate = getTimestampFromString(startDateStr);
		java.sql.Timestamp endDate;
		endDate = getTimestampFromString(endDateStr);
		//endDate = UtilDateTime.toTimestamp(java.sql.Date.valueOf(endDateStr));

		dateLists.add(0, startDate);
		dateLists.add(1, endDate);
		return dateLists;

	}

	public static String getMonthName(int month, int year) {

		String monthName = null;
		SimpleDateFormat formatter = new SimpleDateFormat("MMMMM");

		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, 1);
		monthName = formatter.format(calendar.getTime());

		return monthName;

	}

	public static Long getYearLong(Timestamp date) {
		Long year = null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		year = (long) cal.get(Calendar.YEAR);
		return year;
	}

	public static Long getMonthInt(Timestamp date) {
		Long monthLong = null;
		SimpleDateFormat formatter = new SimpleDateFormat("MM");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(date.getTime());
		monthLong = Long.parseLong(formatter.format(calendar.getTime()))
				- new Long(1);
		return monthLong;
	}

	public static Timestamp getTimestampFromString(String str_date,
			DateFormat formatter) {
		if (isNotEmpty(str_date)) {
			try {
				//31/03/2017 00:00 
				// you can change format of date
				Date date = formatter.parse(str_date.trim());
				java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

				return timeStampDate;
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
		}else{
			return null;
		}
		
	}

	public static Timestamp getTimestampFromString(String str_date) {
		//01-Jun-2008
		if (isNotEmpty(str_date)) {
			try {
				SimpleDateFormat formatter = null;
				if (str_date.contains("/")) {
					formatter = new SimpleDateFormat("dd/MM/yyyy");
				} else {
					formatter = new SimpleDateFormat("yyyy-MM-dd");
				}

				Date date = formatter.parse(str_date.trim());
				java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

				return timeStampDate;
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
		}else{
			return null;
		}
		
	}

	public static String getStringFromTimestamp(Timestamp timeStampDate,
			DateFormat formatter) {
		// you can change format of date
		String str_date = "";
		try {
			if (isNotEmpty(timeStampDate)) {
				Date date = new Date(timeStampDate.getTime());
				str_date = formatter.format(date);
			}			
		} catch (Exception e) {
		}
		

		return str_date;
	}

	/**
	 * @author arifur
	 * @created_date_time Dec 7, 2015 => 4:03:28 PM
	 * @param month
	 * @param year
	 * @return
	 */
	public static Long getTotalDaysInMonth(int month, int year) {
		month = month + 1;
		String str = " " + month + "-" + year;
		SimpleDateFormat formatter = new SimpleDateFormat("MM-yyyy");
		Long totalDays = null;
		try {
			Date date = formatter.parse(str);
			// System.out.println(date);
			Calendar calDate = Calendar.getInstance();
			calDate.setTime(date);
			int totalDaysInt = calDate.getActualMaximum(Calendar.DAY_OF_MONTH);
			totalDays = new Long(totalDaysInt);
			// System.out.println(totalDays);

		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return totalDays;

	}

	public static Long getTotalDaysOfDateRange(Timestamp startDate,
			Timestamp endDate) {
		Long totalDays = new Long(0);
		while (!startDate.after(endDate)) {
			startDate = getDateAfterAdditionOfDays(startDate, new Long(1));
			totalDays++;
		}
		return totalDays;
	}

	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

	public static Date convertDateOneFormatToAnother(String dateinString) {
		final String OLD_FORMAT = "dd/MM/yyyy";
		final String NEW_FORMAT = "yyyy-MM-dd";

		String newDateString;

		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date dateOld = null;
		try {
			dateOld = sdf.parse(dateinString);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(dateOld);
		Date shipmentDate = java.sql.Date.valueOf(newDateString.toString());
		return shipmentDate;
	}

	public static Set<Integer> getMonthListOfDateRange(Timestamp startDate,
			Timestamp endDate) {
		Set<Integer> months = new HashSet<Integer>();
		while (!startDate.after(endDate)) {
			String month = getStringFromTimestamp(startDate,
					new SimpleDateFormat("MM"));
			if (!months.contains(month)) {
				months.add(Integer.parseInt(month) - 1);
			}
			startDate = getDateAfterAdditionOfDays(startDate, new Long(1));
		}
		return months;
	}

	public static Long getCurrentMonth() {
		Integer month = null;
		GregorianCalendar today = new GregorianCalendar();
		month = today.get(Calendar.MONTH);
		return Long.valueOf(month);
	}

	public static Long getCurrentYear() {
		Integer year = null;
		GregorianCalendar today = new GregorianCalendar();
		year = today.get(Calendar.YEAR);
		return Long.valueOf(year);
	}

	public static java.sql.Date getTodayDate() {
		Date today = new Date();
		Calendar todayCalDate = Calendar.getInstance();

		todayCalDate.set(Calendar.HOUR, 0);
		todayCalDate.set(Calendar.MINUTE, 0);
		todayCalDate.set(Calendar.SECOND, 0);
		todayCalDate.set(Calendar.AM_PM, 0);
		todayCalDate.set(Calendar.MILLISECOND, 0);

		today.setTime(todayCalDate.getTimeInMillis());
		java.sql.Date sqlDate = new java.sql.Date(today.getTime());

		return sqlDate;
	}
	/*
	public static Object[] getArrayFromJsonString(String str){
		Object[] output = null;
		if (isNotEmpty(str)) {
			JSONArray jsonArray = (JSONArray) JSONSerializer.toJSON( str ); 
			JsonConfig jsonConfig = new JsonConfig();  
			jsonConfig.setArrayMode( JsonConfig.MODE_OBJECT_ARRAY );  
			output = (Object[]) JSONSerializer.toJava( jsonArray, jsonConfig ); 			
		}
		return output;
	}
	public static String getMonthPeriodString(Map<String, Object> map){

		String monthPeriodUI = (String) map.get("monthPeriod");
		if (isNotEmpty(monthPeriodUI) && monthPeriodUI.trim().contains(" ")) {
			String month = org.apache.commons.lang.StringUtils.substringBefore(monthPeriodUI , " ");
			String year = org.apache.commons.lang.StringUtils.substringAfter(monthPeriodUI , " ");
			String monthNo = getMonthNumber(month);
			monthPeriodUI = year.concat(monthNo);
		}
		return monthPeriodUI;
	}

	public static String getMonthPeriodString(String monthPeriodUI){

		if (isNotEmpty(monthPeriodUI)) {
			String month = org.apache.commons.lang.StringUtils.substringBefore(monthPeriodUI , " ");
			String year = org.apache.commons.lang.StringUtils.substringAfter(monthPeriodUI , " ");
			String monthNo = getMonthNumber(month);
			monthPeriodUI = year.concat(monthNo);

		}
		return monthPeriodUI;
	}*/


	public static String getMonthNumber(String monthName) {

		String monthNumber = "";
		if (monthName.equals("January")) monthNumber = "01";
		if (monthName.equals("February")) monthNumber = "02";
		if (monthName.equals("March")) monthNumber = "03";
		if (monthName.equals("April")) monthNumber = "04";
		if (monthName.equals("May")) monthNumber = "05";
		if (monthName.equals("June")) monthNumber = "06";
		if (monthName.equals("July")) monthNumber = "07";
		if (monthName.equals("August")) monthNumber = "08";
		if (monthName.equals("September")) monthNumber = "09";
		if (monthName.equals("October")) monthNumber = "10";
		if (monthName.equals("November")) monthNumber = "11";
		if (monthName.equals("December")) monthNumber = "12";
		return monthNumber;        
	}

	public static String getMonthName(String monthNumber) {

		String monthName = "";
		if (monthNumber.equals("0")) monthName = "January";
		if (monthNumber.equals("1")) monthName = "February";
		if (monthNumber.equals("2")) monthName = "March";
		if (monthNumber.equals("3")) monthName = "April";
		if (monthNumber.equals("4")) monthName = "May";
		if (monthNumber.equals("5")) monthName = "June";
		if (monthNumber.equals("6")) monthName = "July";
		if (monthNumber.equals("7")) monthName = "August";
		if (monthNumber.equals("8")) monthName = "September";
		if (monthNumber.equals("9")) monthName = "October";
		if (monthNumber.equals("10")) monthName = "November";
		if (monthNumber.equals("11")) monthName = "December";
		return monthName;        
	}	
	
}
