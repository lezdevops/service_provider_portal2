package app.framework.businesstier;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

public class CommonFileUtils {

	public static String[] getFolderNames(File curDir) {
		String[] folderNames = new String[50];
		int i = 0;
		File[] filesList = curDir.listFiles();
		for (File file : filesList) {
			if (file.isDirectory())
				folderNames[i++] = file.getName();
		}
		String[] newFolderNames = new String[i];
		i = 0;
		for (String folderName : folderNames) {
			if (folderName != null) {
				newFolderNames[i++] = folderName;
			}
		}
		return newFolderNames;
	}

	public static String[] getFileNames(File curDir) {
		String[] fileNames = new String[50];
		int i = 0;
		File[] filesList = curDir.listFiles();
		for (File file : filesList) {
			if (file.isFile())
				fileNames[i++] = file.getName();
		}
		String[] newFileNames = new String[i];
		i = 0;
		for (String fileName : fileNames) {
			if (fileName != null) {
				newFileNames[i++] = fileName;
			}
		}
		return newFileNames;
	}

	static String getJavaType(String type) {
		String javaType = "";
		if (type.equals("name") || type.equals("description")
				|| type.equals("id-ne") || type.equals("indicator")
				|| type.equals("very-long") || type.equals("id-vlong")
				|| type.equals("description") || type.equals("comment")
				|| type.equals("value")) {
			javaType = "String";
		} else if (type.equals("date-time")) {
			javaType = "Timestamp";
		} else if (type.equals("date")) {
			javaType = "Date";
		} else if (type.equals("fixed-point")) {
			javaType = "BigDecimal";
		} else if (type.equals("numeric")) {
			javaType = "Long";
		} else {
			javaType = type;
		}

		return javaType;
	}

	public static String getExtension(String stringValue) {
		String reverseValue = reverse(stringValue);
		String fileName = fileNameWithoutExtension(reverseValue);
		return reverse(fileName);
	}

	public static String reverse(String stringValue) {
		String reverseValue = "";
		for (int i = stringValue.length() - 1; i >= 0; i--) {
			char c = stringValue.charAt(i);
			reverseValue = reverseValue + c;
		}
		return reverseValue;
	}

	public static String fileNameWithoutExtension(String stringValue) {
		String fileName = "";
		for (int i = 0; i <= stringValue.length(); i++) {
			char c = stringValue.charAt(i);
			if (c == '.') {
				break;
			}
			fileName = fileName + c;
		}
		return fileName;
	}

	public static void test() {
		/*
		 * File directory = new
		 * File("generated-files/src/main/java/app/commonerp"); String[]
		 * folderNames = getFolderNames(directory); for (int i = 0; i <
		 * folderNames.length; i++) { System.out.println(i + ". " +
		 * folderNames[i]); }
		 * 
		 * directory = new File(
		 * "generated-files/src/main/java/app/commonerp/settings/presentationtier"
		 * ); String[] fileNames = getFileNames(directory); for (int i = 0; i <
		 * fileNames.length; i++) { System.out.println(i + ". " + fileNames[i]);
		 * }
		 */
		// applicationOptions();
	}
	/*
	 * public static File send3TierFilesToDefault(File fromFile ,
	 * Map<String,Object> fileParams, String fileName,Boolean isFtl){ File
	 * preparedFile = null; System.out.println(fileParams); String module =
	 * (String)fileParams.get("moduleName"); String menuController =
	 * (String)fileParams.get("menuController"); String entityName =
	 * (String)fileParams.get("entityName"); String entityNameId =
	 * (String)fileParams.get("entityNameId");
	 * 
	 * String field1 = (String)fileParams.get("field1"); String field2 =
	 * (String)fileParams.get("field2"); String field3 =
	 * (String)fileParams.get("field3"); String field4 =
	 * (String)fileParams.get("field4"); String field5 =
	 * (String)fileParams.get("field5"); //String dateField =
	 * (String)fileParams.get("dateField");
	 * 
	 * //String fromAddress = fromFile.getAbsolutePath();
	 * 
	 * if (fromFile!=null) { String preparedFilesString; try {
	 * preparedFilesString = FileUtils.readFileToString(fromFile); if
	 * (differentFieldFile!=null) { differentTypeFieldsStr =
	 * FileUtils.readFileToString(differentFieldFile); }
	 * 
	 * Long size = fromFile.length(); Double size_double = size.doubleValue();
	 * size_double = size_double / 1024; size_double = size_double / 1024;
	 * //size = size/1000; //System.out.println(size_double);
	 * 
	 * String fileFullName = fromFile.getName(); String extension =
	 * CRUD_Utils.getExtension(fileFullName); System.out.println(extension); if
	 * (isFtl) { File dir = new File("hot-deploy/" + module + "/webapp/" +
	 * module + "/" + CaseConverter.toCamelCase(menuController)); dir.mkdir();
	 * preparedFile = new File("hot-deploy/" + module + "/webapp/" + module +
	 * "/" + CaseConverter.toCamelCase(menuController) + "/" + fileName +"." +
	 * extension);
	 * 
	 * Map<String,Object> entityDetails = new LinkedHashMap<String, Object>();
	 * 
	 * entityDetails.put("entityName", entityName);
	 * entityDetails.put("primaryKeys", primaryKeys);
	 * entityDetails.put("fieldNames", fieldNames);
	 * entityDetails.put("fieldTypes", fieldTypes);
	 * 
	 * entityDetails.put("relationalFields", relationalFields);
	 * entityDetails.put("relationalFieldNames", relationalFieldNames);
	 * entityDetails.put("relationalEntityNames", relationalEntityNames);
	 * 
	 * preparedFilesString =
	 * getFieldsAppliedToFtlFile(entityDetails,preparedFilesString); } else if
	 * (extension.equalsIgnoreCase("jrxml") && fileName.contains("Report")) {
	 * File dir = new File("hot-deploy/" + module + "/webapp/" + module + "/" +
	 * CaseConverter.toCamelCase(menuController) + "/reports"); dir.mkdir();
	 * preparedFile = new File("hot-deploy/" + module + "/webapp/" + module +
	 * "/reports/" + fileName +"." + extension);
	 * 
	 * } else if (fileName.contains("Forms")) { preparedFile = new
	 * File("hot-deploy/" + module + "/widget/"
	 * +CaseConverter.toCamelCase(menuController)+"/"+ fileName +"."+
	 * extension);
	 * 
	 * Map<String,Object> entityDetails = new LinkedHashMap<String, Object>();
	 * 
	 * entityDetails.put("entityName", entityName);
	 * entityDetails.put("primaryKeys", primaryKeys);
	 * entityDetails.put("fieldNames", fieldNames);
	 * entityDetails.put("fieldTypes", fieldTypes);
	 * 
	 * entityDetails.put("relationalFields", relationalFields);
	 * entityDetails.put("relationalFieldNames", relationalFieldNames);
	 * entityDetails.put("relationalEntityNames", relationalEntityNames);
	 * preparedFilesString =
	 * getFieldsAppliedToXMLFormFile(entityDetails,preparedFilesString);
	 * 
	 * }else if (fileName.contains("Screens") || fileName.contains("Menus")) {
	 * preparedFile = new File("hot-deploy/" + module + "/widget/"
	 * +CaseConverter.toCamelCase(menuController)+"/"+ fileName +"."+
	 * extension);
	 * 
	 * }else if (fileName.contains("-controller")) { preparedFile = new
	 * File("hot-deploy/" + module + "/webapp/" + module + "/WEB-INF/" +
	 * fileName +"." + extension); try { String fileString =
	 * FileUtils.readFileToString(new
	 * File("hot-deploy/"+module+"/webapp/"+module+"/WEB-INF/controller.xml"));
	 * if (!fileString.contains(CaseConverter.toCamelCase(menuController)
	 * +"-controller.xml\"")) { fileString =
	 * fileString.replaceAll("common-controller.xml\"/>"
	 * ,"common-controller.xml\"/>\n<include location=\"component://"+
	 * CaseConverter.toLowerCase(module) +"/webapp/"+
	 * CaseConverter.toLowerCase(module) +"/WEB-INF/"+
	 * CaseConverter.toCamelCase(menuController) +"-controller.xml\" />");
	 * FileUtils.writeStringToFile(new
	 * File("hot-deploy/"+module+"/webapp/"+module
	 * +"/WEB-INF/controller.xml"),fileString); }
	 * 
	 * fileString = FileUtils.readFileToString(new
	 * File("hot-deploy/"+module+"/widget/"
	 * +CaseConverter.toPascalCase(module)+"Menus.xml")); if
	 * (!fileString.contains(CaseConverter.toPascalCase(menuController))) {
	 * fileString =
	 * fileString.replaceAll("</menu>","<menu-item name=\""+CaseConverter
	 * .toPascalCase
	 * (menuController)+"\" title=\""+CaseConverter.toSentence(menuController
	 * )+"\"><link target=\""
	 * +CaseConverter.toPascalCase(menuController)+"\"/></menu-item>\n</menu>");
	 * FileUtils.writeStringToFile(new
	 * File("hot-deploy/"+module+"/widget/"+CaseConverter
	 * .toPascalCase(module)+"Menus.xml"),fileString); } } catch (IOException e)
	 * { e.printStackTrace(); }//"servicedef/services.xml"/> }else if
	 * (fileName.contains("-services")) { preparedFile = new File("hot-deploy/"
	 * + module + "/servicedef/"+ fileName +"." + extension); try { File
	 * commonServiceFile = new
	 * File("hot-deploy/"+module+"/ofbiz-component.xml"); String fileString =
	 * FileUtils.readFileToString(commonServiceFile); if
	 * (!fileString.contains(CaseConverter.toCamelCase(menuController)))
	 * {//"servicedef/services.xml"/> fileString =
	 * fileString.replaceAll("servicedef/services.xml\"/>",
	 * "servicedef/services.xml\"/>\n<service-resource type=\"model\" loader=\"main\" location=\"servicedef/"
	 * + CaseConverter.toCamelCase(menuController) +"-services.xml\"/>");
	 * 
	 * FileUtils.writeStringToFile(commonServiceFile,fileString); } } catch
	 * (Exception e) {
	 * System.out.println("Exception to modify ofbiz-component.xml"); } } else
	 * if (fileName.contains("List")) { preparedFile = new File("hot-deploy/" +
	 * module + "/webapp/" + module + "/WEB-INF/actions/" + fileName +"." +
	 * extension ); } else if (fileName.contains("Actions")) { preparedFile =
	 * new File("hot-deploy/" + module + "/webapp/" + module + "/images/js/" +
	 * fileName +"." + extension); } else if (extension.equalsIgnoreCase("java")
	 * &&fileName.contains("Service")) { preparedFile = new File("hot-deploy/" +
	 * module + "/src/"+product+"/" + module + "/persistencetier/" + fileName
	 * +"." + extension); }else if (extension.equalsIgnoreCase("java")
	 * &&(fileName.contains("Manager") || fileName.contains("Validator"))) {
	 * preparedFile = new File("hot-deploy/" + module + "/src/"+product+"/" +
	 * module + "/businesstier/" + fileName +"." + extension);
	 * Map<String,Object> entityDetails = new LinkedHashMap<String, Object>();
	 * 
	 * entityDetails.put("entityName", entityName);
	 * entityDetails.put("primaryKeys", primaryKeys);
	 * entityDetails.put("fieldNames", fieldNames);
	 * entityDetails.put("fieldTypes", fieldTypes);
	 * 
	 * entityDetails.put("relationalFields", relationalFields);
	 * entityDetails.put("relationalFieldNames", relationalFieldNames);
	 * entityDetails.put("relationalEntityNames", relationalEntityNames);
	 * 
	 * preparedFilesString =
	 * getFieldsAppliedToJavaFile(entityDetails,preparedFilesString); }else if
	 * (extension.equalsIgnoreCase("java") &&(fileName.contains("Tests") ||
	 * fileName.contains("Report"))) { preparedFile = new File("hot-deploy/" +
	 * module + "/src/"+product+"/" + module + "/presentationtier/" + fileName
	 * +"." + extension); } else { System.out.println("The file is not valid");
	 * }
	 * 
	 * preparedFilesString =
	 * preparedFilesString.replaceAll("MenuController",CaseConverter
	 * .toPascalCase(menuController)); preparedFilesString =
	 * preparedFilesString.
	 * replaceAll("menuController",CaseConverter.toCamelCase(menuController));
	 * preparedFilesString =
	 * preparedFilesString.replaceAll("product",CaseConverter
	 * .toLowerCase(product)); preparedFilesString =
	 * preparedFilesString.replaceAll
	 * ("Module",CaseConverter.toPascalCase(module)); preparedFilesString =
	 * preparedFilesString
	 * .replaceAll("MODULE",CaseConverter.toUpperCase(module));
	 * preparedFilesString =
	 * preparedFilesString.replaceAll("module",CaseConverter
	 * .toLowerCase(module));
	 * 
	 * preparedFilesString =
	 * preparedFilesString.replaceAll("EntityName",CaseConverter
	 * .toPascalCase(entityName)); preparedFilesString =
	 * preparedFilesString.replaceAll
	 * ("entityObject",CaseConverter.toCamelCase(entityName));
	 * preparedFilesString =
	 * preparedFilesString.replaceAll("entityNameId",CaseConverter
	 * .toCamelCase(entityNameId));
	 * 
	 * preparedFilesString =
	 * preparedFilesString.replaceAll("field1",CaseConverter
	 * .toCamelCase(field1)); preparedFilesString =
	 * preparedFilesString.replaceAll
	 * ("field2",CaseConverter.toCamelCase(field2)); preparedFilesString =
	 * preparedFilesString
	 * .replaceAll("field3",CaseConverter.toCamelCase(field3));
	 * preparedFilesString =
	 * preparedFilesString.replaceAll("field4",CaseConverter
	 * .toCamelCase(field4)); preparedFilesString =
	 * preparedFilesString.replaceAll
	 * ("field5",CaseConverter.toCamelCase(field5)); //preparedFilesString =
	 * preparedFilesString
	 * .replaceAll("dateField",CaseConverter.toCamelCase(dateField));
	 * 
	 * FileUtils.writeStringToFile(preparedFile,preparedFilesString);
	 * FileUtils.writeStringToFile(differentFieldFile,differentTypeFieldsStr); }
	 * catch (IOException e1) { e1.printStackTrace(); } }
	 * 
	 * return preparedFile; }
	 */
}
