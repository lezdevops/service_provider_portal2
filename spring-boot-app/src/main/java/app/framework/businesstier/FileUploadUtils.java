package app.framework.businesstier;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.json.simple.JSONArray;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.StringUtils;

public class FileUploadUtils {
	/**
	 * 
	 * @param filename
	 * @return
	 */
	private final static Path rootLocation = Paths.get("upload-dir");
	public static void uploadExcellSheet(HttpServletRequest request,
			HttpServletResponse response) {
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);

		List fileItemList = new ArrayList();
		/*
		 * try { upload.parseRequest(request); } catch (FileUploadException e2)
		 * { e2.printStackTrace(); }
		 */

		String targetFileFieldName = "employeeLoanSheet";

		LinkedHashMap<String, Object> dataFormatMap = new LinkedHashMap<String, Object>();

		dataFormatMap.put("employeeCode", "String");
		dataFormatMap.put("loanProfileId", "String");
		dataFormatMap.put("loanApplyDate", "Timestamp");
		dataFormatMap.put("appliedLoanAmount", "BigDecimal");
		dataFormatMap.put("principleLoanAmount", "BigDecimal");
		dataFormatMap.put("loanStartDate", "Timestamp");
		dataFormatMap.put("totalInstallment", "BigDecimal");
		dataFormatMap.put("loanStatus", "String");
		dataFormatMap.put("purpose", "String");

		int totalSheets = 0;
		Integer specificSheetLocation = 0;
		List<Map<String, Object>> spreadSheetObjectMappedRowList = getSpreadSheetObjectMappedRowList(
				request, targetFileFieldName, fileItemList, dataFormatMap,
				totalSheets, specificSheetLocation);

		String value = JSONArray.toJSONString(spreadSheetObjectMappedRowList);
		request.setAttribute("employeeLoanInfoJson", value);
	}

	public static String filterDataAndFormatToString(Object objDataForFilter) {
		String cellDataStr = "";
		Object objCellData = objDataForFilter;

		if (objCellData != null) {
			if (objCellData instanceof BigDecimal) {
				String dataDouble = ((BigDecimal) objCellData).toString();
				cellDataStr = dataDouble;
			} else if (objCellData instanceof Double) {
				String dataDouble = ((Double) objCellData).toString();
				cellDataStr = dataDouble;
			} else if (objCellData instanceof Integer) {
				String dataInteger = ((Integer) objCellData).toString();
				cellDataStr = dataInteger;
			} else if (objCellData instanceof BigInteger) {
				String dataInteger = ((BigInteger) objCellData).toString();
				cellDataStr = dataInteger;
			} else {
				cellDataStr = objCellData.toString();
			}
		} /*
		 * else cellDataStr = null;
		 */

		return cellDataStr;
	}

	public static List<List<String>> getSpreadSheetRowList(
			HttpServletRequest request, String targetFileFieldName,
			List<FileItem> fileItemList, int totalColumns,
			Integer specificSheetLocation) {
		List<List<String>> spreadSheetRowList = new ArrayList();

		try {
			FileItem file_item = null;
			String fieldName = "";
			boolean targetFileFound = false;
			for (int i = 0; i < fileItemList.size(); i++) {
				Map<String, Object> map = new HashMap();
				file_item = (FileItem) fileItemList.get(i);

				if (file_item.isFormField()) {
					String name = file_item.getFieldName();
					String value = file_item.getString();
					map.put(name, value);
				}
				fieldName = file_item.getFieldName();
				if (fieldName.equals(targetFileFieldName)) {
					targetFileFound = true;
					break;
				}
			}

			// employeeProfileSheet
			if (targetFileFound) {

				FileItem item = (FileItem) file_item;

				String fileName = item.getName();
				String root = request.getSession().getServletContext()
						.getRealPath("/");
				File path = new File(root + "/uploads");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}

				File uploadedFile = new File(path + "/" + fileName);
				item.write(uploadedFile);
				String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
				int count = 0;
				if (extension.equalsIgnoreCase("xlsx")) {
					spreadSheetRowList = getDataFromExcelFile2007(uploadedFile,
							totalColumns, specificSheetLocation);
					// sheetData.remove(0);
				}
				if (extension.equalsIgnoreCase("xls")) {
					spreadSheetRowList = getDataFromExcelFile2003(uploadedFile,
							totalColumns, specificSheetLocation);
					// sheetData.remove(0);
				}
			}

		}

		catch (NullPointerException e1) {
			e1.printStackTrace();
			request.setAttribute(
					"uploadMessage",
					"Error To Upload Employee Data. Caused By: "
							+ e1.getMessage());
			return spreadSheetRowList;
		} catch (Exception e) {
			request.setAttribute(
					"uploadMessage",
					"Error To Upload Employee Data. Caused By: "
							+ e.getMessage());
			return spreadSheetRowList;
		}
		TOTAL_ROWS_TO_PROCESS = spreadSheetRowList.size();
		return spreadSheetRowList;
	}

	public static List<Map<String, Object>> getSpreadSheetObjectMappedRowList(
			HttpServletRequest request, String targetFileFieldName,
			List<FileItem> fileItemList,
			LinkedHashMap<String, Object> dataFormatMap, int totalSheets,
			Integer specificSheetLocation) {
		List<Map<String, Object>> spreadSheetObjectMappedRowList = new ArrayList<Map<String, Object>>();
		List<List<String>> spreadSheetRowList = new ArrayList<List<String>>();
		DateFormat sheetDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

		try {
			Iterator iterator = fileItemList.iterator();
			FileItem file_item = null;
			// selected_file_item = null;
			String fieldName = "";
			boolean targetFileFound = false;
			for (int i = 0; i < fileItemList.size(); i++) {
				Map<String, Object> map = new HashMap();
				file_item = (FileItem) fileItemList.get(i);

				if (file_item.isFormField()) {
					String name = file_item.getFieldName();
					String value = file_item.getString();
					map.put(name, value);
				}
				fieldName = file_item.getFieldName();
				if (fieldName.equals(targetFileFieldName)) {
					targetFileFound = true;
					break;
				}
			}

			// employeeProfileSheet
			if (targetFileFound) {

				FileItem item = (FileItem) file_item;

				String fileName = item.getName();
				String root = request.getSession().getServletContext()
						.getRealPath("/");
				File path = new File(root + "/uploads");
				if (!path.exists()) {
					boolean status = path.mkdirs();
				}

				File uploadedFile = new File(path + "/" + fileName);
				item.write(uploadedFile);
				String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
				int count = 0;
				if (extension.equalsIgnoreCase("xlsx")) {
					spreadSheetRowList = getDataFromExcelFile2007(uploadedFile,
							dataFormatMap.size(), specificSheetLocation);
					spreadSheetRowList.remove(0);
				}
				if (extension.equalsIgnoreCase("xls")) {
					spreadSheetRowList = getDataFromExcelFile2003(uploadedFile,
							dataFormatMap.size(), specificSheetLocation);
					spreadSheetRowList.remove(0);
				}
				for (List<String> spreadSheetRowValues : spreadSheetRowList) {
					int i = 0;
					Set<String> dataFormatMapKeys = dataFormatMap.keySet();
					if (dataFormatMapKeys.size() == spreadSheetRowValues.size()) {
						List<String> dataFormatMapKeysList = new ArrayList<String>();
						for (String dataFormatMapKey : dataFormatMapKeys) {
							dataFormatMapKeysList.add(dataFormatMapKey);
						}
						Map<String, Object> fieldMap = new HashMap();

						for (String spreadSheetColumnString : spreadSheetRowValues) {
							String objectFieldName = dataFormatMapKeysList
									.get(i++);
							String objectFieldType = (String) dataFormatMap
									.get(objectFieldName);
							Object object = getConvertedData(
									spreadSheetColumnString, objectFieldType,
									sheetDateFormat);

							fieldMap.put(objectFieldName, object);
						}
						spreadSheetObjectMappedRowList.add(fieldMap);
					}
				}
			}

		}

		catch (NullPointerException e1) {
			e1.printStackTrace();
			request.setAttribute("uploadMessage",
					"Error To Upload Spreasheet. Caused By: " + e1.getMessage());
			return spreadSheetObjectMappedRowList;
		} catch (Exception e) {
			request.setAttribute("uploadMessage",
					"Error To Upload Spreasheet. Caused By: " + e.getMessage());
			return spreadSheetObjectMappedRowList;
		}
		TOTAL_ROWS_TO_PROCESS = spreadSheetObjectMappedRowList.size();
		return spreadSheetObjectMappedRowList;
	}

	private static Object getConvertedData(String spreadSheetColumnString,
			String objectFieldType, DateFormat dateFormat) {
		Object object = null;
		try {
			if (UtilValidate.isNotEmpty(spreadSheetColumnString)) {
				if (objectFieldType.equalsIgnoreCase("BigDecimal")) {
					object = new BigDecimal(spreadSheetColumnString);
				} else if (objectFieldType.equalsIgnoreCase("Long")) {
					spreadSheetColumnString = StringUtils.substringBefore(
							spreadSheetColumnString, ".");
					object = new Long(spreadSheetColumnString);
				} else if (objectFieldType.equalsIgnoreCase("Timestamp")) {
					object = HelperUtils.getTimestampFromString(
							spreadSheetColumnString, dateFormat);
				} else if (objectFieldType.equalsIgnoreCase("String")) {
					object = new String(spreadSheetColumnString);
				} else {
					object = new String("");
				}
			} else {
				object = new String("");
			}

		} catch (Exception e) {
			e.printStackTrace();
			object = new String(spreadSheetColumnString);
		}

		return object;
	}

	public static List<List<String>> getDataFromExcelFile2003(
			File uploadedFile, int totalColumns, Integer specificSheetLocation) {
		List<List<String>> sheetData = new ArrayList<List<String>>();
		FileInputStream fis = null;
		try {
			/* Create a FileInputStream that will be use to read the excel file. */
			fis = new FileInputStream(uploadedFile);
			/* Create an excel workbook from the file system. */
			HSSFWorkbook workbook = new HSSFWorkbook(fis);
			/* Get the first sheet on the workbook. */
			if (UtilValidate.isNotEmpty(specificSheetLocation)) {
				HSSFSheet sheet = workbook.getSheetAt(specificSheetLocation);
				sheetData.addAll(getExcel2003SheetDataRowList(sheet,
						totalColumns));
			} else {
				int totalSheets = workbook.getNumberOfSheets();

				for (int i = 0; i < totalSheets; i++) {
					HSSFSheet sheet = workbook.getSheetAt(i);
					List<List<String>> fetchSheetRowList = getExcel2003SheetDataRowList(
							sheet, totalColumns);

					if (UtilValidate.isNotEmpty(fetchSheetRowList)) {
						fetchSheetRowList.remove(0);
						sheetData.addAll(fetchSheetRowList);
					}
				}
			}

			delAllFiles(uploadedFile);
		} catch (IOException e) {
			delAllFiles(uploadedFile);
			e.printStackTrace();
		} finally {
			delAllFiles(uploadedFile);
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sheetData;

	}

	public static List<List<String>> getExcel2003SheetDataRowList(
			HSSFSheet sheet, int totalColumns) {
		List<List<String>> sheetData = new ArrayList<List<String>>();

		Iterator rows = sheet.rowIterator();
		while (rows.hasNext()) {
			HSSFRow row = (HSSFRow) rows.next();
			Iterator<Cell> cells = row.cellIterator();

			List<String> rowFields = new ArrayList();
			for (int j = 0; j < totalColumns; j++) {
				HSSFCell cell = row.getCell(j);
				String cellDataStr = filterDataAndFormatToString(cell);
				rowFields.add(cellDataStr);
			}

			/*
			 * while (cells.hasNext()) { HSSFCell cell = (HSSFCell)
			 * cells.next(); String cellDataStr =
			 * filterDataAndFormatToString(cell); rowFields.add(cellDataStr); }
			 */
			boolean emptyRow = true;
			for (String rowField : rowFields) {
				if (UtilValidate.isNotEmpty(rowField.trim())) {
					emptyRow = false;
					break;
				}
			}
			if (!emptyRow) {
				sheetData.add(rowFields);
			}

		}
		return sheetData;
	}

	/**
	 * 
	 * @param uploadedFile
	 * @return
	 */
	public static List<List<String>> getDataFromExcelFile2007(
			File uploadedFile, int totalColumns, Integer specificSheetLocation) {
		@SuppressWarnings({ "unchecked", "rawtypes" })
		List<List<String>> sheetData = new ArrayList();
		FileInputStream fis = null;
		try {
			/* Create a FileInputStream that will be use to read the excel file. */
			fis = new FileInputStream(uploadedFile);
			/* Create an excel workbook from the file system. */
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			if (UtilValidate.isNotEmpty(specificSheetLocation)) {
				XSSFSheet sheet = workbook.getSheetAt(specificSheetLocation);
				List<List<String>> fetchSheetRowList = getExcel2007SheetDataRowList(
						sheet, totalColumns);

				if (UtilValidate.isNotEmpty(fetchSheetRowList)) {
					fetchSheetRowList.remove(0);
					sheetData.addAll(fetchSheetRowList);
				}
			} else {
				int totalSheets = workbook.getNumberOfSheets();

				for (int i = 0; i < totalSheets; i++) {
					XSSFSheet sheet = workbook.getSheetAt(i);
					List<List<String>> fetchSheetRowList = getExcel2007SheetDataRowList(
							sheet, totalColumns);

					if (UtilValidate.isNotEmpty(fetchSheetRowList)) {
						fetchSheetRowList.remove(0);
						sheetData.addAll(fetchSheetRowList);
					}
				}
			}

			delAllFiles(uploadedFile);
		} catch (IOException e) {
			delAllFiles(uploadedFile);
			e.printStackTrace();
		} finally {
			delAllFiles(uploadedFile);
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sheetData;

	}

	public static List<List<String>> getExcel2007SheetDataRowList(
			XSSFSheet sheet, int totalColumns) {
		List<List<String>> sheetData = new ArrayList<List<String>>();
		@SuppressWarnings("rawtypes")
		Iterator rows = sheet.rowIterator();
		while (rows.hasNext()) {
			XSSFRow row = (XSSFRow) rows.next();
			Iterator cells = row.cellIterator();

			List<String> rowFields = new ArrayList<String>();
			for (int j = 0; j < totalColumns; j++) {
				XSSFCell cell = row.getCell(j);
				String cellDataStr = filterDataAndFormatToString(cell);
				rowFields.add(cellDataStr);
			}
			boolean emptyRow = true;
			for (String rowField : rowFields) {
				if (UtilValidate.isNotEmpty(rowField.trim())) {
					emptyRow = false;
					break;
				}
			}
			if (!emptyRow) {
				sheetData.add(rowFields);
			}
		}
		return sheetData;
	}

	/**
	 * 
	 * @param uploadedFile
	 */
	public static void delAllFiles(File uploadedFile) {
		String filePath = uploadedFile.getParent();
		String fileName = uploadedFile.getName();
		String fileExt = FilenameUtils.getExtension(fileName);
		new FileUploadUtils().deleteFile(filePath, "." + fileExt);
	}

	/**
	 * 
	 * @param folder
	 * @param ext
	 */
	public void deleteFile(String folder, String ext) {

		GenericExtFilter filter = new GenericExtFilter(ext);
		File dir = new File(folder);

		// list out all the file name with .txt extension
		String[] list = dir.list(filter);

		if (list.length == 0)
			return;

		File fileDelete;

		for (String file : list) {
			String temp = new StringBuffer(folder).append(File.separator)
					.append(file).toString();
			fileDelete = new File(temp);
			boolean isdeleted = fileDelete.delete();
			System.out.println("file : " + temp + " is deleted : " + isdeleted);
		}
	}

	class GenericExtFilter implements FilenameFilter {
		private String ext;

		public GenericExtFilter(String ext) {
			this.ext = ext;
		}

		public boolean accept(File dir, String name) {
			return (name.endsWith(ext));
		}
	}

	public static int TOTAL_ROWS_TO_PROCESS = 1;
	public static int TOTAL_PROCESSED_ROWS = 0;

	public static String getApplicationLoadingProgressInfo(
			HttpServletRequest request, HttpServletResponse response) {
		BigDecimal processPercentage = BigDecimal.ZERO;
		processPercentage = (new BigDecimal(new Long(TOTAL_PROCESSED_ROWS))
				.divide(new BigDecimal(new Long(TOTAL_ROWS_TO_PROCESS)), 2,
						RoundingMode.HALF_UP)).multiply(new BigDecimal("100"));
		request.setAttribute("processPercentageLong",
				processPercentage.longValue());
		request.setAttribute("processPercentage", processPercentage.toString()
				+ "%");
		return "success";
	}

	public static Map<String, Object> getMultiPartFormValues(
			HttpServletRequest request, String targetUploadedFileName) {
		Map<String, Object> multiPartFormValues = new LinkedHashMap<String, Object>();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (!isMultipart) {
			return null;
		}
		FileItem uploadedFileItem = null;
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List fileItemList = new ArrayList();
		/*
		 * try { fileItemList = upload.parseRequest(request); } catch
		 * (FileUploadException e2) { e2.printStackTrace(); }
		 */
		for (int i = 0; i < fileItemList.size(); i++) {
			FileItem fileItem = null;
			fileItem = (FileItem) fileItemList.get(i);

			if (fileItem.isFormField()) {
				String name = fileItem.getFieldName();
				String value = fileItem.getString();
				multiPartFormValues.put(name, value);
			}
			String fieldName = fileItem.getFieldName();
			if (fieldName.equals(targetUploadedFileName)) {
				uploadedFileItem = fileItem;
				multiPartFormValues.put(targetUploadedFileName,
						uploadedFileItem);
			}
		}
		return multiPartFormValues;
	}
	public void store(MultipartFile file) {
		try {
			Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
		} catch (Exception e) {
			throw new RuntimeException("FAIL!");
		}
	}

	public static Path store(String base64String, String filePath) {
		Path path = null;
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(base64String);
			//path = Files.write(Paths.get(filePath), decodedBytes, StandardOpenOption.CREATE);
			
			
			FileUtils.writeByteArrayToFile(new File(filePath), decodedBytes);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("FAIL!");
		}
		return path;
	}

	public static String getBase64StringFromFilePath(String fileName) throws IOException {
		String base64String = "";

	    File file = new File(fileName);
	    byte[] bytes = loadFile(file);
	    //byte[] encoded = Base64.encodeBase64();
	    byte[] encodedBytes = Base64.getEncoder().encode(bytes);
	    base64String = new String(encodedBytes,StandardCharsets.US_ASCII);

	    return base64String;
	}
	private static byte[] loadFile(File file) throws IOException {
	    InputStream is = new FileInputStream(file);

	    long length = file.length();
	    if (length > Integer.MAX_VALUE) {
	        // File is too large
	    }
	    byte[] bytes = new byte[(int)length];

	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length
	           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	        offset += numRead;
	    }

	    if (offset < bytes.length) {
	        throw new IOException("Could not completely read file "+file.getName());
	    }

	    is.close();
	    return bytes;
	}

	public static Resource loadFile(String filename) {
		try {
			Path file = rootLocation.resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("FAIL!");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("FAIL!");
		}
	}

	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	public void init() {
		try {
			Files.createDirectory(rootLocation);
		} catch (IOException e) {
			throw new RuntimeException("Could not initialize storage!");
		}
	}
	public static byte[] getByteArrayFromBase64String(String base64String) {
		String delims = "[,]";
		String[] parts = base64String.split(delims);
		String imageString = parts[1];
		System.out.println("imageString: ");
		System.out.println(imageString);
		
		byte[] byteArray = Base64.getDecoder().decode(imageString);

		//byteArray = org.apache.tomcat.util.codec.binary.Base64.decodeBase64(base64String.getBytes());
		
		return byteArray;
	}
	public static String getExtensionFromBase64String(String base64FileString) {
		String extension = "";
		// Decode the Base64 encoded string into byte array
		// tokenize the data since the 64 encoded data look like this
		// "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAoAAAAKAC"

		String delims = "[,]";
		//delims = "[;]";
		String[] parts = base64FileString.split(delims);
		
		String base64Value = parts[1];
		byte[] byteArray = Base64.getDecoder().decode(base64Value);
		
		//byteArray = org.apache.tomcat.util.codec.binary.Base64.decodeBase64(base64Value.getBytes());
		
		InputStream is = new ByteArrayInputStream(byteArray);
		System.out.println("-----------------------"+is);//data:application/pdf;base64
		
		// Find out image type
		String mimeType = null;
		try {
			mimeType = URLConnection.guessContentTypeFromStream(is); // mimeType is something like "image/jpeg"
			
			String delimiter = "[/]";
			String[] tokens = mimeType.split(delimiter);
			extension = tokens[1];
		} catch (IOException ioException) {

		}
		return extension;
	}
	public static Map<String,Object> getByteArrayAndExtensionFromBase64String(String base64FileString) {
		Map<String,Object> byteArrayAndExtension = new LinkedHashMap<String,Object>();
		String extension = "";
		// Decode the Base64 encoded string into byte array
		// tokenize the data since the 64 encoded data look like this
		// "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAoAAAAKAC"

		String delims = "[,]";
		//delims = "[;]";
		String[] parts = base64FileString.split(delims);
		
		String fileTypePart = parts[0];
		String base64Value = parts[1];
		byte[] byteArray = Base64.getDecoder().decode(base64Value);
		byteArrayAndExtension.put("byteArray", base64Value);

		System.out.println("####################################################");
		//System.out.println("base64Value  : "+base64Value);
		
		System.out.println("####################################################");
		//System.out.println("byteArray  : "+byteArray);
		
		//byteArray = org.apache.tomcat.util.codec.binary.Base64.decodeBase64(base64Value.getBytes());
		
		InputStream is = new ByteArrayInputStream(byteArray);
		System.out.println("-----------------------"+is);//data:application/pdf;base64
		
		// Find out image type
		String mimeType = null;
		try {
			mimeType = URLConnection.guessContentTypeFromStream(is); // mimeType is something like "image/jpeg"
			String[] tokens = null;
			String delimiter = "[/]";
			if (UtilValidate.isNotEmpty(mimeType)) {
				tokens = mimeType.split(delimiter);
				extension = tokens[1];
			}else {
				//data:image/png;base64
				String[] words = fileTypePart.split(delimiter);
				//data:image and png;base64
				tokens = words[1].split("[;]");
				//png and base64
				extension = tokens[0];
			}
			
			
			byteArrayAndExtension.put("extension", extension);
		} catch (IOException ioException) {

		}
		return byteArrayAndExtension;
	}
	public static String getFileExtension(String fileName) {
        String extension = ""; //uploads/serviceProvider/jkl.png
        try {
        	extension = fileName.substring(fileName.lastIndexOf("."));
        } catch (Exception e) {
            extension = "";
        } 
        return extension; 
    }
	//"data:image/png;base64,"
	public static String getBase64StringWithFileTypeString(String base64FileStr, String extension) {
		//System.out.println("Making Final Base64String. Extension: " +extension+" base64String: "+base64FileStr);
		String finalBase64String="";
		if (extension.equalsIgnoreCase(".gif") 
				|| extension.equalsIgnoreCase(".png") 
				|| extension.equalsIgnoreCase(".jpg")
				|| extension.equalsIgnoreCase(".jpeg")
				|| extension.equalsIgnoreCase(".tiff")) {
			finalBase64String = "data:image/png;base64," + base64FileStr;			
		}else if (extension.equalsIgnoreCase(".xls") || extension.equalsIgnoreCase(".xlsx")) {
			finalBase64String = "data:image/png;base64," + base64FileStr;	
		}else if (extension.equalsIgnoreCase(".pdf")) {
			finalBase64String = "data:application/pdf;base64," + base64FileStr;	
		}else if (extension.equalsIgnoreCase(".doc") || extension.equalsIgnoreCase(".docx")) {
			finalBase64String = "data:image/png;base64," + base64FileStr;				
		}else if (extension.equalsIgnoreCase(".sql")) {
			finalBase64String = "data:application/octet-stream;base64," + base64FileStr;			
		}else if (extension.equalsIgnoreCase(".text")||extension.equalsIgnoreCase(".txt")) {
			finalBase64String = "data:image/png;base64," + base64FileStr;		
		}else if (extension.equalsIgnoreCase(".7z")||extension.equalsIgnoreCase(".rar")||extension.equalsIgnoreCase(".zip")) {
			finalBase64String = "data:image/png;base64," + base64FileStr;			
		}else if (extension.equalsIgnoreCase(".exe")) {
			finalBase64String = "data:application/octet-stream;base64," + base64FileStr;			
		}
		return finalBase64String;
	}
	
	public static File writeBytesToFile(byte[] bytes, File file) {
		try {
			OutputStream os = new FileOutputStream(file);
			os.write(bytes);
			System.out.println("Successfully" + " byte inserted");
			os.close();
		}catch (Exception e) {
			System.out.println("Exception: " + e);
		}
		return file;
	}
	public static File getFileFromBase64String(String base64String, String fileLocation) {
		byte[] byteArray = getByteArrayFromBase64String(base64String);
		
		File file = new File(fileLocation);
		file = writeBytesToFile(byteArray, file);
		
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
	public static List<File> getFilesForFolder(final File folder, List<File> files) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
            	getFilesForFolder(fileEntry, files);
            } else {
                files.add(fileEntry.getAbsoluteFile());
            }
        }
        return files;
    }

    public static File getZippedFile(List<File> files, String fileName) {
        File zipFile = new File(fileName);
        try {

            // create byte buffer
            byte[] buffer = new byte[1024];

            FileOutputStream fos = new FileOutputStream(zipFile);

            ZipOutputStream zos = new ZipOutputStream(fos);

            for (File file : files) {
                //System.out.println(file.getAbsolutePath());
                FileInputStream fis = new FileInputStream(file);

                // begin writing a new ZIP entry, positions the stream to the start of the entry data
                
                zos.putNextEntry(new ZipEntry(file.getCanonicalPath()));
                int length;

                while ((length = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }

                zos.closeEntry();

                // close the InputStream
                fis.close();

            }
            System.out.println("Compressed Successfully...");
            // close the ZipOutputStream
            zos.close();

        } catch (IOException ioe) {
            System.out.println("Error creating zip file: " + ioe);
        }

        return zipFile;
    }
}