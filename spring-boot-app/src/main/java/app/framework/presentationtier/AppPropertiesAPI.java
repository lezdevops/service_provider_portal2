package app.framework.presentationtier;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.framework.businesstier.CaseConverter;
import app.framework.businesstier.CommonFileUtils;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;

@RestController
@RequestMapping(value = "/appProperties")
public class AppPropertiesAPI {
	@RequestMapping(value = "/geti18NPropertiesList/{applicationName}", method = RequestMethod.POST)
	//@ApiMethod(description = "Get all hotel bookings from the database")
	public List<Map<String,Object>> geti18NPropertiesList(@PathVariable String applicationName) {
		//System.out.println("Invoked first.........................."+applicationName);
		List<Map<String,Object>> propertiesFileMapList = new ArrayList<Map<String,Object>>();
		String[] fileNames = CommonFileUtils.getFileNames(new File("i18n/"+applicationName));
		
		for (String fileName : fileNames) {
			Map<String,Object> propertiesFileMap = new LinkedHashMap<String, Object>();
			propertiesFileMap.put("screenName", fileName);
			propertiesFileMap.put("fileName", fileName);
			propertiesFileMapList.add(propertiesFileMap);
		}
		
		return propertiesFileMapList;
	}
	
	@RequestMapping(value = "/loadPropertiesDetails/{applicationName}/{pageName}", method = RequestMethod.POST)
	public List<Map<String,Object>> loadPropertiesDetails(@PathVariable String applicationName,@PathVariable String pageName) {
		//System.out.println("Invoked second..........................");
		List<Map<String,Object>> propertiesFileMapList = new ArrayList<Map<String,Object>>();
		File propertiesFile = new File("i18n/"+applicationName+"/"+pageName+".properties");
		String fileString = "";
		try {
			fileString = FileUtils.readFileToString(propertiesFile);
			String[] lines = fileString.split("\n");
			//pageName = appUser_i18n.properties
			for (String line : lines) {
				String[] lineStrings = line.split("=");
				if (UtilValidate.isNotEmpty(lineStrings) && lineStrings.length>1) {
					Map<String,Object> propertiesFileMap = new LinkedHashMap<String, Object>();
					propertiesFileMap.put("keyProperty", lineStrings[0]);
					propertiesFileMap.put("valueProperty", lineStrings[1]);
					propertiesFileMapList.add(propertiesFileMap);
				}
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return propertiesFileMapList;
	}
	@RequestMapping(value = "/appPropertiesSave", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public String appPropertiesSave(@RequestBody Map<String,Object> appPropertiesMap) {
		System.out.println("Invoked third..........................");
		String resourceName = (String)appPropertiesMap.get("resourceName");
		String applicationName = (String)appPropertiesMap.get("applicationName");
		try {
			List<Map<String,Object>> appPropertiessMapList = (List<Map<String,Object>>)appPropertiesMap.get("appPropertiess");
			//System.out.println("resourceName-> "+resourceName);
			//System.out.println("appPropertiess.get(0).get(\"keyProperty\")-> "+appPropertiessMapList.get(0).get("keyProperty"));
			
			String propertiesString = "";
			
			for (Map<String,Object> appPropertiessMap : appPropertiessMapList) {
				String keyProperty = (String)appPropertiessMap.get("keyProperty");
				String valueProperty = (String)appPropertiessMap.get("valueProperty");
				propertiesString = propertiesString + keyProperty + " = "+ valueProperty+"\n";
			}
			//System.out.println("propertiesString-> "+propertiesString);
			if (UtilValidate.isNotEmpty(resourceName)) {
				File frontUIFile = new File("i18n/"+applicationName+"/"+ CaseConverter.toCamelCase(resourceName) +"_i18n.properties");
	            frontUIFile.setReadable(true, false);
	            frontUIFile.setExecutable(true, false);
	            frontUIFile.setWritable(true, false);
	            FileUtils.writeStringToFile(frontUIFile,propertiesString); 
			}
			
		} catch (Exception e) {
			return "Following Error Occured: "+e.getMessage();
		}
		return "Successfully Saved Following Data: "+resourceName;
	}
	@RequestMapping(value = "/changeLanguage/{language}", method = RequestMethod.POST)
	public String changeLanguage(@PathVariable String language) {
		File configProperties = new File("i18n/i18n_config.properties");
		
		ConfigUtility configUtility = new ConfigUtility(configProperties);
		try {
			List<Map<String,Object>> properties = new ArrayList<Map<String,Object>>();
			Map<String,Object> propertyMap = new LinkedHashMap<String,Object>();
			propertyMap.put("enabled.language", language);
			properties.add(propertyMap);
			
			configUtility.saveProperties(properties, propertyMap.keySet());
		} catch (IOException e) {
			e.printStackTrace();
			return "Error to change language.";
		}
		
		return "Changed Language.";
	}
}
