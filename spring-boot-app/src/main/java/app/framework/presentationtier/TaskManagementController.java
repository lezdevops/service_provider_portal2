package app.framework.presentationtier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.framework.businesstier.TaskManagementManager;
import app.framework.persistencetier.TaskManagement;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/taskManagements")
//@Api(name = "Modules TaskManagement API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class TaskManagementController {

	@Autowired
	private TaskManagementManager taskManagementManager;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	//@ApiMethod(description = "Get all entitys from the database")
	public List<TaskManagement> getAll() {
		return taskManagementManager.getTaskManagements();
	}
	
	@RequestMapping(value = "/getTaskManagementByAssigneeId",params = { "assigneeId" }, method = RequestMethod.GET)
	public List<Map<String,Object>> getTaskManagementByAssigneeId(@RequestParam("assigneeId") Long assigneeId) {
		List<Map<String,Object>> taskManagementList = new ArrayList();
		
		List<TaskManagement> taskManagementListByAssigneeId = taskManagementManager.findTaskManagementsByAssigneeId(assigneeId);
		for (TaskManagement taskManagement : taskManagementListByAssigneeId) {
			Map<String,Object> taskManagementMap = new HashMap<String, Object>();
			taskManagementMap.put("reporterId", taskManagement.getReporterId());
			taskManagementMap.put("taskDetails", taskManagement.getTaskDetails());
			taskManagementMap.put("completeParcentage", taskManagement.getCompleteParcentage());
			taskManagementList.add(taskManagementMap);
		}
		return taskManagementList;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a entity and save it to the database")
	@ResponseBody
	public List<Map<String,Object>> save(@RequestBody TaskManagement taskManagement) {
		return taskManagementManager.save(taskManagement);
	}

	@RequestMapping(value = "/saveMultipleTaskManagementData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<TaskManagement> saveMultipleTaskManagementData(@RequestBody List<TaskManagement> taskManagements) {
		return taskManagementManager.saveMultipleTaskManagementData(taskManagements);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public String remove(@PathVariable long id) {
		return taskManagementManager.remove(id);
	}
	@RequestMapping(value = "/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<TaskManagement> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<TaskManagement> resultPage = taskManagementManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
}
