package app.framework.presentationtier;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import app.appmodel.User;
import app.common.persistencetier.UserService;
import app.framework.businesstier.CaseConverter;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.framework.persistencetier.ApplicationComponent;
import app.framework.persistencetier.ApplicationComponentRepository;

@Controller
public class IndexController {
	private String appMode;
	
	@Autowired
	private ApplicationComponentRepository applicationComponentRepository;
	
    @Autowired
    public IndexController(Environment environment){
        appMode = environment.getProperty("app-mode");
    }

    @RequestMapping("/apps")
    public String index(Model model){
    	 ConfigUtility configUtility = new ConfigUtility(new File("basic.properties"));
    	 try {
			Properties properties = configUtility.loadProperties();
			if (UtilValidate.isNotEmpty(properties)) {
				String appTitle = properties.getProperty("app.title");
				String appName = properties.getProperty("app.name");
				String appShortName = properties.getProperty("app.shortName");
				String appSlogan = properties.getProperty("app.slogan");
				String appOwnerCompany = properties.getProperty("app.ownerCompany");
				model.addAttribute("appTitle", appTitle);
				model.addAttribute("appName", appName);
				model.addAttribute("appShortName", appShortName);
				model.addAttribute("appSlogan", appSlogan);
				model.addAttribute("appOwnerCompany", appOwnerCompany);
				
			}else{
				model.addAttribute("appTitle", "Application Title");
				model.addAttribute("appName", "Application Name");
				model.addAttribute("appShortName", "AN");
				model.addAttribute("appSlogan", "Application Slogan.");				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

    	//return "module/index";
        commonDashboardContents(model);
        return "dashboard/index";
    }
    @RequestMapping("/dashboard")
    public String dashboard(Model model){
        model.addAttribute("datetime", new Date());
        model.addAttribute("username", "John");
        model.addAttribute("mode", appMode);
        commonDashboardContents(model);
        //return "kroybikroy";
        //return "index";
        return "dashboard/index";
    }

	@Autowired
	private UserService userService;
	public void commonDashboardContents(Model model) {
		List<Map<String, Object>> applicationComponentMapList = new ArrayList<Map<String, Object>>();

		List<Map<String, Object>> folders = getApplicationModuleList();

		model.addAttribute("applicationComponents", applicationComponentMapList);
		List<ApplicationComponent> applicationComponents = applicationComponentRepository
				.findAll();
		
		if (UtilValidate.isNotEmpty(applicationComponents)) {
			applicationComponentMapList = new ArrayList<Map<String, Object>>();
			for (ApplicationComponent applicationComponent : applicationComponents) {
				Map<String, Object> applicationComponentMap = new HashMap<String, Object>();
				String componentName = applicationComponent.getComponentName();
				applicationComponentMap.put("moduleName", componentName);
				applicationComponentMap.put("details",
						applicationComponent.getDetails());

				Boolean folderFound = false;

				for (Map<String, Object> folder : folders) {
					String moduleName = (String) folder.get("moduleName");
					if (componentName.equalsIgnoreCase(moduleName)) {
						folderFound = true;
					}
				}
				if (folderFound) {
					applicationComponentMapList.add(applicationComponentMap);
				}
			}
		} else {
			applicationComponentMapList = folders;
		}
		applicationComponentMapList = folders;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		String userName = user.getUserName();
		
		applicationComponentMapList = new ArrayList<Map<String, Object>>();
		if (UtilValidate.isNotEmpty(userName) && userName.equalsIgnoreCase("cloudserviceuser")) {
			Map<String,Object> moduleMap = new LinkedHashMap<String, Object>();
			moduleMap.put("moduleName", "swapnobikash");
			moduleMap.put("details", CaseConverter.toSentence("swapnobikash"));
			applicationComponentMapList.add(moduleMap);
			
			moduleMap = new LinkedHashMap<String, Object>();
			moduleMap.put("moduleName", "massPublicity");
			moduleMap.put("details", CaseConverter.toSentence("massPublicity"));
			applicationComponentMapList.add(moduleMap);
			
		}else if(UtilValidate.isNotEmpty(userName) && userName.equalsIgnoreCase("swapnobikash")){
			Map<String,Object> moduleMap = new LinkedHashMap<String, Object>();
			moduleMap.put("moduleName", "swapnobikash");
			moduleMap.put("details", CaseConverter.toSentence("swapnobikash"));
			applicationComponentMapList.add(moduleMap);
		}else if(UtilValidate.isNotEmpty(userName) && userName.equalsIgnoreCase("projectmanager")){
			Map<String,Object> moduleMap = new LinkedHashMap<String, Object>();
			moduleMap.put("moduleName", "projectManagement");
			moduleMap.put("details", CaseConverter.toSentence("projectManagement"));
			applicationComponentMapList.add(moduleMap);
		}else{
			applicationComponentMapList = folders;			
		}
		model.addAttribute("welcomeMessage", "Welcome " + user.getFirstName()+ " " + user.getLastName() + " (" + user.getEmail() + ")");
		
		model.addAttribute("applicationComponents", applicationComponentMapList);
	}
    @RequestMapping("/dashboardTabMenus")
    public String dashboardTabMenus(Model model){
        model.addAttribute("datetime", new Date());
        model.addAttribute("username", "John");
        model.addAttribute("mode", appMode);
        List<Map<String,Object>> components = getApplicationModuleList();
        model.addAttribute("components", components);
        
        //return "kroybikroy";
        //return "index";
        return "dashboard/dashboardTabMenus";
    }
   
	public static List<Map<String,Object>> getApplicationModuleList(){
		List<Map<String,Object>> moduleList = new ArrayList<Map<String,Object>>();
		String[] moduleNames = getFolderNames(new File("src/main/resources/templates"));
		for (String moduleName : moduleNames) {
			if (CaseConverter.toSentence(moduleName).equalsIgnoreCase("Bootscaff") 
					||CaseConverter.toSentence(moduleName).equalsIgnoreCase("Dashboard") 
					|| CaseConverter.toSentence(moduleName).equalsIgnoreCase("Themeincludes")) {
				continue;
			}
			Map<String,Object> moduleMap = new LinkedHashMap<String, Object>();
			moduleMap.put("moduleName", moduleName);
			moduleMap.put("details", CaseConverter.toSentence(moduleName));
			moduleList.add(moduleMap);
		}
		return moduleList;
	}
	public static String[] getFolderNames(File curDir) {
		String[] folderNames = new String[50];
		int i = 0;
		File[] filesList = curDir.listFiles();
		for (File file : filesList) {
			if (file.isDirectory())
				folderNames[i++] = file.getName();
		}
		String[] newFolderNames = new String[i];
		i = 0;
		for (String folderName : folderNames) {
			if (folderName != null) {
				newFolderNames[i++] = folderName;
			}
		}
		return newFolderNames;
	}

	/*@Controller
	public class HelloController {
		@RequestMapping("/hello")
		public String hello(
				Model model,
				@RequestParam(value = "name", required = false, defaultValue = "World") String name) {
			List<String> names = new ArrayList();
			names.add("Arif");
			names.add("Jobair");
			names.add("Ehasun Khan");
			names.add("Amit");
			model.addAttribute("names", names);
			model.addAttribute("name", name);
			return "hello";
		}
	}*/

	
}