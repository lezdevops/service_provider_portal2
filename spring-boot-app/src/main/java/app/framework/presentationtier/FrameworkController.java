package app.framework.presentationtier;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/framework")
public class FrameworkController {
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model) {
		return "framework/index";
	}
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String dashboard(Model model) {
		return "framework/dashboard";
	}
	@RequestMapping(value = "/mailbox", method = RequestMethod.GET)
	public String mailbox(Model model) {
		return "framework/mailbox";
	}
	@RequestMapping(value = "/mailbox_starter", method = RequestMethod.GET)
	public String mailbox_starter(Model model) {
		return "framework/mailbox_starter";
	}
	@RequestMapping(value = "/taskManagement", method = RequestMethod.GET)
	public String taskManagement(Model model) {
		return "framework/taskManagement";
	}
	@RequestMapping(value = "/notification", method = RequestMethod.GET)
	public String notification(Model model) {
		return "framework/notification";
	}
	// newControllerGoesHere

}
