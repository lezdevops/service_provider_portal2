package app.framework.presentationtier;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.framework.businesstier.UserMessageManager;
import app.framework.persistencetier.UserMessage;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/userMessages")
//@Api(name = "Modules UserMessage API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class UserMessageController {

	@Autowired
	private UserMessageManager userMessageManager;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	//@ApiMethod(description = "Get all entitys from the database")
	public List<UserMessage> getAll() {
		return userMessageManager.getUserMessages();
	}

	
	@RequestMapping(value = "/save", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a entity and save it to the database")
	@ResponseBody
	public List<Map<String,Object>> save(@RequestBody UserMessage userMessage) {
		return userMessageManager.save(userMessage);
	}
	@RequestMapping(value = "/saveMultipleUserMessageData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	//@ApiMethod(description = "Save's a hotel booking and save it to the database")
	@ResponseBody
	public List<UserMessage> saveMultipleUserMessageData(@RequestBody List<UserMessage> userMessages) {
		return userMessageManager.saveMultipleUserMessageData(userMessages);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	//@ApiMethod(description = "Remove the entity database with the provided ID from the database")
	public String remove(@PathVariable long id) {
		return userMessageManager.remove(id);
	}
	@RequestMapping(value = "/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Page<UserMessage> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
		 
        Page<UserMessage> resultPage = userMessageManager.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
           // throw new MyResourceNotFoundException();
        }
 
        return resultPage;
	 }
	
	@RequestMapping(value = "/getUserMessagesByMessageType", params = { "messageType"},  method = RequestMethod.GET)
	public List<UserMessage> getUserMessagesByMessageType(@RequestParam("messageType") String messageType) {
		 
		List<UserMessage> userMessages = userMessageManager.getUserMessagesByMessageType(messageType);
        return userMessages;
	}
	@RequestMapping(value = "/getUserMessagesBySenderId", params = { "senderId"},  method = RequestMethod.GET)
	public List<UserMessage> getUserMessagesBySenderId(@RequestParam("senderId") Long senderId) {
		 
		List<UserMessage> userMessages = userMessageManager.getUserMessagesBySenderId(senderId);
        return userMessages;
	}
	@RequestMapping(value = "/getUserMessagesByReceiverId", params = { "receiverId"},  method = RequestMethod.GET)
	public List<UserMessage> getUserMessagesByReceiverId(@RequestParam("receiverId") Long receiverId) {
		 
		List<UserMessage> userMessages = userMessageManager.getUserMessagesByReceiverId(receiverId);
        return userMessages;
	}
}
