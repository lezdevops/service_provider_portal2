package app.framework.presentationtier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import app.framework.businesstier.GenerateTaskManagementExcelReport;
import app.framework.businesstier.GenerateTaskManagementReport;
import app.framework.businesstier.TaskManagementManager;
import app.framework.persistencetier.TaskManagement;

@Controller
public class TaskManagementReportController {
	@Autowired
	private TaskManagementManager taskManagementManager;

	@RequestMapping(path = "/taskManagements/allTaskManagementsReport", method = RequestMethod.GET)
    public ModelAndView report() {
        
        Map<String, Object> model = new HashMap<>();

        List<TaskManagement> taskManagements = taskManagementManager.getTaskManagements();
        model.put("taskManagements", taskManagements);

        return new ModelAndView(new GenerateTaskManagementReport(), model);
    }
	@RequestMapping(path = "/taskManagements/allTaskManagementsExcelReport", method = RequestMethod.GET)
    public ModelAndView excelReport() {
        
        Map<String, Object> model = new HashMap<>();

        List<TaskManagement> taskManagements = taskManagementManager.getTaskManagements();
        model.put("taskManagements", taskManagements);

        return new ModelAndView(new GenerateTaskManagementExcelReport(), model);
    }
	
	@RequestMapping(path = "/taskManagements/uploadTaskManagementTemplateForm", method = RequestMethod.GET)
    public ModelAndView templateForm() {
        
        Map<String, Object> model = new HashMap<>();

        List<TaskManagement> taskManagements = new ArrayList<TaskManagement>();
        model.put("taskManagements", taskManagements);

        return new ModelAndView(new GenerateTaskManagementExcelReport(), model);
    }
}
