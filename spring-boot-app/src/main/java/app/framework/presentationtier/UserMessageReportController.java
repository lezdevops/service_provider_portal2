package app.framework.presentationtier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import app.framework.businesstier.GenerateUserMessageExcelReport;
import app.framework.businesstier.GenerateUserMessageReport;
import app.framework.businesstier.UserMessageManager;
import app.framework.persistencetier.UserMessage;

@Controller
public class UserMessageReportController {
	@Autowired
	private UserMessageManager userMessageManager;

	@RequestMapping(path = "/userMessages/allUserMessagesReport", method = RequestMethod.GET)
    public ModelAndView report() {
        
        Map<String, Object> model = new HashMap<>();

        List<UserMessage> userMessages = userMessageManager.getUserMessages();
        model.put("userMessages", userMessages);

        return new ModelAndView(new GenerateUserMessageReport(), model);
    }
	@RequestMapping(path = "/userMessages/allUserMessagesExcelReport", method = RequestMethod.GET)
    public ModelAndView excelReport() {
        
        Map<String, Object> model = new HashMap<>();

        List<UserMessage> userMessages = userMessageManager.getUserMessages();
        model.put("userMessages", userMessages);

        return new ModelAndView(new GenerateUserMessageExcelReport(), model);
    }
	
	@RequestMapping(path = "/userMessages/uploadUserMessageTemplateForm", method = RequestMethod.GET)
    public ModelAndView templateForm() {
        
        Map<String, Object> model = new HashMap<>();

        List<UserMessage> userMessages = new ArrayList<UserMessage>();
        model.put("userMessages", userMessages);

        return new ModelAndView(new GenerateUserMessageExcelReport(), model);
    }
}
