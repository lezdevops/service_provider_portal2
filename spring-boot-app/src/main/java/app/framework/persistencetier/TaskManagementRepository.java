package app.framework.persistencetier;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TaskManagementRepository extends JpaRepository<TaskManagement, Long>{

	List<TaskManagement> findTaskManagementsByAssigneeId(Long assigneeId);
	
}
