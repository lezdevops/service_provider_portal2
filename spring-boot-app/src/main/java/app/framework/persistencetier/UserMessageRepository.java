package app.framework.persistencetier;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserMessageRepository extends JpaRepository<UserMessage, Long>{

	List<UserMessage> findUserMessagesBySenderMessageType(String senderMessageType);

	List<UserMessage> findUserMessagesBySenderId(Long senderId);

	List<UserMessage> findUserMessagesByReceiverId(Long receiverId);
	
}
