package app.framework.persistencetier;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationComponentRepository extends JpaRepository<ApplicationComponent, Long>{
	public List<ApplicationComponent> findByComponentName(String componentName);
	
}
