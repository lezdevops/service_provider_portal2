package app.framework.persistencetier;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ApplicationComponent")
public class ApplicationComponent {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long componentId;
	private String componentName;
	private String details;
	private String basePermission;
	private String iconStyle;
	private String linkStyle;

	public Long getComponentId() {
		return componentId;
	}

	public void setComponentId(Long componentId) {
		this.componentId = componentId;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getBasePermission() {
		return basePermission;
	}

	public void setBasePermission(String basePermission) {
		this.basePermission = basePermission;
	}

	public String getIconStyle() {
		return iconStyle;
	}

	public void setIconStyle(String iconStyle) {
		this.iconStyle = iconStyle;
	}

	public String getLinkStyle() {
		return linkStyle;
	}

	public void setLinkStyle(String linkStyle) {
		this.linkStyle = linkStyle;
	}

}
