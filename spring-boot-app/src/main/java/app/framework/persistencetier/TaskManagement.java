package app.framework.persistencetier;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TaskManagement")
public class TaskManagement {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long taskId;
	public Long assigneeId;
	public Long reporterId;
	public String taskDetails;
	public Timestamp createdDate;
	public Timestamp planReportDate;
	public Timestamp actualReportDate;
	public BigDecimal completeParcentage;
	public String taskStatus;

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Long getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(Long assigneeId) {
		this.assigneeId = assigneeId;
	}

	public Long getReporterId() {
		return reporterId;
	}

	public void setReporterId(Long reporterId) {
		this.reporterId = reporterId;
	}

	public String getTaskDetails() {
		return taskDetails;
	}

	public void setTaskDetails(String taskDetails) {
		this.taskDetails = taskDetails;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getPlanReportDate() {
		return planReportDate;
	}

	public void setPlanReportDate(Timestamp planReportDate) {
		this.planReportDate = planReportDate;
	}

	public Timestamp getActualReportDate() {
		return actualReportDate;
	}

	public void setActualReportDate(Timestamp actualReportDate) {
		this.actualReportDate = actualReportDate;
	}

	public BigDecimal getCompleteParcentage() {
		return completeParcentage;
	}

	public void setCompleteParcentage(BigDecimal completeParcentage) {
		this.completeParcentage = completeParcentage;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

}
