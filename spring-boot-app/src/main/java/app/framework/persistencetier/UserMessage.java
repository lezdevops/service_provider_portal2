package app.framework.persistencetier;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UserMessage")
public class UserMessage {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long userMessageId;
	public Long senderId;
	public Long receiverId;
	public String subject;

	public String message;
	public Timestamp createdDate;
	public Timestamp seenDate;
	public String messageStatus;
	public String senderMessageType;

	public Long getUserMessageId() {
		return userMessageId;
	}

	public void setUserMessageId(Long userMessageId) {
		this.userMessageId = userMessageId;
	}

	public Long getSenderId() {
		return senderId;
	}

	public void setSenderId(Long senderId) {
		this.senderId = senderId;
	}

	public Long getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(Long receiverId) {
		this.receiverId = receiverId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getSeenDate() {
		return seenDate;
	}

	public void setSeenDate(Timestamp seenDate) {
		this.seenDate = seenDate;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getSenderMessageType() {
		return senderMessageType;
	}

	public void setSenderMessageType(String senderMessageType) {
		this.senderMessageType = senderMessageType;
	}

}
