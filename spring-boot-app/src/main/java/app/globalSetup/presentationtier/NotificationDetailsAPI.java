package app.globalSetup.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.NotificationDetails;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.NotificationDetailsManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class NotificationDetailsAPI {
	
	@Autowired
	private NotificationDetailsManager notificationDetailsManager;

	@PostMapping("/notificationDetails")
	public Map<String,Object> createNotificationDetails(HttpServletRequest request, @RequestBody NotificationDetails notificationDetails) {
		
		

		return notificationDetailsManager.create(request,notificationDetails);
	}
	@GetMapping("/notificationDetails/{id}")
	public Map<String,Object> getNotificationDetails(HttpServletRequest request, @PathVariable Long id) {
		return notificationDetailsManager.getNotificationDetailsForEdit(request,id);
	}
	@GetMapping(value = "/notificationDetailss")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return notificationDetailsManager.getNotificationDetailss(request);
	}
	@PutMapping("/notificationDetails")
	public Map<String,Object> updateNotificationDetails(HttpServletRequest request, @RequestBody NotificationDetails notificationDetails) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = notificationDetailsManager.getNotificationDetails(request,notificationDetails.getId());
			NotificationDetails newNotificationDetails = (NotificationDetails)resultMap.get("notificationDetails");
			newNotificationDetails.setSenderId(notificationDetails.getSenderId());
newNotificationDetails.setReceiverId(notificationDetails.getReceiverId());
newNotificationDetails.setIsRead(notificationDetails.getIsRead());

			return notificationDetailsManager.update(request,newNotificationDetails);
		}
		
	}
	@DeleteMapping("/notificationDetails/{id}")
	public Map<String,Object> deleteNotificationDetails(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return notificationDetailsManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/notificationDetails/saveMultipleNotificationDetailsData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleNotificationDetailsData( HttpServletRequest request, @RequestBody List<NotificationDetails> notificationDetailss) {
		return notificationDetailsManager.saveMultipleNotificationDetailsData(request,notificationDetailss);
	}
	@RequestMapping(value = "/notificationDetails/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return notificationDetailsManager.remove(request,id);
	}
	@RequestMapping(value = "/notificationDetails/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return notificationDetailsManager.removeAll(request);
	}
	@RequestMapping(value = "/notificationDetails/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return notificationDetailsManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/notificationDetails/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return notificationDetailsManager.findPaginatedNotificationDetailssBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/notificationDetails/searchOnNotificationDetailsTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnNotificationDetailsTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return notificationDetailsManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/notificationDetails/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<NotificationDetails> notificationDetailss = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				NotificationDetails notificationDetails = new NotificationDetails();
				HSSFRow row = worksheet.getRow(i++);
				//notificationDetails.setField2(row.getCell(1).getStringCellValue());
				notificationDetails.setSenderId(row.getCell(0).getStringCellValue());
notificationDetails.setReceiverId(row.getCell(1).getStringCellValue());
notificationDetails.setIsRead(row.getCell(2).getBooleanCellValue());

				notificationDetailss.add(notificationDetails);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("notificationDetailss", notificationDetailss);
		return returnResult;
	}
	@RequestMapping(value = "/notificationDetails/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<NotificationDetails> notificationDetailss = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				NotificationDetails notificationDetails = new NotificationDetails();
				XSSFRow row = worksheet.getRow(i++);
				//notificationDetails.setField2(row.getCell(1).getStringCellValue());
				notificationDetails.setSenderId(row.getCell(0).getStringCellValue());
notificationDetails.setReceiverId(row.getCell(1).getStringCellValue());
notificationDetails.setIsRead(row.getCell(2).getBooleanCellValue());

				notificationDetailss.add(notificationDetails);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("notificationDetailss", notificationDetailss);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/notificationDetails/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<NotificationDetails> notificationDetailss = new ArrayList<NotificationDetails>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			notificationDetailss = (List<NotificationDetails>) resultMap.get("notificationDetailss");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			notificationDetailss = (List<NotificationDetails>) resultMap.get("notificationDetailss");
		}
		
		if (UtilValidate.isNotEmpty(notificationDetailss) && notificationDetailss.size()>0) {
			notificationDetailss.remove(0);
		}
		returnResult.put("notificationDetailss", notificationDetailss);
		return returnResult;
	}
}
