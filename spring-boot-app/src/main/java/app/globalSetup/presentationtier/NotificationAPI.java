package app.globalSetup.presentationtier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.Notification;
import app.appmodel.NotificationDetails;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.NotificationDetailsManager;
import app.globalSetup.businesstier.NotificationManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = { "http://localhost:4200", "http://techpoint.pro" }, allowedHeaders = "*")
public class NotificationAPI {

	@Autowired
	private NotificationManager notificationManager;

	@GetMapping("/userNotifications")
	public Map<String,Object> getNotificationDetailsesByReceiverId(HttpServletRequest request){
		Map<String,Object> resultMap = new LinkedHashMap<String,Object>();
		List<NotificationDetails> userNotifications = notificationManager.getUserNotifications(request);
		resultMap.put("userNotifications", userNotifications);
		return resultMap;
	}
	
	@PostMapping("/notification")
	public Map<String, Object> createNotification(HttpServletRequest request, @RequestBody Notification notification) {

		return notificationManager.create(request, notification);
	}

	@GetMapping("/notification/{id}")
	public Map<String, Object> getNotification(HttpServletRequest request, @PathVariable Long id) {
		return notificationManager.getNotificationForEdit(request, id);
	}

	@GetMapping(value = "/notifications")
	public Map<String, Object> getAll(HttpServletRequest request) {
		return notificationManager.getNotifications(request);
	}

	@PutMapping("/notification")
	public Map<String, Object> updateNotification(HttpServletRequest request, @RequestBody Notification notification) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		} else {
			Map<String, Object> resultMap = notificationManager.getNotification(request, notification.getId());
			Notification newNotification = (Notification) resultMap.get("notification");
			newNotification.setMessage(notification.getMessage());
			newNotification.setOldObjectVal(notification.getOldObjectVal());
			newNotification.setNewObjectVal(notification.getNewObjectVal());
			newNotification.setAttachment(notification.getAttachment());
			newNotification.setUrl(notification.getUrl());

			return notificationManager.update(request, newNotification);
		}

	}

	@DeleteMapping("/notification/{id}")
	public Map<String, Object> deleteNotification(HttpServletRequest request, @PathVariable Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		} else {
			return notificationManager.remove(request, id);
		}
	}

	@RequestMapping(value = "/notification/saveMultipleNotificationData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> saveMultipleNotificationData(HttpServletRequest request,
			@RequestBody List<Notification> notifications) {
		return notificationManager.saveMultipleNotificationData(request, notifications);
	}

	@RequestMapping(value = "/notification/delete/{id}", method = RequestMethod.POST)
	public Map<String, Object> remove(HttpServletRequest request, @PathVariable Long id) {
		return notificationManager.remove(request, id);
	}

	@RequestMapping(value = "/notification/removeAll", method = RequestMethod.POST)
	public Map<String, Object> removeAll(HttpServletRequest request) {
		return notificationManager.removeAll(request);
	}

	@RequestMapping(value = "/notification/get", params = { "page", "size" }, method = RequestMethod.GET)
	public Map<String, Object> findPaginated(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size) {
		return notificationManager.findPaginated(request, page, size);
	}

	@RequestMapping(value = "/notification/getPaginatedBy", params = { "page", "size",
			"searchToken" }, method = RequestMethod.GET)
	public Map<String, Object> getPaginatedBy(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size, @RequestParam("searchToken") String searchToken) {
		return notificationManager.findPaginatedNotificationsBySearchToken(request, page, size, searchToken);
	}

	@RequestMapping(value = "/notification/searchOnNotificationTable", method = RequestMethod.GET)
	public Map<String, Object> searchOnNotificationTable(HttpServletRequest request, @RequestParam("id") Long id,
			Pageable pageRequest) {
		return notificationManager.findById(request, id, pageRequest);
	}

	@RequestMapping(value = "/notification/processExcel2003", method = RequestMethod.POST)
	public Map<String, Object> processExcel2003(HttpServletRequest request,
			@RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		List<Notification> notifications = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Notification notification = new Notification();
				HSSFRow row = worksheet.getRow(i++);
				// notification.setField2(row.getCell(1).getStringCellValue());
				notification.setMessage(row.getCell(0).getStringCellValue());
				notification.setOldObjectVal(row.getCell(1).getStringCellValue());
				notification.setNewObjectVal(row.getCell(2).getStringCellValue());
				notification.setAttachment(row.getCell(3).getStringCellValue());
				notification.setUrl(row.getCell(4).getStringCellValue());

				notifications.add(notification);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("notifications", notifications);
		return returnResult;
	}

	@RequestMapping(value = "/notification/processExcel2007", method = RequestMethod.POST)
	public Map<String, Object> processExcel2007(HttpServletRequest request,
			@RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		List<Notification> notifications = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Notification notification = new Notification();
				XSSFRow row = worksheet.getRow(i++);
				// notification.setField2(row.getCell(1).getStringCellValue());
				notification.setMessage(row.getCell(0).getStringCellValue());
				notification.setOldObjectVal(row.getCell(1).getStringCellValue());
				notification.setNewObjectVal(row.getCell(2).getStringCellValue());
				notification.setAttachment(row.getCell(3).getStringCellValue());
				notification.setUrl(row.getCell(4).getStringCellValue());

				notifications.add(notification);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		returnResult.put("notifications", notifications);
		return returnResult;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/notification/excelFileUpload")
	@ResponseBody
	public Map<String, Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<Notification> notifications = new ArrayList<Notification>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String, Object> resultMap = processExcel2007(request, file);
			notifications = (List<Notification>) resultMap.get("notifications");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String, Object> resultMap = processExcel2003(request, file);
			notifications = (List<Notification>) resultMap.get("notifications");
		}

		if (UtilValidate.isNotEmpty(notifications) && notifications.size() > 0) {
			notifications.remove(0);
		}
		returnResult.put("notifications", notifications);
		return returnResult;
	}
}
