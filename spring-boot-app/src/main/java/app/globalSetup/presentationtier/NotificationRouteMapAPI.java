package app.globalSetup.presentationtier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.NotificationRouteMap;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.NotificationRouteMapManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = { "http://localhost:4200", "http://techpoint.pro" }, allowedHeaders = "*")
public class NotificationRouteMapAPI {

	@Autowired
	private NotificationRouteMapManager notificationRouteMapManager;

	@PostMapping("/notificationRouteMap")
	public Map<String, Object> createNotificationRouteMap(HttpServletRequest request,
			@RequestBody NotificationRouteMap notificationRouteMap) {

		return notificationRouteMapManager.create(request, notificationRouteMap);
	}

	@GetMapping("/notificationRouteMap/{id}")
	public Map<String, Object> getNotificationRouteMap(HttpServletRequest request, @PathVariable Long id) {
		return notificationRouteMapManager.getNotificationRouteMapForEdit(request, id);
	}

	@GetMapping(value = "/notificationRouteMaps")
	public Map<String, Object> getAll(HttpServletRequest request) {
		return notificationRouteMapManager.getNotificationRouteMaps(request);
	}

	@PutMapping("/notificationRouteMap")
	public Map<String, Object> updateNotificationRouteMap(HttpServletRequest request,
			@RequestBody NotificationRouteMap notificationRouteMap) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		} else {
			Map<String, Object> resultMap = notificationRouteMapManager.getNotificationRouteMap(request,
					notificationRouteMap.getId());
			NotificationRouteMap newNotificationRouteMap = (NotificationRouteMap) resultMap.get("notificationRouteMap");
			newNotificationRouteMap.setSenderId(notificationRouteMap.getSenderId());
			newNotificationRouteMap.setIsActive(notificationRouteMap.getIsActive());

			return notificationRouteMapManager.update(request, newNotificationRouteMap);
		}

	}

	@DeleteMapping("/notificationRouteMap/{id}")
	public Map<String, Object> deleteNotificationRouteMap(HttpServletRequest request, @PathVariable Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		} else {
			return notificationRouteMapManager.remove(request, id);
		}
	}

	@RequestMapping(value = "/notificationRouteMap/saveMultipleNotificationRouteMapData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> saveMultipleNotificationRouteMapData(HttpServletRequest request,
			@RequestBody List<NotificationRouteMap> notificationRouteMaps) {
		return notificationRouteMapManager.saveMultipleNotificationRouteMapData(request, notificationRouteMaps);
	}

	@RequestMapping(value = "/notificationRouteMap/delete/{id}", method = RequestMethod.POST)
	public Map<String, Object> remove(HttpServletRequest request, @PathVariable Long id) {
		return notificationRouteMapManager.remove(request, id);
	}

	@RequestMapping(value = "/notificationRouteMap/removeAll", method = RequestMethod.POST)
	public Map<String, Object> removeAll(HttpServletRequest request) {
		return notificationRouteMapManager.removeAll(request);
	}

	@RequestMapping(value = "/notificationRouteMap/get", params = { "page", "size" }, method = RequestMethod.GET)
	public Map<String, Object> findPaginated(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size) {
		return notificationRouteMapManager.findPaginated(request, page, size);
	}

	@RequestMapping(value = "/notificationRouteMap/getPaginatedBy", params = { "page", "size",
			"searchToken" }, method = RequestMethod.GET)
	public Map<String, Object> getPaginatedBy(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size, @RequestParam("searchToken") String searchToken) {
		return notificationRouteMapManager.findPaginatedNotificationRouteMapsBySearchToken(request, page, size,
				searchToken);
	}

	@RequestMapping(value = "/notificationRouteMap/searchOnNotificationRouteMapTable", method = RequestMethod.GET)
	public Map<String, Object> searchOnNotificationRouteMapTable(HttpServletRequest request,
			@RequestParam("id") Long id, Pageable pageRequest) {
		return notificationRouteMapManager.findById(request, id, pageRequest);
	}

	@RequestMapping(value = "/notificationRouteMap/processExcel2003", method = RequestMethod.POST)
	public Map<String, Object> processExcel2003(HttpServletRequest request,
			@RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		List<NotificationRouteMap> notificationRouteMaps = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				NotificationRouteMap notificationRouteMap = new NotificationRouteMap();
				HSSFRow row = worksheet.getRow(i++);
				// notificationRouteMap.setField2(row.getCell(1).getStringCellValue());
				notificationRouteMap.setSenderId(row.getCell(0).getStringCellValue());
				notificationRouteMap.setIsActive(row.getCell(2).getBooleanCellValue());

				notificationRouteMaps.add(notificationRouteMap);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("notificationRouteMaps", notificationRouteMaps);
		return returnResult;
	}

	@RequestMapping(value = "/notificationRouteMap/processExcel2007", method = RequestMethod.POST)
	public Map<String, Object> processExcel2007(HttpServletRequest request,
			@RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		List<NotificationRouteMap> notificationRouteMaps = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				NotificationRouteMap notificationRouteMap = new NotificationRouteMap();
				XSSFRow row = worksheet.getRow(i++);
				// notificationRouteMap.setField2(row.getCell(1).getStringCellValue());
				notificationRouteMap.setSenderId(row.getCell(0).getStringCellValue());
				notificationRouteMap.setIsActive(row.getCell(2).getBooleanCellValue());

				notificationRouteMaps.add(notificationRouteMap);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		returnResult.put("notificationRouteMaps", notificationRouteMaps);
		return returnResult;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/notificationRouteMap/excelFileUpload")
	@ResponseBody
	public Map<String, Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<NotificationRouteMap> notificationRouteMaps = new ArrayList<NotificationRouteMap>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String, Object> resultMap = processExcel2007(request, file);
			notificationRouteMaps = (List<NotificationRouteMap>) resultMap.get("notificationRouteMaps");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String, Object> resultMap = processExcel2003(request, file);
			notificationRouteMaps = (List<NotificationRouteMap>) resultMap.get("notificationRouteMaps");
		}

		if (UtilValidate.isNotEmpty(notificationRouteMaps) && notificationRouteMaps.size() > 0) {
			notificationRouteMaps.remove(0);
		}
		returnResult.put("notificationRouteMaps", notificationRouteMaps);
		return returnResult;
	}
}
