package app.globalSetup.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.ApprovalQueueDetails;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.ApprovalQueueDetailsManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class ApprovalQueueDetailsAPI {
	
	@Autowired
	private ApprovalQueueDetailsManager approvalQueueDetailsManager;

	@PostMapping("/approvalQueueDetails")
	public Map<String,Object> createApprovalQueueDetails(HttpServletRequest request, @RequestBody ApprovalQueueDetails approvalQueueDetails) {
		
		

		return approvalQueueDetailsManager.create(request,approvalQueueDetails);
	}
	@GetMapping("/approvalQueueDetails/{id}")
	public Map<String,Object> getApprovalQueueDetails(HttpServletRequest request, @PathVariable Long id) {
		return approvalQueueDetailsManager.getApprovalQueueDetailsForEdit(request,id);
	}
	@GetMapping(value = "/approvalQueueDetailss")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return approvalQueueDetailsManager.getApprovalQueueDetailss(request);
	}
	@PutMapping("/approvalQueueDetails")
	public Map<String,Object> updateApprovalQueueDetails(HttpServletRequest request, @RequestBody ApprovalQueueDetails approvalQueueDetails) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = approvalQueueDetailsManager.getApprovalQueueDetails(request,approvalQueueDetails.getId());
			ApprovalQueueDetails newApprovalQueueDetails = (ApprovalQueueDetails)resultMap.get("approvalQueueDetails");
			newApprovalQueueDetails.setApproverId(approvalQueueDetails.getApproverId());
newApprovalQueueDetails.setLevel(approvalQueueDetails.getLevel());
newApprovalQueueDetails.setIsLastApprover(approvalQueueDetails.getIsLastApprover());
newApprovalQueueDetails.setIsApproved(approvalQueueDetails.getIsApproved());

			return approvalQueueDetailsManager.update(request,newApprovalQueueDetails);
		}
		
	}
	@DeleteMapping("/approvalQueueDetails/{id}")
	public Map<String,Object> deleteApprovalQueueDetails(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return approvalQueueDetailsManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/approvalQueueDetails/saveMultipleApprovalQueueDetailsData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleApprovalQueueDetailsData( HttpServletRequest request, @RequestBody List<ApprovalQueueDetails> approvalQueueDetailss) {
		return approvalQueueDetailsManager.saveMultipleApprovalQueueDetailsData(request,approvalQueueDetailss);
	}
	@RequestMapping(value = "/approvalQueueDetails/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return approvalQueueDetailsManager.remove(request,id);
	}
	@RequestMapping(value = "/approvalQueueDetails/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return approvalQueueDetailsManager.removeAll(request);
	}
	@RequestMapping(value = "/approvalQueueDetails/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return approvalQueueDetailsManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/approvalQueueDetails/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return approvalQueueDetailsManager.findPaginatedApprovalQueueDetailssBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/approvalQueueDetails/searchOnApprovalQueueDetailsTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnApprovalQueueDetailsTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return approvalQueueDetailsManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/approvalQueueDetails/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<ApprovalQueueDetails> approvalQueueDetailss = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ApprovalQueueDetails approvalQueueDetails = new ApprovalQueueDetails();
				HSSFRow row = worksheet.getRow(i++);
				//approvalQueueDetails.setField2(row.getCell(1).getStringCellValue());
				approvalQueueDetails.setApproverId(row.getCell(0).getStringCellValue());
approvalQueueDetails.setLevel(Double.valueOf(row.getCell(1).getNumericCellValue()).longValue());
approvalQueueDetails.setIsLastApprover(row.getCell(2).getBooleanCellValue());
approvalQueueDetails.setIsApproved(row.getCell(3).getBooleanCellValue());

				approvalQueueDetailss.add(approvalQueueDetails);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("approvalQueueDetailss", approvalQueueDetailss);
		return returnResult;
	}
	@RequestMapping(value = "/approvalQueueDetails/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<ApprovalQueueDetails> approvalQueueDetailss = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ApprovalQueueDetails approvalQueueDetails = new ApprovalQueueDetails();
				XSSFRow row = worksheet.getRow(i++);
				//approvalQueueDetails.setField2(row.getCell(1).getStringCellValue());
				approvalQueueDetails.setApproverId(row.getCell(0).getStringCellValue());
approvalQueueDetails.setLevel(Double.valueOf(row.getCell(1).getNumericCellValue()).longValue());
approvalQueueDetails.setIsLastApprover(row.getCell(2).getBooleanCellValue());
approvalQueueDetails.setIsApproved(row.getCell(3).getBooleanCellValue());

				approvalQueueDetailss.add(approvalQueueDetails);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("approvalQueueDetailss", approvalQueueDetailss);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/approvalQueueDetails/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<ApprovalQueueDetails> approvalQueueDetailss = new ArrayList<ApprovalQueueDetails>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			approvalQueueDetailss = (List<ApprovalQueueDetails>) resultMap.get("approvalQueueDetailss");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			approvalQueueDetailss = (List<ApprovalQueueDetails>) resultMap.get("approvalQueueDetailss");
		}
		
		if (UtilValidate.isNotEmpty(approvalQueueDetailss) && approvalQueueDetailss.size()>0) {
			approvalQueueDetailss.remove(0);
		}
		returnResult.put("approvalQueueDetailss", approvalQueueDetailss);
		return returnResult;
	}
}
