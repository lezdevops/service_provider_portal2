package app.globalSetup.presentationtier;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.appmodel.UserRole;
import app.globalSetup.businesstier.UserRoleManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
//@Api(name = "Modules UserRole API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class UserRoleAPI {

	@Autowired
	private UserRoleManager userRoleManager;

	@GetMapping(value = "/userRoles")
	//@ApiMethod(description = "Get all entitys from the database")
	public List<UserRole> getAll(HttpServletRequest request) {
		return userRoleManager.getUserRoles(request);
	}

	@PostMapping("/userRole")
	public Map<String,Object> createUserRole(HttpServletRequest request, @RequestBody List<UserRole> userRoles) {
		return userRoleManager.saveMultipleUserRoleData(request,userRoles);
	}


}
