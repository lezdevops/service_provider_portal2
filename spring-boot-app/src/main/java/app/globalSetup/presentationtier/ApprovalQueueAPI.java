package app.globalSetup.presentationtier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.ApprovalQueue;
import app.appmodel.ApprovalQueueDetails;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.ApprovalQueueManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = { "http://localhost:4200", "http://techpoint.pro" }, allowedHeaders = "*")
public class ApprovalQueueAPI {

	@Autowired
	private ApprovalQueueManager approvalQueueManager;

	@GetMapping("/userApprovalQueues")
	public Map<String, Object> getApprovalQueueDetailsByReceiverId(HttpServletRequest request) {
		Map<String, Object> resultMap = new LinkedHashMap<String, Object>();
		List<ApprovalQueueDetails> approvalQueueDetailsList = approvalQueueManager.getApprovalQueueDetailsList(request);
		resultMap.put("approvalQueueDetailsList", approvalQueueDetailsList);
		return resultMap;
	}

	@PostMapping("/approvalQueue")
	public Map<String, Object> createApprovalQueue(HttpServletRequest request,
			@RequestBody ApprovalQueue approvalQueue) {
		return approvalQueueManager.create(request, approvalQueue);
	}

	@GetMapping("/approvalQueue/{id}")
	public Map<String, Object> getApprovalQueue(HttpServletRequest request, @PathVariable Long id) {
		return approvalQueueManager.getApprovalQueueForEdit(request, id);
	}

	@GetMapping(value = "/approvalQueues")
	public Map<String, Object> getAll(HttpServletRequest request) {
		return approvalQueueManager.getApprovalQueues(request);
	}

	@PutMapping("/approvalQueue")
	public Map<String, Object> updateApprovalQueue(HttpServletRequest request,
			@RequestBody ApprovalQueue approvalQueue) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		} else {
			Map<String, Object> resultMap = approvalQueueManager.getApprovalQueue(request, approvalQueue.getId());
			ApprovalQueue newApprovalQueue = (ApprovalQueue) resultMap.get("approvalQueue");
			newApprovalQueue.setIsActive(approvalQueue.getIsActive());
			newApprovalQueue.setAttachment(approvalQueue.getAttachment());
			newApprovalQueue.setApprovalMessage(approvalQueue.getApprovalMessage());

			return approvalQueueManager.update(request, newApprovalQueue);
		}

	}

	@DeleteMapping("/approvalQueue/{id}")
	public Map<String, Object> deleteApprovalQueue(HttpServletRequest request, @PathVariable Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		} else {
			return approvalQueueManager.remove(request, id);
		}
	}

	@RequestMapping(value = "/approvalQueue/saveMultipleApprovalQueueData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> saveMultipleApprovalQueueData(HttpServletRequest request,
			@RequestBody List<ApprovalQueue> approvalQueues) {
		return approvalQueueManager.saveMultipleApprovalQueueData(request, approvalQueues);
	}

	@RequestMapping(value = "/approvalQueue/delete/{id}", method = RequestMethod.POST)
	public Map<String, Object> remove(HttpServletRequest request, @PathVariable Long id) {
		return approvalQueueManager.remove(request, id);
	}

	@RequestMapping(value = "/approvalQueue/removeAll", method = RequestMethod.POST)
	public Map<String, Object> removeAll(HttpServletRequest request) {
		return approvalQueueManager.removeAll(request);
	}

	@RequestMapping(value = "/approvalQueue/get", params = { "page", "size" }, method = RequestMethod.GET)
	public Map<String, Object> findPaginated(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size) {
		return approvalQueueManager.findPaginated(request, page, size);
	}

	@RequestMapping(value = "/approvalQueue/getPaginatedBy", params = { "page", "size",
			"searchToken" }, method = RequestMethod.GET)
	public Map<String, Object> getPaginatedBy(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size, @RequestParam("searchToken") String searchToken) {
		return approvalQueueManager.findPaginatedApprovalQueuesBySearchToken(request, page, size, searchToken);
	}

	@RequestMapping(value = "/approvalQueue/searchOnApprovalQueueTable", method = RequestMethod.GET)
	public Map<String, Object> searchOnApprovalQueueTable(HttpServletRequest request, @RequestParam("id") Long id,
			Pageable pageRequest) {
		return approvalQueueManager.findById(request, id, pageRequest);
	}

	@RequestMapping(value = "/approvalQueue/processExcel2003", method = RequestMethod.POST)
	public Map<String, Object> processExcel2003(HttpServletRequest request,
			@RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		List<ApprovalQueue> approvalQueues = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ApprovalQueue approvalQueue = new ApprovalQueue();
				HSSFRow row = worksheet.getRow(i++);
				// approvalQueue.setField2(row.getCell(1).getStringCellValue());
				approvalQueue.setIsActive(row.getCell(0).getBooleanCellValue());
				approvalQueue.setAttachment(row.getCell(1).getStringCellValue());
				approvalQueue.setApprovalMessage(row.getCell(2).getStringCellValue());

				approvalQueues.add(approvalQueue);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("approvalQueues", approvalQueues);
		return returnResult;
	}

	@RequestMapping(value = "/approvalQueue/processExcel2007", method = RequestMethod.POST)
	public Map<String, Object> processExcel2007(HttpServletRequest request,
			@RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		List<ApprovalQueue> approvalQueues = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ApprovalQueue approvalQueue = new ApprovalQueue();
				XSSFRow row = worksheet.getRow(i++);
				// approvalQueue.setField2(row.getCell(1).getStringCellValue());
				approvalQueue.setIsActive(row.getCell(0).getBooleanCellValue());
				approvalQueue.setAttachment(row.getCell(1).getStringCellValue());
				approvalQueue.setApprovalMessage(row.getCell(2).getStringCellValue());

				approvalQueues.add(approvalQueue);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		returnResult.put("approvalQueues", approvalQueues);
		return returnResult;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/approvalQueue/excelFileUpload")
	@ResponseBody
	public Map<String, Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<ApprovalQueue> approvalQueues = new ArrayList<ApprovalQueue>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String, Object> resultMap = processExcel2007(request, file);
			approvalQueues = (List<ApprovalQueue>) resultMap.get("approvalQueues");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String, Object> resultMap = processExcel2003(request, file);
			approvalQueues = (List<ApprovalQueue>) resultMap.get("approvalQueues");
		}

		if (UtilValidate.isNotEmpty(approvalQueues) && approvalQueues.size() > 0) {
			approvalQueues.remove(0);
		}
		returnResult.put("approvalQueues", approvalQueues);
		return returnResult;
	}
}
