package app.globalSetup.presentationtier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.ApproverAdmin;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.ApproverAdminManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = { "http://localhost:4200", "http://techpoint.pro" }, allowedHeaders = "*")
public class ApproverAdminAPI {

	@Autowired
	private ApproverAdminManager approverAdminManager;

	@PostMapping("/approverAdmin")
	public Map<String, Object> createApproverAdmin(HttpServletRequest request,
			@RequestBody ApproverAdmin approverAdmin) {

		return approverAdminManager.create(request, approverAdmin);
	}

	@GetMapping("/approverAdmin/{id}")
	public Map<String, Object> getApproverAdmin(HttpServletRequest request, @PathVariable Long id) {
		return approverAdminManager.getApproverAdminForEdit(request, id);
	}

	@GetMapping(value = "/approverAdmins")
	public Map<String, Object> getAll(HttpServletRequest request) {
		return approverAdminManager.getApproverAdmins(request);
	}

	@PutMapping("/approverAdmin")
	public Map<String, Object> updateApproverAdmin(HttpServletRequest request,
			@RequestBody ApproverAdmin approverAdmin) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		} else {
			Map<String, Object> resultMap = approverAdminManager.getApproverAdmin(request, approverAdmin.getId());
			ApproverAdmin newApproverAdmin = (ApproverAdmin) resultMap.get("approverAdmin");
			newApproverAdmin.setName(approverAdmin.getName());
			newApproverAdmin.setDesignation(approverAdmin.getDesignation());
			newApproverAdmin.setIsActive(approverAdmin.getIsActive());

			return approverAdminManager.update(request, newApproverAdmin);
		}

	}

	@DeleteMapping("/approverAdmin/{id}")
	public Map<String, Object> deleteApproverAdmin(HttpServletRequest request, @PathVariable Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		} else {
			return approverAdminManager.remove(request, id);
		}
	}

	@RequestMapping(value = "/approverAdmin/saveMultipleApproverAdminData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> saveMultipleApproverAdminData(HttpServletRequest request,
			@RequestBody List<ApproverAdmin> approverAdmins) {
		return approverAdminManager.saveMultipleApproverAdminData(request, approverAdmins);
	}

	@RequestMapping(value = "/approverAdmin/delete/{id}", method = RequestMethod.POST)
	public Map<String, Object> remove(HttpServletRequest request, @PathVariable Long id) {
		return approverAdminManager.remove(request, id);
	}

	@RequestMapping(value = "/approverAdmin/removeAll", method = RequestMethod.POST)
	public Map<String, Object> removeAll(HttpServletRequest request) {
		return approverAdminManager.removeAll(request);
	}

	@RequestMapping(value = "/approverAdmin/get", params = { "page", "size" }, method = RequestMethod.GET)
	public Map<String, Object> findPaginated(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size) {
		return approverAdminManager.findPaginated(request, page, size);
	}

	@RequestMapping(value = "/approverAdmin/getPaginatedBy", params = { "page", "size",
			"searchToken" }, method = RequestMethod.GET)
	public Map<String, Object> getPaginatedBy(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size, @RequestParam("searchToken") String searchToken) {
		return approverAdminManager.findPaginatedApproverAdminsBySearchToken(request, page, size, searchToken);
	}

	@RequestMapping(value = "/approverAdmin/searchOnApproverAdminTable", method = RequestMethod.GET)
	public Map<String, Object> searchOnApproverAdminTable(HttpServletRequest request, @RequestParam("id") Long id,
			Pageable pageRequest) {
		return approverAdminManager.findById(request, id, pageRequest);
	}

	@RequestMapping(value = "/approverAdmin/processExcel2003", method = RequestMethod.POST)
	public Map<String, Object> processExcel2003(HttpServletRequest request,
			@RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		List<ApproverAdmin> approverAdmins = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ApproverAdmin approverAdmin = new ApproverAdmin();
				HSSFRow row = worksheet.getRow(i++);
				// approverAdmin.setField2(row.getCell(1).getStringCellValue());
				approverAdmin.setName(row.getCell(0).getStringCellValue());
				approverAdmin.setDesignation(row.getCell(1).getStringCellValue());
				approverAdmin.setIsActive(row.getCell(2).getBooleanCellValue());

				approverAdmins.add(approverAdmin);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("approverAdmins", approverAdmins);
		return returnResult;
	}

	@RequestMapping(value = "/approverAdmin/processExcel2007", method = RequestMethod.POST)
	public Map<String, Object> processExcel2007(HttpServletRequest request,
			@RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		List<ApproverAdmin> approverAdmins = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ApproverAdmin approverAdmin = new ApproverAdmin();
				XSSFRow row = worksheet.getRow(i++);
				// approverAdmin.setField2(row.getCell(1).getStringCellValue());
				approverAdmin.setName(row.getCell(0).getStringCellValue());
				approverAdmin.setDesignation(row.getCell(1).getStringCellValue());
				approverAdmin.setIsActive(row.getCell(2).getBooleanCellValue());

				approverAdmins.add(approverAdmin);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		returnResult.put("approverAdmins", approverAdmins);
		return returnResult;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/approverAdmin/excelFileUpload")
	@ResponseBody
	public Map<String, Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<ApproverAdmin> approverAdmins = new ArrayList<ApproverAdmin>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String, Object> resultMap = processExcel2007(request, file);
			approverAdmins = (List<ApproverAdmin>) resultMap.get("approverAdmins");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String, Object> resultMap = processExcel2003(request, file);
			approverAdmins = (List<ApproverAdmin>) resultMap.get("approverAdmins");
		}

		if (UtilValidate.isNotEmpty(approverAdmins) && approverAdmins.size() > 0) {
			approverAdmins.remove(0);
		}
		returnResult.put("approverAdmins", approverAdmins);
		return returnResult;
	}
}
