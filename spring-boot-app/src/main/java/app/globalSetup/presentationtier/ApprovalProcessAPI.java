package app.globalSetup.presentationtier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.ApprovalProcess;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.ApprovalProcessManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class ApprovalProcessAPI {
	
	@Autowired
	private ApprovalProcessManager approvalProcessManager;

	@PostMapping("/approvalProcess")
	public Map<String,Object> createApprovalProcess(HttpServletRequest request, @RequestBody ApprovalProcess approvalProcess) {
		
		

		return approvalProcessManager.create(request,approvalProcess);
	}
	@GetMapping("/approvalProcess/{id}")
	public Map<String,Object> getApprovalProcess(HttpServletRequest request, @PathVariable Long id) {
		return approvalProcessManager.getApprovalProcessForEdit(request,id);
	}
	@GetMapping(value = "/approvalProcesss")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return approvalProcessManager.getApprovalProcesss(request);
	}
	@PutMapping("/approvalProcess")
	public Map<String,Object> updateApprovalProcess(HttpServletRequest request, @RequestBody ApprovalProcess approvalProcess) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = approvalProcessManager.getApprovalProcess(request,approvalProcess.getId());
			ApprovalProcess newApprovalProcess = (ApprovalProcess)resultMap.get("approvalProcess");
			newApprovalProcess.setProcessName(approvalProcess.getProcessName());
newApprovalProcess.setIsActive(approvalProcess.getIsActive());
newApprovalProcess.setCreatedBy(approvalProcess.getCreatedBy());
newApprovalProcess.setCreated(approvalProcess.getCreated());

			return approvalProcessManager.update(request,newApprovalProcess);
		}
		
	}
	@DeleteMapping("/approvalProcess/{id}")
	public Map<String,Object> deleteApprovalProcess(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return approvalProcessManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/approvalProcess/saveMultipleApprovalProcessData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleApprovalProcessData( HttpServletRequest request, @RequestBody List<ApprovalProcess> approvalProcesss) {
		return approvalProcessManager.saveMultipleApprovalProcessData(request,approvalProcesss);
	}
	@RequestMapping(value = "/approvalProcess/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return approvalProcessManager.remove(request,id);
	}
	@RequestMapping(value = "/approvalProcess/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return approvalProcessManager.removeAll(request);
	}
	@RequestMapping(value = "/approvalProcess/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return approvalProcessManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/approvalProcess/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return approvalProcessManager.findPaginatedApprovalProcesssBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/approvalProcess/searchOnApprovalProcessTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnApprovalProcessTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return approvalProcessManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/approvalProcess/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<ApprovalProcess> approvalProcesss = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ApprovalProcess approvalProcess = new ApprovalProcess();
				HSSFRow row = worksheet.getRow(i++);
				//approvalProcess.setField2(row.getCell(1).getStringCellValue());
				approvalProcess.setProcessName(row.getCell(0).getStringCellValue());
approvalProcess.setIsActive(row.getCell(1).getStringCellValue());
approvalProcess.setCreatedBy(row.getCell(2).getStringCellValue());
approvalProcess.setCreated(HelperUtils.getTimestampFromString(row.getCell(3).getStringCellValue()));

				approvalProcesss.add(approvalProcess);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("approvalProcesss", approvalProcesss);
		return returnResult;
	}
	@RequestMapping(value = "/approvalProcess/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<ApprovalProcess> approvalProcesss = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				ApprovalProcess approvalProcess = new ApprovalProcess();
				XSSFRow row = worksheet.getRow(i++);
				//approvalProcess.setField2(row.getCell(1).getStringCellValue());
				approvalProcess.setProcessName(row.getCell(0).getStringCellValue());
approvalProcess.setIsActive(row.getCell(1).getStringCellValue());
approvalProcess.setCreatedBy(row.getCell(2).getStringCellValue());
approvalProcess.setCreated(HelperUtils.getTimestampFromString(row.getCell(3).getStringCellValue()));

				approvalProcesss.add(approvalProcess);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("approvalProcesss", approvalProcesss);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/approvalProcess/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<ApprovalProcess> approvalProcesss = new ArrayList<ApprovalProcess>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			approvalProcesss = (List<ApprovalProcess>) resultMap.get("approvalProcesss");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			approvalProcesss = (List<ApprovalProcess>) resultMap.get("approvalProcesss");
		}
		
		if (UtilValidate.isNotEmpty(approvalProcesss) && approvalProcesss.size()>0) {
			approvalProcesss.remove(0);
		}
		returnResult.put("approvalProcesss", approvalProcesss);
		return returnResult;
	}
}
