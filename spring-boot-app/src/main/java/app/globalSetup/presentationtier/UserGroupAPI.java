package app.globalSetup.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.UserGroup;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.UserGroupManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class UserGroupAPI {
	
	@Autowired
	private UserGroupManager userGroupManager;

	@PostMapping("/userGroup")
	public Map<String,Object> createUserGroup(HttpServletRequest request, @RequestBody UserGroup userGroup) {
		
		userGroup.setCreated(null);userGroup.setUpdated(null);

		return userGroupManager.create(request,userGroup);
	}
	@GetMapping("/userGroup/{id}")
	public Map<String,Object> getUserGroup(HttpServletRequest request, @PathVariable Long id) {
		return userGroupManager.getUserGroupForEdit(request,id);
	}
	@GetMapping(value = "/userGroups")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return userGroupManager.getUserGroups(request);
	}
	@PutMapping("/userGroup")
	public Map<String,Object> updateUserGroup(HttpServletRequest request, @RequestBody UserGroup userGroup) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = userGroupManager.getUserGroup(request,userGroup.getId());
			UserGroup newUserGroup = (UserGroup)resultMap.get("userGroup");
			newUserGroup.setName(userGroup.getName());
newUserGroup.setMetaData(userGroup.getMetaData());
newUserGroup.setWebsiteAddress(userGroup.getWebsiteAddress());
newUserGroup.setPhoneNo(userGroup.getPhoneNo());
newUserGroup.setEmail(userGroup.getEmail());
newUserGroup.setAddress(userGroup.getAddress());
newUserGroup.setCurrency(userGroup.getCurrency());
newUserGroup.setImageAddress(userGroup.getImageAddress());

			return userGroupManager.update(request,newUserGroup);
		}
		
	}
	@DeleteMapping("/userGroup/{id}")
	public Map<String,Object> deleteUserGroup(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return userGroupManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/userGroup/saveMultipleUserGroupData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleUserGroupData( HttpServletRequest request, @RequestBody List<UserGroup> userGroups) {
		return userGroupManager.saveMultipleUserGroupData(request,userGroups);
	}
	@RequestMapping(value = "/userGroup/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return userGroupManager.remove(request,id);
	}
	@RequestMapping(value = "/userGroup/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return userGroupManager.removeAll(request);
	}
	@RequestMapping(value = "/userGroup/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return userGroupManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/userGroup/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return userGroupManager.findPaginatedUserGroupsBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/userGroup/searchOnUserGroupTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnUserGroupTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return userGroupManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/userGroup/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<UserGroup> userGroups = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				UserGroup userGroup = new UserGroup();
				HSSFRow row = worksheet.getRow(i++);
				//userGroup.setField2(row.getCell(1).getStringCellValue());
				userGroup.setName(row.getCell(0).getStringCellValue());
userGroup.setMetaData(row.getCell(1).getStringCellValue());
userGroup.setWebsiteAddress(row.getCell(2).getStringCellValue());
userGroup.setPhoneNo(row.getCell(3).getStringCellValue());
userGroup.setEmail(row.getCell(4).getStringCellValue());
userGroup.setAddress(row.getCell(5).getStringCellValue());
userGroup.setCurrency(row.getCell(6).getStringCellValue());
userGroup.setImageAddress(row.getCell(7).getStringCellValue());
userGroup.setCreated(HelperUtils.getTimestampFromString(row.getCell(8).getStringCellValue()));
userGroup.setUpdated(HelperUtils.getTimestampFromString(row.getCell(9).getStringCellValue()));

				userGroups.add(userGroup);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("userGroups", userGroups);
		return returnResult;
	}
	@RequestMapping(value = "/userGroup/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<UserGroup> userGroups = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				UserGroup userGroup = new UserGroup();
				XSSFRow row = worksheet.getRow(i++);
				//userGroup.setField2(row.getCell(1).getStringCellValue());
				userGroup.setName(row.getCell(0).getStringCellValue());
userGroup.setMetaData(row.getCell(1).getStringCellValue());
userGroup.setWebsiteAddress(row.getCell(2).getStringCellValue());
userGroup.setPhoneNo(row.getCell(3).getStringCellValue());
userGroup.setEmail(row.getCell(4).getStringCellValue());
userGroup.setAddress(row.getCell(5).getStringCellValue());
userGroup.setCurrency(row.getCell(6).getStringCellValue());
userGroup.setImageAddress(row.getCell(7).getStringCellValue());
userGroup.setCreated(HelperUtils.getTimestampFromString(row.getCell(8).getStringCellValue()));
userGroup.setUpdated(HelperUtils.getTimestampFromString(row.getCell(9).getStringCellValue()));

				userGroups.add(userGroup);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("userGroups", userGroups);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/userGroup/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<UserGroup> userGroups = new ArrayList<UserGroup>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			userGroups = (List<UserGroup>) resultMap.get("userGroups");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			userGroups = (List<UserGroup>) resultMap.get("userGroups");
		}
		
		if (UtilValidate.isNotEmpty(userGroups) && userGroups.size()>0) {
			userGroups.remove(0);
		}
		returnResult.put("userGroups", userGroups);
		return returnResult;
	}
}
