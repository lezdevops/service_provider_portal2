package app.globalSetup.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.Permission;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.PermissionManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class PermissionAPI {
	@Autowired
	private PermissionManager permissionManager;

	@PostMapping("/permission")
	public Map<String,Object> createPermission(HttpServletRequest request, @RequestBody Permission permission) {
		permission.setCreatedDate(null);

		return permissionManager.create(request,permission);
	}
	@GetMapping("/permission/{id}")
	public Map<String,Object> getPermission(HttpServletRequest request, @PathVariable Long id) {
		return permissionManager.getPermissionForEdit(request,id);
	}
	@GetMapping(value = "/permissions")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return permissionManager.getPermissions(request);
	}
	@PutMapping("/permission")
	public Map<String,Object> updatePermission(HttpServletRequest request, @RequestBody Permission permission) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = permissionManager.getPermission(request,permission.getId());
			Permission newPermission = (Permission)resultMap.get("permission");
			
			return permissionManager.update(request,newPermission);
		}
		
	}
	@DeleteMapping("/permission/{id}")
	public Map<String,Object> deletePermission(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return permissionManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/permission/saveMultiplePermissionData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultiplePermissionData( HttpServletRequest request, @RequestBody List<Permission> permissions) {
		return permissionManager.saveMultiplePermissionData(request,permissions);
	}
	@RequestMapping(value = "/permission/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return permissionManager.remove(request,id);
	}
	@RequestMapping(value = "/permission/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return permissionManager.removeAll(request);
	}
	@RequestMapping(value = "/permission/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return permissionManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/permission/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return permissionManager.findPaginatedPermissionsBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/permission/searchOnPermissionTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnPermissionTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return permissionManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/permission/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<Permission> permissions = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Permission permission = new Permission();
				HSSFRow row = worksheet.getRow(i++);
				//permission.setField2(row.getCell(1).getStringCellValue());
				permission.setName(row.getCell(0).getStringCellValue());
permission.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				permissions.add(permission);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("permissions", permissions);
		return returnResult;
	}
	@RequestMapping(value = "/permission/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<Permission> permissions = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Permission permission = new Permission();
				XSSFRow row = worksheet.getRow(i++);
				//permission.setField2(row.getCell(1).getStringCellValue());
				permission.setName(row.getCell(0).getStringCellValue());
permission.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				permissions.add(permission);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("permissions", permissions);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/permission/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<Permission> permissions = new ArrayList<Permission>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			permissions = (List<Permission>) resultMap.get("permissions");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			permissions = (List<Permission>) resultMap.get("permissions");
		}
		
		if (UtilValidate.isNotEmpty(permissions) && permissions.size()>0) {
			permissions.remove(0);
		}
		returnResult.put("permissions", permissions);
		return returnResult;
	}
}
