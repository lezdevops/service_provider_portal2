package app.globalSetup.presentationtier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.RegistrationQueue;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.RegistrationQueueManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = { "http://localhost:4200", "http://techpoint.pro" }, allowedHeaders = "*")
public class RegistrationQueueAPI {

	@Autowired
	private RegistrationQueueManager registrationQueueManager;

	@PostMapping("/registrationQueue")
	public Map<String, Object> createRegistrationQueue(HttpServletRequest request,
			@RequestBody RegistrationQueue registrationQueue) {

		registrationQueue.setCreatedDate(null);

		return registrationQueueManager.create(request, registrationQueue);
	}
	
	@PostMapping("/approveRegistrationQueue")
	public Map<String, Object> approveRegistrationQueue(HttpServletRequest request,
			@RequestBody RegistrationQueue registrationQueue) {

		registrationQueue.setCreatedDate(null);

		return registrationQueueManager.approveRegistrationQueue(request, registrationQueue);
	}

	@GetMapping("/registrationQueue/{id}")
	public Map<String, Object> getRegistrationQueue(HttpServletRequest request, @PathVariable Long id) {
		return registrationQueueManager.getRegistrationQueueForEdit(request, id);
	}

	@GetMapping(value = "/registrationQueues")
	public Map<String, Object> getAll(HttpServletRequest request) {
		return registrationQueueManager.getRegistrationQueues(request);
	}

	@PutMapping("/registrationQueue")
	public Map<String, Object> updateRegistrationQueue(HttpServletRequest request,
			@RequestBody RegistrationQueue registrationQueue) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		} else {
			Map<String, Object> resultMap = registrationQueueManager.getRegistrationQueue(request,
					registrationQueue.getId());
			RegistrationQueue newRegistrationQueue = (RegistrationQueue) resultMap.get("registrationQueue");
			newRegistrationQueue.setEmail(registrationQueue.getEmail());
			newRegistrationQueue.setUserName(registrationQueue.getUserName());
			newRegistrationQueue.setPassword(registrationQueue.getPassword());
			newRegistrationQueue.setSecurityNumber(registrationQueue.getSecurityNumber());
			newRegistrationQueue.setSecurityQuestion(registrationQueue.getSecurityQuestion());
			newRegistrationQueue.setAnswer(registrationQueue.getAnswer());
			newRegistrationQueue.setAccountType(registrationQueue.getAccountType());
			newRegistrationQueue.setJsonString(registrationQueue.getJsonString());
			newRegistrationQueue.setCreatedDate(registrationQueue.getCreatedDate());
			newRegistrationQueue.setApprovalStatus(registrationQueue.getApprovalStatus());

			return registrationQueueManager.update(request, newRegistrationQueue);
		}

	}

	@DeleteMapping("/registrationQueue/{id}")
	public Map<String, Object> deleteRegistrationQueue(HttpServletRequest request, @PathVariable Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		} else {
			return registrationQueueManager.remove(request, id);
		}
	}


	@RequestMapping(value = "/registrationQueue/saveMultipleRegistrationQueueData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> saveMultipleRegistrationQueueData(HttpServletRequest request,
			@RequestBody List<RegistrationQueue> registrationQueues) {
		return registrationQueueManager.saveMultipleRegistrationQueueData(request, registrationQueues);
	}

	@RequestMapping(value = "/registrationQueue/delete/{id}", method = RequestMethod.POST)
	public Map<String, Object> remove(HttpServletRequest request, @PathVariable Long id) {
		return registrationQueueManager.remove(request, id);
	}

	@RequestMapping(value = "/registrationQueue/removeAll", method = RequestMethod.POST)
	public Map<String, Object> removeAll(HttpServletRequest request) {
		return registrationQueueManager.removeAll(request);
	}

	@RequestMapping(value = "/registrationQueue/get", params = { "page", "size" }, method = RequestMethod.GET)
	public Map<String, Object> findPaginated(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size) {
		return registrationQueueManager.findPaginated(request, page, size);
	}

	@RequestMapping(value = "/registrationQueue/getPaginatedBy", params = { "page", "size",
			"searchToken" }, method = RequestMethod.GET)
	public Map<String, Object> getPaginatedBy(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size, @RequestParam("searchToken") String searchToken) {
		return registrationQueueManager.findPaginatedRegistrationQueuesBySearchToken(request, page, size, searchToken);
	}

	@RequestMapping(value = "/registrationQueue/searchOnRegistrationQueueTable", method = RequestMethod.GET)
	public Map<String, Object> searchOnRegistrationQueueTable(HttpServletRequest request, @RequestParam("id") Long id,
			Pageable pageRequest) {
		return registrationQueueManager.findById(request, id, pageRequest);
	}

	@RequestMapping(value = "/registrationQueue/processExcel2003", method = RequestMethod.POST)
	public Map<String, Object> processExcel2003(HttpServletRequest request,
			@RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		List<RegistrationQueue> registrationQueues = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				RegistrationQueue registrationQueue = new RegistrationQueue();
				HSSFRow row = worksheet.getRow(i++);
				// registrationQueue.setField2(row.getCell(1).getStringCellValue());
				registrationQueue.setEmail(row.getCell(0).getStringCellValue());
				registrationQueue.setUserName(row.getCell(1).getStringCellValue());
				registrationQueue.setPassword(row.getCell(2).getStringCellValue());
				registrationQueue.setSecurityNumber(row.getCell(3).getStringCellValue());
				registrationQueue.setSecurityQuestion(row.getCell(4).getStringCellValue());
				registrationQueue.setAnswer(row.getCell(5).getStringCellValue());
				registrationQueue.setAccountType(row.getCell(6).getStringCellValue());
				registrationQueue.setJsonString(row.getCell(7).getStringCellValue());
				registrationQueue
						.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(8).getStringCellValue()));
				registrationQueue.setApprovalStatus(row.getCell(9).getStringCellValue());

				registrationQueues.add(registrationQueue);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("registrationQueues", registrationQueues);
		return returnResult;
	}

	@RequestMapping(value = "/registrationQueue/processExcel2007", method = RequestMethod.POST)
	public Map<String, Object> processExcel2007(HttpServletRequest request,
			@RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		List<RegistrationQueue> registrationQueues = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				RegistrationQueue registrationQueue = new RegistrationQueue();
				XSSFRow row = worksheet.getRow(i++);
				// registrationQueue.setField2(row.getCell(1).getStringCellValue());
				registrationQueue.setEmail(row.getCell(0).getStringCellValue());
				registrationQueue.setUserName(row.getCell(1).getStringCellValue());
				registrationQueue.setPassword(row.getCell(2).getStringCellValue());
				registrationQueue.setSecurityNumber(row.getCell(3).getStringCellValue());
				registrationQueue.setSecurityQuestion(row.getCell(4).getStringCellValue());
				registrationQueue.setAnswer(row.getCell(5).getStringCellValue());
				registrationQueue.setAccountType(row.getCell(6).getStringCellValue());
				registrationQueue.setJsonString(row.getCell(7).getStringCellValue());
				registrationQueue
						.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(8).getStringCellValue()));
				registrationQueue.setApprovalStatus(row.getCell(9).getStringCellValue());

				registrationQueues.add(registrationQueue);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		returnResult.put("registrationQueues", registrationQueues);
		return returnResult;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/registrationQueue/excelFileUpload")
	@ResponseBody
	public Map<String, Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<RegistrationQueue> registrationQueues = new ArrayList<RegistrationQueue>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String, Object> resultMap = processExcel2007(request, file);
			registrationQueues = (List<RegistrationQueue>) resultMap.get("registrationQueues");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String, Object> resultMap = processExcel2003(request, file);
			registrationQueues = (List<RegistrationQueue>) resultMap.get("registrationQueues");
		}

		if (UtilValidate.isNotEmpty(registrationQueues) && registrationQueues.size() > 0) {
			registrationQueues.remove(0);
		}
		returnResult.put("registrationQueues", registrationQueues);
		return returnResult;
	}
}
