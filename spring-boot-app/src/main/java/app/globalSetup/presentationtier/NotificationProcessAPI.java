package app.globalSetup.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.NotificationProcess;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.NotificationProcessManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class NotificationProcessAPI {
	
	@Autowired
	private NotificationProcessManager notificationProcessManager;

	@PostMapping("/notificationProcess")
	public Map<String,Object> createNotificationProcess(HttpServletRequest request, @RequestBody NotificationProcess notificationProcess) {
		
		

		return notificationProcessManager.create(request,notificationProcess);
	}
	@GetMapping("/notificationProcess/{id}")
	public Map<String,Object> getNotificationProcess(HttpServletRequest request, @PathVariable Long id) {
		return notificationProcessManager.getNotificationProcessForEdit(request,id);
	}
	@GetMapping(value = "/notificationProcesss")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return notificationProcessManager.getNotificationProcesss(request);
	}
	@PutMapping("/notificationProcess")
	public Map<String,Object> updateNotificationProcess(HttpServletRequest request, @RequestBody NotificationProcess notificationProcess) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = notificationProcessManager.getNotificationProcess(request,notificationProcess.getId());
			NotificationProcess newNotificationProcess = (NotificationProcess)resultMap.get("notificationProcess");
			newNotificationProcess.setName(notificationProcess.getName());
newNotificationProcess.setIsActive(notificationProcess.getIsActive());
newNotificationProcess.setSendEmail(notificationProcess.getSendEmail());
newNotificationProcess.setSendSms(notificationProcess.getSendSms());

			return notificationProcessManager.update(request,newNotificationProcess);
		}
		
	}
	@DeleteMapping("/notificationProcess/{id}")
	public Map<String,Object> deleteNotificationProcess(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return notificationProcessManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/notificationProcess/saveMultipleNotificationProcessData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleNotificationProcessData( HttpServletRequest request, @RequestBody List<NotificationProcess> notificationProcesss) {
		return notificationProcessManager.saveMultipleNotificationProcessData(request,notificationProcesss);
	}
	@RequestMapping(value = "/notificationProcess/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return notificationProcessManager.remove(request,id);
	}
	@RequestMapping(value = "/notificationProcess/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return notificationProcessManager.removeAll(request);
	}
	@RequestMapping(value = "/notificationProcess/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return notificationProcessManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/notificationProcess/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return notificationProcessManager.findPaginatedNotificationProcesssBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/notificationProcess/searchOnNotificationProcessTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnNotificationProcessTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return notificationProcessManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/notificationProcess/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<NotificationProcess> notificationProcesss = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				NotificationProcess notificationProcess = new NotificationProcess();
				HSSFRow row = worksheet.getRow(i++);
				//notificationProcess.setField2(row.getCell(1).getStringCellValue());
				notificationProcess.setName(row.getCell(0).getStringCellValue());
notificationProcess.setIsActive(row.getCell(1).getBooleanCellValue());
notificationProcess.setSendEmail(row.getCell(2).getBooleanCellValue());
notificationProcess.setSendSms(row.getCell(3).getBooleanCellValue());

				notificationProcesss.add(notificationProcess);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("notificationProcesss", notificationProcesss);
		return returnResult;
	}
	@RequestMapping(value = "/notificationProcess/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<NotificationProcess> notificationProcesss = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				NotificationProcess notificationProcess = new NotificationProcess();
				XSSFRow row = worksheet.getRow(i++);
				//notificationProcess.setField2(row.getCell(1).getStringCellValue());
				notificationProcess.setName(row.getCell(0).getStringCellValue());
notificationProcess.setIsActive(row.getCell(1).getBooleanCellValue());
notificationProcess.setSendEmail(row.getCell(2).getBooleanCellValue());
notificationProcess.setSendSms(row.getCell(3).getBooleanCellValue());

				notificationProcesss.add(notificationProcess);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("notificationProcesss", notificationProcesss);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/notificationProcess/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<NotificationProcess> notificationProcesss = new ArrayList<NotificationProcess>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			notificationProcesss = (List<NotificationProcess>) resultMap.get("notificationProcesss");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			notificationProcesss = (List<NotificationProcess>) resultMap.get("notificationProcesss");
		}
		
		if (UtilValidate.isNotEmpty(notificationProcesss) && notificationProcesss.size()>0) {
			notificationProcesss.remove(0);
		}
		returnResult.put("notificationProcesss", notificationProcesss);
		return returnResult;
	}
}
