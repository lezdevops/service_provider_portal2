package app.globalSetup.presentationtier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.User;
import app.common.persistencetier.UserService;
import app.framework.businesstier.HelperUtils;
import app.globalSetup.businesstier.UserManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = { "http://localhost:4200", "http://techpoint.pro" }, allowedHeaders = "*")
//@Api(name = "Modules User API", description = "Provides a list of methods that manage hotel bookings", stage = ApiStage.RC)
public class UserAPI {

	@Autowired
	private UserManager userManager;
	

	@PostMapping("/user")
	public Map<String, Object> createUser(HttpServletRequest request, @RequestBody User user) {
		
		return userManager.save(request, user);
	}

	@GetMapping("/user/{id}")
	public User getUser(HttpServletRequest request, @PathVariable Long id) {
		return userManager.findOne(request, id);
	}

	@GetMapping(value = "/users")
	// @ApiMethod(description = "Get all entitys from the database")
	public List<User> getAll(HttpServletRequest request) {
		return userManager.getUsers(request);
	}

	@PutMapping("/user")
	public Map<String, Object> updateUser(HttpServletRequest request, @RequestBody User user) {
		boolean isReadonly = false;
		if (isReadonly) {
			return null;
		} else {
			User newUser = getUser(request, user.getId());
			newUser.setPasswordHint(user.getPasswordHint());
			newUser.setEmail(user.getEmail());
			newUser.setFirstName(user.getFirstName());
			newUser.setLastName(user.getLastName());
			newUser.setActive(user.getActive());
			newUser.setCreatedBy(user.getCreatedBy());
			newUser.setCreated(user.getCreated());
			newUser.setUpdated(user.getUpdated());

			return userManager.save(request, newUser);
		}

	}

	@DeleteMapping("/user/{id}")
	public boolean deleteUser(HttpServletRequest request, @PathVariable Long id) {
		boolean isReadonly = false;
		if (isReadonly) {
			return false;
		} else {
			userManager.remove(request, id);
			return true;
		}
	}


	@RequestMapping(value = "/user/saveMultipleUserData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	// @ApiMethod(description = "Save's a hotel booking and save it to the
	// database")
	@ResponseBody
	public List<User> saveMultipleUserData(HttpServletRequest request, @RequestBody List<User> users) {
		return userManager.saveMultipleUserData(request, users);
	}

	@RequestMapping(value = "/user/delete/{id}", method = RequestMethod.POST)
	// @ApiMethod(description = "Remove the entity database with the provided ID
	// from the database")
	public Map<String, Object> remove(HttpServletRequest request, @PathVariable Long id) {
		Map<String, Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("message", "");
		String message = userManager.remove(request, id);
		if (message.contains("Error")) {
			messageMap.put("type", "error");
		} else {
			messageMap.put("type", "success");
		}
		messageMap.put("deleteMessage", message);
		return messageMap;
	}

	@RequestMapping(value = "/user/removeAll", method = RequestMethod.POST)
	public String removeAll(HttpServletRequest request) {
		return userManager.removeAll(request);
	}

	@RequestMapping(value = "/user/get", params = { "page", "size" }, method = RequestMethod.GET)
	public Page<User> findPaginated(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size) {

		Page<User> resultPage = userManager.findPaginated(request, page, size);
		// if (page > resultPage.getTotalPages()) {
		// throw new MyResourceNotFoundException();
		// }

		return resultPage;
	}

	@RequestMapping(value = "/user/getPaginatedBy", params = { "page", "size",
			"searchToken" }, method = RequestMethod.GET)
	public Page<User> getPaginatedBy(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size, @RequestParam("searchToken") String searchToken) {

		Page<User> resultPage = userManager.findPaginatedUsersBySearchToken(request, page, size, searchToken);
		if (page > resultPage.getTotalPages()) {
			// throw new MyResourceNotFoundException();
		}

		return resultPage;
	}

	@RequestMapping(value = "/user/searchOnUserTable", method = RequestMethod.GET)
	public Page<User> searchOnUserTable(HttpServletRequest request, @RequestParam("id") Long id, Pageable pageRequest) {
		return userManager.findById(request, id, pageRequest);
	}

	@RequestMapping(value = "/user/processExcel2003", method = RequestMethod.POST)
	public List<User> processExcel2003(HttpServletRequest request,
			@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<User> users = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				User user = new User();
				HSSFRow row = worksheet.getRow(i++);
				// user.setField2(row.getCell(1).getStringCellValue());
				user.setUserName(row.getCell(0).getStringCellValue());
				user.setPassword(row.getCell(1).getStringCellValue());
				user.setPasswordHint(row.getCell(2).getStringCellValue());
				user.setEmail(row.getCell(3).getStringCellValue());
				user.setFirstName(row.getCell(4).getStringCellValue());
				user.setLastName(row.getCell(5).getStringCellValue());
				user.setActive(Double.valueOf(row.getCell(6).getNumericCellValue()).longValue());
				user.setCreatedBy(row.getCell(7).getStringCellValue());
				user.setCreated(HelperUtils.getTimestampFromString(row.getCell(8).getStringCellValue()));
				user.setUpdated(HelperUtils.getTimestampFromString(row.getCell(9).getStringCellValue()));

				users.add(user);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return users;
	}

	@RequestMapping(value = "/user/processExcel2007", method = RequestMethod.POST)
	public List<User> processExcel2007(HttpServletRequest request,
			@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<User> users = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				User user = new User();
				XSSFRow row = worksheet.getRow(i++);
				// user.setField2(row.getCell(1).getStringCellValue());
				user.setUserName(row.getCell(0).getStringCellValue());
				user.setPassword(row.getCell(1).getStringCellValue());
				user.setPasswordHint(row.getCell(2).getStringCellValue());
				user.setEmail(row.getCell(3).getStringCellValue());
				user.setFirstName(row.getCell(4).getStringCellValue());
				user.setLastName(row.getCell(5).getStringCellValue());
				user.setActive(Double.valueOf(row.getCell(6).getNumericCellValue()).longValue());
				user.setCreatedBy(row.getCell(7).getStringCellValue());
				user.setCreated(HelperUtils.getTimestampFromString(row.getCell(8).getStringCellValue()));
				user.setUpdated(HelperUtils.getTimestampFromString(row.getCell(9).getStringCellValue()));

				users.add(user);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return users;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/user/excelFileUpload")
	@ResponseBody
	public List<User> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<User> users = new ArrayList<User>();
		if (extension.equalsIgnoreCase("xlsx")) {
			users = processExcel2007(request, file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			users = processExcel2003(request, file);
		}

		if (users.size() > 0) {
			users.remove(0);
		}
		return users;
	}
}
