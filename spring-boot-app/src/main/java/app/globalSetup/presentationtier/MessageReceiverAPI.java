package app.globalSetup.presentationtier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import app.framework.businesstier.ConfigUtility;

import javax.servlet.http.HttpServletRequest;

import app.framework.businesstier.ConfigUtility;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.MessageReceiver;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.MessageReceiverManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins= {"http://localhost:4200","http://techpoint.pro"}, allowedHeaders="*")
public class MessageReceiverAPI {
	
	@Autowired
	private MessageReceiverManager messageReceiverManager;

	@PostMapping("/messageReceiver")
	public Map<String,Object> createMessageReceiver(HttpServletRequest request, @RequestBody MessageReceiver messageReceiver) {
		
		

		return messageReceiverManager.create(request,messageReceiver);
	}
	@GetMapping("/messageReceiver/{id}")
	public Map<String,Object> getMessageReceiver(HttpServletRequest request, @PathVariable Long id) {
		return messageReceiverManager.getMessageReceiverForEdit(request,id);
	}
	@GetMapping(value = "/messageReceivers")
	public Map<String,Object> getAll(HttpServletRequest request) {
		return messageReceiverManager.getMessageReceivers(request);
	}
	@PutMapping("/messageReceiver")
	public Map<String,Object> updateMessageReceiver(HttpServletRequest request, @RequestBody MessageReceiver messageReceiver) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("updateMessage", "The Record is Update Rectricted.");
			return returnResult;
		}else {
			Map<String,Object> resultMap = messageReceiverManager.getMessageReceiver(request,messageReceiver.getId());
			MessageReceiver newMessageReceiver = (MessageReceiver)resultMap.get("messageReceiver");
			newMessageReceiver.setName(messageReceiver.getName());
newMessageReceiver.setDesignation(messageReceiver.getDesignation());
newMessageReceiver.setIsActive(messageReceiver.getIsActive());

			return messageReceiverManager.update(request,newMessageReceiver);
		}
		
	}
	@DeleteMapping("/messageReceiver/{id}")
	public Map<String,Object> deleteMessageReceiver(HttpServletRequest request, @PathVariable Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			returnResult.put("deleteMessage", "The Record is Delete Restricted.");
			return returnResult;
		}else {		
			return messageReceiverManager.remove(request,id);
		}
	}
	
	@RequestMapping(value = "/messageReceiver/saveMultipleMessageReceiverData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,Object> saveMultipleMessageReceiverData( HttpServletRequest request, @RequestBody List<MessageReceiver> messageReceivers) {
		return messageReceiverManager.saveMultipleMessageReceiverData(request,messageReceivers);
	}
	@RequestMapping(value = "/messageReceiver/delete/{id}", method = RequestMethod.POST)
	public Map<String,Object> remove( HttpServletRequest request, @PathVariable Long id) {
		return messageReceiverManager.remove(request,id);
	}
	@RequestMapping(value = "/messageReceiver/removeAll", method = RequestMethod.POST)
	public Map<String,Object> removeAll(HttpServletRequest request) {
		return messageReceiverManager.removeAll(request);
	}
	@RequestMapping(value = "/messageReceiver/get", params = { "page", "size" },  method = RequestMethod.GET)
	public Map<String,Object> findPaginated( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size) {
	    return messageReceiverManager.findPaginated(request,page, size);
	 }
	@RequestMapping(value = "/messageReceiver/getPaginatedBy", params = { "page", "size", "searchToken" },  method = RequestMethod.GET)
	public Map<String,Object> getPaginatedBy( HttpServletRequest request, @RequestParam("page") int page, @RequestParam("size") int size, 
			@RequestParam("searchToken") String searchToken) {
        return messageReceiverManager.findPaginatedMessageReceiversBySearchToken(request,page, size,searchToken);
	 }
	@RequestMapping(value = "/messageReceiver/searchOnMessageReceiverTable", method = RequestMethod.GET)
    public Map<String,Object> searchOnMessageReceiverTable(HttpServletRequest request, @RequestParam("id") Long id, 
                                          Pageable pageRequest) {
        return messageReceiverManager.findById(request,id, pageRequest);
    }
	@RequestMapping(value = "/messageReceiver/processExcel2003", method = RequestMethod.POST)
	public Map<String,Object> processExcel2003(HttpServletRequest request, @RequestParam("excelfile2003") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<MessageReceiver> messageReceivers = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				MessageReceiver messageReceiver = new MessageReceiver();
				HSSFRow row = worksheet.getRow(i++);
				//messageReceiver.setField2(row.getCell(1).getStringCellValue());
				messageReceiver.setName(row.getCell(0).getStringCellValue());
messageReceiver.setDesignation(row.getCell(1).getStringCellValue());
messageReceiver.setIsActive(row.getCell(2).getBooleanCellValue());

				messageReceivers.add(messageReceiver);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		returnResult.put("messageReceivers", messageReceivers);
		return returnResult;
	}
	@RequestMapping(value = "/messageReceiver/processExcel2007", method = RequestMethod.POST)
	public Map<String,Object> processExcel2007(HttpServletRequest request, @RequestParam("excelfile2007") MultipartFile excelfile) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		List<MessageReceiver> messageReceivers = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				MessageReceiver messageReceiver = new MessageReceiver();
				XSSFRow row = worksheet.getRow(i++);
				//messageReceiver.setField2(row.getCell(1).getStringCellValue());
				messageReceiver.setName(row.getCell(0).getStringCellValue());
messageReceiver.setDesignation(row.getCell(1).getStringCellValue());
messageReceiver.setIsActive(row.getCell(2).getBooleanCellValue());

				messageReceivers.add(messageReceiver);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		returnResult.put("messageReceivers", messageReceivers);
		return returnResult;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/messageReceiver/excelFileUpload")
	@ResponseBody
	public Map<String,Object> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		Map<String,Object> returnResult = new LinkedHashMap<String,Object>();
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<MessageReceiver> messageReceivers = new ArrayList<MessageReceiver>();
		if (extension.equalsIgnoreCase("xlsx")) {
			Map<String,Object> resultMap = processExcel2007(request,file);
			messageReceivers = (List<MessageReceiver>) resultMap.get("messageReceivers");
		}
		if (extension.equalsIgnoreCase("xls")) {
			Map<String,Object> resultMap = processExcel2003(request,file);
			messageReceivers = (List<MessageReceiver>) resultMap.get("messageReceivers");
		}
		
		if (UtilValidate.isNotEmpty(messageReceivers) && messageReceivers.size()>0) {
			messageReceivers.remove(0);
		}
		returnResult.put("messageReceivers", messageReceivers);
		return returnResult;
	}
}
