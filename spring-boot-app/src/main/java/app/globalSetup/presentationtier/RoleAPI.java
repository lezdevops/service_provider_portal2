package app.globalSetup.presentationtier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.appmodel.Role;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.RoleManager;
// newDependentBeanImportGoesHere

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = { "http://localhost:4200", "http://techpoint.pro" }, allowedHeaders = "*")
public class RoleAPI {

	@Autowired
	private RoleManager roleManager;

	@PostMapping("/role")
	public Map<String, Object> createRole(HttpServletRequest request, @RequestBody Role role) {
		role.setCreatedDate(null);
		return roleManager.save(request, role);
	}

	@GetMapping("/role/{id}")
	public Map<String, Object> getRole(HttpServletRequest request, @PathVariable Long id) {
		return roleManager.findOne(request, id);
	}

	@GetMapping(value = "/roles")
	public Map<String, Object> getAll(HttpServletRequest request) {
		return roleManager.getRoles(request);
	}

	@PutMapping("/role")
	public Map<String, Object> updateRole(HttpServletRequest request, @RequestBody Role role) {
		Map<String, Object> resultMessage = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			return resultMessage;
		} else {
			//permission_validationMessage
			resultMessage = getRole(request, role.getId());
			Role newRole = (Role)resultMessage.get("role");
			if (UtilValidate.isEmpty(newRole)) {
				return resultMessage;
			}
			newRole.setName(role.getName());
			newRole.setCreatedDate(role.getCreatedDate());

			return roleManager.save(request, newRole);
		}

	}

	@DeleteMapping("/role/{id}")
	public Map<String, Object> deleteRole(HttpServletRequest request, @PathVariable Long id) {
		Map<String, Object> resultMessage = new LinkedHashMap<String,Object>();
		boolean isReadonly = false;
		if (isReadonly) {
			return resultMessage;
		} else {			
			return roleManager.remove(request, id);
		}
	}

	@RequestMapping(value = "/role/saveMultipleRoleData", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> saveMultipleRoleData(HttpServletRequest request, @RequestBody List<Role> roles) {
		return roleManager.saveMultipleRoleData(request, roles);
	}

	@RequestMapping(value = "/role/delete/{id}", method = RequestMethod.POST)
	public Map<String, Object> remove(HttpServletRequest request, @PathVariable Long id) {
		return roleManager.remove(request, id);
	}

	@RequestMapping(value = "/role/removeAll", method = RequestMethod.POST)
	public Map<String, Object> removeAll(HttpServletRequest request) {
		return roleManager.removeAll(request);
	}

	@RequestMapping(value = "/role/get", params = { "page", "size" }, method = RequestMethod.GET)
	public Map<String, Object> findPaginated(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size) {
		return roleManager.findPaginated(request, page, size);
	}

	@RequestMapping(value = "/role/getPaginatedBy", params = { "page", "size",
			"searchToken" }, method = RequestMethod.GET)
	public Map<String, Object> getPaginatedBy(HttpServletRequest request, @RequestParam("page") int page,
			@RequestParam("size") int size, @RequestParam("searchToken") String searchToken) {
		return roleManager.findPaginatedRolesBySearchToken(request, page, size, searchToken);
	}

	@RequestMapping(value = "/role/searchOnRoleTable", method = RequestMethod.GET)
	public Map<String,Object> searchOnRoleTable(HttpServletRequest request, @RequestParam("id") Long id, Pageable pageRequest) {
		return roleManager.findById(request, id, pageRequest);
	}

	@RequestMapping(value = "/role/processExcel2003", method = RequestMethod.POST)
	public List<Role> processExcel2003(HttpServletRequest request,
			@RequestParam("excelfile2003") MultipartFile excelfile) {
		List<Role> roles = new ArrayList<>();
		try {
			int i = 1;
			HSSFWorkbook workbook = new HSSFWorkbook(excelfile.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Role role = new Role();
				HSSFRow row = worksheet.getRow(i++);
				// role.setField2(row.getCell(1).getStringCellValue());
				role.setName(row.getCell(0).getStringCellValue());
				role.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				roles.add(role);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return roles;
	}

	@RequestMapping(value = "/role/processExcel2007", method = RequestMethod.POST)
	public List<Role> processExcel2007(HttpServletRequest request,
			@RequestParam("excelfile2007") MultipartFile excelfile) {
		List<Role> roles = new ArrayList<>();
		try {
			int i = 1;
			XSSFWorkbook workbook = new XSSFWorkbook(excelfile.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			while (i <= worksheet.getLastRowNum()) {
				Role role = new Role();
				XSSFRow row = worksheet.getRow(i++);
				// role.setField2(row.getCell(1).getStringCellValue());
				role.setName(row.getCell(0).getStringCellValue());
				role.setCreatedDate(HelperUtils.getTimestampFromString(row.getCell(1).getStringCellValue()));

				roles.add(role);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return roles;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/role/excelFileUpload")
	@ResponseBody
	public List<Role> excelFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
		String fileName = file.getOriginalFilename();

		String extension = fileName.replaceAll("^.*\\.([^.]+)$", "$1");
		List<Role> roles = new ArrayList<Role>();
		if (extension.equalsIgnoreCase("xlsx")) {
			roles = processExcel2007(request, file);
		}
		if (extension.equalsIgnoreCase("xls")) {
			roles = processExcel2003(request, file);
		}

		if (roles.size() > 0) {
			roles.remove(0);
		}
		return roles;
	}
}
