package app.globalSetup.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.ApproverAdmin;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.ApproverAdminRepository;

@Service
public class ApproverAdminManager {
	private static final org.apache.log4j.Logger ApproverAdminManagerLogger = org.apache.log4j.Logger
			.getLogger(ApproverAdminManager.class);
	@Autowired
	private ApproverAdminRepository approverAdminRepository;
	@Autowired
	private ApproverAdminValidator approverAdminValidator;
	@Autowired
	private NotificationManager notificationManager;
	@Autowired
	private ApprovalQueueManager approvalQueueManager;

	// newAutowiredPropertiesGoesHere
	public Map<String, Object> getApproverAdminForEdit(HttpServletRequest request, Long id) {
		ApproverAdmin newApproverAdmin = new ApproverAdmin();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVERADMIN_EDIT");

		returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		ApproverAdmin approverAdmin = null;
		try {
			approverAdmin = approverAdminRepository.getOne(id);
			if (UtilValidate.isNotEmpty(approverAdmin)) {
				newApproverAdmin.setId(approverAdmin.getId());
				newApproverAdmin.setName(approverAdmin.getName());
				newApproverAdmin.setDesignation(approverAdmin.getDesignation());
				newApproverAdmin.setIsActive(approverAdmin.getIsActive());

			}
			returnResult.put("approverAdmin", newApproverAdmin);
		} catch (Exception e) {
			ApproverAdminManagerLogger
					.info("Exception Occured During Fetching ApproverAdmin Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public Map<String, Object> getApproverAdmin(HttpServletRequest request, Long id) {
		ApproverAdmin newApproverAdmin = new ApproverAdmin();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVERADMIN_READ");

		returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			ApproverAdmin approverAdmin = approverAdminRepository.getOne(id);
			if (UtilValidate.isNotEmpty(approverAdmin)) {
				newApproverAdmin.setId(approverAdmin.getId());
				newApproverAdmin.setName(approverAdmin.getName());
				newApproverAdmin.setDesignation(approverAdmin.getDesignation());
				newApproverAdmin.setIsActive(approverAdmin.getIsActive());

			}

			returnResult.put("approverAdmin", newApproverAdmin);
		} catch (Exception e) {
			ApproverAdminManagerLogger
					.info("Exception Occured During Fetching ApproverAdmin Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}

	public Map<String, Object> getApproverAdmins(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVERADMIN_READ");

		returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			List<ApproverAdmin> approverAdminsList = new ArrayList<ApproverAdmin>();
			Iterable<ApproverAdmin> approverAdmins = approverAdminRepository.findAll();
			for (ApproverAdmin approverAdmin : approverAdmins) {
				approverAdminsList.add(approverAdmin);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("approverAdmins", approverAdminsList);
		} catch (Exception e) {
			ApproverAdminManagerLogger
					.info("Exception Occured During Fetching ApproverAdmin List Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public List<ApproverAdmin> getApproverAdminesByName(String name, Long id) {
		List<ApproverAdmin> approverAdminsList = new ArrayList<ApproverAdmin>();
		Iterable<ApproverAdmin> approverAdmins = null;
		if (UtilValidate.isNotEmpty(id)) {
			approverAdmins = approverAdminRepository.findByNameDuringUpdate(name, id);
		} else {
			approverAdmins = approverAdminRepository.findByName(name);
		}
		for (ApproverAdmin approverAdmin : approverAdmins) {
			approverAdminsList.add(approverAdmin);
		}
		return approverAdminsList;
	}


	public Map<String, Object> create(HttpServletRequest request, ApproverAdmin approverAdmin) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("APPROVERADMIN_CREATE");

			returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = approverAdminValidator.getValidated(request, approverAdmin);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/approverAdmin");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				approverAdmin = approverAdminRepository.save(approverAdmin);
				notificationManager.send(request, null, approverAdmin, "APPROVERADMIN_CREATE");
				approvalQueueManager.saveApprovalQueue(request, approverAdmin, "APPROVERADMIN_CREATE");
				returnResult.put("successMessage", "Successfully Saved ApproverAdmin Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			ApproverAdminManagerLogger
					.info("Exception Occured During Creating ApproverAdmin Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> update(HttpServletRequest request, ApproverAdmin approverAdmin) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("APPROVERADMIN_EDIT");

			returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = approverAdminValidator.getValidated(request, approverAdmin);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/approverAdmin");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				approverAdmin = approverAdminRepository.save(approverAdmin);
				returnResult.put("successMessage", "Successfully Saved ApproverAdmin Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ApproverAdminManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			ApproverAdminManagerLogger
					.info("Exception Occured During Updating ApproverAdmin Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> saveMultipleApproverAdminData(HttpServletRequest request,
			List<ApproverAdmin> approverAdmins) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVERADMIN_CREATE");

		returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}

		try {
			for (ApproverAdmin approverAdmin : approverAdmins) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = approverAdminValidator.getValidated(request, approverAdmin);
				approverAdmin = approverAdminRepository.save(approverAdmin);
				returnResult.put(approverAdmin + " " + approverAdmin.getId() + " - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: " + e.getMessage());
			ApproverAdminManagerLogger
					.info("Exception Occured During Adding ApproverAdmin List Data. caused by: " + e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> remove(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully ApproverAdmin Deleted. Id: " + id + ".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVERADMIN_DELETE");

		returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			returnResult.put("error", "found");
			return returnResult;
		}

		try {
			ApproverAdmin approverAdmin = approverAdminRepository.findOne(id);
			if (UtilValidate.isNotEmpty(approverAdmin)) {
				approverAdminRepository.delete(id);

			}

		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: " + e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			ApproverAdminManagerLogger
					.info("Exception Occured During Removing ApproverAdmin Data. caused by: " + e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);

		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}

	public Map<String, Object> removeAll(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVERADMIN_DELETE");

		returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		String removeMessage = "Successfully All ApproverAdmin Deleted.";
		try {
			approverAdminRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApproverAdminManagerLogger
					.info("Exception Occured During Removing ApproverAdmin Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVERADMIN_READ");

		returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<ApproverAdmin> paginatedResult = approverAdminRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApproverAdminManagerLogger.info(
					"Exception Occured During Fetching Paginated ApproverAdmin Data. caused by: " + e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> findPaginatedApproverAdminsBySearchToken(HttpServletRequest request, int page, int size,
			String searchToken) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVERADMIN_READ");

		returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<ApproverAdmin> paginatedResultBySearchToken = approverAdminRepository
					.findPaginatedApproverAdminsBySearchToken(searchToken, new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApproverAdminManagerLogger
					.info("Exception Occured During Fetching Paginated ApproverAdmin Data Search. caused by: "
							+ e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findOne(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVERADMIN_READ");

		returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			ApproverAdmin approverAdmin = approverAdminRepository.findOne(id);
			returnResult.put("approverAdmin", approverAdmin);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApproverAdminManagerLogger
					.info("Exception Occured During Fetching ApproverAdmin Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<ApproverAdmin> approverAdmins = new ArrayList<ApproverAdmin>();
		allowedPermissions.add("APPROVERADMIN_READ");

		returnResult = approverAdminValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			approverAdmins = approverAdminRepository.findById(id);
			Page<ApproverAdmin> pages = new PageImpl<ApproverAdmin>(approverAdmins, pageRequest, approverAdmins.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApproverAdminManagerLogger
					.info("Exception Occured During Fetching ApproverAdmin Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

}
