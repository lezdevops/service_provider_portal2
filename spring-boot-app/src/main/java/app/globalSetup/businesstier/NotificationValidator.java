package app.globalSetup.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.Notification;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.NotificationManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class NotificationValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private NotificationManager notificationManager;
	public void validate(Notification notification) throws Exception {
		if (!checkNull(notification)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}
	
	public Map<String,Object> getValidated(HttpServletRequest request, Notification notification){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility notificationProperties = new ConfigUtility(new File("i18n/globalSetup/notification_i18n.properties"));
			Properties notificationPropertyValue = notificationProperties.loadProperties();
			
if (UtilValidate.isEmpty(notification.getNotificationProcess())) {
errorMessageMap.put("notificationProcess_validationMessage" , notificationPropertyValue.getProperty(language+ ".notificationProcess.requiredValidationLabel"));
}
if (UtilValidate.isNotEmpty(notificationManager.getNotificationesByNotificationProcess(notification.getNotificationProcess(),notification.getId()))) {
errorMessageMap.put("NotificationProcess_validationMessage" , notificationPropertyValue.getProperty(language+ ".notificationProcess.duplicateValidationLabel"));
}
if (UtilValidate.isEmpty(notification.getMessage())) {
errorMessageMap.put("message_validationMessage" , notificationPropertyValue.getProperty(language+ ".message.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(notification.getOldObjectVal())) {
errorMessageMap.put("oldObjectVal_validationMessage" , notificationPropertyValue.getProperty(language+ ".oldObjectVal.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(notification.getNewObjectVal())) {
errorMessageMap.put("newObjectVal_validationMessage" , notificationPropertyValue.getProperty(language+ ".newObjectVal.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(Notification notification){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
