package app.globalSetup.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.NotificationProcess;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.NotificationProcessManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class NotificationProcessValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private NotificationProcessManager notificationProcessManager;
	public void validate(NotificationProcess notificationProcess) throws Exception {
		if (!checkNull(notificationProcess)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}
	
	public Map<String,Object> getValidated(HttpServletRequest request, NotificationProcess notificationProcess){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility notificationProcessProperties = new ConfigUtility(new File("i18n/globalSetup/notificationProcess_i18n.properties"));
			Properties notificationProcessPropertyValue = notificationProcessProperties.loadProperties();
			
if (UtilValidate.isEmpty(notificationProcess.getName())) {
errorMessageMap.put("name_validationMessage" , notificationProcessPropertyValue.getProperty(language+ ".name.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(notificationProcess.getIsActive())) {
errorMessageMap.put("isActive_validationMessage" , notificationProcessPropertyValue.getProperty(language+ ".isActive.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(NotificationProcess notificationProcess){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
