package app.globalSetup.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.ApprovalProcess;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.ApprovalProcessManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class ApprovalProcessValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private ApprovalProcessManager approvalProcessManager;
	public void validate(ApprovalProcess approvalProcess) throws Exception {
		if (!checkNull(approvalProcess)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}
	
	public Map<String,Object> getValidated(HttpServletRequest request, ApprovalProcess approvalProcess){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility approvalProcessProperties = new ConfigUtility(new File("i18n/globalSetup/approvalProcess_i18n.properties"));
			Properties approvalProcessPropertyValue = approvalProcessProperties.loadProperties();
			
if (UtilValidate.isEmpty(approvalProcess.getProcessName())) {
errorMessageMap.put("processName_validationMessage" , approvalProcessPropertyValue.getProperty(language+ ".processName.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(approvalProcess.getIsActive())) {
errorMessageMap.put("isActive_validationMessage" , approvalProcessPropertyValue.getProperty(language+ ".isActive.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(ApprovalProcess approvalProcess){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
