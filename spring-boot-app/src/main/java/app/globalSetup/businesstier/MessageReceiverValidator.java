package app.globalSetup.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.MessageReceiver;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.MessageReceiverManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class MessageReceiverValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private MessageReceiverManager messageReceiverManager;

	public void validate(MessageReceiver messageReceiver) throws Exception {
		if (!checkNull(messageReceiver)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}

	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}

	public Map<String, Object> getValidated(HttpServletRequest request, MessageReceiver messageReceiver) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		try {

			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility messageReceiverProperties = new ConfigUtility(
					new File("i18n/globalSetup/messageReceiver_i18n.properties"));
			Properties messageReceiverPropertyValue = messageReceiverProperties.loadProperties();

			if (UtilValidate.isEmpty(messageReceiver.getName())) {
				errorMessageMap.put("name_validationMessage",
						messageReceiverPropertyValue.getProperty(language + ".name.requiredValidationLabel"));
			}
			if (UtilValidate.isNotEmpty(messageReceiverManager.getMessageReceiveresByName(messageReceiver.getName(),
					messageReceiver.getId()))) {
				errorMessageMap.put("name_validationMessage",
						messageReceiverPropertyValue.getProperty(language + ".name.duplicateValidationLabel"));
			}

			if (UtilValidate.isEmpty(messageReceiver.getIsActive())) {
				errorMessageMap.put("isActive_validationMessage",
						messageReceiverPropertyValue.getProperty(language + ".isActive.requiredValidationLabel"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return errorMessageMap;
	}

	public Boolean checkNull(MessageReceiver messageReceiver) {
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
