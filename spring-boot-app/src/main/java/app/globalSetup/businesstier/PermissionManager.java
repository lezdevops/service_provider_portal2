package app.globalSetup.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.Permission;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.PermissionRepository;

//newDependentBeanImportGoesHere

@Service
public class PermissionManager {
	@Autowired
	private PermissionRepository permissionRepository;
	@Autowired
	private PermissionValidator permissionValidator;

	// newAutowiredPropertiesGoesHere
	public Map<String, Object> getPermissionForEdit(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("PERMISSION_EDIT");

		returnResult = permissionValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		Permission permission = permissionRepository.getOne(id);

		returnResult.put("permission", permission);
		return returnResult;
	}

	public Map<String, Object> getPermission(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("PERMISSION_READ");

		returnResult = permissionValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		Permission permission = permissionRepository.getOne(id);

		returnResult.put("permission", permission);
		return returnResult;
	}

	public Map<String, Object> getPermissions(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("PERMISSION_READ");

		returnResult = permissionValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		List<Permission> permissionsList = new ArrayList<Permission>();
		Iterable<Permission> permissions = permissionRepository.findAll();
		for (Permission permission : permissions) {
			permissionsList.add(permission);
			// newGenerateEntityFetchActionGoesHere
		}
		returnResult.put("permissions", permissionsList);
		return returnResult;
	}

	public List<Permission> getPermissionesByName(String name) {
		List<Permission> permissionsList = new ArrayList<Permission>();
		Iterable<Permission> permissions = permissionRepository.findByName(name);
		for (Permission permission : permissions) {
			permissionsList.add(permission);
		}
		return permissionsList;
	}
	public Permission findPermissionByName(String name) {
		return permissionRepository.findPermissionByName(name);
	}

	public Map<String, Object> create(HttpServletRequest request, Permission permission) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("PERMISSION_CREATE");

			returnResult = permissionValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = permissionValidator.getValidated(request, permission);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/permission");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				permission = permissionRepository.save(permission);
				returnResult.put("successMessage", "Successfully Saved Permission Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(PermissionManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> update(HttpServletRequest request, Permission permission) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("PERMISSION_EDIT");

			returnResult = permissionValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = permissionValidator.getValidated(request, permission);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/permission");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				permission = permissionRepository.save(permission);
				returnResult.put("successMessage", "Successfully Saved Permission Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(PermissionManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> saveMultiplePermissionData(HttpServletRequest request, List<Permission> permissions) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("PERMISSION_CREATE");

		returnResult = permissionValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}

		try {
			for (Permission permission : permissions) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = permissionValidator.getValidated(request, permission);
				permission = permissionRepository.save(permission);
				returnResult.put(permission + " " + permission.getId() + " - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(PermissionManager.class.getName());
			logger.info(e);
			returnResult.put("addErrorMessage", "Techical Error: " + e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> remove(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully Permission Deleted. Id: " + id + ".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("PERMISSION_DELETE");

		returnResult = permissionValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			returnResult.put("error", "found");
			return returnResult;
		}

		try {
			Permission permission = permissionRepository.findOne(id);
			if (UtilValidate.isNotEmpty(permission)) {

				permissionRepository.delete(id);
			}

		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: " + e.getMessage();
		}
		returnResult.put("removeMessage", removeMessage);

		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}

	public Map<String, Object> removeAll(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("PERMISSION_DELETE");

		returnResult = permissionValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		String removeMessage = "Successfully All Permission Deleted.";
		permissionRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		returnResult.put("removeMessage", removeMessage);
		return returnResult;
	}

	public Map<String, Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("PERMISSION_READ");

		returnResult = permissionValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		Page<Permission> paginatedResult = permissionRepository.findAll(new PageRequest(page, size));
		returnResult.put("paginatedResult", paginatedResult);
		return returnResult;
	}

	public Map<String, Object> findPaginatedPermissionsBySearchToken(HttpServletRequest request, int page, int size,
			String searchToken) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("PERMISSION_READ");

		returnResult = permissionValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		Page<Permission> paginatedResultBySearchToken = permissionRepository
				.findPaginatedPermissionsBySearchToken(searchToken, new PageRequest(page, size));
		returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);

		return returnResult;
	}

	public Map<String, Object> findOne(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("PERMISSION_READ");

		returnResult = permissionValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		Permission permission = permissionRepository.findOne(id);
		returnResult.put("permission", permission);
		return returnResult;
	}

	public Map<String, Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<Permission> permissions = new ArrayList<Permission>();
		allowedPermissions.add("PERMISSION_READ");

		returnResult = permissionValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		permissions = permissionRepository.findById(id);
		Page<Permission> pages = new PageImpl<Permission>(permissions, pageRequest, permissions.size());
		returnResult.put("resultPages", pages);

		return returnResult;
	}

}
