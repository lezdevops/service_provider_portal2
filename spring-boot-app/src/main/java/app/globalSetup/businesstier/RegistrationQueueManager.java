package app.globalSetup.businesstier;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.adminPanel.businesstier.CustomerManager;
import app.adminPanel.businesstier.ServiceProviderManager;
import app.appmodel.Customer;
import app.appmodel.RegistrationQueue;
import app.appmodel.ServiceProvider;
import app.appmodel.User;
import app.common.businesstier.EmailManager;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.RegistrationQueueRepository;


//newDependentBeanImportGoesHere

@Service
public class RegistrationQueueManager {
	private static final org.apache.log4j.Logger RegistrationQueueManagerLogger = org.apache.log4j.Logger.getLogger(RegistrationQueueManager.class);
	@Autowired
	private RegistrationQueueRepository registrationQueueRepository;
	@Autowired
	private RegistrationQueueValidator registrationQueueValidator;
	
	@Autowired
	private UserManager userManager;
	
	@Autowired
	private ServiceProviderManager serviceProviderManager;
	
	@Autowired
	private CustomerManager customerManager;
	
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getRegistrationQueueForEdit(HttpServletRequest request, Long id){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_EDIT");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		RegistrationQueue registrationQueue = null;
		try {
			registrationQueue = registrationQueueRepository.getOne(id);
			
			returnResult.put("registrationQueue", registrationQueue);
		} catch (Exception e) {
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching RegistrationQueue Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public Map<String,Object> getRegistrationQueue(HttpServletRequest request, Long id){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			RegistrationQueue registrationQueue = registrationQueueRepository.getOne(id);
			
			returnResult.put("registrationQueue", registrationQueue);
		} catch (Exception e) {
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching RegistrationQueue Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getRegistrationQueues(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<RegistrationQueue> registrationQueuesList = new ArrayList<RegistrationQueue>();
			Iterable<RegistrationQueue> registrationQueues = registrationQueueRepository.findAll();
			for (RegistrationQueue registrationQueue : registrationQueues) {
				registrationQueuesList.add(registrationQueue);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("registrationQueues", registrationQueuesList);
		} catch (Exception e) {
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching RegistrationQueue List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public List<RegistrationQueue> getRegistrationQueueesByEmail(String email){List<RegistrationQueue> registrationQueuesList = new ArrayList<RegistrationQueue>();Iterable<RegistrationQueue> registrationQueues = registrationQueueRepository.findByEmail(email);for (RegistrationQueue registrationQueue : registrationQueues) {	registrationQueuesList.add(registrationQueue);}return registrationQueuesList;}
	
	public Map<String,Object> create(HttpServletRequest request, RegistrationQueue registrationQueue) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("REGISTRATIONQUEUE_CREATE");
			
			returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = registrationQueueValidator.getValidated(request,registrationQueue);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/registrationQueue");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				registrationQueue = registrationQueueRepository.save(registrationQueue);
				returnResult.put("successMessage", "Successfully Saved RegistrationQueue Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Creating RegistrationQueue Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> approveRegistrationQueue(HttpServletRequest request, RegistrationQueue registrationQueue) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("REGISTRATIONQUEUE_EDIT");
			
			returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				//returnResult = registrationQueueValidator.getValidated(request,registrationQueue);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/registrationQueue");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				try {
					String userName = registrationQueue.getUserName();
					String email = registrationQueue.getEmail();
					String accountType = registrationQueue.getAccountType();
					User user = new User();

					user.setUserName(userName);
					user.setPassword(registrationQueue.getPassword());
					user.setActive(Long.valueOf(1));
					user.setEmail(email);
					
					// create user
					Map<String,Object> messageMap = userManager.simpleUpdate(request, user);
					if (messageMap.get("error").equals("")) {
						if (accountType.equalsIgnoreCase("SP")) {
							ServiceProvider serviceProvider = new ServiceProvider();
							serviceProvider.setName(userName);
							serviceProvider.setUser(user);
							serviceProvider.setEmail(email);
							// create service provider
							messageMap = serviceProviderManager.save(request, serviceProvider);
							if (messageMap.get("error").equals("")) {
								sendEmailForApplicationMemberApproval(userName, email, accountType);
								
								registrationQueue.setApprovalStatus("ADMIN_APPROVED");
								registrationQueue = registrationQueueRepository.save(registrationQueue);
								returnResult.put("approveMessage", "Successfully Saved RegistrationQueue Data.");
							}else {
								System.out.println("Error to create service provider: "+messageMap);
							}
						}else if (accountType.equalsIgnoreCase("CUSTOMER")) {
							Customer customer = new Customer();
							customer.setName(userName);
							customer.setUser(user);
							customer.setEmail(email);
							// create service provider
							messageMap = customerManager.create(request, customer);
							if (messageMap.get("error").equals("")) {
								sendEmailForApplicationMemberApproval(userName, email, accountType);
								
								registrationQueue.setApprovalStatus("ADMIN_APPROVED");
								registrationQueue = registrationQueueRepository.save(registrationQueue);
								returnResult.put("approveMessage", "Successfully Saved RegistrationQueue Data.");
							}else {
								System.out.println("Error to create service provider: "+messageMap);
							}
							
						}else if (accountType.equalsIgnoreCase("GLOBAL_SETUPADMIN")) {
							
						}else if (accountType.equalsIgnoreCase("ADMIN")) {
							
						}else if (accountType.equalsIgnoreCase("FULLADMIN")) {
							
						}
						
					}else {
						System.out.println("Error to create user: "+messageMap);
					}
					
					
				} catch (Exception e) {
					returnResult.put("approveMessage", "Error To Approve.");
					e.printStackTrace();
					
				}
				
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Creating RegistrationQueue Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public void sendEmailForApplicationMemberApproval(String userName, String email, String accountType) throws Exception {
		Set<String> recipients = new LinkedHashSet<String>();
		recipients.add(email);
		String subject = "Welcome You Are Member Of Our Platform. Please Find Following Information.";
		Timestamp today = HelperUtils.getToday();
		StringBuilder emailBody = new StringBuilder("<html>");
		emailBody.append("<h1>Dear "+userName+", Please Find Following Information Below:</h1>");
		emailBody.append("<ul>");
		emailBody.append("<li>Email: "+email+"</li>");
		emailBody.append("<li>Account Type: "+accountType+"</li>");
		emailBody.append("<li>Registration Date: "+today+"</li>");
		emailBody.append("<li>Registration Status: Approved By Admin.</li>");
		emailBody.append("</ul>");
		
		StringBuilder emailFooter = new StringBuilder("<div>");
		emailFooter.append("<p>This email may contain confidential and/or legally privileged information. "
				+ "If you have received it in error, please notify the sender immediately and delete it "
				+ "(together with any attachments) from your system without using or "
				+ "disclosing its contents for any purposes or to any other person. Many thanks for your co-operation.</p>");
		emailFooter.append("</div>");
		emailFooter.append("</html>");
		List<File> attachmentFiles = new ArrayList<File>();
		EmailManager.send(recipients, subject, emailBody, emailFooter, attachmentFiles, false);
	}
	public Map<String,Object> update(HttpServletRequest request, RegistrationQueue registrationQueue) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("REGISTRATIONQUEUE_EDIT");
			
			returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				//returnResult = registrationQueueValidator.getValidated(request,registrationQueue);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/registrationQueue");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				registrationQueue = registrationQueueRepository.save(registrationQueue);
				returnResult.put("successMessage", "Successfully Saved RegistrationQueue Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(RegistrationQueueManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Updating RegistrationQueue Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	
	public Map<String,Object> saveMultipleRegistrationQueueData(HttpServletRequest request, List<RegistrationQueue> registrationQueues) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_CREATE");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (RegistrationQueue registrationQueue : registrationQueues) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = registrationQueueValidator.getValidated(request, registrationQueue);
				registrationQueue = registrationQueueRepository.save(registrationQueue);
				returnResult.put(registrationQueue+" "+registrationQueue.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Adding RegistrationQueue List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
	
		String removeMessage = "Successfully RegistrationQueue Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_DELETE");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			RegistrationQueue registrationQueue = registrationQueueRepository.findOne(id);
			if (UtilValidate.isNotEmpty(registrationQueue)) {
				
				registrationQueueRepository.delete(id);
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Removing RegistrationQueue Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_DELETE");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All RegistrationQueue Deleted.";
		try {
			registrationQueueRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Removing RegistrationQueue Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<RegistrationQueue> paginatedResult = registrationQueueRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching Paginated RegistrationQueue Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Map<String,Object> findPaginatedRegistrationQueuesBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<RegistrationQueue> paginatedResultBySearchToken = registrationQueueRepository.findPaginatedRegistrationQueuesBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching Paginated RegistrationQueue Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			RegistrationQueue registrationQueue = registrationQueueRepository.findOne(id);
			returnResult.put("registrationQueue", registrationQueue);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching RegistrationQueue Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<RegistrationQueue> registrationQueues = new ArrayList<RegistrationQueue>();
		allowedPermissions.add("REGISTRATIONQUEUE_READ");
		
		returnResult = registrationQueueValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			registrationQueues = registrationQueueRepository.findById(id);
			Page<RegistrationQueue> pages = new PageImpl<RegistrationQueue>(registrationQueues, pageRequest, registrationQueues.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			RegistrationQueueManagerLogger.info("Exception Occured During Fetching RegistrationQueue Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
