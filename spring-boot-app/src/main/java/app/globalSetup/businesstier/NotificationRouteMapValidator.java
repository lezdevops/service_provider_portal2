package app.globalSetup.businesstier;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.NotificationRouteMap;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class NotificationRouteMapValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private NotificationRouteMapManager notificationRouteMapManager;

	public void validate(NotificationRouteMap notificationRouteMap) throws Exception {
		if (!checkNull(notificationRouteMap)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}

	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}

	public Map<String, Object> getValidated(HttpServletRequest request, NotificationRouteMap notificationRouteMap) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		try {

			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility notificationRouteMapProperties = new ConfigUtility(
					new File("i18n/globalSetup/notificationRouteMap_i18n.properties"));
			Properties notificationRouteMapPropertyValue = notificationRouteMapProperties.loadProperties();

			if (UtilValidate.isEmpty(notificationRouteMap.getNotificationProcess())) {
				errorMessageMap.put("notificationProcess_validationMessage", notificationRouteMapPropertyValue
						.getProperty(language + ".notificationProcess.requiredValidationLabel"));
			}
			if (UtilValidate.isNotEmpty(notificationRouteMapManager.getNotificationRouteMapesByNotificationProcess(
					notificationRouteMap.getNotificationProcess(), notificationRouteMap.getId()))) {
				// errorMessageMap.put("NotificationProcess_validationMessage",
				// notificationRouteMapPropertyValue.getProperty(language +
				// ".notificationProcess.duplicateValidationLabel"));
			}
			if (UtilValidate.isEmpty(notificationRouteMap.getSenderId())) {
				errorMessageMap.put("senderId_validationMessage",
						notificationRouteMapPropertyValue.getProperty(language + ".senderId.requiredValidationLabel"));
			}

			if (UtilValidate.isEmpty(notificationRouteMap.getIsActive())) {
				errorMessageMap.put("isActive_validationMessage",
						notificationRouteMapPropertyValue.getProperty(language + ".isActive.requiredValidationLabel"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return errorMessageMap;
	}

	public Boolean checkNull(NotificationRouteMap notificationRouteMap) {
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
