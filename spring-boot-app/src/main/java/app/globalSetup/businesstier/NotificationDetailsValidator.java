package app.globalSetup.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.NotificationDetails;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.NotificationDetailsManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class NotificationDetailsValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private NotificationDetailsManager notificationDetailsManager;
	public void validate(NotificationDetails notificationDetails) throws Exception {
		if (!checkNull(notificationDetails)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}
	
	public Map<String,Object> getValidated(HttpServletRequest request, NotificationDetails notificationDetails){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility notificationDetailsProperties = new ConfigUtility(new File("i18n/globalSetup/notificationDetails_i18n.properties"));
			Properties notificationDetailsPropertyValue = notificationDetailsProperties.loadProperties();
			
if (UtilValidate.isEmpty(notificationDetails.getNotification())) {
errorMessageMap.put("notification_validationMessage" , notificationDetailsPropertyValue.getProperty(language+ ".notification.requiredValidationLabel"));
}
if (UtilValidate.isNotEmpty(notificationDetailsManager.getNotificationDetailsesByNotification(notificationDetails.getNotification(),notificationDetails.getId()))) {
errorMessageMap.put("Notification_validationMessage" , notificationDetailsPropertyValue.getProperty(language+ ".notification.duplicateValidationLabel"));
}
if (UtilValidate.isEmpty(notificationDetails.getSenderId())) {
errorMessageMap.put("senderId_validationMessage" , notificationDetailsPropertyValue.getProperty(language+ ".senderId.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(notificationDetails.getReceiverId())) {
errorMessageMap.put("receiverId_validationMessage" , notificationDetailsPropertyValue.getProperty(language+ ".receiverId.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(notificationDetails.getIsRead())) {
errorMessageMap.put("isRead_validationMessage" , notificationDetailsPropertyValue.getProperty(language+ ".isRead.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(NotificationDetails notificationDetails){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
