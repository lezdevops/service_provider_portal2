package app.globalSetup.businesstier;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.ApprovalQueue;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class ApprovalQueueValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private ApprovalQueueManager approvalQueueManager;

	public void validate(ApprovalQueue approvalQueue) throws Exception {
		if (!checkNull(approvalQueue)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}

	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}

	public Map<String, Object> getValidated(HttpServletRequest request, ApprovalQueue approvalQueue) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		try {

			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility approvalQueueProperties = new ConfigUtility(
					new File("i18n/globalSetup/approvalQueue_i18n.properties"));
			Properties approvalQueuePropertyValue = approvalQueueProperties.loadProperties();

			if (UtilValidate.isEmpty(approvalQueue.getApprovalProcess())) {
				errorMessageMap.put("approvalProcess_validationMessage",
						approvalQueuePropertyValue.getProperty(language + ".approvalProcess.requiredValidationLabel"));
			}
			if (UtilValidate.isNotEmpty(approvalQueueManager
					.getApprovalQueueesByApprovalProcess(approvalQueue.getApprovalProcess(), approvalQueue.getId()))) {
				errorMessageMap.put("ApprovalProcess_validationMessage",
						approvalQueuePropertyValue.getProperty(language + ".approvalProcess.duplicateValidationLabel"));
			}
			if (UtilValidate.isEmpty(approvalQueue.getIsActive())) {
				errorMessageMap.put("isActive_validationMessage",
						approvalQueuePropertyValue.getProperty(language + ".isActive.requiredValidationLabel"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return errorMessageMap;
	}

	public Boolean checkNull(ApprovalQueue approvalQueue) {
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
