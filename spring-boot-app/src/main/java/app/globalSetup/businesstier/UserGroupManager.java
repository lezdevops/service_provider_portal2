package app.globalSetup.businesstier;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.UserGroup;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.UserGroupRepository;


//newDependentBeanImportGoesHere

@Service
public class UserGroupManager {
	private static final org.apache.log4j.Logger UserGroupManagerLogger = org.apache.log4j.Logger.getLogger(UserGroupManager.class);
	@Autowired
	private UserGroupRepository userGroupRepository;
	@Autowired
	private UserGroupValidator userGroupValidator;
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getUserGroupForEdit(HttpServletRequest request, Long id){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USERGROUP_EDIT");
		
		returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		UserGroup userGroup = null;
		try {
			userGroup = userGroupRepository.getOne(id);
			String imageAddress = userGroup.getImageAddress();
	if (UtilValidate.isNotEmpty(imageAddress)) {	File file = new File(imageAddress);
		if (file.exists()) {
		String base64String ="";
		try {
			base64String = FileUploadUtils.getBase64StringFromFilePath(imageAddress);
			String extension = FileUploadUtils.getFileExtension(file.getName());
			
			base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		userGroup.setImageAddress(base64String);
	}
}

			returnResult.put("userGroup", userGroup);
		} catch (Exception e) {
			UserGroupManagerLogger.info("Exception Occured During Fetching UserGroup Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public Map<String,Object> getUserGroup(HttpServletRequest request, Long id){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USERGROUP_READ");
		
		returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			UserGroup userGroup = userGroupRepository.getOne(id);
			String imageAddress = userGroup.getImageAddress();
	if (UtilValidate.isNotEmpty(imageAddress)) {	File file = new File(imageAddress);
		if (file.exists()) {
		String base64String ="";
		try {
			base64String = FileUploadUtils.getBase64StringFromFilePath(imageAddress);
			String extension = FileUploadUtils.getFileExtension(file.getName());
			
			base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		userGroup.setImageAddress(base64String);
	}
}

			returnResult.put("userGroup", userGroup);
		} catch (Exception e) {
			UserGroupManagerLogger.info("Exception Occured During Fetching UserGroup Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getUserGroups(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USERGROUP_READ");
		
		returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<UserGroup> userGroupsList = new ArrayList<UserGroup>();
			Iterable<UserGroup> userGroups = userGroupRepository.findAll();
			for (UserGroup userGroup : userGroups) {
				userGroupsList.add(userGroup);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("userGroups", userGroupsList);
		} catch (Exception e) {
			UserGroupManagerLogger.info("Exception Occured During Fetching UserGroup List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	
	public Map<String,Object> create(HttpServletRequest request, UserGroup userGroup) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("USERGROUP_CREATE");
			
			returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = userGroupValidator.getValidated(request,userGroup);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/userGroup");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				String imageAddress = userGroup.getImageAddress();
  if (UtilValidate.isNotEmpty(imageAddress)) {
		long now = HelperUtils.nowTimestamp().getTime();
		Map<String,Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(imageAddress);
		String byteArray = (String)mapData.get("byteArray");
		String extension = (String)mapData.get("extension");		String filePath = path+"/"+now+"."+extension;
		userGroup.setImageAddress(filePath);
		FileUploadUtils.store(byteArray, filePath);
}
				userGroup = userGroupRepository.save(userGroup);
				returnResult.put("successMessage", "Successfully Saved UserGroup Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			UserGroupManagerLogger.info("Exception Occured During Creating UserGroup Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> update(HttpServletRequest request, UserGroup userGroup) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("USERGROUP_EDIT");
			
			returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = userGroupValidator.getValidated(request,userGroup);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/userGroup");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				String imageAddress = userGroup.getImageAddress();
  if (UtilValidate.isNotEmpty(imageAddress)) {
		long now = HelperUtils.nowTimestamp().getTime();
		Map<String,Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(imageAddress);
		String byteArray = (String)mapData.get("byteArray");
		String extension = (String)mapData.get("extension");		String filePath = path+"/"+now+"."+extension;
		userGroup.setImageAddress(filePath);
		FileUploadUtils.store(byteArray, filePath);
}
				userGroup = userGroupRepository.save(userGroup);
				returnResult.put("successMessage", "Successfully Saved UserGroup Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(UserGroupManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			UserGroupManagerLogger.info("Exception Occured During Updating UserGroup Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	
	public Map<String,Object> saveMultipleUserGroupData(HttpServletRequest request, List<UserGroup> userGroups) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USERGROUP_CREATE");
		
		returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (UserGroup userGroup : userGroups) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = userGroupValidator.getValidated(request, userGroup);
				userGroup = userGroupRepository.save(userGroup);
				returnResult.put(userGroup+" "+userGroup.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			UserGroupManagerLogger.info("Exception Occured During Adding UserGroup List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully UserGroup Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USERGROUP_DELETE");
		
		returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			UserGroup userGroup = userGroupRepository.findOne(id);
			if (UtilValidate.isNotEmpty(userGroup)) {
						String imageAddress = userGroup.getImageAddress();
		if (UtilValidate.isNotEmpty(imageAddress)) {				
			File file = new File(imageAddress);
			if (file.exists()) {
						FileUtils.forceDelete(file);
			}
		}
				userGroupRepository.delete(id);
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			UserGroupManagerLogger.info("Exception Occured During Removing UserGroup Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USERGROUP_DELETE");
		
		returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All UserGroup Deleted.";
		try {
			userGroupRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			UserGroupManagerLogger.info("Exception Occured During Removing UserGroup Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USERGROUP_READ");
		
		returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<UserGroup> paginatedResult = userGroupRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			UserGroupManagerLogger.info("Exception Occured During Fetching Paginated UserGroup Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Map<String,Object> findPaginatedUserGroupsBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USERGROUP_READ");
		
		returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<UserGroup> paginatedResultBySearchToken = userGroupRepository.findPaginatedUserGroupsBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			UserGroupManagerLogger.info("Exception Occured During Fetching Paginated UserGroup Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USERGROUP_READ");
		
		returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			UserGroup userGroup = userGroupRepository.findOne(id);
			returnResult.put("userGroup", userGroup);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			UserGroupManagerLogger.info("Exception Occured During Fetching UserGroup Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<UserGroup> userGroups = new ArrayList<UserGroup>();
		allowedPermissions.add("USERGROUP_READ");
		
		returnResult = userGroupValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			userGroups = userGroupRepository.findById(id);
			Page<UserGroup> pages = new PageImpl<UserGroup>(userGroups, pageRequest, userGroups.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			UserGroupManagerLogger.info("Exception Occured During Fetching UserGroup Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
