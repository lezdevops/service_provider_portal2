package app.globalSetup.businesstier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import app.appmodel.NotificationProcess;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.NotificationProcessRepository;


import app.appmodel.NotificationRouteMap;
import app.appmodel.Notification;


@Service
public class NotificationProcessManager {
	private static final org.apache.log4j.Logger NotificationProcessManagerLogger = org.apache.log4j.Logger.getLogger(NotificationProcessManager.class);
	@Autowired
	private NotificationProcessRepository notificationProcessRepository;
	@Autowired
	private NotificationProcessValidator notificationProcessValidator;
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getNotificationProcessForEdit(HttpServletRequest request, Long id){
		NotificationProcess newNotificationProcess = new NotificationProcess();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONPROCESS_EDIT");
		
		returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		NotificationProcess notificationProcess = null;
		try {
			notificationProcess = notificationProcessRepository.getOne(id);
			if (UtilValidate.isNotEmpty(notificationProcess)) {
				newNotificationProcess.setId(notificationProcess.getId());
newNotificationProcess.setName(notificationProcess.getName());
newNotificationProcess.setIsActive(notificationProcess.getIsActive());
newNotificationProcess.setSendEmail(notificationProcess.getSendEmail());
newNotificationProcess.setSendSms(notificationProcess.getSendSms());

				
			}
			returnResult.put("notificationProcess", newNotificationProcess);
		} catch (Exception e) {
			NotificationProcessManagerLogger.info("Exception Occured During Fetching NotificationProcess Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	public Map<String,Object> getNotificationProcess(HttpServletRequest request, Long id){
		NotificationProcess newNotificationProcess = new NotificationProcess();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONPROCESS_READ");
		
		returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			NotificationProcess notificationProcess = notificationProcessRepository.getOne(id);
			if (UtilValidate.isNotEmpty(notificationProcess)) {
				newNotificationProcess.setId(notificationProcess.getId());
newNotificationProcess.setName(notificationProcess.getName());
newNotificationProcess.setIsActive(notificationProcess.getIsActive());
newNotificationProcess.setSendEmail(notificationProcess.getSendEmail());
newNotificationProcess.setSendSms(notificationProcess.getSendSms());

				
			}
			
			returnResult.put("notificationProcess", newNotificationProcess);
		} catch (Exception e) {
			NotificationProcessManagerLogger.info("Exception Occured During Fetching NotificationProcess Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getNotificationProcesss(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONPROCESS_READ");
		
		returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<NotificationProcess> notificationProcesssList = new ArrayList<NotificationProcess>();
			Iterable<NotificationProcess> notificationProcesss = notificationProcessRepository.findAll();
			for (NotificationProcess notificationProcess : notificationProcesss) {
				notificationProcesssList.add(notificationProcess);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("notificationProcesss", notificationProcesssList);
		} catch (Exception e) {
			NotificationProcessManagerLogger.info("Exception Occured During Fetching NotificationProcess List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	
	public Map<String,Object> create(HttpServletRequest request, NotificationProcess notificationProcess) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("NOTIFICATIONPROCESS_CREATE");
			
			returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = notificationProcessValidator.getValidated(request,notificationProcess);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/notificationProcess");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				notificationProcess = notificationProcessRepository.save(notificationProcess);
				returnResult.put("successMessage", "Successfully Saved NotificationProcess Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			NotificationProcessManagerLogger.info("Exception Occured During Creating NotificationProcess Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> update(HttpServletRequest request, NotificationProcess notificationProcess) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("NOTIFICATIONPROCESS_EDIT");
			
			returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = notificationProcessValidator.getValidated(request,notificationProcess);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/notificationProcess");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				notificationProcess = notificationProcessRepository.save(notificationProcess);
				returnResult.put("successMessage", "Successfully Saved NotificationProcess Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(NotificationProcessManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			NotificationProcessManagerLogger.info("Exception Occured During Updating NotificationProcess Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	
	public Map<String,Object> saveMultipleNotificationProcessData(HttpServletRequest request, List<NotificationProcess> notificationProcesss) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONPROCESS_CREATE");
		
		returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (NotificationProcess notificationProcess : notificationProcesss) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = notificationProcessValidator.getValidated(request, notificationProcess);
				notificationProcess = notificationProcessRepository.save(notificationProcess);
				returnResult.put(notificationProcess+" "+notificationProcess.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			NotificationProcessManagerLogger.info("Exception Occured During Adding NotificationProcess List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully NotificationProcess Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONPROCESS_DELETE");
		
		returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			NotificationProcess notificationProcess = notificationProcessRepository.findOne(id);
			if (UtilValidate.isNotEmpty(notificationProcess)) {
				notificationProcessRepository.delete(id);
				
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			NotificationProcessManagerLogger.info("Exception Occured During Removing NotificationProcess Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONPROCESS_DELETE");
		
		returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All NotificationProcess Deleted.";
		try {
			notificationProcessRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			NotificationProcessManagerLogger.info("Exception Occured During Removing NotificationProcess Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONPROCESS_READ");
		
		returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<NotificationProcess> paginatedResult = notificationProcessRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			NotificationProcessManagerLogger.info("Exception Occured During Fetching Paginated NotificationProcess Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Map<String,Object> findPaginatedNotificationProcesssBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONPROCESS_READ");
		
		returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<NotificationProcess> paginatedResultBySearchToken = notificationProcessRepository.findPaginatedNotificationProcesssBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			NotificationProcessManagerLogger.info("Exception Occured During Fetching Paginated NotificationProcess Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONPROCESS_READ");
		
		returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			NotificationProcess notificationProcess = notificationProcessRepository.findOne(id);
			returnResult.put("notificationProcess", notificationProcess);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			NotificationProcessManagerLogger.info("Exception Occured During Fetching NotificationProcess Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public NotificationProcess findNotificationProcessByName(String name) {
		NotificationProcess notificationProcess = null;

		try {
			notificationProcess = notificationProcessRepository.findNotificationProcessByName(name);
		} catch (Exception e) {
			NotificationProcessManagerLogger.info("Exception Occured During Fetching NotificationProcess Data. caused by: "+e.getMessage());
		}
		
		return notificationProcess;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<NotificationProcess> notificationProcesss = new ArrayList<NotificationProcess>();
		allowedPermissions.add("NOTIFICATIONPROCESS_READ");
		
		returnResult = notificationProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			notificationProcesss = notificationProcessRepository.findById(id);
			Page<NotificationProcess> pages = new PageImpl<NotificationProcess>(notificationProcesss, pageRequest, notificationProcesss.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			NotificationProcessManagerLogger.info("Exception Occured During Fetching NotificationProcess Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
