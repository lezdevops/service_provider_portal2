package app.globalSetup.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.UserGroup;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.UserGroupManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class UserGroupValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private UserGroupManager userGroupManager;
	public void validate(UserGroup userGroup) throws Exception {
		if (!checkNull(userGroup)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("permission_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}
	
	public Map<String,Object> getValidated(HttpServletRequest request, UserGroup userGroup){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility userGroupProperties = new ConfigUtility(new File("i18n/globalSetup/userGroup_i18n.properties"));
			Properties userGroupPropertyValue = userGroupProperties.loadProperties();
			
if (UtilValidate.isEmpty(userGroup.getName())) {
errorMessageMap.put("name_validationMessage" , userGroupPropertyValue.getProperty(language+ ".name.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(userGroup.getMetaData())) {
errorMessageMap.put("metaData_validationMessage" , userGroupPropertyValue.getProperty(language+ ".metaData.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(UserGroup userGroup){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
