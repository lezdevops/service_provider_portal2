package app.globalSetup.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.Notification;
import app.appmodel.NotificationDetails;
import app.appmodel.User;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.NotificationDetailsRepository;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class NotificationDetailsManager {
	private static final org.apache.log4j.Logger NotificationDetailsManagerLogger = org.apache.log4j.Logger
			.getLogger(NotificationDetailsManager.class);
	@Autowired
	private NotificationDetailsRepository notificationDetailsRepository;
	@Autowired
	private NotificationDetailsValidator notificationDetailsValidator;
	@Autowired
	private NotificationManager notificationManager;

	// newAutowiredPropertiesGoesHere
	public Map<String, Object> getNotificationDetailsForEdit(HttpServletRequest request, Long id) {
		NotificationDetails newNotificationDetails = new NotificationDetails();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONDETAILS_EDIT");

		returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		NotificationDetails notificationDetails = null;
		try {
			notificationDetails = notificationDetailsRepository.getOne(id);
			if (UtilValidate.isNotEmpty(notificationDetails)) {
				newNotificationDetails.setId(notificationDetails.getId());
				newNotificationDetails.setNotification(notificationDetails.getNotification());
				newNotificationDetails.setSenderId(notificationDetails.getSenderId());
				newNotificationDetails.setReceiverId(notificationDetails.getReceiverId());
				newNotificationDetails.setIsRead(notificationDetails.getIsRead());

			}
			returnResult.put("notificationDetails", newNotificationDetails);
		} catch (Exception e) {
			NotificationDetailsManagerLogger
					.info("Exception Occured During Fetching NotificationDetails Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public Map<String, Object> getNotificationDetails(HttpServletRequest request, Long id) {
		NotificationDetails newNotificationDetails = new NotificationDetails();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONDETAILS_READ");

		returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			NotificationDetails notificationDetails = notificationDetailsRepository.getOne(id);
			if (UtilValidate.isNotEmpty(notificationDetails)) {
				newNotificationDetails.setId(notificationDetails.getId());
				newNotificationDetails.setNotification(notificationDetails.getNotification());
				newNotificationDetails.setSenderId(notificationDetails.getSenderId());
				newNotificationDetails.setReceiverId(notificationDetails.getReceiverId());
				newNotificationDetails.setIsRead(notificationDetails.getIsRead());

			}

			returnResult.put("notificationDetails", newNotificationDetails);
		} catch (Exception e) {
			NotificationDetailsManagerLogger
					.info("Exception Occured During Fetching NotificationDetails Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}

	public Map<String, Object> getNotificationDetailss(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONDETAILS_READ");

		returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			List<NotificationDetails> notificationDetailssList = new ArrayList<NotificationDetails>();
			Iterable<NotificationDetails> notificationDetailss = notificationDetailsRepository.findAll();
			for (NotificationDetails notificationDetails : notificationDetailss) {
				notificationDetailssList.add(notificationDetails);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("notificationDetailss", notificationDetailssList);
		} catch (Exception e) {
			NotificationDetailsManagerLogger.info(
					"Exception Occured During Fetching NotificationDetails List Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public List<NotificationDetails> getNotificationDetailsesByNotification(Notification Notification, Long id) {
		List<NotificationDetails> notificationDetailssList = new ArrayList<NotificationDetails>();
		Iterable<NotificationDetails> notificationDetailss = null;
		if (UtilValidate.isNotEmpty(id)) {
			notificationDetailss = notificationDetailsRepository.findByNotificationDuringUpdate(Notification, id);
		} else {
			notificationDetailss = notificationDetailsRepository.findByNotification(Notification);
		}
		for (NotificationDetails notificationDetails : notificationDetailss) {
			notificationDetailssList.add(notificationDetails);
		}
		return notificationDetailssList;
	}
	public List<NotificationDetails> getNotificationDetailsesByReceiverId(String receiverId) {
		List<NotificationDetails> notificationDetailssList = new ArrayList<NotificationDetails>();
		Iterable<NotificationDetails> notificationDetailss = null;
		
		if (UtilValidate.isNotEmpty(receiverId)) {
			notificationDetailss = notificationDetailsRepository.findByReceiverIdAndIsRead(receiverId,false);
		}
		for (NotificationDetails notificationDetails : notificationDetailss) {
			notificationDetailssList.add(notificationDetails);
		}
		return notificationDetailssList;
	}

	public Map<String, Object> create(HttpServletRequest request, NotificationDetails notificationDetails) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("NOTIFICATIONDETAILS_CREATE");

			returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = notificationDetailsValidator.getValidated(request, notificationDetails);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/notificationDetails");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				notificationDetails = notificationDetailsRepository.save(notificationDetails);
				returnResult.put("successMessage", "Successfully Saved NotificationDetails Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			NotificationDetailsManagerLogger
					.info("Exception Occured During Creating NotificationDetails Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> update(HttpServletRequest request, NotificationDetails notificationDetails) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("NOTIFICATIONDETAILS_EDIT");

			returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = notificationDetailsValidator.getValidated(request, notificationDetails);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/notificationDetails");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				notificationDetails = notificationDetailsRepository.save(notificationDetails);
				returnResult.put("successMessage", "Successfully Saved NotificationDetails Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(NotificationDetailsManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			NotificationDetailsManagerLogger
					.info("Exception Occured During Updating NotificationDetails Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> saveMultipleNotificationDetailsData(HttpServletRequest request,
			List<NotificationDetails> notificationDetailss) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONDETAILS_CREATE");

		returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}

		try {
			for (NotificationDetails notificationDetails : notificationDetailss) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = notificationDetailsValidator.getValidated(request, notificationDetails);
				notificationDetails = notificationDetailsRepository.save(notificationDetails);
				returnResult.put(notificationDetails + " " + notificationDetails.getId() + " - Result",
						"Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: " + e.getMessage());
			NotificationDetailsManagerLogger.info(
					"Exception Occured During Adding NotificationDetails List Data. caused by: " + e.getMessage());
		}
		return returnResult;
	}

	public void saveAll(List<NotificationDetails> notificationDetailss) {
		try {
			for (NotificationDetails notificationDetails : notificationDetailss) {
				notificationDetails = notificationDetailsRepository.save(notificationDetails);
			}
		} catch (Exception e) {
			NotificationDetailsManagerLogger.info(
					"Exception Occured During Adding NotificationDetails List Data. caused by: " + e.getMessage());
		}
	}

	public Map<String, Object> remove(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully NotificationDetails Deleted. Id: " + id + ".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONDETAILS_DELETE");

		returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			returnResult.put("error", "found");
			return returnResult;
		}

		try {
			NotificationDetails notificationDetails = notificationDetailsRepository.findOne(id);
			if (UtilValidate.isNotEmpty(notificationDetails)) {
				notificationDetailsRepository.delete(id);

			}

		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: " + e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			NotificationDetailsManagerLogger
					.info("Exception Occured During Removing NotificationDetails Data. caused by: " + e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);

		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}

	public Map<String, Object> removeAll(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONDETAILS_DELETE");

		returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		String removeMessage = "Successfully All NotificationDetails Deleted.";
		try {
			notificationDetailsRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationDetailsManagerLogger
					.info("Exception Occured During Removing NotificationDetails Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONDETAILS_READ");

		returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<NotificationDetails> paginatedResult = notificationDetailsRepository
					.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationDetailsManagerLogger
					.info("Exception Occured During Fetching Paginated NotificationDetails Data. caused by: "
							+ e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> findPaginatedNotificationDetailssBySearchToken(HttpServletRequest request, int page,
			int size, String searchToken) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONDETAILS_READ");

		returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<NotificationDetails> paginatedResultBySearchToken = notificationDetailsRepository
					.findPaginatedNotificationDetailssBySearchToken(searchToken, new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationDetailsManagerLogger
					.info("Exception Occured During Fetching Paginated NotificationDetails Data Search. caused by: "
							+ e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findOne(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONDETAILS_READ");

		returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			NotificationDetails notificationDetails = notificationDetailsRepository.findOne(id);
			returnResult.put("notificationDetails", notificationDetails);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationDetailsManagerLogger
					.info("Exception Occured During Fetching NotificationDetails Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<NotificationDetails> notificationDetailss = new ArrayList<NotificationDetails>();
		allowedPermissions.add("NOTIFICATIONDETAILS_READ");

		returnResult = notificationDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			notificationDetailss = notificationDetailsRepository.findById(id);
			Page<NotificationDetails> pages = new PageImpl<NotificationDetails>(notificationDetailss, pageRequest,
					notificationDetailss.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationDetailsManagerLogger
					.info("Exception Occured During Fetching NotificationDetails Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

}
