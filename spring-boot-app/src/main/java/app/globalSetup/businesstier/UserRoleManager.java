package app.globalSetup.businesstier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.Role;
import app.appmodel.User;
import app.appmodel.UserRole;
import app.framework.businesstier.UtilValidate;

//newDependentBeanImportGoesHere

@Service
public class UserRoleManager {
	@Autowired
	private UserRoleValidator userRoleValidator;
	@Autowired
	private UserManager userManager;
	@Autowired
	private RoleManager roleManager;
	// newAutowiredPropertiesGoesHere


	public List<UserRole> getUserRoles(HttpServletRequest request) {
		List<UserRole> userRolesList = new ArrayList<UserRole>();

		List<User> users = userManager.getUsers(request);
		if (UtilValidate.isNotEmpty(users)) {
			for (User user : users) {

				UserRole userRole = new UserRole();
				userRole.setId(user.getId());
				userRole.setUserName(user.getUserName());
				List<Role> roles = user.getRoles();

				List<String> roleStrings = new ArrayList<String>();
				for (Role role : roles) {
					String name = role.getName();
					roleStrings.add(name);
				}
				userRole.setRoles(roleStrings);

				userRolesList.add(userRole);
			}
		}
		return userRolesList;
	}

	public Map<String, Object> saveMultipleUserRoleData(HttpServletRequest request, List<UserRole> userRoles) {

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USERPERMISSION_CREATE");

		// errorMessageMap = userRoleValidator.validatePermission(request,
		// allowedPermissions);

		Map<String, Object> messageMap = new LinkedHashMap<String, Object>();
		try {
			for (UserRole userRole : userRoles) {
				User user = userManager.findUserByUserName(userRole.getUserName());
				List<String> roleStrings = userRole.getRoles();
				Set<String> roleSet = new LinkedHashSet<String>();
				for (String role : roleStrings) {
					roleSet.add(role);
				}
				List<Role> roles = new ArrayList<Role>();
				for (String role : roleSet) {
					Map<String,Object> resultMap = new LinkedHashMap<String,Object>();
					resultMap = roleManager.findByName(request, role);
					if (UtilValidate.isNotEmpty(resultMap) && UtilValidate.isNotEmpty(resultMap.get("role"))) {
						Role roleEntity = (Role)resultMap.get("role");
						roles.add(roleEntity);
					}
				}
				user.setRoles(roles);
				userManager.simpleUpdate(request, user);
				messageMap.put(user.getUserName(),
						"Successfully UserRole Added For User " + userRole.getUserName() + " .");

				// newGenerateEntityAddActionGoesHere
			}

			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(UserRoleManager.class.getName());
			logger.info(e);
			messageMap.put("error", "Techical Error: " + e.getMessage());
		}
		return messageMap;
	}

}
