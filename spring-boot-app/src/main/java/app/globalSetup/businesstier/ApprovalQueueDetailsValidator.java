package app.globalSetup.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.ApprovalQueueDetails;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.ApprovalQueueDetailsManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class ApprovalQueueDetailsValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private ApprovalQueueDetailsManager approvalQueueDetailsManager;
	public void validate(ApprovalQueueDetails approvalQueueDetails) throws Exception {
		if (!checkNull(approvalQueueDetails)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}
	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}
	
	public Map<String,Object> getValidated(HttpServletRequest request, ApprovalQueueDetails approvalQueueDetails){
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		try {
			
			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility approvalQueueDetailsProperties = new ConfigUtility(new File("i18n/globalSetup/approvalQueueDetails_i18n.properties"));
			Properties approvalQueueDetailsPropertyValue = approvalQueueDetailsProperties.loadProperties();
			
if (UtilValidate.isEmpty(approvalQueueDetails.getApprovalQueue())) {
errorMessageMap.put("approvalQueue_validationMessage" , approvalQueueDetailsPropertyValue.getProperty(language+ ".approvalQueue.requiredValidationLabel"));
}
if (UtilValidate.isEmpty(approvalQueueDetails.getApproverId())) {
errorMessageMap.put("approverId_validationMessage" , approvalQueueDetailsPropertyValue.getProperty(language+ ".approverId.requiredValidationLabel"));
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return errorMessageMap;
	}
	public Boolean checkNull(ApprovalQueueDetails approvalQueueDetails){
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
