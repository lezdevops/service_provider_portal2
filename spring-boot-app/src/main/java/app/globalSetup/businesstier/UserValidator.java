package app.globalSetup.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.User;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.UserManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class UserValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private UserManager userManager;

	public void validate(User user) throws Exception {
		if (!checkNull(user)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}

	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap.put("permission_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}

	public Map<String, Object> getValidated(HttpServletRequest request, User user) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		try {

			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility userProperties = new ConfigUtility(new File("i18n/globalSetup/user_i18n.properties"));
			Properties userPropertyValue = userProperties.loadProperties();

			if (UtilValidate.isEmpty(user.getUserName())) {
				errorMessageMap.put("userName_validationMessage",
						userPropertyValue.getProperty(language + ".userName.requiredValidationLabel"));
			}
			if (UtilValidate.isNotEmpty(userManager.getUseresByUserName(user.getUserName()))) {
				errorMessageMap.put("userName_validationMessage",
						userPropertyValue.getProperty(language + ".userName.duplicateValidationLabel"));
			}
			if (UtilValidate.isEmpty(user.getPassword())) {
				errorMessageMap.put("password_validationMessage",
						userPropertyValue.getProperty(language + ".password.requiredValidationLabel"));
			}
			if (UtilValidate.isNotEmpty(userManager.getUseresByEmail(user.getEmail()))) {
				errorMessageMap.put("email_validationMessage",
						userPropertyValue.getProperty(language + ".email.duplicateValidationLabel"));
			}
			if (UtilValidate.isEmpty(user.getActive())) {
				errorMessageMap.put("active_validationMessage",
						userPropertyValue.getProperty(language + ".active.requiredValidationLabel"));
			}
			/*if (UtilValidate.isEmpty(user.getUserGroup())) {
				errorMessageMap.put("userGroup_validationMessage",
						userPropertyValue.getProperty(language + ".userGroup.requiredValidationLabel"));
			}*/
		} catch (IOException e) {
			e.printStackTrace();
		}

		return errorMessageMap;
	}

	public Boolean checkNull(User user) {
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
