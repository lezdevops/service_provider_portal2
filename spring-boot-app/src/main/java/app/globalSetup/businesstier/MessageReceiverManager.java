package app.globalSetup.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.MessageReceiver;
import app.appmodel.User;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.MessageReceiverRepository;

@Service
public class MessageReceiverManager {
	private static final org.apache.log4j.Logger MessageReceiverManagerLogger = org.apache.log4j.Logger
			.getLogger(MessageReceiverManager.class);
	@Autowired
	private MessageReceiverRepository messageReceiverRepository;
	@Autowired
	private MessageReceiverValidator messageReceiverValidator;
	@Autowired
	private NotificationManager notificationManager;
	@Autowired
	private ApprovalQueueManager approvalQueueManager;

	// newAutowiredPropertiesGoesHere
	public Map<String, Object> getMessageReceiverForEdit(HttpServletRequest request, Long id) {
		MessageReceiver newMessageReceiver = new MessageReceiver();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("MESSAGERECEIVER_EDIT");

		returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		MessageReceiver messageReceiver = null;
		try {
			messageReceiver = messageReceiverRepository.getOne(id);
			if (UtilValidate.isNotEmpty(messageReceiver)) {
				newMessageReceiver.setId(messageReceiver.getId());
				newMessageReceiver.setName(messageReceiver.getName());
				newMessageReceiver.setDesignation(messageReceiver.getDesignation());
				newMessageReceiver.setIsActive(messageReceiver.getIsActive());

			}
			returnResult.put("messageReceiver", newMessageReceiver);
		} catch (Exception e) {
			MessageReceiverManagerLogger
					.info("Exception Occured During Fetching MessageReceiver Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public Map<String, Object> getMessageReceiver(HttpServletRequest request, Long id) {
		MessageReceiver newMessageReceiver = new MessageReceiver();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("MESSAGERECEIVER_READ");

		returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			MessageReceiver messageReceiver = messageReceiverRepository.getOne(id);
			if (UtilValidate.isNotEmpty(messageReceiver)) {
				newMessageReceiver.setId(messageReceiver.getId());
				newMessageReceiver.setName(messageReceiver.getName());
				newMessageReceiver.setDesignation(messageReceiver.getDesignation());
				newMessageReceiver.setIsActive(messageReceiver.getIsActive());

			}

			returnResult.put("messageReceiver", newMessageReceiver);
		} catch (Exception e) {
			MessageReceiverManagerLogger
					.info("Exception Occured During Fetching MessageReceiver Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}

	public Map<String, Object> getMessageReceivers(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("MESSAGERECEIVER_READ");

		returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			List<MessageReceiver> messageReceiversList = new ArrayList<MessageReceiver>();
			Iterable<MessageReceiver> messageReceivers = messageReceiverRepository.findAll();
			for (MessageReceiver messageReceiver : messageReceivers) {
				messageReceiversList.add(messageReceiver);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("messageReceivers", messageReceiversList);
		} catch (Exception e) {
			MessageReceiverManagerLogger
					.info("Exception Occured During Fetching MessageReceiver List Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public List<MessageReceiver> getMessageReceiveresByName(String name, Long id) {
		List<MessageReceiver> messageReceiversList = new ArrayList<MessageReceiver>();
		Iterable<MessageReceiver> messageReceivers = null;
		if (UtilValidate.isNotEmpty(id)) {
			messageReceivers = messageReceiverRepository.findByNameDuringUpdate(name, id);
		} else {
			messageReceivers = messageReceiverRepository.findByName(name);
		}
		for (MessageReceiver messageReceiver : messageReceivers) {
			messageReceiversList.add(messageReceiver);
		}
		return messageReceiversList;
	}



	public Map<String, Object> create(HttpServletRequest request, MessageReceiver messageReceiver) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("MESSAGERECEIVER_CREATE");

			returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = messageReceiverValidator.getValidated(request, messageReceiver);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/messageReceiver");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				messageReceiver = messageReceiverRepository.save(messageReceiver);
				notificationManager.send(request, null, messageReceiver, "MESSAGERECEIVER_CREATE");
				approvalQueueManager.saveApprovalQueue(request, messageReceiver, "MESSAGERECEIVER_CREATE");
				returnResult.put("successMessage", "Successfully Saved MessageReceiver Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			MessageReceiverManagerLogger
					.info("Exception Occured During Creating MessageReceiver Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> update(HttpServletRequest request, MessageReceiver messageReceiver) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("MESSAGERECEIVER_EDIT");

			returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = messageReceiverValidator.getValidated(request, messageReceiver);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/messageReceiver");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				messageReceiver = messageReceiverRepository.save(messageReceiver);
				returnResult.put("successMessage", "Successfully Saved MessageReceiver Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(MessageReceiverManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			MessageReceiverManagerLogger
					.info("Exception Occured During Updating MessageReceiver Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> saveMultipleMessageReceiverData(HttpServletRequest request,
			List<MessageReceiver> messageReceivers) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("MESSAGERECEIVER_CREATE");

		returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}

		try {
			for (MessageReceiver messageReceiver : messageReceivers) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = messageReceiverValidator.getValidated(request, messageReceiver);
				messageReceiver = messageReceiverRepository.save(messageReceiver);
				returnResult.put(messageReceiver + " " + messageReceiver.getId() + " - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: " + e.getMessage());
			MessageReceiverManagerLogger
					.info("Exception Occured During Adding MessageReceiver List Data. caused by: " + e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> remove(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully MessageReceiver Deleted. Id: " + id + ".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("MESSAGERECEIVER_DELETE");

		returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			returnResult.put("error", "found");
			return returnResult;
		}

		try {
			MessageReceiver messageReceiver = messageReceiverRepository.findOne(id);
			if (UtilValidate.isNotEmpty(messageReceiver)) {
				messageReceiverRepository.delete(id);

			}

		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: " + e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			MessageReceiverManagerLogger
					.info("Exception Occured During Removing MessageReceiver Data. caused by: " + e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);

		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}

	public Map<String, Object> removeAll(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("MESSAGERECEIVER_DELETE");

		returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		String removeMessage = "Successfully All MessageReceiver Deleted.";
		try {
			messageReceiverRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			MessageReceiverManagerLogger
					.info("Exception Occured During Removing MessageReceiver Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("MESSAGERECEIVER_READ");

		returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<MessageReceiver> paginatedResult = messageReceiverRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			MessageReceiverManagerLogger.info(
					"Exception Occured During Fetching Paginated MessageReceiver Data. caused by: " + e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> findPaginatedMessageReceiversBySearchToken(HttpServletRequest request, int page,
			int size, String searchToken) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("MESSAGERECEIVER_READ");

		returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<MessageReceiver> paginatedResultBySearchToken = messageReceiverRepository
					.findPaginatedMessageReceiversBySearchToken(searchToken, new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			MessageReceiverManagerLogger
					.info("Exception Occured During Fetching Paginated MessageReceiver Data Search. caused by: "
							+ e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findOne(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("MESSAGERECEIVER_READ");

		returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			MessageReceiver messageReceiver = messageReceiverRepository.findOne(id);
			returnResult.put("messageReceiver", messageReceiver);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			MessageReceiverManagerLogger
					.info("Exception Occured During Fetching MessageReceiver Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<MessageReceiver> messageReceivers = new ArrayList<MessageReceiver>();
		allowedPermissions.add("MESSAGERECEIVER_READ");

		returnResult = messageReceiverValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			messageReceivers = messageReceiverRepository.findById(id);
			Page<MessageReceiver> pages = new PageImpl<MessageReceiver>(messageReceivers, pageRequest,
					messageReceivers.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			MessageReceiverManagerLogger
					.info("Exception Occured During Fetching MessageReceiver Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

}
