package app.globalSetup.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.ApprovalQueueDetails;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.ApprovalQueueDetailsRepository;

@Service
public class ApprovalQueueDetailsManager {
	private static final org.apache.log4j.Logger ApprovalQueueDetailsManagerLogger = org.apache.log4j.Logger
			.getLogger(ApprovalQueueDetailsManager.class);
	@Autowired
	private ApprovalQueueDetailsRepository approvalQueueDetailsRepository;
	@Autowired
	private ApprovalQueueDetailsValidator approvalQueueDetailsValidator;

	// newAutowiredPropertiesGoesHere
	public Map<String, Object> getApprovalQueueDetailsForEdit(HttpServletRequest request, Long id) {
		ApprovalQueueDetails newApprovalQueueDetails = new ApprovalQueueDetails();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUEDETAILS_EDIT");

		returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		ApprovalQueueDetails approvalQueueDetails = null;
		try {
			approvalQueueDetails = approvalQueueDetailsRepository.getOne(id);
			if (UtilValidate.isNotEmpty(approvalQueueDetails)) {
				newApprovalQueueDetails.setId(approvalQueueDetails.getId());
				newApprovalQueueDetails.setApprovalQueue(approvalQueueDetails.getApprovalQueue());
				newApprovalQueueDetails.setApproverId(approvalQueueDetails.getApproverId());
				newApprovalQueueDetails.setLevel(approvalQueueDetails.getLevel());
				newApprovalQueueDetails.setIsLastApprover(approvalQueueDetails.getIsLastApprover());
				newApprovalQueueDetails.setIsApproved(approvalQueueDetails.getIsApproved());

			}
			returnResult.put("approvalQueueDetails", newApprovalQueueDetails);
		} catch (Exception e) {
			ApprovalQueueDetailsManagerLogger
					.info("Exception Occured During Fetching ApprovalQueueDetails Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public Map<String, Object> getApprovalQueueDetails(HttpServletRequest request, Long id) {
		ApprovalQueueDetails newApprovalQueueDetails = new ApprovalQueueDetails();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUEDETAILS_READ");

		returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			ApprovalQueueDetails approvalQueueDetails = approvalQueueDetailsRepository.getOne(id);
			if (UtilValidate.isNotEmpty(approvalQueueDetails)) {
				newApprovalQueueDetails.setId(approvalQueueDetails.getId());
				newApprovalQueueDetails.setApprovalQueue(approvalQueueDetails.getApprovalQueue());
				newApprovalQueueDetails.setApproverId(approvalQueueDetails.getApproverId());
				newApprovalQueueDetails.setLevel(approvalQueueDetails.getLevel());
				newApprovalQueueDetails.setIsLastApprover(approvalQueueDetails.getIsLastApprover());
				newApprovalQueueDetails.setIsApproved(approvalQueueDetails.getIsApproved());

			}

			returnResult.put("approvalQueueDetails", newApprovalQueueDetails);
		} catch (Exception e) {
			ApprovalQueueDetailsManagerLogger
					.info("Exception Occured During Fetching ApprovalQueueDetails Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}

	public Map<String, Object> getApprovalQueueDetailss(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUEDETAILS_READ");

		returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			List<ApprovalQueueDetails> approvalQueueDetailssList = new ArrayList<ApprovalQueueDetails>();
			Iterable<ApprovalQueueDetails> approvalQueueDetailss = approvalQueueDetailsRepository.findAll();
			for (ApprovalQueueDetails approvalQueueDetails : approvalQueueDetailss) {
				approvalQueueDetailssList.add(approvalQueueDetails);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("approvalQueueDetailss", approvalQueueDetailssList);
		} catch (Exception e) {
			ApprovalQueueDetailsManagerLogger.info(
					"Exception Occured During Fetching ApprovalQueueDetails List Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public List<ApprovalQueueDetails> getApprovalQueueDetailsesByApproverId(String approverId) {
		List<ApprovalQueueDetails> approvalQueueDetailsList = new ArrayList<ApprovalQueueDetails>();
		Iterable<ApprovalQueueDetails> approvalQueueDetailss = null;

		if (UtilValidate.isNotEmpty(approverId)) {
			approvalQueueDetailss = approvalQueueDetailsRepository.findByApproverIdAndIsApproved(approverId,false);
			for (ApprovalQueueDetails approvalQueueDetails : approvalQueueDetailss) {
				approvalQueueDetailsList.add(approvalQueueDetails);
			}
		}
		return approvalQueueDetailsList;
	}

	public Map<String, Object> create(HttpServletRequest request, ApprovalQueueDetails approvalQueueDetails) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("APPROVALQUEUEDETAILS_CREATE");

			returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = approvalQueueDetailsValidator.getValidated(request, approvalQueueDetails);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/approvalQueueDetails");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				approvalQueueDetails = approvalQueueDetailsRepository.save(approvalQueueDetails);
				returnResult.put("successMessage", "Successfully Saved ApprovalQueueDetails Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			ApprovalQueueDetailsManagerLogger
					.info("Exception Occured During Creating ApprovalQueueDetails Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public void saveAll(List<ApprovalQueueDetails> approvalQueueDetailsList) {
		try {
			for (ApprovalQueueDetails approvalQueueDetails : approvalQueueDetailsList) {
				approvalQueueDetails = approvalQueueDetailsRepository.save(approvalQueueDetails);
			}
			// newGenerateEntityAddActionGoesHere
		} catch (Exception e) {
			ApprovalQueueDetailsManagerLogger
					.info("Exception Occured During Creating ApprovalQueueDetails Data. caused by: " + e.getMessage());

		}
	}

	public Map<String, Object> update(HttpServletRequest request, ApprovalQueueDetails approvalQueueDetails) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("APPROVALQUEUEDETAILS_EDIT");

			returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = approvalQueueDetailsValidator.getValidated(request, approvalQueueDetails);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/approvalQueueDetails");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				approvalQueueDetails = approvalQueueDetailsRepository.save(approvalQueueDetails);
				returnResult.put("successMessage", "Successfully Saved ApprovalQueueDetails Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ApprovalQueueDetailsManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			ApprovalQueueDetailsManagerLogger
					.info("Exception Occured During Updating ApprovalQueueDetails Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> saveMultipleApprovalQueueDetailsData(HttpServletRequest request,
			List<ApprovalQueueDetails> approvalQueueDetailss) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUEDETAILS_CREATE");

		returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}

		try {
			for (ApprovalQueueDetails approvalQueueDetails : approvalQueueDetailss) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = approvalQueueDetailsValidator.getValidated(request, approvalQueueDetails);
				approvalQueueDetails = approvalQueueDetailsRepository.save(approvalQueueDetails);
				returnResult.put(approvalQueueDetails + " " + approvalQueueDetails.getId() + " - Result",
						"Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: " + e.getMessage());
			ApprovalQueueDetailsManagerLogger.info(
					"Exception Occured During Adding ApprovalQueueDetails List Data. caused by: " + e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> remove(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully ApprovalQueueDetails Deleted. Id: " + id + ".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUEDETAILS_DELETE");

		returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			returnResult.put("error", "found");
			return returnResult;
		}

		try {
			ApprovalQueueDetails approvalQueueDetails = approvalQueueDetailsRepository.findOne(id);
			if (UtilValidate.isNotEmpty(approvalQueueDetails)) {
				approvalQueueDetailsRepository.delete(id);

			}

		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: " + e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			ApprovalQueueDetailsManagerLogger
					.info("Exception Occured During Removing ApprovalQueueDetails Data. caused by: " + e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);

		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}

	public Map<String, Object> removeAll(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUEDETAILS_DELETE");

		returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		String removeMessage = "Successfully All ApprovalQueueDetails Deleted.";
		try {
			approvalQueueDetailsRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApprovalQueueDetailsManagerLogger
					.info("Exception Occured During Removing ApprovalQueueDetails Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUEDETAILS_READ");

		returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<ApprovalQueueDetails> paginatedResult = approvalQueueDetailsRepository
					.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApprovalQueueDetailsManagerLogger
					.info("Exception Occured During Fetching Paginated ApprovalQueueDetails Data. caused by: "
							+ e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> findPaginatedApprovalQueueDetailssBySearchToken(HttpServletRequest request, int page,
			int size, String searchToken) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUEDETAILS_READ");

		returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<ApprovalQueueDetails> paginatedResultBySearchToken = approvalQueueDetailsRepository
					.findPaginatedApprovalQueueDetailssBySearchToken(searchToken, new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApprovalQueueDetailsManagerLogger
					.info("Exception Occured During Fetching Paginated ApprovalQueueDetails Data Search. caused by: "
							+ e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findOne(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUEDETAILS_READ");

		returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			ApprovalQueueDetails approvalQueueDetails = approvalQueueDetailsRepository.findOne(id);
			returnResult.put("approvalQueueDetails", approvalQueueDetails);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApprovalQueueDetailsManagerLogger
					.info("Exception Occured During Fetching ApprovalQueueDetails Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<ApprovalQueueDetails> approvalQueueDetailss = new ArrayList<ApprovalQueueDetails>();
		allowedPermissions.add("APPROVALQUEUEDETAILS_READ");

		returnResult = approvalQueueDetailsValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			approvalQueueDetailss = approvalQueueDetailsRepository.findById(id);
			Page<ApprovalQueueDetails> pages = new PageImpl<ApprovalQueueDetails>(approvalQueueDetailss, pageRequest,
					approvalQueueDetailss.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApprovalQueueDetailsManagerLogger
					.info("Exception Occured During Fetching ApprovalQueueDetails Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

}
