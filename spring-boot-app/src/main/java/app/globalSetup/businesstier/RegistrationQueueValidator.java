package app.globalSetup.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.RegistrationQueue;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.RegistrationQueueManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class RegistrationQueueValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private RegistrationQueueManager registrationQueueManager;

	public void validate(RegistrationQueue registrationQueue) throws Exception {
		if (!checkNull(registrationQueue)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}

	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("permission_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}

	public Map<String, Object> getValidated(HttpServletRequest request, RegistrationQueue registrationQueue) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		try {

			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility registrationQueueProperties = new ConfigUtility(
					new File("i18n/globalSetup/registrationQueue_i18n.properties"));
			Properties registrationQueuePropertyValue = registrationQueueProperties.loadProperties();

			if (UtilValidate.isEmpty(registrationQueue.getEmail())) {
				errorMessageMap.put("email_validationMessage",
						registrationQueuePropertyValue.getProperty(language + ".email.requiredValidationLabel"));
			}
			if (UtilValidate
					.isNotEmpty(registrationQueueManager.getRegistrationQueueesByEmail(registrationQueue.getEmail()))) {
				errorMessageMap.put("email_validationMessage",
						registrationQueuePropertyValue.getProperty(language + ".email.duplicateValidationLabel"));
			}
			if (UtilValidate.isEmpty(registrationQueue.getUserName())) {
				errorMessageMap.put("userName_validationMessage",
						registrationQueuePropertyValue.getProperty(language + ".userName.requiredValidationLabel"));
			}
			if (UtilValidate.isEmpty(registrationQueue.getPassword())) {
				errorMessageMap.put("password_validationMessage",
						registrationQueuePropertyValue.getProperty(language + ".password.requiredValidationLabel"));
			}
			if (UtilValidate.isEmpty(registrationQueue.getSecurityNumber())) {
				errorMessageMap.put("securityNumber_validationMessage", registrationQueuePropertyValue
						.getProperty(language + ".securityNumber.requiredValidationLabel"));
			}
			if (UtilValidate.isEmpty(registrationQueue.getSecurityQuestion())) {
				errorMessageMap.put("securityQuestion_validationMessage", registrationQueuePropertyValue
						.getProperty(language + ".securityQuestion.requiredValidationLabel"));
			}
			if (UtilValidate.isEmpty(registrationQueue.getAnswer())) {
				errorMessageMap.put("answer_validationMessage",
						registrationQueuePropertyValue.getProperty(language + ".answer.requiredValidationLabel"));
			}
			if (UtilValidate.isEmpty(registrationQueue.getAccountType())) {
				errorMessageMap.put("accountType_validationMessage",
						registrationQueuePropertyValue.getProperty(language + ".accountType.requiredValidationLabel"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return errorMessageMap;
	}

	public Boolean checkNull(RegistrationQueue registrationQueue) {
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
