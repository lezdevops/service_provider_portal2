package app.globalSetup.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.Role;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.RoleRepository;


//newDependentBeanImportGoesHere

@Service
public class RoleManager {
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private RoleValidator roleValidator;
	// newAutowiredPropertiesGoesHere
	
	public Map<String,Object> getRole(HttpServletRequest request, Long id){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("ROLE_READ");
		
		returnResult = roleValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		Role role = roleRepository.findRoleById(id);
		returnResult.put("role", role);
		return returnResult;
	}
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("ROLE_READ");
		
		returnResult = roleValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		Role role = roleRepository.findOne(id);
		returnResult.put("role", role);
		return returnResult;
	}
	public Map<String,Object> getRoles(HttpServletRequest request){
		Map<String,Object> resultMessage = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("ROLE_READ");
		
		resultMessage = roleValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(resultMessage) && resultMessage.keySet().size()>0) {
			return resultMessage;
		}
		List<Role> rolesList = new ArrayList<Role>();
		Iterable<Role> roles = roleRepository.findAll();
		for (Role role : roles) {
			rolesList.add(role);
			// newGenerateEntityFetchActionGoesHere
		}
		resultMessage.put("roles", rolesList);
		return resultMessage;
	}
	public Map<String,Object> save(HttpServletRequest request, Role role) {
		Map<String,Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("ROLE_CREATE");
			
			returnResult = roleValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
				returnResult = roleValidator.getValidated(request,role);
			}
			
			if (returnResult.keySet().size()>0) {
				messageMap.put("error", "found");
				messageMap.putAll(returnResult);
			}else{
				File path = new File("uploads/role");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				role = roleRepository.save(role);
				messageMap.put("successMessage", "Successfully Saved Role Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(RoleManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: "+e.getMessage());
			return messageMap;
		}
	}
	
	public Map<String,Object> saveMultipleRoleData(HttpServletRequest request, List<Role> roles) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("ROLE_CREATE");
		
		returnResult = roleValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		List<Map<String,Object>> messageList = new ArrayList<Map<String,Object>>();
		Map<String,Object> messageMap = new LinkedHashMap();
		try {	
			for (Role role : roles) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = roleValidator.getValidated(request, role);
				role = roleRepository.save(role);
				messageMap.put("addSucessmessage", "Successfully Role Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}
			
			return getRoles(request);
		} catch (Exception e) {
			Logger logger = Logger.getLogger(RoleManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: "+e.getMessage());
			messageList.add(messageMap);
		}
		return getRoles(request);
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> resultMessage = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("ROLE_DELETE");
		
		resultMessage = roleValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(resultMessage) && resultMessage.keySet().size()>0) {
			return resultMessage;
		}
		String removeMessage = "Successfully Role Deleted. Id: "+id+".";
		try {
			Role role = roleRepository.findOne(id);
			if (UtilValidate.isNotEmpty(role)) {
				roleRepository.delete(id);
				resultMessage.put("success", removeMessage);
			}
			
		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			resultMessage.put("error", removeMessage);
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return resultMessage;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> responseResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("ROLE_DELETE");
		
		responseResult = roleValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(responseResult) && responseResult.keySet().size()>0) {
			return responseResult;
		}
		
		String removeMessage = "Successfully All Role Deleted.";
		try {
			roleRepository.deleteAll();
			responseResult.put("message", removeMessage);
		} catch (Exception e) {
			responseResult.put("error", "Error During Delete, Cause: "+e.getMessage());
		}
		
		// newGenerateEntityDeleteActionGoesHere
		return responseResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("ROLE_READ");
		
		returnResult = roleValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		Page<Role> paginatedResult = roleRepository.findAll(new PageRequest(page, size));
		returnResult.put("paginatedResult", paginatedResult);
        return returnResult;
    }
	public Map<String,Object> findPaginatedRolesBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("ROLE_READ");
		
		returnResult = roleValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		Page<Role> paginatedResultBySearchToken = roleRepository.findPaginatedRolesBySearchToken(searchToken,  new PageRequest(page, size));
		returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		return returnResult;
    }
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("ROLE_READ");
		
		returnResult = roleValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		List<Role> roles = roleRepository.findById(id);
		Page<Role> pages = new PageImpl<Role>(roles, pageRequest, roles.size());
		returnResult.put("resultPages", pages);
		return returnResult;
	}
	public Map<String,Object> findByName(HttpServletRequest request, String name) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("ROLE_READ");
		
		returnResult = roleValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		Role role = roleRepository.findByName(name);
		returnResult.put("role", role);
		return returnResult;
	}
		
}
