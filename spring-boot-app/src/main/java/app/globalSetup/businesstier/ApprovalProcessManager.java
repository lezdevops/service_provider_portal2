package app.globalSetup.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.ApprovalProcess;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.ApprovalProcessRepository;


@Service
public class ApprovalProcessManager {
	private static final org.apache.log4j.Logger ApprovalProcessManagerLogger = org.apache.log4j.Logger.getLogger(ApprovalProcessManager.class);
	@Autowired
	private ApprovalProcessRepository approvalProcessRepository;
	@Autowired
	private ApprovalProcessValidator approvalProcessValidator;
	@Autowired
	private NotificationManager notificationManager;
	@Autowired
	private ApprovalQueueManager approvalQueueManager;
	
	// newAutowiredPropertiesGoesHere
	public Map<String,Object> getApprovalProcessForEdit(HttpServletRequest request, Long id){
		ApprovalProcess newApprovalProcess = new ApprovalProcess();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALPROCESS_EDIT");
		
		returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		ApprovalProcess approvalProcess = null;
		try {
			approvalProcess = approvalProcessRepository.getOne(id);
			if (UtilValidate.isNotEmpty(approvalProcess)) {
				newApprovalProcess.setId(approvalProcess.getId());
newApprovalProcess.setProcessName(approvalProcess.getProcessName());
newApprovalProcess.setIsActive(approvalProcess.getIsActive());
newApprovalProcess.setCreatedBy(approvalProcess.getCreatedBy());
newApprovalProcess.setCreated(approvalProcess.getCreated());

				
			}
			returnResult.put("approvalProcess", newApprovalProcess);
		} catch (Exception e) {
			ApprovalProcessManagerLogger.info("Exception Occured During Fetching ApprovalProcess Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}

	public ApprovalProcess findApprovalProcessByName(String approvalProcessName) {

		ApprovalProcess approvalProcess = null;
		try {
			approvalProcess = approvalProcessRepository.findApprovalProcessByProcessName(approvalProcessName);
		} catch (Exception e) {
			ApprovalProcessManagerLogger.info("Exception Occured During Fetching ApprovalProcess Data. caused by: " + e.getMessage());			
		}

		return approvalProcess;
	}
	public Map<String,Object> getApprovalProcess(HttpServletRequest request, Long id){
		ApprovalProcess newApprovalProcess = new ApprovalProcess();
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALPROCESS_READ");
		
		returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			ApprovalProcess approvalProcess = approvalProcessRepository.getOne(id);
			if (UtilValidate.isNotEmpty(approvalProcess)) {
				newApprovalProcess.setId(approvalProcess.getId());
newApprovalProcess.setProcessName(approvalProcess.getProcessName());
newApprovalProcess.setIsActive(approvalProcess.getIsActive());
newApprovalProcess.setCreatedBy(approvalProcess.getCreatedBy());
newApprovalProcess.setCreated(approvalProcess.getCreated());

				
			}
			
			returnResult.put("approvalProcess", newApprovalProcess);
		} catch (Exception e) {
			ApprovalProcessManagerLogger.info("Exception Occured During Fetching ApprovalProcess Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}
	public Map<String,Object> getApprovalProcesss(HttpServletRequest request){
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALPROCESS_READ");
		
		returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			List<ApprovalProcess> approvalProcesssList = new ArrayList<ApprovalProcess>();
			Iterable<ApprovalProcess> approvalProcesss = approvalProcessRepository.findAll();
			for (ApprovalProcess approvalProcess : approvalProcesss) {
				approvalProcesssList.add(approvalProcess);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("approvalProcesss", approvalProcesssList);
		} catch (Exception e) {
			ApprovalProcessManagerLogger.info("Exception Occured During Fetching ApprovalProcess List Data. caused by: "+e.getMessage());
			returnResult.put("error", "found");
		}
		
		return returnResult;
	}
	
	public Map<String,Object> create(HttpServletRequest request, ApprovalProcess approvalProcess) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("APPROVALPROCESS_CREATE");
			
			returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = approvalProcessValidator.getValidated(request,approvalProcess);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/approvalProcess");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				approvalProcess = approvalProcessRepository.save(approvalProcess);
				notificationManager.send(request, null, approvalProcess, "APPROVALPROCESS_CREATE");
				approvalQueueManager.saveApprovalQueue(request, approvalProcess, "APPROVALPROCESS_CREATE");
				returnResult.put("successMessage", "Successfully Saved ApprovalProcess Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			ApprovalProcessManagerLogger.info("Exception Occured During Creating ApprovalProcess Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	public Map<String,Object> update(HttpServletRequest request, ApprovalProcess approvalProcess) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("APPROVALPROCESS_EDIT");
			
			returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
			
			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()==0) {
				returnResult = approvalProcessValidator.getValidated(request,approvalProcess);
			}
			
			if (returnResult.keySet().size()>0) {
				returnResult.put("error", "found");
			}else{
				File path = new File("uploads/approvalProcess");
				if (!path.exists()) {
					//boolean status = path.mkdirs();
					path.mkdirs();
				}
				
				approvalProcess = approvalProcessRepository.save(approvalProcess);
				returnResult.put("successMessage", "Successfully Saved ApprovalProcess Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ApprovalProcessManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			ApprovalProcessManagerLogger.info("Exception Occured During Updating ApprovalProcess Data. caused by: "+e.getMessage());
			return returnResult;
		}
	}
	
	public Map<String,Object> saveMultipleApprovalProcessData(HttpServletRequest request, List<ApprovalProcess> approvalProcesss) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALPROCESS_CREATE");
		
		returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		
		try {	
			for (ApprovalProcess approvalProcess : approvalProcesss) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = approvalProcessValidator.getValidated(request, approvalProcess);
				approvalProcess = approvalProcessRepository.save(approvalProcess);
				returnResult.put(approvalProcess+" "+approvalProcess.getId()+" - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: "+e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: "+e.getMessage());
			ApprovalProcessManagerLogger.info("Exception Occured During Adding ApprovalProcess List Data. caused by: "+e.getMessage());
		}
		return returnResult;
	}
	public Map<String,Object> remove(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully ApprovalProcess Deleted. Id: "+id+".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALPROCESS_DELETE");
		
		returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			returnResult.put("error", "found");
			return returnResult;
		}
		
		try {
			ApprovalProcess approvalProcess = approvalProcessRepository.findOne(id);
			if (UtilValidate.isNotEmpty(approvalProcess)) {
				approvalProcessRepository.delete(id);
				
			}
			
		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: "+e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			ApprovalProcessManagerLogger.info("Exception Occured During Removing ApprovalProcess Data. caused by: "+e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);
		
		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}
	public Map<String,Object> removeAll(HttpServletRequest request) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALPROCESS_DELETE");
		
		returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		String removeMessage = "Successfully All ApprovalProcess Deleted.";
		try {
			approvalProcessRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			ApprovalProcessManagerLogger.info("Exception Occured During Removing ApprovalProcess Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALPROCESS_READ");
		
		returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<ApprovalProcess> paginatedResult = approvalProcessRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			ApprovalProcessManagerLogger.info("Exception Occured During Fetching Paginated ApprovalProcess Data. caused by: "+e.getMessage());
		}
        return returnResult;
    }
	public Map<String,Object> findPaginatedApprovalProcesssBySearchToken(HttpServletRequest request, int page, int size, String searchToken) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALPROCESS_READ");
		
		returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			Page<ApprovalProcess> paginatedResultBySearchToken = approvalProcessRepository.findPaginatedApprovalProcesssBySearchToken(searchToken,  new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			ApprovalProcessManagerLogger.info("Exception Occured During Fetching Paginated ApprovalProcess Data Search. caused by: "+e.getMessage());
		}
		
		return returnResult;
    }
	public Map<String,Object> findOne(HttpServletRequest request, Long id) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALPROCESS_READ");
		
		returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			ApprovalProcess approvalProcess = approvalProcessRepository.findOne(id);
			returnResult.put("approvalProcess", approvalProcess);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			ApprovalProcessManagerLogger.info("Exception Occured During Fetching ApprovalProcess Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	public Map<String,Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String,Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<ApprovalProcess> approvalProcesss = new ArrayList<ApprovalProcess>();
		allowedPermissions.add("APPROVALPROCESS_READ");
		
		returnResult = approvalProcessValidator.validatePermission(request, allowedPermissions);
		
		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size()>0) {
			return returnResult;
		}
		try {
			approvalProcesss = approvalProcessRepository.findById(id);
			Page<ApprovalProcess> pages = new PageImpl<ApprovalProcess>(approvalProcesss, pageRequest, approvalProcesss.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error","found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: "+e.getMessage());
			ApprovalProcessManagerLogger.info("Exception Occured During Fetching ApprovalProcess Data. caused by: "+e.getMessage());
		}
		
		return returnResult;
	}
	
}
