package app.globalSetup.businesstier;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import app.appmodel.ApprovalProcess;
import app.appmodel.ApprovalQueue;
import app.appmodel.ApprovalQueueDetails;
import app.appmodel.ApproverAdmin;
import app.appmodel.User;
import app.framework.businesstier.FileUploadUtils;
import app.framework.businesstier.HelperUtils;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.ApprovalQueueRepository;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class ApprovalQueueManager {
	private static final org.apache.log4j.Logger ApprovalQueueManagerLogger = org.apache.log4j.Logger
			.getLogger(ApprovalQueueManager.class);
	@Autowired
	private ApprovalQueueRepository approvalQueueRepository;
	@Autowired
	private ApprovalQueueValidator approvalQueueValidator;
	@Autowired
	private NotificationManager notificationManager;
	@Autowired
	private ApprovalProcessManager approvalProcessManager;
	@Autowired
	private ApprovalQueueDetailsManager approvalQueueDetailsManager;

	// newAutowiredPropertiesGoesHere
	public Map<String, Object> getApprovalQueueForEdit(HttpServletRequest request, Long id) {
		ApprovalQueue newApprovalQueue = new ApprovalQueue();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUE_EDIT");

		returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		ApprovalQueue approvalQueue = null;
		try {
			approvalQueue = approvalQueueRepository.getOne(id);
			if (UtilValidate.isNotEmpty(approvalQueue)) {
				newApprovalQueue.setId(approvalQueue.getId());
				newApprovalQueue.setApprovalProcess(approvalQueue.getApprovalProcess());
				newApprovalQueue.setIsActive(approvalQueue.getIsActive());
				newApprovalQueue.setApprovalMessage(approvalQueue.getApprovalMessage());

				String attachment = approvalQueue.getAttachment();
				if (UtilValidate.isNotEmpty(attachment)) {
					File file = new File(attachment);
					if (file.exists()) {
						String base64String = "";
						try {
							base64String = FileUploadUtils.getBase64StringFromFilePath(attachment);
							String extension = FileUploadUtils.getFileExtension(file.getName());

							base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);

						} catch (IOException e) {
							e.printStackTrace();
						}
						newApprovalQueue.setAttachment(base64String);
					}
				}

			}
			returnResult.put("approvalQueue", newApprovalQueue);
		} catch (Exception e) {
			ApprovalQueueManagerLogger
					.info("Exception Occured During Fetching ApprovalQueue Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public Map<String, Object> getApprovalQueue(HttpServletRequest request, Long id) {
		ApprovalQueue newApprovalQueue = new ApprovalQueue();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUE_READ");

		returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			ApprovalQueue approvalQueue = approvalQueueRepository.getOne(id);
			if (UtilValidate.isNotEmpty(approvalQueue)) {
				newApprovalQueue.setId(approvalQueue.getId());
				newApprovalQueue.setApprovalProcess(approvalQueue.getApprovalProcess());
				newApprovalQueue.setIsActive(approvalQueue.getIsActive());
				newApprovalQueue.setApprovalMessage(approvalQueue.getApprovalMessage());

				String attachment = approvalQueue.getAttachment();
				if (UtilValidate.isNotEmpty(attachment)) {
					File file = new File(attachment);
					if (file.exists()) {
						String base64String = "";
						try {
							base64String = FileUploadUtils.getBase64StringFromFilePath(attachment);
							String extension = FileUploadUtils.getFileExtension(file.getName());

							base64String = FileUploadUtils.getBase64StringWithFileTypeString(base64String, extension);

						} catch (IOException e) {
							e.printStackTrace();
						}
						newApprovalQueue.setAttachment(base64String);
					}
				}

			}

			returnResult.put("approvalQueue", newApprovalQueue);
		} catch (Exception e) {
			ApprovalQueueManagerLogger
					.info("Exception Occured During Fetching ApprovalQueue Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}

	public Map<String, Object> getApprovalQueues(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUE_READ");

		returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			List<ApprovalQueue> approvalQueuesList = new ArrayList<ApprovalQueue>();
			Iterable<ApprovalQueue> approvalQueues = approvalQueueRepository.findAll();
			for (ApprovalQueue approvalQueue : approvalQueues) {
				approvalQueuesList.add(approvalQueue);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("approvalQueues", approvalQueuesList);
		} catch (Exception e) {
			ApprovalQueueManagerLogger
					.info("Exception Occured During Fetching ApprovalQueue List Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	@Autowired
	private HttpRequestProcessor httpRequestProcessor;

	public List<ApprovalQueueDetails> getApprovalQueueDetailsList(HttpServletRequest request) {
		List<ApprovalQueueDetails> approvalQueueDetailsList = new ArrayList<ApprovalQueueDetails>();

		User user = httpRequestProcessor.getUser(request);
		String approverId = user.getUsername();

		approvalQueueDetailsList = approvalQueueDetailsManager.getApprovalQueueDetailsesByApproverId(approverId);
		return approvalQueueDetailsList;
	}

	public List<ApprovalQueue> getApprovalQueueesByApprovalProcess(ApprovalProcess ApprovalProcess, Long id) {
		List<ApprovalQueue> approvalQueuesList = new ArrayList<ApprovalQueue>();
		Iterable<ApprovalQueue> approvalQueues = null;
		if (UtilValidate.isNotEmpty(id)) {
			approvalQueues = approvalQueueRepository.findByApprovalProcessDuringUpdate(ApprovalProcess, id);
		} else {
			approvalQueues = approvalQueueRepository.findByApprovalProcess(ApprovalProcess);
		}
		for (ApprovalQueue approvalQueue : approvalQueues) {
			approvalQueuesList.add(approvalQueue);
		}
		return approvalQueuesList;
	}

	public Map<String, Object> create(HttpServletRequest request, ApprovalQueue approvalQueue) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("APPROVALQUEUE_CREATE");

			returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = approvalQueueValidator.getValidated(request, approvalQueue);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/approvalQueue");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}
				String attachment = approvalQueue.getAttachment();
				if (UtilValidate.isNotEmpty(attachment)) {
					if (UtilValidate.isNotEmpty(approvalQueue.getId())) {
						ApprovalQueue oldApprovalQueue = approvalQueueRepository
								.findApprovalQueueById(approvalQueue.getId());
						if (UtilValidate.isNotEmpty(oldApprovalQueue)) {
							String attachmentOld = oldApprovalQueue.getAttachment();
							if (UtilValidate.isNotEmpty(attachmentOld)) {
								File file = new File(attachmentOld);
								if (file.exists()) {
									FileUtils.forceDelete(file);
								}
							}
						}
					}
					long now = HelperUtils.nowTimestamp().getTime();
					Map<String, Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(attachment);
					String byteArray = (String) mapData.get("byteArray");
					String extension = (String) mapData.get("extension");
					String filePath = path + "/" + now + "." + extension;
					approvalQueue.setAttachment(filePath);
					FileUploadUtils.store(byteArray, filePath);
				}
				approvalQueue = approvalQueueRepository.save(approvalQueue);
				notificationManager.send(request, null, approvalQueue, "APPROVALQUEUE_CREATE");
				saveApprovalQueue(request, approvalQueue, "APPROVALQUEUE_CREATE");
				returnResult.put("successMessage", "Successfully Saved ApprovalQueue Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			ApprovalQueueManagerLogger
					.info("Exception Occured During Creating ApprovalQueue Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public void saveApprovalQueue(HttpServletRequest request, Object newValue, String approvalProcessStr) {
		Runnable runnable = () -> {

			Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
			try {
				ObjectMapper Obj = new ObjectMapper();
				try {
					ApprovalProcess approvalProcess = approvalProcessManager
							.findApprovalProcessByName(approvalProcessStr);
					if (UtilValidate.isNotEmpty(approvalProcess)
							&& approvalProcess.getIsActive().equalsIgnoreCase("Y")) {
						ApprovalQueue approvalQueue = new ApprovalQueue();
						if (UtilValidate.isNotEmpty(newValue)) {
							String newValueStr = Obj.writeValueAsString(newValue);
							approvalQueue.setApprovalMessage(newValueStr);
						}

						approvalQueue.setApprovalProcess(approvalProcess);
						approvalQueue.setIsActive(false);

						approvalQueue.setAttachment("");

						approvalQueue = approvalQueueRepository.save(approvalQueue);
						List<ApproverAdmin> approverAdmins = approvalProcess.getApproverAdmins();
						Long level = new Long(0);
						List<ApprovalQueueDetails> approvalQueueDetailsList = new ArrayList<ApprovalQueueDetails>();
						
						for (ApproverAdmin approverAdmin : approverAdmins) {
							ApprovalQueueDetails approvalQueueDetails = new ApprovalQueueDetails();
							approvalQueueDetails.setIsApproved(false);
							approvalQueueDetails.setApproverId(approverAdmin.getName());
							approvalQueueDetails.setApprovalQueue(approvalQueue);
							approvalQueueDetails.setLevel(level);
							approvalQueueDetails.setIsLastApprover(false);
							approvalQueueDetailsList.add(approvalQueueDetails);
							level++;
							
						}
						
						approvalQueueDetailsManager.saveAll(approvalQueueDetailsList);
					}

				} catch (IOException e) {
					e.printStackTrace();
				}

			} catch (Exception e) {
				returnResult.put("error", "found");
				returnResult.put("technicalError", "Techical Error: " + e.getMessage());
				ApprovalQueueManagerLogger
						.info("Exception Occured During Creating Notification Data. caused by: " + e.getMessage());

			}
		};

		System.out.println("Approval Process Initializing...");
		Thread thread = new Thread(runnable);

		System.out.println("Approval Queue Making...");
		thread.start();

	}

	public Map<String, Object> update(HttpServletRequest request, ApprovalQueue approvalQueue) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("APPROVALQUEUE_EDIT");

			returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = approvalQueueValidator.getValidated(request, approvalQueue);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/approvalQueue");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}
				String attachment = approvalQueue.getAttachment();
				if (UtilValidate.isNotEmpty(attachment)) {
					if (UtilValidate.isNotEmpty(approvalQueue.getId())) {
						ApprovalQueue oldApprovalQueue = approvalQueueRepository
								.findApprovalQueueById(approvalQueue.getId());
						if (UtilValidate.isNotEmpty(oldApprovalQueue)) {
							String attachmentOld = oldApprovalQueue.getAttachment();
							if (UtilValidate.isNotEmpty(attachmentOld)) {
								File file = new File(attachmentOld);
								if (file.exists()) {
									FileUtils.forceDelete(file);
								}
							}
						}
					}
					long now = HelperUtils.nowTimestamp().getTime();
					Map<String, Object> mapData = FileUploadUtils.getByteArrayAndExtensionFromBase64String(attachment);
					String byteArray = (String) mapData.get("byteArray");
					String extension = (String) mapData.get("extension");
					String filePath = path + "/" + now + "." + extension;
					approvalQueue.setAttachment(filePath);
					FileUploadUtils.store(byteArray, filePath);
				}
				approvalQueue = approvalQueueRepository.save(approvalQueue);
				returnResult.put("successMessage", "Successfully Saved ApprovalQueue Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ApprovalQueueManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			ApprovalQueueManagerLogger
					.info("Exception Occured During Updating ApprovalQueue Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> saveMultipleApprovalQueueData(HttpServletRequest request,
			List<ApprovalQueue> approvalQueues) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUE_CREATE");

		returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}

		try {
			for (ApprovalQueue approvalQueue : approvalQueues) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = approvalQueueValidator.getValidated(request, approvalQueue);
				approvalQueue = approvalQueueRepository.save(approvalQueue);
				returnResult.put(approvalQueue + " " + approvalQueue.getId() + " - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: " + e.getMessage());
			ApprovalQueueManagerLogger
					.info("Exception Occured During Adding ApprovalQueue List Data. caused by: " + e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> remove(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully ApprovalQueue Deleted. Id: " + id + ".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUE_DELETE");

		returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			returnResult.put("error", "found");
			return returnResult;
		}

		try {
			ApprovalQueue approvalQueue = approvalQueueRepository.findOne(id);
			if (UtilValidate.isNotEmpty(approvalQueue)) {
				approvalQueueRepository.delete(id);
				String attachment = approvalQueue.getAttachment();
				if (UtilValidate.isNotEmpty(attachment)) {
					File file = new File(attachment);
					if (file.exists()) {
						FileUtils.forceDelete(file);
					}
				}
			}

		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: " + e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			ApprovalQueueManagerLogger
					.info("Exception Occured During Removing ApprovalQueue Data. caused by: " + e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);

		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}

	public Map<String, Object> removeAll(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUE_DELETE");

		returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		String removeMessage = "Successfully All ApprovalQueue Deleted.";
		try {
			approvalQueueRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApprovalQueueManagerLogger
					.info("Exception Occured During Removing ApprovalQueue Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUE_READ");

		returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<ApprovalQueue> paginatedResult = approvalQueueRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApprovalQueueManagerLogger.info(
					"Exception Occured During Fetching Paginated ApprovalQueue Data. caused by: " + e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> findPaginatedApprovalQueuesBySearchToken(HttpServletRequest request, int page, int size,
			String searchToken) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUE_READ");

		returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<ApprovalQueue> paginatedResultBySearchToken = approvalQueueRepository
					.findPaginatedApprovalQueuesBySearchToken(searchToken, new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApprovalQueueManagerLogger
					.info("Exception Occured During Fetching Paginated ApprovalQueue Data Search. caused by: "
							+ e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findOne(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("APPROVALQUEUE_READ");

		returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			ApprovalQueue approvalQueue = approvalQueueRepository.findOne(id);
			returnResult.put("approvalQueue", approvalQueue);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApprovalQueueManagerLogger
					.info("Exception Occured During Fetching ApprovalQueue Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<ApprovalQueue> approvalQueues = new ArrayList<ApprovalQueue>();
		allowedPermissions.add("APPROVALQUEUE_READ");

		returnResult = approvalQueueValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			approvalQueues = approvalQueueRepository.findById(id);
			Page<ApprovalQueue> pages = new PageImpl<ApprovalQueue>(approvalQueues, pageRequest, approvalQueues.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			ApprovalQueueManagerLogger
					.info("Exception Occured During Fetching ApprovalQueue Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

}
