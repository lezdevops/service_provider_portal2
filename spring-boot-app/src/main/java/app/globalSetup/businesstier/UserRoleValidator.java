package app.globalSetup.businesstier;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.User;
import app.appmodel.UserRole;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class UserRoleValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private UserRoleManager userRoleManager;

	public void validate(UserRole userRole) throws Exception {
		if (!checkNull(userRole)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}

	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = null;
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("permission_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}

	public Map<String, Object> getValidated(HttpServletRequest request, UserRole userRole) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		try {

			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility userRoleProperties = new ConfigUtility(
					new File("i18n/globalSetup/userRole_i18n.properties"));
			//Properties userRolePropertyValue = userRoleProperties.loadProperties();

			/*if (UtilValidate.isEmpty(userRole.getUser())) {
				errorMessageMap.put("user_validationMessage",
						userRolePropertyValue.getProperty(language + ".user.requiredValidationLabel"));
			}
			if (UtilValidate.isNotEmpty(userRoleManager.getUserRoleesByUser(userRole.getUser()))) {
				errorMessageMap.put("user_validationMessage",
						userRolePropertyValue.getProperty(language + ".user.duplicateValidationLabel"));
			}
			if (UtilValidate.isEmpty(userRole.getRole())) {
				errorMessageMap.put("role_validationMessage",
						userRolePropertyValue.getProperty(language + ".role.requiredValidationLabel"));
			}*/
		} catch (IOException e) {
			e.printStackTrace();
		}

		return errorMessageMap;
	}

	public Boolean checkNull(UserRole userRole) {
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
