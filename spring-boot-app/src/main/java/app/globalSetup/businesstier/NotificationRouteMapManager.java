package app.globalSetup.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.appmodel.NotificationProcess;
import app.appmodel.NotificationRouteMap;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.NotificationRouteMapRepository;

@Service
public class NotificationRouteMapManager {
	private static final org.apache.log4j.Logger NotificationRouteMapManagerLogger = org.apache.log4j.Logger
			.getLogger(NotificationRouteMapManager.class);
	@Autowired
	private NotificationRouteMapRepository notificationRouteMapRepository;
	@Autowired
	private NotificationRouteMapValidator notificationRouteMapValidator;

	// newAutowiredPropertiesGoesHere
	public Map<String, Object> getNotificationRouteMapForEdit(HttpServletRequest request, Long id) {
		NotificationRouteMap newNotificationRouteMap = new NotificationRouteMap();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONROUTEMAP_EDIT");

		returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		NotificationRouteMap notificationRouteMap = null;
		try {
			notificationRouteMap = notificationRouteMapRepository.getOne(id);
			if (UtilValidate.isNotEmpty(notificationRouteMap)) {
				newNotificationRouteMap.setId(notificationRouteMap.getId());
				newNotificationRouteMap.setNotificationProcess(notificationRouteMap.getNotificationProcess());
				newNotificationRouteMap.setSenderId(notificationRouteMap.getSenderId());
				newNotificationRouteMap.setMessageReceivers(notificationRouteMap.getMessageReceivers());
				newNotificationRouteMap.setIsActive(notificationRouteMap.getIsActive());

			}
			returnResult.put("notificationRouteMap", newNotificationRouteMap);
		} catch (Exception e) {
			NotificationRouteMapManagerLogger
					.info("Exception Occured During Fetching NotificationRouteMap Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public Map<String, Object> getNotificationRouteMap(HttpServletRequest request, Long id) {
		NotificationRouteMap newNotificationRouteMap = new NotificationRouteMap();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONROUTEMAP_READ");

		returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			NotificationRouteMap notificationRouteMap = notificationRouteMapRepository.getOne(id);
			if (UtilValidate.isNotEmpty(notificationRouteMap)) {
				newNotificationRouteMap.setId(notificationRouteMap.getId());
				newNotificationRouteMap.setNotificationProcess(notificationRouteMap.getNotificationProcess());
				newNotificationRouteMap.setSenderId(notificationRouteMap.getSenderId());
				newNotificationRouteMap.setMessageReceivers(notificationRouteMap.getMessageReceivers());
				newNotificationRouteMap.setIsActive(notificationRouteMap.getIsActive());

			}

			returnResult.put("notificationRouteMap", newNotificationRouteMap);
		} catch (Exception e) {
			NotificationRouteMapManagerLogger
					.info("Exception Occured During Fetching NotificationRouteMap Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}

	public Map<String, Object> getNotificationRouteMaps(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONROUTEMAP_READ");

		returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			List<NotificationRouteMap> notificationRouteMapsList = new ArrayList<NotificationRouteMap>();
			Iterable<NotificationRouteMap> notificationRouteMaps = notificationRouteMapRepository.findAll();
			for (NotificationRouteMap notificationRouteMap : notificationRouteMaps) {
				notificationRouteMapsList.add(notificationRouteMap);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("notificationRouteMaps", notificationRouteMapsList);
		} catch (Exception e) {
			NotificationRouteMapManagerLogger.info(
					"Exception Occured During Fetching NotificationRouteMap List Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public List<NotificationRouteMap> getNotificationRouteMapesByNotificationProcess(
			NotificationProcess NotificationProcess, Long id) {
		List<NotificationRouteMap> notificationRouteMapsList = new ArrayList<NotificationRouteMap>();
		Iterable<NotificationRouteMap> notificationRouteMaps = null;
		if (UtilValidate.isNotEmpty(id)) {
			notificationRouteMaps = notificationRouteMapRepository
					.findByNotificationProcessDuringUpdate(NotificationProcess, id);
		} else {
			notificationRouteMaps = notificationRouteMapRepository.findByNotificationProcess(NotificationProcess);
		}
		for (NotificationRouteMap notificationRouteMap : notificationRouteMaps) {
			notificationRouteMapsList.add(notificationRouteMap);
		}
		return notificationRouteMapsList;
	}

	public Map<String, Object> create(HttpServletRequest request, NotificationRouteMap notificationRouteMap) {
		
		System.out.println("1: "+notificationRouteMap.getMessageReceivers());
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("NOTIFICATIONROUTEMAP_CREATE");

			returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = notificationRouteMapValidator.getValidated(request, notificationRouteMap);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/notificationRouteMap");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}
				System.out.println("2: "+notificationRouteMap.getMessageReceivers());
				notificationRouteMap = notificationRouteMapRepository.save(notificationRouteMap);
				returnResult.put("successMessage", "Successfully Saved NotificationRouteMap Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			NotificationRouteMapManagerLogger
					.info("Exception Occured During Creating NotificationRouteMap Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> update(HttpServletRequest request, NotificationRouteMap notificationRouteMap) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("NOTIFICATIONROUTEMAP_EDIT");

			returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = notificationRouteMapValidator.getValidated(request, notificationRouteMap);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/notificationRouteMap");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				notificationRouteMap = notificationRouteMapRepository.save(notificationRouteMap);
				returnResult.put("successMessage", "Successfully Saved NotificationRouteMap Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(NotificationRouteMapManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			NotificationRouteMapManagerLogger
					.info("Exception Occured During Updating NotificationRouteMap Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> saveMultipleNotificationRouteMapData(HttpServletRequest request,
			List<NotificationRouteMap> notificationRouteMaps) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONROUTEMAP_CREATE");

		returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}

		try {
			for (NotificationRouteMap notificationRouteMap : notificationRouteMaps) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = notificationRouteMapValidator.getValidated(request, notificationRouteMap);
				notificationRouteMap = notificationRouteMapRepository.save(notificationRouteMap);
				returnResult.put(notificationRouteMap + " " + notificationRouteMap.getId() + " - Result",
						"Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: " + e.getMessage());
			NotificationRouteMapManagerLogger.info(
					"Exception Occured During Adding NotificationRouteMap List Data. caused by: " + e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> remove(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully NotificationRouteMap Deleted. Id: " + id + ".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONROUTEMAP_DELETE");

		returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			returnResult.put("error", "found");
			return returnResult;
		}

		try {
			NotificationRouteMap notificationRouteMap = notificationRouteMapRepository.findOne(id);
			if (UtilValidate.isNotEmpty(notificationRouteMap)) {
				notificationRouteMapRepository.delete(id);

			}

		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: " + e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			NotificationRouteMapManagerLogger
					.info("Exception Occured During Removing NotificationRouteMap Data. caused by: " + e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);

		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}

	public Map<String, Object> removeAll(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONROUTEMAP_DELETE");

		returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		String removeMessage = "Successfully All NotificationRouteMap Deleted.";
		try {
			notificationRouteMapRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationRouteMapManagerLogger
					.info("Exception Occured During Removing NotificationRouteMap Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONROUTEMAP_READ");

		returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<NotificationRouteMap> paginatedResult = notificationRouteMapRepository
					.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationRouteMapManagerLogger
					.info("Exception Occured During Fetching Paginated NotificationRouteMap Data. caused by: "
							+ e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> findPaginatedNotificationRouteMapsBySearchToken(HttpServletRequest request, int page,
			int size, String searchToken) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONROUTEMAP_READ");

		returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<NotificationRouteMap> paginatedResultBySearchToken = notificationRouteMapRepository
					.findPaginatedNotificationRouteMapsBySearchToken(searchToken, new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationRouteMapManagerLogger
					.info("Exception Occured During Fetching Paginated NotificationRouteMap Data Search. caused by: "
							+ e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findOne(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATIONROUTEMAP_READ");

		returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			NotificationRouteMap notificationRouteMap = notificationRouteMapRepository.findOne(id);
			returnResult.put("notificationRouteMap", notificationRouteMap);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationRouteMapManagerLogger
					.info("Exception Occured During Fetching NotificationRouteMap Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public List<NotificationRouteMap> findByNotificationProcess(NotificationProcess notificationProcess) {
		List<NotificationRouteMap> notificationRouteMapList = null;

		try {
			notificationRouteMapList = notificationRouteMapRepository.findByNotificationProcess(notificationProcess);
		} catch (Exception e) {
			NotificationRouteMapManagerLogger
					.info("Exception Occured During Fetching NotificationProcess Data. caused by: " + e.getMessage());
		}

		return notificationRouteMapList;
	}

	public Map<String, Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<NotificationRouteMap> notificationRouteMaps = new ArrayList<NotificationRouteMap>();
		allowedPermissions.add("NOTIFICATIONROUTEMAP_READ");

		returnResult = notificationRouteMapValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			notificationRouteMaps = notificationRouteMapRepository.findById(id);
			Page<NotificationRouteMap> pages = new PageImpl<NotificationRouteMap>(notificationRouteMaps, pageRequest,
					notificationRouteMaps.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationRouteMapManagerLogger
					.info("Exception Occured During Fetching NotificationRouteMap Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

}
