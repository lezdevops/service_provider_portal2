package app.globalSetup.businesstier;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.ApproverAdmin;
import app.appmodel.Role;
import app.appmodel.User;
import app.framework.businesstier.ConfigUtility;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.businesstier.ApproverAdminManager;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class ApproverAdminValidator {
	@Autowired
	HttpRequestProcessor httpRequestProcessor;
	@Autowired
	private ApproverAdminManager approverAdminManager;

	public void validate(ApproverAdmin approverAdmin) throws Exception {
		if (!checkNull(approverAdmin)) {
			throw new Exception("Validator.MENUNAME_NOT_FOUND");
		}
	}

	public Map<String, Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		boolean permitted = false;
		User user = httpRequestProcessor.getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (httpRequestProcessor.hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		} else {
			permitted = true;
		}
		if (!permitted) {
			errorMessageMap = new LinkedHashMap<String, Object>();
			errorMessageMap.put("common_validationMessage", "Permission Not Found!");
		}
		return errorMessageMap;
	}

	public Map<String, Object> getValidated(HttpServletRequest request, ApproverAdmin approverAdmin) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();
		try {

			ConfigUtility i18nConfig = new ConfigUtility(new File("i18n/i18n_config.properties"));
			Properties i18nConfigProperties = i18nConfig.loadProperties();
			String language = i18nConfigProperties.getProperty("enabled.language");
			ConfigUtility approverAdminProperties = new ConfigUtility(
					new File("i18n/globalSetup/approverAdmin_i18n.properties"));
			Properties approverAdminPropertyValue = approverAdminProperties.loadProperties();

			if (UtilValidate.isEmpty(approverAdmin.getName())) {
				errorMessageMap.put("name_validationMessage",
						approverAdminPropertyValue.getProperty(language + ".name.requiredValidationLabel"));
			}
			if (UtilValidate.isNotEmpty(
					approverAdminManager.getApproverAdminesByName(approverAdmin.getName(), approverAdmin.getId()))) {
				errorMessageMap.put("name_validationMessage",
						approverAdminPropertyValue.getProperty(language + ".name.duplicateValidationLabel"));
			}

			if (UtilValidate.isEmpty(approverAdmin.getIsActive())) {
				errorMessageMap.put("isActive_validationMessage",
						approverAdminPropertyValue.getProperty(language + ".isActive.requiredValidationLabel"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return errorMessageMap;
	}

	public Boolean checkNull(ApproverAdmin approverAdmin) {
		Boolean isValid = false;
		isValid = true;
		return isValid;
	}
}
