package app.globalSetup.businesstier;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import app.appmodel.User;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.UserRepository;

//newDependentBeanImportGoesHere

@Service
public class UserManager {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserValidator userValidator;
	BCryptPasswordEncoder bCryptPasswordEncoder;


	@Autowired
	UserManager(BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	// newAutowiredPropertiesGoesHere

	public User getUser(HttpServletRequest request, Long id) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USER_READ");

		errorMessageMap = userValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(errorMessageMap) && errorMessageMap.keySet().size()>0) {
			return null;
		}
		User user = userRepository.getOne(id);

		return user;
	}

	public List<User> getUsers(HttpServletRequest request) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USER_READ");

		errorMessageMap = userValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(errorMessageMap) && errorMessageMap.keySet().size()>0) {
			return null;
		}
		List<User> usersList = new ArrayList<User>();
		Iterable<User> users = userRepository.findAll();
		for (User user : users) {
			usersList.add(user);
			// newGenerateEntityFetchActionGoesHere
		}
		return usersList;
	}
	
	public List<User> getUseresByUserName(String userName) {
		List<User> usersList = new ArrayList<User>();
		Iterable<User> users = userRepository.findByUserName(userName);
		for (User user : users) {
			usersList.add(user);
		}
		return usersList;
	}
	public User findUserByUserName(String userName) {
		return userRepository.findUserByUserName(userName);
	}

	public List<User> getUseresByEmail(String email) {
		List<User> usersList = new ArrayList<User>();
		Iterable<User> users = userRepository.findByEmail(email);
		for (User user : users) {
			usersList.add(user);
		}
		return usersList;
	}

	public Map<String, Object> save(HttpServletRequest request, User user) {
		Map<String, Object> messageMap = new LinkedHashMap<String, Object>();
		messageMap.put("error", "");
		try {
			Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();

			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("USER_CREATE");

			errorMessageMap = userValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isEmpty(errorMessageMap)) {
				errorMessageMap = userValidator.getValidated(request, user);
			}

			if (errorMessageMap.keySet().size() > 0) {
				messageMap.put("error", "found");
				messageMap.putAll(errorMessageMap);
			} else {
				File path = new File("uploads/user");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}
				user.setCreated(null);
				user.setUpdated(null);
				user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
				
				user = userRepository.save(user);
				messageMap.put("successMessage", "Successfully Saved User Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(UserManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: " + e.getMessage());
			return messageMap;
		}
	}
	public Map<String, Object> simpleUpdate(HttpServletRequest request, User user) {
		Map<String, Object> messageMap = new LinkedHashMap<String, Object>();
		
		try {
			messageMap = userValidator.getValidated(request, user);
			if (messageMap.keySet().size()==0) {
				user = userRepository.save(user);
				messageMap.put("error", "");
			}else {
				messageMap.put("error", "found");
			}
			
			return messageMap;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(UserManager.class.getName());
			logger.info(e);
			messageMap.put("error", "found");
			messageMap.put("technicalError", "Techical Error: " + e.getMessage());
			return messageMap;
		}
	}



	public List<User> saveMultipleUserData(HttpServletRequest request, List<User> users) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USER_CREATE");

		errorMessageMap = userValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(errorMessageMap) && errorMessageMap.keySet().size()>0) {
			return null;
		}
		List<Map<String, Object>> messageList = new ArrayList<Map<String, Object>>();
		Map<String, Object> messageMap = new LinkedHashMap();
		try {
			for (User user : users) {
				errorMessageMap = new LinkedHashMap<String, Object>();
				errorMessageMap = userValidator.getValidated(request, user);
				user = userRepository.save(user);
				messageMap.put("addSucessmessage", "Successfully User Added.");
				messageList.add(messageMap);
				// newGenerateEntityAddActionGoesHere
			}

			return getUsers(request);
		} catch (Exception e) {
			Logger logger = Logger.getLogger(UserManager.class.getName());
			logger.info(e);
			messageMap.put("addErrorMessage", "Techical Error: " + e.getMessage());
			messageList.add(messageMap);
		}
		return getUsers(request);
	}

	public String remove(HttpServletRequest request, Long id) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USER_DELETE");

		errorMessageMap = userValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(errorMessageMap) && errorMessageMap.keySet().size()>0) {
			return null;
		}
		String removeMessage = "Successfully User Deleted. Id: " + id + ".";
		try {
			User user = userRepository.findOne(id);
			if (UtilValidate.isNotEmpty(user)) {

				userRepository.delete(id);
			}

		} catch (Exception e) {
			removeMessage = "Error To Delete. Cause: " + e.getMessage();
		}

		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}

	public String removeAll(HttpServletRequest request) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USER_DELETE");

		errorMessageMap = userValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(errorMessageMap) && errorMessageMap.keySet().size()>0) {
			return null;
		}
		String removeMessage = "Successfully All User Deleted.";
		userRepository.deleteAll();
		// newGenerateEntityDeleteActionGoesHere
		return removeMessage;
	}

	public Page<User> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USER_READ");

		errorMessageMap = userValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(errorMessageMap) && errorMessageMap.keySet().size()>0) {
			return null;
		}else {
			System.out.println("########### errorMessageMap: " +errorMessageMap);
		}
		return userRepository.findAll(new PageRequest(page, size));
	}

	public Page<User> findPaginatedUsersBySearchToken(HttpServletRequest request, int page, int size,
			String searchToken) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USER_READ");

		errorMessageMap = userValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(errorMessageMap) && errorMessageMap.keySet().size()>0) {
			return null;
		}
		return userRepository.findPaginatedUsersBySearchToken(searchToken, new PageRequest(page, size));
	}

	public User findOne(HttpServletRequest request, Long id) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USER_READ");

		errorMessageMap = userValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(errorMessageMap) && errorMessageMap.keySet().size()>0) {
			return null;
		}
		User user = userRepository.findOne(id);
		return user;
	}

	public Page<User> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String, Object> errorMessageMap = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("USER_READ");

		errorMessageMap = userValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(errorMessageMap) && errorMessageMap.keySet().size()>0) {
			return null;
		}
		List<User> users = userRepository.findById(id);
		Page<User> pages = null;
		pages = new PageImpl<User>(users, pageRequest, users.size());
		return pages;
	}

}
