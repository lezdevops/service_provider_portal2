package app.globalSetup.businesstier;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import app.appmodel.MessageReceiver;
import app.appmodel.Notification;
import app.appmodel.NotificationDetails;
import app.appmodel.NotificationProcess;
import app.appmodel.NotificationRouteMap;
import app.appmodel.User;
import app.framework.businesstier.UtilValidate;
import app.globalSetup.persistencetier.NotificationRepository;
import app.security.httpRequest.HttpRequestProcessor;

@Service
public class NotificationManager {
	private static final org.apache.log4j.Logger NotificationManagerLogger = org.apache.log4j.Logger
			.getLogger(NotificationManager.class);
	@Autowired
	private NotificationRepository notificationRepository;
	@Autowired
	private NotificationValidator notificationValidator;

	// newAutowiredPropertiesGoesHere
	public Map<String, Object> getNotificationForEdit(HttpServletRequest request, Long id) {
		Notification newNotification = new Notification();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATION_EDIT");

		returnResult = notificationValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		Notification notification = null;
		try {
			notification = notificationRepository.getOne(id);
			if (UtilValidate.isNotEmpty(notification)) {
				newNotification.setId(notification.getId());
				newNotification.setNotificationProcess(notification.getNotificationProcess());
				newNotification.setMessage(notification.getMessage());
				newNotification.setOldObjectVal(notification.getOldObjectVal());
				newNotification.setNewObjectVal(notification.getNewObjectVal());
				newNotification.setAttachment(notification.getAttachment());
				newNotification.setUrl(notification.getUrl());

			}
			returnResult.put("notification", newNotification);
		} catch (Exception e) {
			NotificationManagerLogger
					.info("Exception Occured During Fetching Notification Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public Map<String, Object> getNotification(HttpServletRequest request, Long id) {
		Notification newNotification = new Notification();
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATION_READ");

		returnResult = notificationValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Notification notification = notificationRepository.getOne(id);
			if (UtilValidate.isNotEmpty(notification)) {
				newNotification.setId(notification.getId());
				newNotification.setNotificationProcess(notification.getNotificationProcess());
				newNotification.setMessage(notification.getMessage());
				newNotification.setOldObjectVal(notification.getOldObjectVal());
				newNotification.setNewObjectVal(notification.getNewObjectVal());
				newNotification.setAttachment(notification.getAttachment());
				newNotification.setUrl(notification.getUrl());

			}

			returnResult.put("notification", newNotification);
		} catch (Exception e) {
			NotificationManagerLogger
					.info("Exception Occured During Fetching Notification Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}
		return returnResult;
	}

	public Map<String, Object> getNotifications(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATION_READ");

		returnResult = notificationValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			List<Notification> notificationsList = new ArrayList<Notification>();
			Iterable<Notification> notifications = notificationRepository.findAll();
			for (Notification notification : notifications) {
				notificationsList.add(notification);
				// newGenerateEntityFetchActionGoesHere
			}
			returnResult.put("notifications", notificationsList);
		} catch (Exception e) {
			NotificationManagerLogger
					.info("Exception Occured During Fetching Notification List Data. caused by: " + e.getMessage());
			returnResult.put("error", "found");
		}

		return returnResult;
	}

	public List<Notification> getNotificationesByNotificationProcess(NotificationProcess NotificationProcess, Long id) {
		List<Notification> notificationsList = new ArrayList<Notification>();
		Iterable<Notification> notifications = null;
		if (UtilValidate.isNotEmpty(id)) {
			notifications = notificationRepository.findByNotificationProcessDuringUpdate(NotificationProcess, id);
		} else {
			notifications = notificationRepository.findByNotificationProcess(NotificationProcess);
		}
		for (Notification notification : notifications) {
			notificationsList.add(notification);
		}
		return notificationsList;
	}

	public Map<String, Object> create(HttpServletRequest request, Notification notification) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("NOTIFICATION_CREATE");

			returnResult = notificationValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = notificationValidator.getValidated(request, notification);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/notification");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				notification = notificationRepository.save(notification);
				returnResult.put("successMessage", "Successfully Saved Notification Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			NotificationManagerLogger
					.info("Exception Occured During Creating Notification Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}
	@Autowired
	private NotificationProcessManager notificationProcessManager;
	@Autowired
	private NotificationDetailsManager notificationDetailsManager;
	@Autowired
	private NotificationRouteMapManager notificationRouteMapManager;
	
	public void send(HttpServletRequest request, Object oldValue, Object newValue,String notificationProcessStr) {
		Runnable runnable = () -> {
			 
			Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
			try {
				ObjectMapper Obj = new ObjectMapper(); 
		        try { 
		            NotificationProcess notificationProcess = notificationProcessManager.findNotificationProcessByName(notificationProcessStr);
		            if (UtilValidate.isNotEmpty(notificationProcess) && notificationProcess.getIsActive()) {
		            	Notification notification = new Notification();
		            	if (UtilValidate.isNotEmpty(oldValue)) {
		            		String oldValueStr = Obj.writeValueAsString(oldValue); 
		            		notification.setOldObjectVal(oldValueStr);
						}
		            	if (UtilValidate.isNotEmpty(newValue)) {
		            		String newValueStr = Obj.writeValueAsString(newValue); 
		            		notification.setNewObjectVal(newValueStr);
						}
			            
		            	notification.setNotificationProcess(notificationProcess);
		            	notification.setUrl("");
		            	notification.setAttachment("");
		            	notification.setMessage(notificationProcessStr+" Occured.");
		            	List<NotificationRouteMap> notificationRouteMapList = notificationRouteMapManager.findByNotificationProcess(notificationProcess);
		            	
		            	
		            	notification = notificationRepository.save(notification);
		            	List<NotificationDetails> notificationDetailsList = new ArrayList<NotificationDetails>();
		            	
		            	for (NotificationRouteMap notificationRouteMap : notificationRouteMapList) {
		            		List<MessageReceiver> messageReceivers = notificationRouteMap.getMessageReceivers();
		            		for (MessageReceiver messageReceiver : messageReceivers) {
		            			NotificationDetails notificationDetails = new NotificationDetails();
			            		notificationDetails.setIsRead(false);
			            		notificationDetails.setSenderId(notificationRouteMap.getSenderId());
			            		notificationDetails.setReceiverId(messageReceiver.getName());
			            		notificationDetails.setNotification(notification);
			            		notificationDetailsList.add(notificationDetails);
							}
		            		
						}
		            	notificationDetailsManager.saveAll(notificationDetailsList);
		            	
		    			returnResult.put("successMessage", "Successfully Saved Notification Data.");
					}
		            
		            
		        } catch (IOException e) { 
		            e.printStackTrace(); 
		        } 
				
				
			} catch (Exception e) {
				returnResult.put("error", "found");
				returnResult.put("technicalError", "Techical Error: " + e.getMessage());
				NotificationManagerLogger.info("Exception Occured During Creating Notification Data. caused by: " + e.getMessage());
				
			}
        };

        System.out.println("Notification Preparing...");
        Thread thread = new Thread(runnable);

        System.out.println("Notification Sending...");
        thread.start();
        
	}
	@Autowired
	private HttpRequestProcessor httpRequestProcessor;
	public List<NotificationDetails> getUserNotifications(HttpServletRequest request) {
		List<NotificationDetails> notificationDetailsList = new ArrayList<NotificationDetails>();

		User user = httpRequestProcessor.getUser(request);
		String receiverId = user.getUsername();
		
		notificationDetailsList = notificationDetailsManager.getNotificationDetailsesByReceiverId(receiverId);
		return notificationDetailsList;
	}

	public Map<String, Object> update(HttpServletRequest request, Notification notification) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		try {
			Set<String> allowedPermissions = new HashSet<>();
			allowedPermissions.add("NOTIFICATION_EDIT");

			returnResult = notificationValidator.validatePermission(request, allowedPermissions);

			if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() == 0) {
				returnResult = notificationValidator.getValidated(request, notification);
			}

			if (returnResult.keySet().size() > 0) {
				returnResult.put("error", "found");
			} else {
				File path = new File("uploads/notification");
				if (!path.exists()) {
					// boolean status = path.mkdirs();
					path.mkdirs();
				}

				notification = notificationRepository.save(notification);
				returnResult.put("successMessage", "Successfully Saved Notification Data.");
			}
			// newGenerateEntityAddActionGoesHere
			return returnResult;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(NotificationManager.class.getName());
			logger.info(e);
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			NotificationManagerLogger
					.info("Exception Occured During Updating Notification Data. caused by: " + e.getMessage());
			return returnResult;
		}
	}

	public Map<String, Object> saveMultipleNotificationData(HttpServletRequest request,
			List<Notification> notifications) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATION_CREATE");

		returnResult = notificationValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}

		try {
			for (Notification notification : notifications) {
				returnResult = new LinkedHashMap<String, Object>();
				returnResult = notificationValidator.getValidated(request, notification);
				notification = notificationRepository.save(notification);
				returnResult.put(notification + " " + notification.getId() + " - Result", "Added Successful.");
				// newGenerateEntityAddActionGoesHere
			}
			return returnResult;
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Techical Error: " + e.getMessage());
			returnResult.put("addErrorMessage", "Techical Error: " + e.getMessage());
			NotificationManagerLogger
					.info("Exception Occured During Adding Notification List Data. caused by: " + e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> remove(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();
		returnResult.put("error", "");
		String removeMessage = "Successfully Notification Deleted. Id: " + id + ".";
		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATION_DELETE");

		returnResult = notificationValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			returnResult.put("error", "found");
			return returnResult;
		}

		try {
			Notification notification = notificationRepository.findOne(id);
			if (UtilValidate.isNotEmpty(notification)) {
				notificationRepository.delete(id);

			}

		} catch (Exception e) {
			returnResult.put("error", "found");
			removeMessage = "Error To Delete. Cause: " + e.getMessage();
			returnResult.put("technicalError", "Delete Message: Technical Error Occured");
			NotificationManagerLogger
					.info("Exception Occured During Removing Notification Data. caused by: " + e.getMessage());
		}
		returnResult.put("removeMessage", removeMessage);

		// newGenerateEntityDeleteActionGoesHere
		return returnResult;
	}

	public Map<String, Object> removeAll(HttpServletRequest request) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATION_DELETE");

		returnResult = notificationValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		String removeMessage = "Successfully All Notification Deleted.";
		try {
			notificationRepository.deleteAll();
			// newGenerateEntityDeleteActionGoesHere
			returnResult.put("removeMessage", removeMessage);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationManagerLogger
					.info("Exception Occured During Removing Notification Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findPaginated(HttpServletRequest request, int page, int size) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATION_READ");

		returnResult = notificationValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<Notification> paginatedResult = notificationRepository.findAll(new PageRequest(page, size));
			returnResult.put("paginatedResult", paginatedResult);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationManagerLogger.info(
					"Exception Occured During Fetching Paginated Notification Data. caused by: " + e.getMessage());
		}
		return returnResult;
	}

	public Map<String, Object> findPaginatedNotificationsBySearchToken(HttpServletRequest request, int page, int size,
			String searchToken) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATION_READ");

		returnResult = notificationValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Page<Notification> paginatedResultBySearchToken = notificationRepository
					.findPaginatedNotificationsBySearchToken(searchToken, new PageRequest(page, size));
			returnResult.put("paginatedResultBySearchToken", paginatedResultBySearchToken);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationManagerLogger
					.info("Exception Occured During Fetching Paginated Notification Data Search. caused by: "
							+ e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findOne(HttpServletRequest request, Long id) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		allowedPermissions.add("NOTIFICATION_READ");

		returnResult = notificationValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			Notification notification = notificationRepository.findOne(id);
			returnResult.put("notification", notification);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationManagerLogger
					.info("Exception Occured During Fetching Notification Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

	public Map<String, Object> findById(HttpServletRequest request, Long id, Pageable pageRequest) {
		Map<String, Object> returnResult = new LinkedHashMap<String, Object>();

		Set<String> allowedPermissions = new HashSet<>();
		List<Notification> notifications = new ArrayList<Notification>();
		allowedPermissions.add("NOTIFICATION_READ");

		returnResult = notificationValidator.validatePermission(request, allowedPermissions);

		if (UtilValidate.isNotEmpty(returnResult) && returnResult.keySet().size() > 0) {
			return returnResult;
		}
		try {
			notifications = notificationRepository.findById(id);
			Page<Notification> pages = new PageImpl<Notification>(notifications, pageRequest, notifications.size());
			returnResult.put("resultPages", pages);
		} catch (Exception e) {
			returnResult.put("error", "found");
			returnResult.put("technicalError", "Technical Error Occured! caused by: " + e.getMessage());
			NotificationManagerLogger
					.info("Exception Occured During Fetching Notification Data. caused by: " + e.getMessage());
		}

		return returnResult;
	}

}
