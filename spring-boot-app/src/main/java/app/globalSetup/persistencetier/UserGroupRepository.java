package app.globalSetup.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.UserGroup;

@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup, Long>{
	List<UserGroup> findById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT userGroup FROM UserGroup userGroup WHERE  cast(userGroup.name AS string) like %:searchToken% OR cast(userGroup.metaData AS string) like %:searchToken% OR cast(userGroup.websiteAddress AS string) like %:searchToken% OR cast(userGroup.phoneNo AS string) like %:searchToken% OR cast(userGroup.email AS string) like %:searchToken% OR cast(userGroup.address AS string) like %:searchToken% OR cast(userGroup.currency AS string) like %:searchToken% OR cast(userGroup.imageAddress AS string) like %:searchToken% OR cast(userGroup.user.userName AS string) like %:searchToken% OR cast(userGroup.created AS string) like %:searchToken% OR cast(userGroup.updated AS string) like %:searchToken%")
	Page<UserGroup> findPaginatedUserGroupsBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
