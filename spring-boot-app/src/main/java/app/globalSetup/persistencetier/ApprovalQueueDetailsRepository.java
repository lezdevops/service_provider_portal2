package app.globalSetup.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.ApprovalQueueDetails;
import app.appmodel.ApprovalQueue;


@Repository
public interface ApprovalQueueDetailsRepository extends JpaRepository<ApprovalQueueDetails, Long>{
	List<ApprovalQueueDetails> findById(Long id);
	List<ApprovalQueueDetails> findByApproverId(String approverId);
	List<ApprovalQueueDetails> findByApproverIdAndIsApproved(String approverId,Boolean isApproved);
	
	ApprovalQueueDetails findApprovalQueueDetailsById(Long id);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT approvalQueueDetails FROM ApprovalQueueDetails approvalQueueDetails WHERE  cast(approvalQueueDetails.approvalQueue.id AS string) like %:searchToken% OR cast(approvalQueueDetails.approverId AS string) like %:searchToken% OR cast(approvalQueueDetails.level AS string) like %:searchToken% OR cast(approvalQueueDetails.isLastApprover AS string) like %:searchToken% OR cast(approvalQueueDetails.isApproved AS string) like %:searchToken%")
	Page<ApprovalQueueDetails> findPaginatedApprovalQueueDetailssBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
