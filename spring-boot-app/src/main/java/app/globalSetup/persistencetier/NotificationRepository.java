package app.globalSetup.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.Notification;
import app.appmodel.NotificationProcess;
import app.appmodel.NotificationDetails;


@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long>{
	List<Notification> findById(Long id);
	Notification findNotificationById(Long id);
	@Query("select notification from Notification notification where notification.notificationProcess = :notificationProcess and notification.id!=:id")
	List<Notification> findByNotificationProcessDuringUpdate(@Param("notificationProcess") NotificationProcess notificationProcess, @Param("id") Long id);List<Notification> findByNotificationProcess(NotificationProcess notificationProcess);
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT notification FROM Notification notification WHERE  cast(notification.notificationProcess.name AS string) like %:searchToken% OR cast(notification.message AS string) like %:searchToken% OR cast(notification.oldObjectVal AS string) like %:searchToken% OR cast(notification.newObjectVal AS string) like %:searchToken% OR cast(notification.attachment AS string) like %:searchToken% OR cast(notification.url AS string) like %:searchToken%")
	Page<Notification> findPaginatedNotificationsBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
