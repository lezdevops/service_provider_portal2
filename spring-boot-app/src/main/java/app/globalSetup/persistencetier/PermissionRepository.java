package app.globalSetup.persistencetier;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.Permission;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long>{
	List<Permission> findById(Long id);
	List<Permission> findByName(String name);
	Permission findPermissionByName(String name);
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT permission FROM Permission permission WHERE  cast(permission.name AS string) like %:searchToken% OR cast(permission.createdDate AS string) like %:searchToken%")
	Page<Permission> findPaginatedPermissionsBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
