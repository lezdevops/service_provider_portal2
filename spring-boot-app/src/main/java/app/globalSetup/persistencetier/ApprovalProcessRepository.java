package app.globalSetup.persistencetier;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.ApprovalProcess;


@Repository
public interface ApprovalProcessRepository extends JpaRepository<ApprovalProcess, Long>{
	List<ApprovalProcess> findById(Long id);
	ApprovalProcess findApprovalProcessById(Long id);
	ApprovalProcess findApprovalProcessByProcessName(String processName);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT approvalProcess FROM ApprovalProcess approvalProcess WHERE  cast(approvalProcess.processName AS string) like %:searchToken% OR cast(approvalProcess.isActive AS string) like %:searchToken% OR cast(approvalProcess.createdBy AS string) like %:searchToken% OR cast(approvalProcess.created AS string) like %:searchToken%")
	Page<ApprovalProcess> findPaginatedApprovalProcesssBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
