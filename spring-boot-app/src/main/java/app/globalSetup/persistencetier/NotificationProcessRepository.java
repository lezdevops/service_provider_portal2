package app.globalSetup.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.NotificationProcess;
import app.appmodel.NotificationRouteMap;
import app.appmodel.Notification;


@Repository
public interface NotificationProcessRepository extends JpaRepository<NotificationProcess, Long>{
	List<NotificationProcess> findById(Long id);
	NotificationProcess findNotificationProcessById(Long id);
	NotificationProcess findNotificationProcessByName(String name);
	
	 
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT notificationProcess FROM NotificationProcess notificationProcess WHERE  cast(notificationProcess.name AS string) like %:searchToken% OR cast(notificationProcess.isActive AS string) like %:searchToken% OR cast(notificationProcess.sendEmail AS string) like %:searchToken% OR cast(notificationProcess.sendSms AS string) like %:searchToken%")
	Page<NotificationProcess> findPaginatedNotificationProcesssBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
