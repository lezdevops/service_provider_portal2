package app.globalSetup.persistencetier;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.MessageReceiver;

@Repository
public interface MessageReceiverRepository extends JpaRepository<MessageReceiver, Long> {
	List<MessageReceiver> findById(Long id);

	MessageReceiver findMessageReceiverById(Long id);

	@Query("select messageReceiver from MessageReceiver messageReceiver where messageReceiver.name = :name and messageReceiver.id!=:id")
	List<MessageReceiver> findByNameDuringUpdate(@Param("name") String name, @Param("id") Long id);

	@Query("select messageReceiver from MessageReceiver messageReceiver where messageReceiver.name = :name")
	List<MessageReceiver> findByName(@Param("name") String name);

	// @Query("SELECT agentType FROM AgentType agentType where agentType.shortCode
	// like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT messageReceiver FROM MessageReceiver messageReceiver WHERE  cast(messageReceiver.name AS string) like %:searchToken% OR cast(messageReceiver.designation AS string) like %:searchToken%  OR cast(messageReceiver.isActive AS string) like %:searchToken%")
	Page<MessageReceiver> findPaginatedMessageReceiversBySearchToken(@Param("searchToken") String searchToken,
			Pageable pageable);

}
