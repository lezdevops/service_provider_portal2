package app.globalSetup.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.NotificationDetails;
import app.appmodel.Notification;


@Repository
public interface NotificationDetailsRepository extends JpaRepository<NotificationDetails, Long>{
	List<NotificationDetails> findById(Long id);
	NotificationDetails findNotificationDetailsById(Long id);
	List<NotificationDetails> findByReceiverIdAndIsRead(String receiverId,Boolean isRead);
	@Query("select notificationDetails from NotificationDetails notificationDetails where notificationDetails.notification = :notification and notificationDetails.id!=:id")
	List<NotificationDetails> findByNotificationDuringUpdate(@Param("notification") Notification notification, @Param("id") Long id);List<NotificationDetails> findByNotification(Notification notification);
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT notificationDetails FROM NotificationDetails notificationDetails WHERE  cast(notificationDetails.notification.message AS string) like %:searchToken% OR cast(notificationDetails.senderId AS string) like %:searchToken% OR cast(notificationDetails.receiverId AS string) like %:searchToken% OR cast(notificationDetails.isRead AS string) like %:searchToken%")
	Page<NotificationDetails> findPaginatedNotificationDetailssBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
