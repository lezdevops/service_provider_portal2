package app.globalSetup.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.NotificationRouteMap;
import app.appmodel.NotificationProcess;


@Repository
public interface NotificationRouteMapRepository extends JpaRepository<NotificationRouteMap, Long>{
	List<NotificationRouteMap> findById(Long id);
	NotificationRouteMap findNotificationRouteMapById(Long id);
	@Query("select notificationRouteMap from NotificationRouteMap notificationRouteMap where notificationRouteMap.notificationProcess = :notificationProcess and notificationRouteMap.id!=:id")
	List<NotificationRouteMap> findByNotificationProcessDuringUpdate(@Param("notificationProcess") NotificationProcess notificationProcess, @Param("id") Long id);
	List<NotificationRouteMap> findByNotificationProcess(NotificationProcess notificationProcess);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT notificationRouteMap FROM NotificationRouteMap notificationRouteMap WHERE  cast(notificationRouteMap.notificationProcess.name AS string) like %:searchToken% OR cast(notificationRouteMap.senderId AS string) like %:searchToken% OR cast(notificationRouteMap.isActive AS string) like %:searchToken%")
	Page<NotificationRouteMap> findPaginatedNotificationRouteMapsBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
