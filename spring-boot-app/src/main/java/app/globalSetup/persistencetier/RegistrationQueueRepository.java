package app.globalSetup.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.RegistrationQueue;

@Repository
public interface RegistrationQueueRepository extends JpaRepository<RegistrationQueue, Long>{
	List<RegistrationQueue> findById(Long id);
	List<RegistrationQueue> findByEmail(String email);
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT registrationQueue FROM RegistrationQueue registrationQueue WHERE  cast(registrationQueue.email AS string) like %:searchToken% OR cast(registrationQueue.userName AS string) like %:searchToken% OR cast(registrationQueue.password AS string) like %:searchToken% OR cast(registrationQueue.securityNumber AS string) like %:searchToken% OR cast(registrationQueue.securityQuestion AS string) like %:searchToken% OR cast(registrationQueue.answer AS string) like %:searchToken% OR cast(registrationQueue.accountType AS string) like %:searchToken% OR cast(registrationQueue.jsonString AS string) like %:searchToken% OR cast(registrationQueue.createdDate AS string) like %:searchToken% OR cast(registrationQueue.approvalStatus AS string) like %:searchToken%")
	Page<RegistrationQueue> findPaginatedRegistrationQueuesBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
}
