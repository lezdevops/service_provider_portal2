package app.globalSetup.persistencetier;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.ApproverAdmin;


@Repository
public interface ApproverAdminRepository extends JpaRepository<ApproverAdmin, Long>{
	List<ApproverAdmin> findById(Long id);
	ApproverAdmin findApproverAdminById(Long id);
	@Query("select approverAdmin from ApproverAdmin approverAdmin where approverAdmin.name = :name and approverAdmin.id!=:id")
	List<ApproverAdmin> findByNameDuringUpdate(@Param("name") String name, @Param("id") Long id);List<ApproverAdmin> findByName(String name);
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT approverAdmin FROM ApproverAdmin approverAdmin WHERE  cast(approverAdmin.name AS string) like %:searchToken% OR cast(approverAdmin.designation AS string) like %:searchToken% OR cast(approverAdmin.isActive AS string) like %:searchToken%")
	Page<ApproverAdmin> findPaginatedApproverAdminsBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
