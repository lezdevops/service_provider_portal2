package app.globalSetup.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.ApprovalQueue;
import app.appmodel.ApprovalProcess;
import app.appmodel.ApprovalQueueDetails;


@Repository
public interface ApprovalQueueRepository extends JpaRepository<ApprovalQueue, Long>{
	List<ApprovalQueue> findById(Long id);
	ApprovalQueue findApprovalQueueById(Long id);
	@Query("select approvalQueue from ApprovalQueue approvalQueue where approvalQueue.approvalProcess = :approvalProcess and approvalQueue.id!=:id")
	List<ApprovalQueue> findByApprovalProcessDuringUpdate(@Param("approvalProcess") ApprovalProcess approvalProcess, @Param("id") Long id);List<ApprovalQueue> findByApprovalProcess(ApprovalProcess approvalProcess);
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT approvalQueue FROM ApprovalQueue approvalQueue WHERE  cast(approvalQueue.approvalProcess.processName AS string) like %:searchToken% OR cast(approvalQueue.isActive AS string) like %:searchToken% OR cast(approvalQueue.attachment AS string) like %:searchToken% OR cast(approvalQueue.approvalMessage AS string) like %:searchToken%")
	Page<ApprovalQueue> findPaginatedApprovalQueuesBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);
	
	
}
