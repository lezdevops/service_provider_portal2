package app.globalSetup.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findUserByEmail(String email);

	List<User> findById(Long id);

	User findUserByUserName(String userName);

	List<User> findByUserName(String userName);

	List<User> findByEmail(String userName);

	// @Query("SELECT agentType FROM AgentType agentType where agentType.shortCode
	// like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT user FROM User user WHERE  cast(user.userName AS string) like %:searchToken% "
			+ "OR cast(user.password AS string) like %:searchToken% "
			+ "OR cast(user.passwordHint AS string) like %:searchToken% "
			+ "OR cast(user.email AS string) like %:searchToken% "
			+ "OR cast(user.firstName AS string) like %:searchToken% "
			+ "OR cast(user.lastName AS string) like %:searchToken% "
			+ "OR cast(user.active AS string) like %:searchToken% ")
	Page<User> findPaginatedUsersBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);

}
