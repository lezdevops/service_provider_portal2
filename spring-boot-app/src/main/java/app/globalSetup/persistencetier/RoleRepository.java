package app.globalSetup.persistencetier;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.appmodel.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
	List<Role> findById(Long id);
	Role findRoleById(Long id);
	Role findByName(String role);
	
	
	//@Query("SELECT agentType FROM AgentType agentType where agentType.shortCode like %:searchToken% OR agentType.description like %:searchToken%")
	@Query("SELECT role FROM Role role WHERE  cast(role.name AS string) like %:searchToken% OR cast(role.user.userName AS string) like %:searchToken% OR cast(role.createdDate AS string) like %:searchToken%")
	Page<Role> findPaginatedRolesBySearchToken(@Param("searchToken") String searchToken, Pageable pageable);

}
