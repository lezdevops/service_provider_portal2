package app.security.httpRequest;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.appmodel.Permission;
import app.appmodel.Role;
import app.appmodel.User;
import app.common.persistencetier.UserService;
import app.framework.businesstier.UtilValidate;
import app.security.jwtsecurity.SecurityConstants;
import io.jsonwebtoken.Jwts;

//referer=http://localhost:4200/, content-length=2, accept-language=en-US,en;q=0.5, 
//origin=http://localhost:4200, accept=application/json, text/plain, */*,
//access-control-allow-origin=*, serviceproviderid=1, 
//authorization=Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6W10sImV4cCI6MTU1MTg4Nzc1NX0.0k4zdhhFOkswBPGXbqi6G1c0JRMSQS9sOAH7klC3GB4EVJ3xb5lOrgm_bcT3uiSBclT0tQxDxm79k9haI02Gfw, 
//host=localhost:8095, content-type=application/json, connection=keep-alive, accept-encoding=gzip, deflate, 
//user-agent=Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0

@Service
public class HttpRequestProcessor {
	@Autowired
	private UserService userService;

	public Map<String, String> getHeadersInfo(HttpServletRequest request) {
		Map<String, String> map = new HashMap<String, String>();
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
		}
		return map;
	}

	public String getRequestFrom(HttpServletRequest request) {
		Map<String, String> headers = getHeadersInfo(request);
		String requestStr = headers.get("referer");
		return requestStr;
	}

	public String getRequestTo(HttpServletRequest request) {
		Map<String, String> headers = getHeadersInfo(request);
		String requestStr = headers.get("host");
		return requestStr;
	}

	public String getContentType(HttpServletRequest request) {
		Map<String, String> headers = getHeadersInfo(request);
		String requestStr = headers.get("content-type");
		return requestStr;
	}

	public String getUserAgent(HttpServletRequest request) {
		Map<String, String> headers = getHeadersInfo(request);
		String requestStr = headers.get("user-agent");
		return requestStr;
	}

	public String getToken(HttpServletRequest request) {
		Map<String, String> headers = getHeadersInfo(request);
		String requestStr = headers.get("authorization");
		return requestStr;
	}

	public User getUserDetailsByToken(String token) {
		User user = null;
		if (token != null) {
			String userName = Jwts.parser().setSigningKey(SecurityConstants.SECRET)
					.parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX, "")).getBody().getSubject();

			if (userName != null) {
				user = userService.findByUserName(userName);
			}
		}

		return user;
	}
	public User getUser(HttpServletRequest request) {
		return getUserDetailsByToken(getToken(request));
	}
	public boolean hasRole(HttpServletRequest request, Set<String> allowedRoles) {
		boolean permitted = false;
		
		User user = getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			List<Role> roles = user.getRoles();
			if (UtilValidate.isEmpty(roles)) {
				return permitted;
			}else {
				for (Role role : roles) {
					String roleName = role.getName();
					
					if (UtilValidate.isNotEmpty(roleName) && allowedRoles.contains(roleName)) {
						permitted = true;
						System.out.println("#######################################################################");
						System.out.println("###################### "+roleName+" ###################################");
						System.out.println("#######################################################################");
						break;
					}		
					
				}
			}
		}
		return permitted;
	}
	public boolean hasPermission(HttpServletRequest request, Set<String> allowedPermissions) {
		boolean permitted = false;
		
		User user = getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			List<Role> roles = user.getRoles();
			if (UtilValidate.isEmpty(roles)) {
				return permitted;
			}else {
				for (Role role : roles) {
					List<Permission> permissions = role.getPermissions();

					for (Permission permission : permissions) {

						String permissionName = permission.getName();
						if (allowedPermissions.contains(permissionName)) {
							permitted = true;
							System.out.println("#######################################################################");
							System.out.println("###################### "+permissionName+" ###################################");
							System.out.println("#######################################################################");
							
							break;
						}
						
					}
					if (permitted) {
						break;
					}		
					
				}
			}
		}
		return permitted;
	}
	public Map<String,Object> getPermissionMap(HttpServletRequest request, String pageName) {
		Map<String,Object> permissionMap = new LinkedHashMap<String,Object>();
		Set<String> allowedPermissions = new LinkedHashSet<String>();
		allowedPermissions.add(pageName.toUpperCase()+"_CREATE");
		allowedPermissions.add(pageName.toUpperCase()+"_READ");
		allowedPermissions.add(pageName.toUpperCase()+"_EDIT");
		allowedPermissions.add(pageName.toUpperCase()+"_DELETE");
		allowedPermissions.add(pageName.toUpperCase()+"_REPORT");
		
		permissionMap.put("hasCreatePermission", false);
		permissionMap.put("hasReadPermission", false);
		permissionMap.put("hasEditPermission", false);
		permissionMap.put("hasDeletePermission", false);
		permissionMap.put("hasReportPermission", false);
		
		User user = getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			List<Role> roles = user.getRoles();
			if (UtilValidate.isEmpty(roles)) {
				return permissionMap;
			}else {
				for (Role role : roles) {
					List<Permission> permissions = role.getPermissions();

					for (Permission permission : permissions) {
						if (UtilValidate.isNotEmpty(permission)) {
							String permissionName = permission.getName();
							if (allowedPermissions.contains(permissionName) && permissionName.contains("CREATE")) {
								System.out.println("Found Create Permission");
								permissionMap.put("hasCreatePermission", true);
							}
							if (allowedPermissions.contains(permissionName) && permissionName.contains("READ")) {
								System.out.println("Found Read Permission");
								permissionMap.put("hasReadPermission", true);
							}
							if (allowedPermissions.contains(permissionName) && permissionName.contains("EDIT")) {
								System.out.println("Found Edit Permission");
								permissionMap.put("hasEditPermission", true);
							}
							if (allowedPermissions.contains(permissionName) && permissionName.contains("DELETE")) {
								System.out.println("Found Delete Permission");
								permissionMap.put("hasDeletePermission", true);
							}
							if (allowedPermissions.contains(permissionName) && permissionName.contains("REPORT")) {
								System.out.println("Found Report Permission");
								permissionMap.put("hasReportPermission", true);
							}
						}
						
					}	
					
				}
			}
		}
		return permissionMap;
	}

	public Map<String,Object> validatePermission(HttpServletRequest request, Set<String> allowedPermissions) {
		Map<String,Object> errorMessageMap = new LinkedHashMap<String,Object>();
		boolean permitted = false;
		User user = getUser(request);
		if (UtilValidate.isNotEmpty(user)) {
			if (hasPermission(request, allowedPermissions)) {
				permitted = true;
			}
		}else {
			 permitted = true;
		}
		if (!permitted) {
			errorMessageMap.put("permission_validationMessage","Permission Not Found!");
		}
		return errorMessageMap;
	}
}
