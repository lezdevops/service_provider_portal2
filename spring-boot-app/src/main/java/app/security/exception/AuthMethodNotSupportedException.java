package app.security.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * Created by eclipse
 */
public class AuthMethodNotSupportedException extends AuthenticationException {

  public AuthMethodNotSupportedException(String message) {
    super(message);
  }
}
