package app.security.jwtsecurity;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

import app.security.ajaxauth.AjaxAuthenticationProvider;
import app.security.ajaxauth.AjaxLoginProcessingFilter;


@EnableGlobalMethodSecurity(
  securedEnabled = true,
  jsr250Enabled = true,
  prePostEnabled = true
)
@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

  public static final String AUTHENTICATION_URL = "/api/auth/login";
  public static final String REFRESH_TOKEN_URL = "/api/auth/token";
  public static final String SIGN_UP_URL = "/users";
  public static final String API_ROOT_URL = "/api/**";

  @Qualifier("userDetailsServiceImpl")
  @Autowired
  private UserDetailsService userDetailsService;

  @Autowired
  BCryptPasswordEncoder bCryptPasswordEncoder;

  @Autowired private AuthenticationSuccessHandler successHandler;
  @Autowired private AuthenticationFailureHandler failureHandler;
  @Autowired private AjaxAuthenticationProvider ajaxAuthenticationProvider;
  @Autowired private AuthenticationManager authenticationManager;
  @Autowired private ObjectMapper objectMapper;

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }



//  @Autowired
//  JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;


  @Autowired
  JwtAuthenticationEntryPoint entryPoint;

//  @Autowired
//  JwtAuthenticationProvider jwtAuthenticationProvider;
//
//  @Autowired
//  AjaxAuthenticationProvider ajaxAuthenticationProvider;
//
//  @Autowired
//  AuthenticationSuccessHandler authenticationSuccessHandler;
//
//  @Autowired
//  AuthenticationFailureHandler authenticationFailureHandler;
//
//  @Autowired
//  private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

//
//  @Autowired
//  ObjectMapper objectMapper;
//
//  @Autowired
//  AuthenticationManager authenticationManager;
//
//  public AuthenticationManager authenticationManager() {
//    return new ProviderManager(Collections.singletonList(jwtAuthenticationProvider));
//  }
//


  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    List<String> permitAllEndpointList = Arrays.asList(
      AUTHENTICATION_URL,
      REFRESH_TOKEN_URL,
      "/console"
    );
    http.
	authorizeRequests()
    .antMatchers("/login","/public/**").permitAll()
		.antMatchers("/").permitAll()
		.antMatchers("/registration").permitAll()
		.antMatchers("/frontPage").permitAll()
		//.antMatchers("/hasPermission/**").permitAll()
		.antMatchers("/app").permitAll()
		.antMatchers("/app/**").permitAll()
		//.antMatchers("/api").permitAll()
		.antMatchers("/public/**").permitAll()
		.antMatchers("/userMessages").permitAll()
		.antMatchers("/userMessages/**").permitAll()
		.antMatchers("/applicationComponents").permitAll()
		.antMatchers("/applicationComponents/**").permitAll()
		.antMatchers("/applicationTables").permitAll()
		.antMatchers("/applicationTables/**").permitAll()
		.antMatchers("/userNotifications").permitAll()
		.antMatchers("/userNotifications/**").permitAll()
		.antMatchers("/bootscaff").permitAll()
		.antMatchers("/bootscaff/**").permitAll()
    	.antMatchers("/frontPage/**").permitAll()
		.antMatchers("/admin/**").hasAuthority("ADMIN").anyRequest()
		.authenticated().and().csrf().disable().formLogin()
		.loginPage("/login").failureUrl("/login?error=true")
		.defaultSuccessUrl("/")
		.usernameParameter("email")
		.passwordParameter("password")
		.and().logout()
		.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
		.logoutSuccessUrl("/login").and().exceptionHandling()
		.accessDeniedPage("/access-denied");
    
    ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry configurer = http.cors().and().csrf().disable()
      .authorizeRequests();
    configurer
    
      .antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
      .antMatchers(HttpMethod.POST, "/roles").permitAll()
            
      .anyRequest().authenticated()
      .and().exceptionHandling().authenticationEntryPoint(entryPoint)
      .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    http.addFilter(new AjaxLoginProcessingFilter(this.authenticationManager()));
    http.addFilter(new JwtAuthorizationFilter(this.authenticationManager(), userDetailsService));
    // http.
    // addFilterBefore(buildJwtAuthenticationProcessingFilter(permitAllEndpointList,API_ROOT_URL),UsernamePasswordAuthenticationFilter.class);
    http.headers().cacheControl();
  }
  protected void configure_old(HttpSecurity http) throws Exception {

	    List<String> permitAllEndpointList = Arrays.asList(
	      AUTHENTICATION_URL,
	      REFRESH_TOKEN_URL,
	      "/console"
	    );

	    ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry configurer = http.cors().and().csrf().disable()
	      .authorizeRequests();
	    configurer
	      .antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
	      .antMatchers(HttpMethod.POST, "/roles").hasAnyRole("ADMIN")
	      .antMatchers(HttpMethod.POST, "/shops").hasAnyRole("ADMIN","USER")
	      .antMatchers(HttpMethod.POST, "/stocks").hasAnyRole("ADMIN","USER")
	      .antMatchers(HttpMethod.POST, "/payTypes").hasAnyRole("ADMIN","USER")
	      .antMatchers(HttpMethod.POST, "/stockItems").hasAnyRole("ADMIN","USER")
	      .antMatchers(HttpMethod.POST, "/stockItems/sell").hasAnyRole("ADMIN","USER")
	            .antMatchers(HttpMethod.GET, "/shops").hasAnyRole("ADMIN","USER")
	            .antMatchers(HttpMethod.GET, "/categorys").hasAnyRole("ADMIN","USER")
	            .antMatchers(HttpMethod.POST, "/categorys").hasAnyRole("ADMIN","USER")
	            .antMatchers(HttpMethod.GET, "/customers").hasAnyRole("ADMIN","USER")
	            .antMatchers(HttpMethod.POST, "/customers").hasAnyRole("ADMIN","USER")
	            .antMatchers(HttpMethod.GET, "/employees").hasAnyRole("ADMIN","USER")
	            .antMatchers(HttpMethod.POST, "/employees").hasAnyRole("ADMIN","USER")
	            .antMatchers(HttpMethod.GET, "/merchants").hasAnyRole("ADMIN","USER")
	            .antMatchers(HttpMethod.POST, "/merchants").hasAnyRole("ADMIN","USER")
	      .antMatchers(HttpMethod.POST, "/products").hasAnyRole("ADMIN","USER")
	      .antMatchers(HttpMethod.GET, "/products").hasAnyRole("ADMIN","USER")
	            .antMatchers(HttpMethod.POST, "/expenses").hasAnyRole("ADMIN","USER")
	            .antMatchers(HttpMethod.GET, "/expenses").hasAnyRole("ADMIN","USER")
	      .anyRequest().authenticated()
	      .and().exceptionHandling().authenticationEntryPoint(entryPoint)
	      .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	    http.addFilter(new AjaxLoginProcessingFilter(this.authenticationManager()));
	    http.addFilter(new JwtAuthorizationFilter(this.authenticationManager(), userDetailsService));
	    // http.
	    // addFilterBefore(buildJwtAuthenticationProcessingFilter(permitAllEndpointList,API_ROOT_URL),UsernamePasswordAuthenticationFilter.class);
	    http.headers().cacheControl();
	  }

}
