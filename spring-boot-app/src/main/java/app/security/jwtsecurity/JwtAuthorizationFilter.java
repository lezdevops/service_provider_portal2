package app.security.jwtsecurity;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import app.appmodel.User;
import io.jsonwebtoken.Jwts;

/**
 * Created by eclipse
 */
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {


  UserDetailsService userDetailsService ;

  public JwtAuthorizationFilter(AuthenticationManager authenticationManager,UserDetailsService userDetailsService) {
    super(authenticationManager);
    this.userDetailsService = userDetailsService;
  }


  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

    String header = request.getHeader(SecurityConstants.HEADER_STRING);

    if (header == null || !header.startsWith(SecurityConstants.TOKEN_PREFIX)) {
      chain.doFilter(request, response);
      return;
    }
    UsernamePasswordAuthenticationToken authentication = getAuthentication(header);
  //  SecurityContextHolder.clearContext();

    SecurityContextHolder.getContext().setAuthentication(authentication);

    chain.doFilter(request, response);
  }

  public UsernamePasswordAuthenticationToken getAuthentication(String token) {

  //  String token = req.getHeader(SecurityConstants.HEADER_STRING);
    System.out.println(token);
    if (token != null) {

      String user = Jwts.parser()
        .setSigningKey(SecurityConstants.SECRET)
        .parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX, ""))
        .getBody()
        .getSubject();

      if (user != null) {
        User userDetails = (User) userDetailsService.loadUserByUsername(user);
        return new UsernamePasswordAuthenticationToken(user, null, userDetails.getAuthorities());
      }
    }

    return null;
  }
  public User getUserDetailsByToken(String token) {
	  User userDetails = null;
	  //  String token = req.getHeader(SecurityConstants.HEADER_STRING);
	    //System.out.println(token);
	    if (token != null) {
	      String user = Jwts.parser()
	        .setSigningKey(SecurityConstants.SECRET)
	        .parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX, ""))
	        .getBody()
	        .getSubject();

	      if (user != null) {
	        userDetails = (User) userDetailsService.loadUserByUsername(user);
	      }
	    }

	    return userDetails;
	  }

}
