package app.security.ajaxauth;

/**
 * Created by eclipse
 */
public class SearchParam {

  private String params;
  private Integer pageNum;

  public String getParams() {
    return params;
  }

  public void setParams(String params) {
    this.params = params;
  }

  public Integer getPageNum() {
    return pageNum;
  }

  public void setPageNum(Integer pageNum) {
    this.pageNum = pageNum;
  }
}
