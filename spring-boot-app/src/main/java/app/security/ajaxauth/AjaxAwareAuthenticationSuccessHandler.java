package app.security.ajaxauth;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import app.appmodel.Customer;
import app.appmodel.Permission;
import app.appmodel.Role;
import app.appmodel.ServiceProvider;
import app.appmodel.User;
import app.framework.businesstier.UtilValidate;
import app.security.jwtsecurity.SecurityConstants;

@Component
public class AjaxAwareAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

  private ObjectMapper mapper = new ObjectMapper();
  private JwtTokenFactory tokenFactory;

  @Autowired
  public AjaxAwareAuthenticationSuccessHandler(JwtTokenFactory tokenFactory) {
    this.tokenFactory = tokenFactory;
  }

  @Override
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

    UsernamePasswordAuthenticationToken jwtAuthenticationToken = (UsernamePasswordAuthenticationToken) authentication;

    User user = (User) jwtAuthenticationToken.getPrincipal();
    Map<String,Object> tokenMap = new HashMap();
    if (UtilValidate.isNotEmpty(user) && user.getActive()==1) {
    	String accessToken = tokenFactory.createJwtToken(user);
        String refreshToken = tokenFactory.createRefreshToken(user);
        
        List<Role> roles  = user.getRoles();
        Set<String> roleNames = new LinkedHashSet<String>();
        Set<String> permisionNames = new LinkedHashSet<String>();
        for (Role role : roles) {
        	roleNames.add(role.getName());
        	//Map<String,Object> returnResult = rolePermissionManager.findByRoleName(request, role.getName());
        	List<Permission> permissions = role.getPermissions();
        	for (Permission permission : permissions) {
        		permisionNames.add(permission.getName());
    		}
    	}
        List<ServiceProvider> serviceProviders = user.getServiceProviders();
        List<Customer> customers = user.getCustomers();
        if (UtilValidate.isNotEmpty(serviceProviders)) {
        	tokenMap.put("userType", "SERVICEPROVIDER");
        	tokenMap.put("serviceProvider", serviceProviders.get(0));
		}else if (UtilValidate.isNotEmpty(customers)) {
			tokenMap.put("userType", "CUSTOMER");
			tokenMap.put("customer", customers.get(0));
		}else {
			tokenMap.put("userType", "ADMIN");
		}
        
        tokenMap.put("userName", user.getUserName());
        tokenMap.put("roles", roleNames);
        tokenMap.put("permissions", permisionNames);
        tokenMap.put("token", SecurityConstants.TOKEN_PREFIX + accessToken);
        tokenMap.put("refreshToken", SecurityConstants.TOKEN_PREFIX + refreshToken);
        //tokenMap.put("userGroupId", userDetails.getUserGroup().getId());
	}


    mapper.writeValue(response.getWriter(), tokenMap);
  }
}
