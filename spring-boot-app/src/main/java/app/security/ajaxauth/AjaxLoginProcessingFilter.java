package app.security.ajaxauth;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import app.security.exception.AuthMethodNotSupportedException;
import app.security.exception.AuthenticationServiceException;
import app.security.jwtsecurity.JwtAuthenticationToken;

public class AjaxLoginProcessingFilter extends UsernamePasswordAuthenticationFilter {


  @Autowired
  AuthenticationSuccessHandler authenticationSuccessHandler = new AjaxAwareAuthenticationSuccessHandler(new JwtTokenFactory());

  @Autowired
  AuthenticationFailureHandler authenticationFailureHandler = new AjaxAwareAuthenticationFailureHandler();

  private ObjectMapper objectMapper = new ObjectMapper();

  private AuthenticationManager authenticationManager;

  public AjaxLoginProcessingFilter(AuthenticationManager authenticationManager){

    this.authenticationManager = authenticationManager;
  }


  @Override
  public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse httpServletResponse) throws AuthenticationException {

    if (!"POST".equals(request.getMethod())) {
      throw new AuthMethodNotSupportedException("Mehtod is not supported");
    }
    LoginRequest loginRequest = new LoginRequest();

    try {
      loginRequest = objectMapper.readValue(request.getReader(), LoginRequest.class);
    } catch (IOException ex) {

    }

    if (StringUtils.isEmpty(loginRequest.getUserName()) || StringUtils.isEmpty(loginRequest.getPassword())) {
      throw new AuthenticationServiceException("Username or password is not provided");
    }

    //JwtUserDetails
    JwtAuthenticationToken jwtAuthenticationToken = new JwtAuthenticationToken(loginRequest.getUserName(), loginRequest.getPassword());

    return this.authenticationManager.authenticate(jwtAuthenticationToken);

  }

  @Override
  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                          Authentication authResult) throws IOException, ServletException {
    authenticationSuccessHandler.onAuthenticationSuccess(request, response, authResult);
  }

  @Override
  protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            AuthenticationException failed) throws IOException, ServletException {
    SecurityContextHolder.clearContext();
    authenticationFailureHandler.onAuthenticationFailure(request, response, failed);
  }

}
