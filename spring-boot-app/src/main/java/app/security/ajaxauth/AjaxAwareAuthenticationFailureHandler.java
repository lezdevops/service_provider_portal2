package app.security.ajaxauth;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import app.base.ErrorCode;
import app.base.ErrorResponse;
import app.security.exception.JwtExpiredTokenException;

@Component
public class AjaxAwareAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private ObjectMapper mapper = new ObjectMapper();



    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        if (e instanceof BadCredentialsException) {
            mapper.writeValue(response.getWriter(), new ErrorResponse(HttpStatus.UNAUTHORIZED, "Invalid username or password", ErrorCode.AUTHENTICATION, new Date()));
        } else if (e instanceof JwtExpiredTokenException) {
            mapper.writeValue(response.getWriter(), new ErrorResponse(HttpStatus.UNAUTHORIZED, "Token has expired", ErrorCode.JWT_TOKEN_EXPIRED, new Date()));
        } else if (e instanceof AuthenticationException) {
            mapper.writeValue(response.getWriter(), new ErrorResponse(HttpStatus.UNAUTHORIZED, "Authentication Failed", ErrorCode.AUTHENTICATION, new Date()));
        }

        mapper.writeValue(response.getWriter(), new ErrorResponse(HttpStatus.UNAUTHORIZED, "Authentication Failed", ErrorCode.AUTHENTICATION, new Date()));
    }
}
