package app.security.ajaxauth;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import app.appmodel.User;
import app.common.persistencetier.UserService;
import app.security.jwtsecurity.JwtAuthenticationToken;

@Component
public class AjaxAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    UserService userService;

    private BCryptPasswordEncoder encoder;

    @Autowired
    public AjaxAuthenticationProvider(BCryptPasswordEncoder encoder) {
        this.encoder = encoder;
    }


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userName = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();
        User user = userService.findByUserName(userName);
        if (!encoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("Authentication failed.Username or password is not valid");
        }

        if (user.getRoles() == null) {
            throw new InsufficientAuthenticationException("User has no role assigned");
        }

        List<GrantedAuthority> authorities = user.getRoles()
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());

        return new JwtAuthenticationToken(user,null,authorities);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return JwtAuthenticationToken.class.isAssignableFrom(aClass);
    }
}
