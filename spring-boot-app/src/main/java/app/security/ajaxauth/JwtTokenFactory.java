package app.security.ajaxauth;

import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import app.appmodel.User;
import app.security.jwtsecurity.SecurityConstants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenFactory {

    public String createJwtToken(User userDetails) {

        String userName = userDetails.getUsername();

        if (StringUtils.isEmpty(userName)) {
            throw new IllegalArgumentException("Can not create jwt token without username");
        }

        if (userDetails.getAuthorities() == null) {
            throw new IllegalArgumentException("User does not have any priviliges");

        }

        Claims claims = Jwts.claims().setSubject(userName);
        claims.put("scopes",userDetails.getAuthorities().stream()
                .map( authority -> authority.getAuthority()).collect(Collectors.toList()));

        String token = Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET).compact();

        return token;
    }


    public String createRefreshToken(UserDetails userDetails) {

        String userName = userDetails.getUsername();

        if (StringUtils.isEmpty(userName)) {
            throw new IllegalArgumentException("Can not create jwt token without username");
        }

        if (userDetails.getAuthorities() == null) {
            throw new IllegalArgumentException("User does not have any priviliges");

        }

        Claims claims = Jwts.claims().setSubject(userName);

        claims.put("scopes", Arrays.asList("ROLE_REFRESH_TOKEN"));

        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET)
                .compact();


        return token;
    }

}
