package app.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "entityName", "permission" })
class EntityPermission {

	@JsonProperty("entityName")
	private String entityName;
	@JsonProperty("permission")
	private Permission permission;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("entityName")
	public String getEntityName() {
		return entityName;
	}

	@JsonProperty("entityName")
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	@JsonProperty("permission")
	public Permission getPermission() {
		return permission;
	}

	@JsonProperty("permission")
	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "userPermissions" })
class Example {

	@JsonProperty("userPermissions")
	private List<UserPermission> userPermissions = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("userPermissions")
	public List<UserPermission> getUserPermissions() {
		return userPermissions;
	}

	@JsonProperty("userPermissions")
	public void setUserPermissions(List<UserPermission> userPermissions) {
		this.userPermissions = userPermissions;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "pageName", "entityPermissions" })
class PagePermission {

	@JsonProperty("pageName")
	private String pageName;
	@JsonProperty("entityPermissions")
	private List<EntityPermission> entityPermissions = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("pageName")
	public String getPageName() {
		return pageName;
	}

	@JsonProperty("pageName")
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	@JsonProperty("entityPermissions")
	public List<EntityPermission> getEntityPermissions() {
		return entityPermissions;
	}

	@JsonProperty("entityPermissions")
	public void setEntityPermissions(List<EntityPermission> entityPermissions) {
		this.entityPermissions = entityPermissions;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "create", "read", "update", "delete", "report" })
class Permission {

	@JsonProperty("create")
	private Boolean create;
	@JsonProperty("read")
	private Boolean read;
	@JsonProperty("update")
	private Boolean update;
	@JsonProperty("delete")
	private Boolean delete;
	@JsonProperty("report")
	private Boolean report;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("create")
	public Boolean getCreate() {
		return create;
	}

	@JsonProperty("create")
	public void setCreate(Boolean create) {
		this.create = create;
	}

	@JsonProperty("read")
	public Boolean getRead() {
		return read;
	}

	@JsonProperty("read")
	public void setRead(Boolean read) {
		this.read = read;
	}

	@JsonProperty("update")
	public Boolean getUpdate() {
		return update;
	}

	@JsonProperty("update")
	public void setUpdate(Boolean update) {
		this.update = update;
	}

	@JsonProperty("delete")
	public Boolean getDelete() {
		return delete;
	}

	@JsonProperty("delete")
	public void setDelete(Boolean delete) {
		this.delete = delete;
	}

	@JsonProperty("report")
	public Boolean getReport() {
		return report;
	}

	@JsonProperty("report")
	public void setReport(Boolean report) {
		this.report = report;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "user", "pagePermissions" })
class UserPermission {

	@JsonProperty("user")
	private String user;
	@JsonProperty("pagePermissions")
	private List<PagePermission> pagePermissions = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("user")
	public String getUser() {
		return user;
	}

	@JsonProperty("user")
	public void setUser(String user) {
		this.user = user;
	}

	@JsonProperty("pagePermissions")
	public List<PagePermission> getPagePermissions() {
		return pagePermissions;
	}

	@JsonProperty("pagePermissions")
	public void setPagePermissions(List<PagePermission> pagePermissions) {
		this.pagePermissions = pagePermissions;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
